<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeCheckNo1AdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_check_no1_admins', function (Blueprint $table) {
            $table->id();
            $table->string('date');
            $table->text('url_folder_early_month',  2048)->nullable();
            $table->text('url_folder_mid_month',  2048)->nullable();
            $table->integer('mail_noti_file_template_id')->nullable();
            $table->integer('mail_noti_not_done_id')->nullable();
            $table->integer('mail_noti_to_check_id')->nullable();
            $table->date('deadline')->nullable();
            $table->integer('step')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_check_no1_admins');
    }
}
