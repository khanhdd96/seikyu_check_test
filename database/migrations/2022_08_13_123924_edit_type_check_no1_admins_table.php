<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditTypeCheckNo1AdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_check_no1_admins', function (Blueprint $table) {
            $table->date('deadline_mid_month')->nullable();
            $table->date('deadline_early_month')->nullable();
            $table->integer('step_early_month')->nullable();
            $table->integer('step_mid_month')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_check_no1_admins', function (Blueprint $table) {
            $table->dropColumn('deadline_mid_month');
            $table->dropColumn('deadline_early_month');
            $table->dropColumn('step_early_month');
            $table->dropColumn('step_mid_month');
        });
    }
}
