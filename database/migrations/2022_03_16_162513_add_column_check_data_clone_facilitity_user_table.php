<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCheckDataCloneFacilitityUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilitity_user', function (Blueprint $table) {
            $table->tinyInteger('is_clone')->default(false);
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->tinyInteger('is_clone')->default(false);
        });

        Schema::table('errors', function (Blueprint $table) {
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::table('files', function (Blueprint $table) {
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::table('setting_months', function (Blueprint $table) {
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::table('type_check', function (Blueprint $table) {
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facilitity_user', function (Blueprint $table) {
            $table->dropColumn('is_clone');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('is_clone');
        });

        Schema::table('errors', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('setting_months', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('type_check', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
