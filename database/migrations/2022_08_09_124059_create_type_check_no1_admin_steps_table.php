<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeCheckNo1AdminStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_check_no1_admin_steps', function (Blueprint $table) {
            $table->id();
            $table->integer('type_check_no_1_admin_id')->nullable();
            $table->integer('step')->nullable();
            $table->date('deadline')->nullable();
            $table->text('url_file_early_month',  2048)->nullable();
            $table->text('url_file_mid_month',  2048)->nullable();
            $table->integer('facility_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_check_no1_admin_steps');
    }
}
