<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('code', 45);
            $table->string('address', 255)->nullable();
            $table->string('birthday');
            $table->string('phone', 45)->nullable();
            $table->integer('gender')->nullable();
            $table->text('avatar')->nullable();
            $table->string('join_date');
            $table->string('retire_date')->nullable();
            $table->integer('role');
            $table->integer('is_admin');
            $table->string('password')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('address');
            $table->dropColumn('birthday');
            $table->dropColumn('phone');
            $table->dropColumn('gender');
            $table->dropColumn('avatar');
            $table->dropColumn('join_date');
            $table->dropColumn('retire_date');
            $table->dropColumn('role');
            $table->dropColumn('is_admin');
            $table->string('password')->change();
        });
    }
}
