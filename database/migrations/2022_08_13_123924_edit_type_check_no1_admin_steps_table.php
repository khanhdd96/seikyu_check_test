<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditTypeCheckNo1AdminStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_check_no1_admin_steps', function (Blueprint $table) {
            $table->date('deadline_mid_month')->nullable();
            $table->date('deadline_early_month')->nullable();
            $table->integer('step_mid_month')->nullable();
            $table->boolean('done_file_1')->nullable();
            $table->boolean('done_file_2')->nullable();
            $table->boolean('done_file_3')->nullable();
            $table->boolean('done_file_4')->nullable();
            $table->boolean('done_file_5')->nullable();
            $table->boolean('done_file_6')->nullable();
            $table->boolean('done_file_7')->nullable();
            $table->boolean('status')->nullable();
            $table->integer('step_early_month')->nullable();
            $table->text('url_file_early_month_comment',  2048)->nullable();
            $table->text('url_file_early_month_after_comment',  2048)->nullable();
            $table->text('url_file_mid_month_after_comment',  2048)->nullable();
            $table->text('url_file_mid_month_comment',  2048)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setting_months', function (Blueprint $table) {
            $table->dropColumn('deadline_mid_month');
            $table->dropColumn('deadline_early_month');
            $table->dropColumn('step_early_month');
            $table->dropColumn('step_mid_month');
            $table->dropColumn('done_file_1');
            $table->dropColumn('done_file_2');
            $table->dropColumn('done_file_3');
            $table->dropColumn('done_file_4');
            $table->dropColumn('done_file_5');
            $table->dropColumn('done_file_6');
            $table->dropColumn('done_file_7');
            $table->dropColumn('url_file_early_month_comment');
            $table->dropColumn('url_file_early_month_after_comment');
            $table->dropColumn('url_file_mid_month_after_comment');
            $table->dropColumn('url_file_mid_month_comment');
        });
    }
}
