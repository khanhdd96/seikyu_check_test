<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('file_name', 255);  //  file name
            $table->tinyInteger('status'); // status của file (完了, エラー, 保留)
            $table->text('suspended_message')->nullable(); // message gửi mail khi có error của chức năng số 9,
            $table->string('filepath', 255); // file url
            $table->integer('type_check_id'); //  id type check
            $table->tinyInteger('check_send_mail')->default(0); // Tích vào checkbox sendmail khi có error của chức năng số 9
            $table->integer('updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
