<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_groups', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable(); // Tên sơ sở
            $table->string('code', 45);
            $table->integer('number_layer');
            $table->dateTime('deleted_at')->nullable();
            $table->string('address', 255)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('fax', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->integer('updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_groups');
    }
}
