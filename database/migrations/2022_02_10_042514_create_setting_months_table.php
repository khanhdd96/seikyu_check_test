<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_months', function (Blueprint $table) {
            $table->id();
            $table->string('year_month', 45); //tháng năm check
            $table->integer('count_file_done')->nullable();  // số lượng loại check done
            $table->dateTime('deadline')->nullable();  // deadline
            $table->integer('facilities_id'); //  cơ sở id
            $table->integer('updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_months');
    }
}
