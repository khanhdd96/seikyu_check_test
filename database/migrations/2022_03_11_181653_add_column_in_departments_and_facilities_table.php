<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInDepartmentsAndFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->string('address', 255)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('fax', 255)->nullable();
            $table->string('email', 255)->nullable();
        });
        Schema::table('facilities', function (Blueprint $table) {
            $table->string('address', 255)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('fax', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('organization_id');
            $table->dropColumn('code');
            $table->dropColumn('number_layer');
            $table->dropColumn('change_at');
            $table->string('email', 255)->nullable();
            $table->string('list_type_check')->change();
        });
        Schema::table('facilities', function (Blueprint $table) {
            $table->dropColumn('organization_id');
            $table->dropColumn('code');
            $table->dropColumn('number_layer');
            $table->dropColumn('change_at');
            $table->string('list_type_check')->change();
        });
    }
}
