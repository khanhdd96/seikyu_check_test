<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_mail', function (Blueprint $table) {
            $table->id();
            $table->dateTime('datetime');
            $table->integer('mail_setting_id'); //  id setting alert
            $table->longText('user_ids')->nullable(); // list user
            $table->longText('grourp_ids')->nullable(); // list group
            $table->longText('facilitity_ids')->nullable(); // list id cơ sở
            $table->date('start_date')->nullable(); // start date
            $table->time('time')->nullable(); // thời gian gửi(giờ phút)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_mail');
    }
}
