<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTypeCheckNo1AdminStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_check_no1_admin_steps', function (Blueprint $table) {
            $table->string('url_download_file_early_month', 500)->nullable();
            $table->string('url_download_file_mid_month', 500)->nullable();
            $table->string('url_download_file_early_month_after_comment', 500)->nullable();
            $table->string('url_download_file_mid_month_after_comment', 500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_check_no1_admin_steps', function (Blueprint $table) {
            $table->dropColumn('url_download_file_early_month');
            $table->dropColumn('url_download_file_mid_month');
            $table->dropColumn('url_download_file_early_month_after_comment');
            $table->dropColumn('url_download_file_mid_month_after_comment');
        });
    }
}
