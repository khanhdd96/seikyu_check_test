<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_check', function (Blueprint $table) {
            $table->id();
            $table->string('code_check', 45)->nullable(); // id loại check
            $table->integer('setting_months_id'); // id setting month
            $table->integer('facilities_id'); // cơ sở id
            $table->tinyInteger('status'); // Hoàn thành, chưa hoàn thành, bảo lưu
            $table->text('reason')->nullable(); // lý do bảo lưu
            $table->string('year_month_check', 45)->nullable();
            $table->tinyInteger('check_reserve')->default(0); // Tích vào checkbox bảo lưu
            $table->integer('updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_check');
    }
}
