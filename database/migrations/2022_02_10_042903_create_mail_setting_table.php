<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_setting', function (Blueprint $table) {
            $table->id();
            $table->longText('user_ids')->nullable(); // list user
            $table->longText('grourp_ids')->nullable(); // list group
            $table->longText('facilitity_ids')->nullable(); // list id cơ sở
            $table->date('start_date')->nullable(); // start date
            $table->time('time')->nullable(); // thời gian gửi(giờ phút)
            $table->integer('frequency_type')->nullable(); // Tần suất gửi(1: hàng ngày, 2: cách ngày, 3: Cách tuần, 4: Cách tháng)
            $table->integer('interval')->nullable();  //setting khoảng gửi(Chỉ có khi chọn cách ngày, cách tuần, cách tháng),
            $table->integer('send_times')->nullable(); // Số lần gửi
            $table->text('content')->nullable(); //  template mail(string) , nội dung gửi
            $table->tinyInteger('type')->nullable(); // Loại setting: Tự động và custom
            $table->integer('updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_setting');
    }
}
