<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRoleInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('role_admin_updated_at')->nullable();
            $table->dateTime('role_facility_updated_at')->nullable();
            $table->integer('is_admin')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role_admin_updated_at');
            $table->dropColumn('role_facility_updated_at');
            $table->integer('is_admin')->change();
        });
    }
}
