<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/template', function () {
    return view('template.home');
});

Route::get('/random_check', function () {
    return view('welcome');
});

// template B1000
Route::get('/template/admin', function () {
    return view('template.admin');
});

// template B1000.2
Route::get('/detail-branch', function () {
    return view('template.detail-branch');
});

// template B1001
Route::get('/decentralization', function () {
    return view('template.decentralization');
});

// template B1001.1
Route::get('/detail-decentralization', function () {
    return view('template.detail-decentralization');
});

// template B2000
Route::get('/home-B2000', function () {
    return view('template.home-B2000');
});

// template B3000 setting-alert
Route::get('/setting-alert', function () {
    return view('template.setting-alert');
});

// template B3001 list-setting
Route::get('/list-setting-alert', function () {
    return view('template.list-setting-alert');
});

// template B3002 setting-alert-custom
Route::get('/setting-alert-custom', function () {
    return view('template.setting-alert-custom');
});

// template B3003.1 alert-management
Route::get('/alert-management', function () {
    return view('template.alert-management');
});

// template B3004 setting-group-user
Route::get('/setting-group-user', function () {
    return view('template.setting-group-user');
});

// template B4000 check-statistics
Route::get('/check-statistics', function () {
    return view('template.check-statistics');
});

// template A0000
Route::get('/A0000', function () {
    return view('template.login-A0000');
});

// template B0000
Route::get('/B0000', function () {
    return view('template.login-B0000');
});
//404 not found
Route::get('/not-found', function () {
    return view('errors.404');
})->name('404-not-found');
Route::get('/login', [App\Http\Controllers\Auth\AuthController::class, 'index'])->name('login');
Route::get('/logout', [App\Http\Controllers\Auth\AuthController::class, 'logout'])->name('logout');
Route::middleware(['admin:0 1'])->post('/get-facilyties', [App\Http\Controllers\FacilitiesController::class, 'getList'])->name('get-list-facilyties');
Route::middleware(['auth'])->group(function () {
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/random_check', [App\Http\Controllers\RandomCheckController::class, 'checkFile'])->name('random_check');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-10', [App\Http\Controllers\CheckNo10Controller::class, 'checkFile'])->name('check-10');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-7', 'App\Http\Controllers\CheckNo7Controller@checkFile')->name('check-7');

    Route::middleware(['admin:0 1'])->get('/', [App\Http\Controllers\HomepageController::class, 'index'])->name('home.index');
    Route::middleware(['admin:0 1'])->get('/load-detail-check', [App\Http\Controllers\HomepageController::class, 'getDetailCheck'])->name('load-detail-check');
    Route::middleware(['admin:0 1'])->get('/link-download', [App\Http\Controllers\HomepageController::class, 'getLinkDownload'])->name('link-download');

    Route::middleware(['admin:0 1'])->get('/test', [App\Http\Controllers\HomepageController::class, 'index'])->name('home.test');

    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/seikyu-check-3', [App\Http\Controllers\SeikyuCheckRedingFile\SeikyuCheck3Controller::class, 'checkFileNo3'])->name('seikyu-check-3');

    Route::middleware(['admin:0 1'])->post('/check-7-success', [App\Http\Controllers\CheckNo7Controller::class, 'check7Success'])->name('check-7-success');
    Route::middleware(['admin:0 1'])->post('/authorized', [App\Http\Controllers\CheckNo7Controller::class, 'authorized'])->name('authorized');

    Route::middleware(['admin:0 1'])->post('/send-mail-no-9', [App\Http\Controllers\SendMailController::class, 'sendMailTypeCheckNo9'])->name('sendMail.no-9');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-5', [App\Http\Controllers\CheckNo5Controller::class, 'checkFile'])->name('check-5');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-6', [App\Http\Controllers\CheckNo6Controller::class, 'checkFile'])->name('check-6');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-2', [App\Http\Controllers\CheckNo2Controller::class, 'checkFile'])->name('check-2');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-1', [App\Http\Controllers\CheckNo1Controller::class, 'checkFile'])->name('check-1');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-4', [App\Http\Controllers\CheckNo4Controller::class, 'checkFile'])->name('check-4');
    Route::middleware(['admin:0 1'])->middleware(['checkUpdate'])->post('/check-all', [App\Http\Controllers\CheckAllController::class, 'checkFile'])->name('check-all');
    Route::middleware(['admin:0 1'])->get('/file_check/{type_check}', [App\Http\Controllers\FileCheckController::class, 'index'])->name('file_check.detail');
    Route::middleware(['admin:0 1'])->post('/reserve', [App\Http\Controllers\FileCheckController::class, 'reserve'])->name('file_check.reserve');
    Route::middleware(['admin:0 1'])->get('/type_check/{id}', [App\Http\Controllers\TypeCheckController::class, 'getDetail'])->name('type_check.detail');
    Route::middleware(['admin:0 1'])->post('/delete-job', [App\Http\Controllers\CheckAllController::class, 'deleteJob'])->name('delete-job');

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
        // Controllers Within The "App\Http\Controllers\Admin" Namespace
        Route::middleware(['admin:1'])->get('/file_check/{type_check}', [App\Http\Controllers\Admin\FileCheckController::class, 'index'])->name('file_check_admin.detail');
        Route::middleware(['admin:1'])->post('/check-9', [App\Http\Controllers\Admin\CheckNo9Controller::class, 'checkFile'])->name('check-9');
        Route::middleware(['admin:1'])->get('/decentralization', [App\Http\Controllers\Admin\DecentralizateController::class, 'index'])->name('decentralization.index');
        Route::middleware(['admin:1'])->get('/decentralization/get_data_role_admin', [App\Http\Controllers\Admin\DecentralizateController::class, 'getDataRoleAdmin'])->name('decentralization.get_data_role_admin');
        Route::middleware(['admin:1'])->get('/decentralization/get_data_role_facility', [App\Http\Controllers\Admin\DecentralizateController::class, 'getDataRoleFacility'])->name('decentralization.get_data_role_facility');
        Route::middleware(['admin:1'])->put('/update-role', [App\Http\Controllers\Admin\DecentralizateController::class, 'updateRole'])->name('detail-decentralization.update-role');
        Route::middleware(['admin:1'])->delete('/delete-role', [App\Http\Controllers\Admin\DecentralizateController::class, 'deleteRole'])->name('detail-decentralization.delete-role');
        Route::middleware(['admin:1'])->get('/detail-decentralization/{id}', [App\Http\Controllers\Admin\DecentralizateDetailController::class, 'index'])->name('detail-decentralization.index');
        Route::middleware(['admin:1'])->put('/detail-decentralization', [App\Http\Controllers\Admin\DecentralizateDetailController::class, 'update'])->name('detail-decentralization.update');
        Route::middleware(['admin:1'])->get('/', [App\Http\Controllers\Admin\HomepageController::class, 'index'])->name('admin.home.index');
        Route::middleware(['admin:1'])->get('/detail-decentralization-facility/{id}', [App\Http\Controllers\Admin\DecentralizateFacilityDetailController::class, 'index'])->name('detail-decentralization-facility.index');
        Route::middleware(['admin:1'])->post('/detail-decentralization-facility/{id}', [App\Http\Controllers\Admin\DecentralizateFacilityDetailController::class, 'store'])->name('detail-decentralization-facility.store');
        Route::middleware(['admin:1'])->delete('/detail-decentralization-facility', [App\Http\Controllers\Admin\DecentralizateFacilityDetailController::class, 'delete'])->name('detail-decentralization.delete');
        //    Route::get('/detail-decentralization-user/{id}', [App\Http\Controllers\Admin\DecentralizateUserDetailController::class, 'index'])->name('detail-decentralization-user.index');
        Route::middleware(['admin:1'])->put('/detail-decentralization-facility', [App\Http\Controllers\Admin\DecentralizateFacilityDetailController::class, 'update'])->name('detail-decentralization-facility.update');
        //    Route::put('/detail-decentralization-user', [App\Http\Controllers\Admin\DecentralizateUserDetailController::class, 'update'])->name('detail-decentralization-user.update');

        Route::middleware(['admin:1'])->post('/setting-deadline', [App\Http\Controllers\Admin\HomepageController::class, 'settingDeadline'])->name('setting-deadline');
        Route::middleware(['admin:1'])->get('/setting-alert-custom/create', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'create'])->name('setting-alert-custom.create');
        Route::middleware(['admin:1'])->get('/setting-alert-auto/create', [\App\Http\Controllers\Admin\SettingAlertAutoController::class, 'create'])->name('setting-alert-auto.create');
        Route::middleware(['admin:1'])->post('/setting-alert-custom', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'store'])->name('setting-alert-custom.store');
        Route::middleware(['admin:1'])->get('/setting-alert-auto', [\App\Http\Controllers\Admin\SettingAlertAutoController::class, 'index'])->name('setting-alert-auto.index');
        Route::middleware(['admin:1'])->post('/setting-alert-auto', [\App\Http\Controllers\Admin\SettingAlertAutoController::class, 'store'])->name('setting-alert-auto.store');
        Route::middleware(['admin:1'])->get('/setting-alert-custom/{id}/edit', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'edit'])->name('setting-alert-custom.edit');
        Route::middleware(['admin:1'])->get('/setting-alert-auto/{id}/edit', [\App\Http\Controllers\Admin\SettingAlertAutoController::class, 'edit'])->name('setting-alert-auto.edit');
        Route::middleware(['admin:1'])->put('/setting-alert-custom/{id}', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'update'])->name('setting-alert-custom.update');
        Route::middleware(['admin:1'])->put('/setting-alert-auto/{id}', [\App\Http\Controllers\Admin\SettingAlertAutoController::class, 'update'])->name('setting-alert-auto.update');
        Route::middleware(['admin:1'])->get('/setting-alert-custom', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'index'])->name('setting-alert-custom.index');
        Route::middleware(['admin:1'])->delete('/setting-alert-custom/{id}', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'delete'])->name('setting-alert-custom.delete');
        Route::middleware(['admin:1'])->get('/setting-alert-custom/{id}', [\App\Http\Controllers\Admin\SettingAlertCustomController::class, 'show'])->name('setting-alert-custom.show');

        Route::middleware(['admin:1'])->post('/setup-status-upload-all', [\App\Http\Controllers\Admin\HomepageController::class, 'setupStatusUploadAll'])->name('setup-status-upload-all');
        Route::middleware(['admin:1'])->get('/export-csv', [\App\Http\Controllers\Admin\HomepageController::class, 'exportCsv'])->name('export-csv');
        Route::group(['prefix' => 'facilitity', 'namespace' => 'Facilitity'], function () {
            Route::middleware(['admin:1'])->get('/{id}', [\App\Http\Controllers\Admin\FacilitityController::class, 'show'])->name('facilitity.show');
            Route::middleware(['admin:1'])->post('/edit-deadline', [\App\Http\Controllers\Admin\FacilitityController::class, 'editDeadline'])->name('edit-deadline');
            Route::middleware(['admin:1'])->post('/edit-status', [\App\Http\Controllers\Admin\FacilitityController::class, 'editStatus'])->name('edit-status');
            Route::middleware(['admin:1'])->post('/open-status-upload', [\App\Http\Controllers\Admin\FacilitityController::class, 'openStatusUpload'])->name('open-status-upload');
            Route::middleware(['admin:1'])->post('/edit-status-active', [\App\Http\Controllers\Admin\FacilitityController::class, 'changeActiveStatus'])->name('edit-status-active');
        });
        Route::middleware(['admin:1'])->get('/department', [\App\Http\Controllers\Admin\DepartmentController::class, 'index'])->name('department.index');
        Route::middleware(['admin:1'])->get('/department/{id}/edit', [\App\Http\Controllers\Admin\DepartmentController::class, 'edit'])->name('department.edit');
        Route::middleware(['admin:1'])->post('/department/status', [\App\Http\Controllers\Admin\DepartmentController::class, 'departmentStatus'])->name('department.status');
        Route::middleware(['admin:1'])->get('/department/facility', [\App\Http\Controllers\Admin\DepartmentController::class, 'getListFacility'])->name('department.facility');

        // setting group user
        Route::middleware(['admin:1'])->get('/setting-group-user', [\App\Http\Controllers\Admin\SettingGroupController::class, 'index'])->name('setting-group-user.index');
        Route::middleware(['admin:1'])->delete('/setting-group-user', [\App\Http\Controllers\Admin\SettingGroupController::class, 'delete'])->name('setting-group-user.delete');
        Route::middleware(['admin:1'])->post('/setting-group-user', [\App\Http\Controllers\Admin\SettingGroupController::class, 'store'])->name('setting-group-user.store');
        Route::middleware(['admin:1'])->get('/setting-group-user/show', [\App\Http\Controllers\Admin\SettingGroupController::class, 'show'])->name('setting-group-user.show');
        Route::middleware(['admin:1'])->get('/setting-group-user/search', [\App\Http\Controllers\Admin\SettingGroupController::class, 'search'])->name('setting-group-user.search');

        Route::middleware(['admin:1'])->get('/check-statistics', [\App\Http\Controllers\Admin\CheckStatisticsController::class, 'index'])->name('check-statistics.index');
        Route::middleware(['admin:1'])->post('/list-error-code', [\App\Http\Controllers\Admin\CheckStatisticsController::class, 'getListErrorCode'])->name('get-list-error-code');

        Route::middleware(['admin:1'])->get('/setting-group-user/search-facility', [\App\Http\Controllers\Admin\SettingGroupController::class, 'searchFacility'])->name('setting-group-user.search-facility');
        Route::middleware(['admin:1'])->get('/setting-group-user/search-user', [\App\Http\Controllers\Admin\SettingGroupController::class, 'searchUser'])->name('setting-group-user.search-user');
        Route::middleware(['admin:1'])->get('/setting-group-user/search-all', [\App\Http\Controllers\Admin\SettingGroupController::class, 'searchAllFacilityAndUser'])->name('setting-group-user.search-all');

        //file in insurance
        Route::middleware(['admin:1'])->get('/file-in-insurance', [App\Http\Controllers\Admin\FileInInsuranceController::class, 'index'])->name('file-in-insurance.index');
        Route::middleware(['admin:1'])->post('/file-in-insurance/store', [App\Http\Controllers\Admin\FileInInsuranceController::class, 'store'])->name('file-in-insurance.store');
        Route::middleware(['admin:1'])->get('/file-in-insurance/show', [App\Http\Controllers\Admin\FileInInsuranceController::class, 'show'])->name('file-in-insurance.show');
        Route::middleware(['admin:1'])->put('/file-in-insurance/update', [App\Http\Controllers\Admin\FileInInsuranceController::class, 'update'])->name('file-in-insurance.update');
        Route::middleware(['admin:1'])->delete('/file-in-insurance/destroy', [App\Http\Controllers\Admin\FileInInsuranceController::class, 'destroy'])->name('file-in-insurance.destroy');

        //file outside insurance
        Route::middleware(['admin:1'])->get('/file-outside-insurance', [App\Http\Controllers\Admin\FileOutsideInsuranceController::class, 'index'])->name('file-outside-insurance.index');
        Route::middleware(['admin:1'])->post('/file-outside-insurance/store', [App\Http\Controllers\Admin\FileOutsideInsuranceController::class, 'store'])->name('file-outside-insurance.store');
        Route::middleware(['admin:1'])->get('/file-outside-insurance/show', [App\Http\Controllers\Admin\FileOutsideInsuranceController::class, 'show'])->name('file-outside-insurance.show');
        Route::middleware(['admin:1'])->put('/file-outside-insurance/update', [App\Http\Controllers\Admin\FileOutsideInsuranceController::class, 'update'])->name('file-outside-insurance.update');
        Route::middleware(['admin:1'])->delete('/file-outside-insurance/destroy', [App\Http\Controllers\Admin\FileOutsideInsuranceController::class, 'destroy'])->name('file-outside-insurance.destroy');

        //day off
        Route::middleware(['admin:1'])->get('/day-off', [App\Http\Controllers\Admin\DayOffController::class, 'index'])->name('day-off.index');
        Route::middleware(['admin:1'])->post('/day-off/store', [App\Http\Controllers\Admin\DayOffController::class, 'store'])->name('day-off.store');
        Route::middleware(['admin:1'])->get('/day-off/show', [App\Http\Controllers\Admin\DayOffController::class, 'show'])->name('day-off.show');
        Route::middleware(['admin:1'])->put('/day-off/update', [App\Http\Controllers\Admin\DayOffController::class, 'update'])->name('day-off.update');
        Route::middleware(['admin:1'])->delete('/day-off/destroy', [App\Http\Controllers\Admin\DayOffController::class, 'destroy'])->name('day-off.destroy');

        //master-code
        Route::middleware(['admin:1'])->get('/master-code', [App\Http\Controllers\Admin\MasterCodeController::class, 'index'])->name('master-code.index');
        Route::middleware(['admin:1'])->post('/master-code/store', [App\Http\Controllers\Admin\MasterCodeController::class, 'store'])->name('master-code.store');
        Route::middleware(['admin:1'])->get('/master-code/show', [App\Http\Controllers\Admin\MasterCodeController::class, 'show'])->name('master-code.show');
        Route::middleware(['admin:1'])->put('/master-code/update', [App\Http\Controllers\Admin\MasterCodeController::class, 'update'])->name('master-code.update');
        Route::middleware(['admin:1'])->delete('/master-code/destroy', [App\Http\Controllers\Admin\MasterCodeController::class, 'destroy'])->name('master-code.destroy');

        //Template mail
        Route::middleware(['admin:1'])->get('/template-mail', [\App\Http\Controllers\Admin\TemplateMailController::class, 'index'])->name('template-mail.index');
        Route::middleware(['admin:1'])->get('/template-mail/get-list', [\App\Http\Controllers\Admin\TemplateMailController::class, 'getList'])->name('template-mail.get-list');
        Route::middleware(['admin:1'])->post('/template-mail', [\App\Http\Controllers\Admin\TemplateMailController::class, 'store'])->name('template-mail.store');
        Route::middleware(['admin:1'])->put('/template-mail/update', [\App\Http\Controllers\Admin\TemplateMailController::class, 'update'])->name('template-mail.edit');
        Route::middleware(['admin:1'])->get('/template-mail/detail', [\App\Http\Controllers\Admin\TemplateMailController::class, 'detail'])->name('template-mail.detail');
        Route::middleware(['admin:1'])->delete('/template-mail', [\App\Http\Controllers\Admin\TemplateMailController::class, 'delete'])->name('template-mail.delete');

        //download file
        Route::middleware(['admin:1'])->get('/download', [\App\Http\Controllers\Admin\DownloadController::class, 'index'])->name('download-file.index');
        Route::middleware(['admin:1'])->post('/download', [\App\Http\Controllers\Admin\DownloadController::class, 'downloadAll'])->name('download-file.all');
        Route::middleware(['admin:1'])->post('/download-ajax', [\App\Http\Controllers\Admin\DownloadController::class, 'downloadAllAjax'])->name('download-file.all-ajax');

        //template file
        Route::middleware(['admin:1'])->get('/type-check-no-1-admin', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'index'])->name('template');
        Route::middleware(['admin:1'])->post('/check-1-admin', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'upFileEarlyMonth'])->name('check-1-admin');
        Route::middleware(['admin:1'])->post('/check-1-admin/update-deadline', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'updateDeadline'])->name('check-1-admin.update-deadline');
        Route::middleware(['admin:1'])->get('/check-1-admin/get-facility', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFacility'])->name('check-1-admin.get-facility');
        Route::middleware(['admin:1'])->get('/check-1-admin/get-facility-to-read-comment', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFacilityToReadComment'])->name('check-1-admin.get-facility-to-read-comment');
        Route::middleware(['admin:1'])->get('/check-1-admin/get-facility-distribute-file-after-comment', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFacilityFistributeFileAfterComment'])->name('check-1-admin.get-facility-distribute-file-after-comment');
        Route::middleware(['admin:1'])->post('/check-1-admin/distribution-file', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'distributionFile'])->name('check-1-admin.distribution-file');
        Route::middleware(['admin:1'])->post('/check-1-admin/distribution-file-after-comment', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'distributionFileComment'])->name('check-1-admin.distribution-file-after-comment');
        Route::middleware(['admin:1'])->post('/check-1-admin/read-file-comment', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'readFileComment'])->name('check-1-admin.read-file-comment');
        Route::middleware(['admin:1'])->post('/check-1-admin/distribution-file/all', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'distributionAllFile'])->name('check-1-admin.distribution-all-file');
        Route::middleware(['admin:1'])->post('/check-1-admin/distribution-all-file-comment', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'distributionAllFileComment'])->name('check-1-admin.distribution-all-file-comment');
        Route::middleware(['admin:1'])->post('/check-1-admin/read-all-file-comment/all', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'readAllFileComment'])->name('check-1-admin.read-all-file-comment');
        Route::middleware(['admin:1'])->post('/check-1/upload-file-comment', [\App\Http\Controllers\CheckNo1Controller::class, 'uploadFileComment'])->name('check-1.upload-file-comment');
        Route::middleware(['admin:1'])->post('/check-1/save-template-mail', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'saveTemplateMail'])->name('check-1.add-template-mail');
        Route::middleware(['admin:1'])->put('/check-1/update-deadline', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'updateFacilityDeadline'])->name('check-1.update-facility-deadline');

        Route::middleware(['admin:1'])->post('/check-1-admin/check-1-admin.upload-facilities', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'uploadFacilities'])->name('check-1-admin.upload-facilities');
        Route::middleware(['admin:1'])->get('/check-1-admin/export-csv', [\App\Http\Controllers\Admin\CheckNo1AdminController::class, 'exportCSV'])->name('check-1-admin.export-csv');
    });
});

Route::get('/download/early/{hash}', [App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFileEarly']);
Route::get('/download/early_comment/{hash}', [App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFileEarlyComment']);
Route::get('/download/mid/{hash}', [App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFileMid']);
Route::get('/download/mid_comment/{hash}', [App\Http\Controllers\Admin\CheckNo1AdminController::class, 'getFileMidComment']);
