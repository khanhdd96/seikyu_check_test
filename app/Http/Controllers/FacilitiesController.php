<?php

namespace App\Http\Controllers;

use App\Consts;
use App\Http\Services\Admin\DepartmentService;
use App\Http\Services\FacilityService;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacilitiesController extends Controller
{
    public function __construct(FacilityService $service, DepartmentService $departmentService)
    {
        $this->service = $service;
        $this->departmentService = $departmentService;
    }

    public function getList(Request $request)
    {
        $datas = $this->service->getFacilityByDepartmentId($request->department_id);
        $user = Auth::user();
        if ($user->is_admin != Consts::ADMIN) {
            $facilityIds = Role::whereUsersId($user->id)->pluck('facilities_id');
            $datas = $datas->whereIn('id', $facilityIds);
        }
        $datas = $datas->get()
            ->toArray();
        return response()->json(['data' => $datas]);
    }
}
