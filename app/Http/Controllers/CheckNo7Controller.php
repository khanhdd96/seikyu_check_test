<?php

namespace App\Http\Controllers;

use App\Http\Services\CheckNo7Service;
use App\Rules\CheckNo7FileName;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Messages;

class CheckNo7Controller extends Controller
{
    private $service;
    public function __construct(CheckNo7Service $service)
    {
        $this->service = $service;
    }
    public function checkFile(Request $request)
    {
        $validator = Validator::make(
            [
                'file' => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
            ],
            [
                'file' => ['required' , 'max:112640', new CheckNo7FileName()],
                'extension' => 'required|in:xlsx,xls',
            ],
            [
                'extension.required' => Messages::FILE_FOMAT_WRONG,
                'extension.in' => Messages::FILE_FOMAT_WRONG,
                'file.required' => Messages::FILE_REQUIRED,
                'file.max' => Messages::OVERSIZE,
                'file.uploaded' => Messages::OVERSIZE
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->checkFile($request);
    }

    public function check7Success(Request $request)
    {
        return $this->service->check7Success($request);
    }
}
