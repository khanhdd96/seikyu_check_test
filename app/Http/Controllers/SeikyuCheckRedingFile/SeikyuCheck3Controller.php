<?php

namespace App\Http\Controllers\SeikyuCheckRedingFile;

use App\Http\Services\SeikyuCheckNo3Service;
use App\Rules\CheckNo3FileCSV;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Messages;

class SeikyuCheck3Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    protected $seikyuCheckServes;

    public function __construct(SeikyuCheckNo3Service $seikyuCheckServes)
    {
        $this->seikyuCheckServes = $seikyuCheckServes;
    }

    public function checkFileNo3(Request $request)
    {
        $month = $request->month ?? date('Y/m');
        $validator = \Validator::make(
            [
                'file' => $request->file,
                'extension0' => strtolower($request->file[0]->getClientOriginalExtension()),
                'extension1' => strtolower($request->file[1]->getClientOriginalExtension()),
                'name0' => $request->file[0]->getClientOriginalName(),
                'name1' => $request->file[1]->getClientOriginalName(),
            ],
            [
                'file' => 'required|array|min:2|max:2',
                // 'file.*' => 'required|mimes:csv,xls,xlsx',
                'extension0' => 'required|in:csv,xlsx,xls',
                'extension1' => 'required|in:csv,xlsx,xls',
                'name0' => [new CheckNo3FileCSV($month)],
                'name1' => 'regex:/(.*)(売上一覧表_保険外)(.*)/'
            ],
            [
                'file.required' => Messages::FILE_FOMAT_WRONG,
                'file.min' => Messages::UPLOAD_FILE_ERROR,
                'name0.regex' => Messages::CSV_ERROR,
                'name1.regex' => Messages::EXCEL_ERROR
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                'validator' => true,
            ], 400);
        }

        return $this->seikyuCheckServes->seikyuCheck($request);
    }
}
