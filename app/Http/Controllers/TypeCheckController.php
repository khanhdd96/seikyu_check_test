<?php

namespace App\Http\Controllers;

use App\Http\Services\TypeCheckService;
use Illuminate\Http\Request;

class TypeCheckController extends Controller
{
    private $service;
    public function __construct(TypeCheckService $service)
    {
        $this->service = $service;
    }
    public function getDetail($id)
    {
        $result = $this->service->getDetail($id);
        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }
}
