<?php

namespace App\Http\Controllers;

use App\Http\Services\CheckNo6Service;
use App\Messages;
use App\Rules\CheckNo6FileName;
use Illuminate\Http\Request;

class CheckNo6Controller extends Controller
{
    public function __construct(CheckNo6Service $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $month = $request->month ?? date('Y/m');
        $validator = \Validator::make(
            [
                'file' => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
                'name' => $request->file->getClientOriginalName(),
            ],
            [
                'file' => 'required|max:112640',
                'extension' => 'required|in:xlsx,xls,csv',
                'name' => [new CheckNo6FileName($month)]
            ],
            [
                'extension.required' => Messages::FILE_FOMAT_WRONG,
                'extension.in' => Messages::FILE_FOMAT_WRONG,
                'file.required' => Messages::FILE_REQUIRED,
                'file.max' => Messages::OVERSIZE,
                'file.uploaded' => Messages::OVERSIZE,
                'name.regex' => Messages::NAME_ERROR
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
