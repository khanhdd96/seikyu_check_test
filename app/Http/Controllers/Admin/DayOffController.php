<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\CheckUniqueDayOff;
use Illuminate\Http\Request;
use App\Http\Services\Admin\DayOffService;
use App\Models\DayOff;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use App\Messages;

class DayOffController extends Controller
{
    public function __construct(DayOffService $dayOffService)
    {
        $this->dayOffService = $dayOffService;
    }

    public function index(Request $request)
    {
        $results = $this->dayOffService->index($request['search']);
        return view('page.admin.day-off.index', compact('results'));
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'dayOff' => ['required', new CheckUniqueDayOff()],
            ],
            [
                'dayOff.required' => Messages::DAY_OFF_REQUIRED
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        return response()->json([], $this->dayOffService->store($request));
    }

    public function show(Request $request)
    {
        $result = $this->dayOffService->show($request['id']);
        return response()->json([
            "data" => $result
        ], 200);
    }

    public function update(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'dayOff' => [
                    'required', Rule::unique('day_offs', 'date_off')
                        ->where(function ($query) use ($request) {
                            $query->where('date_off', $request['dayOff'])
                            ->where('id', '<>', $request['id']);
                        }),
                ]
            ],
            [
                'dayOff.required' => Messages::DAY_OFF_REQUIRED,
                'dayOff.unique' => date('Y/m/d', strtotime($request['dayOff'])) .
                    'はすでに' . ($request['dayOffType'] == 1 ? '特定'  : '日祝') . 'として存在しています。'
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }
        return response()->json([], $this->dayOffService->update($request));
    }

    public function destroy(Request $request)
    {
        return response()->json([], $this->dayOffService->destroy($request['id']));
    }
}
