<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\DownloadService;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    private $service;
    public function __construct(DownloadService $downloadService)
    {
        $this->service = $downloadService;
    }

    public function index(Request $request)
    {
        $data = $this->service->index($request);
        return view('page.admin.download.index', compact('data'));
    }

    public function downloadAll(Request $request)
    {
        return $this->service->downloadAll($request);
    }

    public function downloadAllAjax(Request $request)
    {
        return response()->json([
            'data' => $this->service->downloadAllAjax($request)
        ]);
    }
}
