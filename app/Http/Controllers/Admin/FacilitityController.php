<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\FacilitityService;
use Illuminate\Http\Request;

class FacilitityController extends Controller
{
    protected $service;

    public function __construct(FacilitityService $service)
    {
        $this->service = $service;
    }

    public function show(Request $request, $id)
    {
        $requestDatas = $request->all();
        $data = $this->service->show($request, $id);
        return view('homes.admin.facilitity-detail', compact('data', 'requestDatas'));
    }

    public function editDeadline(Request $request)
    {
        return $this->service->editDeadline($request);
    }

    public function editStatus(Request $request)
    {
        return $this->service->editStatus($request);
    }

    public function openStatusUpload(Request $request)
    {
        return $this->service->openStatusUpload($request);
    }

    public function changeActiveStatus(Request $request)
    {
        return $this->service->changeActiveStatus($request);
    }
}
