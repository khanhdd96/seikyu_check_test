<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\DayOffService;
use App\Http\Services\Admin\TemplateMailService;
use App\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function view;

class TemplateMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(TemplateMailService $service)
    {
        $this->service = $service;
    }

    public function getList(Request $request)
    {
        return response()->json([
            'data' => $this->service->getList($request)
        ], 200);
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.template-mail.index', compact('results'));
    }

    public function detail(Request $request)
    {
        $data = $this->service->detail($request);
        return response()->json([
            'data' => $data
        ], 200);
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:100|min:2|unique:template_mails,title',
                'content' => 'required|max:1500|min:20',
            ],
            [
                'title.required' => '件名を入力してください。',
                'title.unique' => '件名が既存しています。他の件名を入力してください。',
                'title.max' => '件名 は100文字以内入力してください。',
                'content.required' => 'メールの内容を入力してください。',
                'content.max' => '20より大きい1500未満を入力してください。',
                'content.min' => '20より大きい1500未満を入力してください。',
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }
        return response()->json([], $this->service->store($request));
    }

    public function update(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:100|min:2|unique:template_mails,title,'. $request['id'],
                'content' => 'required|max:1500|min:20',
            ],
            [
                'title.required' => '件名を入力してください。',
                'title.unique' => '件名が既存しています。他の件名を入力してください。',
                'title.max' => '件名 は100文字以内入力してください。',
                'content.required' => 'メールの内容を入力してください。',
                'content.max' => '20より大きい1500未満を入力してください。',
                'content.min' => '20より大きい1500未満を入力してください。',
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }
        return response()->json([], $this->service->update($request));
    }

    public function delete(Request $request)
    {
        return response()->json([], $this->service->delete($request));
    }
}
