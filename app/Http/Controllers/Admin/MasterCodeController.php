<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\CheckUniqueMasterCode;
use App\Rules\CheckUniqueMasterSubCode;
use Illuminate\Http\Request;
use App\Http\Services\Admin\MasterCodeService;
use Illuminate\Support\Facades\Validator;
use App\Messages;
use Illuminate\Validation\Rule;

class MasterCodeController extends Controller
{
    private $masterCodeService;
    public function __construct(MasterCodeService $masterCodeService)
    {
        $this->masterCodeService = $masterCodeService;
    }

    public function index(Request $request)
    {
        $results = $this->masterCodeService->index($request);
        return view('page.admin.master-code.index', compact('results'));
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'createCodeName' => [
                    'required',
                    'max:100',
                    new CheckUniqueMasterCode('master_code'),
                    Rule::unique('master_code', 'code_name')
                    ->where(function ($query) use ($request) {
                        $query->whereType($request['type']);
                    })
                ],
                'createSubCodeName.*' => ['required', 'max:100',
                    new CheckUniqueMasterCode('sub_code_name'),
                    new CheckUniqueMasterSubCode($request['createSubCodeName'])]
            ],
            [
                'createCodeName.required' => Messages::CODE_NAME_REQUIRED,
                'createCodeName.unique' => Messages::MASTERCODE_UNIQUE,
                'createCodeName.max' => Messages::CODE_NAME_MAX,
                'createSubCodeName.*.required' => Messages::SUB_CODE_NAME_REQUIRED,
                'createSubCodeName.*.max' => Messages::SUB_CODE_NAME_MAX
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->masterCodeService->store($request);

        return response()->json([], $results['code']);
    }

    public function show(Request $request)
    {
        $result = $this->masterCodeService->show($request['id']);
        if (!$result) {
            return response()->json([], 404);
        }

        return response()->json([
            "data" => $result
        ], 200);
    }

    public function update(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'editCodeName' => [
                    'required',
                    'max:100',
                    new CheckUniqueMasterCode('master_code', $request['id']),
                    Rule::unique('master_code', 'code_name')
                        ->where(function ($query) use ($request) {
                            $query->whereType($request['type'])->where('id', '<>', $request['id']);
                        })
                ],
                'editSubCodeName.*' => ['required', 'max:100',
                    new CheckUniqueMasterCode(
                        'sub_code_name',
                        $request['subCodeEditId'],
                        $request['editSubCodeName']
                    ),
                    new CheckUniqueMasterSubCode($request['editSubCodeName'])]
            ],
            [
                'editCodeName.required' => Messages::CODE_NAME_REQUIRED,
                'editCodeName.unique' => Messages::MASTERCODE_UNIQUE,
                'editCodeName.max' => Messages::CODE_NAME_MAX,
                'editSubCodeName.*.required' => Messages::SUB_CODE_NAME_REQUIRED,
                'editSubCodeName.*.max' => Messages::SUB_CODE_NAME_MAX
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->masterCodeService->update($request);

        return response()->json([], $results['code']);
    }

    public function destroy(Request $request)
    {
        $result = $this->masterCodeService->destroy($request['id']);
        if ($result['code'] == 400) {
            return response()->json([
                'status' => false,
                'message' => Messages::DELETE_MASTER_CODE_FAIL
            ], 400);
        }

        return response()->json([], 200);
    }
}
