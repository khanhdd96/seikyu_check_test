<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlertRequestAuto;
use App\Http\Services\Admin\SettingAlertAutoService;
use App\Messages;
use Illuminate\Http\Request;
use Redirect;

class SettingAlertAutoController extends Controller
{
    public function __construct(SettingAlertAutoService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.setting-alert-auto.index', compact('results'));
    }

    public function create()
    {
        $data = $this->service->create();
        if ($data['create'] == false) {
            return redirect()->route('setting-alert-auto.edit', ['id' => $data['id']]);
        }
        return view('page.admin.setting-alert-auto.setting-alert', compact('data'));
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        return view('page.admin.setting-alert-auto.setting-alert-edit', compact('data'));
    }

    public function store(AlertRequestAuto $request)
    {
        return $this->service->store($request);
    }

    public function update($id, AlertRequestAuto $request)
    {
        return $this->service->update($id, $request);
    }
    public function delete($id)
    {
        return $this->service->delete($id);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }
}
