<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\HomepageServices;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    protected $service;

    public function __construct(HomepageServices $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $requestDatas = $request->all();
        $data = $this->service->index($request);
        return view('homes.admin.home', compact('data', 'requestDatas'));
    }

    public function settingDeadline(Request $request)
    {
        return $this->service->settingDeatline($request);
    }

    public function setupStatusUploadAll(Request $request)
    {
        return $this->service->setupStatusUploadAll($request);
    }

    public function exportCsv(Request $request)
    {
        return $this->service->exportCsv($request);
    }
}
