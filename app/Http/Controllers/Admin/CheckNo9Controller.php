<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\CheckNo9Service;
use App\Messages;
use Illuminate\Http\Request;

class CheckNo9Controller extends Controller
{
    public function __construct(CheckNo9Service $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $validator = \Validator::make(
            [
                'file' => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
                'fileName' => $request->file->getClientOriginalName()
            ],
            [
                'file' => 'required|max:112640',
                'extension' => 'required|in:xlsx,xls,csv',
                'fileName' => 'regex:/(.*)(予実利用者スケジュール)(.*)/'
            ],
            [
                'extension.required' => Messages::FILE_FOMAT_WRONG,
                'extension.in' => Messages::FILE_FOMAT_WRONG,
                'file.required' => Messages::FILE_REQUIRED,
                'file.max' => Messages::OVERSIZE,
                'file.uploaded' => Messages::OVERSIZE,
                'fileName.regex' => Messages::NAME_ERROR
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
