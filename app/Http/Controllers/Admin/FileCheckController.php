<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\FileCheckService;
use Illuminate\Http\Request;
use App\Http\Services\MDataInputService;

class FileCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(FileCheckService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        $files = $results['files'];
        $typeCheck = $results['typeCheck'];
        $data = [
            'settingMonth' => $results['settingMonth'],
            'month' => $results['month'],
            'facilityId' => $results['facilityId'],
            'fileCount' => $results['fileCount'],
            'templateMail' => $results['templateMail'],
            'createdAt' => $results['createdAt'],
            'updatedAt' => $results['updatedAt'],
//            'canUpload' => $results['canUpload']
        ];
        if ($request->ajax()) {
            return view('page.admin.detailCheck-No9.item-paginate', compact('files', 'typeCheck'));
        }
        return view('page.admin.detailCheck-No9.detail-check', compact('files', 'typeCheck', 'data'));
    }
}
