<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\CheckStatisticsServices;
use Illuminate\Http\Request;

class CheckStatisticsController extends Controller
{
    protected $service;

    public function __construct(CheckStatisticsServices $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $requestDatas = $request->all();
        $data = $this->service->index($request);

        return view('page.admin.check-statistics.overview', compact('data', 'requestDatas'));
    }

    public function getListErrorCode(Request $request)
    {
        return $this->service->getListErrorCode($request);
    }
}
