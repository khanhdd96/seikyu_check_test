<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\Admin\DepartmentService;
use Illuminate\Http\Request;

class DepartmentController
{
    public function __construct(DepartmentService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.department.list-department', compact('results'));
    }

    public function edit($id, Request $request)
    {
        $results = $this->service->edit($id, $request);
        return view('page.admin.department.detail-department', compact('results'));
    }

    public function departmentStatus(Request $request)
    {
        return $this->service->departmentStatus($request);
    }

    public function getListFacility(Request $request)
    {
        return $this->service->getListFacility($request);
    }
}
