<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Admin\DecentralizateUserDetailService;
use App\Messages;
use Illuminate\Http\Request;
use Redirect;

class DecentralizateUserDetailController extends Controller
{
    public function __construct(DecentralizateUserDetailService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.decentralization.detail-decentralization-user', compact('results'));
    }
    public function update(Request $request)
    {
        $results = $this->service->update($request);
        if (!$results) {
            return back();
        } else {
            return Redirect::back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }
}
