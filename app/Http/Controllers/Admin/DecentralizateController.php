<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Services\Admin\DecentralizateService;
use Illuminate\Http\Request;

class DecentralizateController extends Controller
{
    public function __construct(DecentralizateService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.decentralization.decentralization', compact('results'));
    }
    public function updateRole(Request $request)
    {
        return $this->service->updateRole($request);
    }
    public function deleteRole(Request $request)
    {
        return $this->service->deleteRole($request);
    }
    public function getDataRoleAdmin()
    {
        return $this->service->getDataRoleAdmin();
    }
    public function getDataRoleFacility()
    {
        return $this->service->getDataRoleFacility();
    }
}
