<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingGroupRequest;
use App\Http\Services\Admin\SettingGroupService;
use Redirect;
use Illuminate\Http\Request;

class SettingGroupController extends Controller
{
    public function __construct(SettingGroupService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        $data = $this->service->index($request);
        return view('page.admin.setting-group.list-group', compact('data'));
    }

    public function store(SettingGroupRequest $request)
    {
        return $this->service->store($request);
    }

    public function delete(Request $request)
    {
        return $this->service->delete($request);
    }
    public function show(Request $request)
    {
        return $this->service->show($request->id);
    }
    public function searchFacility(Request $request)
    {
        return $this->service->searchFacility($request);
    }
    public function searchUser(Request $request)
    {
        return $this->service->searchUser($request);
    }

    public function searchAllFacilityAndUser()
    {
        return $this->service->searchAllFacilityAndUser();
    }
}
