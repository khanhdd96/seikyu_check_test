<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlertRequest;
use App\Http\Services\Admin\SettingAlertCustomService;
use App\Messages;
use Illuminate\Http\Request;
use Redirect;

class SettingAlertCustomController extends Controller
{
    public function __construct(SettingAlertCustomService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.setting-alert-custom.list-setting-alert', compact('results'));
    }

    public function create()
    {
        $data = $this->service->create();
        return view('page.admin.setting-alert-custom.setting-alert-custom', compact('data'));
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        return view('page.admin.setting-alert-custom.setting-alert-custom-edit', compact('data'));
    }

    public function store(AlertRequest $request)
    {
        return $this->service->store($request);
    }

    public function update($id, AlertRequest $request)
    {
        return $this->service->update($id, $request);
    }
    public function delete($id)
    {
        return $this->service->delete($id);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }
}
