<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\CheckUniqueInsurance;
use Illuminate\Http\Request;
use App\Http\Services\Admin\FileOutsideInsuranceService;
use Illuminate\Support\Facades\Validator;
use App\Messages;
use Illuminate\Validation\Rule;

class FileOutsideInsuranceController extends Controller
{
    public function __construct(FileOutsideInsuranceService $fileOutsideInsuranceService)
    {
        $this->fileOutsideInsuranceService = $fileOutsideInsuranceService;
    }

    public function index(Request $request)
    {
        $results = $this->fileOutsideInsuranceService->index($request['search']);
        return view('page.admin.file-outside-insurance.index', compact('results'));
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'createCategoryName' => [
                    'required',
                    'max:100',
                    new CheckUniqueInsurance('out-insurance'),
                    Rule::unique('file_outside_insurances', 'category_name')
                        ->where(function ($query) use ($request) {
                            $query->whereFiduciaryGoal($request['createFiduciaryGoal']);
                        })
                ],
            ],
            [
                'createCategoryName.required' => Messages::CATEGORY_NAME_REQUIRED,
                'createCategoryName.max' => Messages::CATEGORY_NAME_MAX_100,
                'createCategoryName.unique' => '区分名称が既存しています。他の区分名称を入力してください。'
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->fileOutsideInsuranceService->store($request);

        return response()->json([], $results['code']);
    }

    public function show(Request $request)
    {
        return response()->json([
            'data' => $this->fileOutsideInsuranceService->show($request['id'])
        ], 200);
    }

    public function update(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'editCategoryName' => [
                    'required',
                    'max:100',
                    new CheckUniqueInsurance('out-insurance', $request['id']),
                    Rule::unique('file_outside_insurances', 'category_name')
                        ->where(function ($query) use ($request) {
                            $query->whereFiduciaryGoal($request['editFiduciaryGoal'])
                            ->where('id', '<>', $request['id']);
                        })
                ],
            ],
            [
                'editCategoryName.required' => Messages::CATEGORY_NAME_REQUIRED,
                'editCategoryName.max' => Messages::CATEGORY_NAME_MAX_100,
                'editCategoryName.unique' => Messages::OUT_INSURANCE_UNIQUE
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->fileOutsideInsuranceService->update($request);

        return response()->json([], $results['code']);
    }

    public function destroy(Request $request)
    {
        $result = $this->fileOutsideInsuranceService->destroy($request['id']);
        return response()->json([], 200);
    }
}
