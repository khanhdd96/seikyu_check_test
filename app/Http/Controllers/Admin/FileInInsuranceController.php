<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\CheckUniqueInsurance;
use Illuminate\Http\Request;
use App\Models\FileInInsurance;
use App\Http\Services\Admin\FileInInsuranceService;
use Illuminate\Support\Facades\Validator;
use App\Messages;
use Illuminate\Validation\Rule;

class FileInInsuranceController extends Controller
{
    public function __construct(FileInInsurance $fileInInsurance, FileInInsuranceService $fileInInsuranceService)
    {
        $this->fileInInsurance =  $fileInInsurance;
        $this->fileInInsuranceService =  $fileInInsuranceService;
    }

    public function index(Request $request)
    {
        $results = $this->fileInInsuranceService->index($request['search']);
        return view('page.admin.file-in-insurance.index', compact('results'));
    }

    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'createCompanyName' => [
                    'required',
                    'max:100',
                    Rule::unique('file_in_insurances', 'company_name')
                    ->where(function ($query) use ($request) {
                        $query->whereFiduciaryGoal($request['createFiduciaryGoal']);
                    }),
                    new CheckUniqueInsurance('in-insurance')
                ],
            ],
            [
                'createCompanyName.required' => Messages::COMPANY_NAME_REQUIRED,
                'createCompanyName.max' => Messages::COMPANY_NAME_MAX_100,
                'createCompanyName.unique' => Messages::IN_INSURANCE_UNIQUE,
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->fileInInsuranceService->store($request);

        return response()->json([], $results['code']);
    }

    public function show(Request $request)
    {
        return response()->json([
            'data' => $this->fileInInsuranceService->show($request['id'])
        ], 200);
    }

    public function update(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(),
            [
                'editCompanyName' => [
                    'required',
                    'max:100',
                    new CheckUniqueInsurance('in-insurance', $request['id']),
                    Rule::unique('file_in_insurances', 'company_name')
                        ->where(function ($query) use ($request) {
                            $query->whereFiduciaryGoal($request['editFiduciaryGoal'])
                            ->where('id', '<>', $request['id']);
                        })
                ],
            ],
            [
                'editCompanyName.required' => Messages::COMPANY_NAME_REQUIRED,
                'editCompanyName.max' => Messages::COMPANY_NAME_MAX_100,
                'editCompanyName.unique' => Messages::IN_INSURANCE_UNIQUE
            ]
        );
        if ($validatedData->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validatedData->errors()->messages(),
            ], 400);
        }

        $results = $this->fileInInsuranceService->update($request);

        return response()->json([], $results['code']);
    }

    public function destroy(Request $request)
    {
        $result = $this->fileInInsuranceService->destroy($request['id']);
        return response()->json([], 200);
    }
}
