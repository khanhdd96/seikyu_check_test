<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\Admin\CheckNo1AdminService;
use App\Jobs\ProcessFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckNo1AdminController extends Controller
{
    private $service;

    public function __construct(CheckNo1AdminService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $result = $this->service->index($request);
        return view('page.admin.template-file.index', compact('result'));
    }

    public function upFileEarlyMonth(Request $request)
    {
        $fileUploads = $request->file;
        return $this->service->upFileEarlyMonth($fileUploads, $type = $request->type ?? null);
    }

    public function updateDeadline(Request $request)
    {
        return $this->service->updateDeadline($request);
    }

    public function getFacility(Request $request)
    {
        return $this->service->getFacility($request);
    }
    public function getFacilityToReadComment(Request $request)
    {
        return $this->service->getFacilityToReadComment($request);
    }
    public function getFacilityFistributeFileAfterComment(Request $request)
    {
        return $this->service->getFacilityFistributeFileAfterComment($request);
    }

    public function distributionFile(Request $request)
    {
        return $this->service->distributionFile($request);
    }
    public function distributionAllFile()
    {
        return $this->service->distributionAllFile();
    }
    public function distributionAllFileComment()
    {
        return $this->service->distributionAllFileComment();
    }
    public function readAllFileComment()
    {
        return $this->service->readAllFileComment();
    }
    public function readFileComment(Request $request)
    {
        return $this->service->readFileComment($request);
    }
    public function saveTemplateMail(Request $request)
    {
        return $this->service->saveTemplateMail($request);
    }
    public function distributionFileComment(Request $request)
    {
        return $this->service->distributionFileComment($request);
    }
    public function uploadFacilities(Request $request)
    {
        return $this->service->uploadFacilities($request);
    }
    public function updateFacilityDeadline(Request $request)
    {
        return $this->service->updateFacilityDeadline($request);
    }
    public function exportCSV(Request $request)
    {
        return $this->service->exportCSV($request);
    }
    public function getFileEarly(Request $request)
    {
        return $this->service->getFileEarly($request);
    }
    public function getFileEarlyComment(Request $request)
    {
        return $this->service->getFileEarlyComment($request);
    }
    public function getFileMid(Request $request)
    {
        return $this->service->getFileMid($request);
    }

    public function getFileMidComment(Request $request)
    {
        return $this->service->getFileMidComment($request);
    }
}
