<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Services\Admin\DecentralizateFacilityDetailService;
use App\Messages;
use Illuminate\Http\Request;
use Redirect;

class DecentralizateFacilityDetailController extends Controller
{
    public function __construct(DecentralizateFacilityDetailService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        $results = $this->service->index($request);
        return view('page.admin.decentralization.detail-decentralization-facility', compact('results'));
    }
    public function update(Request $request)
    {
        $results = $this->service->update($request);
        if (!$results) {
            return back();
        } else {
            return Redirect::back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }
    public function store($id, Request $request)
    {
        $results = $this->service->store($id, $request);
        if (!$results) {
            return back();
        } else {
            return Redirect::back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }
    public function delete(Request $request)
    {
        return $this->service->delete($request);
    }
}
