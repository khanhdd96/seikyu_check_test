<?php

namespace App\Http\Controllers;

use App\Http\Services\CheckNo4Service;
use App\Rules\CheckNo4FileName;
use Illuminate\Http\Request;
use App\Messages;
use Illuminate\Support\Facades\Validator;

class CheckNo4Controller extends Controller
{
    private $service;
    public function __construct(CheckNo4Service $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $month = $request->month ?? date('Y/m');
        $fileUpload = $request->file;
        $fileName = $fileUpload->getClientOriginalName();
        $validator = Validator::make(
            [
                'file' => $fileUpload,
                'fileExtension' => strtolower($fileUpload->getClientOriginalExtension()),
                'fileName' => $fileName
            ],
            [
                'file' => 'max:112640',
                'fileExtension' => 'required|in:csv,xlsx,xls',
                'fileName' => [
                    'required',
                    new CheckNo4FileName($month)
                ]
            ],
            [
                'file.max' => Messages::OVERSIZE,
                'fileExtension.in' => Messages::CHECK_4_FILE_FORMAT,
            ]
        );
        if ($validator->stopOnFirstFailure()->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
                'file' => $fileName
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
