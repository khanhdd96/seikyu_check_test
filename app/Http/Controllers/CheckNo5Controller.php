<?php

namespace App\Http\Controllers;

use App\Consts;
use App\Http\Services\CheckNo5Service;
use App\Messages;
use App\Rules\CheckNo5THFileName;
use Illuminate\Http\Request;

class CheckNo5Controller extends Controller
{
    public function __construct(CheckNo5Service $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $month = $request->month ?? date('Y/m');
        if ($request->type == Consts::TYPE_CHECK_NUMBER_5_M) {
            $validator = \Validator::make(
                [
                    'file' => $request->file,
                    'extension0' => strtolower($request->file[0]->getClientOriginalExtension()),
                    'extension1' => strtolower($request->file[1]->getClientOriginalExtension()),
                    'name0' => $request->file[0]->getClientOriginalName(),
                    'name1' => $request->file[1]->getClientOriginalName(),
                ],
                [
                    'file' => 'required|array|min:2|max:2',
                    'file.*' => 'max:112640',
                    'extension0' => 'required|in:csv',
                    'extension1' => 'required|in:xlsx,xls',
                    'name0' => 'regex:/(.*)([\d]{4})m([\d]{3}.(.*))$/',
                    'name1' => 'regex:/(.*)(請求計算確認表)(.*)/'
                ],
                [
                    'file.required' => Messages::FILE_REQUIRED,
                    'file.min' => Messages::FILE_REQUIRED,
                    'extension0.required' => Messages::FILE_FOMAT_WRONG,
                    'extension1.required' => Messages::FILE_FOMAT_WRONG,
                    'extension0.in' => Messages::FILE_FOMAT_WRONG,
                    'extension1.in' => Messages::FILE_FOMAT_WRONG,
                    'file.*.max' => Messages::OVERSIZE,
                    'file.uploaded' => Messages::OVERSIZE,
                    'name0.regex' => Messages::CSV_ERROR,
                    'name1.regex' => Messages::EXCEL_ERROR
                ]
            );
        } else {
            $rules = [
                'file' => 'required|max:112640'
            ];
            if ($request->type == Consts::TYPE_CHECK_NUMBER_5_S) {
                $rules['name'] = 'regex:/(.*)([\d]{4})s([\d]{3}.(.*))$/';
                $rules['extension'] = 'required|in:xlsx,xls,csv';
            }
            if ($request->type == Consts::TYPE_CHECK_NUMBER_5_TH) {
                $rules['name'] = [new CheckNo5THFileName($month)];
                $rules['extension'] = 'required|in:csv';
            }
            $validator = \Validator::make(
                [
                    'file' => $request->file,
                    'extension' => strtolower($request->file->getClientOriginalExtension()),
                    'name' => $request->file->getClientOriginalName()
                ],
                $rules,
                [
                    'extension.required' => Messages::FILE_FOMAT_WRONG,
                    'extension.in' => Messages::FILE_FOMAT_WRONG,
                    'file.required' => Messages::FILE_REQUIRED,
                    'file.max' => Messages::OVERSIZE,
                    'file.uploaded' => Messages::OVERSIZE,
                    'name.regex' => Messages::NAME_ERROR
                ]
            );
        }
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
