<?php

namespace App\Http\Controllers;

use App\Http\Services\RandomCheckService;
use App\Messages;
use App\Rules\CheckNo8FileName;
use Illuminate\Http\Request;

class RandomCheckController extends Controller
{
    public function __construct(RandomCheckService $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $month = $request->month ?? date('Y/m');
        $validator = \Validator::make(
            [
                'file' => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
            ],
            [
                'file' => ['required', 'max:112640', new CheckNo8FileName($month)],
                'extension' => 'required|in:xlsx,xls',
            ],
            [
                'extension.required' => Messages::FILE_FOMAT_WRONG,
                'extension.in' => Messages::FILE_FOMAT_WRONG,
                'file.required' => Messages::FILE_REQUIRED,
                'file.max' => Messages::OVERSIZE,
                'file.uploaded' => Messages::OVERSIZE
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
