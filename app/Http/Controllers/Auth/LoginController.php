<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Aacotroneo\Saml2\Http\Controllers\Saml2Controller;
use Aacotroneo\Saml2\Saml2Auth;
use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Illuminate\Support\Facades\Auth;
use App\Messages;

class LoginController extends Saml2Controller
{
    public function acs(Saml2Auth $saml2Auth, $idpName)
    {
        $errors = $saml2Auth->acs();
        $user = $saml2Auth->getSaml2User();
        if (!empty($errors)) {
            logger()->error('Saml2 error_detail', ['error' => $saml2Auth->getLastErrorReason()]);
            session()->flash('saml2_error_detail', [$saml2Auth->getLastErrorReason()]);

            logger()->error('Saml2 error', $errors);
            session()->flash('saml2_error', $errors);
            return redirect(config('saml2_settings.errorRoute'));
        }
        $user = $saml2Auth->getSaml2User();
        event(new Saml2LoginEvent($idpName, $user, $saml2Auth));
        $redirectUrl = $user->getIntendedUrl();
        if ($redirectUrl !== null) {
            return redirect($redirectUrl);
        } else {
            if (!Auth::user()) {
                return redirect()->route('login')->withErrors(['msg' => Messages::LOGIN_ERROR]);
            }
            if (Auth::user()->is_admin) {
                return redirect(config('saml2_settings.loginRoute'));
            }
            return redirect('/');
        }
    }
}
