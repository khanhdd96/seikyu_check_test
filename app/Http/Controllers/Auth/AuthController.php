<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Consts;
use Illuminate\Support\Facades\Redirect;

class AuthController extends BaseController
{
    protected $company;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        if (Auth::check()) {
            return redirect(url()->previous());
        }

        return view('login');
    }

    public function logout(Request $request)
    {
        try {
            if (Auth::check()) {
                Auth::logout();
            }

            return redirect()->route('login');
        } catch (\Exception $e) {
            \Log::error($e);

            return redirect()->route('home.index');
        }
        if (Auth::check()) {
        }
    }
}
