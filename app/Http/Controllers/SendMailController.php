<?php

namespace App\Http\Controllers;

use App\Http\Services\Admin\SendMailService;
use App\Http\Services\FileCheckService;
use App\Messages;
use Illuminate\Http\Request;

class SendMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(SendMailService $service)
    {
        $this->service = $service;
    }

    public function sendMailTypeCheckNo9(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                'facility' => "required_if:send_mail,==,on",
                "mail_content" => "required_if:send_mail,==,on|min:50|nullable|string"
            ],
            [
                'facility.required_if' => Messages::FACILITY_REQUIRED,
                'mail_content.required_if' => Messages::CONTENT_MAIL_REQUIRED,
                'mail_content.min' => Messages::CONTENT_MAIL_LENGTH
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->sendMailTypeCheckNo9($request);
    }
}
