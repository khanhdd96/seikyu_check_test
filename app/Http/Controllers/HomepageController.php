<?php

namespace App\Http\Controllers;

use App\Http\Services\FileCheckService;
use App\Http\Services\HomepageServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomepageController extends Controller
{
    protected $service;

    public function __construct(HomepageServices $service, FileCheckService $fileCheckService)
    {
        $this->service = $service;
        $this->fileCheckService = $fileCheckService;
    }

    public function index(Request $request)
    {
        $requestDatas = $request->all();
        $data = $this->service->index($request);
        return view('homes.home', compact('data', 'requestDatas'));
    }

    public function getDetailCheck(Request $request)
    {
        return response()->json([
            'data' => $this->fileCheckService->getdetailCheck($request)
        ], 200);
    }

    public function getLinkDownload(Request $request)
    {
        return response()->json([
            'data' => $this->fileCheckService->getLinkDownload($request->id)
        ], 200);
    }
}
