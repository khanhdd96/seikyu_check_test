<?php

namespace App\Http\Controllers;

use App\Http\Services\CheckAllService;
use Illuminate\Http\Request;

class CheckAllController extends Controller
{
    private $checkAllService;
    public function __construct(CheckAllService $checkAllService)
    {
        $this->checkAllService = $checkAllService;
    }

    public function checkFile(Request $request)
    {
        return $this->checkAllService->checkAll($request);
    }

    public function deleteJob(Request $request)
    {
        return $this->checkAllService->deleteJob($request);
    }
}
