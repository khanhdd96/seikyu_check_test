<?php

namespace App\Http\Controllers;

use App\Consts;
use App\Http\Services\CheckNo2Service;
use App\Messages;
use App\Rules\CheckNo2FileDuplicate;
use App\Rules\CheckNo2FileExtension;
use App\Rules\CheckNo2FileName;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CheckNo2Controller extends Controller
{
    private $service;
    public function __construct(CheckNo2Service $service)
    {
        $this->service = $service;
    }

    public function checkFile(Request $request)
    {
        $fileUpload = $request->file;
        $validator = Validator::make(
            [
                'file' => $fileUpload,
            ],
            [
                'file.*' => ['bail', new CheckNo2FileName(), new CheckNo2FileExtension(), 'max:112640'],
                'file' => [new CheckNo2FileDuplicate($fileUpload)],
            ],
            [
                'file.*.max' => Messages::OVERSIZE,
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
                'file' => $this->service->getFileName($fileUpload)
            ], 400);
        }
        return $this->service->checkFile($request);
    }
}
