<?php

namespace App\Http\Controllers;

use App\Http\Services\FileCheckService;
use App\Messages;
use Illuminate\Http\Request;
use App\Http\Services\MDataInputService;

class FileCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(FileCheckService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $results = $this->service->index($request);
        $files = $results['files'];
        $typeCheck = $results['typeCheck'];
        $data = [
            'settingMonth' => $results['settingMonth'],
            'month' => $results['month'],
            'facilityId' => $results['facilityId'],
            'fileCount' => $results['fileCount'],
            'canUpload' => $results['canUpload'],
            'facilities' => $results['facilities'],
            'departments' => $results['departments'],
            'createdAt' => $results['createdAt'],
            'updatedAt' => $results['updatedAt'],
            'typeCheck1Admin' => $results['typeCheck1Admin']
        ];
        if ($request->ajax()) {
            return view('page.detailCheck.item-paginate', compact('files', 'typeCheck'));
        }
        return view('page.detailCheck.detail-check', compact('files', 'typeCheck', 'data'));
    }
    public function reserve(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                'reason' => "required_if:check_reserve,==,on|min:5|nullable|string",
            ],
            [
                'reason.required_if' => Messages::RESERVE_REASON_REQUIRE,
                'reason.max' => Messages::RESERVE_REASON_LENGTH,
                'reason.min' => Messages::RESERVE_REASON_LENGTH,
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
            ], 400);
        }
        return $this->service->reserve($request);
    }
}
