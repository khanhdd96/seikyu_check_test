<?php

namespace App\Http\Controllers;

use App\Http\Services\CheckNo1Service;
use App\Messages;
use App\Rules\CheckNo1AllFileExist;
use App\Rules\CheckNo1FileName;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CheckNo1Controller extends Controller
{
    private $service;
    public function __construct(CheckNo1Service $service)
    {
        $this->service = $service;
    }
    public function checkFile(Request $request)
    {
        $fileUploads = $request->file;
        $validator = Validator::make(
            [
                'file' => $fileUploads,
            ],
            [
                'file.*' => ['bail', new CheckNo1FileName(), 'max:112640'],
                'file' => new CheckNo1AllFileExist($fileUploads),
            ],
            [
                'file.*.max' => Messages::OVERSIZE,
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
                "validator" => true,
                'file' => $this->service->getFileName($fileUploads)
            ], 400);
        }
        return $this->service->checkFile($request);
    }
    public function uploadFileComment(Request $request)
    {
        return $this->service->uploadFileComment($request);
    }
}
