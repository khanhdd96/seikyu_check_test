<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\SettingMonth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Consts;

class CheckUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $settingMonth = SettingMonth::whereFacilitiesId($request->facilityId)->whereYearMonth($request->month)->first();

        $role = Role::whereFacilitiesId($request->facilityId)->whereUsersId(Auth::id())->first();
        if ($settingMonth->can_upload && ((isset($role) && $role->edit) || Auth::user()->is_admin) &&
            $settingMonth->status_facilitity != Consts::STATUS_CHECK_FILE['success']
        ) {
            return $next($request);
        }
        return response()->json([
            'error' => true,
            'message' => ['無許可'],
            "validator" => true,
        ], 400);
    }
}
