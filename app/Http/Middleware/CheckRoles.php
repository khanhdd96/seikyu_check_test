<?php

namespace App\Http\Middleware;

use App\Consts;
use App\Messages;
use App\Models\Role;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $stringRoles)
    {
        $user = Auth::user();
        $listRoles =  array_map('intval', explode(' ', $stringRoles));
        $role = $user->is_admin;
        $countFacility = $user->facilitys()->where('is_active', 1)->count();
        $facilityIds = $user->facilitys()->pluck('facilities_id')->toArray();
        $roles = Role::whereIn('facilities_id', $facilityIds)->whereUsersId($user->id)->get();
        $canView = false;
        foreach ($roles as $roleFacility) {
            if ($roleFacility->view) {
                $canView = true;
                break;
            }
        }
        if (!in_array($role, $listRoles) || ($role != Consts::ADMIN && $canView == false) ||
            ($role != Consts::ADMIN && $countFacility <= 0)) {
            return redirect()->route('logout');
        }
        return $next($request);
    }
}
