<?php

namespace App\Http\Requests;

use App\Consts;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SettingGroupRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id_group');
        return [
            'group_name' => 'required|string|unique:groups,group_name,'. $id ?? '' . '',
            'facilities' => 'required_without_all:select_all_users,users,select_all_facilities',
            'users' => 'required_without_all:select_all_users,facilities,select_all_facilities',
            ];
    }
    public function messages()
    {
        return [
            'group_name.required' => "グループ名は必須項目です。",
            'group_name.unique' => "同じグループ名",
            'facilities.required' => "Chọn ít nhất một cơ sở hoặc 1 user",
            'facilities.users' => "Chọn ít nhất một user hoặc 1 cơ sở"
        ];
    }
}
