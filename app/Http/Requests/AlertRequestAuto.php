<?php

namespace App\Http\Requests;

use App\Consts;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AlertRequestAuto extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date_format:Y/m/d',
//            'time' => 'required|date_format:H:i',
            'frequency_type' => 'required',
            'interval' => 'nullable|numeric|min:1|max:31|
                            required_unless:frequency_type,'. Consts::MAILING_FREQUENCY_DAILI,
//            'send_times' => 'required|numeric|min:1|max:31',
//            'content' => 'required|min:20',
        ];
    }
    public function messages()
    {
        return [
            'start_date.required' => '日選択を入力してください。',
//            'time.required' => '時選択を入力してください。',
            'frequency_type.required' => '頻度を入力してください。',
            'interval.required_unless' => '送信間隔を入力してください。',
            'interval.min' => '1より大きい31未満を入力してください。',
            'interval.max' => '1より大きい31未満を入力してください。',
//            'send_times.min' => '1より大きい31未満を入力してください。',
//            'send_times.max' => '1より大きい31未満を入力してください。',
//            'send_times.required' => '回数を入力してください。',
//            'content.required' => '内容を入力してください。',
//            'content.min' => '少なくとも20文字を入力してください。',
        ];
    }
}
