<?php

namespace App\Http\Requests;

use App\Consts;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AlertRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'groups' => 'required_without_all:users,facility',
            'users' => 'required_without_all:groups,facility',
            'facility' => 'required_without_all:groups,users',
            'start_date' => 'required|date_format:Y/m/d',
            'time' => 'required|date_format:H:i',
            'frequency_type' => 'required',
            'subject' => 'required',
            'interval' => 'nullable|numeric|min:1|max:31|required_if:frequency_type,' .
                Consts::MAILING_FREQUENCY_DAILI_INTERVAL . ',' . Consts::MAILING_FREQUENCY_WEEKLY_INTERVAL,
            'send_times' => 'required|numeric|min:1|max:31',
            'content' => 'required|min:20',
        ];
    }
    public function messages()
    {
        return [
            'groups.required_without_all' => 'ユーザー名を入力してください。',
            'users.required_without_all' => 'グループを入力してください。',
            'facility.required_without_all' => '事業所を入力してください。',
            'start_date.required' => '日選択を入力してください。',
            'time.required' => '時選択を入力してください。',
            'time.date_format' => '入力された時間がhh:iiのフォーマットと一致していません',
            'frequency_type.required' => '頻度を入力してください。',
            'interval.required_if' => '送信間隔を入力してください。',
            'interval.min' => '1より大きい31未満を入力してください。',
            'interval.max' => '1より大きい31未満を入力してください。',
            'send_times.min' => '1より大きい31未満を入力してください。',
            'send_times.max' => '1より大きい31未満を入力してください。',
            'send_times.required' => '回数を入力してください。',
            'subject.required' => 'メール件名を入力してください。',
            'content.required' => '内容を入力してください。',
            'content.min' => '少なくとも20文字を入力してください。',
        ];
    }
}
