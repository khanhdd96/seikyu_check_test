<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Jobs\SendMailCheckNo9;
use App\Messages;
use App\Models\FacilityUser;
use App\Models\File;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SendMailService
{
    public function __construct(
        File $model,
        FacilityService $facilityService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
    }
    public function sendMailTypeCheckNo9($request)
    {
        $subject = Consts::SUBJECT_MAIL_TYPE_9;
        $errors = json_decode($request->data);
        $file = File::whereId($request->file_check)->first();
        $facilities = $this->facilityService->getFacilityByNames($request->facility);
        $userIds = Role::whereIn('facilities_id', array_column($facilities, 'id'))
            ->pluck('users_id')->toArray();
        $userEmails = User::whereIn('id', $userIds)
            ->whereIsFacility(true)
            ->select('id', 'email')
            ->get()->toArray();
        foreach ($facilities as &$facility) {
            $facilityEmails = [];
            foreach ($userEmails as $userEmail) {
                if (in_array($userEmail['id'], array_column($facility['roles'], 'users_id'))) {
                    $facilityEmails[] = $userEmail['email'];
                }
            }
            $facility['user_email'] = $facilityEmails;
        }
        if (isset($request->type_mail) && $request->type_mail == 'all') {
            $template = $this->setTemplateMail($request, $errors);
            SendMailCheckNo9::dispatch($template, array_column($userEmails, 'email'), $subject);
        } else {
            foreach ($facilities as $facilityData) {
                $template = $this->setSpecificTemplateMail($request, $facilityData['name'], $errors);
                SendMailCheckNo9::dispatch($template, $facilityData['user_email'], $subject);
            }
        }
        try {
            $file->check_send_mail = 1;
            $file->suspended_message = $request->mail_content;
            $file->time_send_mail = Carbon::now();
            $file->mail_recipients = $request->facility;
            $file->save();
            return response()->json([
                'error' => false,
                'data' => []

            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }
    public function setTemplateMail($request, $errors)
    {
        $template = $request->mail_content ?? '';
        foreach ($errors as $error) {
            $template .= "
{$error->error_position}: {$error->error_count}";
        }
        return $template;
    }
    public function setSpecificTemplateMail($request, $name, $errors)
    {
        $template = $request->mail_content ?? '';
        foreach ($errors as $error) {
            if ($name == $error->error_position) {
                $template .= "
{$error->error_position}: {$error->error_count}";
            }
        }
        return $template;
    }
}
