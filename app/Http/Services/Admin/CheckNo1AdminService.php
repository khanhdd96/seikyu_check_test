<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Exports\Function1Export;
use App\Jobs\CreateFileAfterCommentEarlyMonth;
use App\Jobs\CreateFileAfterCommentMidMonth;
use App\Jobs\CreateFileTemplateEarlyMonth;
use App\Jobs\ProcessFile;
use App\Messages;
use App\Models\Department;
use App\Models\Facility;
use App\Models\Output2LayerBusiness;
use App\Models\TOrganizationData;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Response as Download;
use App\Jobs\SendDistributionMail;

class CheckNo1AdminService
{
    public function __construct(
        TypeCheckNo1Admin $model,
        Facility $facility
    ) {
        $this->model = $model;
        $this->facility = $facility;
    }

    public function index($request)
    {
        $date = !empty($request->month) ? str_replace('/', '-', $request->month) : date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $month = explode('-', $date);
        if ($typeCheck && count($month) == 2) {
            $query = Facility::with([
                'detailTypeCheck1Admin' => function ($query) use ($typeCheck) {
                    $query->where('type_check_no_1_admin_id', $typeCheck->id);
                },
                'department']);
            if (!empty($request->department_id)) {
                $query = $query->whereDepartmentsId($request->department_id);
            }
            if (!empty($request->facility_id)) {
                $query = $query->whereId($request->facility_id);
            }
            $details = $query->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        };
        $department = Department::pluck('name', 'id')->toArray();
        $facilities = Facility::pluck('name', 'id')->toArray();
        $typeCheck = $this->model->where('date', '=', date('Y-m'))->first();
        return [
            'typeCheck' => $typeCheck,
            'details' => $details ?? [],
            'departments' => $department,
            'facilities' => $facilities
        ];
    }

    public function upFileEarlyMonth($fileUploads, $type)
    {
        $typeCheckNo1Admin = TypeCheckNo1Admin::where('date', '=', date('Y-m'))->first();
        if (!$typeCheckNo1Admin) {
            $typeCheckNo1Admin = new TypeCheckNo1Admin;
        }
        $date = date('Y-m');
        $typeCheckNo1Admin->date = $date;
        $typeCheckNo1Admin->has_error = false;
        if ($type == 'mid') {
            $path = 'uploads/type-check-1-admin' . '/file-upload/mid/' . $date;
            $typeCheckNo1Admin->url_folder_mid_month = $path;
            $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'];
        } else {
            $path = 'uploads/type-check-1-admin' . '/file-upload/' . $date;
            $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_EARLY_MONTH'];
            $typeCheckNo1Admin->url_folder_early_month = $path;
        }
        if (!\File::isDirectory(public_path($path))) {
            \File::makeDirectory(public_path($path), 0777, true, true);
        }
        $fileNames = [
            '予実明細',
            '債権一覧照会_利用者',
            '債権一覧照会_国保連',
            '債権一覧照会_市区町村',
            '公費情報',
            '返戻一覧',
        ];
        try {
            foreach ($fileUploads as $file) {
                foreach ($fileNames as $fileName) {
                    $name = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    if (str_starts_with($name, $fileName)) {
                        $name = $fileName . "." . $extension;
                        $destinationPath = public_path($path);
                        $file->move($destinationPath, $name);
                    }
                }
            }
            $typeCheckNo1Admin->is_run_cron = true;
            $typeCheckNo1Admin->save();
            CreateFileTemplateEarlyMonth::dispatch($type)->onQueue('createFileTemplate');
            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function getFacility($request)
    {
        $keywork = $request->key_word;
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $facilities = $this->facility->whereHas('detailTypeCheck1Admin', function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
            }
        })->with(['detailTypeCheck1Admin' => function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
            }
        }]);
        if ($keywork) {
            $keywork = "\\" . $keywork;
            $facilities = $facilities->where('name', 'like', '%' . $keywork . '%');
        }
        $facilities = $facilities->get();
        return response()->json(['facilities' => $facilities], 200);
    }

    public function updateDeadline($request)
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        if (!$typeCheck) {
            $typeCheck = new TypeCheckNo1Admin();
            $typeCheck->date = $date;
        }
        $stepEarlyMonth = $typeCheck->step;
        DB::beginTransaction();
        try {
            if ($stepEarlyMonth < Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH']) {
                $typeCheck->deadline_early_month = $request->date;
                TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                    ->update(['deadline_early_month' => $request->date, 'deadline' => $request->date]);
            } else {
                $typeCheck->deadline_mid_month = $request->date;
                TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                    ->update(['deadline_mid_month' => $request->date, 'deadline' => $request->date]);
            }
            $typeCheck->deadline = $request->date;
            $typeCheck->save();
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function distributionFile($request)
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $step = $typeCheck->step;
        $step = $step <= Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'];
        $stepRollBack = $step == Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_EARLY_MONTH'];
        $status = $step == Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ? 6 : 1;
        $statusRollBack = $step == Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ? 5 : 0;
        $typeCheck->step = $step;
        $idUnChecks = $request->un_check ?? [];
        $idChecks = $request->facility ?? [];
        DB::beginTransaction();
        try {
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->whereIn('facility_id', $idChecks)
                ->update(['step' => $step, 'status' => $status]);
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->whereIn('facility_id', $idUnChecks)
                ->update(['step' => $stepRollBack, 'status' => $statusRollBack]);
            $typeCheck->save();
            DB::commit();
            if ($idChecks) {
                SendDistributionMail::dispatch(
                    $typeCheck,
                    $idChecks,
                    Consts::TYPE_DISTRIBUTION_MAIL['DISTRIBUTE'],
                    $status
                );
            }
            if ($idUnChecks) {
                SendDistributionMail::dispatch(
                    $typeCheck,
                    $idUnChecks,
                    Consts::TYPE_DISTRIBUTION_MAIL['NOT_DISTRIBUTE'],
                    $status
                );
            }
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function distributionFileComment($request)
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $step = $typeCheck->step;
        $step = $step < Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'];
        $stepRollBack = $step < Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'];
        $status = $step == Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ? 3 : 8;
        $typeCheck->step = $step;
        $idUnChecks = $request->un_check ?? [];
        $totalStep = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', '=', $typeCheck->id)
            ->where('step', '>=', $stepRollBack)->count();
        DB::beginTransaction();
        try {
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->whereIn('facility_id', $idUnChecks)
                ->update(['step' => $stepRollBack, 'status' => $status - 1]);
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->whereIn('facility_id', $request->facility)
                ->update(['step' => $step, 'status' => $status]);
            $facilityRead = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', '=', $typeCheck->id)
                ->where('step', '>=', $step)->count();
            if ($totalStep == $facilityRead) {
                $typeCheck->step = $step <= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ?
                    Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'] :
                    Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'];
            }
            $typeCheck->save();
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function readFileComment($request)
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->firstOrFail();
        $step = $typeCheck->step < Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'];
        $typeCheck->step = $step;
        $typeCheck->is_run_cron = true;
        DB::beginTransaction();
        try {
            $typeCheck->save();
            if ($step == Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']) {
                CreateFileAfterCommentEarlyMonth::dispatch($request->facility)->onQueue('createFileAfterComment');
            } else {
                CreateFileAfterCommentMidMonth::dispatch($request->facility)->onQueue('createFileAfterComment');
            }
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function distributionAllFile()
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $step = $typeCheck->step;
        $step = $step <= Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'];
        $status = $step <= Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ? 1 : 6;
        $typeCheck->step = $step;
        DB::beginTransaction();
        try {
            $facilityIds = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->pluck('facility_id')->toArray();
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->update(['step' => $step, 'status' => $status]);
            $typeCheck->save();
            DB::commit();
            SendDistributionMail::dispatch(
                $typeCheck,
                $facilityIds,
                Consts::TYPE_DISTRIBUTION_MAIL['DISTRIBUTE'],
                $status
            );
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function distributionAllFileComment()
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $step = $typeCheck->step;
        $step = $step <= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'];
        $typeCheck->step = $step;
        $status = $step <= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ? 3 : 8;
        DB::beginTransaction();
        try {
            TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->where(function ($query) {
                    $query->where('step', '=', 4)
                        ->orWhere('step', '=', 11);
                })
                ->update(['step' => $step, 'status' => $status]);
            $typeCheck->save();
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function readAllFileComment()
    {
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->firstOrFail();
        $step = $typeCheck->step <= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'] ?
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] :
            Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'];
        $typeCheck->step = $step;
        $typeCheck->is_run_cron = true;
        DB::beginTransaction();
        try {
            $typeCheck->save();
            if ($step == Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']) {
                CreateFileAfterCommentEarlyMonth::dispatch()->onQueue('createFileAfterComment');
            } else {
                CreateFileAfterCommentMidMonth::dispatch()->onQueue('createFileAfterComment');
            }
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 500);
        }
    }

    public function getFacilityToReadComment($request)
    {
        $keywork = $request->key_word;
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $facilities = $this->facility->whereHas('detailTypeCheck1Admin', function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_MID_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_EARLY_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
            }
        })->with(['detailTypeCheck1Admin' => function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_MID_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_EARLY_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
            }
        }]);
        if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
            $type = 'mid';
        } else {
            $type = 'early';
        }
        if ($keywork) {
            $keywork = "\\" . $keywork;
            $facilities = $facilities->where('name', 'like', '%' . $keywork . '%');
        }
        $facilities = $facilities->get();
        return response()->json(['facilities' => $facilities, 'type' => $type], 200);
    }

    public function getFacilityFistributeFileAfterComment($request)
    {
        $keywork = $request->key_word;
        $date = date('Y-m');
        $typeCheck = $this->model->where('date', '=', $date)->first();
        $facilities = $this->facility->whereHas('detailTypeCheck1Admin', function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '=', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']);
            }
        })->with(['detailTypeCheck1Admin' => function ($query) use ($typeCheck) {
            $query->where('type_check_no_1_admin_id', $typeCheck->id);
            if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
                $query->where("step", '=', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']);
            } else {
                $query->where("step", '>=', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH']);
                $query->where('step', '<', Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']);
            }
        }]);
        if ($typeCheck->step >= Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) {
            $type = 'mid';
        } else {
            $type = 'early';
        }
        if ($keywork) {
            $keywork = "\\" . $keywork;
            $facilities = $facilities->where('name', 'like', '%' . $keywork . '%');
        }
        $facilities = $facilities->get();
        return response()->json(['facilities' => $facilities, 'type' => $type], 200);
    }

    public function saveTemplateMail($request)
    {
        $column = Consts::FUNCTION_1_MAIL_TYPE[$request->mail_type] ?? '';
        if (!empty($column)) {
            $date = date('Y-m');
            $model = $this->model->where('date', $date)->first();
            if (empty($model)) {
                $model = new TypeCheckNo1Admin();
                $model->date = $date;
            }
            $model->{$column} = $request->mail_id;
            $model->save();
            return response()->json([]);
        }
        return response()->json([], 500);
    }

    public function updateFacilityDeadline($request)
    {
        if (!empty($request->facility_id)) {
            $facility = TypeCheckNo1AdminStep::findOrFail($request->facility_id);
            $typeCheckId = $facility->type_check_no_1_admin_id;
            $typeCheck = $this->model->findOrFail($typeCheckId);
            $stepEarlyMonth = $typeCheck->step;
            if ($stepEarlyMonth < Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH']) {
                $facility->deadline_early_month = $request->date;
            } else {
                $facility->deadline_mid_month = $request->date;
            }
            $facility->deadline = $request->date;
            $facility->save();
            return response()->json([]);
        }
        return response()->json([], 500);
    }

    public function uploadFacilities($request)
    {
        $fileUpload = $request->file('file');
        $path = $fileUpload->getRealPath();
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $spreadsheet->setActiveSheetIndex(4);
        $sheet_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $row = [];
        foreach ($sheet_data as $key => $data) {
            if ($key > 1) {
                $row[$key]['事業'] = $data[array_search('事業', $sheet_data[1])];
                $row[$key]['系統'] = $data[array_search('系統', $sheet_data[1])];
                $row[$key]['2階層ＣＤ'] = $data[array_search('2階層ＣＤ', $sheet_data[1])];
                $row[$key]['2階層名称'] = $data[array_search('2階層名称', $sheet_data[1])];
                $row[$key]['2階層名称(略)'] = $data[array_search('2階層名称(略)', $sheet_data[1])];
                $row[$key]['3階層ＣＤ'] = $data[array_search('3階層ＣＤ', $sheet_data[1])];
                $row[$key]['3階層名称'] = $data[array_search('3階層名称', $sheet_data[1])];
                $row[$key]['3階層名称(略)'] = $data[array_search('3階層名称(略)', $sheet_data[1])];
                $row[$key]['4階層ＣＤ'] = $data[array_search('4階層ＣＤ', $sheet_data[1])];
                $row[$key]['4階層名称'] = $data[array_search('4階層名称', $sheet_data[1])];
                $row[$key]['4階層名称(略)'] = $data[array_search('4階層名称(略)', $sheet_data[1])];
                $row[$key]['5階層ＣＤ'] = $data[array_search('5階層ＣＤ', $sheet_data[1])];
                $row[$key]['5階層名称'] = $data[array_search('5階層名称', $sheet_data[1])];
                $row[$key]['5階層名称(略)'] = $data[array_search('5階層名称(略)', $sheet_data[1])];
                $row[$key]['事業所コード'] = $data[array_search('事業所コード', $sheet_data[1])];
                $row[$key]['事業所名'] = $data[array_search('事業所名', $sheet_data[1])];
                $row[$key]['指定事業所No'] = $data[array_search('指定事業所No', $sheet_data[1])];
            }
        }
        DB::beginTransaction();
        try {
            TOrganizationData::truncate();
            TOrganizationData::insert($row);
            DB::commit();
            return response()->json([], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }

    public function exportCSV($request)
    {
        $month = !empty($request->month) ? str_replace('/', '-', $request->month) : date('Y-m');
        $typeCheck = $this->model->where('date', $month)->first();
        if ($typeCheck) {
            $query = Facility::with([
                'detailTypeCheck1Admin' => function ($query) use ($typeCheck) {
                    $query->where('type_check_no_1_admin_id', $typeCheck->id);
                },
                'department'
            ]);
            if (!empty($request->department_id)) {
                $query = $query->whereDepartmentsId($request->department_id);
            }
            if (!empty($request->facility_id)) {
                $query = $query->whereId($request->facility_id);
            }
            $details = $query->get();
        } else {
            $details = null;
        }
        $name = '請求締めチェックツール_' . date('Ymd') . '_' . date('His') . '.csv';
        return Excel::download(new Function1Export($details), $name);
    }

    public function getFileEarly($request)
    {
        $typeCheck = TypeCheckNo1AdminStep::where('url_download_file_early_month', $request->fullUrl())
            ->where('step', '>', 1)->firstOrFail();
        if (!$typeCheck->url_file_early_month) {
            $kouhiLastData = DB::connection('mysql2')
                ->select("Select DISTINCT `利用者ID` from `公費情報_加工後`");
            $kouhiLastData = array_column($kouhiLastData, '利用者ID');
            $dataSheet5 = array_map(function ($value) {
                return [$value];
            }, $kouhiLastData);
            $dataSheet5 = array_merge([['利用者ID']], $dataSheet5);
            $facility = Facility::find($typeCheck->facility_id);
            $dataLayerBusiness = Output2LayerBusiness::get();
            $title = [
                '事業所コード',
                '事業所名',
                'サービス提供年月',
                '保険者番号',
                '保険者名',
                '被保険者番号',
                '利用者ID',
                '利用者名',
                '請求先区分',
                'サービス種別・事業名',
                '請求金額',
                '入金金額',
                '未収金額',
                '最終請求年月',
                '最新審査状況',
                '初回返戻月',
                '最新返戻月',
                '対応方法',
                '今回事業所コメント',
                '前回事業所コメント',
                '過去コメント１',
                '過去コメント２',
                '請求締め判定',
                '督促状フラグ',
                '解約通知フラグ',
                '二階層名称',
                '指定事業所番号',
                '指定事業所名'];
            $dataSheet4 = [];
            $dataFacilities = [
                'name' => $facility->name ?? '',
                'code' => $facility->hiiragi_code ?? '',
            ];
            foreach ($dataLayerBusiness->toArray() as $data) {
                if ($facility->office_code == $data['指定事業所番号'] &&
                    $facility->hiiragi_code == $data['事業所コード']) {
                    $dataSheet4[0] = $title;
                    $dataSheet4 = array_merge($dataSheet4, [array_values($data)]);
                    break;
                }
            }
            $path = public_path('uploads/type-check-1-admin' . '/file-create/' . date('Y-m'));
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 0777, true, true);
            }
            $dataSheet = $dataSheet4 ?? $title;
            $date = date('Ym');
            $outputFileName = $date . "_未収金明細表_" . $date;
            $outputFileName .= "_" . $dataFacilities['code'] . "_";
            $outputFileName .= $dataFacilities['name'] . ".xlsx";
            $path = public_path('uploads/type-check-1-admin/template/チェックシート_原本.xlsx');
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($path);
            $sheet2 = $spreadsheet->createSheet(4);
            $title = $dataFacilities['code'] . '_' . $dataFacilities['name'];
            $sheet2->setTitle($title)
                ->fromArray($dataSheet, null, 'A1');
            $sheet3 = $spreadsheet->createSheet(5);
            $sheet3->setTitle('公費情報')
                ->fromArray($dataSheet5, null, 'A1')
                ->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);
            $writer = IOFactory::createWriter($spreadsheet, $inputFileType);
            $writer->setPreCalculateFormulas(false);
            $fileOutput = 'uploads/type-check-1-admin' . '/file-create/' . date('Y-m') . '/' . $outputFileName;
            try {
                $writer->save(public_path($fileOutput));
                $typeCheck->url_file_early_month = $fileOutput;
                \Storage::disk('s3')->put($fileOutput, file_get_contents(public_path($fileOutput)), 'public');
                \File::delete(public_path($fileOutput));
                $typeCheck->save();
                $headers = [
                    'Content-Type' => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
                ];
                return Download::make(\Storage::disk('s3')->get($fileOutput), 200, $headers);
            } catch (\Exception $exception) {
                return;
            }
        }
        $outputFileName = explode('/', $typeCheck->url_file_early_month);
        $outputFileName = end($outputFileName);
        $headers = [
            'Content-Type' => 'Content-Type: application/zip',
            'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
        ];
        return Download::make(\Storage::disk('s3')->get($typeCheck->url_file_early_month), 200, $headers);
    }

    public function getFileEarlyComment($request)
    {
        $typeCheck = TypeCheckNo1AdminStep::where('url_download_file_early_month_after_comment', $request->fullUrl())
            ->where('step', '>', 1)->firstOrFail();
        if (!$typeCheck->url_file_early_month_after_comment) {
            $kouhiLastData = DB::connection('mysql2')
                ->select("Select DISTINCT `利用者ID` from `公費情報_加工後`");
            $kouhiLastData = array_column($kouhiLastData, '利用者ID');
            $dataSheet5 = array_map(function ($value) {
                return [$value];
            }, $kouhiLastData);
            $dataSheet5 = array_merge([['利用者ID']], $dataSheet5);
            $facility = Facility::find($typeCheck->facility_id);
            $dataLayerBusiness = Output2LayerBusiness::get();
            $title = [
                '事業所コード',
                '事業所名',
                'サービス提供年月',
                '保険者番号',
                '保険者名',
                '被保険者番号',
                '利用者ID',
                '利用者名',
                '請求先区分',
                'サービス種別・事業名',
                '請求金額',
                '入金金額',
                '未収金額',
                '最終請求年月',
                '最新審査状況',
                '初回返戻月',
                '最新返戻月',
                '対応方法',
                '今回事業所コメント',
                '前回事業所コメント',
                '過去コメント１',
                '過去コメント２',
                '請求締め判定',
                '督促状フラグ',
                '解約通知フラグ',
                '二階層名称',
                '指定事業所番号',
                '指定事業所名'];
            $dataSheet4 = [];
            $dataFacilities = [
                'name' => $facility->name ?? '',
                'code' => $facility->hiiragi_code ?? ''
            ];
            $dataSheet4[0] = $title;
            foreach ($dataLayerBusiness->toArray() as $data) {
                if ($facility->office_code == $data['指定事業所番号'] &&
                    $facility->hiiragi_code == $data['事業所コード']) {
                    $dataSheet4 = array_merge($dataSheet4, [array_values($data)]);
                    break;
                }
            }
            $pathOutput = 'uploads/type-check-1-admin' . '/file-after-comment/' . date('Y-m');
            if (!\File::isDirectory($pathOutput)) {
                \File::makeDirectory($pathOutput, 0777, true, true);
            }
            $dataSheet = $dataSheet4 ?? $title;
            $date = date('Ym');
            $outputFileName = $date . "_未収金明細表_" . $date . "_" .
                $dataFacilities['name'] . "_" . $dataFacilities['code'] . ".xlsx";
            $path = public_path('uploads/type-check-1-admin/template/チェックシート_原本.xlsx');
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($path);
            $sheet2 = $spreadsheet->createSheet(4);
            $title = $dataFacilities['name'] . '_' . $dataFacilities['code'];
            $sheet2->setTitle($title)
                ->fromArray($dataSheet, null, 'A1');
            $rows = [];
            $sheetDatas = $spreadsheet->getActiveSheet()
                ->toArray(null, true, true, true);
            foreach ($sheetDatas as $index => $value) {
                if (isset($value['R']) && str_contains($value['R'], '再請求')) {
                    $rows[] = $index;
                }
            }
            foreach ($rows as $row) {
                $calc = "=IF(COUNTIFS(①請求計算確認表（貼り付け）!C$8:C$1048576,D";
                $calc .= $row . ",①請求計算確認表（貼り付け）!D$8:D$1048576,F$row)>0,\"OK\",\"NG\")";
                $spreadsheet
                    ->getActiveSheet()
                    ->setCellValue('W' . $row, $calc)
                    ->getStyle('W' . $row)
                    ->applyFromArray([
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'startColor' => [
                                'rgb' => 'E8E84A',
                            ]
                        ],
                    ]);
            }
            $sheet3 = $spreadsheet->createSheet(5);
            $sheet3->setTitle('公費情報')
                ->fromArray($dataSheet5, null, 'A1')
                ->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);
            $writer = IOFactory::createWriter($spreadsheet, $inputFileType);
            $writer->setPreCalculateFormulas(false);
            $fileOutput = $pathOutput . '/' . $outputFileName;
            try {
                $writer->save(public_path($fileOutput));
                $typeCheck->url_file_early_month_after_comment = $fileOutput;
                \Storage::disk('s3')->put($fileOutput, file_get_contents(public_path($fileOutput)), 'public');
                \File::delete(public_path($fileOutput));
                $typeCheck->save();
                $headers = [
                    'Content-Type' => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
                ];
                return Download::make(\Storage::disk('s3')->get($fileOutput), 200, $headers);
            } catch (\Exception $exception) {
                return;
            }
        } else {
            $outputFileName = explode('/', $typeCheck->url_file_early_month_after_comment);
            $outputFileName = end($outputFileName);
            $headers = [
                'Content-Type' => 'Content-Type: application/zip',
                'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
            ];
            return Download::make(\Storage::disk('s3')->get($typeCheck->url_file_early_month), 200, $headers);
        }
    }
    public function getFileMid($request)
    {
        $typeCheck = TypeCheckNo1AdminStep::where('url_download_file_mid_month', $request->fullUrl())
            ->where('step', '>=', Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'])->firstOrFail();
        if (!$typeCheck->url_file_mid_month) {
            $kouhiLastData = DB::connection('mysql2')
                ->select("Select DISTINCT `利用者ID` from `公費情報_加工後`");
            $kouhiLastData = array_column($kouhiLastData, '利用者ID');
            $dataSheet5 = array_map(function ($value) {
                return [$value];
            }, $kouhiLastData);
            $dataSheet5 = array_merge([['利用者ID']], $dataSheet5);
            $facility = Facility::find($typeCheck->facility_id);
            $dataLayerBusiness = Output2LayerBusiness::get();
            $title = [
                '事業所コード',
                '事業所名',
                'サービス提供年月',
                '保険者番号',
                '保険者名',
                '被保険者番号',
                '利用者ID',
                '利用者名',
                '請求先区分',
                'サービス種別・事業名',
                '請求金額',
                '入金金額',
                '未収金額',
                '最終請求年月',
                '最新審査状況',
                '初回返戻月',
                '最新返戻月',
                '対応方法',
                '今回事業所コメント',
                '前回事業所コメント',
                '過去コメント１',
                '過去コメント２',
                '請求締め判定',
                '督促状フラグ',
                '解約通知フラグ',
                '二階層名称',
                '指定事業所番号',
                '指定事業所名'];
            $dataSheet4 = [];
            $dataFacilities = [
                'name' => $facility->name ?? '',
                'code' => $facility->hiiragi_code ?? '',
            ];
            $dataSheet4[0] = $title;
            foreach ($dataLayerBusiness->toArray() as $data) {
                if ($facility->office_code == $data['指定事業所番号'] &&
                    $facility->hiiragi_code == $data['事業所コード']) {
                    $dataSheet4 = array_merge($dataSheet4, [array_values($data)]);
                    break;
                }
            }
            $path = public_path('uploads/type-check-1-admin' . '/file-create/mid/' . date('Y-m'));
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 0777, true, true);
            }
            $dataSheet = $dataSheet4 ?? $title;
            $date = date('Ym');
            $outputFileName = $date . "_未収金明細表_" . $date;
            $outputFileName .= "_" . $dataFacilities['name'] . "_" . $dataFacilities['code'] . ".xlsx";
            $path = public_path('uploads/type-check-1-admin/template/チェックシート_原本.xlsx');
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($path);
            $sheet2 = $spreadsheet->createSheet(4);
            $title = $dataFacilities['code'] . '_' . $dataFacilities['name'];
            $sheet2->setTitle($title)
                ->fromArray($dataSheet, null, 'A1');
            $spreadsheet->setActiveSheetIndex(4);
            $sheetData = $spreadsheet->getActiveSheet()
                ->toArray(null, true, true, true);
            $rows = [];
            foreach ($sheetData as $index => $value) {
                if (str_contains($value['R'], '再請求')) {
                    $rows[] = $index;
                }
            }
            foreach ($rows as $row) {
                $calc = "=IF(COUNTIFS(①請求計算確認表（貼り付け）!C$8:C$1048576,D";
                $calc .= $row . ",①請求計算確認表（貼り付け）!D$8:D$1048576,F$row)>0,\"OK\",\"NG\")";
                $spreadsheet
                    ->getActiveSheet()
                    ->setCellValue('W' . $row, $calc)
                    ->getStyle('W' . $row)
                    ->applyFromArray([
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'startColor' => [
                                'rgb' => 'E8E84A',
                            ]
                        ],
                    ]);
            }

            $sheet3 = $spreadsheet->createSheet(5);
            $sheet3->setTitle('公費情報')
                ->fromArray($dataSheet5, null, 'A1')
                ->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);
            $writer = IOFactory::createWriter($spreadsheet, $inputFileType);
            $writer->setPreCalculateFormulas(false);
            $fileOutput = 'uploads/type-check-1-admin' . '/file-create/mid/' . date('Y-m') . '/' . $outputFileName;
            try {
                $writer->save(public_path($fileOutput));
                $typeCheck->url_file_mid_month = $fileOutput;
                \Storage::disk('s3')->put($fileOutput, file_get_contents(public_path($fileOutput)), 'public');
                \File::delete(public_path($fileOutput));
                $typeCheck->save();
                $headers = [
                    'Content-Type' => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
                ];
                return Download::make(\Storage::disk('s3')->get($fileOutput), 200, $headers);
            } catch (\Exception $exception) {
                return;
            }
        }
        $outputFileName = explode('/', $typeCheck->url_file_mid_month);
        $outputFileName = end($outputFileName);
        $headers = [
            'Content-Type' => 'Content-Type: application/zip',
            'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
        ];
        return Download::make(\Storage::disk('s3')->get($typeCheck->url_file_mid_month), 200, $headers);
    }
    public function getFileMidComment($request)
    {
        $typeCheck = TypeCheckNo1AdminStep::where('url_download_file_mid_month_after_comment', $request->fullUrl())
            ->where('step', '>', 1)->firstOrFail();
        if (!$typeCheck->url_download_file_mid_month_after_comment) {
            $kouhiLastData = DB::connection('mysql2')
                ->select("Select DISTINCT `利用者ID` from `公費情報_加工後`");
            $kouhiLastData = array_column($kouhiLastData, '利用者ID');
            $dataSheet5 = array_map(function ($value) {
                return [$value];
            }, $kouhiLastData);
            $dataSheet5 = array_merge([['利用者ID']], $dataSheet5);
            $facility = Facility::find($typeCheck->facility_id);
            $dataLayerBusiness = Output2LayerBusiness::get();
            $title = [
                '事業所コード',
                '事業所名',
                'サービス提供年月',
                '保険者番号',
                '保険者名',
                '被保険者番号',
                '利用者ID',
                '利用者名',
                '請求先区分',
                'サービス種別・事業名',
                '請求金額',
                '入金金額',
                '未収金額',
                '最終請求年月',
                '最新審査状況',
                '初回返戻月',
                '最新返戻月',
                '対応方法',
                '今回事業所コメント',
                '前回事業所コメント',
                '過去コメント１',
                '過去コメント２',
                '請求締め判定',
                '督促状フラグ',
                '解約通知フラグ',
                '二階層名称',
                '指定事業所番号',
                '指定事業所名'];
            $dataSheet4 = [];
            $dataFacilities = [
                'name' => $facility->name ?? '',
                'code' => $facility->hiiragi_code ?? ''
            ];
            $dataSheet4[0] = $title;
            foreach ($dataLayerBusiness->toArray() as $data) {
                if ($facility->office_code == $data['指定事業所番号'] &&
                    $facility->hiiragi_code == $data['事業所コード']) {
                    $dataSheet4 = array_merge($dataSheet4, [array_values($data)]);
                    break;
                }
            }
            $pathOutput = 'uploads/type-check-1-admin' . '/file-after-comment/' . date('Y-m'). '/mid';
            if (!\File::isDirectory($pathOutput)) {
                \File::makeDirectory($pathOutput, 0777, true, true);
            }
            $dataSheet = $dataSheet4 ?? $title;
            $date = date('Ym');
            $outputFileName = $date . "_未収金明細表_" . $date . "_" .
                $dataFacilities['code'] . "_" . $dataFacilities['name'] . ".xlsx";
            $path = public_path('uploads/type-check-1-admin/template/チェックシート_原本.xlsx');
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($path);
            $sheet2 = $spreadsheet->createSheet(4);
            $title = $dataFacilities['code'] . '_' . $dataFacilities['name'];
            $sheet2->setTitle($title)
                ->fromArray($dataSheet, null, 'A1');
            $rows = [];
            $sheetDatas = $spreadsheet->getActiveSheet()
                ->toArray(null, true, true, true);
            foreach ($sheetDatas as $index => $value) {
                if (isset($value['R']) && str_contains($value['R'], '再請求')) {
                    $rows[] = $index;
                }
            }
            foreach ($rows as $row) {
                $calc = "=IF(COUNTIFS(①請求計算確認表（貼り付け）!C$8:C$1048576,D";
                $calc .= $row . ",①請求計算確認表（貼り付け）!D$8:D$1048576,F$row)>0,\"OK\",\"NG\")";
                $spreadsheet
                    ->getActiveSheet()
                    ->setCellValue('W' . $row, $calc)
                    ->getStyle('W' . $row)
                    ->applyFromArray([
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'startColor' => [
                                'rgb' => 'E8E84A',
                            ]
                        ],
                    ]);
            }
            $sheet3 = $spreadsheet->createSheet(5);
            $sheet3->setTitle('公費情報')
                ->fromArray($dataSheet5, null, 'A1')
                ->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);
            $writer = IOFactory::createWriter($spreadsheet, $inputFileType);
            $writer->setPreCalculateFormulas(false);
            $fileOutput = $pathOutput . '/' . $outputFileName;
            try {
                $writer->save(public_path($fileOutput));
                $typeCheck->url_file_mid_month_after_comment = $fileOutput;
                \Storage::disk('s3')->put($fileOutput, file_get_contents(public_path($fileOutput)), 'public');
                \File::delete(public_path($fileOutput));
                $typeCheck->save();
                $headers = [
                    'Content-Type' => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
                ];
                return Download::make(\Storage::disk('s3')->get($fileOutput), 200, $headers);
            } catch (\Exception $exception) {
                return;
            }
        } else {
            $outputFileName = explode('/', $typeCheck->url_file_mid_month_after_comment);
            $outputFileName = end($outputFileName);
            $headers = [
                'Content-Type' => 'Content-Type: application/zip',
                'Content-Disposition' => 'attachment; filename="' . $outputFileName . '"',
            ];
            return Download::make(
                \Storage::disk('s3')->get($typeCheck->url_file_mid_month_after_comment),
                200,
                $headers
            );
        }
    }
}
