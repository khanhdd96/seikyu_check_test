<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Messages;
use App\Models\DepartmentGroup;
use App\Models\DepartmentGroupUser;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\Position;
use App\Models\PositionUser;
use App\Models\Role;
use App\Models\RolesWithType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DecentralizateService
{
    public function __construct(
        Facility $model
    ) {
        $this->model = $model;
    }

    public function index($request)
    {
        $search = $request->search;
        if ($request->type == 'role-admin' || !$request->type) {
            $users = User::all()->pluck('name', 'id')->toArray();
            $userCodes = User::all()->pluck('code', 'id')->toArray();
            $departments = DepartmentGroup::all()->pluck('name', 'id')->toArray();
            $departmentCodes = DepartmentGroup::all()->pluck('code', 'id')->toArray();
            $result = RolesWithType::where('type_role', '=', Consts::ROLE_ADMIN_TYPE);
            if ($request->option_type) {
                $result = $result->whereType($request->option_type);
            }
            if ($search) {
                $result = $result->whereHas('user', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                })->orWhereHas('departmentGroup', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                });
            }
            $result = $result->orderBy('created_at', 'DESC')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
            return $data = [
                'list-data' => $result,
                'users' => $users,
                'list_department' => $departments,
                'userCodes' => $userCodes,
                'departmentCodes' => $departmentCodes
            ];
        } elseif ($request->type == 'role-facility') {
            $users = User::all()->pluck('name', 'id')->toArray();
            $userCodes = User::all()->pluck('code', 'id')->toArray();
            $positions = Position::all()->pluck('name', 'id')->toArray();
            $positionCodes = Position::all()->pluck('code', 'id')->toArray();
            $result = RolesWithType::where('type_role', '=', Consts::ROLE_FACILITY_TYPE);
            if ($request->option_type) {
                $result = $result->whereType($request->option_type);
            }
            if ($search) {
                $result = $result->whereHas('user', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                })->orWhereHas('position', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                });
            }
            $result = $result->orderBy('created_at', 'DESC')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
            return $data = [
                'list-data' => $result,
                'users' => $users,
                'list_position' => $positions,
                'userCodes' => $userCodes,
                'positionCodes' => $positionCodes

            ];
        } elseif ($request->type == 'user') {
            $users = [];
            $result = Facility::with(['department' => function ($query) {
                $query->select('name', 'id');
            }])->withCount('roles')->whereIsActive(true);
            if ($search) {
                $result = $result->where('name', 'like', '%' . $search . '%');
            }
            $result = $result->orderBy('created_at', 'DESC')
                ->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
            return $data = [
                'list-data' => $result,
                'users' => $users
            ];
        }
    }

    public function updateRole($request)
    {
        $userIds = $request->users ?? [];
        if ($request->type == 'role-admin') {
            //department group
            $dataRolesAdded = [];
            $departmentGroups = $request->departments ?? [];
            $roleDepartmentGroups = RolesWithType::where('type', Consts::DEPARTMENT_TYPE)
                ->where('type_role', Consts::ROLE_ADMIN_TYPE)->get()->pluck('id_component')->toArray();
            $departmentGroupsPermissionAdded = $departmentGroups ?? [];
            $departmentGroupsPermissionRemoved = [];
            if (!empty($roleDepartmentGroups)) {
                $departmentGroupsPermissionRemoved = array_diff($roleDepartmentGroups, $departmentGroups);
                $departmentGroupsPermissionAdded = array_diff($departmentGroups, $roleDepartmentGroups);
            }
            $departmentGroupInfos = DepartmentGroup::whereIn('id', $departmentGroupsPermissionAdded)->get();
            foreach ($departmentGroupInfos as $item) {
                $dataRolesAdded[] = [
                    'type' => Consts::DEPARTMENT_TYPE,
                    'type_role' => Consts::ROLE_ADMIN_TYPE,
                    'id_component' => $item->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            //user
            $userPermissionAdded = $userIds;
            $userPermissionRemoved = [];
            $userRoleFacilities = RolesWithType::where('type', Consts::EMPLOYEE_TYPE)
                ->where('type_role', Consts::ROLE_ADMIN_TYPE)->get()->pluck('id_component')->toArray();
            if (!empty($userRoleFacilities)) {
                $userPermissionRemoved = array_diff($userRoleFacilities, $userIds);
                $userPermissionAdded = array_diff($userIds, $userRoleFacilities);
            }
            $userInfos = User::whereIn('id', $userPermissionAdded)->where('id', '<>', env('DEFAULT_ADMIN_ID'))->get();
            foreach ($userInfos as $item) {
                $dataRolesAdded[] = [
                    'type' => Consts::EMPLOYEE_TYPE,
                    'type_role' => Consts::ROLE_ADMIN_TYPE,
                    'id_component' => $item->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            // bảng roles
            $userIdByPositionsAdded = DepartmentGroupUser::
            whereIn('department_group_id', $departmentGroupsPermissionAdded)
                ->where('users_id', '<>', env('DEFAULT_ADMIN_ID'))
                ->get()->pluck('users_id')->toArray();
            $userIdByPositionsRemoved = DepartmentGroupUser::
            whereIn('department_group_id', $departmentGroupsPermissionRemoved)
                ->pluck('users_id')->toArray();
            $dataRolesUserRemoved = array_merge($userPermissionRemoved, $userIdByPositionsRemoved) ?? [];
            $userPermissionAdded = array_merge($userPermissionAdded, $userIdByPositionsAdded) ?? [];
            DB::beginTransaction();
            try {
                User::whereIn('id', $dataRolesUserRemoved)
                    ->update(['is_admin' => false, 'role_admin_updated_at' => null]);
                User::whereIn('id', $userPermissionAdded)
                    ->update(['is_admin' => true, 'role_admin_updated_at' => Carbon::now()]);
                // user
                RolesWithType::whereIn('id_component', $userPermissionRemoved)->where('type', Consts::EMPLOYEE_TYPE)
                    ->where('type_role', Consts::ROLE_ADMIN_TYPE)->delete();
                //vị trí
                RolesWithType::whereIn('id_component', $departmentGroupsPermissionRemoved)
                    ->where('type', Consts::DEPARTMENT_TYPE)
                    ->where('type_role', Consts::ROLE_ADMIN_TYPE)->delete();
                RolesWithType::insert($dataRolesAdded);
                $this->updateRoleToZoo();
                DB::commit();
                return back();
            } catch (\Exception $exception) {
                DB::rollBack();
                return back()->withError(Messages::SYSTERM_ERROR)->withInput();
            }
        }
        // done
        if ($request->type == 'role-facility') {
            //vị trí
            $dataRolesAdded = [];
            $positions = $request->positions ?? [];
            $rolePositions = RolesWithType::where('type', Consts::POSITION_TYPE)
                ->where('type_role', Consts::ROLE_FACILITY_TYPE)->get()->pluck('id_component')->toArray();
            $positionPermissionAdded = $positions ?? [];
            $positionPermissionRemoved = [];
            if (!empty($rolePositions)) {
                $positionPermissionRemoved = array_diff($rolePositions, $positions);
                $positionPermissionAdded = array_diff($positions, $rolePositions);
            }
            $positionInfos = Position::whereIn('id', $positionPermissionAdded)->get();
            foreach ($positionInfos as $item) {
                $dataRolesAdded[] = [
                    'type' => Consts::POSITION_TYPE,
                    'type_role' => Consts::ROLE_FACILITY_TYPE,
                    'id_component' => $item->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            //user
            $userPermissionAdded = $userIds;
            $userPermissionRemoved = [];
            $userRoleFacilities = RolesWithType::where('type', Consts::EMPLOYEE_TYPE)
                ->where('type_role', Consts::ROLE_FACILITY_TYPE)->get()->pluck('id_component')->toArray();
            if (!empty($userRoleFacilities)) {
                $userPermissionRemoved = array_diff($userRoleFacilities, $userIds);
                $userPermissionAdded = array_diff($userIds, $userRoleFacilities);
            }
            $userInfos = User::whereIn('id', $userPermissionAdded)->get();
            foreach ($userInfos as $item) {
                $dataRolesAdded[] = [
                    'type' => Consts::EMPLOYEE_TYPE,
                    'type_role' => Consts::ROLE_FACILITY_TYPE,
                    'id_component' => $item->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            // bảng roles
            $userIdByPositionsAdded = PositionUser::whereIn('position_id', $positionPermissionAdded)
                ->get()->pluck('user_id')->toArray();
            $userIdByPositionsRemoved = PositionUser::whereIn('position_id', $positionPermissionRemoved)
                ->pluck('user_id')->toArray();
            $dataRolesUserRemoved = array_merge($userPermissionRemoved, $userIdByPositionsRemoved) ?? [];
            $userPermissionAdded = array_merge($userPermissionAdded, $userIdByPositionsAdded) ?? [];
            $facilityUsers = FacilityUser::whereIn('users_id', $userPermissionAdded)->get();
            $datas = [];
            foreach ($facilityUsers as $facilityUser) {
                $datas[] = [
                    'users_id' => $facilityUser->users_id,
                    'facilities_id' => $facilityUser->facilities_id,
                    'is_clone' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'view' => true,
                    'edit' => true,
                    'position_id' => $facilityUser->position_id
                ];
            }
            DB::beginTransaction();
            try {
                User::whereIn('id', $dataRolesUserRemoved)
                    ->update(['is_facility' => false, 'role_facility_updated_at' => null]);
                User::whereIn('id', $userPermissionAdded)
                    ->update(['is_facility' => true, 'role_facility_updated_at' => Carbon::now()]);
                Role::whereIn('users_id', $dataRolesUserRemoved)->delete();
                Role::insert($datas);
                // user
                RolesWithType::whereIn('id_component', $userPermissionRemoved)->where('type', Consts::EMPLOYEE_TYPE)
                    ->where('type_role', Consts::ROLE_FACILITY_TYPE)->delete();
                //vị trí
                RolesWithType::whereIn('id_component', $positionPermissionRemoved)->where('type', Consts::POSITION_TYPE)
                    ->where('type_role', Consts::ROLE_FACILITY_TYPE)->delete();
                RolesWithType::insert($dataRolesAdded);
                $this->updateRoleToZoo();
                DB::commit();
                return back();
            } catch (\Exception $exception) {
                DB::rollBack();
                return back()->withError(Messages::SYSTERM_ERROR)->withInput();
            }
        }
    }

    public function deleteRole($request)
    {
        $roles = RolesWithType::findOrFail($request->id);
        if ($request->type == 'role-admin') {
            if ($roles->type == Consts::EMPLOYEE_TYPE) {
                DB::beginTransaction();
                try {
                    RolesWithType::where('id', '=', $request->id)->delete();
                    User::where('id', $roles->id_component)->update([
                        'is_admin' => false,
                        'role_admin_updated_at' => null
                    ]);
//                    Role::where('users_id', '=', $roles->id_component)->delete();
                    $this->updateRoleToZoo();
                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
            } else {
                $userIds = DepartmentGroupUser::where('department_group_id', '=', $roles->id_component)
                    ->get()->pluck('users_id')->toArray();
                DB::beginTransaction();
                try {
                    RolesWithType::where('id', '=', $request->id)->delete();
                    User::whereIn('id', $userIds)->update([
                        'is_admin' => false,
                        'role_admin_updated_at' => null
                    ]);
                    $this->updateRoleToZoo();
                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
            }
        }
        if ($request->type == 'role-facility') {
            if ($roles->type == Consts::EMPLOYEE_TYPE) {
                DB::beginTransaction();
                try {
                    RolesWithType::where('id', '=', $request->id)->delete();
                    User::where('id', $roles->id_component)->update([
                        'is_facility' => false,
                        'role_facility_updated_at' => null
                    ]);
                    Role::where('users_id', '=', $roles->id_component)->delete();
                    $this->updateRoleToZoo();
                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
            } else {
                $userIds = PositionUser::where('position_id', '=', $roles->id_component)
                    ->get()->pluck('user_id')->toArray();
                DB::beginTransaction();
                try {
                    RolesWithType::where('id', '=', $request->id)->delete();
                    User::whereIn('id', $userIds)->update([
                        'is_facility' => false,
                        'role_facility_updated_at' => null
                    ]);
                    Role::where('position_id', '=', $roles->id_component)->delete();
                    $this->updateRoleToZoo();
                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                }
            }
        }
        return back();
    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $url = env('URL_ZOO_LOGIN');
        $appId = env('ZOO_APP_ID');
        $appKey = env('ZOO_APP_SECRET');
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
        $body = [
            "app_id" => $appId,
            "app_secret" => $appKey
        ];
        $data = $client->post($url, [
            'headers' => $headers,
            'form_params' => $body,
        ]);
        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);
        $token = $dataContent['data'];

        return $token;
    }

    public function updateRoleToZoo()
    {
        $datas = [];
        $roleAdmins = RolesWithType::where('type', '=', Consts::EMPLOYEE_TYPE)
            ->where('type_role', '=', Consts::ROLE_ADMIN_TYPE)
            ->get()->pluck('id_component')->toArray();
        $roleAdmins = array_merge($roleAdmins, [env('DEFAULT_ADMIN_ID')]);
        $userInfos = User::whereIn('id', $roleAdmins)->get();
        $datasRoleAdmin = [];
        foreach ($userInfos as $user) {
            $datasRoleAdmin[] = (object)[
                "object_name" => $user->name,
                "object_code" => $user->code,
                "object_type" => Consts::EMPLOYEE_TYPE
            ];
        }
        $roleFacilitiess = RolesWithType::where('type', '=', Consts::EMPLOYEE_TYPE)
            ->where('type_role', '=', Consts::ROLE_FACILITY_TYPE)
            ->get()->pluck('id_component')->toArray();
        $userInfos = User::whereIn('id', $roleFacilitiess)->get();
        foreach ($userInfos as $user) {
            $datas[] = (object)[
                "object_name" => $user->name,
                "object_code" => $user->code,
                "object_type" => Consts::EMPLOYEE_TYPE
            ];
        }

        $roleAdmins = RolesWithType::where('type', '=', Consts::DEPARTMENT_TYPE)
            ->where('type_role', '=', Consts::ROLE_ADMIN_TYPE)
            ->get()->pluck('id_component')->toArray();
        $roles = RolesWithType::where('type', '=', Consts::POSITION_TYPE)
            ->where('type_role', '=', Consts::ROLE_FACILITY_TYPE)
            ->get()->pluck('id_component')->toArray();
        $postion = Position::whereIn('id', $roles)->get();
        $departmentGroup = DepartmentGroup::whereIn('id', $roleAdmins)->get();
        foreach ($postion as $data) {
            $datas[] = (object)[
                "object_name" => $data->name,
                "object_code" => $data->code,
                "object_type" => Consts::POSITION_TYPE
            ];
        }
        foreach ($departmentGroup as $data) {
            $datasRoleAdmin[] = (object)[
                "object_name" => $data->name,
                "object_code" => $data->code,
                "object_type" => Consts::DEPARTMENT_TYPE
            ];
        }
        $client = new \GuzzleHttp\Client();
        $url = env('URL_ZOO_UPDATE_ROLE');
        $body = (object)[
            'roles' => [(object)[
                "role_id" => 1,
                'name' => '管理者権限',
                'data' => $datasRoleAdmin
            ],
                (object)[
                    "role_id" => 2,
                    'name' => '利用者権限',
                    'data' => $datas
                ]
            ]
        ];
        $token = $this->getToken();
        $headers = [
            'token' => $token['token'],
        ];
        $result = $client->put($url, [
            'headers' => $headers,
            'form_params' => $body,
        ]);
    }

    public function getDataRoleAdmin()
    {
        $users = User::all()->pluck('name', 'id')->toArray();
        $usersSelect = RolesWithType::where('type', '=', Consts::EMPLOYEE_TYPE)
            ->where('type_role', '=', Consts::ROLE_ADMIN_TYPE)->get()->pluck('id_component')->toArray();
        $departments = DepartmentGroup::select('name', 'id', 'code')->get()->toArray();
        $departmentsSelect = RolesWithType::where('type', '=', Consts::DEPARTMENT_TYPE)
            ->where('type_role', '=', Consts::ROLE_ADMIN_TYPE)->get()->pluck('id_component')->toArray();
        return [
            'users' => $users,
            'list_department' => $departments,
            'usersSelect' => $usersSelect,
            'departmentsSelect' => $departmentsSelect
        ];
    }
    public function getDataRoleFacility()
    {
        $users = User::all()->pluck('name', 'id')->toArray();
        $usersSelect = RolesWithType::where('type', '=', Consts::EMPLOYEE_TYPE)
            ->where('type_role', '=', Consts::ROLE_FACILITY_TYPE)->get()->pluck('id_component')->toArray();
        $positions = Position::all()->pluck('name', 'id')->toArray();
        $positionsSelect = RolesWithType::where('type', '=', Consts::POSITION_TYPE)
            ->where('type_role', '=', Consts::ROLE_FACILITY_TYPE)->get()->pluck('id_component')->toArray();
        return [
            'users' => $users,
            'list_position' => $positions,
            'usersSelect' => $usersSelect,
            'positionsSelect' => $positionsSelect
        ];
    }
}
