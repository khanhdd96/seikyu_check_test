<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\FileInInsurance;
use App\Models\Facility;
use Illuminate\Support\Facades\DB;
use App\Messages;
use Carbon\Carbon;

class FileInInsuranceService
{
    public function __construct(FileInInsurance $fileInInsurance)
    {
        $this->fileInInsurance =  $fileInInsurance;
    }

    public function index($search)
    {
        $results = $this->fileInInsurance;
        if ($search) {
            $results = $results->where('company_name', 'like', '%'.$search.'%');
        }

        return $results->orderBy('updated_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
    }

    public function store($request)
    {
        try {
            $this->fileInInsurance->create([
                'company_name' => $request['createCompanyName'],
                'fiduciary_goal' => $request['createFiduciaryGoal'],
                'date_update' => Carbon::now()
            ]);

            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            return [
                'code' => 500
            ];
        }
    }

    public function show($id)
    {
        return $this->fileInInsurance->findOrFail($id);
    }

    public function update($request)
    {
        try {
            $this->fileInInsurance->findOrFail($request['id'])->update([
                'company_name' => $request['editCompanyName'],
                'fiduciary_goal' => $request['editFiduciaryGoal'],
                'date_update' => Carbon::now()
            ]);
            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            return [
                'code' => 500
            ];
        }
    }

    public function destroy($id)
    {
        return $this->fileInInsurance->findOrFail($id)->delete();
    }
}
