<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Http\Services\ReadFilter\FilterNo9;

class CheckNo9Service
{

    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
    }
    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
        }
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_9)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_9;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError($request, $typeCheck->id);
            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            $file = $response['file'];
            if ($errors) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $settingMonth->count_file_done = 0;
                $typeCheck->save();
            } else {
                $settingMonth->count_file_done += 1;
            }
            $settingMonth->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($file->filepath);
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        return response()->json([
            'error' => false,
            'data' => [
                'facilities' => $errors,
                'file' => $file->id,
                'file_name' => $request->file->getClientOriginalName(),
                'count_errors' => $response['count_errors']
            ]
        ], 200);
    }
    public function checkError($request, $typeCheckId)
    {

        $fileUpload = $request->file('file');
        $path = $fileUpload->getRealPath();
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);
        $filterSubset = new FilterNo9();
        $reader->setReadFilter($filterSubset);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);

        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $extension = strtolower($request->file->getClientOriginalExtension());
        if ($extension == 'csv') {
            if (($fh = fopen($path, 'r')) !== false) {
                while (($data = fgetcsv($fh)) !== false) {
                    $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                }
                fclose($fh);
            }
            $sheetData = $professors;
        }
        $name = time() . $fileUpload->getClientOriginalName();
        $filePath = 'files/' . $name;
        try {
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
            $filePath = env('AWS_URL') . $filePath;
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);
            return response()->json([
                'error' => true,
                'message' => Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $errors = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        /**  Loop through all the remaining files in the list  **/
        $facilitiesNameError = [];
        for ($row = 1; $row < count($sheetData); $row++) {
            $facilityName = $sheetData[$row][1];
            if ($sheetData[$row][7] == Consts::TYPE_CHECK_9_STATUST) {
                if (!in_array($facilityName, $facilitiesNameError)) {
                    $errors[] = [
                        'error_position' => $sheetData[$row][1],
                        'error_code' => 'A90001',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A90001'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                }
                $facilitiesNameError[] = $facilityName;
            }
        }
        if ($errors) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }
        $facilitiesNameError = array_count_values($facilitiesNameError);
        $errorsWithCount = [];
        foreach ($errors as $error) {
            foreach ($facilitiesNameError as $key => $value) {
                if ($error['error_position'] == $key) {
                    $error['error_count'] = $value;
                    $errorsWithCount[] = $error;
                    break;
                }
            }
        }
        return ['errors' => $errors, 'file' => $file, 'count_errors' => $errorsWithCount];
    }
}
