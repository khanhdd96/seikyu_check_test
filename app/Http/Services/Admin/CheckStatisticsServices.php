<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FileCheckService;
use App\Models\Department;
use App\Models\Error;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use App\Models\User;

class CheckStatisticsServices
{
    protected $user;
    protected $settingMonth;
    protected $facilityService;
    protected $facility;
    protected $department;
    protected $typeCheck;
    protected $file;
    protected $fileCheckService;
    protected $error;

    public function __construct(
        User $user,
        SettingMonth $settingMonth,
        Facility $facility,
        Department $department,
        TypeCheck $typeCheck,
        File $file,
        FileCheckService $fileCheckService,
        Error $error
    ) {
        $this->user = $user;
        $this->settingMonth = $settingMonth;
        $this->facility = $facility;
        $this->department = $department;
        $this->typeCheck = $typeCheck;
        $this->file = $file;
        $this->fileCheckService = $fileCheckService;
        $this->error = $error;
    }

    public function index($request)
    {
        $dateMonth = date('Y/m');
        if (isset($request->month) && $request->month) {
            $dateMonth = $request->month;
        }

        $year = date('Y');
        if (isset($request->year) && $request->year) {
            $year = $request->year;
        }
        $facilitys = $this->facility->where('is_active', 1)->get();
        $facilityIds = $facilitys->pluck('id')->toArray();
        $departmentIds = array_unique($facilitys->pluck('departments_id')->toArray());
        $departments = $this->department->whereIn('id', $departmentIds)->get();
        if ($request->department_id && isset($request->department_id) && !is_array($request->department_id)) {
            $facilitys = Facility::where('departments_id', '=', $request->department_id)
                ->whereIn('id', $facilityIds)->where('is_active', '=', true)->get();
        }
        if ($request->department_id && is_array($request->department_id)) {
            $facilitys = Facility::whereIn('departments_id', $request->department_id)
                ->whereIn('id', $facilityIds)->where('is_active', '=', true)->get();
        }

        $facilityIds = $facilitys->pluck('id')->toArray();
        if ($request->facility_id && isset($request->facility_id) && !is_array($request->department_id)) {
            $facilityIds = [$request->facility_id];
        }
        if ($request->facility_id && is_array($request->facility_id)) {
            $facilityIds = $request->facility_id;
        }

        //overview
        $typeChecks = [];
        if (!$request->type || $request->type == 'overview') {
            $typeChecks = $this->getErrorDataType($facilityIds, $dateMonth);
        }
        //Detail
        $getDetailErrors = null;
        if ($request->type == 'detail') {
            $getDetailErrors = $this->getDetailErrors($facilityIds, $year, $request);
        }

        // comparison
        $getComparisonErrors = ['names' => [], 'month' => []];
        if ($request->type == 'comparison') {
            $getComparisonErrors = $this->getComparisonErrors($facilityIds, $year, $request, $departmentIds);
        }
        $data = [
            'departments' => $departments,
            'facilitys' => $facilitys,
            'typeChecks' => $typeChecks,
            'detailErrors' => $getDetailErrors,
            'comparisonErrors' => $getComparisonErrors,
        ];

        return $data;
    }

    public function getErrorDataType($facilityIds, $dateMonth)
    {
        $settingMonth = $this->settingMonth->whereIn('facilities_id', $facilityIds)
            ->where('year_month', $dateMonth)->get();
        $settingMonthIds = $settingMonth->pluck('id')->toArray();
        $typeChecks = $this->typeCheck->whereIn('setting_months_id', $settingMonthIds)
            ->withCount('fileNotHold')->withCount('fileError')->with('fileNotHold')->withCount('files')->get();
        $newTypeChecks = [];
        $listTypeMenus = Consts::TYPE_MENU;
        foreach ($listTypeMenus as $value) {
            $newTypeChecks[$value] = 0;
            foreach ($typeChecks as $typeCheck) {
                if ($value == $typeCheck['code_check']) {
                    $newTypeChecks[$value] += $typeCheck['file_error_count'];
                }
            }
            if ($value == Consts::TYPE_CHECK_NUMBER_3 || $value == Consts::TYPE_CHECK_NUMBER_5_M) {
                $newTypeChecks[$value] = $newTypeChecks[$value] / 2;
            }
        }

        return $newTypeChecks;
    }

    public function getDetailErrors($facilityIds, $year, $request)
    {
        $codeCheck = Consts::TYPE_MENU[6];
        if (isset($request->type_check) && $request->type_check) {
            $codeCheck = $request->type_check;
        }

        $codeErrors = Consts::LIST_CONST_CHECK_CODE[$codeCheck];
        if (isset($request->code_error) && $request->code_error) {
            $codeErrors = [$request->code_error => ''];
        }

        $dataMonth = [];
        $typeCheck = $this->typeCheck->whereIn('facilities_id', $facilityIds)
            ->where('year_month_check', 'like', '%'.$year.'%')
            ->where('code_check', $codeCheck)
            ->with('files', function ($query) {
                $query->with('errors');
            })
            ->get();

        $newTypeCheck = [];
        foreach ($typeCheck as $key => $value) {
            foreach ($value['files'] as $file) {
                $newTypeCheck[$value['year_month_check']][] = $file->errors;
            }
        }
        $dataMonth = [];
        for ($i = 1; $i <= 12; ++$i) {
            $month = $year.'/'.sprintf('%02d', $i);
            $dataMonth[$i] = $this->getDataErrorMonth($newTypeCheck, $month, $codeErrors, $i, $year);
        }

        $dataYear = [];
        $month = [];
        foreach ($codeErrors as $key => $value) {
            $dataYear[$key] = 0;
            $month[$key][] = 0;
            foreach ($dataMonth as $key2 => $value2) {
                $dataYear[$key] += $value2[$key];
                $month[$key][$key2] = $value2[$key];
            }
        }

        return ['month' => $month, 'year' => $dataYear];
    }

    public function getComparisonErrors($facilityIds, $year, $request, $departmentIds)
    {
        $facilitys = $this->facility->whereIn('id', $facilityIds)->get();
        $dataMonth = [];
        if (!empty($request->department_id)) {
            $departmentIds = $request->department_id;
        }

        $typeCheck = $this->typeCheck->whereIn('facilities_id', $facilityIds)
            ->where('year_month_check', 'like', '%'.$year.'%');

        if (isset($request->type_check) && $request->type_check) {
            $typeCheck = $typeCheck->where('code_check', $request->type_check);
        }

        if ($request->code_error && !empty($request->code_error)) {
            $typeCheck = $typeCheck->with('files', function ($query) use ($request) {
                $query->with(['errors' => function ($query) use ($request) {
                    $query->whereIn('error_code', $request->code_error);
                }]);
            })->get();
        } else {
            $typeCheck = $typeCheck->with('files', function ($query) {
                $query->withCount('errors');
            })->get();
        }

        $newTypeCheck = [];
        foreach ($typeCheck as $key => $value) {
            foreach ($value->files as $file) {
                $newTypeCheck[$value['facilities_id']][$value['year_month_check']][] = $file->errors->count();
            }
        }

        $dataMonth = [];
        foreach ($facilityIds as $value) {
            $dataMonth[$value] = [];
            if (!isset($newTypeCheck[$value])) {
                $newTypeCheck[$value] = [];
            }
            $dataMonth[$value] = $this->getDataComparisonMonth($newTypeCheck[$value], $year);
        }

        $names = $facilitys->pluck('name')->toArray();
        $departmentFacilitys = $facilitys->pluck('departments_id', 'id')->toArray();
        if (count($departmentIds) > 1 && (!$request->facility_id || count($request->facility_id) == 0)) {
            $names = $this->department->whereIn('id', $departmentIds)->pluck('name');
            $dataMonth = $this->getDepartmentDataMonth($dataMonth, $departmentFacilitys, $departmentIds);
        }

        return ['names' => $names, 'month' => $dataMonth];
    }

    public function countErrorCode($code, $fileIds)
    {
        $count = 0;
        $count = $this->error->whereIn('files_id', $fileIds)->where('error_code', $code)->count();

        return $count;
    }

    public function getListErrorCode($request)
    {
        $html = '<option value="">全て</option>';
        $codeErrors = Consts::LIST_CONST_CHECK_CODE[$request->type_check];
        foreach ($codeErrors as $key => $value) {
            $html .= '<option value="'.$key.'">'.$value.'</option>';
        }

        return $html;
    }

    public function getDataErrorMonth($newTypeCheck, $month, $codeErrors, $m, $year)
    {
        $mNow = date('m');
        $yNow = date('Y');
        foreach ($codeErrors as $key => $value) {
            $codeErrors[$key] = 0;
            if ($mNow + 1 < $m  && $year == $yNow) {
                $codeErrors[$key] = null;
            }
        }

        if (isset($newTypeCheck[$month])) {
            foreach ($newTypeCheck[$month] as $key => $value) {
                foreach ($value as $error) {
                    if ($error['error_code']!= " ") {
                        ++$codeErrors[$error['error_code']];
                    }
                }
            }
        }
        return $codeErrors;
    }

    public function getDataComparisonMonth($newTypeCheck, $year)
    {
        $mNow = date('m');
        $yNow = date('Y');
        $dataMonth = [];
        $dataMonth[0] = 0;
        for ($i = 1; $i <= 12; ++$i) {
            $month = $year.'/'.sprintf('%02d', $i);
            $dataMonth[$i] = isset($newTypeCheck[$month]) ? array_sum($newTypeCheck[$month]) : 0;
            if ($mNow + 1 < $i && $yNow == $year) {
                $dataMonth[$i] = null;
            }
        }

        return $dataMonth;
    }

    public function getDepartmentDataMonth($dataMonth, $departmentFacilitys, $departmentIds)
    {
        $data = [];
        // dd($departmentFacilitys, $dataMonth);
        foreach ($departmentFacilitys as $key => $value) {
            foreach ($dataMonth as $key2 => $facilitys) {
                if ($key == $key2 && !empty($facilitys)) {
                    $data[$value][] = $facilitys;
                }
            }
        }

        foreach ($data as $key => $value) {
            $newData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($value as $key2 => $value2) {
                foreach ($value2 as $key3 => $value3) {
                    if ($value3 === null) {
                        $newData[$key3] = null;
                    } else {
                        $newData[$key3] += $value3;
                    }
                }
            }
            $data[$key] = $newData;
        }

        $return = [];
        foreach ($departmentIds as $departmentId) {
            $return[$departmentId] = [];
            foreach ($data as $key => $value) {
                if ($departmentId == $key) {
                    $return[$departmentId] = $value;
                }
            }
        }

        return $return;
    }
}
