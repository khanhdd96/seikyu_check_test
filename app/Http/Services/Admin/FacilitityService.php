<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FileCheckService;
use App\Messages;
use App\Models\Department;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Services\SendMailService;

class FacilitityService
{
    protected $user;
    protected $settingMonth;
    protected $facility;
    protected $department;
    protected $typeCheck;
    protected $file;
    protected $fileCheckService;
    protected $sendMailService;

    public function __construct(
        User $user,
        SettingMonth $settingMonth,
        Facility $facility,
        Department $department,
        TypeCheck $typeCheck,
        File $file,
        FileCheckService $fileCheckService,
        SendMailService $sendMailService
    ) {
        $this->user = $user;
        $this->settingMonth = $settingMonth;
        $this->facility = $facility;
        $this->department = $department;
        $this->typeCheck = $typeCheck;
        $this->file = $file;
        $this->fileCheckService = $fileCheckService;
        $this->sendMailService = $sendMailService;
    }

    public function show($request, $id)
    {
        $facility = $this->facility->find($id);
        $data = [
            'facilitys' => $facility,
            'departments' => null,
            'facility' => null,
            'department' => null,
            'type_checks' => null,
            'setting_month' => null,
            'data_type_check_detail' => null,
            'canUpload' => null,
        ];
        $getDetail = $this->getDetailFacility($facility, $request);
        $data = [
            'facilitys' => $facility,
            'departments' => null,
            'facility' => $getDetail['facility'],
            'department' => $getDetail['department'],
            'type_checks' => $getDetail['type_checks'],
            'setting_month' => $getDetail['setting_month'],
            'data_type_check_detail' => $getDetail['dataTypeCheckDetail'],
            'canUpload' => $getDetail['canUpload'],
        ];

        return $data;
    }

    public function getDetailFacility($facility, $request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = date('Y/m');
        if ($request->setting_month) {
            $date = $request->setting_month;
        }

        if ($request->facility_id) {
            $facility = $this->facility->find($request->facility_id);
        }
        $department = $this->department->find($facility->departments_id);
        $settingMonth = $facility->settingMonth()->where('year_month', 'like', $date)->first();
        if (!$settingMonth) {
            $settingMonth = new SettingMonth();
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $date;
            $settingMonth->facilities_id = $facility->id;
            $settingMonth->save();
        }
        $typeChecks = $facility->list_type_check;
        // get type Check detail
        $dataTypeCheckDetail = $this->getDataTypeCheckDetail($facility->id, $typeChecks, $request);
        $typeChecks = $this->typeCheck->where('setting_months_id', $settingMonth->id)
            ->withCount('fileNotHold')
            ->withCount('fileError')
            ->withCount(['checkTimes', 'checkErrorTimes'])
            ->with('fileNotHold')
            ->with('files')->get();
        $newTypeChecks = [];
        //conUplaod
        $canUpload = $this->checkCanUpload($settingMonth, $date, $facility->id);
        foreach ($typeChecks as $typeCheck) {
            $newTypeChecks[$typeCheck->code_check] = $typeCheck;
        }

        return [
            'facility' => $facility,
            'department' => $department,
            'type_checks' => $newTypeChecks,
            'setting_month' => $settingMonth,
            'dataTypeCheckDetail' => $dataTypeCheckDetail,
            'canUpload' => $canUpload,
        ];
    }

    public function getDataTypeCheckDetail($facility_id, $typeChecks, $requets)
    {
        $month = date('Y/m');
        $requets->month = $month;
        $requets->facilities_id = $facility_id;
        if (isset($requets->setting_month)) {
            $requets->month = $requets->setting_month;
        }
        $typeChecks = explode(',', $typeChecks);
        $data = [];
        foreach ($typeChecks as $typeCheck) {
            $requets->type_check = $typeCheck;
            $data[$typeCheck] = $this->getTypeCheckDetail($requets);
        }

        return $data;
    }

    public function editDeadline($request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = date('Y/m');
        $settingMonth = $this->settingMonth->where('facilities_id', $request->facilityId)
            ->where('year_month', 'like', $date)->first();
        if (!$settingMonth) {
            $settingMonth = new SettingMonth();
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $date;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $settingMonth->deadline = $request->seting_deadline;
        $settingMonth->can_upload = true;
        try {
            DB::beginTransaction();
            $settingMonth->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [],
        ], 200);
    }

    public function editStatus($request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = $request->facilityYearMonth;
        $settingMonth = $this->settingMonth->where('facilities_id', $request->facilityId)
            ->where('year_month', 'like', $date)->first();
        if (!$settingMonth) {
            $settingMonth = new SettingMonth();
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $date;
            $settingMonth->facilities_id = $request->facilityId;
            $settingMonth->save();
        }
        $oldStatusFacility = $settingMonth->status_facilitity;
        $settingMonth->status_facilitity = $request->status_facility_update;
        DB::beginTransaction();
        try {
            $settingMonth->save();
            if ($oldStatusFacility == Consts::STATUS_CHECK_FILE['hold'] &&
                ($settingMonth->status_facilitity == Consts::STATUS_CHECK_FILE['success'] ||
                $settingMonth->status_facilitity == Consts::STATUS_CHECK_FILE['error'])
            ) {
                $this->sendMailService->sendMailAdminUpdateType($settingMonth, $settingMonth->status_facilitity);
                // $this->sendMailService->sendMailReserve($settingMonth);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [],
        ], 200);
    }

    public function openStatusUpload($request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = date('Y/m');
        if ($request->month) {
            preg_match_all('!\d+!', $request->month, $matches);
            $date = $matches[0][0].'/'.$matches[0][1];
        }
        $settingMonth = $this->settingMonth->where('facilities_id', $request->facility_id)
            ->where('year_month', 'like', $date)->first();
        if (!$settingMonth) {
            $settingMonth = new SettingMonth();
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $date;
            $settingMonth->facilities_id = $request->facility_id;
            $settingMonth->save();
        }

        if ($settingMonth->can_upload == Consts::CAN_UPLOAD['active']) {
            $settingMonth->can_upload = Consts::CAN_UPLOAD['not-active'];
        } else {
            $settingMonth->can_upload = Consts::CAN_UPLOAD['active'];
        }
        DB::beginTransaction();
        try {
            $settingMonth->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [
                'facilities_id' => $request->facility_id,
                'can_upload' => $this->checkCanUpload($settingMonth, $date, $request->facility_id),
            ],
        ], 200);
    }

    public function checkCanUploadHomeAdmin()
    {
    }

    public static function checkCanUpload($settingMonth, $month, $facilityId)
    {

        $canUpload = false;
        $now = Carbon::now()->format('Y-m-d');
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        if (Auth::user()->is_admin != Consts::ADMIN) {
            $role = Role::where('users_id', '=', $userId)->where('facilities_id', '=', $facilityId)->firstOrFail();
            if (!$role->view) {
                return redirect()->route('login');
            }
        }

        if ($settingMonth) {
            $canUpload = $settingMonth->can_upload;
        } else {
            $canUpload = true;
        }
        if (date('Y/m') != $month && !$settingMonth) {
            $canUpload = false;
        }
        return $canUpload;
    }

    public function getTypeCheckDetail($request)
    {
        $type = $request->type_check;
        $month = $request->month ?? Carbon::now()->format('Y/m');
        $facilityId = $request->facilities_id;
        $settingMonth = $this->settingMonth->where('facilities_id', '=', $facilityId)
            ->where('year_month', '=', $month)->first();
        $files = [];
        $typeCheck = [];
        $fileCount = 0;
        if ($settingMonth) {
            $typeCheck = $settingMonth->typeCheck($type)->first();
            if ($typeCheck) {
                $fileCount = $files = $typeCheck->files()->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])
                                                            ->count();
                $files = $typeCheck->files()->with('errors')->withCount('errors');
                if ($type == Consts::TYPE_CHECK_UPLOAD['3']) {
                    $files = $files->with('file')->with('file', function ($query) {
                        $query->with('errors')->withCount('errors');
                    })->where('file_id', '=', null);
                }
                $files = $files->orderBy('created_at', 'DESC')
                    ->paginate(Consts::BASE_PAGE_SIZE);
            }
        }
        $canUpload = $this->checkCanUpload($settingMonth, $month, $facilityId);

        return [
            'files' => $files,
            'typeCheck' => $typeCheck,
            'settingMonth' => $settingMonth,
            'facilityId' => $facilityId,
            'month' => $month,
            'fileCount' => $fileCount,
            'canUpload' => $canUpload,
        ];
    }

    public function changeActiveStatus($request)
    {
        $facility = $this->facility->findOrFail($request->id);
        $facility->is_active = $facility->is_active ? 0 : 1;
        $facility->save();

        return response()->json([], 200);
    }
}
