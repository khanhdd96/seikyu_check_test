<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\MasterCode;
use App\Models\MasterSubCode;
use Illuminate\Support\Facades\DB;
use App\Messages;
use Carbon\Carbon;

class MasterCodeService
{
    public function __construct(MasterCode $masterCode, MasterSubCode $masterSubCode)
    {
        $this->masterCode =  $masterCode;
        $this->masterSubCode = $masterSubCode;
    }

    public function index($request)
    {
        $results = $this->masterCode->withCount('masterSubCode');

        if ($request->search) {
            $search = "\\" . $request->search;
            $results = $results->where('code_name', 'like', '%' . $search . '%');
        }
        if ($request->type !== null) {
            $type = $request->type != -1 ? $request->type : null;
            $results = $results->where('type', '=', $type);
        }
        return $results->orderBy('updated_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
    }

    public function store($request)
    {
        DB::beginTransaction();
        try {
            $masterCode = $this->masterCode->create([
                'code_name' => $request['createCodeName'],
                'type' => $request['type'],
                'date_update' => Carbon::now()
            ]);

            $subCode = [];
            if ($request['createSubCodeName']) {
                $types = $request['createSubCodeType'];
                $subCodes = [];
                $typeCode = [];
                foreach ($request['createSubCodeName'] as $key => $subCodeName) {
                    if (!in_array($subCodeName, $subCodes)) {
                        $subCode[] = [
                            'sub_code_name' => $subCodeName,
                            'master_code_id' => $masterCode->id,
                            'type' => $types[$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                        $subCodes[] = $subCodeName;
                        $typeCode[] = [
                            'code_name' => $subCodeName,
                            'type' => $types[$key]
                        ];
                    } else {
                        $checked = false;
                        foreach ($typeCode as $code) {
                            if ($code['code_name'] == $subCodeName && $code['type'] == $types[$key]) {
                                $checked = true;
                                break;
                            }
                        }
                        if ($checked == false) {
                            $subCode[] = [
                                'sub_code_name' => $subCodeName,
                                'master_code_id' => $masterCode->id,
                                'type' => $types[$key],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];
                            $typeCode[] = [
                                'code_name' => $subCodeName,
                                'type' => $types[$key]
                            ];
                        }
                    }
                }
                $this->masterSubCode->insert($subCode);
            }
            DB::commit();
            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 500
            ];
        }
    }

    public function show($id)
    {
        return $this->masterCode->with('masterSubCode')->where('id', $id)->first();
    }

    public function update($request)
    {
        DB::beginTransaction();
        try {
            $this->masterCode->findOrFail($request['id'])->update([
                'code_name' => $request['editCodeName'],
                'date_update' => Carbon::now(),
                'type' => $request['editCodeType']
            ]);

            $this->masterSubCode->whereMasterCodeId($request['id'])->delete();
            if ($request['editSubCodeName']) {
                $types = $request['editSubCodeType'];
                $subCodes = [];
                $typeCode = [];
                $subCode = [];
                foreach ($request['editSubCodeName'] as $key => $subCodeName) {
                    if (!in_array($subCodeName, $subCodes)) {
                        $subCode[] = [
                            'sub_code_name' => $subCodeName,
                            'master_code_id' => $request['id'],
                            'type' => $types[$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                        $subCodes[] = $subCodeName;
                        $typeCode[] = [
                            'code_name' => $subCodeName,
                            'type' => $types[$key]
                        ];
                    } else {
                        $checked = false;
                        foreach ($typeCode as $code) {
                            if ($code['code_name'] == $subCodeName && $code['type'] === $types[$key]) {
                                $checked = true;
                                break;
                            }
                        }
                        if ($checked == false) {
                            $subCode[] = [
                                'sub_code_name' => $subCodeName,
                                'master_code_id' => $request['id'],
                                'type' => $types[$key],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];
                            $typeCode[] = [
                                'code_name' => $subCodeName,
                                'type' => $types[$key]
                            ];
                        }
                    }
                }
                $this->masterSubCode->insert($subCode);
            }
            DB::commit();
            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'code' => 500
            ];
        }
    }

    public function destroy($id)
    {
        $masterCode = $this->masterCode->findOrFail($id);
        if ($masterCode->masterSubCode()->exists()) {
            return [
                'code' => 400
            ];
        }

        $masterCode->delete();

        return [
            'code' => 200
        ];
    }
}
