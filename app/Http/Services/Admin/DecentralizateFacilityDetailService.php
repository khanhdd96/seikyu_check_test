<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DecentralizateFacilityDetailService
{
    public function __construct(
        Facility $model
    ) {
        $this->model = $model;
    }
    public function index($request)
    {
        $facilityId = $request->id;
        $facility =  $this->model->findOrFail($facilityId);
        $search = $request->search;
        $userRols = $this->model->where('id', '=', $facilityId)->first()->roles()
            ->with(['user'])->with(['position']);
        if ($search) {
            $userRols = $userRols->whereHas('user', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        }
        $userRols = $userRols->orderBy('is_clone', 'ASC')->orderBy('created_at', 'DESC');
        $userRols = $userRols->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        $userSelect = $this->model->where('id', '=', $facilityId)->first()->roles()->pluck('users_id')->toArray();
        $users = User::where('is_facility', '=', true)->whereNotIn('id', $userSelect)->get();
        return [
            'facility' => $facility,
            'userRols' => $userRols,
            'users' => $users,
            'countRole' => count($userSelect)
        ];
    }
    public function update($request)
    {
        $views = $request->view ?? [];
        $edits = $request->edit ?? [];
        $idRoles = $request->id_roles;
        $hasError = false;
        DB::beginTransaction();
        try {
            foreach ($idRoles as $idRole) {
                $role = Role::find($idRole);
                $role->view = isset($views[$idRole]) ? true : false;
                $role->edit = isset($edits[$idRole]) ? true : false;
                $role->save();
            }
            DB::commit();
        } catch (\Exception $exception) {
            $hasError = true;
            DB::rollBack();
        }
        return $hasError;
    }
    public function store($id, $request)
    {
        $view = $request->view ? true : false;
        $edit = $request->edit ? true : false;
        $users = $request->users ?? [];
        $hasError = false;
        foreach ($users as $user) {
            $datas[] = [
                'users_id' => $user,
                'facilities_id' => $id,
                'is_clone' => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'view' => $view,
                'edit' => $edit,
            ];
        }
        DB::beginTransaction();
        try {
            if (isset($datas) && $datas) {
                Role::insert($datas);
            }
            DB::commit();
        } catch (\Exception $exception) {
            $hasError = true;
            DB::rollBack();
        }
        return $hasError;
    }

    public function delete($request)
    {
        Role::destroy($request->id);
        return back();
    }
}
