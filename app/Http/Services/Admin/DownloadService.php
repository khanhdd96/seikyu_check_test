<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FileCheckService;

use App\Models\Department;
use App\Models\Error;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Zip;

class DownloadService
{
    protected $user;
    protected $settingMonth;
    protected $facilityService;
    protected $facility;
    protected $department;
    protected $typeCheck;
    protected $file;
    protected $fileCheckService;
    protected $error;

    public function __construct(
        SettingMonth $settingMonth,
        Facility $facility,
        Department $department,
        TypeCheck $typeCheck,
        File $file,
        FileCheckService $fileCheckService,
        Error $error
    ) {
        $this->settingMonth = $settingMonth;
        $this->facility = $facility;
        $this->department = $department;
        $this->typeCheck = $typeCheck;
        $this->file = $file;
        $this->fileCheckService = $fileCheckService;
        $this->error = $error;
    }
    public function index($request)
    {
        $month = $request->month ?? date('Y/m');
        $facilitys = $this->facility->get();
        $facilityIds = $facilitys->pluck('id')->toArray();
        $departmentIds = array_unique($facilitys->pluck('departments_id')->toArray());
        $departments = $this->department->whereIn('id', $departmentIds)->get();
        if (isset($request->department_id) && $request->department_id) {
            $facilitys = $this->facility->where('departments_id', '=', $request->department_id)
                ->whereIn('id', $facilityIds)->get();
        }
        $facilityIds = $facilitys->pluck('id')->toArray();
        if (isset($request->facility_id) && $request->facility_id) {
            $facilityIds = [$request->facility_id];
        }
        $typeChecks = $this->typeCheck->whereStatus(Consts::STATUS_CHECK_FILE['success'])
                           ->whereYearMonthCheck($month)->whereIn('facilities_id', $facilityIds);
        if (isset($request->type_check) && $request->type_check) {
            $typeChecks = $typeChecks->whereCodeCheck($request->type_check);
        }
        $files = $typeChecks->whereHas('fileSuccessLatest')->with(['fileSuccessLatest' => function ($query) {
            $query->with('files');
        }])->with('facility')->orderBy('updated_at', 'DESC')->paginate(count(Consts::TYPE_MENU));
        return [
            'departments' => $departments,
            'facilitys' => $facilitys,
            'files' => $files
        ];
    }

    public function downloadAll($request)
    {
        $files = $this->file->whereIn('id', $request->file)->orWhereIn('file_id', $request->file)->get();
        $filePaths = $files->pluck('filepath')->toArray();
        $zipName = 'Seikyu-Check' . date('Y/m/d') . '.zip';
        $zip = Zip::create($zipName);
        foreach ($filePaths as $filePath) {
            $arrayFileName = explode('/', $filePath);
            $zip->add($filePath, end($arrayFileName));
        }
        return $zip;
    }

    public function downloadAllAjax($request)
    {
        return $this->file->whereIn('id', $request->file)->orWhereIn('file_id', $request->file)
            ->pluck('filepath')->toArray();
    }
}
