<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\Department;
use App\Models\Facility;
use Illuminate\Support\Facades\DB;
use App\Messages;

class DepartmentService
{
    public function __construct(
        Department $model
    ) {
        $this->model = $model;
    }

    public function index($request)
    {
        $result = $this->model->withCount('facilitieActive')
            ->withCount('facilities')->having('facilities_count', '>', 0);
        $search = $request->search;
        if ($search) {
            $result = $result->where('name', 'like', '%'.$search.'%');
        }
        $result = $result->orderBy('created_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        return $result;
    }

    public function edit($id, $request)
    {
        $department = $this->model->withCount('facilities')->whereId($id)->first();
        $result = $department->facilities();
        $search = $request->search;
        if ($search) {
            $result = $result->where('name', 'like', '%'.$search.'%');
        }
        $result = $result->orderBy('created_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        return [
            'department' => $department,
            'facilities' => $result
            ];
    }

    public function departmentStatus($request)
    {
        $facility = Facility::where('departments_id', $request->id);
        DB::beginTransaction();
        try {
            if ($request->status == 'true') {
                $facility->update(['is_active' => 1]);
            } else {
                $facility->update(['is_active' => 0]);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        return response()->json([
            'error' => false,
            'data' => [],
        ], 200);
    }

    public function getListFacility($request)
    {
        $isActive = $request->type == 'active';
        $facilities = Facility::whereDepartmentsId($request->department_id)->whereIsActive($isActive)
            ->select('id', 'name', 'created_at')->get();
        foreach ($facilities as $facility) {
            $facility->create = date('Y/m/d', strtotime($facility->created_at));
        }
        return response()->json([
            'data' => $facilities
        ]);
    }
}
