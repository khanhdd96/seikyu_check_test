<?php

namespace App\Http\Services\Admin;

use App\Models\HistoryMail;

class HistoryMailService
{
    protected $model;
    public function __construct(HistoryMail $model)
    {
        $this->model = $model;
    }
    public function create($alert)
    {
        $this->model->create([
            'mail_setting_id' => $alert['id'],
            'user_ids' => $alert['user_ids'],
            'grourp_ids' => $alert['grourp_ids'],
            'facilitity_ids' => $alert['facilitity_ids'],
            'start_date' => $alert['start_date'],
            'time' => $alert['time'],
            'email_recipients' => $alert['recipients'],
            'datetime' => $alert['datetime'],
            'list_mail' => $alert['list_mail_send']
        ]);
    }
}
