<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\TemplateMail;

class TemplateMailService
{
    protected $service;

    public function __construct(TemplateMail $model)
    {
        $this->model = $model;
    }

    public function index($request)
    {
        $keyWord = $request->key_word;
        $results = $this->model;
        if ($keyWord) {
            $results = $results->where('title', 'like', "%\\{$keyWord}%");
        }
        $results = $results->orderBy('updated_at', "DESC")
                            ->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        return [
            'data' => $results,
        ];
    }

    public function getList($request)
    {
        $keyWord = $request->key_word;
        $results = $this->model;
        if ($keyWord) {
            $results = $results->where('title', 'like', "%\\{$keyWord}%");
        }
        return [
            'data' => $results->orderBy('created_at', "DESC")->get(),
        ];
    }

    public function detail($request)
    {
        return TemplateMail::findOrFail($request->id);
    }

    public function edit($id)
    {
        $mailSetting = $this->model->findOrFail($id);
        $users = User::whereIsFacility(true)->get();
        $groups = Group::all();
        $facilities = Facility::whereIsActive(true)->get();
        return [
            'users' => $users,
            'groups' => $groups,
            'facilities' => $facilities,
            'mailSetting' => $mailSetting
        ];
    }

    public function store($request)
    {
        try {
            $this->model->create([
                'title' => $request->title,
                'content' => $request->content,
            ]);
            return 200;
        } catch (\Exception $e) {
            return 500;
        }
    }

    public function update($request)
    {
        $model = $this->model->findOrFail($request['id']);
        $model->title = $request['title'];
        $model->content = $request['content'];
        if ($model->save()) {
            return 200;
        }
        return 500;
    }

    public function delete($request)
    {
        $model = $this->model->findOrFail($request['id']);
        if ($model->delete()) {
            return 200;
        }
        return 500;
    }
}
