<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Exports\FacilitieExports;
use App\Http\Services\FileCheckService;
use App\Messages;
use App\Models\Department;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use App\Models\User;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomepageServices
{
    protected $user;
    protected $settingMonth;
    protected $facilityService;
    protected $facility;
    protected $department;
    protected $typeCheck;
    protected $file;
    protected $fileCheckService;

    public function __construct(
        User $user,
        SettingMonth $settingMonth,
        Facility $facility,
        Department $department,
        TypeCheck $typeCheck,
        File $file,
        FileCheckService $fileCheckService
    ) {
        $this->user = $user;
        $this->settingMonth = $settingMonth;
        $this->facility = $facility;
        $this->department = $department;
        $this->typeCheck = $typeCheck;
        $this->file = $file;
        $this->fileCheckService = $fileCheckService;
    }

    public function index($request)
    {
        preg_match_all('!\d+!', $request->month, $matches);
        $dateMonth = date('Y/m');
        if (isset($request->month)) {
            preg_match_all('!\d+!', $request->month, $matches);
            $dateMonth = $matches[0][0].'/'.$matches[0][1];
        }
        $facilitys = $this->facility->with('department')->where('is_active', 1);
        $countTotal = $facilitys->count();

        $departments = $this->department->whereIn('id', $facilitys->pluck('departments_id')->toArray())->get();
        $countCanUploadAllNot = $this->settingMonth->where('year_month', '=', $dateMonth)
            ->where('can_upload', Consts::CAN_UPLOAD['not-active'])->count();
        $countCanUploadAll = $this->settingMonth->where('year_month', '=', $dateMonth)
            ->where('can_upload', Consts::CAN_UPLOAD['active'])->count();
        $facilitys = $this->getFacilityTable($request, $dateMonth);
        $dataCountFacility = [
            'success' => $this->countStatusFacility($dateMonth, Consts::STATUS_CHECK_FILE['success']),
            'error' => $this->countStatusFacility($dateMonth, Consts::STATUS_CHECK_FILE['error']),
            'hold' => $this->countStatusFacility($dateMonth, Consts::STATUS_CHECK_FILE['hold']),
            'total' => $countTotal,
        ];
        $notHasCheck = $countTotal - $dataCountFacility['success']
            - $dataCountFacility['error'] - $dataCountFacility['hold'];

        $dataCountFacility['not_has_check'] = $notHasCheck;
        $checkCanUploadAllFacility = $this->canUploadAllFacility($dateMonth);
        $data = [
            'departments' => $departments,
            'facilitys' => $facilitys,
            'dataCountFacility' => $dataCountFacility,
            'countCanUploadAllNot' => $countCanUploadAllNot,
            'countCanUploadAll' => $countCanUploadAll,
            'checkCanUploadAllFacility' => $checkCanUploadAllFacility
        ];

        return $data;
    }

    public function settingDeatline($request)
    {
        $date = date('Y/m');
        $deadline = date('Y-m-d', strtotime($request->seting_deadline));
        // if (isset($request->month)) {
        //     preg_match_all('!\d+!', $request->month, $matches);
        //     $date = $matches[0][0].'/'.$matches[0][1];
        // }

        // $facilityIds = $this->getFacilityIdTableAll($request, $date);
        $facilityIds = $this->facility->where('is_active', 1)->pluck('id');
        $settingMonths = $this->settingMonth->whereIn('facilities_id', $facilityIds)
        ->where('year_month', $date);
        $facilitiesIds = $settingMonths->pluck('facilities_id')->toArray();

        $settingMonthCreate = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        foreach ($facilityIds as $facilityId) {
            if (!in_array($facilityId, $facilitiesIds)) {
                $settingMonthCreate[] = [
                    'year_month' => $date,
                    'facilities_id' => $facilityId,
                    'updated_by' => $userId,
                    'created_by' => $userId,
                    'count_file_done' => 0,
                    'deadline' => $deadline,
                ];
            }
        }
        DB::beginTransaction();
        try {
            if (count($settingMonthCreate) > 0) {
                SettingMonth::insert($settingMonthCreate);
            }
            $settingMonths->update(['deadline' => $deadline, 'can_upload' => true]);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [],
        ], 200);
    }

    public function setupStatusUploadAll($request)
    {
        $date = date('Y/m');
        $deadline = date('Y-m-d', strtotime($request->seting_deadline));
        if (isset($request->month)) {
            preg_match_all('!\d+!', $request->month, $matches);
            $date = $matches[0][0].'/'.$matches[0][1];
        }

        $facilityIds = $this->getFacilityIdTableAll($request, $date);
        $settingMonths = $this->settingMonth->whereIn('facilities_id', $facilityIds)
        ->where('year_month', $date);
        $facilitiesIds = $settingMonths->pluck('facilities_id')->toArray();

        $settingMonthCreate = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        foreach ($facilityIds as $facilityId) {
            if (!in_array($facilityId, $facilitiesIds)) {
                $settingMonthCreate[] = [
                    'year_month' => $date,
                    'facilities_id' => $facilityId,
                    'updated_by' => $userId,
                    'created_by' => $userId,
                    'count_file_done' => 0,
                    'deadline' => null,
                ];
            }
        }
        DB::beginTransaction();
        try {
            if (count($settingMonthCreate) > 0) {
                SettingMonth::insert($settingMonthCreate);
            }
            if ($request->toggle_input_all == 1) {
                $settingMonths->update(['can_upload' => 1]);
            }
            if ($request->toggle_input_all == 0) {
                $settingMonths->update(['can_upload' => 0]);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [],
        ], 200);
    }

    public function countStatusFacility($dateMonth, $status)
    {
        $count = $this->settingMonth->where('year_month', '=', $dateMonth)
            ->where('status_facilitity', $status);

        return $count->count();
    }

    public function getFacilityTable($request, $dateMonth)
    {
        $conditions = [];
        if (isset($request->search)) {
            $conditions[] = ['name', 'like', '%'.$request->search.'%'];
        }
        if (isset($request->department_id)) {
            $conditions[] = ['departments_id', '=', $request->department_id];
        }
        $statusFacilitity = [['year_month', '=', $dateMonth]];

        $statusChecks = Consts::STATUS_CHECK_FILE;

        $facilitys = $this->facility->with('department')
            ->where('is_active', 1)->where($conditions);
        if (isset($request->status_facilitity) && $request->status_facilitity != $statusChecks['not_has_check']) {
            $statusFacilitity = $request->status_facilitity;
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($statusFacilitity, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)
                    ->where('status_facilitity', '=', $statusFacilitity);
            });
        }

        if (isset($request->status_facilitity) && $request->status_facilitity == $statusChecks['not_has_check']) {
            $facilitys = $facilitys->whereDoesntHave('settingMonth', function ($query) use ($statusChecks, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)->whereIn(
                    'status_facilitity',
                    [$statusChecks['success'], $statusChecks['error'], $statusChecks['hold']]
                );
            });
        }

        if (isset($request->deadline_search)) {
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($request) {
                $query->where('deadline', '=', $request->deadline_search);
            });
        }
        $facilitys = $facilitys->with('settingMonth')->with('settingMonth', function ($query) use ($dateMonth) {
            $query->where('year_month', '=', $dateMonth)->with('typeChecks')->with('typeChecks', function ($queryFile) {
                $queryFile->withCount('fileNotHold');
            });
        })->paginate(Consts::BASE_ADMIN_PAGE_SIZE);

        return $facilitys;
    }

    public function getFacilityIdTableAll($request, $dateMonth)
    {
        $conditions = [];
        if ($request->search) {
            $conditions[] = ['name', 'like', '%'.$request->search.'%'];
        }
        if ($request->department_id) {
            $conditions[] = ['departments_id', '=', $request->department_id];
        }
        $statusFacilitity = [['year_month', '=', $dateMonth]];

        $statusChecks = Consts::STATUS_CHECK_FILE;

        $facilitys = $this->facility->where('is_active', 1)->where($conditions);
        if ($request->status_facilitity && $request->status_facilitity != $statusChecks['not_has_check']) {
            $statusFacilitity = $request->status_facilitity;
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($statusFacilitity, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)
                    ->where('status_facilitity', '=', $statusFacilitity);
            });
        }

        if ($request->status_facilitity && $request->status_facilitity == $statusChecks['not_has_check']) {
            $facilitys = $facilitys->whereDoesntHave('settingMonth', function ($query) use ($statusChecks, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)->whereIn(
                    'status_facilitity',
                    [$statusChecks['success'], $statusChecks['error'], $statusChecks['hold']]
                );
            });
        }

        if ($request->deadline_search) {
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($request) {
                $query->where('deadline', '=', $request->deadline_search);
            });
        }
        $facilitys = $facilitys->with('settingMonth')->with('settingMonth', function ($query) use ($dateMonth) {
            $query->where('year_month', '=', $dateMonth)
                ->with('typeChecks')->with('typeChecks', function ($queryFile) {
                    $queryFile->withCount('fileNotHold');
                });
        })->pluck('id')->toArray();

        return $facilitys;
    }

    public function exportCsv($request)
    {
        $date = $request->month;
        $dateMonth = date('Y/m');
        $statusChecks = Consts::STATUS_CHECK_FILE;
        $dateExport = date('Ymd');
        $name =  "請求チェック状況管理_$dateExport.csv";
        $facilitys = $this->facility->where('is_active', 1)->with(['settingMonth', 'department'])
            ->with('settingMonth', function ($query) use ($date) {
                $query->where('year_month', '=', $date)
                    ->with('typeChecks')->with('typeChecks', function ($queryFile) {
                        $queryFile->withCount('fileNotHold');
                    });
            });
        $facilityIds = $facilitys->pluck('id')->toArray();
        if (!empty($request->search)) {
            $facilitys->where('name', 'LIKE', '%'. $request->search . '%');
        }
        if (!empty($request->department_id)) {
            $facilitys->where('departments_id', $request->department_id);
        }
        if (isset($request->status_facilitity) && $request->status_facilitity != $statusChecks['not_has_check']) {
            $statusFacilitity = $request->status_facilitity;
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($statusFacilitity, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)
                    ->where('status_facilitity', '=', $statusFacilitity);
            });
        }

        if (isset($request->status_facilitity) && $request->status_facilitity == $statusChecks['not_has_check']) {
            $facilitys = $facilitys->whereDoesntHave('settingMonth', function ($query) use ($statusChecks, $dateMonth) {
                $query->where('year_month', '=', $dateMonth)->whereIn(
                    'status_facilitity',
                    [$statusChecks['success'], $statusChecks['error'], $statusChecks['hold']]
                );
            });
        }
        if (isset($request->deadline_search)) {
            $facilitys = $facilitys->whereHas('settingMonth', function ($query) use ($request) {
                $query->where('deadline', '=', $request->deadline_search);
            });
        }
        $settingMonths = $this->settingMonth->whereIn('facilities_id', $facilityIds)
        ->where('year_month', $date);
        $facilitiesIds = $settingMonths->pluck('facilities_id')->toArray();
        $settingMonthCreate = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        foreach ($facilityIds as $facilityId) {
            if (!in_array($facilityId, $facilitiesIds)) {
                $settingMonthCreate[] = [
                    'year_month' => $date,
                    'facilities_id' => $facilityId,
                    'updated_by' => $userId,
                    'created_by' => $userId,
                    'count_file_done' => 0,
                ];
            }
        }
        DB::beginTransaction();
        try {
            if (count($settingMonthCreate) > 0) {
                SettingMonth::insert($settingMonthCreate);
            }
            DB::commit();
            return Excel::download(new FacilitieExports($request, $facilitys->get()), $name);
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error($exception);

            return redirect()->back();
        }
    }

    public function canUploadAllFacility($dateMonth)
    {
        $status = true;
        $date = date('Y-m-d');
        $settingMonth = $this->settingMonth->where('year_month', $dateMonth)->orderBy('deadline', 'DESC')->first();
        if (!$settingMonth) {
            return false;
        }
        $deadline = $settingMonth->deadline;
        if ($dateMonth != date('Y/m')) {
            $status = false;
        }
        if (strtotime($date) > strtotime($deadline)) {
            $status = false;
        }
        return $status;
    }
}
