<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Jobs\CustomAlertJob;
use App\Mail\CustomAlert;
use App\Messages;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\File;
use App\Models\Group;
use App\Models\HistoryMail;
use App\Models\MailSetting;
use App\Models\Role;
use App\Models\SettingMonth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class SettingAlertCustomService
{
    protected $service;

    public function __construct(MailSetting $model, HistoryMailService $historyMailService)
    {
        $this->model = $model;
        $this->historyMailService = $historyMailService;
    }

    public function index($request)
    {
        $keyWord = $request->key_word;
        $month = $request->month;
        $results = $this->model->whereType(Consts::MAILING_CUSTOM);
        if ($month) {
            $results = $results->whereDate('created_at', '=', date('Y-m-d', strtotime($month)));
        }
        if ($keyWord) {
            $frequencyType = array_search($keyWord, Consts::MAILING_FREQUENCY);
            $userIds = User::where('name', 'like', '%'.$keyWord.'%')->get()->pluck('id');
            $results = $results->where('frequency_type', '=', $frequencyType)
                                ->orWhereIN('created_by', $userIds);
        }
        $userIds =  $results->get()->pluck('created_by')->toArray();
        $userNames = User::whereIn('id', $userIds)->pluck('name', 'id')->toArray();
        $results = $results->orderBy('created_at', "DESC")->withCount('historyMails')
                            ->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        return [
            'data' => $results,
            'userNames' => $userNames
        ];
    }

    public function create()
    {
        $users = User::whereIsFacility(true)->get();
        $groups = Group::all();
        $facilities = Facility::whereIsActive(true)->get();
        return [
            'users' => $users,
            'groups' => $groups,
            'facilities' => $facilities
        ];
    }

    public function edit($id)
    {
        $mailSetting = $this->model->findOrFail($id);
        $users = User::whereIsFacility(true)->get();
        $groups = Group::all();
        $facilities = Facility::whereIsActive(true)->get();
        return [
            'users' => $users,
            'groups' => $groups,
            'facilities' => $facilities,
            'mailSetting' => $mailSetting
        ];
    }

    public function store($request)
    {
        try {
            $this->model->create([
                'user_ids' => $request->users,
                'grourp_ids' => $request->groups,
                'facilitity_ids' => $request->facility,
                'start_date' => $request->start_date,
                'list_mail' => array_filter($request->list_mail),
                'time' => $request->time,
                'frequency_type' => $request->frequency_type,
                'interval' => $request->interval,
                'send_times' => $request->send_times,
                'content' => $request->content,
                'subject' => $request->subject,
                'type' => Consts::MAILING_CUSTOM,
            ]);
            return redirect()->back()->with('success', 'Data Saved successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }

    public function update($id, $request)
    {
        $mailSetting = $this->model->findOrFail($id);
        $mailSetting->user_ids = $request->users;
        $mailSetting->grourp_ids= $request->groups;
        $mailSetting->facilitity_ids = $request->facility;
        $mailSetting->start_date = $request->start_date;
        $mailSetting->time = $request->time;
        $mailSetting->list_mail= array_filter($request->list_mail);
        $mailSetting->frequency_type = $request->frequency_type;
        $mailSetting->interval = $request->interval;
        $mailSetting->send_times = $request->send_times;
        $mailSetting->content = $request->content;
        $mailSetting->subject = $request->subject;
        $mailSetting->type = Consts::MAILING_CUSTOM;
        try {
            $mailSetting->save();
            return redirect()->back()->with('success', 'Data Saved successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }

    public function delete($id)
    {
        $mailSetting = $this->model->findOrFail($id);
        return $mailSetting->delete();
    }

    public function show($id)
    {
        $mailSetting = $this->model->where('id', '=', $id)->withCount('historyMails')->with('historyMails')
                                    ->firstOrFail();
        $historyMails = [];
        foreach ($mailSetting->historyMails as $key => $historyMail) {
            $allUserNames = User::whereIn('id', $historyMail->email_recipients)->pluck('name')->toArray();
            $listMail = $historyMail->list_mail ?? [];
            $allUserNames = array_merge($allUserNames, $listMail);
            $nameUsers = array_slice($allUserNames, 0, 2);
            $nameUser = implode(', ', $nameUsers);
            $countNumberUser = count($allUserNames) - 2;
            if ($countNumberUser > 0) {
                $nameUser .= "...<button class='rest-more' id='expandNum" .
                    $key . "'>+" . $countNumberUser . "</button>";
            }
            $historyMails[] = [
                'datetime' => date('Y/m/d', strtotime($historyMail->datetime)),
                'time' => date('H:i', strtotime($historyMail->time)),
                'userName' => $nameUser,
                'allUserName' => implode(', ', $allUserNames)
            ];
        }
        $user = User::where('id', '=', $mailSetting->created_by)->first();
        $userName = $user ? $user->name : '';
        $mailSetting = [
            'id' => $mailSetting->id,
            'name' => $userName,
            'created_at' => date('Y/m/d', strtotime($mailSetting->created_at)),
            'time_send' => date('H:i', strtotime($mailSetting->time)),
            'frequency_type' => Consts::MAILING_FREQUENCY[$mailSetting->frequency_type],
            'interval' => $mailSetting->interval,
            'list_mail' => $mailSetting->list_mail ?? [],
            'nume_time_send' => $mailSetting->history_mails_count . "/" . $mailSetting->send_times,
            'subject' => $mailSetting->subject,
            'content_mail' => $mailSetting->content,
            'historyMails' => $historyMails
        ];
        return response()->json([
            'error' => false,
            'data' => $mailSetting,
        ], 200);
    }

    public function getAlert($time)
    {
        $timeSend = $time->format('H:i') . ':00';
        $query = $this->model->whereType(Consts::MAILING_CUSTOM)
            ->whereDate('start_date', '<=', $time->format('Y-m-d'))
            ->where('send_times', '>', DB::raw('
            (select count(*) from history_mail where mail_setting.id = history_mail.mail_setting_id)'))
            ->whereTime('time', date('H:i:s', strtotime($timeSend)))
            ->with('latestHistoryMail');
        $alertId = [];
        $records = $query->get();
        foreach ($records as $record) {
            if (is_null($record->latestHistoryMail) || $record->start_date == date('Y-m-d')) {
                $alertId[] = $record->id;
            } else {
                switch ($record->frequency_type) {
                    case Consts::FREQUENCY_TYPE['DAY_GAP']:
                        $interval = $record->interval;
                        break;
                    case Consts::FREQUENCY_TYPE['WEEK_GAP']:
                        $interval = $record->interval * 7;
                        break;
                    default:
                        $interval = Consts::FREQUENCY_TYPE['DAILY'];
                        break;
                }
                $lastSend = Carbon::create($record->latestHistoryMail->datetime);
                $daysPassed = Carbon::now()->diffInDays($lastSend);
                if (!(boolean)($daysPassed % $interval)) {
                    $alertId[] = $record->id;
                }
            }
        }
        return $query->whereIn('id', $alertId)->get()->toArray();
    }

    public function alertCustom()
    {
        $time = now();
        $alerts = $this->getAlert($time);
        try {
            DB::beginTransaction();
            foreach ($alerts as $alert) {
                $facilityEmails = [];
                $groupEmails = [];
                $userEmails = [];
                if ($alert['facilitity_ids']) {
                    $facilityEmails = $this->getFacilityEmail($alert['facilitity_ids']);
                }
                if ($alert['grourp_ids']) {
                    $groupEmails = $this->getGroupEmail($alert['grourp_ids']);
                }
                if ($alert['user_ids']) {
                    $userEmails = $this->getUserEmail($alert['user_ids']);
                }
                $emails = array_merge($userEmails, $facilityEmails, $groupEmails);
                $emails = array_values(array_map(
                    "unserialize",
                    array_unique(array_map("serialize", $emails))
                ));
                $subject = $alert['subject'] ?? '';
                $alert['datetime'] = $time;
                $alert['emails'] = array_column($emails, 'email');
                $alert['recipients'] = array_column($emails, 'id');
                $mail = $this->customBody($alert);
                $contentMail = $mail['content'];
                $subjectMail = $mail['subject'];
                foreach ($emails as $email) {
                    $content = str_replace('#USER_NAME#', $email['name'], $contentMail);
                    $subject = str_replace('#USER_NAME#', $email['name'], $subjectMail);
                    $content = preg_replace("/\r|\n/", "", $content);
                    $subject = preg_replace("/\r|\n/", "", $subject);
                    $mailable = new CustomAlert($content, $subject);
                    CustomAlertJob::dispatch($email['email'], $mailable);
                }
                $alert['list_mail_send'] = [];
                $listMails = is_array($alert['list_mail']) ? $alert['list_mail'] : [];
                foreach ($listMails as $email) {
                    if (!in_array($email, $alert['emails'])) {
                        $content = str_replace('#USER_NAME#', $email, $contentMail);
                        $subject = str_replace('#USER_NAME#', $email, $subjectMail);
                        $content = preg_replace("/\r|\n/", "", $content);
                        $subject = preg_replace("/\r|\n/", "", $subject);
                        $mailable = new CustomAlert($content, $subject);
                        CustomAlertJob::dispatch($email, $mailable);
                        $alert['emails'][] = $email;
                        $alert['list_mail_send'][] = $email;
                    }
                }
                $this->historyMailService->create($alert);
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    private function getFacilityId($date)
    {
        return SettingMonth::whereYearMonth(date('Y/m'))
            ->where(function ($query) {
                $query->where('status_facilitity', '<>', Consts::FACILITY_STATUS['COMPLETE'])
                    ->orWhereNull('status_facilitity');
            })->whereNotNull('deadline')
            ->whereYear('deadline', $date->year)
            ->whereMonth('deadline', $date->month)
            ->whereDate('deadline', '<=', $date->format('Y-m-d'))
            ->pluck('facilities_id')->toArray();
    }

    private function getFacility($facilityIds, $date)
    {
        return Facility::whereIn('id', $facilityIds)
            ->whereIsActive(true)
            ->select('id', 'name', 'departments_id', 'list_type_check')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])
            ->withCount('typeCheckNotDone')->get()->toArray();
    }
    public function formatAlert12hTable($facilities)
    {
        $table = '<style>table,th,td{border: 1px solid black;border-collapse: collapse;}</style>
<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th></tr></thead><tbody>';
        $department = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (!in_array($departmentName, $department)) {
                $department[] = $departmentName;
                if (count($department) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }
    public function formatAlert15hTable()
    {
        $date = now();
        $table = '<style>table,th,td{border: 1px solid black;border-collapse: collapse;}</style>
<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th><th>初回提出日</th>';
        $table .= '<th>初回提出時間</th><th>終了日</th><th>終了時間</th></tr></thead><tbody>';
        $doneFacilities = SettingMonth::whereYearMonth(date('Y/m'))
            ->whereStatusFacilitity(Consts::STATUS_CHECK_FILE['success'])
            ->pluck('facilities_id')
            ->toArray();
        $facilities = Facility::whereIn('id', $doneFacilities)
            ->select('id', 'name', 'departments_id')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])->get()->toArray();
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        $department = array_column($facilities, 'department_name');
        array_multisort($department, SORT_ASC, $facilities);
        $typeCheckIds = [];
        foreach ($facilities as &$facility) {
            foreach ($facility['type_check'] as $typeCheck) {
                $typeCheckIds[] = $typeCheck['id'];
            }
            $facility['type_check'] = array_column($facility['type_check'], 'id');
        }
        $files = File::whereYear('created_at', $date->year)
            ->whereMonth('created_at', $date->month)
            ->whereIn('type_check_id', $typeCheckIds)->latest()->get()->toArray();
        foreach ($facilities as &$facility) {
            $facilityFiles = [];
            foreach ($files as $file) {
                if (in_array($file['type_check_id'], $facility['type_check'])) {
                    $facilityFiles[] = $file;
                }
            }
            $facility['file'] = $facilityFiles;
        }
        $departments = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (empty($value['file'])) {
                $firstCheckDate = $firstCheckTime = $lastCheckDate = $lastCheckTime = '';
            } else {
                $firstCheck = end($value['file']);
                $firstCheckDate = date('Y/m/d', strtotime($firstCheck['created_at']));
                $firstCheckTime = date('H:i', strtotime($firstCheck['created_at']));
                $lastCheck = array_shift($value['file']);
                $lastCheckDate = date('Y/m/d', strtotime($lastCheck['created_at']));
                $lastCheckTime = date('H:i', strtotime($lastCheck['created_at']));
            }
            if (!in_array($departmentName, $departments)) {
                $departments[] = $departmentName;
                if (count($departments) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckTime}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckTime}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }
    public function formatAlert18h($content)
    {
        $facilities = Facility::whereIsActive(true)->select('id', 'name', 'departments_id', 'list_type_check')
            ->with('currentSettingMonth')
            ->withCount('typeCheckError')
            ->orderBy('type_check_error_count', 'DESC')
            ->get()->toArray();
        $table = '<style>table,th,td{border: 1px solid black;border-collapse: collapse;}</style>
<table><thead><th>事業所名</th><th>エラー回数</th><th>ステータス</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $status = $facility['current_setting_month']['status_facilitity'] ??
                Consts::STATUS_CHECK_FILE['not_has_check'];
            $status = Consts::STATUS_CHECK[$status];
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px'>エラー{$facility['type_check_error_count']}</td>";
            $table .= "<td style='padding: 0 5px'>{$status}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLEDATA18H#', $table, $content);
    }
    public function formatNotDoneList($content, $facilities)
    {
        $table = '<style>table,th,td{border: 1px solid black;border-collapse: collapse;}</style>
<table><thead><th>事業所名</th><th>未チェック機能数／チェック機能総数</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $functionExcept = ['1', '2', '4'];
            $allFunctions = explode(',', $facility['list_type_check']);
            $allFunctions = count(array_diff($allFunctions, $functionExcept));
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>";
            $table .= "{$facility['type_check_not_done_count']}/{$allFunctions}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLE_NOT_DONE#', $table, $content);
    }
    private function customBody($alert)
    {
        $date = now();
        $facilityIds = $this->getFacilityId($date);
        $facilities = $this->getFacility($facilityIds, $date);
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        $subject = $alert['subject'];
        $content = $alert['content'];
        $subject = str_replace('#CURRENT_MONTH#', $date->month, $subject);
        $subject = str_replace('#CURRENT_DATE#', $date->day, $subject);
        $content = str_replace('#CURRENT_MONTH#', $date->month, $content);
        $content = str_replace('#CURRENT_DATE#', $date->day, $content);
        if (str_contains($content, '#TABLEDATA12H#')) {
            $table = $this->formatAlert12hTable($facilities);
            $content = str_replace('#TABLEDATA12H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA15H#')) {
            $table = $this->formatAlert15hTable();
            $content = str_replace('#TABLEDATA15H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA18H#')) {
            $content = $this->formatAlert18h($content);
        }
        if (str_contains($content, '#TABLE_NOT_DONE#')) {
            $content = $this->formatNotDoneList($content, $facilities);
        }
        $alert['subject'] = $subject;
        $alert['content'] = $content;
        return $alert;
    }

    public function getFacilityEmail($facilityIds)
    {
        $facilityUsers = Role::whereIn('facilities_id', $facilityIds)->pluck('users_id')->toArray();
        return $this->getUserEmail($facilityUsers);
    }

    public function getGroupEmail($groupId)
    {
        $groups = Group::whereIn('id', $groupId)->get()->toArray();
        $userIds = [];
        $facilityIds = [];
        foreach ($groups as $group) {
            $userIds = $group['user_ids'] ? array_merge($userIds, $group['user_ids']) : $userIds;
            $facilityIds = $group['facilitity_ids'] ?
                array_merge($facilityIds, $group['facilitity_ids']) : $facilityIds;
        }
        $userEmails = [];
        $facilityEmails = [];
        if ($userIds) {
            $userEmails = $this->getUserEmail($userIds);
        }
        if ($facilityIds) {
            $facilityEmails = $this->getFacilityEmail($facilityIds);
        }
        return array_merge($userEmails, $facilityEmails);
    }

    public function getUserEmail($userIds)
    {
        return User::whereIn('id', $userIds)->whereIsFacility(true)
            ->get()->toArray();
    }
}
