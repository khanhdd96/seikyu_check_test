<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\FileOutsideInsurance;
use App\Models\Facility;
use Illuminate\Support\Facades\DB;
use App\Messages;
use Carbon\Carbon;

class FileOutsideInsuranceService
{
    public function __construct(FileOutsideInsurance $fileOutsideInsurance)
    {
        $this->fileOutsideInsurance =  $fileOutsideInsurance;
    }

    public function index($search)
    {
        $results = $this->fileOutsideInsurance;
        if ($search) {
            $results = $results->where('category_name', 'like', '%'.$search.'%');
        }

        return $results->orderBy('updated_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
    }

    public function store($request)
    {
        try {
            $this->fileOutsideInsurance->create([
                'category_name' => $request['createCategoryName'],
                'fiduciary_goal' => $request['createFiduciaryGoal'],
                'date_update' => Carbon::now()
            ]);

            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            return [
                'code' => 500
            ];
        }
    }

    public function show($id)
    {
        return $this->fileOutsideInsurance->findOrFail($id);
    }

    public function update($request)
    {
        try {
            $this->fileOutsideInsurance->findOrFail($request['id'])->update([
                'category_name' => $request['editCategoryName'],
                'fiduciary_goal' => $request['editFiduciaryGoal'],
                'date_update' => Carbon::now()
            ]);
            return [
                'code' => 200
            ];
        } catch (\Exception $e) {
            return [
                'code' => 500
            ];
        }
    }

    public function destroy($id)
    {
        return $this->fileOutsideInsurance->findOrFail($id)->delete();
    }
}
