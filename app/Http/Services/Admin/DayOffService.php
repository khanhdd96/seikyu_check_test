<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\DayOff;
use App\Models\Facility;
use Illuminate\Support\Facades\DB;
use App\Messages;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DayOffService
{
    public function __construct(DayOff $dayOff)
    {
        $this->dayOff = $dayOff;
    }

    public function index($search)
    {
        $results = $this->dayOff;
        if ($search) {
            $results = $results->where('date_off_type', $search);
        }

        return $results->orderBy('updated_at', 'desc')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
    }

    public function store($request)
    {
        $dayOffs = explode(',', $request['dayOff']);
        DB::beginTransaction();
        try {
            foreach ($dayOffs as $dayOff) {
                $this->dayOff->updateOrCreate([
                    "user_id" => Auth::id(),
                    "date_off" => $dayOff,
                    "date_off_type" => $request['dayOffType']
                ], [
                    "date_off_update" => Carbon::now(),
                ]);
            }
            DB::commit();
            return 200;
        } catch (\Exception $e) {
            DB::rollBack();
        }
        return 500;
    }

    public function show($id)
    {
        return $this->dayOff->findOrFail($id);
    }

    public function update($request)
    {
        $dayOff = $this->dayOff->findOrFail($request['id']);
        $dayOff->date_off = $request['dayOff'];
        $dayOff->date_off_type = $request['dayOffType'];
        $dayOff->date_off_update = Carbon::now();
        if ($dayOff->save()) {
            return 200;
        }
        return 500;
    }

    public function destroy($id)
    {
        $dayOff = $this->dayOff->findOrFail($id);
        $dayOff->delete();
        return 200;
    }
}
