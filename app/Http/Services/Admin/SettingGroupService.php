<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Messages;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SettingGroupService
{
    protected $service;

    public function __construct(Group $model)
    {
        $this->model = $model;
    }

    public function index($request)
    {
        $results = $this->model;
        $search = $request->search;
        $date = $request->date;
        if ($search) {
            $results = $results->where('group_name', 'like', '%'. $search . '%');
        }
        if ($date) {
            $results = $results->whereDate('created_at', '=', $date);
        }
        $results = $results->orderBy('created_at', "DESC")->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        $groups = $results->getCollection();
        $datas = [];
        $facilityIds = [];
        $userIds = [];
        foreach ($groups as $group) {
            $userId = $group->user_ids ?? [];
            $facilitityId = $group->facilitity_ids ?? [];
            $facilityIds = array_merge($facilityIds, $facilitityId);
            $userIds = array_merge($userIds, $userId);
            $datas[$group->id] = [
                'user' => $group->user_ids ?? [],
                'facilitity_ids' => $group->facilitity_ids ?? []
            ];
        }
        $users = User::whereIn('id', $userIds)->get()->pluck('id')->toArray();
        $facilities = Facility::whereIn('id', $facilityIds)->get()->pluck('id')->toArray();
        $dataCounts = [];
        foreach ($datas as $key => $data) {
            $dataCounts[$key] = [
                'user' => count(array_intersect($users, $data['user'])),
                'facilitity_ids' => count(array_intersect($facilities, $data['facilitity_ids'])),
            ];
        }
        return [
            'list-data' => $results,
            'dataCounts' => $dataCounts,
        ];
    }

    public function searchAllFacilityAndUser()
    {
        $listAccounting = User::where('is_admin', '=', true)->get();
        $listFacility = Facility::whereIsActive(true)->get();
        return [
            'listAccounting' => $listAccounting,
            'listFacility' => $listFacility
        ];
    }

    public function store($request)
    {
        if (!$request->id_group) {
            $group = new Group();
        } else {
            $group = Group::find($request->id_group);
            if (!$group) {
                return response()->json([
                    'error' => true,
                    'message' => Messages::SYSTERM_ERROR,
                ], 404);
            }
        }
        $group->group_name = $request->group_name;
        $group->facilitity_ids = $request->facilities ?? null;
        $group->user_ids = $request->users ?? null;
        $group->created_by = Auth::id();
        $group->updated_by = Auth::id();
        try {
            $group->save();
            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }

    public function update($id, $request)
    {
    }
    public function delete($request)
    {
        Group::findOrFail($request->id)->delete();
        return back();
    }
    public function show($id)
    {
        $group = Group::find($id);
        if (!$group) {
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 404);
        }
        $groupUsers = $group->user_ids ?? [];
        $groupFacilities = $group->facilitity_ids ?? [];
        $userInfo = User::whereIn('id', $groupUsers)->get();
        $facilitiesInfo = Facility::whereIn('id', $groupFacilities)->get();
        return [
            'group' => $group,
            'user' => $userInfo,
            'facilities' => $facilitiesInfo
        ];
    }
    public function searchFacility($request)
    {
        $keywork = $request->keyword;
        $idSearched = $request->ids ?? [];
        $facilities = Facility::whereNotIn('id', $idSearched);
        $checked = '';
        if ($keywork) {
            $facilities = $facilities->where('name', 'like', '%' . $keywork . '%');
            $checked = '';
        }
        $facilities = $facilities->get();
        return [
            'facilities' => $facilities,
            'name' => $keywork,
            'checked' => $checked
        ];
    }
    public function searchUser($request)
    {
        $keywork = $request->keyword;
        $idSearched = $request->ids ?? [];
        $users = User::where('is_admin', '=', true)
            ->whereNotIn('id', $idSearched);
        $checked = '';
        if ($keywork) {
            $users = $users->where('name', 'like', '%' . $keywork . '%');
            $checked = '';
        }
        $users = $users->get();
        return [
            'users' => $users,
            'name' => $keywork,
            'checked' => $checked
        ];
    }
}
