<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DecentralizateUserDetailService
{
    public function __construct(
        Role $model
    ) {
        $this->model = $model;
    }
    public function index($request)
    {
        $userId = $request->id;
        $facilityUserCount =  FacilityUser::where('users_id', '=', $userId)->count();
        $user =  User::where('id', '=', $userId)->first();
        $userRols = $this->model->where('users_id', '=', $userId)
                            ->orderBy('created_at', 'DESC')->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        $facilitiesId = $userRols->pluck('facilities_id')->toArray();
        $facilities = Facility::whereIn('id', $facilitiesId)->get();
        $plucked = [];
        foreach ($facilities as $item) {
            $plucked[$item->id] = [
                'name' => $item->name,
                'email' => $item->email
                ];
        }
        return [
            'facilityUserCount' => $facilityUserCount,
            'userRols' => $userRols,
            'facilities' => $plucked,
            'user' => $user
        ];
    }
    public function update($request)
    {
        $views = $request->view ?? [];
        $edits = $request->edit ?? [];
        $idRoles = $request->id_roles;
        $hasError = false;
        DB::beginTransaction();
        try {
            foreach ($idRoles as $idRole) {
                $role = Role::find($idRole);
                $role->view = isset($views[$idRole]) ? true : false;
                $role->edit = isset($edits[$idRole]) ? true : false;
                $role->save();
            }
            DB::commit();
        } catch (\Exception $exception) {
            $hasError = true;
            DB::rollBack();
        }
        return $hasError;
    }
}
