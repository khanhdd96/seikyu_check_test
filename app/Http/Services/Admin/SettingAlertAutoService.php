<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Jobs\AutoAlertJob;
use App\Jobs\AutoAlertMailJob;
use App\Mail\AutoAlert;
use App\Messages;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\File;
use App\Models\Group;
use App\Models\MailSetting;
use App\Models\Role;
use App\Models\SettingMonth;
use App\Models\TemplateMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SettingAlertAutoService
{
    protected $service;
    protected $settingAlertCustomService;
    protected $historyMailService;
    public function __construct(
        MailSetting $model,
        SettingAlertCustomService $settingAlertCustomService,
        HistoryMailService $historyMailService
    ) {
        $this->model = $model;
        $this->settingAlertCustomService = $settingAlertCustomService;
        $this->historyMailService = $historyMailService;
    }

    public function index($request)
    {
        $month = $request->month;
        $results = $this->model->whereType(Consts::MAILING_AUTO);
        if ($month) {
            $monthYear = explode('/', $month);
            $results = $results->whereMonth('start_date', $monthYear[1])->whereYear('start_date', $monthYear[0]);
        }
        $results = $results->orderBy('created_at', "DESC")->withCount('historyMails')
            ->paginate(Consts::BASE_ADMIN_PAGE_SIZE);
        return [
            'data' => $results,
        ];
    }

    public function create()
    {
        $users = User::whereIsFacility(true)->get();
        $groups = Group::all();
        $facilities = Facility::whereIsActive(true)->get();
        $mailSettingId = $this->model->where('type', '=', 2)->orderBy('start_date', 'DESC')->pluck('id')->first();
        if ($mailSettingId) {
            return [
                'id' => $mailSettingId,
                'create' => false
                ];
        }
        return [
            'users' => $users,
            'groups' => $groups,
            'facilities' => $facilities,
            'create' => true
        ];
    }

    public function edit($id)
    {
        $mailSetting = $this->model->findOrFail($id);
        $users = User::whereIsFacility(true)->get();
        $groups = Group::all();
        $facilities = Facility::whereIsActive(true)->get();
        return [
            'users' => $users,
            'groups' => $groups,
            'facilities' => $facilities,
            'mailSetting' => $mailSetting
        ];
    }

    public function store($request)
    {
        try {
            $this->model->create([
                'user_ids' => $request->users,
                'grourp_ids' => $request->groups,
                'start_date' => $request->start_date,
                'time' => $request->time,
                'list_mail' => $request->list_mail,
                'content' => $request->content,
                'type' => Consts::MAILING_AUTO,
                'send_times' => 10,
                'frequency_type' => Consts::MAILING_FREQUENCY_DAILI,
            ]);
            return redirect()->back()->with('success', 'Data Saved successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }

    public function update($id, $request)
    {
        $mailSetting = $this->model->findOrFail($id);
        $mailSetting->user_ids = $request->users;
        $mailSetting->grourp_ids= $request->groups;
        $mailSetting->list_mail= array_filter($request->list_mail);
        $mailSetting->start_date = $request->start_date;
        $mailSetting->frequency_type = $request->frequency_type;
//        $mailSetting->time = $request->time;
        $mailSetting->interval = $request->interval;
//        $mailSetting->send_times = $request->send_times;
        $mailSetting->content = $request->content;
        $mailSetting->subject = $request->subject;
        try {
            $mailSetting->save();
            return redirect()->back()->with('success', 'Data Saved successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => Messages::SYSTERM_ERROR]);
        }
    }

    public function autoCreateAlert()
    {
        $firstDay = now()->addDay();
        $date = date('w', strtotime($firstDay));
        switch ($date) {
            case Consts::SUNDAY:
                $startDate = $firstDay->addDay()->format('Y-m-d');
                break;
            case Consts::SATURDAY:
                $startDate = $firstDay->addDays(2)->format('Y-m-d');
                break;
            default:
                $startDate = $firstDay->format('Y-m-d');
                break;
        }
        foreach (Consts::AUTO_ALERT_TEMPLATE as $key => $value) {
            $autoAlerts[] =[
                'start_date' => $startDate,
                'time' => $key,
                'content' => $value['body'],
                'frequency_type' => Consts::MAILING_FREQUENCY_DAILI,
                'send_times' => Consts::AUTO_SEND_TIMES,
                'type' => Consts::MAILING_AUTO,
                'created_by' => env('DEFAULT_ADMIN_ID'),
                'updated_by' => env('DEFAULT_ADMIN_ID'),
                'created_at' => now(),
                'updated_at' => now(),
                'subject' => $value['subject']
            ];
        }
        return $this->model->insert($autoAlerts);
    }

    public function getAlert($time)
    {
        $timeSend = $time->format('H:i') . ':00';
        $query = $this->model
            ->whereType(Consts::MAILING_AUTO)
            ->whereYear('start_date', $time->year)
            ->whereMonth('start_date', $time->month)
            ->whereTime('time', date('H:i:s', strtotime($timeSend)))
            ->with('latestHistoryMail');
        $records = $query->get();
        $alertId = [];
        foreach ($records as $record) {
            if (is_null($record->latestHistoryMail)) {
                $alertId[] = $record->id;
            } else {
                switch ($record->frequency_type) {
                    case Consts::FREQUENCY_TYPE['DAY_GAP']:
                        $interval = $record->interval;
                        break;
                    case Consts::FREQUENCY_TYPE['WEEK_GAP']:
                        $interval = $record->interval * 7;
                        break;
                    default:
                        $interval = Consts::FREQUENCY_TYPE['DAILY'];
                        break;
                }
                $lastSend = Carbon::create($record->latestHistoryMail->datetime);
                $daysPassed = Carbon::now()->diffInDays($lastSend);
                if (!(boolean)($daysPassed % $interval)) {
                    $alertId[] = $record->id;
                }
            }
        }
        return $query->whereIn('id', $alertId)->get()->toArray();
    }

    public function getFacilityId($date)
    {
        return SettingMonth::whereYearMonth(date('Y/m'))
            ->where(function ($query) {
                $query->where('status_facilitity', '<>', Consts::FACILITY_STATUS['COMPLETE'])
                    ->orWhereNull('status_facilitity');
            })->whereNotNull('deadline')
            ->whereYear('deadline', $date->year)
            ->whereMonth('deadline', $date->month)
            ->whereDate('deadline', '<=', $date->format('Y-m-d'))
            ->pluck('facilities_id')->toArray();
    }

    public function getFacility($facilityIds, $date)
    {
        return Facility::whereIn('id', $facilityIds)
            ->whereIsActive(true)
            ->select('id', 'name', 'departments_id', 'list_type_check')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])
            ->withCount('typeCheckNotDone')->get()->toArray();
    }

    public function autoAlert()
    {
        $date = now();
        $alerts = $this->getAlert($date);
        $facilityIds = $this->getFacilityId($date);
        $facilities = $this->getFacility($facilityIds, $date);
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        $department = array_column($facilities, 'department_name');
        array_multisort($department, SORT_ASC, $facilities);
        $allUsers = $this->getAutoAlertFacilityUser($facilityIds);
        $admins = User::whereIsAdmin(true)->where('email', '<>', 'tzoo2235@gmail.com')
            ->with('facilityUsers')->get()->toArray();
        $allUsers = array_merge($allUsers, $admins);
        $userIds = array_column($allUsers, 'id');
        foreach ($alerts as $alert) {
            if ($alert['grourp_ids']) {
                $groupUser = $this->getAutoAlertGroupUser($alert['grourp_ids']);
                if ($groupUser) {
                    $groupUserIds = array_column($groupUser, 'id');
                    $userIds = array_diff($userIds, $groupUserIds);
                }
            }
            if ($alert['user_ids']) {
                $userNotSend = $this->getAutoAlertUser($alert['user_ids']);
                if ($userNotSend) {
                    $userNotSendIds = array_column($userNotSend, 'id');
                    $userIds = array_diff($userIds, $userNotSendIds);
                }
            }
            $users = [];
            foreach ($allUsers as $person) {
                if (in_array($person['id'], $userIds)) {
                    $users[] = $person;
                }
            }
            $users = array_values(array_map("unserialize", array_unique(array_map("serialize", $users))));
            $emails = array_values(array_filter(array_column($users, 'email')));
            $alert['datetime'] = $date;
            $alert['emails'] = $emails;
            $mail = $this->formatAlertMail($alert, $facilities);
            $contentMail = $mail['content'];
            $subjectMail = $mail['subject'];
            $alert['recipients'] = array_column($users, 'id');
            foreach ($users as $userMail) {
                $content = str_replace('#USER_NAME#', $userMail['name'], $contentMail);
                $subject = str_replace('#USER_NAME#', $userMail['name'], $subjectMail);
                $mailSend = $mail;
                $mailSend['content'] = $content;
                $mailSend['subject'] = $subject;
                $mailable = new AutoAlert($mailSend);
                AutoAlertMailJob::dispatch($userMail['email'], $mailable);
            }
            $alert['list_mail_send'] = [];
            $listMails = is_array($alert['list_mail']) ? $alert['list_mail'] : [];
            foreach ($listMails as $email) {
                if (!in_array($email, $alert['emails'])) {
                    $content = str_replace('#USER_NAME#', $email, $contentMail);
                    $subject = str_replace('#USER_NAME#', $email, $subjectMail);
                    $mailSend = $mail;
                    $mailSend['content'] = $content;
                    $mailSend['subject'] = $subject;
                    $mailable = new AutoAlert($mailSend);
                    AutoAlertMailJob::dispatch($email, $mailable);
                    $alert['emails'][] = $email;
                    $alert['list_mail_send'][] = $email;
                }
            }
            $this->historyMailService->create($alert);
        }
    }

    public function formatAlertMail($alert, $facilities)
    {
        $date = now();
        $subject = $alert['subject'];
        $content = $alert['content'];
        $subject = str_replace('#CURRENT_MONTH#', $date->month, $subject);
        $subject = str_replace('#CURRENT_DATE#', $date->day, $subject);
        $content = str_replace('#CURRENT_MONTH#', $date->month, $content);
        $content = str_replace('#CURRENT_DATE#', $date->day, $content);
        if (str_contains($content, '#TABLEDATA12H#')) {
            $table = $this->formatAlert12hTable($facilities);
            $content = str_replace('#TABLEDATA12H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA15H#')) {
            $table = $this->formatAlert15hTable();
            $content = str_replace('#TABLEDATA15H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA18H#')) {
            $content = $this->formatAlert18h($content);
        }
        if (str_contains($content, '#TABLE_NOT_DONE#')) {
            $content = $this->formatNotDoneList($content, $facilities);
        }
        return [
            'subject' => $subject,
            'content' => $content
        ];
    }

    public function formatAlert12hTable($facilities)
    {
        $table = '<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th></tr></thead><tbody>';
        $department = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (!in_array($departmentName, $department)) {
                $department[] = $departmentName;
                if (count($department) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }

    public function formatAlert15hTable()
    {
        $date = now();
        $table = '<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th><th>初回提出日</th>';
        $table .= '<th>初回提出時間</th><th>終了日</th><th>終了時間</th></tr></thead><tbody>';
        $doneFacilities = SettingMonth::whereYearMonth(date('Y/m'))
            ->whereStatusFacilitity(Consts::STATUS_CHECK_FILE['success'])
            ->pluck('facilities_id')
            ->toArray();
        $facilities = Facility::whereIn('id', $doneFacilities)
            ->select('id', 'name', 'departments_id')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])->get()->toArray();
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        $department = array_column($facilities, 'department_name');
        array_multisort($department, SORT_ASC, $facilities);
        $typeCheckIds = [];
        foreach ($facilities as &$facility) {
            foreach ($facility['type_check'] as $typeCheck) {
                $typeCheckIds[] = $typeCheck['id'];
            }
            $facility['type_check'] = array_column($facility['type_check'], 'id');
        }
        $files = File::whereYear('created_at', $date->year)
            ->whereMonth('created_at', $date->month)
            ->whereIn('type_check_id', $typeCheckIds)->latest()->get()->toArray();
        foreach ($facilities as &$facility) {
            $facilityFiles = [];
            foreach ($files as $file) {
                if (in_array($file['type_check_id'], $facility['type_check'])) {
                    $facilityFiles[] = $file;
                }
            }
            $facility['file'] = $facilityFiles;
        }
        $departments = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (empty($value['file'])) {
                $firstCheckDate = $firstCheckTime = $lastCheckDate = $lastCheckTime = '';
            } else {
                $firstCheck = end($value['file']);
                $firstCheckDate = date('Y/m/d', strtotime($firstCheck['created_at']));
                $firstCheckTime = date('H:i', strtotime($firstCheck['created_at']));
                $lastCheck = array_shift($value['file']);
                $lastCheckDate = date('Y/m/d', strtotime($lastCheck['created_at']));
                $lastCheckTime = date('H:i', strtotime($lastCheck['created_at']));
            }
            if (!in_array($departmentName, $departments)) {
                $departments[] = $departmentName;
                if (count($departments) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckTime}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckTime}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }
    public function formatAlert18h($content)
    {
        $facilities = Facility::whereIsActive(true)->select('id', 'name', 'departments_id', 'list_type_check')
            ->with('currentSettingMonth')
            ->withCount('typeCheckError')
            ->orderBy('type_check_error_count', 'DESC')
            ->get()->toArray();
        $table = '<table><thead><th>事業所名</th><th>エラー回数</th><th>ステータス</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $status = $facility['current_setting_month']['status_facilitity'] ??
                Consts::STATUS_CHECK_FILE['not_has_check'];
            $status = Consts::STATUS_CHECK[$status];
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px'>エラー{$facility['type_check_error_count']}</td>";
            $table .= "<td style='padding: 0 5px'>{$status}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLEDATA18H#', $table, $content);
    }
    public function formatNotDoneList($content, $facilities)
    {
        $table = '<table><thead><th>事業所名</th><th>未チェック機能数／チェック機能総数</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $allFunctions = explode(',', $facility['list_type_check']);
            $allFunctions = count($allFunctions);
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>";
            $table .= "{$facility['type_check_not_done_count']}/{$allFunctions}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLE_NOT_DONE#', $table, $content);
    }
    public function getAutoAlertFacilityUser($facilityIds)
    {
        $facilityUsers = Role::whereIn('facilities_id', $facilityIds)->pluck('users_id')->toArray();
        return $this->getAutoAlertUser($facilityUsers);
    }

    public function getAutoAlertGroupUser($groupId)
    {
        $groups = Group::whereIn('id', $groupId)->get()->toArray();
        $userIds = [];
        $facilityIds = [];
        foreach ($groups as $group) {
            $userIds = $group['user_ids'] ? array_merge($userIds, $group['user_ids']) : $userIds;
            $facilityIds = $group['facilitity_ids'] ?
                array_merge($facilityIds, $group['facilitity_ids']) : $facilityIds;
        }
        $userEmails = [];
        $facilityEmails = [];
        if ($userIds) {
            $userEmails = $this->getAutoAlertUser($userIds);
        }
        if ($facilityIds) {
            $facilityEmails = $this->getAutoAlertFacilityUser($facilityIds);
        }
        return array_merge($userEmails, $facilityEmails);
    }

    public function getAutoAlertUser($userIds)
    {
        return User::whereIn('id', $userIds)->where('email', '<>', 'tzoo2235@gmail.com')->whereIsFacility(1)
            ->with('facilityUsers')->get()->toArray();
    }
}
