<?php

namespace App\Http\Services\Admin;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;

class FileCheckService
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
    }
    public function index($request)
    {
        $type = $request->type_check;
        $month = $request->month ?? Carbon::now()->format('Y/m');
        $facilityId = $request->facilities_id ?? Consts::USER_ID_DEFAULT;
        $settingMonth = $this->model->whereNull('facilities_id')
            ->where('year_month', '=', $month)->first();
        $files = [];
        $typeCheck = [];
        $fileCount = 0;
        $createdAt = '';
        $updatedAt = '';
        if ($settingMonth) {
            $typeCheck = $settingMonth->typeCheck($type)->first();
            if ($typeCheck) {
                $fileCount = $typeCheck->files()->count();
                $files = $typeCheck->files()->with('errors')->withCount('errors')->orderBy('created_at', 'DESC');
                if ($files->get()->count()) {
                    $createdAt = $files->get()->last()->created_at;
                    $updatedAt = $files->get()->first()->created_at;
                }
                $files = $files->paginate(Consts::BASE_PAGE_SIZE);
            }
        }
        $TypeCheckNo9 = TypeCheck::where('code_check', '=', Consts::TYPE_CHECK_NUMBER_9)->get()->pluck('id')->toArray();
        $templateMail = File::whereIn('type_check_id', $TypeCheckNo9)
            ->whereNotNull('suspended_message')->orderBy('time_send_mail', 'DESC')
            ->pluck('suspended_message')->first();
//        $canUpload = $this->checkCanUpload($settingMonth);
        return [
            'files' => $files,
            'typeCheck' => $typeCheck,
            'settingMonth' => $settingMonth,
            'facilityId' => $facilityId,
            'month' => $month,
            'fileCount' => $fileCount,
            'templateMail' => $templateMail,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt
//            'canUpload' => $canUpload
        ];
    }

    public function getFacilityById($request)
    {
        $facilityId = $request->facilities_id ?? Consts::USER_ID_DEFAULT;
        return $this->facilityService->getFacilityById($facilityId);
    }
//    public function checkCanUpload($settingMonth)
//    {
//        $canUpload = false;
//        $now = Carbon::now()->format('Y-m-d');
//        $deadline = $settingMonth && $settingMonth->deadline ? date('Y-m-d', strtotime($settingMonth->deadline)) :
//            date('Y-m-d', strtotime("+1 month", strtotime(str_replace('/', '-', $month) . '-01')));
//        if ($settingMonth && $now > $deadline && $settingMonth->can_upload == true || $now <= $deadline) {
//            $canUpload = true;
//        }
//        return $canUpload;
//    }
}
