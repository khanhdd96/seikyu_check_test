<?php

namespace App\Http\Services;

use App\Consts;
use App\Http\Services\ReadFilter\FilterNo4;
use App\Messages;
use App\Models\DayOff;
use App\Models\Error;
use App\Models\File;
use App\Models\FileInInsurance;
use App\Models\FileOutsideInsurance;
use App\Models\MasterCode;
use App\Models\MasterSubCode;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckNo4Service
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $fileUpload = $request->file('file');
        $extension = strtolower($fileUpload->getClientOriginalExtension());
        $dayOff = DayOff::pluck('date_off_type', 'date_off')->toArray();
        $dayOff = array_map(function ($value) {
            return Consts::DATE_OFF[$value];
        }, $dayOff);
        $masterCodeData = MasterCode::with('masterSubCode')->withCount('masterSubCode')->get();
        $masterCode = $masterCodeData->pluck('type', 'code_name')->toArray();
        $subMasterCode = MasterSubCode::pluck('type', 'sub_code_name')->toArray();
        $omakase = array_merge($masterCode, $subMasterCode);
        $masterCode = array_map(function ($value) {
            return Consts::TYPE_CODE[$value] ?? '';
        }, $omakase);
        $subMasterCode = $masterCodeData->pluck('master_sub_code_count', 'code_name')->toArray();
        $codes = $masterCodeData->pluck('masterSubCode', 'code_name')->toArray();
        $codes = array_map(function ($value) {
            return array_column($value, 'sub_code_name');
        }, $codes);
        $insurance = FileInInsurance::pluck('fiduciary_goal', 'company_name')->toArray();
        $notInsurance = FileOutsideInsurance::pluck('fiduciary_goal', 'category_name')->toArray();
        $insurance =  array_merge($insurance, $notInsurance);
        $insurance = array_map(function ($value) {
            return Consts::INSURANCE_TYPE[$value];
        }, $insurance);
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck($request->type)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_4;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError(
                $typeCheck->id,
                $fileUpload,
                $dayOff,
                $masterCode,
                $insurance,
                $subMasterCode,
                $codes
            );
            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            $file = $response['file'];
            $errorsData = $response['errorData'];
            if ($extension != 'csv') {
                foreach ($errorsData as &$errorData) {
                    $errorData[4] = date('Y/m/d', $this->formatDate($errorData[4], $extension));
                    $errorData[10] = $this->formatTimeToHour($this->formatTime($errorData[10], $extension));
                    $errorData[11] = $this->formatTimeToHour($this->formatTime($errorData[11], $extension));
                    $errorData[12] = $this->formatTimeToHour($this->formatTime($errorData[12], $extension));
                    $errorData[44] = $this->formatDateAndTime($errorData[44]);
                    $errorData[48] = $this->formatDateAndTime($errorData[48]);
                }
            }
            if ($errors) {
                Error::insert($errors);
                $file->status = Consts::STATUS_CHECK_FILE['error'];
                $file->save();
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $typeCheck->save();
            } else {
                $settingMonth->count_file_done += 1;
            }
            $preStatus = $settingMonth->status_facilitity;
            $settingMonth->save();
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'data' => [
                    'type_check_id' => $typeCheck->id,
                    'errors' => $errors,
                    'file_name' => $file->file_name,
                    'file_id' => $file->id,
                    'errorData' => $errorsData,
                    'column' => $response['column']
                ],
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }
    public function checkError($typeCheckId, $fileUpload, $dayOff, $masterCode, $insurance, $subMasterCode, $codes)
    {
        $userId = Auth::id();
        $errors = [];
        $extension = strtolower($fileUpload->getClientOriginalExtension());
        $name = time() . $fileUpload->getClientOriginalName();
        $filePath = 'files/'.$name;
        try {
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);
            return response()->json([
                'error' => true,
                'message' => Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = env('AWS_URL') . $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $data = $this->getFileCheckData($fileUpload);
        $reg6 = '家事１５分';
        $reg7 = '家事２０分';
        $reg8 = '２０分';
        $column = Consts::EXCEL_COLUMN;
        $timeFrom1 = 360; // 6h
        $timeTo1 = 480; // 8h
        $timeFrom2 = 1080; // 18h
        $timeTo2 = 1320; // 22h
        $timeFrom3 = 1320; // 22h
        $timeTo3 = 360; // 6h
        $errorData = [];
        foreach ($data as $key => $value) {
            if ($this->isNotPassedData($value, $insurance)) {
                $hokenPos = '';
                $codeName = '';
                $codeVal = -1;
                $sortKey = '';
                $error = '';
                $omakase = $value[$column['P']];
                $hokenPos = $insurance[$value[$column['N']]];
                $timeK = $this->formatTime($value[$column['K']], $extension);
                $timeL = $this->formatTime($value[$column['L']], $extension);
                $date = $this->formatDate($value[$column['E']], $extension);
                if (!isset($masterCode[$omakase])) {
                    if ($hokenPos != "対象") {
                        if (str_starts_with($omakase, $reg6) || str_starts_with($omakase, $reg7)) {
                            $error = Consts::ERROR_CHECK_CODE['A4011'];
                            $errors[] = $this->setError($error, $file, $key, $userId);
                            $errorData[$key + 1] = $value;
                        }
                    } else {
                        if (is_int($timeL) &&
                            is_int($timeK) &&
                            in_array($timeL - $timeK, [15, 20]) &&
                            !empty($omakase) &&
                            !str_starts_with($omakase, $reg6) &&
                            !str_starts_with($omakase, $reg7)) {
                            $error = Consts::ERROR_CHECK_CODE['A4012'];
                            $errors[] = $this->setError($error, $file, $key, $userId);
                            $errorData[$key + 1] = $value;
                        }
                    }
                } else {
                    $codeName = $masterCode[$omakase];
                    if (str_contains($omakase, "割増深夜")) {
                        $codeVal = 2;
                    }
                    if (str_contains($omakase, "割増特定")) {
                        $codeVal = 3;
                    }
                    if (str_contains($omakase, "割増日祝")) {
                        $codeVal = 4;
                    }
                    if (str_contains($omakase, "割増夜朝")) {
                        $codeVal = 5;
                    }
                    if (in_array($codeName, ["使用不可", "保留"])) {
                        $error = Consts::ERROR_CHECK_CODE['A4001'];
                        $errors[] = $this->setError($error, $file, $key, $userId);
                        $errorData[$key + 1] = $value;
                        continue;
                    }
                    if (array_key_exists($omakase, $codes)) {
                        $codeVal = 1;
                    }
                    if (empty($error)) {
                        if ($codeVal == 1) {
                            if ($codeName == "割増対象外") {
                                continue;
                            }
                            if (str_starts_with($omakase, $reg6) || str_starts_with($omakase, $reg7)) {
                                $error = Consts::ERROR_CHECK_CODE['A4002'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                                continue;
                            }
                            if ($subMasterCode[$omakase] == 0) {
                                $error = Consts::ERROR_CHECK_CODE['A4003'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                                continue;
                            }
                            if ($timeK >= $timeFrom1 && $timeL <= $timeTo1) {
                                $hasSubCode = $this->checkHasSubCode($codes, $omakase, '割増夜朝');
                            }
                            if ($timeK >= $timeFrom2 && $timeL <= $timeTo2) {
                                $hasSubCode = $this->checkHasSubCode($codes, $omakase, '割増夜朝');
                            }
                            if (($timeK >= $timeFrom3 && $timeL <= $timeTo3) ||
                                ($timeK >= $timeFrom3 && $timeL >= $timeFrom3) ||
                                ($timeK <= $timeTo3 && $timeL <= $timeTo3)) {
                                $hasSubCode = $this->checkHasSubCode($codes, $omakase, '割増深夜');
                            }
                            if (isset($hasSubCode) && !$hasSubCode) {
                                $error = Consts::ERROR_CHECK_CODE['A4004'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                            }
                        } else {
                            if ($codeName == "割増対象外") {
                                $error = Consts::ERROR_CHECK_CODE['A4005'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                                continue;
                            }
                            if (str_contains($omakase, '非課税')) {
                                foreach ($codes as $keyCode => $valueCode) {
                                    if (in_array($omakase, $value) && !str_contains($keyCode, '非課税')) {
                                        $error = Consts::ERROR_CHECK_CODE['A4015'];
                                        $errors[] = $this->setError($error, $file, $key, $userId);
                                        $errorData[$key + 1] = $value;
                                        continue 2;
                                    }
                                }
                            }
                            if (str_contains($omakase, '課税')) {
                                foreach ($codes as $keyCode => $valueCode) {
                                    if (in_array($omakase, $value) &&
                                        (!str_contains($keyCode, '課税') || str_contains($keyCode, '非課税'))) {
                                        $error = Consts::ERROR_CHECK_CODE['A4017'];
                                        $errors[] = $this->setError($error, $file, $key, $userId);
                                        $errorData[$key + 1] = $value;
                                        continue 2;
                                    }
                                }
                            }
                            if (($timeL - $timeK == 20) &&
                                !empty($omakase) &&
                                !str_starts_with($omakase, $reg8)) {
                                $error = Consts::ERROR_CHECK_CODE['A4006'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                                continue;
                            }

                            if ($timeK >= $timeFrom1 && $timeL <= $timeTo1 &&
                                $timeK <= $timeTo1 && $timeL >= $timeFrom1) {
                                $hasText = str_contains($omakase, '割増夜朝');
                            }
                            if ($timeK >= $timeFrom2 && $timeL <= $timeTo2 &&
                                $timeK <= $timeTo2 && $timeL >= $timeFrom2) {
                                $hasText = str_contains($omakase, '割増夜朝');
                            }
                            if (isset($hasText) && !$hasText) {
                                $error = Consts::ERROR_CHECK_CODE['A4007'];
                                $errors[] = $this->setError($error, $file, $key, $userId);
                                $errorData[$key + 1] = $value;
                                continue;
                            }
                            if (($timeK >= $timeFrom3 && $timeL <= $timeTo3) ||
                                ($timeK >= $timeFrom3 && $timeL >= $timeFrom3) ||
                                ($timeK <= $timeTo3 && $timeL <= $timeTo3)) {
                                if (!str_contains($omakase, '割増深夜')) {
                                    $error = Consts::ERROR_CHECK_CODE['A4016'];
                                    $errors[] = $this->setError($error, $file, $key, $userId);
                                    $errorData[$key + 1] = $value;
                                    continue;
                                }
                            }
                            if (date('N', $date) >= 6 || array_key_exists(date('Y-m-d', $date), $dayOff)) {
                                if (!str_contains($omakase, '割増土日祝')) {
                                    $error = Consts::ERROR_CHECK_CODE['A4008'];
                                    $errors[] = $this->setError($error, $file, $key, $userId);
                                    $errorData[$key + 1] = $value;
                                    continue;
                                } else {
                                    if (!str_contains($omakase, '割増日祝') &&
                                        !str_contains($omakase, '割増特定') &&
                                        !str_contains($omakase, '割増深夜')) {
                                        $error = Consts::ERROR_CHECK_CODE['A4013'];
                                        $errors[] = $this->setError($error, $file, $key, $userId);
                                        $errorData[$key + 1] = $value;
                                        continue;
                                    }
                                    if (($timeK >= $timeFrom3 && $timeL <= $timeTo3) ||
                                        ($timeK >= $timeFrom3 && $timeL >= $timeFrom3) ||
                                        ($timeK <= $timeTo3 && $timeL <= $timeTo3)) {
                                        $hasCode = false;
                                        foreach ($codes as $code) {
                                            if (in_array($omakase, $code)) {
                                                foreach ($code as $subCode) {
                                                    if (str_contains($subCode, '割増深夜')) {
                                                        $hasCode = true;
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }
                                        if (!$hasCode) {
                                            $error = Consts::ERROR_CHECK_CODE['A4014'];
                                            $errors[] = $this->setError($error, $file, $key, $userId);
                                            $errorData[$key + 1] = $value;
                                            continue;
                                        }
                                    }
                                }
                                if (!str_contains($omakase, '割増日祝')) {
                                    $error = Consts::ERROR_CHECK_CODE['A4009'];
                                    $errors[] = $this->setError($error, $file, $key, $userId);
                                    $errorData[$key + 1] = $value;
                                    continue;
                                } else {
                                    if (!str_contains($omakase, '割増日祝') &&
                                        !str_contains($omakase, '割増特定') &&
                                        !str_contains($omakase, '割増深夜')) {
                                        $error = Consts::ERROR_CHECK_CODE['A4013'];
                                        $errors[] = $this->setError($error, $file, $key, $userId);
                                        $errorData[$key + 1] = $value;
                                        continue;
                                    }
                                    if (($timeK >= $timeFrom3 && $timeL <= $timeTo3) ||
                                        ($timeK >= $timeFrom3 && $timeL >= $timeFrom3) ||
                                        ($timeK <= $timeTo3 && $timeL <= $timeTo3)) {
                                        $hasCode = false;
                                        foreach ($codes as $code) {
                                            if (in_array($omakase, $code)) {
                                                foreach ($code as $subCode) {
                                                    if (str_contains($subCode, '割増深夜')) {
                                                        $hasCode = true;
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }
                                        if (!$hasCode) {
                                            $error = Consts::ERROR_CHECK_CODE['A4014'];
                                            $errors[] = $this->setError($error, $file, $key, $userId);
                                            $errorData[$key + 1] = $value;
                                            continue;
                                        }
                                    }
                                }
                                if (!str_contains($omakase, '割増特定')) {
                                    $error = Consts::ERROR_CHECK_CODE['A4010'];
                                    $errors[] = $this->setError($error, $file, $key, $userId);
                                    $errorData[$key + 1] = $value;
                                } else {
                                    if (!str_contains($omakase, '割増日祝') &&
                                        !str_contains($omakase, '割増特定') &&
                                        !str_contains($omakase, '割増深夜')) {
                                        $error = Consts::ERROR_CHECK_CODE['A4013'];
                                        $errors[] = $this->setError($error, $file, $key, $userId);
                                        $errorData[$key + 1] = $value;
                                        continue;
                                    }
                                    if (($timeK >= $timeFrom3 && $timeL <= $timeTo3) ||
                                        ($timeK >= $timeFrom3 && $timeL >= $timeFrom3) ||
                                        ($timeK <= $timeTo3 && $timeL <= $timeTo3)) {
                                        $hasCode = false;
                                        foreach ($codes as $code) {
                                            if (in_array($omakase, $code)) {
                                                foreach ($code as $subCode) {
                                                    if (str_contains($subCode, '割増深夜')) {
                                                        $hasCode = true;
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }
                                        if (!$hasCode) {
                                            $error = Consts::ERROR_CHECK_CODE['A4014'];
                                            $errors[] = $this->setError($error, $file, $key, $userId);
                                            $errorData[$key + 1] = $value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return [
            'errors' => $errors,
            'file' => $file,
            'errorData' => $errorData,
            'column' => $data[0]
        ];
    }
    public function getNumberString($string)
    {
        date_default_timezone_set('Asia/Tokyo');
        $formatter = new \IntlDateFormatter(
            'ja_JP@calendar=japanese',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            // 'Europe/Madrid',
            'Asia/Tokyo',
            \IntlDateFormatter::TRADITIONAL,
            'Gy年M月' //Age and year (regarding the age)
        );
        $time = $formatter->parse($string);
        return date('Ym', $time);
    }

    public function saveFile($typeCheckId, $fileUpload, $filePath, $id = null)
    {
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = env('AWS_URL') . $filePath;
        $file->file_id = $id;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        return $file;
    }
    public function getFileCheckData($file)
    {
        $path = $file->getRealPath();
        $extension = strtolower($file->getClientOriginalExtension());
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
//        $filter = new FilterNo4();
//        $reader->setReadFilter($filter);
        $spreadsheet = $reader->load($path);
        $sheetDatas = $spreadsheet->getActiveSheet()->toArray();
        $professors = [];
        if ($extension == 'csv') {
            if (($fh = fopen($path, 'r')) !== false) {
                while (($data = fgetcsv($fh)) !== false) {
                    $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                }
                fclose($fh);
            }
            $sheetDatas = $professors;
        }
        return $sheetDatas;
    }
    public function isNotPassedData($value, $insurance)
    {
        $column = Consts::EXCEL_COLUMN;
        $reg = ['おまかせケア5分', 'おまかせケア10分', 'おまかせ家事5分', 'おまかせ家事10分', '★'];
        $condition2 = true;
        $condition1 = $value[$column['H']] == '実績';
        foreach ($reg as $regex) {
            if (str_contains($value[$column['P']], $regex)) {
                $condition2 = false;
                break;
            }
        }
        $condition3 = isset($insurance[$value[$column['N']]]);
        return $condition1 && $condition2 && $condition3;
    }
    public function formatTime($time, $extension)
    {
        if ($extension == 'csv') {
            $time = explode(':', $time);
            if (count($time) == 2) {
                $time = $time[0] * 60 + $time[1];
            } else {
                $time = '';
            }
        } else {
            $time = intval(round($time * 60 * 24));
        }
        return $time;
    }
    public function formatDate($date, $extension = null)
    {
        if ($extension != 'csv' && $date == intval($date)) {
            $format = ($date - 25569) * 86400;
        } else {
            $format = strtotime($date);
        }
        return $format;
    }
    public function formatTimeToHour($time)
    {
        $hour = intval($time / 60);
        $minute = $time % 60;
        $strMinute = $minute >= 10 ? $minute : '0' . $minute;
        return $hour . ':' . $strMinute;
    }
    public function formatDateAndTime($data)
    {
        $data = explode('.', $data);
        if (count($data) == 2) {
            $date = date('Y/m/d', $this->formatDate($data[0]));
            $hour = '0.' . $data[1];
            $hour = intval(round($hour * 24 * 60));
            return $date . ' ' . $this->formatTimeToHour($hour);
        }
        return '';
    }
    public function setError($error, $file, $key, $userId)
    {
        $now = Carbon::now();
        return [
            'error_position' => $key + 1 .'行目',
            'error_code' => array_search($error, Consts::ERROR_CHECK_CODE),
            'files_id' => $file->id,
            'message' => $error,
            'created_by' => $userId,
            'updated_by' => $userId,
            'created_at' => $now,
            'updated_at' => $now
        ];
    }
    public function checkHasSubCode($codes, $omakase, $text)
    {
        $hasSubCode = false;
        foreach ($codes[$omakase] as $subCode) {
            if (str_contains($subCode, $text)) {
                $hasSubCode = true;
                break;
            }
        }
        return $hasSubCode;
    }
}
