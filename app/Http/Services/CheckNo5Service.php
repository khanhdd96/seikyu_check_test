<?php

namespace App\Http\Services;

use App\Consts;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckNo5Service
{

    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        DB::beginTransaction();
        $codeCheck = $request->type == Consts::TYPE_CHECK_NUMBER_5_M ? Consts::TYPE_CHECK_NUMBER_5_M :
            ($request->type == Consts::TYPE_CHECK_NUMBER_5_S ? Consts::TYPE_CHECK_NUMBER_5_S :
                Consts::TYPE_CHECK_NUMBER_5_TH);
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck($codeCheck)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = $codeCheck;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        $response = $this->checkError($request, $typeCheck->id);
        if (!is_array($response) && $response->status() != 200) {
            return $response;
        }
        $errors = $response['errors'];
        if ($request->type == Consts::TYPE_CHECK_NUMBER_5_M) {
            $file = $response['file'];
            $extension = $response['extension'];
        } else {
            $file = $response['file'];
        }
        try {
            if ($errors) {
                if ($request->type !== Consts::TYPE_CHECK_NUMBER_5_M) {
                    Error::insert($errors);
                }
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $settingMonth->count_file_done = 0;
                $typeCheck->save();
            } else {
                $settingMonth->count_file_done += 1;
            }
            $preStatus = $settingMonth->status_facilitity;
            $settingMonth->save();
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($file->filepath);
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        if ($request->type == Consts::TYPE_CHECK_NUMBER_5_M) {
            return response()->json([
                'error' => false,
                'data' => [
                    'type_check_id' => $typeCheck->id,
                    'errors' => $errors,
                    'file_name' => ['csv' => $file['csv']->file_name, 'excel' => $file[$extension]->file_name],
                    'file_id' => ['csv' => $file['csv']->id, 'excel' => $file[$extension]->id],
                ],
            ], 200);
        }
        return response()->json([
            'error' => false,
            'data' => [
                'type_check_id' => $typeCheck->id,
                'errors' => $errors,
                'file_name' => $request->file->getClientOriginalName(),
                'file_id' => $file->id
            ],
        ], 200);
    }

    public function checkError($request, $typeCheckId)
    {
        $month = $request->month;
        if ($request->type == Consts::TYPE_CHECK_NUMBER_5_M) {
            $result = $this->checkErrorFileM($request, $typeCheckId, $month);
            return ['errors' => $result['errors'], 'file' => $result['files'], 'extension' => $result['extension']];
        } else {
            $fileUpload = $request->file('file');
            $path = $fileUpload->getRealPath();
            $extension = strtolower($request->file->getClientOriginalExtension());
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(true);
            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($path);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            $professors = [];
            if ($extension == 'csv') {
                if (($fh = fopen($path, 'r')) !== false) {
                    while (($data = fgetcsv($fh)) !== false) {
                        $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                    }
                    fclose($fh);
                }
                $sheetData = $professors;
            }
            $name = time() . $fileUpload->getClientOriginalName();
            $filePath = 'files/' . $name;
            try {
                \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
                $filePath = env('AWS_URL') . $filePath;
            } catch (\Exception $e) {
                \Storage::disk('s3')->delete($filePath);
                return response()->json([
                    'error' => true,
                    'message' => Messages::UPLOAD_FILE_ERROR,
                ], 500);
            }
            $file = new File();
            $file->file_name = $fileUpload->getClientOriginalName();
            $file->type_check_id = $typeCheckId;
            $file->filepath = $filePath;
            $file->status = Consts::STATUS_CHECK_FILE['success'];
            $file->save();
            $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
            /**  Loop through all the remaining files in the list  **/
            $date = $request->type == Consts::TYPE_CHECK_NUMBER_5_TH ?
                    intval($sheetData[0][9]) : intval($sheetData[0][10]);
            $errors = $request->type == Consts::TYPE_CHECK_NUMBER_5_TH ?
                $this->checkFileTh($sheetData, $date, $userId, $file) :
                ($request->type == Consts::TYPE_CHECK_NUMBER_5_S ?
                    $this->checkFileS($sheetData, $date, $userId, $file, $month) : '');
            if ($errors) {
                $file->status = Consts::STATUS_CHECK_FILE['error'];
                $file->save();
            }
            return ['errors' => $errors, 'file' => $file, 'extension' => $extension];
        }
    }

    public function checkErrorFileM($request, $typeCheckId, $month)
    {
        $now = date('y-m-d');
        $fileUploads = $request->file('file');
        $dataSheets = [];
        $dataFiles = [];
        $fileId = null;
        $typeCheck = TypeCheck::find($typeCheckId);
        DB::beginTransaction();
        try {
            foreach ($fileUploads as $fileUpload) {
                $extension = strtolower($fileUpload->getClientOriginalExtension());
                $filePath = $fileUpload->getRealPath();
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($filePath);
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $reader->setReadDataOnly(true);

                /**  Load $inputFileName to a Spreadsheet Object  **/
                $spreadsheet = $reader->load($filePath);
                $sheetDatas = $spreadsheet->getActiveSheet()->toArray();
                $professors = [];
                if ($extension == 'csv') {
                    if (($fh = fopen($filePath, 'r')) !== false) {
                        while (($data = fgetcsv($fh)) !== false) {
                            $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                        }
                        fclose($fh);
                    }
                    $sheetDatas = $professors;
                }
//                $fileName = $fileUpload->getClientOriginalName();
                $name = time() . $fileUpload->getClientOriginalName();
                $filePath = 'files/' . $name;
                // Save file
                $file = $this->saveFile($typeCheckId, $fileUpload, $filePath, $fileId);
                $fileId = $file->id;
                $this->uploadFileS3($filePath, $fileUpload);

                $dataSheets[$extension] = $sheetDatas;
                $dataFiles[$extension] = $file;
            }
            // filter data excel
            $errors = [];
            $extension = isset($dataSheets['xlsx']) ? 'xlsx' : 'xls';
            $date = intval($dataSheets['csv'][0][10]);
            $dataCsv = $this->seikyuCheckCsv($dataSheets['csv'], $dataFiles['csv'], $date);
            $dataSeikyuCheckNo5Csv = $this->seikyuCheckFilterDataCsv($dataSheets['csv']);
            $dataExcels = $this->seikyuCheckDataWithExcel(
                $sheetDatas,
                $dataSeikyuCheckNo5Csv,
                $dataFiles['csv'],
                $month
            );
            $errors = array_merge($dataCsv['error'], $dataExcels);
            if (count($errors) > 0) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $file->status = Consts::STATUS_CHECK_FILE['error'];
                $dataFiles['csv']->status = Consts::STATUS_CHECK_FILE['error'];
                $dataFiles['csv']->save();
                $dataFiles[$extension]->status = Consts::STATUS_CHECK_FILE['error'];
                $dataFiles[$extension]->save();
                $file->save();
                $typeCheck->save();
            } else {
                $typeCheck->save();
            }
            //update status setting month
//            $this->model->updateStatusFacilitity($typeCheckId);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($dataFiles['csv']->filepath);
            \Storage::disk('s3')->delete($dataFiles[$extension]->filepath);

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        return ['errors' => $errors, 'files' => $dataFiles, 'extension' => $extension];
    }

    public function checkFileS($sheetData, $date, $userId, $file, $month)
    {
        $start = 1;
        $errors = [];
        $checkDate = true;
        $month = str_replace('/', '', $month);
        for ($row = 0; $row < count($sheetData); $row++) {
            if (isset($sheetData[$row][10]) && $row == 0 && $sheetData[$row][10] != $month) {
                $errors[] = [
                    'error_position' => ($row + 1) . "行目",
                    'error_code' => 'A5201',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A5201'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                ];
            }
            if ($row >= $start) {
                if (isset($sheetData[$row][4]) && $sheetData[$row][4]
                    && !preg_match('/^\d{6}$/', $sheetData[$row][4]) && $checkDate) {
                    $start++;
                    continue;
                }
                $checkDate = false;
                if (isset($sheetData[$row][4]) &&
                    (intval($sheetData[$row][4]) >= $date || $date - intval($sheetData[$row][4]) > 1)
                    && preg_match('/^\d{6}$/', $sheetData[$row][4])) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A5202',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A5202'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                }
                if (isset($sheetData[$row][8])
                    && !preg_match("/^A\d+$/", $sheetData[$row][8]) && $sheetData[$row][8] != null) {
                    if (preg_match("/^\d+$/", $sheetData[$row][8]) && strlen($sheetData[$row][8]) >= 4) {
                        continue;
                    }
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A5203',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A5203'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                }
            }
        }
        return $errors;
    }

    public function checkFileTh($sheetData, $date, $userId, $file)
    {
        $start = 1;
        $errors = [];
        $hasMonthBefore = false;
        $checkDate = true;
        for ($row = 0; $row < count($sheetData); $row++) {
//            if (isset($sheetData[$row][9]) && $row == 0 && $sheetData[$row][9] != $date) {
//                $errors[] = [
//                    'error_position' => ($row + 1) . "行目",
//                    'error_code' => 'A5301',
//                    'files_id' => $file->id,
//                    'message' => Consts::ERROR_CHECK_CODE['A5301'],
//                    'created_at' => Carbon::now(),
//                    'updated_at' => Carbon::now(),
//                    'created_by' => $userId,
//                    'updated_by' => $userId,
//                ];
//            }
            if ($row >= $start) {
                if (isset($sheetData[$row][4]) && $sheetData[$row][4]
                    && !preg_match('/^\d{6}$/', $sheetData[$row][4]) && $checkDate) {
                    $start++;
                    continue;
                }
                $checkDate = false;
                if (isset($sheetData[$row][4]) && $sheetData[$row][4] >= $date
                    && preg_match('/^\d{6}$/', $sheetData[$row][4])) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A5303',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A5303'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                }
                if (isset($sheetData[$row][4]) && $date - intval($sheetData[$row][4]) == 1
                    && preg_match('/^\d{6}$/', $sheetData[$row][4])) {
                    $hasMonthBefore = true;
                }
            }
        }
        if (!$hasMonthBefore) {
            $errors[] = [
                'error_position' => "E列目",
                'error_code' => 'A5302',
                'files_id' => $file->id,
                'message' => Consts::ERROR_CHECK_CODE['A5302'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
            ];
        }
        return $errors;
    }

    public function saveFile($typeCheckId, $fileUpload, $filePath, $id = null)
    {
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = env('AWS_URL').$filePath;
        $file->file_id = $id;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();

        return $file;
    }

    public function uploadFileS3($filePath, $fileUpload)
    {
        try {
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);

            return response()->json([
                'error' => true,
                'message' =>  Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
    }

    public function seikyuCheckFilterDataCsv($sheetDatas)
    {
        $filterDatas = [];
        foreach ($sheetDatas as $key => $value) {
            if (isset($value[22]) && $value[22] != '' && intval($value[22]) == 0) {
                $filterDatas[$key] = $value;
            }
        }
        return $filterDatas;
    }

    public function seikyuCheckCsv($sheetDatas, $file, $date)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        // check date
        $errors = [];
//        if (isset($sheetDatas[0][10]) && $sheetDatas[0][10] != $date) {
//            $errors[] = [
//                'error_position' => "CSV: 1行目",
//                'error_code' => 'A5201',
//                'files_id' => $file->id,
//                'message' => Consts::ERROR_CHECK_CODE['A5201'],
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//                'created_by' => $userId,
//                'updated_by' => $userId,
//            ];
//        }
        $dataSeikyuCheckNo5Csv = $this->seikyuCheckFilterDataCsv($sheetDatas);
        $errors = array_merge($errors, $this->seikyuCheckDataCsv($dataSeikyuCheckNo5Csv, $file, $date));
        if (count($errors) > 0) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }

        return ['csv' => $dataSeikyuCheckNo5Csv, 'error' => $errors];
    }

    public function addTypeCheck($request, $settingMonthId)
    {
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        try {
            DB::beginTransaction();
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_5_M)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_3;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            DB::commit();

            return ['settingMonth' => $settingMonth, 'typeCheck' => $typeCheck];
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function seikyuCheckDataCsv($dataSeikyuCheckNo5Csv, $file, $date)
    {
        $start = 1;
        $errors = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $checkDate = true;
        foreach ($dataSeikyuCheckNo5Csv as $row => $value) {
            if (isset($value[8]) && (
                substr($value[8], 0, 1) == 'A' ||
                substr($value[8], 0, 1) == 'a')) {
                $errors[] = [
                    'error_position' => 'CSV: ' . ($row + 1) . "行目",
                    'error_code' => 'A5105',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A5105'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                ];
            }
            if ($row >= $start) {
                if (isset($value[4]) && $value[4]
                    && !preg_match('/^\d{6}$/', $value[4]) && $checkDate) {
                    $start++;
                    continue;
                }
                $checkDate = false;
                if (isset($value[4]) && ($date - intval($value[4])) != 1
                    && preg_match('/^\d{6}$/', $value[4])) {
                    $errors[] = [
                        'error_position' => 'CSV: ' . ($row + 1) . "行目",
                        'error_code' => 'A5102',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A5102'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                }
            }
        }
        return $errors;
    }

    public function seikyuCheckDataWithExcel($sheetDatas, $dataCsvs, $file, $month)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $filterDatas = [];
        $errors = [];
        mb_internal_encoding('UTF-8');
        mb_regex_encoding('UTF-8');
        $times = mb_split('：', $sheetDatas[1][0]);
        $time = end($times);
        $month = str_replace('/', ' ', $month);
        if ($time != $month) {
            $errors[] = [
                'error_position' => 'Excel:  1行目',
                'error_code' => 'A5101',
                'files_id' => $file->id,
                'message' => Consts::ERROR_CHECK_CODE['A5101'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
            ];
        }
        foreach ($sheetDatas as $key => $value) {
            if ($value[5] != 0 || $value[8] != 0) {
                $filterDatas[$key] = $value;
            }
        }
        foreach ($dataCsvs as $row => $dataCsv) {
            $hasErrorInHAndD = true;
            $hasErrorInEAndA = true;
            foreach ($filterDatas as $key => $value) {
                if ($dataCsv[7] == $value[3]) {
                    $hasErrorInHAndD = false;
                    if ($this->getNumberString($value[0]) == $dataCsv[4]) {
                        $hasErrorInEAndA = false;
                    }
                }
            }
            if ($hasErrorInHAndD) {
                $errors[] = [
                    'error_position' => 'CHECK: '.($row + 1) . "行目",
                    'error_code' => 'A5103',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A5103'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                ];
            }
            if ($hasErrorInEAndA && !$hasErrorInHAndD) {
                $errors[] = [
                    'error_position' => 'CHECK: '.($row + 1) . "行目",
                    'error_code' => 'A5104',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A5104'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                ];
            }
        }
        return $errors;
    }

    public function getNumberString($string)
    {
        date_default_timezone_set('Asia/Tokyo');
        $formatter = new \IntlDateFormatter(
            'ja_JP@calendar=japanese',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            // 'Europe/Madrid',
            'Asia/Tokyo',
            \IntlDateFormatter::TRADITIONAL,
            'Gy年M月' //Age and year (regarding the age)
        );
        $time = $formatter->parse($string);
        return date('Ym', $time);
    }
}
