<?php

namespace App\Http\Services;

use App\Consts;
use App\Models\Department;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomepageServices
{
    protected $user;
    protected $settingMonth;
    protected $facilityService;
    protected $facility;
    protected $department;
    protected $typeCheck;
    protected $file;
    protected $fileCheckService;

    public function __construct(
        User $user,
        SettingMonth $settingMonth,
        FacilityService $facilityService,
        Facility $facility,
        Department $department,
        TypeCheck $typeCheck,
        File $file,
        FileCheckService $fileCheckService
    ) {
        $this->user = $user;
        $this->settingMonth = $settingMonth;
        $this->facilityService = $facilityService;
        $this->facility = $facility;
        $this->department = $department;
        $this->typeCheck = $typeCheck;
        $this->file = $file;
        $this->fileCheckService = $fileCheckService;
    }

    public function index($request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $user = $this->user->find($userId);
        $facilitys = $user->facilitys()->where('is_active', 1)->get();
        if (Auth::user()->is_admin == Consts::ADMIN) {
            $facilitys = Facility::where('is_active', 1)->get();
        }
        if ($facilitys->isEmpty()) {
            abort(404);
        }
        $facilityIds = $facilitys->pluck('id')->toArray();
        $departmentIds = array_unique($facilitys->pluck('departments_id')->toArray());
        $departments = $this->department->whereIn('id', $departmentIds)->get();
        $departmentId = $request->department_id ?? $departments->first()->id;
        $facilitys = $this->facility->where('departments_id', $departmentId)
                ->whereIn('id', $facilityIds)
                ->where('is_active', 1)->get();
        $data = [
            'facilitys' => $facilitys,
            'departments' => $departments,
            'facility' => null,
            'department' => null,
            'type_checks' => null,
            'setting_month' => null,
            'data_type_check_detail' => null,
            'canUpload' => null,
        ];
        if ($facilitys->isEmpty()) {
            return $data;
        }
        $getDetail = $this->getDetailFacility($user, $request, $facilitys);
        $date = str_replace('/', '-', $getDetail['setting_month']->year_month);
        $idTypCheck = TypeCheckNo1Admin::where('date', $date)->first();
        $typeCheck1Admin = null;
        if ($idTypCheck) {
            $idTypCheck = $idTypCheck->id;
            $typeCheck1Admin = TypeCheckNo1AdminStep::where('facility_id', $getDetail['facility']->id)
                ->where('type_check_no_1_admin_id', $idTypCheck)->first();
        }
        $data = [
            'facilitys' => $facilitys,
            'departments' => $departments,
            'facility' => $getDetail['facility'],
            'department' => $getDetail['department'],
            'type_checks' => $getDetail['type_checks'],
            'setting_month' => $getDetail['setting_month'],
            'data_type_check_detail' => $getDetail['dataTypeCheckDetail'],
            'canUpload' => $getDetail['canUpload'],
            'typeCheck1Admin' => $typeCheck1Admin
        ];

        return $data;
    }

    public function getDetailFacility($user, $request, $facilitys)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = date('Y/m');
        $canUpload = true;
        if ($request->setting_month) {
            if ($date !=  $request->setting_month) {
                $canUpload = false;
            }
            $date = $request->setting_month;
        }
        $facility = $facilitys->first();
        if ($request->facility_id) {
            $facilitysIds = $facilitys->pluck('id')->toArray();
            if (in_array($request->facility_id, $facilitysIds)) {
                $facility = $this->facility->find($request->facility_id);
            }
        }
        $department = $this->department->find($facility->departments_id);
        $settingMonth = $facility->settingMonth()->where('year_month', 'like', $date)->first();
        if (!$settingMonth) {
            $settingMonth = new SettingMonth();
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $date;
            $settingMonth->facilities_id = $facility->id;
            $settingMonth->can_upload = $canUpload;
            $settingMonth->save();
        }
        $typeChecks = $facility->list_type_check;
        // get type Check detail
        $dataTypeCheckDetail = $this->getDataTypeCheckDetail($facility->id, $typeChecks, $request);
        $typeChecks = $this->typeCheck->where('setting_months_id', $settingMonth->id)
            ->withCount('fileNotHold')
            ->withCount('fileError')
            ->with('fileNotHold')
            ->withCount(['checkTimes', 'checkErrorTimes'])
            ->with('files')->get();
        $newTypeChecks = [];
        //conUplaod
        $canUpload = false;
        $canUpload = $this->fileCheckService->checkCanUpload($settingMonth, $date, $facility->id);
        // dd($canUpload);
        foreach ($typeChecks as $typeCheck) {
            $newTypeChecks[$typeCheck->code_check] = $typeCheck;
        }
        return [
            'facility' => $facility,
            'department' => $department,
            'type_checks' => $newTypeChecks,
            'setting_month' => $settingMonth,
            'dataTypeCheckDetail' => $dataTypeCheckDetail,
            'canUpload' => $canUpload,
        ];
    }

    public function getDataTypeCheckDetail($facility_id, $typeChecks, $requets)
    {
        $month = date('Y/m');
        $requets->month = $month;
        $requets->facilities_id = $facility_id;
        if (isset($requets->setting_month)) {
            $requets->month = $requets->setting_month;
        }
        $typeChecks = explode(',', $typeChecks);
        $data = [];
        foreach ($typeChecks as $typeCheck) {
            $requets->type_check = $typeCheck;
            $data[$typeCheck] = $this->fileCheckService->index($requets);
        }

        return $data;
    }
}
