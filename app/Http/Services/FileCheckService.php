<?php

namespace App\Http\Services;

use App\Consts;
use App\Messages;
use App\Models\Department;
use App\Models\Facility;
use App\Models\File;
use App\Models\Role;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Services\SendMailService;
use phpDocumentor\Reflection\Types\Null_;

class FileCheckService
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        User $user,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->user = $user;
        $this->sendMailService = $sendMailService;
    }
    public function index($request)
    {
        $type = $request->type_check;
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $user = $this->user->find($userId);
        $facilities = $user->facilitys()->where('is_active', '=', true)->get();
        if (Auth::user()->is_admin == Consts::ADMIN) {
            $facilities = Facility::where('is_active', 1)->get();
        }
        if ($facilities->isEmpty()) {
            abort(404);
        }
        $facilityIds = $facilities->pluck('id')->toArray();
        $departmentIds = array_unique($facilities->pluck('departments_id')->toArray());
        $departments = Department::whereIn('id', $departmentIds)->get();
        $departmentId = $request->department_id ?? $departments->first()->id;
        $facilities = Facility::where('departments_id', '=', $departmentId)->whereIn('id', $facilityIds)
                                ->where('is_active', '=', true)->get();
        $month = $request->month ?? Carbon::now()->format('Y/m');
        $facilityId = $request->facility_id ?? ($facilities->first()->id ?? abort(404));
        $settingMonth = $this->model->where('facilities_id', '=', $facilityId)
            ->where('year_month', '=', $month)->first();
        $files = [];
        $typeCheck = [];
        $fileCount = 0;
        $createdAt = '';
        $updatedAt = '';
        if ($settingMonth) {
            $typeCheck = $settingMonth->typeCheck($type)->first();
            if ($typeCheck) {
                $fileCount = $files = $typeCheck->files()->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])
                                                            ->count();
                $files = $typeCheck->files()->with('errors')->withCount('errors');
                if ($type == Consts::TYPE_CHECK_NUMBER_3 || $type == Consts::TYPE_CHECK_NUMBER_5_M) {
                    $files = $files->with('file')->with('file', function ($query) {
                        $query->with('errors')->withCount('errors');
                    })->where('file_id', '=', null);
                }
                if ($type == Consts::TYPE_CHECK_NUMBER_2 || $type == Consts::TYPE_CHECK_NUMBER_1) {
                    $files = $files->with('files', function ($query) {
                        $query->with('errors')->withCount('errors');
                    })->where('file_id', '=', null);
                    $fileCount = $typeCheck->files()
                        ->whereNull('file_id')
                        ->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])
                        ->count();
                }
//                if ($type == Consts::TYPE_CHECK_NUMBER_1) {
//                    $files = $files->with('warnings')->withCount('warnings')->where('file_id', '=', null);
//                    $files = $files->with('files', function ($query) use ($type) {
//                        $query->with('errors')->withCount('errors');
//                    })->where('file_id', '=', null);
//                    $fileCount = $typeCheck->files()
//                        ->whereNull('file_id')
//                        ->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])
//                        ->count();
//                }
                $files = $files->orderBy('created_at', 'DESC');
                if ($files->get()->count()) {
                    $createdAt = $files->get()->last()->created_at;
                    $updatedAt = $files->get()->first()->created_at;
                }
                $files = $files->paginate(Consts::BASE_PAGE_SIZE);
            }
        } else {
            $settingMonth = $this->createSettingMonth($facilityId, $request);
        }
        $idTypCheck = TypeCheckNo1Admin::where('date', date('Y-m'))->first();
        $typeCheck1Admin = null;
        if ($idTypCheck) {
            $idTypCheck = $idTypCheck->id;
            $typeCheck1Admin = TypeCheckNo1AdminStep::where('facility_id', $facilityId)
                ->where('type_check_no_1_admin_id', $idTypCheck)->first();
        }
        $canUpload = $this->checkCanUpload($settingMonth, $month, $facilityId);
        return [
            'files' => $files,
            'typeCheck' => $typeCheck,
            'settingMonth' => $settingMonth,
            'facilityId' => $facilityId,
            'month' => $month,
            'fileCount' => $fileCount,
            'canUpload' => $canUpload,
            'facilities' => $facilities,
            'departments' => $departments,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt,
            'typeCheck1Admin' => $typeCheck1Admin
        ];
    }

    public function getdetailCheck($request)
    {
        $typeCheck = Consts::TYPE_MENU[$request->type_check];
        $facilityId = $request->facility_id;
        $month = $request->month ??date('Y/m');
        //Get setting month
        $settingMonth = $this->model->whereFacilitiesId($facilityId)
                ->whereYearMonth($month)->first();
        //Get List Type Check
        $result = new \stdClass();
        $result->title_check = Consts::TITLE_NUMBER[$request->type_check] . ' ' .
            Consts::TITLE[$request->type_check];
        $result->time_create = 'N/A';
        $result->time_update = 'N/A';
        $result->number_check = 0;
        $result->has_page = false;
        $result->status_check = Consts::STATUS_CHECK[4];
        $result->color_check = Consts::CLASS_STATUS_CHECK_FILE_DETAIL[4];
        $result->detail_check = [];
        if (!$settingMonth) {
            return $result;
        }
        $dataTypeCheck = TypeCheck::whereSettingMonthsId($settingMonth->id)->whereCodeCheck($typeCheck)->first();
        if (!$dataTypeCheck) {
            return $result;
        }
        $files = $dataTypeCheck->files();
        $countFile = $dataTypeCheck->files();
        if ($typeCheck == 3 || $typeCheck == '5-M') {
            $files = $files
                ->where('file_id', null);
            $countFile = $countFile
                ->where('file_id', null);
        }
        if ($typeCheck == Consts::TYPE_CHECK_NUMBER_2) {
            $files = $files->with('files', function ($query) {
                $query->with('errors')->withCount('errors');
            })->where('file_id', '=', null);
            $countFile = $countFile->where('file_id', null);
        }
        if ($typeCheck == Consts::TYPE_CHECK_NUMBER_1) {
            $files = $files->with('warnings')->withCount('warnings')->where('file_id', '=', null);
            $countFile = $countFile->where('file_id', null);
        }
        if (!$files) {
            return $result;
        }
        $allFile = $files->get();
        $result->time_create = $allFile->count() ? $allFile->last()->created_at->format('Y/m/d') : 'N/A';
        $result->time_update = $allFile->count() ? $allFile->first()->created_at->format('Y/m/d') : 'N/A';
        $result->number_check = $countFile->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])->count();
        $result->status = $dataTypeCheck->status;
        $result->status_check = $dataTypeCheck->status ? Consts::STATUS_CHECK[$dataTypeCheck->status] :
            Consts::STATUS_CHECK[4] ;
        if ($dataTypeCheck->status) {
            $result->color_check = Consts::CLASS_STATUS_CHECK_FILE_DETAIL[$dataTypeCheck->status];
        }
        $files = $files->orderBy('created_at', 'DESC')
            ->paginate(Consts::BASE_PAGE_SIZE);
        if ($files->currentPage() < $files->lastPage()) {
            $result->has_page = true;
        }
        $result->detail_check = [];
        foreach ($files as $file) {
            $resultFile = new \stdClass();
            $resultFile->file_name = $file->file_name;
            $resultFile->class = $file->status == Consts::STATUS_CHECK_FILE['success'] ? 'btn-green-context' :
                ($file->status == Consts::STATUS_CHECK_FILE['error'] ? 'btn-mint-context' : 'btn-blue-context');
            $resultFile->image = $file->status == Consts::STATUS_CHECK_FILE['success'] ?
                asset('icons/verify.svg') : ($file->status == Consts::STATUS_CHECK_FILE['error'] ?
                    asset('icons/forbidden.svg') : asset('icons/timer.svg'));
            $resultFile->created_at = $file->created_at->format('Y/m/d - H:i');
            $resultFile->status_text = Consts::STATUS_CHECK[$file->status];
            $resultFile->status = $file->status;
            $resultFile->reason = nl2br(e($file->reason));
            $resultFile->data_error = [];
            $resultFile->id = $file->id;
            $resultFile->filePath = $file->filepath;
            $dataError = $file->errors->toArray();
            if ($typeCheck == 3 || $typeCheck == '5-M') {
                $resultFile->file_name.= '、' . $file->file->file_name;
                $dataError = $file->errors->merge($file->file->errors)->toArray();
            }
            if ($typeCheck == Consts::TYPE_CHECK_NUMBER_2 || $typeCheck == Consts::TYPE_CHECK_NUMBER_1) {
                $fileName = $file->file_name;
                $errorCount = $file->errors_count;
                $dataError = $file->errors;
                foreach ($file->files as $fileCheck) {
                    $fileName .= ', ' . $fileCheck->file_name;
                    $errorCount += $fileCheck->errors_count;
                    $dataError = $dataError->merge($fileCheck->errors);
                }
                $resultFile->file_name = $fileName;
                $resultFile->errors = $dataError;
            }
            $resultFile->error_count = count($dataError);
            foreach ($dataError as $error) {
//                if ($typeCheck == Consts::TYPE_CHECK_NUMBER_1) {
//                    if ($error['type'] == Consts::TYPE_WARNING) {
//                        $resultFile->data_error[] = $error['error_position'] . '：' .
//                            Consts::ERROR_CHECK_CODE[$error['error_code']];
//                    }
//                } else {
                $resultFile->data_type_error[] = $error['type'];
                $errorMessage = $error['error_code'] != ' ' ? '：' . Consts::ERROR_CHECK_CODE[$error['error_code']] : '';
                $resultFile->data_error[] = $error['error_position'] . $errorMessage;
                    ;
//                }
            }
//            if ($typeCheck == Consts::TYPE_CHECK_NUMBER_1) {
//                $resultFile->error_count = $file->warnings_count;
//                $resultFile->errors = $file->warnings;
//            }
            $result->detail_check[] = $resultFile;
        }
        return $result;
    }

    public function getFacilityById($request)
    {
        $facilityId = $request->facilities_id ?? Consts::USER_ID_DEFAULT;
        return $this->facilityService->getFacilityById($facilityId);
    }
    public function checkCanUpload($settingMonth, $month, $facilityId)
    {
        $canUpload = false;
        if ($settingMonth) {
            $canUpload = $settingMonth->can_upload;
        }
        if ($canUpload == true && Auth::user()->is_admin == false) {
            $role = Role::whereFacilitiesId($facilityId)->whereUsersId(Auth::id())->first();
            $canUpload = $role->edit;
        }
        return $canUpload;
    }
    public function reserve($request)
    {
        if (isset($request->button) && $request->button == 'ok-check' && !isset($request->check_reserve)) {
            $file = File::find($request->file_id);
            $file->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck = TypeCheck::find($file->type_check_id);
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $preStatus = $this->model->find($typeCheck->setting_months_id)->status_facilitity;
            DB::beginTransaction();
            try {
                $file->save();
                $typeCheck->save();
                $settingMonth = $this->model->updateStatusFacilitity($typeCheck->setting_months_id);
                $statusAfter = $settingMonth->status_facilitity;
                if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                    (
                        $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                        $statusAfter === Consts::STATUS_CHECK_FILE['success']
                    )) {
                    $this->sendMailService->sendMailChangeStatus($settingMonth);
                }
                DB::commit();
                return response()->json([
                    'error' => false,
                    'data' => [],
                ], 200);
            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json([
                    'error' => true,
                    'message' => Messages::SYSTERM_ERROR,
                ], 500);
            }
        }
        if ($request->check_reserve) {
            if (is_array($request->file_id)) {
                return $this->reserveFile3($request);
            }
            return $this->reserveFile($request);
        }
    }

    public function reserveFile($request)
    {
        $file = File::whereId($request->file_id)->first();
        $typeCheck = TypeCheck::whereId($request->type_check_id)->first();
        $typeCheck->reason = $request->reason;
        $typeCheck->check_reserve = true;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['hold'];
        $file->reason = $request->reason;
        $file->check_reserve = true;
        $file->status = Consts::STATUS_CHECK_FILE['hold'];
        $preStatus = $this->model->find($typeCheck->setting_months_id)->status_facilitity;
        DB::beginTransaction();
        try {
            $file->save();
            $typeCheck->save();
            $settingMonth = $this->model->updateStatusFacilitity($typeCheck->setting_months_id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'data' => [],
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($file->filepath);
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }

    public function reserveFile3($request)
    {
        $files = File::whereIn('id', $request->file_id);
        $typeCheck = TypeCheck::whereId($request->type_check_id)->first();
        $typeCheck->reason = $request->reason;
        $typeCheck->check_reserve = true;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['hold'];
        $preStatus = $this->model->find($typeCheck->setting_months_id)->status_facilitity;
        DB::beginTransaction();
        try {
            $files->update([
                'reason' => $request->reason,
                'check_reserve' => true,
                'status' => Consts::STATUS_CHECK_FILE['hold']
            ]);
            $typeCheck->save();
            $settingMonth = $this->model->updateStatusFacilitity($typeCheck->setting_months_id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'data' => [],
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            foreach ($files as $file) {
                \Storage::disk('s3')->delete($file->filepath);
            }
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }

    public function getLinkDownload($id)
    {
        $file = File::where('id', $id)->first();
        $links[] = $file->filepath;
        foreach ($file->files as $file) {
            $links[] = $file->filepath;
        }
        return [
            'links' => $links
        ];
    }

    public function createSettingMonth($facilityId, $request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $date = date('Y/m');
        $canUpload = true;
        if ($request->month) {
            if ($date != $request->month) {
                $canUpload = false;
            }
            $date = $request->month;
        }
        $settingMonth = new SettingMonth();
        $settingMonth->created_by = $userId;
        $settingMonth->year_month = $date;
        $settingMonth->facilities_id = $facilityId;
        $settingMonth->can_upload = $canUpload;
        $settingMonth->save();
        return $settingMonth;
    }
}
