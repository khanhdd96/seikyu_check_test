<?php

namespace App\Http\Services;

use App\Consts;
use App\Http\Services\Admin\DepartmentService;
use App\Models\Facility;

class FacilityService
{
    public function __construct(Facility $model, DepartmentService $departmentService)
    {
        $this->model = $model;
        $this->departmentService = $departmentService;
    }

    public function getFacilityById($id)
    {
        return $this->model->whereId($id)->first();
    }
    public function getFacilityByNames($names)
    {
        return Facility::whereIn('name', array_values(array_unique($names)))
            ->with('roles')
            ->select('id', 'name')->get()->toArray();
    }

    public function getFacilityByDepartmentId($departmentId)
    {
        if (is_array($departmentId)) {
            return $this->model->whereIn('departments_id', $departmentId)->whereIsActive(1);
        }
        if ($departmentId) {
            return $this->model->whereDepartmentsId($departmentId)->whereIsActive(1);
        }
        return $this->model->whereIsActive(1);
    }
}
