<?php

namespace App\Http\Services;

use App\Models\TypeCheck;

class TypeCheckService
{
    private $typeCheck;
    public function __construct(TypeCheck $typeCheck)
    {
        $this->typeCheck = $typeCheck;
    }
    public function getDetail($id)
    {
        $typeCheck = $this->typeCheck->with([
            'lastErrorCheckFile' => function ($query) {
                $query->with([
                    'files' => function ($query) {
                        $query->with('errors');
                    },
                    'errors'
                ]);
            }])
            ->findOrFail($id);
        $lastFile = $typeCheck->lastErrorCheckFile;
        $files = [];
        $errors = [];
        $files[] = [
            'name' => $lastFile->file_name,
            'id' => $lastFile->id
        ];
        foreach ($lastFile->errors as $error) {
            $errors[] = [
                'error_position' => $error->error_position,
                'message' => $error->message,
                'type' => $error->type
            ];
        }
        foreach ($lastFile->files as $file) {
            $files[] = [
                'name' => $file->file_name,
                'id' => $file->id
            ];
            foreach ($file->errors as $fileError) {
                $errors[] = [
                    'error_position' => $fileError->error_position,
                    'message' => $fileError->message,
                    'type' => $fileError->type
                ];
            }
        }
        return [
            'files' => $files,
            'errors' => $errors,
            'error_count' => count($errors),
            'is_multi_file' => count($files) > 1,
            'type_check_id' => $id
        ];
    }
}
