<?php

namespace App\Http\Services;

use App\Consts;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckNo10Service
{

    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }
    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_10)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_10;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError($request, $typeCheck->id);
            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            $file = $response['file'];
            $dataErrors = $response['dataErrors'];
            if ($errors) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $settingMonth->count_file_done = 0;
                $typeCheck->save();
            } else {
                $settingMonth->count_file_done += 1;
            }
            $preStatus = $settingMonth->status_facilitity;
            $settingMonth->save();
            //update status setting month
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($file->filepath);
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        return response()->json([
            'error' => false,
            'data' => [
                'type_check_id' => $typeCheck->id,
                'errors' => $errors,
                'file_name' => $request->file->getClientOriginalName(),
                'file_id' => $file->id,
                'dataErrors' => $dataErrors
            ],
        ], 200);
    }
    public function checkError($request, $typeCheckId)
    {
        $fileUpload = $request->file('file');
        $path = $fileUpload->getRealPath();
        $extension = strtolower($request->file->getClientOriginalExtension());
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
//        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if ($extension == 'csv') {
            if (($fh = fopen($path, 'r')) !== false) {
                while (($data = fgetcsv($fh)) !== false) {
                    $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                }
                fclose($fh);
            }
            $sheetData = $professors;
        }
        $name = time() . $fileUpload->getClientOriginalName();
        $filePath = 'files/' . $name;
        try {
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
            $filePath = env('AWS_URL') . $filePath;
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);
            return response()->json([
                'error' => true,
                'message' => Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $errors = [];
        $dataErrors = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        /**  Loop through all the remaining files in the list  **/
        for ($row = 1; $row < count($sheetData); $row++) {
            if (isset($sheetData[$row][21]) && isset($sheetData[$row][24])) {
                if ($sheetData[$row][21] == 0 && $sheetData[$row][24] != 0) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A10001',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A10001'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                    $dataErrors[] = $sheetData[$row];
                }
            }
        }
        if ($errors) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }
        return ['errors' => $errors, 'file' => $file, 'dataErrors' => $dataErrors];
    }
}
