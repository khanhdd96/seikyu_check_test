<?php

namespace App\Http\Services\CheckAll;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Http\Services\SendMailService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Function8
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }
    public function check8($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = $request->userId;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_8)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_8;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        $response = $this->checkError($request, $typeCheck->id);
        $errors = $response['errors'];
        if ($errors) {
            Error::insert($errors);
            $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
            $typeCheck->save();
        } else {
            $settingMonth->count_file_done += 1;
        }
        $this->model->updateStatusFacilitity($settingMonth->id);
        $settingMonth->save();
        return [$response['path']];
    }
    public function checkError($request, $typeCheckId)
    {
        $fileUpload = $request->file;
        $path = $fileUpload['path'];
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $name = time() . $fileUpload['name'];
        $filePath = 'files/' . $name;
        \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload['path']), 'public');
        $filePath = env('AWS_URL') . $filePath;
        $file = new File();
        $file->file_name = $fileUpload['name'];
        $file->type_check_id = $typeCheckId;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $errors = [];
        $dataErrors = [];
        $userId = $request->userId ?? Consts::USER_ID_DEFAULT;
        // check file name
        /**  Loop through all the remaining files in the list  **/
        for ($row = 6; $row < count($sheetData); $row++) {
            if ($sheetData[$row][2] != $sheetData[$row][3]) {
                $errors[] = [
                    'error_position' => ($row + 1) . "行目",
                    'error_code' => 'A8001',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A8001'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'created_by' => $userId,
                    'updated_by' => $userId,
                ];
                array_shift($sheetData[$row]);
                $dataErrors[] = $sheetData[$row];
            } else {
                if (!in_array($sheetData[$row][2], Consts::TEXT_CHECK_COLUNM_C_AND_D_TYPE_CHECK_08)) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A8002',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A8002'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                    array_shift($sheetData[$row]);
                    $dataErrors[] = $sheetData[$row];
                } elseif ($sheetData[$row][3] != Consts::TEXT_CHECK_COLUNM_C_AND_D_TYPE_CHECK_08[2] &&
                    count(array_unique(str_split($sheetData[$row][8]))) == 1) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A8003',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A8003'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                    array_shift($sheetData[$row]);
                    $dataErrors[] = $sheetData[$row];
                }
            }
        }
        if ($errors) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }
        return ['errors' => $errors, 'file' => $file, 'dataErrors' => $dataErrors, 'path' => $filePath];
    }
}
