<?php

namespace App\Http\Services\CheckAll;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Http\Services\SendMailService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Function10
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }
    public function check10($request)
    {
        $fileData = $request->file;
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = $request->userId;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_10)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_10;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        $fileUpload = $fileData['path'];
        $path = $fileData['path'];
        $extension = $fileData['extension'];
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
//        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if ($extension == 'csv') {
            if (($fh = fopen($path, 'r')) !== false) {
                while (($data = fgetcsv($fh)) !== false) {
                    $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                }
                fclose($fh);
            }
            $sheetData = $professors;
        }
        $name = time() . $fileData['name'];
        $filePath = 'files/' . $name;
        \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
        $filePath = env('AWS_URL') . $filePath;
        $file = new File();
        $file->file_name = $fileData['name'];
        $file->type_check_id = $typeCheck->id;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $errors = [];
        $dataErrors = [];
        /**  Loop through all the remaining files in the list  **/
        for ($row = 1; $row < count($sheetData); $row++) {
            if (isset($sheetData[$row][21]) && isset($sheetData[$row][24])) {
                if ($sheetData[$row][21] == 0 && $sheetData[$row][24] != 0) {
                    $errors[] = [
                        'error_position' => ($row + 1) . "行目",
                        'error_code' => 'A10001',
                        'files_id' => $file->id,
                        'message' => Consts::ERROR_CHECK_CODE['A10001'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                    ];
                    $dataErrors[] = $sheetData[$row];
                }
            }
        }
        if ($errors) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }
        if ($errors) {
            Error::insert($errors);
            $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
            $settingMonth->count_file_done = 0;
            $typeCheck->save();
        } else {
            $settingMonth->count_file_done += 1;
        }
        $preStatus = $settingMonth->status_facilitity;

        $settingMonth->save();
        //update status setting month
        $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
        $statusAfter = $settingMonth->status_facilitity;
        if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
            (
                $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                $statusAfter === Consts::STATUS_CHECK_FILE['success']
            )) {
            $this->sendMailService->sendMailChangeStatus($settingMonth);
        }
        return [$filePath];
    }
}
