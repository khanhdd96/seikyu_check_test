<?php

namespace App\Http\Services\CheckAll;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Http\Services\SendMailService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Function1
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function check1($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = $request->userId ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $fileUploads = $request->file;
        $files = $this->getFile($fileUploads);
        $fileOutputPath = $this->handleDataInput($files);
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_1)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_1;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        $dataFiles = $this->uploadFileToS3($fileOutputPath, $files, $typeCheck->id);
        $fileId = $dataFiles['file_id'];
        $errors = $this->checkError($fileOutputPath, $files, $fileId, $userId) ?? [];
        if ($errors) {
            Error::insert($errors);
            $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
            $settingMonth->count_file_done = 0;
            $typeCheck->save();
            foreach ($dataFiles['files'] as $file) {
                $file->status = Consts::STATUS_CHECK_FILE['error'];
                $file->save();
            }
        } else {
            $settingMonth->count_file_done += 1;
        }
        $preStatus = $settingMonth->status_facilitity;
        $settingMonth->save();
        //update status setting month
        $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
        $statusAfter = $settingMonth->status_facilitity;
        if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
            (
                $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                $statusAfter === Consts::STATUS_CHECK_FILE['success']
            )) {
            $this->sendMailService->sendMailChangeStatus($settingMonth);
        }
        if (\File::exists(public_path($fileOutputPath))) {
            \File::delete(public_path($fileOutputPath));
        }
        return $dataFiles['path'];
    }

    public function getFile($fileUpload)
    {
        $files = [];
        foreach ($fileUpload as $file) {
            $name = $file['name'];
            if (str_contains($name, Consts::CHECK_1_FILE_1_NAME)) {
                $files[Consts::CHECK_1_FILE['FILE_ADMIN_TEMPLATE']] = $file;
            } elseif ($this->arraySearchPartial(Consts::CHECK_1_FILE_2_NAME, $name)) {
                $files[Consts::CHECK_1_FILE['FILE_OUT_OF_INSURANCE']] = $file;
            } elseif (str_contains($name, Consts::CHECK_1_FILE_3_NAME)) {
                $files[Consts::CHECK_1_FILE['FILE_IN_INSURANCE']] = $file;
            } elseif (str_contains($name, Consts::CHECK_1_FILE_4_NAME)) {
                $files[Consts::CHECK_1_FILE['FILE_OF_TYPE_CHECK_3']] = $file;
            } elseif (str_contains($name, Consts::CHECK_1_FILE_5_NAME)) {
                $files[Consts::CHECK_1_FILE['FILE_OF_TYPE_CHECK_10']] = $file;
            }
        }
        return $files;
    }

    public function arraySearchPartial($arr, $keyword): bool
    {
        $hasFile = false;
        foreach ($arr as $string) {
            $hasFile = false;
            if (str_contains($keyword, $string)) {
                $hasFile = true;
            }
        }
        return $hasFile;
    }

    public function handleDataInput($fileUploads)
    {
        ini_set('max_execution_time', 99999999);
        set_time_limit(99999999);
        $pathFileInInsurance = $fileUploads[Consts::CHECK_1_FILE['FILE_IN_INSURANCE']]['path'];
        $inputFileInInsuranceType = IOFactory::identify($pathFileInInsurance);
        $readerFileInInsuranceType = IOFactory::createReader($inputFileInInsuranceType);
        $fileInsurance = $readerFileInInsuranceType->load($pathFileInInsurance);
        $fileInInsuranceData = $fileInsurance->getActiveSheet()
            ->toArray(null, false, true, true);
        $pathFileOutOfInsurance = $fileUploads[Consts::CHECK_1_FILE['FILE_OUT_OF_INSURANCE']]['path'];
        $inputFileOutOfInsuranceType = IOFactory::identify($pathFileOutOfInsurance);
        $readerFileOutOfInsuranceType = IOFactory::createReader($inputFileOutOfInsuranceType);
        $fileOutOfInsurance = $readerFileOutOfInsuranceType->load($pathFileOutOfInsurance);
        $fileOutOfInsuranceData = $fileOutOfInsurance->getActiveSheet()
            ->toArray(null, false, true, true);
        $path = $fileUploads[Consts::CHECK_1_FILE['FILE_ADMIN_TEMPLATE']]['path'];
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $spreadsheet->removeSheetByIndex(1);
        $spreadsheet->removeSheetByIndex(0);
        $sheet2 = $spreadsheet->createSheet(0);
        $sheet2->setTitle('①請求計算確認表（貼り付け）')->fromArray($fileInInsuranceData, null, 'A1');
        $sheet3 = $spreadsheet->createSheet(1);
        $sheet3->setTitle('②売上一覧表_保険外（貼り付け）')
            ->fromArray($fileOutOfInsuranceData, null, 'A1');
        $writer = IOFactory::createWriter($spreadsheet, $inputFileType);
        $writer->setPreCalculateFormulas(false);
        $fileOutput = 'all/';
        $fileOutput .= time() . $fileUploads[Consts::CHECK_1_FILE['FILE_ADMIN_TEMPLATE']]['name'];
        $writer->save(public_path($fileOutput));
        return $fileOutput;
    }

    public function checkError($fileOutput, $fileInput, $fileId, $userId)
    {
        $dataChecks = $this->getDataFromFiles($fileOutput, $fileInput);
        $error = [];
        $error = array_merge($error, $this->checkError9($dataChecks['dataFileOutputSheet3'], $fileId, $userId));
        $error = array_merge(
            $error,
            $this->checkError1(
                $dataChecks['dataFileOutputSheet3'],
                $dataChecks['dataFileOutputSheet6'],
                $fileId,
                $userId
            )
        );
        $error = array_merge($error, $this->checkError2($dataChecks['dataFileOutputSheet3'], $fileId, $userId));
        $error = array_merge($error, $this->checkError3($dataChecks['dataFileOutputSheet3'], $fileId, $userId));
        $error = array_merge(
            $error,
            $this->checkError4(
                $dataChecks['dataFileOutputSheet3'],
                $dataChecks['dataFileTypeCheck3'],
                $dataChecks['dataFileTypeCheck10'],
                $fileId,
                $userId
            )
        );
        $error = array_merge($error, $this->checkError5($dataChecks['dataFileOutputSheet3'], $fileId, $userId));
        $error = array_merge($error, $this->checkError6($dataChecks['dataFileOutputSheet4'], $fileId, $userId));
        $error = array_merge($error, $this->checkError7(
            $dataChecks['dataFileOutputSheet5'],
            $dataChecks['dataFileOutputSheet3'],
            $fileId,
            $userId
        ));
        return $error;
    }

    public function getDataFromFiles($fileOutput, $fileInput)
    {
        $fileOutputType = IOFactory::identify(public_path($fileOutput));
        $readerFileOutput = IOFactory::createReader($fileOutputType);
        $fileOutput = $readerFileOutput->load(public_path($fileOutput));
        $fileOutput->setActiveSheetIndex(2);
        $dataFileOutputSheet3 = $fileOutput->getActiveSheet()
            ->toArray(null, true, true, true);
        $fileOutput->setActiveSheetIndex(3);
        $dataFileOutputSheet4 = $fileOutput->getActiveSheet()
            ->toArray(null, true, true, true);
        $fileOutput->setActiveSheetIndex(4);
        $dataFileOutputSheet5 = $fileOutput->getActiveSheet()
            ->toArray(null, false, true, true);
        $fileOutput->setActiveSheetIndex(5);
        $dataFileOutputSheet6 = $fileOutput->getActiveSheet()
            ->toArray(null, false, true, true);
        $dataFileOutputSheet6 = array_column($dataFileOutputSheet6, 'A');
        $pathFileTypeCheck3 = $fileInput[Consts::CHECK_1_FILE['FILE_OF_TYPE_CHECK_3']]['path'];
        $pathFileTypeCheck3Type = IOFactory::identify($pathFileTypeCheck3);
        $readerFileTypeCheck3 = IOFactory::createReader($pathFileTypeCheck3Type);
        $fileTypeCheck3 = $readerFileTypeCheck3->load($pathFileTypeCheck3);
        $dataFileTypeCheck3 = $fileTypeCheck3->getActiveSheet()
            ->toArray(null, false, true, true);
        $pathFileTypeCheck10 = $fileInput[Consts::CHECK_1_FILE['FILE_OF_TYPE_CHECK_3']]['path'];
        $pathFileTypeCheck10Type = IOFactory::identify($pathFileTypeCheck10);
        $readerFileTypeCheck10 = IOFactory::createReader($pathFileTypeCheck10Type);
        $fileTypeCheck10 = $readerFileTypeCheck10->load($pathFileTypeCheck10);
        $dataFileTypeCheck10 = $fileTypeCheck10->getActiveSheet()
            ->toArray(null, false, true, true);
        return [
            'dataFileOutputSheet3' => $dataFileOutputSheet3,
            'dataFileOutputSheet4' => $dataFileOutputSheet4,
            'dataFileOutputSheet5' => $dataFileOutputSheet5,
            'dataFileOutputSheet6' => $dataFileOutputSheet6,
            'dataFileTypeCheck3' => $dataFileTypeCheck3,
            'dataFileTypeCheck10' => $dataFileTypeCheck10
        ];
    }

    public function checkError1($dataSheet3, $dataSheet6, $fileId, $userId)
    {
        $errors = [];
        foreach ($dataSheet3 as $row => $data) {
            if ($row > 7) {
                if (in_array(explode(' ', $data['B'])[0], $dataSheet6)) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1001',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1001'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                }
            }
        }
        return $errors;
    }

    public function checkError2($dataSheet3, $fileId, $userId)
    {
        $errors = [];
        $sum = 0;
        foreach ($dataSheet3 as $key => $item) {
            if ($item['B'] && $this->compareString($item['B'], '合計')) {
                $sum = intval(str_replace(['¥', ','], ['', ''], $dataSheet3[$key + 1]['J']));
                break;
            }
        }
        if ($sum % 1100 != 0) {
            $errors[] = [
                'error_position' => Consts::ERROR_CHECK_CODE['A1002'],
                'error_code' => ' ',
                'files_id' => $fileId,
                'message' => ' ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
                'type' => Consts::TYPE_ERROR
            ];
        }

        return $errors;
    }

    public function checkError3($dataSheet3, $fileId, $userId)
    {
        $errors = [];
        $sum = 0;
        foreach ($dataSheet3 as $key => $item) {
            if ($key > 7 && $item['B'] && $this->compareString($item['B'], '合計')) {
                $sum = intval(str_replace(['¥', ','], ['', ''], $item['L']));
                break;
            }
        }
        if ($sum > 0) {
            $errors[] = [
                'error_position' => Consts::ERROR_CHECK_CODE['A1003'],
                'error_code' => ' ',
                'files_id' => $fileId,
                'message' => ' ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
                'type' => Consts::TYPE_ERROR
            ];
        }

        return $errors;
    }

    public function checkError4($dataSheet3, $dataFileTypeCheck3, $dataFileTypeCheck10, $fileId, $userId)
    {
        $errors = [];
        $dataColumnIInFileTypeCheck10 = array(collect($dataFileTypeCheck10)->map(function ($item) {
            return isset($item['I']) ? intval($item['I']) : '';
        }));
        $dataColumnSInFileTypeCheck10 = array(collect($dataFileTypeCheck10)->map(function ($item) {
            $value = isset($item['S']) ? str_replace(['¥', ','], ['', ''], $item['S']) : '';
            return intval($value);
        }));
        $dataColumnVInFileTypeCheck10 = array(collect($dataFileTypeCheck10)->map(function ($item) {
            $value = isset($item['V']) ? str_replace(['¥', ','], ['', ''], $item['V']) : '';
            return intval($value);
        }));
        $dataColumnEInFileTypeCheck3 = array(collect($dataFileTypeCheck3)->map(function ($item) {
            return isset($item['E']) ? intval($item['E']) : '';
        }));
        $dataColumnCInFileTypeCheck3 = array(collect($dataFileTypeCheck3)->map(function ($item) {
            return $item['C'] ?? '';
        }));
        $dataColumnDInFileTypeCheck3 = array(collect($dataFileTypeCheck3)->map(function ($item) {
            return $item['D'] ?? '';
        }));
        foreach ($dataSheet3 as $row => $data) {
            if ($row > 7) {
                if (str_starts_with($data['D'], 'H')) {
                    if (str_replace(['¥', ','], ['', ''], $data['F']) == 0
                        && str_replace(['¥', ','], ['', ''], $data['I'])) {
                        continue;
                    }
                    if (!str_replace(['¥', ','], ['', ''], $data['I'])
                        && str_replace(['¥', ','], ['', ''], $dataSheet3[$row - 1]['F']) > 0
                        && str_replace(['¥', ','], ['', ''], $dataSheet3[$row - 1]['G'])) {
                        continue;
                    }
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1004',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1004'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                }
                if ($data['D'] && count(array_count_values(str_split($data['D']))) == 1) {
                    $keyValue = explode(' ', $dataSheet3[$row + 1]['B'])[0];
                    foreach ($dataColumnIInFileTypeCheck10 as $key => $data) {
                        if ($data
                            && $data == $keyValue
                            && $dataColumnVInFileTypeCheck10[$key]
                            && $dataColumnSInFileTypeCheck10[$key]
                        ) {
                            $errors[] = [
                                'error_position' => $row . "行目",
                                'error_code' => 'A1004',
                                'files_id' => $fileId,
                                'message' => Consts::ERROR_CHECK_CODE['A1004'],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'created_by' => $userId,
                                'updated_by' => $userId,
                                'type' => Consts::TYPE_ERROR
                            ];
                            continue;
                        }
                    }
                    foreach ($dataColumnEInFileTypeCheck3 as $key => $data) {
                        if (($data && $data == $keyValue)
                            && ($dataColumnCInFileTypeCheck3[$key] != '請求無し'
                                || $dataColumnDInFileTypeCheck3[$key] != '請求無し')
                        ) {
                            $errors[] = [
                                'error_position' => $row . "行目",
                                'error_code' => 'A1004',
                                'files_id' => $fileId,
                                'message' => Consts::ERROR_CHECK_CODE['A1004'],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'created_by' => $userId,
                                'updated_by' => $userId,
                                'type' => Consts::TYPE_ERROR
                            ];
                        }
                    }
                }
            }
        }
        return $errors;
    }

    public function checkError5($dataSheet3, $fileId, $userId)
    {
        $errors = [];
        foreach ($dataSheet3 as $row => $data) {
            if ($row > 7) {
                $value = str_replace(['¥', ','], ['', ''], $data['K']);
                if (filter_var($value, FILTER_VALIDATE_INT) === true && $value > 0) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1005',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1005'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                }
            }
        }
        return $errors;
    }

    public function checkError6($dataSheet4, $fileId, $userId)
    {
        $errors = [];
        foreach ($dataSheet4 as $row => $data) {
            if ($row > 6) {
                if (str_starts_with($data['B'], '9')) {
//                    if (isset($dataSheet4[$row - 1]['B'])
//                        && $this->arraySearchPartial(['加算', '減算'], $dataSheet4[$row - 1]['B'])
//                        && intval(str_replace(['¥', ','], ['', ''], $data['F'])) != 0) {
//                        $errors[] = [
//                            'error_position' => $row . "行目",
//                            'error_code' => 'A1006',
//                            'files_id' => $fileId,
//                            'message' => Consts::ERROR_CHECK_CODE['A1006'],
//                            'created_at' => Carbon::now(),
//                            'updated_at' => Carbon::now(),
//                            'created_by' => $userId,
//                            'updated_by' => $userId,
//                            'type' => Consts::TYPE_ERROR
//                        ];
//                        continue;
//                    }
//                    if (!$this->arraySearchPartial(['加算', '減算'], $dataSheet4[$row - 1]['B'] )
                    if (!$this->arraySearchPartial2(['加算', '減算'], $dataSheet4[$row - 1]['B']) &&
                        intval(str_replace(['¥', ','], ['', ''], $data['F'])) == 0) {
                        $errors[] = [
                            'error_position' => $row . "行目",
                            'error_code' => 'A1006',
                            'files_id' => $fileId,
                            'message' => Consts::ERROR_CHECK_CODE['A1006'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'created_by' => $userId,
                            'updated_by' => $userId,
                            'type' => Consts::TYPE_ERROR
                        ];
                        continue;
                    }
                }
                if (str_starts_with($data['B'], '14')
                    && intval(str_replace(['¥', ','], ['', ''], $data['F'])) == 0) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1006',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1006'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                    continue;
                }
                if (str_starts_with($data['B'], '03')
                    && intval(str_replace(['¥', ','], ['', ''], $data['F'])) == 0) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1006',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1006'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                    continue;
                }
                if (str_starts_with($data['B'], '50')
                    && intval(str_replace(['¥', ','], ['', ''], $data['F'])) != 0) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1006',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1006'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                    continue;
                }
                if (str_starts_with($data['B'], '77')
                    && intval(str_replace(['¥', ','], ['', ''], $data['F'])) != 0) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1006',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1006'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                }
            }
        }
        return $errors;
    }

    public function checkError7($dataSheet5, $dataSheet3, $fileId, $userId)
    {
        $errors = [];
        $dataColumnDSheet3 = array_column($dataSheet3, 'D');
        $dataColumnCSheet3 = array_column($dataSheet3, 'C');
        $dataColumnASheet3 = array_column($dataSheet3, 'A');
        $datas = [];
        foreach ($dataColumnASheet3 as $value) {
            $datas[] = $this->getNumberString($value);
        }
        foreach ($dataSheet5 as $row => $data) {
            if ($row > 1) {
                if (str_contains($data['R'], '再請求')) {
                    if ($data['Q'] != '返戻B/ＡＮＮ２') {
                        $errors[] = [
                            'error_position' => $row . "行目",
                            'error_code' => 'A1007',
                            'files_id' => $fileId,
                            'message' => Consts::ERROR_CHECK_CODE['A1007'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'created_by' => $userId,
                            'updated_by' => $userId,
                            'type' => Consts::TYPE_ERROR
                        ];
                        continue;
                    } else {
                        if ($data['R'] != '二重請求（1回目が正なら対応不要・2回目が正なら過誤＆再請求）') {
                            $errors[] = [
                                'error_position' => $row . "行目",
                                'error_code' => 'A1007',
                                'files_id' => $fileId,
                                'message' => Consts::ERROR_CHECK_CODE['A1007'],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'created_by' => $userId,
                                'updated_by' => $userId,
                                'type' => Consts::TYPE_ERROR
                            ];
                            continue;
                        }
                    }
                }
                if (!in_array($data['F'], $dataColumnDSheet3)
                    || !in_array($data['D'], $dataColumnCSheet3)
                    || !in_array(str_replace('/', '', $data['C']), $datas)
                ) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1007',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1007'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_ERROR
                    ];
                }
            }
        }
        return $errors;
    }

//    public function checkError8($dataSheet3, $fileId) //Đợi BRSE confirm
    public function checkError9($dataSheet3, $fileId, $userId)
    {
        $errors = [];
        foreach ($dataSheet3 as $row => $data) {
            if ($row > 7) {
                $valueH = str_replace(['¥', ','], ['', ''], $data['H']);
                $valueI = str_replace(['¥', ','], ['', ''], $data['I']);
                if ((filter_var($valueH, FILTER_VALIDATE_INT) === true && $valueH > 100000)
                    || (filter_var($valueI, FILTER_VALIDATE_INT) === true && $valueI > 0)
                ) {
                    $errors[] = [
                        'error_position' => $row . "行目",
                        'error_code' => 'A1009',
                        'files_id' => $fileId,
                        'message' => Consts::ERROR_CHECK_CODE['A1009'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => $userId,
                        'updated_by' => $userId,
                        'type' => Consts::TYPE_WARNING
                    ];
                }
            }
        }
        return $errors;
    }

    public function getNumberString($string)
    {
        date_default_timezone_set('Asia/Tokyo');
        $formatter = new \IntlDateFormatter(
            'ja_JP@calendar=japanese',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            // 'Europe/Madrid',
            'Asia/Tokyo',
            \IntlDateFormatter::TRADITIONAL,
            'Gy年M月' //Age and year (regarding the age)
        );
        $time = $formatter->parse($string);

        return date('Ym', $time);
    }

    public function uploadFileToS3($fileOutputPath, $files, $typeCheckId)
    {
        ksort($files);
        $fileId = null;
        $recordFiles = [];
        $fileUpload = [];
        $filePaths = [];
        foreach ($files as $key => $file) {
            if ($key != Consts::CHECK_1_FILE['FILE_ADMIN_TEMPLATE']) {
                $name = time() . $file['name'];
                $filePath = 'files/' . $name;
                \Storage::disk('s3')->put($filePath, file_get_contents($file['path']), 'public');
            } else {
                $name = explode('/', $fileOutputPath);
                $name = end($name);
                $filePath = 'files/' . $name;
                \Storage::disk('s3')->put($filePath, file_get_contents(public_path($fileOutputPath)), 'public');
            }
            $filePath = env('AWS_URL') . $filePath;
            $filePaths[] = $filePath;
            $file = new File();
            $file->file_name = $name;
            $file->type_check_id = $typeCheckId;
            $file->filepath = $filePath;
            $file->status = Consts::STATUS_CHECK_FILE['success'];
            if ($key == Consts::CHECK_1_FILE['FILE_ADMIN_TEMPLATE']) {
                $file->save();
                $fileId = $file->id;
            } else {
                $file->file_id = $fileId;
                $file->save();
            }
            $recordFiles[$key] = $file;
        }
        return ['file_id' => $fileId, 'files' => $recordFiles, 'path' => $filePaths];
    }
    public function arraySearchPartial2($arr, $keyword): bool
    {
        $hasFile = false;
        foreach ($arr as $string) {
            if (str_contains($keyword, $string)) {
                $hasFile = true;
            }
        }
        return $hasFile;
    }
    public function compareString($str1, $str2)
    {
        return strcmp(trim(str_replace('　', '', $str1)), $str2) === 0;
    }
}
