<?php

namespace App\Http\Services\CheckAll;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Http\Services\SendMailService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Function6
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function check6($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = $request->userId ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_6)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_6;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        $response = $this->checkError($request, $typeCheck->id);
        $errors = $response['errors'];
        $file = $response['file'];
        if ($errors) {
            Error::insert($errors);
            $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
            $typeCheck->save();
        } else {
            ++$settingMonth->count_file_done;
        }
        $preStatus = $settingMonth->status_facilitity;
        $settingMonth->save();

        // update status month
        $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
        $statusAfter = $settingMonth->status_facilitity;
        if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
            (
                $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                $statusAfter === Consts::STATUS_CHECK_FILE['success']
            )) {
            $this->sendMailService->sendMailChangeStatus($settingMonth);
        }
        return [$response['path']];
    }

    public function checkError($request, $typeCheckId)
    {
        $userId = $request->userId;
        $fileUpload = $request->file;
        $path = $fileUpload['path'];
        $encoding = \PhpOffice\PhpSpreadsheet\Reader\Csv::guessEncoding($path);

        $extension = $fileUpload['extension'];
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $professors = [];
        if ($extension == 'csv') {
            if (($fh = fopen($path, 'r')) !== false) {
                while (($data = fgetcsv($fh)) !== false) {
                    $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                }
                fclose($fh);
            }
            $sheetData = $professors;
        }

        $name = time() . $fileUpload['name'];
        $filePath = 'files/' . $name;
        \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload['path']), 'public');
        $filePath = env('AWS_URL') . $filePath;
        $file = new File();
        $file->file_name = $fileUpload['name'];
        $file->type_check_id = $typeCheckId;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();

        $arrayTitle = [];
        $keyTitle = '';
        foreach ($sheetData as $key => $value) {
            if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[1]) {
                $keyTitle = 1;
            }
            if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[2]) {
                $keyTitle = 2;
            }

            if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[3]) {
                $keyTitle = 3;
            }

            if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[4]) {
                $keyTitle = 4;
            }
            $arrayTitle[$keyTitle][$key] = $value;
        }

        $errors = [];
        $errorsTitle1 = $this->getErrorCheckTile($arrayTitle[1], 'A6001', $file, $userId);
        $errorsTitle2 = $this->getErrorCheckTile($arrayTitle[2], 'A6002', $file, $userId);
        $errorsTitle3 = $this->getErrorCheckTile($arrayTitle[3], 'A6003', $file, $userId);
        $errorsTitle4 = $this->getErrorCheckTile($arrayTitle[4], 'A6004', $file, $userId);
        if (!empty($errorsTitle1)) {
            $errors[] = $errorsTitle1;
        }
        if (!empty($errorsTitle2)) {
            $errors[] = $errorsTitle2;
        }
        if (!empty($errorsTitle3)) {
            $errors[] = $errorsTitle3;
        }
        if (!empty($errorsTitle4)) {
            $errors[] = $errorsTitle4;
        }
        /*  Loopthrough all the remaining files in the list  **/
        if (!empty($errors)) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }

        return ['errors' => $errors, 'file' => $file, 'path' => $filePath];
    }

    public function getErrorCheckTile($data, $code, $file, $userId)
    {
        $error = [];
        $total = 0;
        $keyRoot = array_key_first($data);
        foreach ($data as $key => $value) {
            if ($key > $keyRoot + 2) {
                $total += $this->sumCheckTile($value);
            }
        }

        if ($total != 0) {
            $error = [
                'error_position' => ($keyRoot + 1).' 行目、'.($keyRoot + 2).'行目',
                'error_code' => $code,
                'files_id' => $file->id,
                'message' => Consts::ERROR_CHECK_CODE[$code],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
            ];
        }

        return $error;
    }

    public function sumCheckTile($data)
    {
        $total = 0;
        foreach ($data as $value) {
            if ($value == null || $value == 0) {
                $value = 0;
            } else {
                $value = 1;
            }
            $total += $value;
        }

        return $total;
    }
}
