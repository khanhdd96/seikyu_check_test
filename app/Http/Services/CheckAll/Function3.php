<?php

namespace App\Http\Services\CheckAll;

use App\Consts;
use App\Http\Services\FacilityService;
use App\Http\Services\SendMailService;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Function3
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }
    public function check3($request)
    {
        $now = date('y-m-d');
        $fileUploads = $request->file;
        $dataSheets = [];
        $dataFiles = [];
        $fileId = null;
        $dataSetting = $this->addTypeCheck($request);
        $filePaths = [];
        $userId = $request->userId;
        foreach ($fileUploads as $fileUpload) {
            $extension = strtolower($fileUpload['extension']);
            $path = $fileUpload['path'];
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(true);

            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($path);
            $sheetDatas = $spreadsheet->getActiveSheet()->toArray();
            $professors = [];
            if ($extension == 'csv') {
                if (($fh = fopen($path, 'r')) !== false) {
                    while (($data = fgetcsv($fh)) !== false) {
                        $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                    }
                    fclose($fh);
                }
                $sheetDatas = $professors;
            }
            $fileName = $fileUpload['name'];
            $name = time() . $fileName;
            $filePath = 'files/' . $name;
            // Save file
            $file = $this->saveFile($dataSetting['typeCheck'], $fileUpload, $filePath, $fileId);
            $fileId = $file->id;
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload['path']), 'public');
            $filePaths[] = env('AWS_URL') . $filePath;
            $dataSheets[$extension] = $sheetDatas;
            $dataFiles[$extension] = $file;
        }
        // filter data excel
        $errors = [];
        $extension = isset($dataSheets['xlsx']) ? 'xlsx' : 'xls';
        $dataFilterExcels = $this->seikyuCheckXlsx($dataSheets[$extension], $dataFiles[$extension], $extension);
        $dataFilterCsv = $this->seikyuCheckCsv($dataSheets['csv'], $dataFiles['csv']);
        $errors = array_merge($dataFilterExcels['error'], $dataFilterCsv['error']);
        $errorFuolicate = $this->checkDuplicateFileExcelVsCsv(
            $dataFilterExcels[$extension],
            $dataFilterCsv['csv'],
            $dataFiles[$extension],
            $dataFiles['csv'],
            $userId
        );
        $errors = array_merge($errors, $errorFuolicate);
        if (count($errors) > 0) {
            Error::insert($errors);
            $dataSetting['typeCheck']->status = Consts::STATUS_CHECK_FILE['error'];
            $dataSetting['typeCheck']->save();
            $dataFiles[$extension]->status = Consts::STATUS_CHECK_FILE['error'];
            $dataFiles[$extension]->save();
        } else {
            $dataSetting['settingMonth']->count_file_done += 1;
            $dataSetting['settingMonth']->save();
        }
        $preStatus = $dataSetting['settingMonth']->status_facilitity;
        //update status setting month
        $settingMonth = $this->model->updateStatusFacilitity($dataSetting['settingMonth']->id);
        $statusAfter = $settingMonth->status_facilitity;
        if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
            (
                $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                $statusAfter === Consts::STATUS_CHECK_FILE['success']
            )) {
            $this->sendMailService->sendMailChangeStatus($settingMonth);
        }
        return $filePaths;
    }

    public function addTypeCheck($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = $request->userId ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $settingMonth->save();
        $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_3)->first() ?? new TypeCheck();
        $typeCheck->updated_by = $userId;
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $typeCheck->year_month_check = $request->month;
        if (!$typeCheck->id) {
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_3;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $settingMonth->id;
        }
        $typeCheck->save();
        return ['settingMonth' => $settingMonth, 'typeCheck' => $typeCheck];
    }

    public function checkDuplicateFileExcelVsCsv($dataFilterExcels, $dataFilterCsvs, $fileExcel, $fileCsv, $userId)
    {
        // $errors = [];

        // $errorDuplicateExcel = $this->checkErrorDuplicateFileExecl($dataFilterExcels, $dataFilterCsvs, $fileExcel);
        $errorDuplicateCsv = $this->checkErrorDuplicateFileCsv($dataFilterExcels, $dataFilterCsvs, $fileCsv, $userId);

        return $errorDuplicateCsv;
    }

    public function checkErrorDuplicateFileCsv($dataFilterExcels, $dataFilterCsvs, $fileCsv, $userId)
    {
        $errors = [];

        foreach ($dataFilterCsvs as $keyCsv => $valueCsv) {
            $dataFilterCsvs[$keyCsv]['duplicate'] = 0;
            $dataFilterCsvs[$keyCsv]['key_xlsx'] = '';

            foreach ($dataFilterExcels as $keyExcel => $valueExcel) {
                if ($valueExcel[0] == null) {
                    continue;
                }
                if ($dataFilterExcels[$keyExcel + 1][5] == $dataFilterCsvs[$keyCsv][22]
                    && $dataFilterExcels[$keyExcel + 1][9] == $dataFilterCsvs[$keyCsv][23]
                    && $dataFilterExcels[$keyExcel][0] == $dataFilterCsvs[$keyCsv][4]
                ) {
                    ++$dataFilterCsvs[$keyCsv]['duplicate'];
                    $dataFilterCsvs[$keyCsv]['key_xlsx'] .= ($keyExcel+1).',';
                }
            }
        }

        foreach ($dataFilterCsvs as $key => $value) {
            if ($value[0] == null) {
                continue;
            }
            if ($value['duplicate'] == 0) {
                $errors[] = [
                    'error_position' => 'CHECK: '.($key + 1).'行目',
                    'error_code' => 'A3004',
                    'files_id' => $fileCsv->id,
                    'message' => Consts::ERROR_CHECK_CODE['A3004'],
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            if ($value['duplicate'] > 1) {
                $errors[] = [
                    // 'error_position' => 'CHECK: '.($key + 1).'行目',
                    'error_position' => 'CHECK: '.rtrim($value['key_xlsx'], ",").'行目',
                    'error_code' => 'A3002',
                    'files_id' => $fileCsv->id,
                    'message' => Consts::ERROR_CHECK_CODE['A3002'],
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
        }
        if (count($errors) > 0) {
            $fileCsv->status = Consts::STATUS_CHECK_FILE['error'];
            $fileCsv->save();
        }

        return $errors;
    }

    public function saveFile($typeCheck, $fileUpload, $filePath, $id = null)
    {
        $file = new File();
        $file->file_name = $fileUpload['name'];
        $file->type_check_id = $typeCheck->id;
        $file->filepath = env('AWS_URL') . $filePath;
        $file->file_id = $id;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();

        return $file;
    }

    public function seikyuCheckXlsx($sheetDatas, $file, $extension)
    {
        $fileNameExplode = explode('-', $file->file_name);
        $fileNameDate = $fileNameExplode[1];

        // check date
        $errors = $this->seikyuCheckXlsxCheckDate($sheetDatas, $fileNameDate, $file);
        // check price
        $dataSeikyuCheckNo3Excel = $this->seikyuCheckFilterDataExcel($sheetDatas, $file);
        // check lỗi trùng lặp sai giá
        if (count($errors) > 0) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
        }
        $file->save();

        return [$extension => $dataSeikyuCheckNo3Excel, 'error' => $errors];
    }

    public function seikyuCheckXlsxCheckDate($sheetDatas, $fileNameDate, $file)
    {
        $error = [];
        $dataCheckDates = [];
        $fileNameDateMonth = date('Ym', strtotime($fileNameDate));
        foreach ($sheetDatas as $key => $value) {
            if ($sheetDatas[$key][0] != null && $key > 5) {
                $dateRow = $value[0];
                $date = $this->getNumberString($dateRow);
            }
        }

        return $error;
    }

    public function seikyuCheckFilterDataExcel($sheetDatas, $file)
    {
        // lỗi không trùng giá
        $filterDatas = [];
        foreach ($sheetDatas as $key => $value) {
            if (strpos($value[1], '140') !== false) {
                $sheetDatas[$key - 1][0] = $this->getNumberString($sheetDatas[$key - 1][0]);
                $filterDatas[$key - 1] = $sheetDatas[$key - 1];
                $filterDatas[$key - 1]['C'] = $sheetDatas[$key][2];
                $filterDatas[$key] = $sheetDatas[$key];
            }
        }

        $arrayKey1 = [];
        $arrayKeyUset = [];

        foreach ($filterDatas as $key => $value) {
            if ($filterDatas[$key][0] == null) {
                continue;
            }

            $string1 = $value[0].'-'.$value['C'];

            if (!in_array($string1, $arrayKey1)) {
                $arrayKey1[$key] = $string1;
            } else {
                $keySearch = array_search($string1, $arrayKey1);
                $filterDatas[$keySearch + 1][5] += $filterDatas[$key + 1][5];
                $filterDatas[$keySearch + 1][9] += $filterDatas[$key + 1][9];
                $arrayKeyUset[] = $key;
                $arrayKeyUset[] = $key + 1;
            }
        }

        foreach ($arrayKeyUset as $key => $value) {
            unset($filterDatas[$value]);
        }

        return $filterDatas;
    }

    public function getNumberString($string)
    {
        date_default_timezone_set('Asia/Tokyo');
        $formatter = new \IntlDateFormatter(
            'ja_JP@calendar=japanese',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            // 'Europe/Madrid',
            'Asia/Tokyo',
            \IntlDateFormatter::TRADITIONAL,
            'Gy年M月' //Age and year (regarding the age)
        );
        $time = $formatter->parse($string);

        return date('Ym', $time);
    }

    public function seikyuCheckCsv($sheetDatas, $file)
    {
        // check date
        $errors = [];
        $dataSeikyuCheckNo3Csv = $this->seikyuCheckFilterDataCsv($sheetDatas, $file);

        if (count($errors) > 0) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }

        return ['csv' => $dataSeikyuCheckNo3Csv, 'error' => $errors];
    }

    public function seikyuCheckFilterDataCsv($sheetDatas, $file)
    {
        $filterDatas = [];
        $arrayKey1 = [];
        foreach ($sheetDatas as $key => $value) {
            if (isset($value[22]) && isset($value[23]) && isset($value[9])
                && $value[22] != 0 && $key > 0 && $value[22] != null && $value[9] != 1) {
                $string1 = $value[4].'-'.$value[9];

                if (!in_array($string1, $arrayKey1)) {
                    $arrayKey1[$key] = $string1;
                } else {
                    $keySearch = array_search($string1, $arrayKey1);
                    $sheetDatas[$keySearch][22] += intval($sheetDatas[$key][22]);
                    $sheetDatas[$keySearch][23] += intval($sheetDatas[$key][23]);
                    unset($sheetDatas[$key]);
                }
            } else {
                unset($sheetDatas[$key]);
            }
        }

        return $sheetDatas;
    }
}
