<?php

namespace App\Http\Services;

use App\Consts;
use App\Jobs\ReserveTypeCheckJob;
use App\Mail\ChangeStatusTypeCheck;
use App\Mail\AdminUpdateTypeCheckSuccess;
use App\Mail\AdminUpdateTypeCheckError;
use App\Models\File;
use App\Models\User;
use App\Models\Role;

class SendMailService
{
    public function __construct(
        File $model,
        FacilityService $facilityService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
    }

    public function sendMailChangeStatus($settingMonth)
    {
        $datas = [];
        $settingMonthStatus = $settingMonth->status_facilitity;
        $facility = $this->facilityService->getFacilityById($settingMonth->facilities_id);
        // $userIds = FacilityUser::where('facilities_id', '=', $settingMonth->facilities_id)
        //     ->get()->pluck('users_id')->toArray();

        $emails = User::where('is_admin', '=', true)
            ->where('email', '<>', 'tzoo2235@gmail.com')
            ->pluck('email')->toArray();
        $subject = "【営業経理】{$facility->name}事業所　請求締め保留通知メール ";
        $datas['nameFacility'] = $facility->name;
        if ($settingMonthStatus === Consts::STATUS_CHECK_FILE['hold']) {
            $subject = "【営業経理】{$facility->name}事業所　請求締め保留通知メール ";
            foreach ($settingMonth->typeChecks as $typeCheck) {
                if ($typeCheck->status === Consts::STATUS_CHECK_FILE['hold']) {
                    $titleIndex = array_search($typeCheck->code_check, Consts::TYPE_MENU, true);
                    $datas['typeCheck'][] = Consts::TITLE[$titleIndex];
                }
            }
            $view = 'mail-reserve';
        } else {
            $subject = "{$facility->name}事業所、請求締め保留結果連絡";
            foreach ($settingMonth->typeChecks as $typeCheck) {
                $titleIndex = array_search($typeCheck->code_check, Consts::TYPE_MENU, true);
                $datas['typeCheck'][] =
                    [
                        'name' => Consts::TITLE[$titleIndex],
                        'status' => Consts::STATUS_CHECK[$typeCheck->status]
                    ];
            }
            $view = 'mail-succes';
        }
        $mailable = new ChangeStatusTypeCheck($datas, $subject, $view);
        dispatch(new ReserveTypeCheckJob($emails, $mailable));
        return response()->json([
            'error' => false,
            'data' => []

        ], 200);
    }

    public function sendMailAdminUpdateType($settingMonth, $type)
    {
        $datas = [];
        foreach ($settingMonth->typeChecks as $typeCheck) {
            if ($typeCheck->status == Consts::STATUS_CHECK_FILE['hold']) {
                $datas['typeCheck'][] = Consts::TITLE[array_search($typeCheck->code_check, Consts::TYPE_MENU)];
            }
        }
        $facility = $this->facilityService->getFacilityById($settingMonth->facilities_id);
        $userIds = Role::where('facilities_id', '=', $settingMonth->facilities_id)
            ->get()->pluck('users_id')->toArray();
        $emails = User::whereIn('id', $userIds)->orWhere('is_admin', '=', true)
            ->pluck('email')->toArray();

        $subject = "【{$facility->name}店】請求締め 保留ステータス完了通知 ";
        $datas['nameFacility'] = $facility->name;
        $mailable = new AdminUpdateTypeCheckSuccess($datas, $subject);
        if ($type == Consts::STATUS_CHECK_FILE['error']) {
            $subject = "【{$facility->name}店】請求締め 保留ステータス完了通知 ";
            $mailable = new AdminUpdateTypeCheckError($datas, $subject);
        }
        dispatch(new ReserveTypeCheckJob($emails, $mailable));
        return response()->json([
            'error' => false,
            'data' => []

        ], 200);
    }
}
