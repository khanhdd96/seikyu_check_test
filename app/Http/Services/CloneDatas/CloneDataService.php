<?php

namespace App\Http\Services\CloneDatas;

use App\Consts;
use App\Models\Department;
use App\Models\DepartmentGroup;
use App\Models\DepartmentGroupUser;
use App\Models\Facility;
use App\Models\FacilityUser;
use App\Models\Position;
use App\Models\PositionUser;
use App\Models\Role;
use App\Models\RolesWithType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CloneDataService
{
    protected $facility;
    protected $department;
    protected $user;
    protected $facilityUser;
    protected $role;
    protected $position;
    protected $positionUser;
    protected $departmentGroup;
    protected $departmentGroupUser;
    protected $rolesWithType;

    public function __construct(
        Facility $facility,
        Department $department,
        User $user,
        FacilityUser $facilityUser,
        Role $role,
        Position $position,
        PositionUser $positionUser,
        DepartmentGroup $departmentGroup,
        DepartmentGroupUser $departmentGroupUser,
        RolesWithType $rolesWithType
    ) {
        $this->facility = $facility;
        $this->department = $department;
        $this->user = $user;
        $this->facilityUser = $facilityUser;
        $this->role = $role;
        $this->position = $position;
        $this->positionUser = $positionUser;
        $this->departmentGroup = $departmentGroup;
        $this->departmentGroupUser = $departmentGroupUser;
        $this->rolesWithType = $rolesWithType;
    }

    public function departmentClone()
    {
        $this->department->truncate();
        $facilitityOlds = $this->facility->select('id', 'is_active', 'list_type_check')->get()->toArray();
        $this->facility->truncate();
        $this->departmentGroup->truncate();
        try {
            DB::beginTransaction();
            $data = $this->getDataDepartments();
            $departments = $data['department'];
            $facilititys = $data['facilitity'];
            $departmentGroups = $data['department_group'];

            foreach ($facilititys as $key => $value) {
                foreach ($facilitityOlds as $facilitityOld) {
                    if ($value['id'] == $facilitityOld['id']) {
                        $facilititys[$key]['is_active'] = $facilitityOld['is_active'];
                    }
                }
            }

            // add department
            $this->department->insert($departments);
            logger()->info(['departments' => $departments, 'facilititys' => $facilititys]);
            // add facilititys
            $this->facility->insert($facilititys);

            // add departmentGroup
            $this->departmentGroup->insert($departmentGroups);
            $departmentGroupDeleteIds = $this->departmentGroup->onlyTrashed()->get()->pluck('id');

            // delete role
            $this->role->whereIn('department_group_id', $departmentGroupDeleteIds)->delete();

            // delete roles with types
            $this->rolesWithType->whereIn('id_component', $departmentGroupDeleteIds)
                ->where('type', Consts::DEPARTMENT_TYPE)->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e,
            ]);
        }
    }

    public function positionsClone()
    {
        try {
            DB::beginTransaction();
            $positions = $this->getPositionsClone();
            $this->position->upsert(
                $positions,
                ['id', 'code']
            );
            $positionsDeleteIds = $this->position->onlyTrashed()->get()->pluck('id');
            // delete roles with types
            $this->rolesWithType->whereIn('id_component', $positionsDeleteIds)
                ->where('type', Consts::POSITION_TYPE)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e,
            ]);
        }
    }

    public function usersClone()
    {
        $this->facilityUser->truncate();
        $this->positionUser->truncate();
        $this->departmentGroupUser->truncate();
        try {
            DB::beginTransaction();
            $data = $this->employeeClone();
            // craete user
            $users = $data['user'];
            foreach (array_chunk($users, 500) as $user) {
                $this->user->upsert(
                    $user,
                    ['id', 'code']
                );
            }

            $userDeletes = $this->getDmployeeDestroyed();
            foreach (array_chunk($userDeletes, 500) as $user) {
                $this->user->upsert(
                    $user,
                    ['id', 'code']
                );
            }
            $userDeleteIds = [];
            foreach ($userDeletes as $value) {
                $userDeleteIds[] = $value['id'];
            }

            // delete roles with types
            $this->rolesWithType->whereIn('id_component', $userDeleteIds)
                ->where('type', Consts::EMPLOYEE_TYPE)->delete();

            // craete Facilitity User
            $this->facilityUser->insert($data['facilitityUser']);

            // craete Roles
            $roleOldClones = $this->role->where('is_clone', true)->get()->toArray();
            $roleId = [];
            foreach ($roleOldClones as $roleOldClone) {
                foreach ($data['facilitityUser'] as $value) {
                    if ($roleOldClone['users_id'] == $value['users_id'] &&
                        $roleOldClone['facilities_id'] == $value['facilities_id']) {
                        $roleId[] = $roleOldClone['id'];
                    }
                }
            }
            $this->role->whereNotIn('id', $roleId)->where('is_clone', true)->delete();
            // craete Roles

            $this->positionUser->insert($data['positions']);

            // create department Group User

            $this->departmentGroupUser->insert($data['departmentGroupUser']);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e,
            ]);
        }
    }

    public function getDataDepartments()
    {
        $token = $this->getToken();
        $urlDepartment = env('ORS_URL').'/api/vendor/departments?with_trash=true&number_layer='
            .Consts::DEPARTMENT_NUMBER_LAYER;
        $urlFacilititys = env('ORS_URL').'/api/vendor/departments?with_trash=true&number_layer='
            .Consts::FLOOR_3;
        $urlFloor4 = env('ORS_URL').'/api/vendor/departments?with_trash=true&number_layer='
        .Consts::FLOOR_4;
        $urlDepartmentGroup = env('ORS_URL').'/api/vendor/departments?with_trash=true&number_layer='
            .Consts::DEPARTMENT_GROUP_AND_FACILITIES_NUMBER_LAYER;
        $dataDepartments = $this->getDataApi($urlDepartment, $token);
        $dataFacilititys = $this->getDataApi($urlFacilititys, $token);
        $dataFloor4s = $this->getDataApi($urlFloor4, $token);
        $dataDepartmentGroup = $this->getDataApi($urlDepartmentGroup, $token);

        $departments = [];
        $floor4Ids = [];
        $departmentGroups = [];

        $facilititys = [];
        $array = [];
        foreach ($dataDepartmentGroup['data'] as $value) {
            if ($value['hiiragi_code'] || $value['office_name'] || $value['office_code']) {
                $floor4Ids[] = $value['parent_department_id'];
                foreach ($dataFloor4s['data'] as $value4) {
                    if ($value['parent_department_id'] == $value4['department_id']) {
                        $array[$value['department_id']] = $value4['parent_department_id'];
                        break;
                    }
                }
            }
        }
        foreach ($array as $key => $value) {
            foreach ($dataFacilititys['data'] as $value3) {
                if ($value == $value3['department_id']) {
                    $array[$key] = $value3['parent_department_id'];
                    break;
                }
            }
        }
        foreach ($array as $key => $value) {
            foreach ($dataDepartments['data'] as $value2) {
                if ($value == $value2['department_id']) {
                    $array[$key] = $value2['department_id'];
                    break;
                }
            }
        }
        $floor4Ids = array_unique($floor4Ids);
        $floor3Ids = [];
        foreach ($dataFloor4s['data'] as $data) {
            if (in_array($data['department_id'], $floor4Ids)) {
                $floor3Ids[] =  $data['parent_department_id'];
            }
        }
        $floor3Ids = array_unique($floor3Ids);
        $departmentIds = [];
        foreach ($dataFacilititys['data'] as $data) {
            if (in_array($data['department_id'], $floor3Ids)) {
                $departmentIds[] =  $data['parent_department_id'];
            }
        }
        $departmentIds = array_unique($departmentIds);
        foreach ($dataDepartments['data'] as $key => $value) {
            if (in_array($value['department_id'], $departmentIds)) {
                $departments[$key]['id'] = $value['department_id'];
                $departments[$key]['address'] = $value['address'];
                $departments[$key]['phone'] = $value['phone'];
                $departments[$key]['email'] = $value['email'];
                $departments[$key]['fax'] = $value['fax'];
                $departments[$key]['number_layer'] = $value['number_layer'];
                $departments[$key]['code'] = $value['code'];
                $departments[$key]['name'] = $value['name'];
                $departments[$key]['created_at'] = $value['created_at'];
                $departments[$key]['updated_at'] = $value['updated_at'];
                $departments[$key]['deleted_at'] = $value['deleted_at'];
                $departments[$key]['created_by'] = $token['id'];
                $departments[$key]['updated_by'] = $token['id'];
                $departments[$key]['organization_id'] = $value['organization_id'];
            }
        }
        foreach ($dataDepartmentGroup['data'] as $key => $value) {
            if ($value['hiiragi_code'] || $value['office_name'] || $value['office_code']) {
                $facilititys[$key]['id'] = $value['department_id'];
                $facilititys[$key]['organization_id'] = $value['organization_id'];
                $facilititys[$key]['number_layer'] = $value['number_layer'];
                $facilititys[$key]['code'] = $value['code'];
                $facilititys[$key]['address'] = $value['address'];
                $facilititys[$key]['phone'] = $value['phone'];
                $facilititys[$key]['email'] = $value['email'];
                $facilititys[$key]['fax'] = $value['fax'];
                $facilititys[$key]['departments_id'] = $array[$value['department_id']];
                $facilititys[$key]['list_type_check'] = Consts::LIST_TYPE_CHECL_STRING;
                $facilititys[$key]['change_at'] = $value['change_at'];
                $facilititys[$key]['created_at'] = $value['created_at'];
                $facilititys[$key]['updated_at'] = $value['updated_at'];
                $facilititys[$key]['deleted_at'] = $value['deleted_at'];
                $facilititys[$key]['created_by'] = $token['id'];
                $facilititys[$key]['updated_by'] = $token['id'];
                $facilititys[$key]['name'] = $value['office_name'];
                $facilititys[$key]['hiiragi_code'] = $value['hiiragi_code'];
                $facilititys[$key]['office_code'] = $value['office_code'];
                $facilititys[$key]['is_active'] = 1;
            }
        }

        foreach ($dataDepartmentGroup['data'] as $key => $value) {
            $departmentGroups[$key]['id'] = $value['department_id'];
            $departmentGroups[$key]['address'] = $value['address'];
            $departmentGroups[$key]['phone'] = $value['phone'];
            $departmentGroups[$key]['email'] = $value['email'];
            $departmentGroups[$key]['fax'] = $value['fax'];
            $departmentGroups[$key]['number_layer'] = $value['number_layer'];
            $departmentGroups[$key]['code'] = $value['code'];
            $departmentGroups[$key]['name'] = $value['name'];
            $departmentGroups[$key]['created_at'] = $value['created_at'];
            $departmentGroups[$key]['updated_at'] = $value['updated_at'];
            $departmentGroups[$key]['deleted_at'] = $value['deleted_at'];
            $departmentGroups[$key]['created_by'] = $token['id'];
            $departmentGroups[$key]['updated_by'] = $token['id'];
        }

        return [
            'department' => $departments,
            'facilitity' => $facilititys,
            'department_group' => $departmentGroups
        ];
    }

    public function getDataFacilitity()
    {
        $token = $this->getToken();
        $client = new \GuzzleHttp\Client();
        $url = env('ORS_URL').'/api/vendor/departments?with_trash=true&number_layer='
            .Consts::FACILITITY_NUMBER_LAYER;
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $token['token'],
        ];

        $data = $client->get($url, [
            'headers' => $headers,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);

        $facilititys = [];

        foreach ($dataContent['data'] as $key => $value) {
            if ($value['code']) {
                $facilititys[$key]['id'] = $value['department_id'];
                $facilititys[$key]['organization_id'] = $value['organization_id'];
                $facilititys[$key]['number_layer'] = $value['number_layer'];
                $facilititys[$key]['code'] = $value['code'];
                $facilititys[$key]['address'] = $value['address'];
                $facilititys[$key]['phone'] = $value['phone'];
                $facilititys[$key]['email'] = $value['email'];
                $facilititys[$key]['fax'] = $value['fax'];
                $facilititys[$key]['departments_id'] = $value['parent_department_id'];
                $facilititys[$key]['list_type_check'] = Consts::LIST_TYPE_CHECL_STRING;
                $facilititys[$key]['change_at'] = $value['change_at'];
                $facilititys[$key]['created_at'] = $value['created_at'];
                $facilititys[$key]['updated_at'] = $value['updated_at'];
                $facilititys[$key]['deleted_at'] = $value['deleted_at'];
                $facilititys[$key]['created_by'] = $token['id'];
                $facilititys[$key]['updated_by'] = $token['id'];
                $facilititys[$key]['name'] = $value['name'];
                $facilititys[$key]['is_active'] = 1;
            }
        }

        return $facilititys;
    }

    public function getDataApi($url, $token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $token['token'],
        ];

        $data = $client->get($url, [
            'headers' => $headers,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);
        return $dataContent;
    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $url = env('ORS_URL').'/api/vendor/auth';
        $appId = env('APP_ID');
        $appKey = env('APP_SECRET');
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
        $body = [
            'app_id' => $appId,
            'app_secret' => $appKey,
        ];
        $data = $client->post($url, [
            'headers' => $headers,
            'form_params' => $body,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);
        $token = $dataContent['data'];

        return $token;
    }

    public function employeeClone()
    {
        $token = $this->getToken();
        $client = new \GuzzleHttp\Client();
        $page = 1;
        $page_size = Consts::USERS_PAGE_SIZE;
        $url = env('ORS_URL').'/api/vendor/employees?page='.$page.'&page_size='.$page_size;
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $token['token'],
        ];

        $data = $client->get($url, [
            'headers' => $headers,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);

        $facilitityUser = [];
        $users = [];
        $positionUsers = [];
        $departmentGroupUser = [];

        foreach ($dataContent['data'] as $key => $value) {
            $role = '0';
            if ($value['is_admin'] == 1) {
                $role = '1';
            }
            if (!empty($value['departments']) && $value['email']) {
                foreach ($value['departments'] as $key2 => $value2) {
                    $positionId = null;
                    if (!empty($value['positions'])) {
                        $positionId = $value['positions'][0]['id'];
                        foreach ($value['positions'] as $key2 => $position) {
                            if ($position['id'] && $position['employee_id']) {
                                $positionUsers[] = [
                                    'position_id' => $position['id'],
                                    'user_id' => $position['employee_id'],
                                ];
                            }
                        }
                    }

                    if ($value2['number_layer'] == 5) {
                        $facilitityUser[] = [
                            'facilities_id' => $value2['id'],
                            'users_id' => $value['id'],
                            'is_clone' => true,
                            'position_id' => $positionId,
                        ];
                        $departmentGroupUser[] = [
                            'department_group_id' => $value2['id'],
                            'users_id' => $value['id'],
                            'is_clone' => true,
                        ];
                    }
                }
            }

            if ($value['email']) {
                $users[$key] = [
                    'id' => $value['id'],
                    'code' => $value['code'],
                    'name' => $value['name'],
                    'email' => $value['email'],
                    'address' => $value['address'],
                    'birthday' => $value['birthday'],
                    'phone' => $value['phone'],
                    'avatar' => $value['avatar'],
                    'role' => $role,
                    'join_date' => $value['join_date'],
                    'retire_date' => $value['retire_date'],
                ];
            }
        }

        $data = [
            'user' => $users,
            'facilitityUser' => $facilitityUser,
            'positions' => $positionUsers,
            'departmentGroupUser' => $departmentGroupUser,
        ];

        return $data;
    }

    public function getDmployeeDestroyed()
    {
        $token = $this->getToken();
        $client = new \GuzzleHttp\Client();
        $page = 1;
        $page_size = Consts::USERS_PAGE_SIZE;
        $url = env('ORS_URL').'/api/vendor/employees/destroyed?page='.$page.'&page_size='.$page_size;
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $token['token'],
        ];

        $data = $client->get($url, [
            'headers' => $headers,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);

        return  $dataContent['data'];
    }

    public function getPositionsClone()
    {
        $token = $this->getToken();
        $client = new \GuzzleHttp\Client();
        $page = 1;
        $page_size = Consts::USERS_PAGE_SIZE;
        $url = env('ORS_URL').'/api/vendor/positions?page='.$page.'&with_trash=true&page_size='.$page_size;
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'token' => $token['token'],
        ];

        $data = $client->get($url, [
            'headers' => $headers,
        ]);

        $dataContent = $data->getBody()->getContents();
        $dataContent = json_decode($dataContent, true);

        $positions = [];

        foreach ($dataContent['data'] as $key => $value) {
            $positions[] = [
                'id' => $value['id'],
                'code' => $value['code'],
                'name' => $value['name'],
                'deleted_at' => $value['deleted_at'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],
            ];
        }

        return $positions;
    }
}
