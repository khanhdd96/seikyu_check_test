<?php

namespace App\Http\Services;

use App\Messages;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Consts;
use App\Models\File;
use App\Models\Error;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CheckNo7Service
{
    private $model;
    private $facilityService;
    protected $typeCheck;
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        TypeCheck $typeCheck,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->typeCheck = $typeCheck;
        $this->sendMailService = $sendMailService;
    }
    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_7)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_7;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError($request, $typeCheck->id);
            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            $file = $response['file'];
            if ($errors) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $typeCheck->save();
            } else {
                $settingMonth->count_file_done += 1;
            }
            $preStatus = $settingMonth->status_facilitity;

            $settingMonth->save();

            //update status setting month
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            \Storage::disk('s3')->delete($file->filepath);
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
        return response()->json([
            'error' => false,
            'data' => [
                'type_check_id' => $typeCheck->id,
                'errors' => $errors,
                'file_name' => $request->file->getClientOriginalName(),
                'file_id' => $file->id
            ],
        ], 200);
    }
    public function checkError($request, $typeCheckId)
    {
        $fileUpload = $request->file('file');
        $path = $fileUpload->getRealPath();
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $name = time().$fileUpload->getClientOriginalName();
        $filePath = 'files/'.$name;
        try {
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
            $filePath = env('AWS_URL').$filePath;
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);

            return response()->json([
                'error' => true,
                'message' => Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = $filePath;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        $errors = [];
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        /*  Loop through all the remaining files in the list  **/
        for ($i = 5; $i < count($sheetData); ++$i) {
            if ($sheetData[$i][3] == Consts::CHECK_7_ERROR_1) {
                $errors[] = [
                    'error_position' => ($i + 1).'行目',
                    'error_code' => 'A7001',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A7001'],
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            if ($sheetData[$i][3] == Consts::CHECK_7_ERROR_2) {
                $errors[] = [
                    'error_position' => ($i + 1).'行目',
                    'error_code' => 'A7002',
                    'files_id' => $file->id,
                    'message' => Consts::ERROR_CHECK_CODE['A7002'],
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
        }
        if ($errors) {
            $file->status = Consts::STATUS_CHECK_FILE['error'];
            $file->save();
        }

        return ['errors' => $errors, 'file' => $file];
    }

    public function check7Success($request)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth = $request->settingMonth;
        $typeCheck =  $this->typeCheck->where('setting_months_id', $request->settingMonth)
            ->where('facilities_id', $request->facilityId)->where('year_month_check', $request->month)
            ->where('code_check', $request->type)->first();
        if (!$typeCheck) {
            $typeCheck = new TypeCheck();
            $typeCheck->created_by = $userId;
            $typeCheck->code_check = $request->type;
            $typeCheck->facilities_id = $request->facilityId;
            $typeCheck->setting_months_id = $request->settingMonth;
        }
        $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
        $preStatus = $this->model->find($settingMonth)->status_facilitity;
        DB::beginTransaction();
        try {
            $typeCheck->save();
            $settingMonthSave = $this->model->updateStatusFacilitity($settingMonth);
            $statusAfter = $settingMonthSave->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonthSave);
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'data' => [],
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }
}
