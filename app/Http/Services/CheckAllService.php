<?php

namespace App\Http\Services;

use App\Consts;
use App\Http\Services\CheckAll\Function1;
use App\Http\Services\CheckAll\Function10;
use App\Http\Services\CheckAll\Function2;
use App\Http\Services\CheckAll\Function3;
use App\Http\Services\CheckAll\Function4;
use App\Http\Services\CheckAll\Function5;
use App\Http\Services\CheckAll\Function6;
use App\Http\Services\CheckAll\Function7;
use App\Http\Services\CheckAll\Function8;
use App\Jobs\CheckAll;
use App\Mail\CheckAllFail;
use App\Mail\CheckAllSuccess;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckAllService
{
    private $service1;
    private $service2;
    private $service3;
    private $service4;
    private $service5;
    private $service6;
    private $service7;
    private $service8;
    private $service10;
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService,
        Function1 $service1,
        Function2 $service2,
        Function3 $service3,
        Function4 $service4,
        Function5 $service5,
        Function6 $service6,
        Function7 $service7,
        Function8 $service8,
        Function10 $service10
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
        $this->service1 = $service1;
        $this->service2 = $service2;
        $this->service3 = $service3;
        $this->service4 = $service4;
        $this->service5 = $service5;
        $this->service6 = $service6;
        $this->service7 = $service7;
        $this->service8 = $service8;
        $this->service10 = $service10;
    }

    public function checkAll($request)
    {
        $userId = Auth::id();
        $email = Auth::user()->email ?? '';
        $listFile = (array)json_decode($request->list_file);
        $listFile = array_filter($listFile, function ($value) {
            return !empty($value);
        });
        $filesUpload = $request->file('file');
        $files = [];
        $settingMonthId = $request->settingMonth;
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if (stripos($userAgent, 'Chrome') === false && stripos($userAgent, 'Safari') !== false) {
            foreach ($filesUpload as $fileUp) {
                foreach (Consts::CHECK_ALL_FILE_NAME as $keyRegex => $regex) {
                    if (preg_match($regex, $fileUp->getClientOriginalName())) {
                        $key = explode('_', $keyRegex)[0];
                        $files[Consts::TYPE_MENU[$key]][] = $fileUp;
                    }
                }
            }
        } else {
            foreach ($listFile as $key => $value) {
                $list = [];
                foreach ($filesUpload as $fileUpload) {
                    if (in_array($fileUpload->getClientOriginalName(), $value)) {
                        $list[] = $fileUpload;
                    }
                }
                $files[Consts::TYPE_MENU[$key]] = $list;
            }
        }
        $requests = [];
        $localFile = [];
        DB::beginTransaction();
        try {
            foreach ($files as $key => $file) {
                $requestFile = new \stdClass();
                $requestFile->settingMonth = $request->settingMonth;
                $requestFile->facilityId = $request->facilityId;
                $requestFile->month = $request->month;
                $requestFile->type = $key;
                $requestFile->userId = $userId;
                if (in_array($key, [1, 2, 3, '5-M'])) {
                    $data = [];
                    foreach ($file as $value) {
                        $name = microtime() . $value->getClientOriginalName();
                        Storage::disk('checkAll')->put($name, file_get_contents($value));
                        $data[] = [
                            'path' => public_path("all/$name"),
                            'name' => $value->getClientOriginalName(),
                            'extension' => $value->getClientOriginalExtension()
                        ];
                        $localFile[] = public_path("all/$name");
                    }
                    $requestFile->file = $data;
                } else {
                    $name = microtime() . $file[0]->getClientOriginalName();
                    Storage::disk('checkAll')->put($name, file_get_contents($file[0]));
                    $requestFile->file = [
                        'path' => public_path("all/$name"),
                        'name' =>  $file[0]->getClientOriginalName(),
                        'extension' => $file[0]->getClientOriginalExtension()
                    ];
                    $localFile[] = public_path("all/$name");
                }
                $requests[$key] = $requestFile;
            }
            $job = (new CheckAll($requests, $localFile, $email, $settingMonthId))->onQueue('all');
            $jobId = app(\Illuminate\Contracts\Bus\Dispatcher::class)->dispatch($job);
            $this->model->whereId($settingMonthId)->update([
                'is_checking' => true,
                'job_id' => $jobId
            ]);
            DB::commit();
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            foreach ($localFile as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            return response()->json([
                'success' => false
            ], 500);
        }
    }
    public function checkAllFunction($requests, $localFile, $email, $settingMonthId)
    {
        $filePaths = [];
        DB::beginTransaction();
        try {
            foreach ($requests as $key => $request) {
                $service = Consts::FUNCTION_ALL_SERVICE[$key];
                $function = Consts::FUNCTION_ALL[$key];
                $response = $this->{$service}->{$function}($request);
                $filePaths = array_merge($filePaths, $response);
            }
            foreach ($localFile as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            $settingMonth = SettingMonth::with('job')->findOrFail($settingMonthId);
            if (!empty($settingMonth->job)) {
                $settingMonth->update([
                    'is_checking' => false,
                    'job_id' => null
                ]);
                DB::commit();
                Mail::to($email)->send(new CheckAllSuccess());
//                Mail::to('quan.nguyen@amela.vn')->send(new CheckAllSuccess());
            } else {
                DB::rollBack();
                foreach ($filePaths as $filePath) {
                    Storage::disk('s3')->delete($filePath);
                }
            }
        } catch (\Exception $e) {
            logger()->info([
                'error' => $e->getMessage(),
                'file' => $localFile
            ]);
            Mail::to($email)->send(new CheckAllFail());
//            Mail::to('quan.nguyen@amela.vn')->send(new CheckAllFail()); // temporary
            foreach ($filePaths as $filePath) {
                Storage::disk('s3')->delete($filePath);
            }
            foreach ($localFile as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            DB::rollBack();
            SettingMonth::whereId($settingMonthId)->update([
                'is_checking' => false,
                'job_id' => null
            ]);
        }
    }
    public function deleteJob($request)
    {
        $settingMonth = SettingMonth::with('job')->findOrFail($request->settingMonth);
        $job = $settingMonth->job;
        if ($request->job_id != $settingMonth->job_id) {
            return response()->json([
                'success' => false
            ]);
        } else {
            DB::beginTransaction();
            try {
                if ($job) {
                    $job->delete();
                }
                $settingMonth->job_id = null;
                $settingMonth->is_checking = false;
                $settingMonth->save();
                DB::commit();
                return response()->json([
                    'success' => true
                ]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json([], 500);
            }
        }
    }
}
