<?php

namespace App\Http\Services;

use App\Consts;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class CheckNo6Service
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck(Consts::TYPE_CHECK_NUMBER_6)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_6;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError($request, $typeCheck->id);

            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            $file = $response['file'];
            if ($errors) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $typeCheck->save();
            } else {
                ++$settingMonth->count_file_done;
            }
            $preStatus = $settingMonth->status_facilitity;
            $settingMonth->save();

            // update status month
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }

        return response()->json([
            'error' => false,
            'data' => [
                'type_check_id' => $typeCheck->id,
                'errors' => $errors,
                'file_name' => $request->file->getClientOriginalName(),
                'file_id' => $file->id,
            ],
        ], 200);
    }

    public function checkError($request, $typeCheckId)
    {
        try {
            $fileUpload = $request->file('file');
            $path = $fileUpload->getRealPath();
            $encoding = \PhpOffice\PhpSpreadsheet\Reader\Csv::guessEncoding($path);

            $extension = strtolower($request->file->getClientOriginalExtension());
            $inputFileType = IOFactory::identify($path);
            $reader = IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(true);
            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($path);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            $professors = [];
            if ($extension == 'csv') {
                if (($fh = fopen($path, 'r')) !== false) {
                    while (($data = fgetcsv($fh)) !== false) {
                        $professors[] = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                    }
                    fclose($fh);
                }
                $sheetData = $professors;
            }

            $name = time().$fileUpload->getClientOriginalName();
            $filePath = 'files/'.$name;
            \Storage::disk('s3')->put($filePath, file_get_contents($fileUpload), 'public');
            $filePath = env('AWS_URL').$filePath;
            $file = new File();
            $file->file_name = $fileUpload->getClientOriginalName();
            $file->type_check_id = $typeCheckId;
            $file->filepath = $filePath;
            $file->status = Consts::STATUS_CHECK_FILE['success'];
            $file->save();

            $arrayTitle = [];
            $keyTitle = '';
            foreach ($sheetData as $key => $value) {
                if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[1]) {
                    $keyTitle = 1;
                }
                if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[2]) {
                    $keyTitle = 2;
                }

                if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[3]) {
                    $keyTitle = 3;
                }

                if ($sheetData[$key][0] == Consts::CONDITION_TITLE_ERROR_6[4]) {
                    $keyTitle = 4;
                }
                $arrayTitle[$keyTitle][$key] = $value;
            }

            $errors = [];
            $errorsTitle1 = $this->getErrorCheckTile($arrayTitle[1], 'A6001', $file);
            $errorsTitle2 = $this->getErrorCheckTile($arrayTitle[2], 'A6002', $file);
            $errorsTitle3 = $this->getErrorCheckTile($arrayTitle[3], 'A6003', $file);
            $errorsTitle4 = $this->getErrorCheckTile($arrayTitle[4], 'A6004', $file);
            if (!empty($errorsTitle1)) {
                $errors[] = $errorsTitle1;
            }
            if (!empty($errorsTitle2)) {
                $errors[] = $errorsTitle2;
            }
            if (!empty($errorsTitle3)) {
                $errors[] = $errorsTitle3;
            }
            if (!empty($errorsTitle4)) {
                $errors[] = $errorsTitle4;
            }
            /*  Loopthrough all the remaining files in the list  **/
            if (!empty($errors)) {
                $file->status = Consts::STATUS_CHECK_FILE['error'];
                $file->save();
            }

            return ['errors' => $errors, 'file' => $file];
        } catch (\Exception $e) {
            \Storage::disk('s3')->delete($filePath);

            return response()->json([
                'error' => true,
                'message' => Messages::UPLOAD_FILE_ERROR,
            ], 500);
        }
    }

    public function getErrorCheckTile($data, $code, $file)
    {
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $error = [];
        $total = 0;
        $keyRoot = array_key_first($data);
        foreach ($data as $key => $value) {
            if ($key > $keyRoot + 1) {
                $total += $this->sumCheckTile($value);
            }
        }

        if ($total != 0) {
            $error = [
                'error_position' => ($keyRoot + 1).' 行目、'.($keyRoot + 2).'行目',
                'error_code' => $code,
                'files_id' => $file->id,
                'message' => Consts::ERROR_CHECK_CODE[$code],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => $userId,
                'updated_by' => $userId,
            ];
        }

        return $error;
    }

    public function sumCheckTile($data)
    {
        $total = 0;
        foreach ($data as $value) {
            if ($value == null || $value == 0) {
                $value = 0;
            } else {
                $value = 1;
            }
            $total += $value;
        }

        return $total;
    }
}
