<?php

namespace App\Http\Services;

use App\Consts;
use App\Messages;
use App\Models\Error;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TypeCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckNo2Service
{
    public function __construct(
        SettingMonth $model,
        FacilityService $facilityService,
        SendMailService $sendMailService
    ) {
        $this->model = $model;
        $this->facilityService = $facilityService;
        $this->sendMailService = $sendMailService;
    }

    public function checkFile($request)
    {
        $settingMonthId = $request->settingMonth;
        $settingMonth = $settingMonthId ? $this->model->whereId($settingMonthId)->first() : new SettingMonth();
        $userId = Auth::id() ?? Consts::USER_ID_DEFAULT;
        $settingMonth->updated_by = $userId;
        if (!$settingMonth->id) {
            $settingMonth->created_by = $userId;
            $settingMonth->year_month = $request->month;
            $settingMonth->facilities_id = $request->facilityId;
        }
        $fileUpload = $request->file;
        $files = $this->getFile($fileUpload);
        DB::beginTransaction();
        try {
            $settingMonth->save();
            $typeCheck = $settingMonth->typeCheck($request->type)->first() ?? new TypeCheck();
            $typeCheck->updated_by = $userId;
            $typeCheck->status = Consts::STATUS_CHECK_FILE['success'];
            $typeCheck->year_month_check = $request->month;
            if (!$typeCheck->id) {
                $typeCheck->created_by = $userId;
                $typeCheck->code_check = Consts::TYPE_CHECK_NUMBER_2;
                $typeCheck->facilities_id = $request->facilityId;
                $typeCheck->setting_months_id = $settingMonth->id;
            }
            $typeCheck->save();
            $response = $this->checkError($typeCheck->id, $files);
            if (!is_array($response) && $response->status() != 200) {
                return $response;
            }
            $errors = $response['errors'];
            if ($errors) {
                Error::insert($errors);
                $typeCheck->status = Consts::STATUS_CHECK_FILE['error'];
                $typeCheck->save();
                foreach ($response['files'] as $fileError) {
                    $fileError->status = Consts::STATUS_CHECK_FILE['error'];
                    $fileError->save();
                }
            } else {
                ++$settingMonth->count_file_done;
            }
            $preStatus = $settingMonth->status_facilitity;
            $settingMonth->save();
            $settingMonth = $this->model->updateStatusFacilitity($settingMonth->id);
            $statusAfter = $settingMonth->status_facilitity;
            if ($statusAfter === Consts::STATUS_CHECK_FILE['hold'] ||
                (
                    $preStatus === Consts::STATUS_CHECK_FILE['hold'] &&
                    $statusAfter === Consts::STATUS_CHECK_FILE['success']
                )) {
                $this->sendMailService->sendMailChangeStatus($settingMonth);
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'data' => [
                    'type_check_id' => $typeCheck->id,
                    'errors' => $errors,
                    'files' => $response['files']
                ],
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'error' => true,
                'message' => Messages::SYSTERM_ERROR,
            ], 500);
        }
    }
    public function checkError($typeCheckId, $files)
    {
        $userId = Auth::id();
        $now = Carbon::now();
        $data1 = $data2 = $data3 = [];
        $fileId = null;
        $fileUpload = [];
        $loop = 0;
        foreach ($files as $key => $file) {
            $filePath = 'files/' . time() . $file->getClientOriginalName();
            $fileUpload[$key] = $this->saveFile($typeCheckId, $file, $filePath, $fileId);
            try {
                \Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            } catch (\Exception $e) {
                \Storage::disk('s3')->delete($filePath);
                return response()->json([
                    'error' => true,
                    'message' => Messages::UPLOAD_FILE_ERROR,
                ], 500);
            }
            if ($loop < 1) {
                $fileId = $fileUpload[$key]->id;
            }
            $loop++;
            if ($key == Consts::CHECK_2_FILE['FILE_1']) {
                $data1 = $this->getFile1Data($file);
            } elseif ($key == Consts::CHECK_2_FILE['FILE_S']) {
                $data2 = $this->getFile2Data($file);
            }
//            else {
//                $data3 = $this->getFile3Data($file);
//            }
        }
        $errors = [];
        $file2Error = false;
//        $file3Error = false;
        foreach ($data2 as $key => $value) {
            if (!in_array($value, $data1)) {
                $errors[] = [
                    'error_position' => $key .'行目',
                    'error_code' => 'A2001',
                    'files_id' => $fileUpload[Consts::CHECK_2_FILE['FILE_S']]->id,
                    'message' => Consts::ERROR_CHECK_CODE['A2001'],
                    'created_by' => $userId,
                    'updated_by' => $userId,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
                $file2Error = true;
            }
        }
//        if (!empty($data3)) {
//            foreach ($data3 as $keyData3 => $valueData3) {
//                foreach ($data1 as $dataCheck) {
//                    if ($valueData3['code'] == $dataCheck['code'] &&
//                        $valueData3['date'] == $dataCheck['date'] &&
//                        $valueData3['status'] == Consts::CHECK_FILE_3_ERROR) {
//                        $errors[] = [
//                            'error_position' => $keyData3 .'行目',
//                            'error_code' => 'A2002',
//                            'files_id' => $fileUpload[Consts::CHECK_2_FILE['FILE_3']]->id,
//                            'message' => Consts::ERROR_CHECK_CODE['A2002'],
//                            'created_by' => $userId,
//                            'updated_by' => $userId,
//                            'created_at' => $now,
//                            'updated_at' => $now
//                        ];
//                        $file3Error = true;
//                        break;
//                    }
//                }
//            }
//        }
        if ($file2Error) {
            $fileUpload[Consts::CHECK_2_FILE['FILE_S']]->status = Consts::STATUS_CHECK_FILE['error'];
            $fileUpload[Consts::CHECK_2_FILE['FILE_S']]->save();
        }
//        if ($file3Error) {
//            $fileUpload[Consts::CHECK_2_FILE['FILE_3']]->status = Consts::STATUS_CHECK_FILE['error'];
//            $fileUpload[Consts::CHECK_2_FILE['FILE_3']]->save();
//        }
        return [
            'errors' => $errors,
            'files' => $fileUpload,
        ];
    }
    public function getNumberString($string)
    {
        date_default_timezone_set('Asia/Tokyo');
        $formatter = new \IntlDateFormatter(
            'ja_JP@calendar=japanese',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            // 'Europe/Madrid',
            'Asia/Tokyo',
            \IntlDateFormatter::TRADITIONAL,
            'Gy年M月' //Age and year (regarding the age)
        );
        $time = $formatter->parse($string);

        return date('Ym', $time);
    }
    public function getFile1Data($file)
    {
        $sheetData = $this->getFileCheckData($file);
        $codes = [];
        for ($i = 7; $i < count($sheetData); $i += 2) {
            $code = $sheetData[$i][3];
            if (!empty($code)) {
                $value = [
                    'code' => $code,
                    'date' => $this->getNumberString($sheetData[$i][0])
                ];
                if (!in_array($value, $codes)) {
                    $codes[] = $value;
                }
            }
        }
        return $codes;
    }
    public function getFile2Data($file)
    {
        $sheetData = $this->getFileCheckData($file);
        $data = [];
        for ($i = 3; $i < count($sheetData); $i++) {
            $code = $sheetData[$i][7];
            if (!empty($code)) {
                $value = [
                    'code' => $code,
                    'date' => $sheetData[$i][4],
                ];
                if (!in_array($value, $data)) {
                    $data[$i + 1] = $value;
                }
            }
        }
        return $data;
    }
//    public function getFile3Data($file)
//    {
//        $sheetData = $this->getFileCheckData($file);
//        $data = [];
//        for ($i = 5; $i < count($sheetData); $i++) {
//            $code = $sheetData[$i][6];
//            if (!empty($code)) {
//                $data[$i + 1] = [
//                    'code' => $code,
//                    'date' => $this->getNumberString($sheetData[$i][4]),
//                    'status' => $sheetData[$i][3],
//                ];
//            }
//        }
//        return $data;
//    }
    public function saveFile($typeCheckId, $fileUpload, $filePath, $id = null)
    {
        $file = new File();
        $file->file_name = $fileUpload->getClientOriginalName();
        $file->type_check_id = $typeCheckId;
        $file->filepath = env('AWS_URL') . $filePath;
        $file->file_id = $id;
        $file->status = Consts::STATUS_CHECK_FILE['success'];
        $file->save();
        return $file;
    }
    public function getFileCheckData($file)
    {
        $path = $file->getRealPath();
        $extension = strtolower($file->getClientOriginalExtension());
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
//        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);
        return $spreadsheet->getActiveSheet()->toArray();
    }
    public function getFile($fileUpload)
    {
        $regex1 = '/^([\d]{4})s([\d]{3}.(.*))$/';
        $regex2 = '/(.*)(請求計算確認表)(.*)/';
        $files = [];
        foreach ($fileUpload as $file) {
            if (preg_match($regex2, $file->getClientOriginalName())) {
                $files[Consts::CHECK_2_FILE['FILE_1']] = $file;
            } elseif (preg_match($regex1, $file->getClientOriginalName())) {
                $files[Consts::CHECK_2_FILE['FILE_S']] = $file;
            }
//            elseif (str_starts_with($file->getClientOriginalName(), Consts::CHECK_2_FILE_3_NAME)) {
//                $files[Consts::CHECK_2_FILE['FILE_3']] = $file;
//            }
        }
        return $files;
    }
    public function getFileName($fileUpload)
    {
        $files = [];
        foreach ($fileUpload as $key => $file) {
            $files[$key] = $file->getClientOriginalName();
        }
        return $files;
    }
}
