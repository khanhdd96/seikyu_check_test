<?php

namespace App\Http\Services\ReadFilter;

class FilterNo1Comment implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($columnAddress, $row, $worksheetName = '')
    {
        if (in_array($columnAddress, range('A', 'Y'))) {
            return true;
        }
        return false;
    }
}
