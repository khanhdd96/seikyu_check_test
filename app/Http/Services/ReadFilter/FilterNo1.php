<?php

namespace App\Http\Services\ReadFilter;

class FilterNo1 implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($columnAddress, $row, $worksheetName = '')
    {
        if (!in_array($columnAddress, range('A', 'A'))) {
            return false;
        }
        return true;
    }
}
