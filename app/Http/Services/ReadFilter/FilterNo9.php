<?php

namespace App\Http\Services\ReadFilter;

class FilterNo9 implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($columnAddress, $row, $worksheetName = '')
    {
        if (in_array($columnAddress, range('A', 'H'))) {
            return true;
        }
        return false;
    }
}
