<?php

namespace App\Http\Services\ReadFilter;

class FilterNo4 implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($columnAddress, $row, $worksheetName = '')
    {
        if (in_array($columnAddress, ['C', 'E', 'H', 'K', 'L', 'N', 'P'])) {
            return true;
        }
        return false;
    }
}
