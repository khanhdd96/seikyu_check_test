<?php

namespace App\Jobs;

use App\Http\Services\CheckAllService;
use App\Http\Services\CheckNo10Service;
use App\Mail\CheckAllFail;
use App\Models\SettingMonth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CheckAll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 999999999;
    private $request;
    private $checkAllService;
    private $localFile;
    private $email;
    private $settingMonthId;
    public function __construct($request, $localFile, $email, $settingMonthId)
    {
        $this->request = $request;
        $this->localFile = $localFile;
        $this->email = $email;
        $this->settingMonthId = $settingMonthId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CheckAllService $checkAllService)
    {
        $this->checkAllService = $checkAllService;
        $this->checkAllService->checkAllFunction($this->request, $this->localFile, $this->email, $this->settingMonthId);
    }

    public function failed(\Throwable $throwable = null)
    {
        SettingMonth::whereId($this->settingMonthId)->update([
            'is_checking' => false,
            'job_id' => null
        ]);
        $localFiles = $this->localFile;
        foreach ($localFiles as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        Mail::to($this->email)->send(new CheckAllFail());
    }
}
