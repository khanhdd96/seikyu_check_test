<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class ReserveTypeCheckJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $to;
    protected $mail;

    /**
     * Create a new job instance.
     * SendOrderToAdminJob constructor.
     *
     * @param array $to
     * @param Mailable $mail
     */
    public function __construct(array $to, Mailable $mail)
    {
        $this->to = $to;
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $abc = Mail::to($this->to)->send($this->mail);
    }
}
