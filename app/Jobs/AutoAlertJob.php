<?php

namespace App\Jobs;

use App\Http\Services\Admin\SettingAlertAutoService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AutoAlertJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $alert;
    protected $time;
    protected $settingAlertAutoService;
    public function __construct($alert, $time)
    {
        $this->alert = $alert;
        $this->time = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SettingAlertAutoService $settingAlertAutoService)
    {
        $this->settingAlertAutoService = $settingAlertAutoService;
        $this->settingAlertAutoService->autoAlert($this->alert, $this->time);
    }
}
