<?php

namespace App\Jobs;

use App\Consts;
use App\Mail\Check1AdminFail;
use App\Mail\CheckAllFail;
use App\Models\Facility;
use App\Models\ForecastActualDetails;
use App\Models\Output2LayerBusiness;
use App\Models\PublicExpenseInformation;
use App\Models\ReceivablesListInquiry;
use App\Models\ReturnList;
use App\Models\TOrganizationData;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\IOFactory;
use File;

class CreateFileTemplateEarlyMonth implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 999999999;

    public function __construct($type = null)
    {
        $this->type = $type;
    }

    /**
     * CALLute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger()->info(['start_job' => date('Y-m-d H:i:s')]);
        $typeCheckNo1Admin = TypeCheckNo1Admin::where('date', '=', date('Y-m'))->first();
        logger()->info(['start_procedure' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 1_delete_old_data');
        logger()->info(['start_import_data' => date('Y-m-d H:i:s')]);
        $this->insertDataFormFile();
        logger()->info(['end_import_data' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select("DELETE FROM アウトプット");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_2階層_事業");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_事業所絞込前");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_番号あり");
        DB::connection('mysql2')->select("DELETE FROM 公費情報_加工後");
        DB::connection('mysql2')->select("DELETE FROM 予実明細_当月のみ");
        DB::connection('mysql2')->select("DELETE FROM 前回コメント_格納");
        DB::connection('mysql2')->select("DELETE FROM 債権一覧照会_番号あり");
        DB::connection('mysql2')->select("DELETE FROM 返戻一覧_番号あり");
        DB::connection('mysql2')->select('CALL 1_債権一覧objインサート');
        logger()->info(['1_債権一覧objインサート' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 2_アウトプット_列追加');
        logger()->info(['2_アウトプット_列追加' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 3_債権一覧照会_列追加');
        logger()->info(['3_債権一覧照会_列追加' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 4_返戻一覧_列追加');
        logger()->info(['4_返戻一覧_列追加' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 5_公費情報_加工');
        logger()->info(['5_公費情報_加工' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 6_最終請求年月_Null');
        logger()->info(['6_最終請求年月_Null' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 7_国保連_返戻一覧あり_work作成');
        logger()->info(['7_国保連_返戻一覧あり_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 8_国保連_返戻一覧あり_UPDATE');
        logger()->info(['8_国保連_返戻一覧あり_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 9_国保連_返戻一覧なし_UPDATE');
        logger()->info(['9_国保連_返戻一覧なし_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 10_国保連_保険者番号なし_UPDATE');
        logger()->info(['10_国保連_保険者番号なし_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 11_市区町村_返戻一覧あり_work作成');
        logger()->info(['11_市区町村_返戻一覧あり_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 12_市区町村_返戻一覧あり_UPDATE');
        logger()->info(['12_市区町村_返戻一覧あり_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 13_市区町村_返戻一覧なし_work作成');
        logger()->info(['13_市区町村_返戻一覧なし_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 14_市区町村_返戻一覧なし_UPDATE');
        logger()->info(['14_市区町村_返戻一覧なし_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 15_市区町村_保険者番号なし_UPDATE');
        logger()->info(['15_市区町村_保険者番号なし_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 16_市区町村_受託事業コード910000未満_work作成');
        logger()->info(['16_市区町村_受託事業コード910000未満_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 17_市区町村_受託事業コード910000未満_UPDATE');
        logger()->info(['17_市区町村_受託事業コード910000未満_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 18_利用者_Null');
        logger()->info(['18_利用者_Null' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 19_国保連_返戻一覧あり_最新返戻月追加クエリ');
        logger()->info(['19_国保連_返戻一覧あり_最新返戻月追加クエリ' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 20_市区町村_返戻一覧あり_最新返戻月追加クエリ');
        logger()->info(['20_市区町村_返戻一覧あり_最新返戻月追加クエリ' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 21_対応方法紐づけ_返戻事由コード_work作成');
        logger()->info(['21_対応方法紐づけ_返戻事由コード_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 22_対応方法紐づけ_返戻事由コード_該当なし作成');
        logger()->info(['22_対応方法紐づけ_返戻事由コード_該当なし作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 23_対応方法紐づけ_返戻事由コード_UPDATE');
        logger()->info(['23_対応方法紐づけ_返戻事由コード_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 24_対応方法紐づけ_増減決定_work作成');
        logger()->info(['24_対応方法紐づけ_増減決定_work作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 25_対応方法紐づけ_増減決定_該当なし作成');
        logger()->info(['25_対応方法紐づけ_増減決定_該当なし作成' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 26_対応方法紐づけ_増減決定_UPDATE');
        logger()->info(['26_対応方法紐づけ_増減決定_UPDATE' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL 27_利用者_未収金額_1以上');
        logger()->info(['27_利用者_未収金額_1以上' => date('Y-m-d H:i:s')]);

        DB::connection('mysql2')->select('CALL `28_利用者_未収金額_-1以下`');
        logger()->info(['`28_利用者_未収金額_-1以下`' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 29_利用者_最新審査状況Null');
        logger()->info(['29_利用者_最新審査状況Null' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 30_今回コメント_入金確定更新クエリ');
        logger()->info(['30_今回コメント_入金確定更新クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 31_前回コメント格納クエリ');
        logger()->info(['31_前回コメント格納クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 32_過去コメントフラグ_更新クエリ');
        logger()->info(['32_過去コメントフラグ_更新クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 33_前回コメント_4番目に古いコメント削除クエリ');
        logger()->info(['33_前回コメント_4番目に古いコメント削除クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 34_過去コメント２_アウトプット追加クエリ');
        logger()->info(['34_過去コメント２_アウトプット追加クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 35_過去コメント１_アウトプット追加クエリ');
        logger()->info(['35_過去コメント１_アウトプット追加クエリ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 36_前回コメント_追加');
        logger()->info(['36_前回コメント_追加' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 37_予実明細_加工_当月のみ');
        logger()->info(['37_予実明細_加工_当月のみ' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 38_督促状フラグ_あり');
        logger()->info(['38_督促状フラグ_あり' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 39_解約通知フラグ_あり');
        logger()->info(['39_解約通知フラグ_あり' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 40_サービス提供年月_絞込(?)', [date('Y/m')]);
        logger()->info(['40_サービス提供年月_絞込(?)' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 41_アウトプット_2階層');
        logger()->info(['41_アウトプット_2階層' => date('Y-m-d H:i:s')]);
        DB::connection('mysql2')->select('CALL 42_事業所マスタ絞込クエリ');
        logger()->info(['42_事業所マスタ絞込クエリ' => date('Y-m-d H:i:s')]);
        logger()->info(['end_procedure' => date('Y-m-d H:i:s')]);
        logger()->info(['start_map_data' => date('Y-m-d H:i:s')]);
        $facilities = Facility::whereNull('deleted_at')->where('is_active', true)->get();
        $dataLayerBusiness = Output2LayerBusiness::get();
        $dataFacilities = [];
        foreach ($dataLayerBusiness->toArray() as $data) {
            foreach ($facilities as $facility) {
                if ($facility->office_code == $data['指定事業所番号'] &&
                    $facility->hiiragi_code == $data['事業所コード']) {
                    $dataFacilities[$facility->id] = [
                        'name' => $facility->name,
                        'code' => $data['事業所コード']
                    ];
                    break;
                }
            }
        }
        if ($this->type == 'mid') {
            $step = Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'];
        } else {
            $step = Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_EARLY_MONTH'];
        }
        logger()->info(['end_map_data' => date('Y-m-d H:i:s')]);
        logger()->info(['count_facilities' => count($dataFacilities)]);
        DB::beginTransaction();
        try {
            foreach ($dataFacilities as $key => $data) {
                if ($this->type == 'mid') {
                    $pathDownload = env('APP_URL') . '/download/mid/' . time() . Str::random(20);
                    $typeCheck = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheckNo1Admin->id)
                        ->where('facility_id', $key)->first();
                    if (!$typeCheck) {
                        $typeCheck = new TypeCheckNo1AdminStep();
                    }
                    $typeCheck->deadline_mid_month = $typeCheckNo1Admin->deadline_mid_month;
                    $typeCheck->deadline = $typeCheckNo1Admin->deadline_early_month;
                    $typeCheck->url_download_file_mid_month = $pathDownload;
                    $typeCheck->step = $step;
                    $typeCheck->done_file_5 = true;
                    $typeCheck->status = 5;
                    $typeCheck->save();
                    logger()->info([
                        'end_create_file' => date('Y-m-d H:i:s'),
                        'path' => $pathDownload,
                        'id' => $typeCheck->id
                    ]);
                } else {
                    $typecheck = new TypeCheckNo1AdminStep();
                    $pathDownload = env('APP_URL') . '/download/early/' . time() . Str::random(20);
                    $typecheck->type_check_no_1_admin_id = $typeCheckNo1Admin->id;
                    $typecheck->done_file_1 = true;
                    $typecheck->deadline_early_month = $typeCheckNo1Admin->deadline_early_month;
                    $typecheck->deadline = $typeCheckNo1Admin->deadline_early_month;
                    $typecheck->url_download_file_early_month = $pathDownload;
                    $typecheck->facility_id = $key;
                    $typecheck->step = $step;
                    $typecheck->status = 0;
                    logger()->info([
                        'end_create_file' => date('Y-m-d H:i:s'),
                        'path' => $pathDownload,
                        'id' => $typecheck->id
                    ]);
                    $typecheck->save();
                    logger()->info(['end_create_file' => date('Y-m-d H:i:s')]);
                }
            }
            $typeCheckNo1Admin->step = $step;
            $typeCheckNo1Admin->is_run_cron = false;
            $typeCheckNo1Admin->save();
            DB::commit();
            DB::connection('mysql2')->select('CALL 1_delete_worktable');
            logger()->info(['1_delete_worktable' => date('Y-m-d H:i:s')]);
            logger()->info(['end_job' => date('Y-m-d H:i:s')]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $typeCheckNo1Admin->step = null;
            $typeCheckNo1Admin->url_folder_early_month = null;
            $typeCheckNo1Admin->is_run_cron = false;
            $typeCheckNo1Admin->has_error = true;
            $typeCheckNo1Admin->save();
        }
    }

    public function insertDataFormFile()
    {
        $date = date('Y-m');
        $folder = 'uploads/type-check-1-admin' . '/file-upload/' . $date;
        if ($this->type == 'mid') {
            $folder = 'uploads/type-check-1-admin' . '/file-upload/mid/' . $date;
        }
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_国保連.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_市区町村.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kouhiFile) {
                $kokuhoFile = explode(', ', $kouhiFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_利用者.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/公費情報.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kouhiFile) {
                $kouhiFile = explode(',', $kouhiFile[0]);
                return [
                    '法人名' => $kouhiFile[0],
                    '利用者' => $kouhiFile[1],
                    '公費番号' => $kouhiFile[2],
                    '履歴ID' => $kouhiFile[3],
                    '項番' => $kouhiFile[4],
                    '公費負担者番号' => $kouhiFile[5],
                    '公費受給者番号' => $kouhiFile[6],
                    '有効期間開始日（公費適用開始日）' => $kouhiFile[7],
                    '有効期間終了日（公費適用終了日）' => $kouhiFile[8],
                    '公費給付率' => $kouhiFile[9],
                    '低所得者フラグ' => $kouhiFile[10],
                    '自己負担金額' => $kouhiFile[11],
                    '削除ステータス' => $kouhiFile[12],
                    '作成ユーザー名' => $kouhiFile[13],
                    '作成事業所略称' => $kouhiFile[14],
                    '作成プログラムID' => $kouhiFile[15],
                    '作成日' => $kouhiFile[16],
                    '更新ユーザー名' => $kouhiFile[17],
                    '更新事業所略称' => $kouhiFile[18],
                    '更新プログラムID' => $kouhiFile[19],
                    '更新日' => $kouhiFile[20],
                    '利用者ID' => $kouhiFile[21]
                ];
            })->toArray();
            PublicExpenseInformation::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/返戻一覧.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($henreiFile) {
                $henreiFile = explode(', ', $henreiFile[0]);
                return [
                    '種別' => $henreiFile[0],
                    '対応状況メモ' => $henreiFile[1],
                    '請求年月' => $henreiFile[2],
                    '経過月数' => $henreiFile[3],
                    '事業所番号' => $henreiFile[4],
                    '事業所略称' => $henreiFile[5],
                    'サービス提供年月' => $henreiFile[6],
                    '保険者番号' => $henreiFile[7],
                    '被保険者番号' => $henreiFile[8],
                    'カナ氏名' => $henreiFile[9],
                    'サービス種類' => $henreiFile[10],
                    'サービス名称' => $henreiFile[11],
                    '単位数' => $henreiFile[12],
                    '再請求単位数' => $henreiFile[13],
                    'エラーコード' => $henreiFile[14],
                    'エラー内容' => $henreiFile[15],
                    '再請求予定年月' => $henreiFile[16],
                    '再請求状況' => $henreiFile[17]
                ];
            })->toArray();
            ReturnList::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/予実明細.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '利用者NO' => $kokuhoFile[3],
                    '対象年月日' => $kokuhoFile[4]
                ];
            })->toArray();
            ForecastActualDetails::insert($records);
        });
    }

    public function failed(\Throwable $throwable)
    {
        $typeCheckNo1Admin = TypeCheckNo1Admin::where('date', '=', date('Y-m'))->first();
        $date = date('Y-m');
        $typeCheckNo1Admin->is_run_cron = false;
        if ($this->type !== 'mid') {
            $typeCheckNo1Admin->step = null;
            $typeCheckNo1Admin->url_folder_early_month = null;
            $path = 'uploads/type-check-1-admin' . '/file-upload/' . $date;
            File::deleteDirectory(public_path($path));
        } else {
            $path = 'uploads/type-check-1-admin' . '/file-upload/mid/' . $date;
            $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'];
            File::deleteDirectory(public_path($path));
            $typeCheckNo1Admin->url_folder_mid_month = null;
        }
        $typeCheckNo1Admin->has_error = true;
        $typeCheckNo1Admin->save();
//        $emails = User::where('is_admin', '=', true)
//            ->where('email', '<>', 'tzoo2235@gmail.com')
//            ->pluck('email')->toArray();
//        Mail::to($emails)->send(new Check1AdminFail());
    }
}
