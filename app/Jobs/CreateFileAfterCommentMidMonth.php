<?php

namespace App\Jobs;

use App\Consts;
use App\Http\Services\ReadFilter\FilterNo1Comment;
use App\Http\Services\ReadFilter\FilterNo9;
use App\Models\Facility;
use App\Models\ForecastActualDetails;
use App\Models\LastComment;
use App\Models\Output2LayerBusiness;
use App\Models\PublicExpenseInformation;
use App\Models\ReceivablesListInquiry;
use App\Models\ReturnList;
use App\Models\TOrganizationData;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Filesystem\Filesystem;

class CreateFileAfterCommentMidMonth implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $facilities;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 999999999;

    public function __construct($facilities = [])
    {
        //
        $this->facilities = $facilities;
    }

    /**
     * CALLute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::connection('mysql2')->select('CALL 1_delete_old_data');
        $typeCheckNo1Admin = TypeCheckNo1Admin::where('date', '=', date('Y-m'))->first();
        $this->insertDataFormFile();
        DB::connection('mysql2')->select("DELETE FROM アウトプット");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_2階層_事業");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_事業所絞込前");
        DB::connection('mysql2')->select("DELETE FROM アウトプット_番号あり");
        DB::connection('mysql2')->select("DELETE FROM 公費情報_加工後");
        DB::connection('mysql2')->select("DELETE FROM 予実明細_当月のみ");
        DB::connection('mysql2')->select("DELETE FROM 前回コメント_格納");
        DB::connection('mysql2')->select("DELETE FROM 債権一覧照会_番号あり");
        DB::connection('mysql2')->select("DELETE FROM 返戻一覧_番号あり");
        DB::connection('mysql2')->select('CALL 1_債権一覧objインサート');
        logger()->info(['1_債権一覧objインサート' => now()]);
        DB::connection('mysql2')->select('CALL 2_アウトプット_列追加');
        logger()->info(['2_アウトプット_列追加' => now()]);

        DB::connection('mysql2')->select('CALL 3_債権一覧照会_列追加');
        logger()->info(['3_債権一覧照会_列追加' => now()]);

        DB::connection('mysql2')->select('CALL 4_返戻一覧_列追加');
        logger()->info(['4_返戻一覧_列追加' => now()]);

        DB::connection('mysql2')->select('CALL 5_公費情報_加工');
        logger()->info(['5_公費情報_加工' => now()]);

        DB::connection('mysql2')->select('CALL 6_最終請求年月_Null');
        logger()->info(['6_最終請求年月_Null' => now()]);

        DB::connection('mysql2')->select('CALL 7_国保連_返戻一覧あり_work作成');
        logger()->info(['7_国保連_返戻一覧あり_work作成' => now()]);

        DB::connection('mysql2')->select('CALL 8_国保連_返戻一覧あり_UPDATE');
        logger()->info(['8_国保連_返戻一覧あり_UPDATE' => now()]);

        DB::connection('mysql2')->select('CALL 9_国保連_返戻一覧なし_UPDATE');
        logger()->info(['9' => now()]);

        DB::connection('mysql2')->select('CALL 10_国保連_保険者番号なし_UPDATE');
        logger()->info(['10' => now()]);

        DB::connection('mysql2')->select('CALL 11_市区町村_返戻一覧あり_work作成');
        logger()->info(['11' => now()]);

        DB::connection('mysql2')->select('CALL 12_市区町村_返戻一覧あり_UPDATE');
        logger()->info(['12' => now()]);

        DB::connection('mysql2')->select('CALL 13_市区町村_返戻一覧なし_work作成');
        logger()->info(['13' => now()]);

        DB::connection('mysql2')->select('CALL 14_市区町村_返戻一覧なし_UPDATE');
        logger()->info(['14' => now()]);

        DB::connection('mysql2')->select('CALL 15_市区町村_保険者番号なし_UPDATE');
        logger()->info(['15' => now()]);

        DB::connection('mysql2')->select('CALL 16_市区町村_受託事業コード910000未満_work作成');
        logger()->info(['16' => now()]);

        DB::connection('mysql2')->select('CALL 17_市区町村_受託事業コード910000未満_UPDATE');
        logger()->info(['17' => now()]);

        DB::connection('mysql2')->select('CALL 18_利用者_Null');
        logger()->info(['19' => now()]);

        DB::connection('mysql2')->select('CALL 19_国保連_返戻一覧あり_最新返戻月追加クエリ');
        logger()->info(['19' => now()]);

        DB::connection('mysql2')->select('CALL 20_市区町村_返戻一覧あり_最新返戻月追加クエリ');
        logger()->info(['20' => now()]);

        DB::connection('mysql2')->select('CALL 21_対応方法紐づけ_返戻事由コード_work作成');
        logger()->info(['21' => now()]);

        DB::connection('mysql2')->select('CALL 22_対応方法紐づけ_返戻事由コード_該当なし作成');
        logger()->info(['22' => now()]);

        DB::connection('mysql2')->select('CALL 23_対応方法紐づけ_返戻事由コード_UPDATE');
        logger()->info(['23' => now()]);

        DB::connection('mysql2')->select('CALL 24_対応方法紐づけ_増減決定_work作成');
        logger()->info(['24' => now()]);

        DB::connection('mysql2')->select('CALL 25_対応方法紐づけ_増減決定_該当なし作成');
        logger()->info(['25' => now()]);

        DB::connection('mysql2')->select('CALL 26_対応方法紐づけ_増減決定_UPDATE');
        logger()->info(['26' => now()]);

        DB::connection('mysql2')->select('CALL 27_利用者_未収金額_1以上');
        logger()->info(['27' => now()]);

        DB::connection('mysql2')->select('CALL `28_利用者_未収金額_-1以下`');
        logger()->info(['28' => now()]);
        DB::connection('mysql2')->select('CALL 29_利用者_最新審査状況Null');
        logger()->info(['29' => now()]);
        DB::connection('mysql2')->select('CALL 30_今回コメント_入金確定更新クエリ');
        logger()->info(['30' => now()]);
        DB::connection('mysql2')->select('CALL 31_前回コメント格納クエリ');
        logger()->info(['31' => now()]);
        DB::connection('mysql2')->select('CALL 32_過去コメントフラグ_更新クエリ');
        logger()->info(['32' => now()]);
        DB::connection('mysql2')->select('CALL 33_前回コメント_4番目に古いコメント削除クエリ');
        logger()->info(['33' => now()]);
        DB::connection('mysql2')->select('CALL 34_過去コメント２_アウトプット追加クエリ');
        logger()->info(['34' => now()]);
        DB::connection('mysql2')->select('CALL 35_過去コメント１_アウトプット追加クエリ');
        logger()->info(['35' => now()]);
        DB::connection('mysql2')->select('CALL 36_前回コメント_追加');
        logger()->info(['36' => now()]);
        DB::connection('mysql2')->select('CALL 37_予実明細_加工_当月のみ');
        logger()->info(['37' => now()]);
        DB::connection('mysql2')->select('CALL 38_督促状フラグ_あり');
        logger()->info(['38' => now()]);
        DB::connection('mysql2')->select('CALL 39_解約通知フラグ_あり');
        logger()->info(['39' => now()]);
        DB::connection('mysql2')->select('CALL 40_サービス提供年月_絞込(?)', [date('Y/m')]);
        logger()->info(['40' => now()]);
        DB::connection('mysql2')->select('CALL 41_アウトプット_2階層');
        logger()->info(['41' => now()]);
        DB::connection('mysql2')->select('CALL 42_事業所マスタ絞込クエリ');
        logger()->info(['42' => now()]);
        $typechecks = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', '=', $typeCheckNo1Admin->id)
            ->where('step', '=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_MID_MONTH']);
        if ($this->facilities) {
            $typechecks = $typechecks->whereIn('facility_id', $this->facilities);
        }
        $typechecks = $typechecks->get();
        DB::beginTransaction();
        try {
            foreach ($typechecks as $data) {
                $pathDownload = env('APP_URL') . '/download/mid_comment/' . time() . Str::random(20);
                $data->done_file_7 = true;
                $data->url_download_file_mid_month_after_comment = $pathDownload;
                $data->step = Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'];
                $data->status = 3;
                $data->save();
            }
            if ($this->facilities) {
                $totalStep = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', '=', $typeCheckNo1Admin->id)
                    ->where('step', '>=', Consts::STEP_TYPE_1_ADMIN['UPLOAD_COMMENT_MID_MONTH'])->count();
                $facilityRead = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', '=', $typeCheckNo1Admin->id)
                    ->where('step', '>', Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'])->count();
                if ($totalStep == $facilityRead) {
                    $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH'];
                }
            }
            $typeCheckNo1Admin->is_run_cron = false;
            $typeCheckNo1Admin->save();
            DB::commit();
            DB::connection('mysql2')->select('CALL 1_delete_worktable');
        } catch (\Exception $exception) {
            DB::rollBack();
            $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'];
            $typeCheckNo1Admin->is_run_cron = false;
            $typeCheckNo1Admin->save();
        }
    }

    public function insertDataFormFile()
    {
        $date = date('Y-m');
        $typeCheck = TypeCheckNo1Admin::where('date', date('Y-m'))->first();
        $details = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id);
        if ($this->facilities) {
            $details = $details->whereIn('facility_id', $this->facilities);
        }
        $details = $details->whereNotNull('url_file_mid_month_comment')->get();
        $names = [];
        $path = public_path('uploads/type-check-1-admin' . '/file-comment/' . $date . '/mid/');
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        $file = new Filesystem;
        $file->cleanDirectory($path);
        foreach ($details as $key => $data) {
            $contents = Storage::disk('s3')->get($data->url_file_mid_month_comment);
            $name = explode('/', $data->url_file_mid_month_comment);
            \File::put($path . '/' . end($name), $contents);
            $names[] = end($name);
        }
        foreach ($names as $name) {
            $inputFileType = IOFactory::identify($path . '/' . $name);
            $reader = IOFactory::createReader($inputFileType);
            $filterSubset = new FilterNo1Comment();
            $reader->setReadFilter($filterSubset);
            $spreadsheet = $reader->load($path . '/' . $name);
            $spreadsheet->setActiveSheetIndex(4);
            $sheet_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $row = [];
            foreach ($sheet_data as $key => $data) {
                if ($key > 1) {
                    $row[$key]['事業所コード'] = $data[array_search('事業所コード', $sheet_data[1])];
                    $row[$key]['事業所名'] = $data[array_search('事業所名', $sheet_data[1])];
                    $row[$key]['サービス提供年月'] = $data[array_search('サービス提供年月', $sheet_data[1])];
                    $row[$key]['保険者番号'] = $data[array_search('保険者番号', $sheet_data[1])];
                    $row[$key]['保険者名'] = $data[array_search('保険者名', $sheet_data[1])];
                    $row[$key]['被保険者番号'] = $data[array_search('被保険者番号', $sheet_data[1])];
                    $row[$key]['利用者ID'] = $data[array_search('利用者ID', $sheet_data[1])];
                    $row[$key]['利用者名'] = $data[array_search('利用者名', $sheet_data[1])];
                    $row[$key]['請求先区分'] = $data[array_search('請求先区分', $sheet_data[1])];
                    $row[$key]['サービス種別・事業名'] = $data[array_search('サービス種別・事業名', $sheet_data[1])];
                    $row[$key]['請求金額'] = doubleval(
                        str_replace(
                            [',' . ' '],
                            '',
                            $data[array_search('請求金額', $sheet_data[1])]
                        )
                    );
                    $row[$key]['入金金額'] = doubleval(
                        str_replace(
                            [',', ' '],
                            '',
                            $data[array_search('入金金額', $sheet_data[1])]
                        )
                    );
                    $row[$key]['未収金額'] = doubleval(
                        str_replace(
                            [',', ' '],
                            '',
                            $data[array_search('未収金額', $sheet_data[1])]
                        )
                    );
                    $row[$key]['最終請求年月'] = $data[array_search('最終請求年月', $sheet_data[1])];
                    $row[$key]['最新審査状況'] = $data[array_search('最新審査状況', $sheet_data[1])];
                    $row[$key]['初回返戻月'] = $data[array_search('初回返戻月', $sheet_data[1])];
                    $row[$key]['最新返戻月'] = $data[array_search('最新返戻月', $sheet_data[1])];
                    $row[$key]['対応方法'] = $data[array_search('対応方法', $sheet_data[1])];
                    $row[$key]['今回事業所コメント'] = $data[array_search('今回事業所コメント', $sheet_data[1])];
                    $row[$key]['前回事業所コメント'] = $data[array_search('前回事業所コメント', $sheet_data[1])];
                    $row[$key]['過去コメント１'] = $data[array_search('過去コメント１', $sheet_data[1])];
                    $row[$key]['過去コメント２'] = $data[array_search('過去コメント２', $sheet_data[1])];
                    $row[$key]['請求締め判定'] = $data[array_search('請求締め判定', $sheet_data[1])];
                    $row[$key]['督促状フラグ'] = $data[array_search('督促状フラグ', $sheet_data[1])];
                    $row[$key]['解約通知フラグ'] = $data[array_search('解約通知フラグ', $sheet_data[1])];
                }
            }
            LastComment::insert($row);
            \File::delete(public_path($path . '/' . $name));
        }
        $date = date('Y-m');
        $folder = 'uploads/type-check-1-admin' . '/file-upload/' . $date;
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_国保連.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_市区町村.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kouhiFile) {
                $kokuhoFile = explode(', ', $kouhiFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/債権一覧照会_利用者.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '請求NO' => $kokuhoFile[0],
                    '事業所コード' => $kokuhoFile[1],
                    '事業所名' => $kokuhoFile[2],
                    '指定事業所番号' => $kokuhoFile[3],
                    '指定事業所名' => $kokuhoFile[4],
                    'サービス提供年月' => $kokuhoFile[5],
                    '取引日' => $kokuhoFile[6],
                    '市区町村番号' => $kokuhoFile[7],
                    '市区町村名' => $kokuhoFile[8],
                    '保険者番号' => $kokuhoFile[9],
                    '保険者名' => $kokuhoFile[10],
                    '利用者ID' => $kokuhoFile[11],
                    '利用者名' => $kokuhoFile[12],
                    '被保険者番号' => $kokuhoFile[13],
                    '受託事業コード' => $kokuhoFile[14],
                    '事業名' => $kokuhoFile[15],
                    '請求領収区分' => $kokuhoFile[16],
                    'サービス種類' => $kokuhoFile[17],
                    '請求先区分' => $kokuhoFile[18],
                    '原始請求年月' => $kokuhoFile[19],
                    '最終請求年月' => $kokuhoFile[20],
                    '請求金額' => str_replace('\\', ' ', $kokuhoFile[21]),
                    '入金金額' => str_replace('\\', ' ', $kokuhoFile[22]),
                    '回収不能額' => str_replace('\\', ' ', $kokuhoFile[23]),
                    '過剰入金額' => str_replace('\\', ' ', $kokuhoFile[24]),
                    '未集金額' => str_replace('\\', ' ', $kokuhoFile[25]),
                    '売上実番号' => $kokuhoFile[26],
                    '月次ステータス' => $kokuhoFile[27],
                    '請求ステータス' => $kokuhoFile[28],
                    '更新日' => $kokuhoFile[29],
                    '更新ユーザ' => $kokuhoFile[30],
                    '作成日' => $kokuhoFile[31],
                    '作成ユーザ' => $kokuhoFile[32]
                ];
            })->toArray();
            ReceivablesListInquiry::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/公費情報.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kouhiFile) {
                $kouhiFile = explode(',', $kouhiFile[0]);
                return [
                    '法人名' => $kouhiFile[0],
                    '利用者' => $kouhiFile[1],
                    '公費番号' => $kouhiFile[2],
                    '履歴ID' => $kouhiFile[3],
                    '項番' => $kouhiFile[4],
                    '公費負担者番号' => $kouhiFile[5],
                    '公費受給者番号' => $kouhiFile[6],
                    '有効期間開始日（公費適用開始日）' => $kouhiFile[7],
                    '有効期間終了日（公費適用終了日）' => $kouhiFile[8],
                    '公費給付率' => $kouhiFile[9],
                    '低所得者フラグ' => $kouhiFile[10],
                    '自己負担金額' => $kouhiFile[11],
                    '削除ステータス' => $kouhiFile[12],
                    '作成ユーザー名' => $kouhiFile[13],
                    '作成事業所略称' => $kouhiFile[14],
                    '作成プログラムID' => $kouhiFile[15],
                    '作成日' => $kouhiFile[16],
                    '更新ユーザー名' => $kouhiFile[17],
                    '更新事業所略称' => $kouhiFile[18],
                    '更新プログラムID' => $kouhiFile[19],
                    '更新日' => $kouhiFile[20],
                    '利用者ID' => $kouhiFile[21]
                ];
            })->toArray();
            PublicExpenseInformation::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/返戻一覧.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($henreiFile) {
                $henreiFile = explode(', ', $henreiFile[0]);
                return [
                    '種別' => $henreiFile[0],
                    '対応状況メモ' => $henreiFile[1],
                    '請求年月' => $henreiFile[2],
                    '経過月数' => $henreiFile[3],
                    '事業所番号' => $henreiFile[4],
                    '事業所略称' => $henreiFile[5],
                    'サービス提供年月' => $henreiFile[6],
                    '保険者番号' => $henreiFile[7],
                    '被保険者番号' => $henreiFile[8],
                    'カナ氏名' => $henreiFile[9],
                    'サービス種類' => $henreiFile[10],
                    'サービス名称' => $henreiFile[11],
                    '単位数' => $henreiFile[12],
                    '再請求単位数' => $henreiFile[13],
                    'エラーコード' => $henreiFile[14],
                    'エラー内容' => $henreiFile[15],
                    '再請求予定年月' => $henreiFile[16],
                    '再請求状況' => $henreiFile[17]
                ];
            })->toArray();
            ReturnList::insert($records);
        });
        LazyCollection::make(function () use ($folder) {
            $fh = fopen(public_path($folder . "/予実明細.csv"), 'r');
            while (($data = fgetcsv($fh)) !== false) {
                $professors = mb_convert_encoding($data, 'UTF-8', 'SJIS');
                $dataString = implode(", ", $professors);
                $row = explode(';', $dataString);
                yield $row;
            }
            fclose($fh);
        })->skip(1)->chunk(1000)->each(function (LazyCollection $chunk) {
            $records = $chunk->map(function ($kokuhoFile) {
                $kokuhoFile = explode(', ', $kokuhoFile[0]);
                return [
                    '利用者NO' => $kokuhoFile[3],
                    '対象年月日' => $kokuhoFile[4]
                ];
            })->toArray();
            ForecastActualDetails::insert($records);
        });
    }

    public function failed(\Throwable $throwable)
    {
        $typeCheckNo1Admin = TypeCheckNo1Admin::where('date', '=', date('Y-m'))->first();
        $typeCheckNo1Admin->is_run_cron = false;
        $typeCheckNo1Admin->step = Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'];
        $typeCheckNo1Admin->save();
    }
}
