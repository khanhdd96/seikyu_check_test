<?php

namespace App\Jobs;

use App\Consts;
use App\Mail\DistributeMail;
use App\Models\Facility;
use App\Models\File;
use App\Models\SettingMonth;
use App\Models\TemplateMail;
use App\Models\TypeCheckNo1AdminStep;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendDistributionMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $typeCheck;
    private $facilityIds;
    private $type;
    private $status;
    public function __construct($typeCheck, $facilityIds, $type, $status = -1)
    {
        $this->typeCheck = $typeCheck;
        $this->facilityIds = $facilityIds;
        $this->type = $type;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = now();
        $template = [];
        $facilityIds = $this->facilityIds;
        $type = $this->type;
        $column = Consts::FUNCTION_1_MAIL_TYPE[$type];
        $templateId = $this->typeCheck->{$column};
        $typeCheck = $this->typeCheck;
        $mails = Facility::whereIn('id', $facilityIds)->whereNotNull('email')->pluck('email', 'id')->toArray();
        if ($templateId) {
            $template = $this->formatMail($templateId, $facilityIds, $date);
        } else {
            $template['subject'] = Consts::FUNCTION_1_MAIL[$type]['SUBJECT'];
            $template['content'] = Consts::FUNCTION_1_MAIL[$type]['BODY'];
        }
        if ($type == Consts::TYPE_DISTRIBUTION_MAIL['DISTRIBUTE']) {
            $stepsData = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheck->id)
                ->whereIn('facility_id', $facilityIds)->get();
            foreach ($stepsData as $step) {
                $content = $template['content'];
                $url = $this->status == 1 ? 'url_download_file_early_month' : 'url_download_file_mid_month';
                $urlHtml = '<a href="' . $step->{$url} . '">' . $step->{$url} . '</a>';
                if (str_contains($template['content'], '#TEMPLATE_URL#')) {
                    $content = str_replace(
                        '#TEMPLATE_URL#',
                        $urlHtml,
                        $content
                    );
                } else {
                    $content .= $urlHtml;
                }
                $newTemplate['subject'] = $template['subject'];
                $newTemplate['content'] = $content;
                $mailable = new DistributeMail($newTemplate);
                if (isset($mails[$step->facility_id])) {
                    Mail::to($mails[$step->facility_id])->send($mailable);
                }
            }
        } else {
            $mailable = new DistributeMail($template);
            Mail::to(array_values($mails))->send($mailable);
        }
    }
    public function formatMail($templateId, $facilityIds, $date)
    {
        $mail = TemplateMail::findOrFail($templateId);
        $template['subject'] = $mail->title;
        $template['content'] = $mail->content;
        $facilities = $this->getFacility($facilityIds, $date);
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        return $this->formatAlertMail($template, $facilities);
    }

    public function getFacility($facilityIds, $date)
    {
        return Facility::whereIn('id', $facilityIds)
            ->whereIsActive(true)
            ->select('id', 'name', 'departments_id', 'list_type_check')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])
            ->withCount('typeCheckNotDone')->get()->toArray();
    }

    public function formatAlertMail($alert, $facilities)
    {
        $date = now();
        $subject = $alert['subject'];
        $content = $alert['content'];
        $subject = str_replace('#CURRENT_MONTH#', $date->month, $subject);
        $subject = str_replace('#CURRENT_DATE#', $date->day, $subject);
        $content = str_replace('#CURRENT_MONTH#', $date->month, $content);
        $content = str_replace('#CURRENT_DATE#', $date->day, $content);
        if (str_contains($content, '#TABLEDATA12H#')) {
            $table = $this->formatAlert12hTable($facilities);
            $content = str_replace('#TABLEDATA12H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA15H#')) {
            $table = $this->formatAlert15hTable();
            $content = str_replace('#TABLEDATA15H#', $table, $content);
        }
        if (str_contains($content, '#TABLEDATA18H#')) {
            $content = $this->formatAlert18h($content);
        }
        if (str_contains($content, '#TABLE_NOT_DONE#')) {
            $content = $this->formatNotDoneList($content, $facilities);
        }
        return [
            'subject' => $subject,
            'content' => $content
        ];
    }
    public function formatAlert12hTable($facilities)
    {
        $table = '<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th></tr></thead><tbody>';
        $department = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (!in_array($departmentName, $department)) {
                $department[] = $departmentName;
                if (count($department) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }

    public function formatAlert15hTable()
    {
        $date = now();
        $table = '<table><thead><tr><th>グループ</th><th>支社</th><th>事業所名</th><th>提出予定日</th><th>初回提出日</th>';
        $table .= '<th>初回提出時間</th><th>終了日</th><th>終了時間</th></tr></thead><tbody>';
        $doneFacilities = SettingMonth::whereYearMonth(date('Y/m'))
            ->whereStatusFacilitity(Consts::STATUS_CHECK_FILE['success'])
            ->pluck('facilities_id')
            ->toArray();
        $facilities = Facility::whereIn('id', $doneFacilities)
            ->select('id', 'name', 'departments_id')
            ->with([
                'department',
                'currentSettingMonth',
                'typeCheck' => function ($query) use ($date) {
                    $query->whereYearMonthCheck($date->format('Y/m'));
                }])->get()->toArray();
        foreach ($facilities as &$facility) {
            $facility['department_name'] = $facility['department'] ? $facility['department']['name'] : '';
        }
        $department = array_column($facilities, 'department_name');
        array_multisort($department, SORT_ASC, $facilities);
        $typeCheckIds = [];
        foreach ($facilities as &$facility) {
            foreach ($facility['type_check'] as $typeCheck) {
                $typeCheckIds[] = $typeCheck['id'];
            }
            $facility['type_check'] = array_column($facility['type_check'], 'id');
        }
        $files = File::whereYear('created_at', $date->year)
            ->whereMonth('created_at', $date->month)
            ->whereIn('type_check_id', $typeCheckIds)->latest()->get()->toArray();
        foreach ($facilities as &$facility) {
            $facilityFiles = [];
            foreach ($files as $file) {
                if (in_array($file['type_check_id'], $facility['type_check'])) {
                    $facilityFiles[] = $file;
                }
            }
            $facility['file'] = $facilityFiles;
        }
        $departments = [];
        $index = 1;
        foreach ($facilities as $value) {
            $departmentName = $value['department_name'];
            $deadline = $value['current_setting_month'] ? $value['current_setting_month']['deadline'] : '';
            $deadline = $deadline ? date('Y/m/d', strtotime($deadline)) : '';
            if (empty($value['file'])) {
                $firstCheckDate = $firstCheckTime = $lastCheckDate = $lastCheckTime = '';
            } else {
                $firstCheck = end($value['file']);
                $firstCheckDate = date('Y/m/d', strtotime($firstCheck['created_at']));
                $firstCheckTime = date('H:i', strtotime($firstCheck['created_at']));
                $lastCheck = array_shift($value['file']);
                $lastCheckDate = date('Y/m/d', strtotime($lastCheck['created_at']));
                $lastCheckTime = date('H:i', strtotime($lastCheck['created_at']));
            }
            if (!in_array($departmentName, $departments)) {
                $departments[] = $departmentName;
                if (count($departments) > 1) {
                    $index++;
                }
            }
            $table .= "<tr><td style='text-align: center'>{$index}</td>";
            $table .= "<td style='padding: 0 5px'>{$departmentName}</td>";
            $table .= "<td style='padding: 0 5px'>{$value['name']}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$deadline}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$firstCheckTime}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckDate}</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>{$lastCheckTime}</td></tr>";
        }
        $table .= '</tbody></table>';
        return $table;
    }
    public function formatAlert18h($content)
    {
        $facilities = Facility::whereIsActive(true)->select('id', 'name', 'departments_id', 'list_type_check')
            ->with('currentSettingMonth')
            ->withCount('typeCheckError')
            ->orderBy('type_check_error_count', 'DESC')
            ->get()->toArray();
        $table = '<table><thead><th>事業所名</th><th>エラー回数</th><th>ステータス</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $status = $facility['current_setting_month']['status_facilitity'] ??
                Consts::STATUS_CHECK_FILE['not_has_check'];
            $status = Consts::STATUS_CHECK[$status];
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px'>エラー{$facility['type_check_error_count']}</td>";
            $table .= "<td style='padding: 0 5px'>{$status}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLEDATA18H#', $table, $content);
    }
    public function formatNotDoneList($content, $facilities)
    {
        $table = '<table><thead><th>事業所名</th><th>未チェック機能数／チェック機能総数</th></thead><tbody>';
        foreach ($facilities as $facility) {
            $allFunctions = explode(',', $facility['list_type_check']);
            $allFunctions = count($allFunctions);
            $table .= "<tr><td style='padding: 0 5px'>{$facility['name']}事業所</td>";
            $table .= "<td style='padding: 0 5px; text-align: center'>";
            $table .= "{$facility['type_check_not_done_count']}/{$allFunctions}</td></tr>";
        }
        $table .= '</tbody></table>';
        return str_replace('#TABLE_NOT_DONE#', $table, $content);
    }
}
