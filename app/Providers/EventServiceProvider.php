<?php

namespace App\Providers;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use App\Consts;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use App\Messages;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen('Aacotroneo\Saml2\Events\Saml2LoginEvent', function (Saml2LoginEvent $event) {
            $messageId = $event->getSaml2Auth()->getLastMessageId();
            // your own code preventing reuse of a $messageId to stop replay attacks
            $user = $event->getSaml2User();
            // 属性からUserモデルを取得する
            $userData = [
                'id' => $user->getUserId(),
                'attributes' => $user->getAttributes(),
                'assertion' => $user->getRawSamlAssertion(),
            ];
            $user = \App\Models\User::where('code', $userData['attributes']['code'][0])->first();


            //if it does not exist create it and go on  or show an error message
            if ($user) {
                if ($user->id == env('DEFAULT_ADMIN_ID')) {
                    $user->is_admin = 1;
                    $user->save();
                }
                $isLogin = $this->checkLoginUser($user);
                if ($isLogin) {
                    Auth::loginUsingId($user->id, true);
                } else {
                    return redirect()->route('login')->withErrors(['msg' => Messages::LOGIN_ERROR]);
                }
            } else {
                return redirect()->route('login')->withErrors(['msg' => Messages::LOGIN_ERROR]);
            }
        });

        Event::listen('Aacotroneo\Saml2\Events\Saml2LogoutEvent', function ($event) {
            Auth::logout();
            Session::save();
        });
    }

    public function checkLoginUser($user)
    {
        $isLogin = false;
        if ($user->is_admin || $user->is_facility) {
            $isLogin = true;
        }
        return $isLogin;
    }
}
