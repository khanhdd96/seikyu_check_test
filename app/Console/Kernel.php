<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:disableUpload')->dailyAt('23:59');
        $schedule->command('cloneDataDepartment:cron')->dailyAt('00:00');
        $schedule->command('cloneDataUser:cron')->dailyAt('00:10');
        $schedule->command('cloneDataPositions:cron')->dailyAt('00:00');
        $schedule->command('createSettingMonth:cron')->dailyAt('00:10');
        $schedule->command('customAlert:cron')->everyMinute();
        $schedule->command('createAutoAlert:cron')->lastDayOfMonth('23:55');
        $schedule->command('autoAlert:cron')->everyMinute();
        $schedule->command('moveFileToS3')->monthlyOn(1, '02:00');
        $schedule->command('check:deadline')->dailyAt('01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
