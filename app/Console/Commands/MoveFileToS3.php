<?php

namespace App\Console\Commands;

use App\Models\TypeCheckNo1Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MoveFileToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moveFileToS3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $month = date('Y-m', strtotime('-2 months'));
        $typeCheck = TypeCheckNo1Admin::where('date', $month)->first();
        if ($typeCheck) {
            $path = public_path($typeCheck->url_folder_early_month);
            $midPath = public_path($typeCheck->url_folder_mid_month);
            $s3Path = 'early/' . $month;
            $s3MidPath = 'mid/' . $month;
            $typeCheck->url_folder_early_month = config('app.aws_url') . $s3Path;
            $typeCheck->url_folder_mid_month = config('app.aws_url') . $s3MidPath;
            try {
                $this->moveFile($path, $s3Path);
                $this->moveFile($midPath, $s3MidPath);
                $typeCheck->save();
                \Log::channel('moveFileToS3')->info(['message' => 'Move file successfully']);
            } catch (\Exception $e) {
                \Log::channel('moveFileToS3')->info(['message' => 'Fail to move file']);
            }
        }
    }
    public function moveFile($path, $s3Path)
    {
        if (File::exists($path)) {
            $files = array_diff(scandir($path), ['.', '..']);
            foreach ($files as $file) {
                Storage::disk('s3')
                    ->put($s3Path . '/' . $file, file_get_contents($path . '/' . $file));
            }
            File::deleteDirectory($path);
        }
    }
}
