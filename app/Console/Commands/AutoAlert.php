<?php

namespace App\Console\Commands;

use App\Http\Services\Admin\SettingAlertAutoService;
use Illuminate\Console\Command;

class AutoAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoAlert:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $settingAlertAutoService;
    public function __construct(SettingAlertAutoService $settingAlertAutoService)
    {
        parent::__construct();
        $this->settingAlertAutoService = $settingAlertAutoService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->settingAlertAutoService->autoAlert();
        \Log::channel('cronjob')->info(['message' => 'Cronjob auto alert runs successfully']);
    }
}
