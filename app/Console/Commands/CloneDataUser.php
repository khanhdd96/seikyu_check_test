<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Services\CloneDatas\CloneDataService;

class CloneDataUser extends Command
{
    protected $cloneDataService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloneDataUser:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CloneDataService $cloneDataService)
    {
        $this->cloneDataService = $cloneDataService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::channel('cronjob')->info(['message' => 'Clone user data']);
        return $this->cloneDataService->usersClone();
    }
}
