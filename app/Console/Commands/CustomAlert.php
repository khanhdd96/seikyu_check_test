<?php

namespace App\Console\Commands;

use App\Http\Services\Admin\SettingAlertCustomService;
use Illuminate\Console\Command;

class CustomAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customAlert:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $service;
    public function __construct(SettingAlertCustomService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->service->alertCustom();
        \Log::channel('cronjob')->info(['message' => 'Cronjob custom alert runs successfully']);
    }
}
