<?php

namespace App\Console\Commands;

use App\Models\SettingMonth;
use App\Models\Facility;
use App\Models\TypeCheck;
use Carbon\Carbon;
use App\Consts;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class CreateSettingMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createSettingMonth:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create setting month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $month = date('Y/m');
        $facilities = Facility::where('is_active', 1)->whereDoesntHave('settingMonth', function ($query) use ($month) {
            $query->where('year_month', '=', $month);
        })->get();
        $settingMonthCreates = [];
        $userId = Auth::id() ?? env('DEFAULT_ADMIN_ID');
        foreach ($facilities as $facility) {
            $settingMonthCreates[] = SettingMonth::create([
                'year_month' => $month,
                'facilities_id' => $facility->id,
                'updated_by' => $userId,
                'created_by' => $userId,
                'count_file_done' => 0,
                'deadline' => null,
                'can_upload' => true,
                'status_facilitity' => Consts::STATUS_CHECK_FILE['not_has_check']
            ]);
        }
        $listTyeCheck = $facilities->pluck('list_type_check', 'id')->toArray();
        foreach ($settingMonthCreates as $settingMonthCreate) {
            foreach (explode(',', $listTyeCheck[$settingMonthCreate->facilities_id]) as $typeCheck) {
                $typeCheckHas = TypeCheck::where('year_month_check', '=', $month)
                    ->where('code_check', '=', $typeCheck)
                    ->where('facilities_id', '=', $settingMonthCreate->facilities_id)->first();
                $typeCheckHasas[] = $typeCheckHas;
                if ($typeCheckHas === null) {
                    TypeCheck::create([
                        'code_check' => $typeCheck,
                        'setting_months_id' => $settingMonthCreate->id,
                        'facilities_id' => $settingMonthCreate->facilities_id,
                        'updated_by' => $userId,
                        'created_by' => $userId,
                        'status' => Consts::STATUS_CHECK_FILE['not_has_check'],
                        'year_month_check' => $month
                    ]);
                }
            }
        }
        \Log::channel('cronjob')->info(['message' => 'Create setting month']);
    }
}
