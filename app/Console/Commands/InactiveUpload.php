<?php

namespace App\Console\Commands;

use App\Models\SettingMonth;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InactiveUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:disableUpload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'chuyển trạng thái khi quá deadline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d');
        SettingMonth::whereDate('deadline', '=', $date)->update(['can_upload' => false]);
        \Log::channel('cronjob')->info(['message' => 'Cronjob inactive upload']);
    }
}
