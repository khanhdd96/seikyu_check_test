<?php

namespace App\Console\Commands;

use App\Consts;
use App\Jobs\SendDistributionMail;
use App\Mail\DistributeMail;
use App\Models\Facility;
use App\Models\TypeCheckNo1Admin;
use App\Models\TypeCheckNo1AdminStep;
use Illuminate\Console\Command;

class CheckDeadlineFunction1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $month = date('Y-m');
        $typeCheck = TypeCheckNo1Admin::where('date', $month)->first();
        if ($typeCheck) {
            $typeCheckId = $typeCheck->id;
            $step = $typeCheck->step;
            if ($step < 4 || ($step >= 8 && $step < 10)) {
                $date = date('Y-m-d');
                $facilities = TypeCheckNo1AdminStep::where('type_check_no_1_admin_id', $typeCheckId)
                    ->where('deadline', $date)
                    ->where(function ($query) {
                        $query->where('step', '<', 3)->orWhere('step', 9);
                    })
                    ->pluck('facility_id')->toArray();
                if ($facilities) {
                    SendDistributionMail::dispatch(
                        $typeCheck,
                        $facilities,
                        Consts::TYPE_DISTRIBUTION_MAIL['OVER_DEADLINE']
                    );
                }
            }
        }
    }
}
