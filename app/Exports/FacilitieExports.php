<?php

namespace App\Exports;

use App\Consts;
use App\Http\Services\Admin\FacilitityService;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class FacilitieExports implements FromCollection
{
    use Exportable;

    protected $request;
    protected $facilitys;

    public function __construct($request, $facilitys)
    {
        $this->request = $request;
        $this->facilitys = $facilitys;
    }

    public function collection()
    {
        $array[] = ['請求チェック状況管理'.$this->request->month];
        $array[] =
        [
            '順番',
            '事業所名',
            '支店名',
            '締め切り',
            '状況', 'やり直し回数',
            '①請求前確認リスト',
            '②請求締めチェックツール',
            '③送り出し指示データ一覧',
            '④総合事業請求止めチェック機能',
            '⑤総合事業計上漏れチェック',
            '⑥障害チェック',
            '⑦トランデータチェック',
            '⑧おまかせチェック',
            '⑨Mファイルチェック',
            '⑩Sファイルチェック',
            '⑪TH障害データチェック',
            'アップロード制限',
        ];
        foreach ($this->facilitys as $key => $value) {
            $date = $this->request->month;
            $typeChecks = [];
            $listTypePhecks = explode(',', $value->list_type_check);
            $statusChecks = [];
            if (isset($value->settingMonth[0])) {
                $typeChecks = $value->settingMonth[0]->typeChecks;
                foreach ($typeChecks as $typeCheck) {
                    $statusChecks[$typeCheck['code_check']] = $typeCheck['status'];
                }
            }

            $deadline = isset($value->settingMonth[0]) && $value->settingMonth[0]->deadline ?
                date('Y/m/d', strtotime($value->settingMonth[0]->deadline)) : null;

            $status_facilitity = isset($value->settingMonth[0]) && $value->settingMonth[0]->status_facilitity
                ? Consts::STATUS_CHECK[$value->settingMonth[0]->status_facilitity] : Consts::STATUS_CHECK[4];

            $countFile = 0;
            if (isset($value->settingMonth[0]) && $value->settingMonth[0]->typeChecks) {
                foreach ($value->settingMonth[0]->typeChecks as $valueSetting) {
                    $valueSetting['code_check'] == Consts::TYPE_MENU[6] ?
                        $countFile += $valueSetting['file_not_hold_count'] / 2 :
                        $countFile += $valueSetting['file_not_hold_count'];
                }
            }
            $type = [];
            foreach (Consts::TYPE_MENU as $keyType => $valueType) {
                $type[$keyType] = null;
                if (in_array($valueType, $listTypePhecks)) {
                    $type[$keyType] = isset($statusChecks[$valueType]) ?
                        Consts::STATUS_CHECK[$statusChecks[$valueType]] : Consts::STATUS_CHECK[4];
                }
            }

            $settingMonth = null;
            if (isset($value->settingMonth[0])) {
                $settingMonth = $value->settingMonth[0];
            }
            // $canUpload = FacilitityService::checkCanUpload($settingMonth, $date, $value->id);
            $canUpload = '○';
            if ($settingMonth) {
                $canUpload = $settingMonth->can_upload ? '○' : '';
            }
            $array[] = [
                $key + 1,
                $value->name,
                $value->department->name,
                $deadline,
                $status_facilitity,
                $countFile,
                $type[1],
                $type[2],
                $type[3],
                $type[4],
                $type[5],
                $type[6],
                $type[7],
                $type[8],
                $type[9],
                $type[10],
                $type[11],
                $canUpload,
            ];
        }

        return collect($array);
    }
}
