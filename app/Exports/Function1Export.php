<?php

namespace App\Exports;

use App\Consts;
use App\Http\Services\Admin\FacilitityService;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class Function1Export implements FromCollection
{
    use Exportable;

    private $detail;

    public function __construct($detail)
    {
        $this->detail = $detail;
    }

    public function collection()
    {
        $array[] =
            [
                '事業所名',
                '支店名',
                '状況',
                '更新日',
                'ファイル状況',
                '個別締切日',
                '配布対象',
            ];
        $details = $this->detail;
        if (!is_null($details)) {
            foreach ($details as $detail) {
                $name = $detail->name;
                $status = $detail->detailTypeCheck1Admin ?
                    Consts::STATUS_FILE_CHECK_NO_1_ADMIN[$detail->detailTypeCheck1Admin->status] : '';
                $update = ($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->updated_at) ?
                    date('Y/m/d', strtotime($detail->detailTypeCheck1Admin->updated_at))  : '';
                $step = '';
                if (!$detail->detailTypeCheck1Admin) {
                    $step = '-';
                } else {
                    foreach (Consts::FUNCTION_1_STEPS as $key => $value) {
                        if ($detail->detailTypeCheck1Admin->{$value}) {
                            $step .= $key;
                        }
                    }
                }
                if ($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->deadline) {
                    $deadline = date('Y/m/d', strtotime($detail->detailTypeCheck1Admin->deadline));
                } else {
                    $deadline = '';
                }
                $type = $detail->detailTypeCheck1Admin &&
                ($detail->detailTypeCheck1Admin->step > Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']) ?
                    '配布する' : ' 配布停止';
                $array[] = [$name, $detail->department->name, $status, $update, $step, $deadline, $type];
            }
        }

        return collect($array);
    }
}
