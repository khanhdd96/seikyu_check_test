<?php

namespace App;

class Messages
{
    const UPLOAD_FILE_ERROR = 'ァイルがアップロードするときエラーが発生して。再度お試しください。';
    const FILE_FOMAT_WRONG = 'アップロードされたファイル形式が正しくない。';
    const FILE_REQUIRED = 'ファイルを必須選択とする';
    const SYSTERM_ERROR = 'システムのエラー。';
    const FILE_TEMPLATE_ERROR = 'エラーが発生した。テンプレートファイルを確認し、再度お試しください。';
    const FACILITY_REQUIRED = '少なくとも1つの事業所を選択してください。';
    const CONTENT_MAIL_REQUIRED = 'メールの内容を入力してください。';
    const CONTENT_MAIL_LENGTH = 'メール内容が50文字以上入力してください。';
    const SEND_MAIL_SUCCESS = 'メールが発信した。';
    const RESERVE_SUCCESS = '保留した。';
    const RESERVE_REASON_REQUIRE = '保留理由を必須入力とする。';
    const RESERVE_REASON_LENGTH = '5から150文字までの理由。';
    const EMPTY_RECORD = '該当データがありません。';
    const LOGIN_ERROR = 'このアカウントが本システムにアクセスする権限がありません。';
    const OVERSIZE = 'サイズが最大110MBのアップロードするファイル';
    const FILE_NAME_ERROR = 'ファイル名が正しくありません。';
    const COMPANY_NAME_REQUIRED = '中心業務名称を入力してください。';
    const COMPANY_NAME_MAX_100 = '中心業務名称は100文字以内入力してください。';
    const DAY_OFF_REQUIRED = '日付を選択してください。';
    const DAY_OFF_UNIQUE = '以前に選択した休暇';
    const CATEGORY_NAME_MAX_100 = '区分名称は100文字以内入力してください。';
    const CATEGORY_NAME_REQUIRED = '区分名称を入力してください。';
    const CODE_NAME_REQUIRED = 'コード名を入力してください。';
    const CODE_NAME_MAX = ' 本体コードは100文字以内入力してください';
    const SUB_CODE_NAME_REQUIRED = '割増コードを入力してください。';
    const SUB_CODE_NAME_MAX = '割増コードは100文字以内入力してください';
    const DELETE_MASTER_CODE_FAIL = '追加のコードはまだ使用中です';
    const NOT_EXCEL = 'アップロードされたファイル形式が正しくない';
    const NOT_EXCEL_OR_CSV = 'アップロードされたファイル形式が正しくない';
    const CHECK_2_NAME_ERROR = 'ファイル名エラー';
    const CHECK_1_NAME_ERROR = '正しくファイル名称をアップロードしてください。';
    const CHECK_2_FILE_1_DUPLICATE = '請求計算確認表 ファイル にはすでにファイルアップロード済みです。';
    const CHECK_2_FILE_2_DUPLICATE = 'Sファイル にはすでにファイルアップロード済みです。';
    const CHECK_4_FILE_FORMAT = 'アップロードされたファイル形式が正しくない';
    const CHECK_4_FILE_NAME = 'ファイル名エラー';
    const IN_INSURANCE_UNIQUE = '中心業務名称が既存しています。他の中心業務名称を入力してください。';
    const OUT_INSURANCE_UNIQUE = '区分名称が既存しています。他の区分名称を入力してください。';
    const MASTERCODE_UNIQUE = '本体コードが既存しています。他の本体コードを入力してください。';
    const MASTERSUBCODE_UNIQUE = '割増コードが既存しています。他の割増コードを入力してください。';
    const CHECK_1_FILE_NAME_MISS = 'チェックファイルを全てアップロードしてください。';
    const NAME_ERROR = 'ファイル名エラー';
    const CSV_ERROR = 'CSV: ファイル名エラー';
    const EXCEL_ERROR = 'Excel: ファイル名エラー';
}
