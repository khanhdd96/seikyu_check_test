<?php

namespace App\Rules;

use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo7FileName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return str_contains($value->getClientOriginalName(), '請求前確認リスト');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::NAME_ERROR;
    }
}
