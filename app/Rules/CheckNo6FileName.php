<?php

namespace App\Rules;

use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo6FileName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $month;
    public function __construct($month)
    {
        $this->month = $month;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (preg_match('/(.*)(トランチェック)[\d]{10}-[\d]{8}-(.*)/', $value)) {
            $fileDate = substr(
                $value,
                strpos($value, 'トランチェック') + strlen('トランチェック') + 11,
                8
            );
            return $this->month == substr($fileDate, 0, 4) . '/' . substr($fileDate, 4, 2);
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::NAME_ERROR;
    }
}
