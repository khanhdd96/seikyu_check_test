<?php

namespace App\Rules;

use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo4FileName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $month;
    public function __construct($month)
    {
        $this->month = $month;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->checkName('おまかせ情報ダウンロード_', $value) ||
            $this->checkName('おまかせ情報ダウンロード_', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::NAME_ERROR;
    }

    public function checkName($text, $value)
    {
        if (preg_match("/(.*)({$text})[\d]{8}(.*)/", $value)) {
            $fileDate = substr(
                $value,
                strpos($value, $text) + strlen($text),
                8
            );
            return $this->month == substr($fileDate, 0, 4) . '/' . substr($fileDate, 4, 2);
        }
        return false;
    }
}
