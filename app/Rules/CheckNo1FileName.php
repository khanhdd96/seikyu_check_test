<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo1FileName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $name = $value->getClientOriginalName();
        if (str_contains($name, Consts::CHECK_1_FILE_1_NAME) ||
            $this->arraySearchPartial(Consts::CHECK_1_FILE_2_NAME, $name) ||
            str_contains($name, Consts::CHECK_1_FILE_3_NAME) ||
            preg_match('/送り出し指示データ一覧|送り出し指示データ一覧/', $name) ||
            str_contains($name, Consts::CHECK_1_FILE_5_NAME)) {
//            $regex = '/^(.*)((-)[\d]{8}(-)[\d]{6})((\.)(.*))$/';
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::CHECK_1_NAME_ERROR;
    }

    public function arraySearchPartial($arr, $keyword)
    {
        $hasFile = false;
        foreach ($arr as $string) {
            $hasFile = false;
            if (str_contains($keyword, $string)) {
                $hasFile = true;
            }
        }
        return $hasFile;
    }
}
