<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use App\Models\DayOff;
use App\Models\FileInInsurance;
use App\Models\FileOutsideInsurance;
use App\Models\MasterCode;
use App\Models\MasterSubCode;
use Illuminate\Contracts\Validation\Rule;

class CheckUniqueDayOff implements Rule
{
    private $id;
    private $dateOff;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $datas = explode(',', $value);
        $exists = DayOff::whereIn('date_off', $datas)->get()->pluck('date_off');
        $this->dateOff = $exists;
        if (count($exists)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        foreach ($this->dateOff as $data) {
            $message[] = date('Y/m/d', strtotime($data)) .'は既に登録されています。再度確認してください。';
        }
        return $message;
    }
}
