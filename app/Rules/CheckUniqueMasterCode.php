<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use App\Models\FileInInsurance;
use App\Models\FileOutsideInsurance;
use App\Models\MasterCode;
use App\Models\MasterSubCode;
use Illuminate\Contracts\Validation\Rule;

class CheckUniqueMasterCode implements Rule
{
    /**
     * @var mixed|null
     */
    private $id;
    private $type;
    private $subcodeName;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type = null, $id = null, $subcodeName = [])
    {
        $this->id = $id;
        $this->type = $type;
        $this->subcodeName = $subcodeName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exist = MasterCode::whereCodeName($value);
        if ($this->type && $this->type == 'master_code') {
            $exist = $exist->where('id', '<>', $this->id);
        }
        $exist = $exist->first();
        if (!$exist) {
            $exist = MasterSubCode::whereSubCodeName($value);
            if ($this->type && $this->type == 'sub_code_name') {
                $key = array_search($value, $this->subcodeName);
                if (isset($this->id[$key])) {
                    $exist = $exist->where('id', '<>', $this->id[$key]);
                }
            }
            $exist = $exist->first();
            if (!$exist) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->type && $this->type == 'master_code') {
            return Messages::MASTERCODE_UNIQUE;
        }
        return Messages::MASTERSUBCODE_UNIQUE;
    }
}
