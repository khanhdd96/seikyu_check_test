<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo2FileName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $name = $value->getClientOriginalName();
        $regex1 = '/(.*)([\d]{4})s([\d]{3})(.*).(.*)$/';
        $regex2 = '/(.*)(請求計算確認表)(.*)/';
        return preg_match($regex1, $name) || preg_match($regex2, $name);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::CHECK_2_NAME_ERROR;
    }
}
