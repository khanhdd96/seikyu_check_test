<?php

namespace App\Rules;

use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo3FileCSV implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $month;
    public function __construct($month)
    {
        $this->month = $month;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (preg_match('/(.*)(TH01_)[\d]{6}(.*)/', $value)) {
            $fileDate = substr(
                $value,
                strpos($value, 'TH01_') + strlen('TH01_'),
                6
            );
            return $this->month == substr($fileDate, 0, 4) . '/' . substr($fileDate, 4, 2);
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::CSV_ERROR;
    }
}
