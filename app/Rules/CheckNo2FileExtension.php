<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo2FileExtension implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $name = $value->getClientOriginalName();
        $extension = $value->getClientOriginalExtension();
        if (preg_match('/(.*)(請求計算確認表)(.*)/', $name)) {
            return in_array($extension, Consts::FILE_EXCEL);
        } elseif (preg_match('/(.*)([\d]{4})s([\d]{3})(.*).(.*)$/', $name)) {
            return in_array($extension, ['xlsx', 'xls', 'csv']);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::FILE_FOMAT_WRONG;
    }
}
