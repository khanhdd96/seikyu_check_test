<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use App\Models\FileInInsurance;
use App\Models\FileOutsideInsurance;
use Illuminate\Contracts\Validation\Rule;

class CheckUniqueInsurance implements Rule
{
    /**
     * @var mixed|null
     */
    private $id;
    private $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type = null, $id = null)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exist = FileInInsurance::whereCompanyName($value);
        if ($this->type && $this->type == 'in-insurance') {
            $exist = $exist->where('id', '<>', $this->id);
        }
        $exist = $exist->first();
        if (!$exist) {
            $exist = FileOutsideInsurance::whereCategoryName($value);
            if ($this->type && $this->type == 'out-insurance') {
                $exist = $exist->where('id', '<>', $this->id);
            }
            $exist = $exist->first();
            if (!$exist) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->type == 'in-insurance') {
            return Messages::IN_INSURANCE_UNIQUE;
        }
        return Messages::OUT_INSURANCE_UNIQUE;
    }
}
