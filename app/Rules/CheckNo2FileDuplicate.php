<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo2FileDuplicate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $files;
    public function __construct($files)
    {
        $this->files = $files;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $hasFile1 = false;
        $hasFile2 = false;
        $fileNames = $this->checkFileName($value);
        foreach ($fileNames as $name) {
            if (str_starts_with($name, Consts::CHECK_2_FILE_1_NAME)) {
                if (!$hasFile1) {
                    $hasFile1 = true;
                } else {
                    return false;
                }
            }
            if (str_starts_with($name, Consts::CHECK_2_FILE_2_NAME)) {
                if (!$hasFile2) {
                    $hasFile2 = true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $files = $this->files;
        $fileNames = $this->checkFileName($files);
        $keyError1 = -1;
        $keyError2 = -1;
        $hasFile1 = false;
        $hasFile2 = false;
        foreach ($fileNames as $key => $name) {
            if (str_starts_with($name, Consts::CHECK_2_FILE_1_NAME)) {
                if (!$hasFile1) {
                    $hasFile1 = true;
                } else {
                    $keyError1 = $key;
                }
            }
            if (str_starts_with($name, Consts::CHECK_2_FILE_2_NAME)) {
                if (!$hasFile2) {
                    $hasFile2 = true;
                } else {
                    $keyError2 = $key;
                }
            }
        }
        $message = $keyError1 >= 0 ? Messages::CHECK_2_FILE_1_DUPLICATE : Messages::CHECK_2_FILE_2_DUPLICATE;
        $keyError = $keyError1 >= 0 ? $keyError1 : $keyError2;
        return $keyError . '/' . $message;
    }

    public function checkFileName($files)
    {
        $fileNames = [];
        foreach ($files as $file) {
            $fileNames[] = $file->getClientOriginalName();
        }
        return $fileNames;
    }
}
