<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use Illuminate\Contracts\Validation\Rule;

class CheckNo1AllFileExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $files;
    private $hasFiles;

    public function __construct($files)
    {
        $this->files = $files;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $fileNames = $this->getFileName($value);
        $ruleNames = Consts::CHECK_1_FILE_NAME;
        foreach ($ruleNames as $key => $ruleName) {
            $this->hasFiles[$key] = false;
            foreach ($fileNames as $name) {
                if ($key != 1) {
                    if ($key == 3) {
                        if (preg_match('/送り出し指示データ一覧|送り出し指示データ一覧/', $name)) {
                            $this->hasFiles[$key] = true;
                        }
                    } else {
                        if (str_contains($name, $ruleName)) {
                            $this->hasFiles[$key] = true;
                        }
                    }
                } else {
                    if ($this->arraySearchPartial(Consts::CHECK_1_FILE_2_NAME, $name)) {
                        $this->hasFiles[$key] = true;
                    }
                }
            }
        }
        if (array_search(false, $this->hasFiles) !== false) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::CHECK_1_FILE_NAME_MISS;
    }

    public function getFileName($files)
    {
        $fileNames = [];
        foreach ($files as $file) {
            $fileNames[] = $file->getClientOriginalName();
        }
        return $fileNames;
    }

    public function arraySearchPartial($arr, $keyword)
    {
        $hasFile = false;
        foreach ($arr as $string) {
            $hasFile = false;
            if (str_contains($keyword, $string)) {
                $hasFile = true;
            }
        }
        return $hasFile;
    }
}
