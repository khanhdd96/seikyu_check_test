<?php

namespace App\Rules;

use App\Consts;
use App\Messages;
use App\Models\FileInInsurance;
use App\Models\FileOutsideInsurance;
use App\Models\MasterCode;
use App\Models\MasterSubCode;
use Illuminate\Contracts\Validation\Rule;

class CheckUniqueMasterSubCode implements Rule
{
    /**
     * @var mixed|null
     */
    private $subcodeNames;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($subcodeNames = [])
    {
        $this->subcodeNames = $subcodeNames;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $keys = array_keys($this->subcodeNames, $value);
        if (count($keys) > 1) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Messages::MASTERCODE_UNIQUE;
    }
}
