<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterCode extends Model
{
    use HasFactory;
    protected $table = "master_code";
    protected $fillable = [
        "code_name",
        "type",
        "date_update"
    ];

    public function masterSubCode()
    {
        return $this->hasMany(MasterSubCode::class);
    }
}
