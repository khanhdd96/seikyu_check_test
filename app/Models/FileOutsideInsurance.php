<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileOutsideInsurance extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "file_outside_insurances";
    protected $primaryKey = "id";
    protected $fillable = [
        "category_name",
        "fiduciary_goal",
        "date_update"
    ];
}
