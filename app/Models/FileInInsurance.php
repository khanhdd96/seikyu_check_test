<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileInInsurance extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "file_in_insurances";
    protected $primaryKey = "id";
    protected $fillable = [
        "company_name",
        "fiduciary_goal",
        "date_update"
    ];
}
