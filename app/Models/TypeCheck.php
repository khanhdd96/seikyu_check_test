<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Consts;

class TypeCheck extends BaseModel
{
    use HasFactory;

    protected $table = 'type_check';
    public $timestamps=true;
    protected $fillable = [
        'code_check',
        'setting_months_id',
        'facilities_id',
        'status',
        'reason',
        'year_month_check',
        'check_reserve',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    public function files()
    {
        return $this->hasMany(File::class, 'type_check_id')->orderBy('id', 'DESC');
    }

    public function fileNotHold()
    {
        return $this->hasMany(File::class, 'type_check_id')
            ->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])->orderBy('id', 'DESC');
    }

    public function fileError()
    {
        return $this->hasMany(File::class, 'type_check_id')->where('status', '=', Consts::STATUS_CHECK_FILE['error']);
    }
    public function fileSuccessLatest()
    {
        return $this->hasOne(File::class, 'type_check_id')
                    ->whereStatus(Consts::STATUS_CHECK_FILE['success'])->whereNull('file_id')->latest();
    }

    public function filesCodeError($errors)
    {
        return $this->hasMany(File::class, 'type_check_id')
            ->withCount('errorsCode')->orderBy('id', 'DESC');
    }
    public function facility()
    {
        return $this->hasOne('App\Models\Facility', 'id', 'facilities_id');
    }
    public function checkTimes()
    {
        return $this->hasMany(File::class, 'type_check_id')
            ->where('status', '!=', Consts::STATUS_CHECK_FILE['hold'])
            ->whereNull('file_id');
    }
    public function checkErrorTimes()
    {
        return $this->hasMany(File::class, 'type_check_id')
            ->whereStatus(Consts::STATUS_CHECK_FILE['error'])
            ->whereNull('file_id');
    }
    public function lastErrorCheckFile()
    {
        return $this->hasOne(File::class, 'type_check_id')
            ->whereStatus(Consts::STATUS_CHECK_FILE['error'])
            ->whereNull('file_id')
            ->latest();
    }
}
