<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterSubCode extends Model
{
    use HasFactory;
    protected $table = "master_sub_code";
    protected $fillable = [
        "sub_code_name",
        "type",
        "master_code_id"
    ];
}
