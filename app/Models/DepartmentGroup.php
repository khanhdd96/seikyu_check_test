<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepartmentGroup extends BaseModel
{
    use HasFactory, SoftDeletes;
    protected $table = 'department_groups';
    public $timestamps=true;
    public function facilities()
    {
        return $this->hasMany('App\Models\Facility', 'departments_id');
    }
}
