<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeCheckNo1AdminStep extends Model
{
    use HasFactory;
    protected $table = 'type_check_no1_admin_steps';
}
