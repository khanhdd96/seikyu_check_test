<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Group extends BaseModel
{
    use HasFactory;

    protected $table = 'groups';
    public $timestamps=true;
    protected $fillable = [
        'group_name',
        'user_ids',
        'facilitity_ids',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    protected $casts = [
        'user_ids' => 'json',
        'facilitity_ids' => 'json'
    ];
}
