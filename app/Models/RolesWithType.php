<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesWithType extends Model
{
    use HasFactory;
    public $timestamps=true;
    protected $fillable = [
        'id_component',
        'type',
        'type_role',
        'created_at',
        'updated_at',
    ];
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'id_component');
    }
    public function position()
    {
        return $this->hasOne('App\Models\Position', 'id', 'id_component');
    }
    public function departmentGroup()
    {
        return $this->hasOne('App\Models\DepartmentGroup', 'id', 'id_component');
    }
}
