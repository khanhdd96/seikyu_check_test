<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DepartmentGroupUser extends Model
{
    use HasFactory;
    protected $table = 'department_group_user';
    protected $fillable = [
        'department_group_id',
        'users_id',
        'is_clone'
    ];
}
