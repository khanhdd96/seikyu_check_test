<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Output2LayerBusiness extends Model
{
    use HasFactory;
    protected $table = 'アウトプット_2階層_事業';
    protected $connection = 'mysql2';
    protected $fillable = [
        '事業所コード',
        '事業所名',
        'サービス提供年月',
        '保険者番号',
        '保険者名',
        '被保険者番号',
        '利用者ID',
        '利用者名',
        '請求先区分',
        'サービス種別・事業名',
        '請求金額',
        '入金金額',
        '未収金額',
        '最終請求年月',
        '最新審査状況',
        '初回返戻月',
        '最新返戻月',
        '対応方法',
        '今回事業所コメント',
        '前回事業所コメント',
        '過去コメント１',
        '過去コメント２',
        '請求締め判定',
        '督促状フラグ',
        '解約通知フラグ',
        '二階層名称',
        '指定事業所番号',
        '指定事業所名'
    ];
}
