<?php

namespace App\Models;

use App\Consts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'files';
    public $timestamps=true;
    protected $fillable = [
        'file_name',
        'status',
        'suspended_message',
        'filepath',
        'type_check_id',
        'check_send_mail',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'mail_recipients'
    ];
    protected $casts = [
      'mail_recipients' => 'json',
    ];
    public function errors()
    {
        return $this->hasMany(Error::class, 'files_id');
    }

    public function file()
    {
        return $this->hasOne(File::class, 'file_id');
    }
    public function files()
    {
        return $this->hasMany(File::class, 'file_id');
    }
    public function warnings()
    {
        return $this->hasMany(Error::class, 'files_id')->whereType(Consts::TYPE_WARNING);
    }
}
