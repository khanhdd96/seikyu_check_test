<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DayOff extends Model
{
    use HasFactory;
    protected $table = "day_offs";
    protected $fillable = [
        "date_off",
        "date_off_type",
        "date_off_update",
        "user_id"
    ];
}
