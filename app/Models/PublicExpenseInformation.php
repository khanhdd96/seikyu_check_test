<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicExpenseInformation extends Model
{
    use HasFactory;
    protected $table = '公費情報';
    protected $connection = 'mysql2';
    protected $fillable = [
        '法人名',
        '利用者',
        '公費番号',
        '履歴ID',
        '項番',
        '公費負担者番号',
        '公費受給者番号',
        '有効期間開始日（公費適用開始日）',
        '有効期間終了日（公費適用終了日）',
        '公費給付率',
        '低所得者フラグ',
        '自己負担金額',
        '削除ステータス',
        '作成ユーザー名',
        '作成事業所略称',
        '作成プログラムID',
        '作成日',
        '更新ユーザー名',
        '更新事業所略称',
        '更新プログラムID',
        '更新日',
        '利用者ID'
    ];
}
