<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Error extends BaseModel
{
    use HasFactory;

    protected $table = 'errors';
    public $timestamps=true;
    protected $fillable = [
        'message',
        'error_position',
        'files_id',
        'error_code',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'type'
    ];
}
