<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class MailSetting extends BaseModel
{
    use HasFactory;

    protected $table = 'mail_setting';
    public $timestamps = true;
    protected $fillable = [
        'user_ids',
        'grourp_ids',
        'facilitity_ids',
        'start_date',
        'time',
        'frequency_type',
        'interval',
        'send_times',
        'subject',
        'content',
        'type',
        'list_mail',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    protected $casts =
        [
            'user_ids' => 'json',
            'grourp_ids' => 'json',
            'facilitity_ids' => 'json',
            'list_mail' => 'json'
        ];

    public function historyMails()
    {
        return $this->hasMany('App\Models\HistoryMail', 'mail_setting_id');
    }

    public function latestHistoryMail()
    {
        return $this->hasOne('App\Models\HistoryMail', 'mail_setting_id')->latest('datetime');
    }
}
