<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FacilityUser extends Model
{
    use HasFactory;
    protected $table = 'facilitity_user';
    protected $fillable = [
        'facilities_id',
        'users_id',
    ];

    public function role()
    {
        return $this->hasOne('App\Models\Role', 'users_id', 'users_id');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Role', 'users_id', 'users_id');
    }
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'users_id');
    }
}
