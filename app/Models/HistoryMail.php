<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryMail extends Model
{
    use HasFactory;

    protected $table = 'history_mail';
    public $timestamps = false;
    protected $fillable = [
        'datetime',
        'mail_setting_id',
        'user_ids',
        'grourp_ids',
        'facilitity_ids',
        'start_date',
        'time',
        'email_recipients',
        'list_mail'
    ];
    protected $casts =
        [
            'user_ids' => 'json',
            'grourp_ids' => 'json',
            'facilitity_ids' => 'json',
            'email_recipients' => 'json',
            'list_mail' => 'json'
        ];
}
