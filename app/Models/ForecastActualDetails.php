<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForecastActualDetails extends Model
{
    use HasFactory;
    protected $table = '予実明細';
    protected $connection = 'mysql2';
}
