<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Facility;

class Department extends BaseModel
{
    use HasFactory, SoftDeletes;
    protected $table = 'departments';
    public $timestamps=true;
    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    public function facilities()
    {
        return $this->hasMany('App\Models\Facility', 'departments_id');
    }

    public function facilitieNotActive()
    {
        return $this->hasMany('App\Models\Facility', 'departments_id')->where('is_active', 0);
    }
    public function facilitieActive()
    {
        return $this->hasMany('App\Models\Facility', 'departments_id')->where('is_active', true);
    }
}
