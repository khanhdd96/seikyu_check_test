<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceivablesListInquiry extends Model
{
    use HasFactory;
    protected $table = '債権一覧照会';
    protected $connection = 'mysql2';
    protected $fillable = [
        '請求NO',
        '事業所コード',
        '事業所名',
        '指定事業所番号',
        '指定事業所名',
        'サービス提供年月',
        '取引日',
        '市区町村番号',
        '市区町村名',
        '保険者番号',
        '保険者名',
        '利用者ID',
        '利用者名',
        '被保険者番号',
        '受託事業コード',
        '事業名',
        '請求領収区分',
        'サービス種類',
        '請求先区分',
        '原始請求年月',
        '最終請求年月',
        '請求金額',
        '入金金額',
        '回収不能額',
        '過剰入金額',
        '未集金額',
        '売上実番号',
        '月次ステータス',
        '請求ステータス',
        '更新日',
        '更新ユーザ',
        '作成日',
        '作成ユーザ',

    ];
}
