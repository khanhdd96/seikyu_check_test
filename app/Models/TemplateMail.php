<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TemplateMail extends BaseModel
{
    use HasFactory;

    protected $table = 'template_mails';
    public $timestamps = true;
    protected $fillable = [
        'name',
        'title',
        'content',
        'created_at',
        'updated_at'
    ];
}
