<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LastComment extends Model
{
    use HasFactory;
    protected $table = '前回コメント';
    protected $connection = 'mysql2';
}
