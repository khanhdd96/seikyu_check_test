<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';
    public $timestamps=true;
    protected $fillable = [
        'view',
        'edit',
        'users_id',
        'facilities_id',
        'created_at',
        'updated_at',
        'position_id'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'users_id');
    }
    public function position()
    {
        return $this->hasOne('App\Models\Position', 'id', 'position_id');
    }
}
