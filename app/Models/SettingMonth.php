<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Facility;
use App\Models\TypeCheck;
use App\Consts;

class SettingMonth extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'setting_months';
    public $timestamps=true;
    protected $fillable = [
        'year_month',
        'count_file_done',
        'deadline',
        'facilities_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'can_upload',
        'deadline_late',
        'status_facilitity',
        'is_checking',
        'job_id',
    ];
    public function typeCheck($type)
    {
        $query = $this->hasOne(TypeCheck::class, 'setting_months_id', 'id')
                        ->where('code_check', '=', $type);
        return $query;
    }

    public function typeChecks()
    {
        return $this->hasMany(TypeCheck::class, 'setting_months_id', 'id');
    }

    public function updateStatusFacilitity($settingMonthId)
    {
        $settingMonth = $this->find($settingMonthId);
        $facility = Facility::find($settingMonth->facilities_id)->list_type_check;
        $listTypeChecks = explode(',', $facility);

        $typeChecks = $settingMonth->typeChecks;
        $countNotHasCheck = 0;
        $countSuccess = 0;
        $countError = 0;
        $countHold = 0;
        $count = 0;

        foreach ($typeChecks as $value) {
            if (in_array($value->code_check, Consts::CNC_OPEN)) {
                ++$count;
                if ($value['status'] == Consts::STATUS_CHECK_FILE['not_has_check']) {
                    ++$countNotHasCheck;
                }

                if ($value['status'] == Consts::STATUS_CHECK_FILE['success']) {
                    ++$countSuccess;
                }

                if ($value['status'] == Consts::STATUS_CHECK_FILE['error']) {
                    ++$countError;
                }

                if ($value['status'] == Consts::STATUS_CHECK_FILE['hold']) {
                    ++$countHold;
                }
            }
        }

        if ($countSuccess == $count) {
            $settingMonth->status_facilitity = Consts::STATUS_CHECK_FILE['success'];
        }

        if ($countHold > 0) {
            $settingMonth->status_facilitity = Consts::STATUS_CHECK_FILE['hold'];
        }

        if ($countError > 0) {
            $settingMonth->status_facilitity = Consts::STATUS_CHECK_FILE['error'];
        }

        if ($countNotHasCheck > 0 || $count < count(Consts::CNC_OPEN)) {
             $settingMonth->status_facilitity = Consts::STATUS_CHECK_FILE['not_has_check'];
        }

        $settingMonth->save();
        return $settingMonth;
    }

    public static function oldCanupload($id, $deadline)
    {
        $status = false;
        $typeCheck = TypeCheck::where('setting_months_id', $id)
            ->orderBy('updated_at', 'DESC')->first();

        if ($typeCheck && $deadline) {
            $updatedAt = $typeCheck->updated_at;
            if (strtotime($deadline) < strtotime(date('Y-m-d 00:00:00', strtotime($updatedAt)))) {
                $status = true;
            }
        }
        return $status;
    }

    public function job()
    {
        return $this->hasOne(Job::class, 'id', 'job_id');
    }
}
