<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Log;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use App\Consts;

class BaseModel extends Model
{
    use HasFactory;

    protected static $rules = [];

    /**
     * Return model validation rules
     *
     * @return array
     */
    public static function getRules()
    {
        return static::$rules;
    }

    protected static $customizeRules = [];

    /**
     * Return model validation rules
     *
     * @return array
     */
    public static function getCustomizeRules()
    {
        return static::$customizeRules;
    }

    protected static $createRules = [];

    /**
     * Return model validation rules
     *
     * @return array
     */
    public static function getCreateRules()
    {
        return static::$createRules;
    }

    protected static $updateRules = [];

    /**
     * Return model validation rules
     *
     * @return array
     */
    public static function getUpdateRules()
    {
        return static::$updateRules;
    }

    protected static $searchRules = [];

    /**
     * Return model validation rules
     *
     * @return array
     */
    public static function getSearchRules()
    {
        return static::$searchRules;
    }

    public static function getExcludeRelations()
    {
        return static::$excludeRelations;
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $user = Auth::user();
            $model->created_by = $user && $user->id ? $user->id : Consts::USER_ID_DEFAULT;
            $model->updated_by = $user && $user->id ? $user->id : Consts::USER_ID_DEFAULT;
        });
        static::creating(function ($model) {
            $user = Auth::user();
            $model->created_by = $user && $user->id ? $user->id : Consts::USER_ID_DEFAULT;
            $model->updated_by = $user && $user->id ? $user->id : Consts::USER_ID_DEFAULT;
        });
        static::updating(function ($model) {
            $user = Auth::user();
            $model->updated_by = $user && $user->id ? $user->id : Consts::USER_ID_DEFAULT;
        });
    }
}
