<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnList extends Model
{
    use HasFactory;
    protected $table = '返戻一覧';
    protected $connection = 'mysql2';

    protected $fillable = [
        '種別',
        '対応状況メモ',
        '請求年月',
        '経過月数',
        '事業所番号',
        '事業所略称',
        'サービス提供年月',
        '保険者番号',
        '被保険者番号',
        'カナ氏名',
        'サービス種類',
        'サービス名称',
        '単位数',
        '再請求単位数',
        'エラーコード',
        'エラー内容',
        '再請求予定年月',
        '再請求状況'
    ];
}
