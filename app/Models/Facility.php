<?php

namespace App\Models;

use App\Consts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends BaseModel
{
    use HasFactory, SoftDeletes;
    protected $table = 'facilities';
    public $timestamps=true;
    protected $fillable = [
        'name',
        'departments_id',
        'list_type_check',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'departments_id');
    }
    public function settingMonth()
    {
        return $this->hasMany('App\Models\SettingMonth', 'facilities_id');
    }
    public function typeChecks()
    {
        return $this->hasMany('App\Models\TypeCheck', 'facilities_id');
    }
    public function facilitityUsers()
    {
        return $this->hasMany('App\Models\FacilityUser', 'facilities_id');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Role', 'facilities_id', 'id');
    }

    public function facilitityRoles()
    {
        return $this->hasMany('App\Models\Role', 'facilities_id');
    }
    public function detailTypeCheck1Admin()
    {
        return $this->hasOne('App\Models\TypeCheckNo1AdminStep', 'facility_id');
    }

    public function currentSettingMonth()
    {
        return $this->hasOne('App\Models\SettingMonth', 'facilities_id')
            ->whereYearMonth(date('Y/m'));
    }

    public function typeCheck()
    {
        return $this->hasMany('App\Models\TypeCheck', 'facilities_id');
    }

    public function typeCheckError()
    {
        return $this->hasMany('App\Models\TypeCheck', 'facilities_id')
            ->whereStatus(Consts::STATUS_CHECK_FILE['error'])
            ->whereYearMonthCheck(date('Y/m'));
    }

    public function typeCheckNotDone()
    {
        return $this->hasMany('App\Models\TypeCheck', 'facilities_id')
            ->where('status', '<>', Consts::STATUS_CHECK_FILE['success'])
            ->whereNotIn('code_check', ['1', '2', '4', '5'])
            ->whereYearMonthCheck(date('Y/m'));
    }
}
