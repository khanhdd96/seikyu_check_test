<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TOrganizationData extends Model
{
    use HasFactory;
    protected $table = 't_組織データ';
    protected $connection = "mysql2";
}
