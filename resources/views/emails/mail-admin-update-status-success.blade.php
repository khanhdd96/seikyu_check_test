<html>
<head>
    <title></title>
</head>
<body>
<div class="OutlineElement Ltr SCXW252131732 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
    <div class="OutlineElement Ltr SCXW94558784 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
        <p>ご担当者様&nbsp;</p>
        <p>お疲れ様です。&nbsp;</p>
        <p>請求締めの提出ありがとうございます。&nbsp;</p>
        <p>「保留」となっておりました内容を確認し、ステータスを完了に変更いたしましたのでお知らせいたします。&nbsp;</p>
        <p>今月も請求締めのご対応ありがとうございました。&nbsp;</p>
        <p>次月もどうぞよろしくお願いいたします。 &nbsp;</p>
    </div>
</div>
</body>
</html>
