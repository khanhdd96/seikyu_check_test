<html>
<head>
    <title></title>
</head>
<body>
<div class="OutlineElement Ltr SCXW252131732 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
    <div class="OutlineElement Ltr SCXW94558784 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
        <p>{{$datas['nameFacility']}}事業所 <br>各位</p>

        <p>お疲れ様です。</p>
        <p>下記項目の保留結果は完了になりましたので、以上で請求締め終了になります。</p>
        <p>よろしくお願いいたします。</p>
        <p>【保留項目】</p>
        @foreach($datas['typeCheck'] as $typecheck)
            <p style="margin:0px; padding:0px">{{$typecheck['name']}}</p>
            <p style="margin:0px; padding-left:10px">・{{$typecheck['status']}}</p>
        @endforeach
    </div>
</div>
</body>
</html>
