<html>
<head>
    <title></title>
</head>
<body>
<div class="OutlineElement Ltr SCXW252131732 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
    <div class="OutlineElement Ltr SCXW94558784 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
        <p>ご担当者様&nbsp;</p>
        <p>お疲れ様です。&nbsp;</p>
        <p>請求締めの提出ありがとうございます。 &nbsp;</p>
        <p>「保留」となっておりました内容を確認し、<span style="color:red; font-size:14px;font-weight: bold;">ステータスをエラー</span>に変更いたしましたのでお知らせいたします。&nbsp;</p>
        <p style="font-weight: bold;">エラー内容は下記の通りとなります。 &nbsp;</p>
        <p style="font-weight: bold;">お手数をお掛けいたしますが、ご確認の上再度ご提出くださいますようお願いいたします。 &nbsp;</p>
    </div>
</div>
</body>
</html>
