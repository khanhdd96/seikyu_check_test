<html>
<head>
    <title></title>
</head>
<body>
<div class="OutlineElement Ltr SCXW252131732 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
    <div class="OutlineElement Ltr SCXW94558784 BCX0" style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr; font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, Arial, Verdana, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);">
        <p>営業経理G 各位&nbsp;</p>
        <p>{{$datas['nameFacility']}}事業所が請求締め完了となりましたが、保留ステータスがある状態です。&nbsp;</p>

        <p>内容を確認し、ステータス変更を行ってください。&nbsp;</p>
        <p>保留ステータスとなっているチェック項目は以下の通りです。&nbsp;</p>
        @foreach($datas['typeCheck'] as $typecheck)
            <p style="margin:0px; padding:0px">{{$typecheck}}</p>
        @endforeach
    </div>
</div>
</body>
</html>
