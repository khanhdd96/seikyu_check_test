<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- css -->
    <link rel="stylesheet" href="{{asset('css/main.css?v=6')}}">
    <link rel="stylesheet" href="{{asset('css/template.css?v=6')}}">
    <!-- css -->
</head>
<body>
    <div class="wrapper d-flex flex-row">
        <!-- sidebar -->
        <section class="left-sidebar">
            <h2 class="title-sidebar">
                ロゴ
            </h2>
            <div class="nav flex-column nav-pills">
                <!-- menu active -->
                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.0585 6.29175C17.5252 4.48342 16.0168 2.97508 14.2085 2.44175C12.8335 2.04175 11.8835 2.07508 11.2252 2.56675C10.4335 3.15842 10.3418 4.22508 10.3418 4.98342V7.05842C10.3418 9.10842 11.2752 10.1501 13.1085 10.1501H15.5002C16.2502 10.1501 17.3252 10.0584 17.9168 9.26675C18.4252 8.61675 18.4668 7.66675 18.0585 6.29175Z" fill="#7C9AA1"/>
                            <path opacity="0.4" d="M15.7585 11.6335C15.5418 11.3835 15.2252 11.2418 14.9002 11.2418H11.9168C10.4502 11.2418 9.25849 10.0501 9.25849 8.58346V5.60013C9.25849 5.27512 9.11683 4.95846 8.86683 4.74179C8.62516 4.52513 8.29183 4.42513 7.97516 4.46679C6.01683 4.71679 4.21683 5.79179 3.04183 7.40846C1.85849 9.03346 1.42516 11.0168 1.80016 13.0001C2.34183 15.8668 4.63349 18.1585 7.50849 18.7001C7.96683 18.7918 8.42516 18.8335 8.88349 18.8335C10.3918 18.8335 11.8502 18.3668 13.0918 17.4585C14.7085 16.2835 15.7835 14.4835 16.0335 12.5251C16.0752 12.2001 15.9752 11.8751 15.7585 11.6335Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    ダッシュボード
                </a>
                <!-- menu active -->

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    請求前確認リスト
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    請求締めチェックツール
                </a>

                <a href="#" class="nav-link nav-sidebar active">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    送り出し指示データ一覧
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    総合事業請求止め <br />
                    チェック機能
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    障害チェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    トランデータチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    おまかせチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    Mファイルチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    Sファイルチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    障害データチェック
                </a>
            </div>
        </section>
        <!-- sidebar -->

        <section class="right-main">
            <header class="main-header">
                <div class="header-account">
                    <span class="name-account">AML.Co  JP</span>
                    <span class="email-account">aml@gmail.com</span>
                </div>
                <a href="#logout" class="header-logout">
                    <i class="icon-logout">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M9 7.2V16.79C9 20 11 22 14.2 22H16.79C19.99 22 21.99 20 21.99 16.8V7.2C22 4 20 2 16.8 2H14.2C11 2 9 4 9 7.2Z" fill="#7C9AA2"/>
                            <path d="M12.43 8.12L15.78 11.47C16.07 11.76 16.07 12.24 15.78 12.53L12.43 15.88C12.14 16.17 11.66 16.17 11.37 15.88C11.08 15.59 11.08 15.11 11.37 14.82L13.44 12.75H2.75C2.34 12.75 2 12.41 2 12C2 11.59 2.34 11.25 2.75 11.25H13.44L11.37 9.18C11.22 9.03 11.15 8.84 11.15 8.65C11.15 8.46 11.22 8.27 11.37 8.12C11.66 7.82 12.13 7.82 12.43 8.12Z" fill="#7C9AA2"/>
                        </svg>
                    </i>
                    ログアウト
                </a>
            </header>

            <form class="main-content main-detail-check">
                <div class="main-heading mb-72">
                    <h1 class="title-heading">送り出し指示データ一覧</h1>
                    <button type="button" class="default-upload-button default-button m-0" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">ファイルアップロード</button>
                </div>

                <div class="main-body">
                    <!-- 4 blocks -->
                    <div class="wrapper-article-block wrapper-article-check">
                        <div class="row">
                            <div class="col-lg-3">
                                <article class="article-block first-upload-block">
                                    <h5 class="title-article-block line-2">作成日 </h5>
                                    <h2 class="date-article-block line-2">2021/12/28</h2>
                                </article>
                            </div>
                            <div class="col-lg-3">
                                <article class="article-block second-upload-block">
                                    <h5 class="title-article-block line-2">支店名</h5>
                                    <h2 class="date-article-block line-2">Sông Đà</h2>
                                </article>
                            </div>
                            <div class="col-lg-3">
                                <article class="article-block third-upload-block">
                                    <h5 class="title-article-block line-2">年月</h5>
                                    <h2 class="date-article-block line-2">2021/10</h2>
                                </article>
                            </div>

                            <!-- block A2001.1  -->
                            <div class="col-lg-3">
                                <article class="article-block checked-upload-block">
                                    <h5 class="title-article-block line-2">状態</h5>
                                    <h2 class="date-article-block line-2">完了</h2>
                                </article>
                            </div>
                            <!-- block A2001.1  -->

                            <!-- block A2001.2  -->
                            <!-- <div class="col-lg-3">
                                <article class="article-block checked-mint-block">
                                    <h5 class="title-article-block line-2">状態</h5>
                                    <h2 class="date-article-block line-2">エラー</h2>
                                </article>
                            </div> -->
                            <!-- block A2001.2  -->

                            <!-- block A2001.3  -->
                            <!-- <div class="col-lg-3">
                                <article class="article-block checked-blue-block">
                                    <h5 class="title-article-block line-2">状態</h5>
                                    <h2 class="date-article-block line-2">保留中</h2>
                                </article>

                            </div> -->
                            <!-- block A2001.3  -->


                            <!-- block A2001.4  -->
                            <!-- <div class="col-lg-3">
                                <article class="article-block checked-red-block">
                                    <h5 class="title-article-block line-2">状態</h5>
                                    <h2 class="date-article-block line-2">未チェック</h2>
                                </article>
                            </div> -->
                            <!-- block A2001.4  -->
                        </div>
                    </div>
                    <!-- 4 blocks -->
                </div>

                <div class="main-heading mt-30 mb-25">
                    <h4 class="mint-title-result">ファイルチェック履歴</h4>
                </div>

                <div class="wrapper-detail-check">
                    <!-- item detail-check block -->
                    <div class="detail-check-block">
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                <div class="btn-right-context btn-green-context">
                                    <img src="{{ asset('icons/verify.svg') }}" alt="verify">
                                    完了
                                </div>
                            </div>
                            <a href="#download" download class="right-check-block">
                                <div class="gray-text-check">ダウンロード</div>
                                <span class="btn-download-check">
                                    <img class="" src="{{ asset('icons/download.svg') }}" alt="download">
                                </span>
                            </a>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                            </div>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">エラー数: <span class="green-number-check">0<span></div>
                            </div>
                        </div>
                    </div>
                    <!-- item detail-check block -->

                    <!-- item detail-check block -->
                    <div class="detail-check-block">
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                <div class="btn-right-context btn-mint-context">
                                    <img src="{{ asset('icons/forbidden.svg') }}" alt="close">
                                    エラー
                                </div>
                            </div>
                            <a href="#download" download class="right-check-block">
                                <div class="gray-text-check">ダウンロード</div>
                                <span class="btn-download-check">
                                    <img class="" src="{{ asset('icons/download.svg') }}" alt="download">
                                </span>
                            </a>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                            </div>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                            </div>
                        </div>
                        <ul class="list-error-file">
                            <li class="item-error-file">
                                2行目、A列目：空白にすることはできません。
                            </li>
                            <li class="item-error-file">
                                2行目、B列目：40文字未満で入力してください。
                            </li>
                            <li class="item-error-file">
                                3行目4列目：数字のみを入力してください。
                            </li>
                        </ul>
                    </div>
                    <!-- item detail-check block -->

                    <!-- item detail-check block -->
                    <div class="detail-check-block">
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                <div class="btn-right-context btn-blue-context">
                                    <img src="{{ asset('icons/timer.svg') }}" alt="timer">
                                    保留中
                                </div>
                            </div>
                            <a href="#download" download class="right-check-block">
                                <div class="gray-text-check">ダウンロード</div>
                                <span class="btn-download-check">
                                    <img class="" src="{{ asset('icons/download.svg') }}" alt="download">
                                </span>
                            </a>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                            </div>
                        </div>
                        <div class="inner-check-block">
                            <div class="left-check-block">
                                <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                            </div>
                        </div>
                        <ul class="list-error-file">
                            <li class="item-error-file">
                                2行目、A列目：空白にすることはできません。
                            </li>
                            <li class="item-error-file">
                                2行目、B列目：40文
                            </li>
                            <li class="item-error-file">
                                3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。
                            </li>
                        </ul>
                        <div class="reason-check-block mt-20">
                            <div class="context-check-block">保留理由</div>
                            <div class="text-reason-check">
                                一部データ確認待ち
                            </div>
                        </div>
                    </div>
                    <!-- item detail-check block -->
                </div>

                <!-- button load more -->
                <div class="wrapper-button-loadmore">
                    <a href="#" class="button-loadmore">
                        <img class="mr-10" src="{{ asset('icons/add-square.svg') }}" alt="add-square">
                        もっと見る
                    </a>
                </div>
            </form>
        </section>
    </div>

    <!-- Modal upload file: id=ID &aria-labelledby=ID -->
    <div class="modal fade" id="modalUploadID3" tabindex="-1" role="dialog" aria-labelledby="modalUploadID3Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog multiple-modal-upload" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <form id="file-upload-form">
                            <div class="multiple-file-upload row">
                                <div class="item-file-upload col-6">
                                    <input id="file-upload-csv" type="file" name="fileUploadCSV" accept="file_extension" />

                                    <label for="file-upload-csv" id="file-drag-csv" class="label-file-drag">
                                        <img id="file-image-csv" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start-csv" class="file-start">
                                            <div id="notimage-csv" class="hidden notimage">Please select file CSV</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> CSVファイル </p>
                                        </div>
                                    </label>
                                    <div id="response-csv" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                        <div id="messages-csv"></div>
                                    </div>
                                     <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>

                                </div>

                                <div class="item-file-upload col-6">
                                    <input id="file-upload-excel" type="file" name="fileUploadExcel" accept="file_extension" />

                                    <label for="file-upload-excel" id="file-drag-excel" class="label-file-drag">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start-excel" class="file-start">
                                            <div id="notimage-excel" class="hidden notimage">Please select file excel</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> Excel ファイル </p>
                                        </div>
                                    </label>
                                    <div id="response-excel" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                        <div id="messages-excel"></div>
                                    </div>
                                    <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>

                                </div>
                            </div>
                            <!-- <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p> -->

                            <button type="button" class="home-modal-button default-button" disabled>チェック</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload file: id=ID &aria-labelledby=ID -->

    <!-- scrip -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/uploadFile.js') }}"></script> -->
    <!-- multiple -->
    <script type="text/javascript" src="{{ asset('js/uploadFileCSV.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadFileExcel.js') }}"></script>
    <!-- multiple -->
    <script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
    <script>
        const container = document.getElementById("wrapper-toggle-btn")
        const toggleInput = document.getElementById("toggle-input")
        const toggleBtn = document.getElementById("toggle-btn")
        const modalQuestion = document.getElementById("modalQuestion")

        toggleInput.addEventListener("click", () => {
            if(toggleInput.checked) {
                toggleBtn.style.marginLeft = "16px"
                toggleBtn.style.backgroundColor = "#0F94B5"
                container.style.backgroundColor = "#B5E5F0"
                modalQuestion.addClass("show")
            }
            else {
                toggleBtn.style.marginLeft = "-2px"
                toggleBtn.style.backgroundColor = "#F5F7FB"
                container.style.backgroundColor = "#7c9aa1"
                modalQuestion.removeClass("show")
            }
        })
    </script>
    <!-- scrip -->
</body>
</html>
