<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>B2000 - Home</title>
  <!-- css -->
  <link rel="stylesheet" href="{{asset('css/main.css?v=6')}}">
  <link rel="stylesheet" href="{{asset('css/template.css?v=6')}}">
  <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
  <!-- css -->
</head>
<body>
    <style>
        body {
            background-image: url("/images/bg-gray-admin.png")!important;
        }
    </style>
    <div class="wrapper d-flex flex-row">
        <!-- sidebar -->
        <section class="left-sidebar admin-sidebar">
            <h2 class="title-sidebar">
                ロゴ
            </h2>
            <div class="nav flex-column nav-pills">
                <a href="#" class="nav-link nav-sidebar active">
                    <i class="icon-sidebar">
                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.4" d="M14.69 0.5H6.31C2.67 0.5 0.5 2.67 0.5 6.31V14.69C0.5 18.33 2.67 20.5 6.31 20.5H14.69C18.33 20.5 20.5 18.33 20.5 14.69V6.31C20.5 2.67 18.33 0.5 14.69 0.5Z" fill="#BFBFBF"/>
                        <path d="M16.8105 7.37012C16.8105 7.78012 16.4805 8.12012 16.0605 8.12012H10.8105C10.4005 8.12012 10.0605 7.78012 10.0605 7.37012C10.0605 6.96012 10.4005 6.62012 10.8105 6.62012H16.0605C16.4805 6.62012 16.8105 6.96012 16.8105 7.37012Z" fill="#BFBFBF"/>
                        <path d="M8.47055 6.40006L6.22055 8.65006C6.07055 8.80006 5.88055 8.87006 5.69055 8.87006C5.50055 8.87006 5.30055 8.80006 5.16055 8.65006L4.41055 7.90006C4.11055 7.61006 4.11055 7.13006 4.41055 6.84006C4.70055 6.55006 5.17055 6.55006 5.47055 6.84006L5.69055 7.06006L7.41055 5.34006C7.70055 5.05006 8.17055 5.05006 8.47055 5.34006C8.76055 5.63006 8.76055 6.11006 8.47055 6.40006Z" fill="#BFBFBF"/>
                        <path d="M16.8105 14.3701C16.8105 14.7801 16.4805 15.1201 16.0605 15.1201H10.8105C10.4005 15.1201 10.0605 14.7801 10.0605 14.3701C10.0605 13.9601 10.4005 13.6201 10.8105 13.6201H16.0605C16.4805 13.6201 16.8105 13.9601 16.8105 14.3701Z" fill="#BFBFBF"/>
                        <path d="M8.47055 13.4001L6.22055 15.6501C6.07055 15.8001 5.88055 15.8701 5.69055 15.8701C5.50055 15.8701 5.30055 15.8001 5.16055 15.6501L4.41055 14.9001C4.11055 14.6101 4.11055 14.1301 4.41055 13.8401C4.70055 13.5501 5.17055 13.5501 5.47055 13.8401L5.69055 14.0601L7.41055 12.3401C7.70055 12.0501 8.17055 12.0501 8.47055 12.3401C8.76055 12.6301 8.76055 13.1101 8.47055 13.4001Z" fill="#BFBFBF"/>
                    </svg>
                    </i>
                    請求チェック状況管理
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.8" d="M13.3992 2.52009L13.3692 2.59009L10.4692 9.32009H7.61922C6.93922 9.32009 6.29922 9.45009 5.69922 9.71009L7.44922 5.53009L7.48922 5.44009L7.54922 5.28009C7.57922 5.21009 7.59922 5.15009 7.62922 5.10009C8.93922 2.07009 10.4192 1.38009 13.3992 2.52009Z" fill="#BFBFBF"/>
                            <path d="M18.7907 9.52002C18.3407 9.39002 17.8707 9.32002 17.3807 9.32002H10.4707L13.3707 2.59002L13.4007 2.52002C13.5407 2.57002 13.6907 2.64002 13.8407 2.69002L16.0507 3.62002C17.2807 4.13002 18.1407 4.66002 18.6707 5.30002C18.7607 5.42002 18.8407 5.53002 18.9207 5.66002C19.0107 5.80002 19.0807 5.94002 19.1207 6.09002C19.1607 6.18002 19.1907 6.26002 19.2107 6.35002C19.4707 7.20002 19.3107 8.23002 18.7907 9.52002Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M22.2602 14.2001V16.1501C22.2602 16.3501 22.2502 16.5501 22.2402 16.7401C22.0502 20.2401 20.1002 22.0001 16.4002 22.0001H8.60023C8.35023 22.0001 8.12023 21.9801 7.89023 21.9501C4.71023 21.7401 3.01023 20.0401 2.79023 16.8601C2.76023 16.6201 2.74023 16.3901 2.74023 16.1501V14.2001C2.74023 12.1901 3.96023 10.4601 5.70023 9.71007C6.30023 9.45007 6.94023 9.32007 7.62023 9.32007H17.3802C17.8702 9.32007 18.3402 9.39007 18.7902 9.52007C20.7902 10.1301 22.2602 11.9901 22.2602 14.2001Z" fill="#BFBFBF"/>
                            <path opacity="0.6" d="M7.45023 5.53003L5.70023 9.71003C3.96023 10.46 2.74023 12.19 2.74023 14.2V11.27C2.74023 8.43003 4.76023 6.06003 7.45023 5.53003Z" fill="#BFBFBF"/>
                            <path opacity="0.6" d="M22.2591 11.2701V14.2001C22.2591 11.9901 20.7891 10.1301 18.7891 9.52009C19.3091 8.23009 19.4691 7.20009 19.2091 6.35009C19.1891 6.26009 19.1591 6.18009 19.1191 6.09009C20.9891 7.06009 22.2591 9.03009 22.2591 11.2701Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    請求未確定
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.1702 6.9499C21.5302 4.7799 19.7202 2.9699 17.5502 2.3299C15.9002 1.8499 14.7602 1.8899 13.9702 2.4799C13.0202 3.1899 12.9102 4.4699 12.9102 5.3799V7.8699C12.9102 10.3299 14.0302 11.5799 16.2302 11.5799H19.1002C20.0002 11.5799 21.2902 11.4699 22.0002 10.5199C22.6102 9.7399 22.6602 8.5999 22.1702 6.9499Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M19.4094 13.3599C19.1494 13.0599 18.7694 12.8899 18.3794 12.8899H14.7994C13.0394 12.8899 11.6094 11.4599 11.6094 9.69991V6.11991C11.6094 5.72991 11.4394 5.34991 11.1394 5.08991C10.8494 4.82991 10.4494 4.70991 10.0694 4.75991C7.71941 5.05991 5.55941 6.34991 4.14941 8.28991C2.72941 10.2399 2.20941 12.6199 2.65941 14.9999C3.30941 18.4399 6.05941 21.1899 9.50941 21.8399C10.0594 21.9499 10.6094 21.9999 11.1594 21.9999C12.9694 21.9999 14.7194 21.4399 16.2094 20.3499C18.1494 18.9399 19.4394 16.7799 19.7394 14.4299C19.7894 14.0399 19.6694 13.6499 19.4094 13.3599Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    エラー統計
                </a>

                <div class="nav-sidebar-dropdown">
                    <div class="nav-link nav-sidebar nav-sidebar-heading">
                        <i class="icon-sidebar">
                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.5 8C21.1569 8 22.5 6.65685 22.5 5C22.5 3.34315 21.1569 2 19.5 2C17.8431 2 16.5 3.34315 16.5 5C16.5 6.65685 17.8431 8 19.5 8Z" fill="#BFBFBF"/>
                                <path opacity="0.4" d="M19.5 9.5C17.02 9.5 15 7.48 15 5C15 4.28 15.19 3.61 15.49 3H8.02C4.57 3 2.5 5.06 2.5 8.52V16.47C2.5 19.94 4.57 22 8.02 22H15.97C19.43 22 21.49 19.94 21.49 16.48V9.01C20.89 9.31 20.22 9.5 19.5 9.5Z" fill="#BFBFBF"/>
                            </svg>
                        </i>
                        アラート管理
                        <i class="icon-polygon ml-auto">
                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.46967 17.5303C9.17678 17.2374 9.17678 16.7626 9.46967 16.4697L13.9393 12L9.46967 7.53033C9.17678 7.23744 9.17678 6.76256 9.46967 6.46967C9.76256 6.17678 10.2374 6.17678 10.5303 6.46967L15.5303 11.4697C15.671 11.6103 15.75 11.8011 15.75 12C15.75 12.1989 15.671 12.3897 15.5303 12.5303L10.5303 17.5303C10.2374 17.8232 9.76256 17.8232 9.46967 17.5303Z" fill="#BFBFBF"/>
                            </svg>
                        </i>
                    </div>
                    <div class="nav-sidebar-body">
                        <a href="" class="nav-link-dropdown">設定</a>

                        <a href="" class="nav-link-dropdown active">アラート管理</a>

                        <a href="" class="nav-link-dropdown">グループ一覧管理</a>
                    </div>
                </div>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.1 4.15002V6H9.62C8.00999 6 7.20001 6.80998 7.20001 8.41998V18H4.65002C3.22002 18 2.5 17.28 2.5 15.85V4.15002C2.5 2.72002 3.22002 2 4.65002 2H8.95001C10.38 2 11.1 2.72002 11.1 4.15002Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M17.8702 8.41998V19.58C17.8702 21.19 17.0702 22 15.4602 22H9.62018C8.01018 22 7.2002 21.19 7.2002 19.58V8.41998C7.2002 6.80998 8.01018 6 9.62018 6H15.4602C17.0702 6 17.8702 6.80998 17.8702 8.41998Z" fill="#BFBFBF"/>
                            <path d="M22.5004 4.15002V15.85C22.5004 17.28 21.7803 18 20.3503 18H17.8704V8.41998C17.8704 6.80998 17.0704 6 15.4604 6H13.9004V4.15002C13.9004 2.72002 14.6204 2 16.0504 2H20.3503C21.7803 2 22.5004 2.72002 22.5004 4.15002Z" fill="#BFBFBF"/>
                            <path d="M14.5 11.75H10.5C10.09 11.75 9.75 11.41 9.75 11C9.75 10.59 10.09 10.25 10.5 10.25H14.5C14.91 10.25 15.25 10.59 15.25 11C15.25 11.41 14.91 11.75 14.5 11.75Z" fill="#BFBFBF"/>
                            <path d="M14.5 14.75H10.5C10.09 14.75 9.75 14.41 9.75 14C9.75 13.59 10.09 13.25 10.5 13.25H14.5C14.91 13.25 15.25 13.59 15.25 14C15.25 14.41 14.91 14.75 14.5 14.75Z" fill="#BFBFBF"/>
                            <path d="M13.25 19V22H11.75V19C11.75 18.59 12.09 18.25 12.5 18.25C12.91 18.25 13.25 18.59 13.25 19Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    支店
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M11.4608 2.06008L5.96078 4.12008C4.91078 4.52008 4.05078 5.76008 4.05078 6.89008V14.9901C4.05078 15.8001 4.58078 16.8701 5.23078 17.3501L10.7308 21.4601C11.7008 22.1901 13.2908 22.1901 14.2608 21.4601L19.7608 17.3501C20.4108 16.8601 20.9408 15.8001 20.9408 14.9901V6.89008C20.9408 5.77008 20.0808 4.52008 19.0308 4.13008L13.5308 2.07008C12.9708 1.85008 12.0308 1.85008 11.4608 2.06008Z" fill="#BFBFBF"/>
                            <path d="M11.1602 14.2301C10.9702 14.2301 10.7802 14.1601 10.6302 14.0101L9.02023 12.4001C8.73023 12.1101 8.73023 11.6301 9.02023 11.3401C9.31023 11.0501 9.79023 11.0501 10.0802 11.3401L11.1602 12.4201L14.9302 8.65012C15.2202 8.36012 15.7002 8.36012 15.9902 8.65012C16.2802 8.94012 16.2802 9.42012 15.9902 9.71012L11.6902 14.0101C11.5402 14.1601 11.3502 14.2301 11.1602 14.2301Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    役割と権限の設定
                </a>
            </div>
        </section>
        <!-- sidebar -->

        <section class="right-main">
            <!-- header -->
            <header class="main-header">
                <div class="header-account">
                    <span class="name-account">AML.Co  JP</span>
                    <span class="email-account">aml@gmail.com</span>
                </div>
                <a href="#logout" class="header-logout">
                    <i class="icon-logout">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M9 7.2V16.79C9 20 11 22 14.2 22H16.79C19.99 22 21.99 20 21.99 16.8V7.2C22 4 20 2 16.8 2H14.2C11 2 9 4 9 7.2Z" fill="#7C9AA2"/>
                            <path d="M12.43 8.12L15.78 11.47C16.07 11.76 16.07 12.24 15.78 12.53L12.43 15.88C12.14 16.17 11.66 16.17 11.37 15.88C11.08 15.59 11.08 15.11 11.37 14.82L13.44 12.75H2.75C2.34 12.75 2 12.41 2 12C2 11.59 2.34 11.25 2.75 11.25H13.44L11.37 9.18C11.22 9.03 11.15 8.84 11.15 8.65C11.15 8.46 11.22 8.27 11.37 8.12C11.66 7.82 12.13 7.82 12.43 8.12Z" fill="#7C9AA2"/>
                        </svg>
                    </i>
                    ログアウト
                </a>
            </header>
            <!-- header -->

            <form action="" class="main-content main-admin">
                <div class="main-heading mb-30">
                    <h1 class="title-heading">請求チェック状況管理</h1>
                </div>

                <div class="main-body">
                    <!-- section-deadline-setting -->
                    <div class="section-wrapper section-deadline-setting">
                        <h5 class="mint-title-heading mb-20">締め切り設定</h5>
                        <div class="filter-heading">
                            <div id="datepicker" class="datepicker-heading ml-0">
                                <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                <span class="icon-datepicker">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                        <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                        <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                        <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                        <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                        <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                        <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                        <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                    </svg>
                                </span>
                            </div>
                            <!-- Button Modal B2000.2 -->
                            <button type="button" class="button-add-branch default-button" data-toggle="modal" data-target="#modalApplication" data-backdrop="static" data-keyboard="false">適用</button>
                        </div>
                    </div>
                    <!-- section-deadline-setting -->

                    <!-- section-check-status -->
                    <div class="section-wrapper section-check-status">
                        <div class="main-heading mb-30">
                            <h5 class="mint-title-heading">チェック状況</h5>
                            <div class="datepicker-heading ml-0 mr-0 w-190">
                                <input class="input-datepicker input-date" type="text" name="" placeholder="年/月/日" value="2021年11月">
                            </div>
                        </div>
                        <div class="wrapper-outer-status row align-items-center justify-content-center mb-50">
                            <div class="false-For-Bottom-Text wrapper-chart col-xl-3 col-lg-4 col-md-4">
                                <div class="mypiechart">
                                    <canvas id="myCanvas1" width="194" height="198"></canvas>
                                    <div class="totalValuePie">
                                        <span>合計</span>
                                        <span>500</span>
                                    </div>
                                </div>
                            </div>
                            <div class="list-index-chart col-xl-7 col-lg-8 col-md-8">
                                <div class="row">
                                    <div class="col-6 mt-15 mb-15">
                                        <div class="item-index-chart">
                                            <img src="{{ asset('icons/chart-index-1.svg') }}" alt="icon">
                                            <div class="right-index-chart">
                                                <div class="text-index-chart text-index-chart1">
                                                    未チェックの事業所数
                                                </div>
                                                <div class="number-index-chart number-index-chart1">100</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 mt-15 mb-15">
                                        <div class="item-index-chart">
                                            <img src="{{ asset('icons/chart-index-2.svg') }}" alt="icon">
                                            <div class="right-index-chart">
                                                <div class="text-index-chart text-index-chart2">
                                                    保留中事業所の数
                                                </div>
                                                <div class="number-index-chart number-index-chart2">100</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 mt-15 mb-15">
                                        <div class="item-index-chart">
                                            <img src="{{ asset('icons/chart-index-3.svg') }}" alt="icon">
                                            <div class="right-index-chart">
                                                <div class="text-index-chart text-index-chart3">
                                                    チェック中の事業所数
                                                </div>
                                                <div class="number-index-chart number-index-chart3">200</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 mt-15 mb-15">
                                        <div class="item-index-chart">
                                            <img src="{{ asset('icons/chart-index-4.svg') }}" alt="icon">
                                            <div class="right-index-chart">
                                                <div class="text-index-chart text-index-chart4">
                                                    完了の事業所数
                                                </div>
                                                <div class="number-index-chart number-index-chart4">100</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- section-check-status -->

                    <!-- section-table-field -->
                    <div class="section-table-field">
                        <form>
                            <div class="heading-table-field">
                                <div class="filter-heading mb-30 pl-20 pr-20 flex-wrap">
                                    <div class="wrapper-search w-206">
                                        <input class="input-search" type="text" placeholder="事業所名" name="search">
                                        <button class="button-search" type="submit">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path opacity="0.4" d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z" fill="#7C9AA1"/>
                                                <path d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z" fill="#7C9AA1"/>
                                            </svg>
                                        </button>
                                    </div>

                                    <div class="selection-heading w-180">
                                        <select class="form-control dropdown-heading">
                                            <option value="1">支店名</option>
                                            <option value="2">Option 1</option>
                                            <option value="3">Option 2</option>
                                        </select>
                                    </div>

                                    <div class="selection-heading w-180">
                                        <select class="form-control dropdown-heading">
                                            <option value="1">状況</option>
                                            <option value="2">Option 1</option>
                                            <option value="3">Option 2</option>
                                        </select>
                                    </div>

                                    <div id="datepicker" class="datepicker-heading">
                                        <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                        <span class="icon-datepicker">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                            </svg>
                                        </span>
                                    </div>

                                    <button class="button-refresh">
                                        <img src="{{ asset('icons/refresh.svg') }}" alt="refresh">
                                        <span class="text-button-refresh">リセット</span>
                                    </button>

                                    <button type="button" class="button-add-branch default-button">CSV出力</button>
                                </div>


                                <div class="annotate-heading d-flex align-items-center mb-20 pl-20 pr-20">
                                    <div class="item-annotate">
                                        <div class="color-annotate color-annotate1"></div>
                                        <div class="text-annotate">完了</div>
                                    </div>
                                    <div class="item-annotate">
                                        <div class="color-annotate color-annotate2"></div>
                                        <div class="text-annotate">エラー</div>
                                    </div>
                                    <div class="item-annotate">
                                        <div class="color-annotate color-annotate3"></div>
                                        <div class="text-annotate">未チェック</div>
                                    </div>
                                    <div class="item-annotate">
                                        <div class="color-annotate color-annotate4"></div>
                                        <div class="text-annotate">保留中</div>
                                    </div>
                                </div>

                                <table class="table table-striped table-admin table-home mt-15 mb-15">
                                    <thead>
                                        <tr>
                                            <th scope="col">順番</th>
                                            <th scope="col">事業所名</th>
                                            <th scope="col">締め切り</th>
                                            <th scope="col">状況</th>
                                            <th scope="col">やり直し回数</th>
                                            <th scope="col"></th>
                                            <th scope="col">
                                                <button type="button" id="wrapper-toggle-btn1" class="wrapper-toggle-btn checked">
                                                    <input type="checkbox" id="toggle-input1" class="toggle-input" checked>
                                                    <div id="toggle-btn1" class="toggle-btn"></div>
                                                </button></th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>AML</td>
                                            <td>
                                                <div id="datepicker" class="datepicker-heading w-165 ml-0">
                                                    <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                                    <span class="icon-datepicker">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData1" class="context-popover bg-transparent p-0"
                                                    data-content="
                                                        <div class='d-flex align-items-center flex-wrap justify-space-between'>
                                                            <span>状態</span>
                                                            <img class='status-button ml-auto' src='{{ asset('icons/button-success.svg') }}' alt=''>
                                                        </div>
                                                    "
                                                    rel="popover"
                                                    data-placement="right"
                                                    data-original-title="総合事業請求止めチェック機能"
                                                    data-trigger="manual"
                                                >
                                                    <div class="selection-heading w-165 ml-0">
                                                        <select class="form-control dropdown-heading">
                                                            <option value="1">未チェック</option>
                                                        </select>
                                                    </div>
                                                </button>

                                            </td>
                                            <td>
                                                10
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="color-annotate color-annotate1">1</div>
                                                    <div class="color-annotate color-annotate4">2</div>
                                                    <div class="color-annotate color-annotate2">3</div>
                                                    <div class="color-annotate color-annotate1">4</div>
                                                    <div class="color-annotate color-annotate1">5</div>
                                                    <div class="color-annotate color-annotate2">6</div>
                                                    <div class="color-annotate color-annotate4">7</div>
                                                    <div class="color-annotate color-annotate1">8</div>
                                                    <div class="color-annotate color-annotate4">9</div>
                                                    <div class="color-annotate color-annotate4">10</div>
                                                    <div class="color-annotate color-annotate4">11</div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="wrapper-toggle-btn1" class="wrapper-toggle-btn checked">
                                                    <input type="checkbox" id="toggle-input1" class="toggle-input" checked>
                                                    <div id="toggle-btn1" class="toggle-btn"></div>
                                                </button>
                                            </td>
                                            <td>
                                                <!-- Button Modal B2001.2 -->
                                                <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalViewDetailID1" data-backdrop="static" data-keyboard="false">
                                                    <span class="text-view-detail">詳細</span>
                                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>AML</td>
                                            <td>
                                                <div id="datepicker" class="datepicker-heading w-165 ml-0">
                                                    <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                                    <span class="icon-datepicker">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData2" class="context-popover bg-transparent p-0"
                                                    data-content="
                                                        <div class='d-flex align-items-center flex-wrap justify-space-between'>
                                                            <span>状態</span>
                                                        </div>
                                                    "
                                                    rel="popover"
                                                    data-placement="right"
                                                    data-original-title="総合事業請求止めチェック機能"
                                                    data-trigger="manual"
                                                >
                                                    <div class="selection-heading w-165 ml-0">
                                                        <select class="form-control dropdown-heading">
                                                            <option value="1">未チェック</option>
                                                        </select>
                                                    </div>
                                                </button>

                                            </td>
                                            <td>
                                                10
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="color-annotate color-annotate1">1</div>
                                                    <div class="color-annotate color-annotate4">2</div>
                                                    <div class="color-annotate color-annotate2">3</div>
                                                    <div class="color-annotate color-annotate1">4</div>
                                                    <div class="color-annotate color-annotate1">5</div>
                                                    <div class="color-annotate color-annotate2">6</div>
                                                    <div class="color-annotate color-annotate4">7</div>
                                                    <div class="color-annotate color-annotate1">8</div>
                                                    <div class="color-annotate color-annotate4">9</div>
                                                    <div class="color-annotate color-annotate4">10</div>
                                                    <div class="color-annotate color-annotate4">11</div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="wrapper-toggle-btn1" class="wrapper-toggle-btn checked">
                                                    <input type="checkbox" id="toggle-input1" class="toggle-input" checked>
                                                    <div id="toggle-btn1" class="toggle-btn"></div>
                                                </button>
                                            </td>
                                            <td>
                                                <!-- Button Modal B2001.2 -->
                                                <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalViewDetailID1" data-backdrop="static" data-keyboard="false">
                                                    <span class="text-view-detail">詳細</span>
                                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>AML</td>
                                            <td>
                                                <div id="datepicker" class="datepicker-heading w-165 ml-0">
                                                    <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                                    <span class="icon-datepicker">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData3" class="context-popover bg-transparent p-0"
                                                    data-content="
                                                        <div class='d-flex align-items-center flex-wrap justify-space-between'>
                                                            <span>状態</span>
                                                            <img class='status-button ml-auto' src='{{ asset('icons/button-success.svg') }}' alt=''>
                                                        </div>
                                                    "
                                                    rel="popover"
                                                    data-placement="right"
                                                    data-original-title="総合事業請求止めチェック機能"
                                                    data-trigger="manual"
                                                >
                                                    <div class="selection-heading w-165 ml-0">
                                                        <select class="form-control dropdown-heading">
                                                            <option value="1">未チェック</option>
                                                        </select>
                                                    </div>
                                                </button>

                                            </td>
                                            <td>
                                                10
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="color-annotate color-annotate1">1</div>
                                                    <div class="color-annotate color-annotate4">2</div>
                                                    <div class="color-annotate color-annotate2">3</div>
                                                    <div class="color-annotate color-annotate1">4</div>
                                                    <div class="color-annotate color-annotate1">5</div>
                                                    <div class="color-annotate color-annotate2">6</div>
                                                    <div class="color-annotate color-annotate4">7</div>
                                                    <div class="color-annotate color-annotate1">8</div>
                                                    <div class="color-annotate color-annotate4">9</div>
                                                    <div class="color-annotate color-annotate4">10</div>
                                                    <div class="color-annotate color-annotate4">11</div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="wrapper-toggle-btn1" class="wrapper-toggle-btn checked">
                                                    <input type="checkbox" id="toggle-input1" class="toggle-input" checked>
                                                    <div id="toggle-btn1" class="toggle-btn"></div>
                                                </button>
                                            </td>
                                            <td>
                                                <!-- Button Modal B2001.2 -->
                                                <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalViewDetailID1" data-backdrop="static" data-keyboard="false">
                                                    <span class="text-view-detail">詳細</span>
                                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>AML</td>
                                            <td>
                                                <div id="datepicker" class="datepicker-heading w-165 ml-0">
                                                    <input class="input-datepicker" type="text" name="" placeholder="年/月/日">
                                                    <span class="icon-datepicker">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData4" class="context-popover bg-transparent p-0"
                                                    data-content="
                                                        <div class='d-flex align-items-center flex-wrap justify-space-between'>
                                                            <span>状態</span>
                                                            <img class='status-button ml-auto' src='{{ asset('icons/button-success.svg') }}' alt=''>
                                                        </div>
                                                    "
                                                    rel="popover"
                                                    data-placement="right"
                                                    data-original-title="総合事業請求止めチェック機能"
                                                    data-trigger="manual"
                                                >
                                                    <div class="selection-heading w-165 ml-0">
                                                        <select class="form-control dropdown-heading">
                                                            <option value="1">未チェック</option>
                                                        </select>
                                                    </div>
                                                </button>

                                            </td>
                                            <td>
                                                10
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="color-annotate color-annotate1">1</div>
                                                    <div class="color-annotate color-annotate4">2</div>
                                                    <div class="color-annotate color-annotate2">3</div>
                                                    <div class="color-annotate color-annotate1">4</div>
                                                    <div class="color-annotate color-annotate1">5</div>
                                                    <div class="color-annotate color-annotate2">6</div>
                                                    <div class="color-annotate color-annotate4">7</div>
                                                    <div class="color-annotate color-annotate1">8</div>
                                                    <div class="color-annotate color-annotate4">9</div>
                                                    <div class="color-annotate color-annotate4">10</div>
                                                    <div class="color-annotate color-annotate4">11</div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" id="wrapper-toggle-btn1" class="wrapper-toggle-btn checked">
                                                    <input type="checkbox" id="toggle-input1" class="toggle-input" checked>
                                                    <div id="toggle-btn1" class="toggle-btn"></div>
                                                </button>
                                            </td>
                                            <td>
                                                <!-- Button Modal B2001.2 -->
                                                <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalViewDetailID1" data-backdrop="static" data-keyboard="false">
                                                    <span class="text-view-detail">詳細</span>
                                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <!-- add pagination if more 10 rows -->
                                <nav class="wrapper-pagination">
                                    <ul class="pagination">
                                        <!-- add class="disabled" if cannot previous -->
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#">
                                                <img src="{{ asset('icons/arrow-left.svg') }}" alt="previous">
                                            </a>
                                        </li>
                                        <!-- add class="active" if current page-link -->
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                                        <!-- add class="disabled" if cannot next -->
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <img src="{{ asset('icons/arrow-right.svg') }}" alt="next">
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </form>
                    </div>
                    <!-- section-table-field -->
                </div>
            </form>
        </section>
    </div>

    <!-- Modal B2000.2 Home -->
    <div class="modal fade" id="modalApplication" tabindex="-1" role="dialog" aria-labelledby="modalApplicationTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">締め切り設定</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text"> 以下の締め切りが設定でしょうか。</p>
                    <p class="modal-text modal-text-bold">2021/30/12</p>
                    <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">同意</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B2000.2 Home -->

    <!-- Modal B2000.3 Home -->
    <div class="modal fade" id="modalChange" tabindex="-1" role="dialog" aria-labelledby="modalChangeTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">状況変更</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text">  この事業所の状況が以下のように変更でしょうか。</p>
                    <p class="modal-text modal-text-bold">完了</p>
                    <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">同意</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B2000.3 Home -->

    <!-- Modal B2001.2  -->
    <div class="modal fade" id="modalViewDetailID1" tabindex="-1" role="dialog" aria-labelledby="modalViewDetailID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered view-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェック詳細</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body text-left pt-50 pb-15 max-height">
                    <div class="pl-100 pr-100">
                        <form>
                            <h2 class="mint-title mb-30">TH障害データチェック</h2>

                            <div class="main-body">
                                <!-- 4 blocks -->
                                <div class="wrapper-article-block wrapper-article-check">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <article class="article-block first-upload-block">
                                                <h5 class="title-article-block line-2">作成日 </h5>
                                                <h2 class="date-article-block line-2">2021/12/28</h2>
                                            </article>
                                        </div>
                                        <div class="col-lg-3">
                                            <article class="article-block second-upload-block">
                                                <h5 class="title-article-block line-2">支店名</h5>
                                                <h2 class="date-article-block line-2">Sông Đà</h2>
                                            </article>
                                        </div>
                                        <div class="col-lg-3">
                                            <article class="article-block third-upload-block">
                                                <h5 class="title-article-block line-2">年月</h5>
                                                <h2 class="date-article-block line-2">2021/10</h2>
                                            </article>
                                        </div>

                                        <!-- block A2001.1  -->
                                        <div class="col-lg-3">
                                            <article class="article-block checked-upload-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">完了</h2>
                                            </article>
                                        </div>
                                        <!-- block A2001.1  -->

                                        <!-- block A2001.2  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-mint-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">エラー</h2>
                                            </article>
                                        </div> -->
                                        <!-- block A2001.2  -->

                                        <!-- block A2001.3  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-blue-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">保留中</h2>
                                            </article>

                                        </div> -->
                                        <!-- block A2001.3  -->


                                        <!-- block A2001.4  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-red-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">未チェック</h2>
                                            </article>
                                        </div> -->
                                        <!-- block A2001.4  -->
                                    </div>
                                </div>
                                <!-- 4 blocks -->
                            </div>

                            <div class="main-heading mt-30 mb-25">
                                <h4 class="blue-title-result">ファイルチェック履歴</h4>
                            </div>

                            <div class="wrapper-detail-check">
                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-green-context">
                                                <img src="{{ asset('icons/verify.svg') }}" alt="verify">
                                                完了
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="green-number-check">0<span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item detail-check block -->

                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-mint-context">
                                                <img src="{{ asset('icons/forbidden.svg') }}" alt="close">
                                                エラー
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                                        </div>
                                    </div>
                                    <ul class="list-error-file">
                                        <li class="item-error-file">
                                            2行目、A列目：空白にすることはできません。
                                        </li>
                                        <li class="item-error-file">
                                            2行目、B列目：40文字未満で入力してください。
                                        </li>
                                        <li class="item-error-file">
                                            3行目4列目：数字のみを入力してください。
                                        </li>
                                    </ul>
                                </div>
                                <!-- item detail-check block -->

                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-blue-context">
                                                <img src="{{ asset('icons/timer.svg') }}" alt="timer">
                                                保留中
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                                        </div>
                                    </div>
                                    <ul class="list-error-file">
                                        <li class="item-error-file">
                                            2行目、A列目：空白にすることはできません。
                                        </li>
                                        <li class="item-error-file">
                                            2行目、B列目：40文
                                        </li>
                                        <li class="item-error-file">
                                            3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。
                                        </li>
                                    </ul>
                                    <div class="reason-check-block mt-20">
                                        <div class="context-check-block">保留理由</div>
                                        <div class="text-reason-check">
                                            一部データ確認待ち
                                        </div>
                                    </div>
                                </div>
                                <!-- item detail-check block -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B2001.2  -->

    <!-- scrip -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/toggle-menu.js') }}"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/style-chart.js') }}"></script>
</body>
</html>
