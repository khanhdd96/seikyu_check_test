<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('icons/favicon.ico')}}">

    <!-- css -->
    <link rel="stylesheet" href="{{asset('css/main.css?v=6')}}">
    <link rel="stylesheet" href="{{asset('css/template.css?v=6')}}">
    <!-- css -->
</head>
<body>
    <div class="wrapper d-flex flex-row">
        <!-- sidebar -->
        <section class="left-sidebar">
            <a href="/" class="title-sidebar d-inline-block">
                <img src="{{ asset('icons/logo-user.svg') }}" alt="logo-user">
            </a>
            <div class="nav flex-column nav-pills">
                <!-- menu active -->
                <a href="#" class="nav-link nav-sidebar active">
                    <i class="icon-sidebar">
                        <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.0585 6.29175C17.5252 4.48342 16.0168 2.97508 14.2085 2.44175C12.8335 2.04175 11.8835 2.07508 11.2252 2.56675C10.4335 3.15842 10.3418 4.22508 10.3418 4.98342V7.05842C10.3418 9.10842 11.2752 10.1501 13.1085 10.1501H15.5002C16.2502 10.1501 17.3252 10.0584 17.9168 9.26675C18.4252 8.61675 18.4668 7.66675 18.0585 6.29175Z" fill="#7C9AA1"/>
                            <path opacity="0.4" d="M15.7585 11.6335C15.5418 11.3835 15.2252 11.2418 14.9002 11.2418H11.9168C10.4502 11.2418 9.25849 10.0501 9.25849 8.58346V5.60013C9.25849 5.27512 9.11683 4.95846 8.86683 4.74179C8.62516 4.52513 8.29183 4.42513 7.97516 4.46679C6.01683 4.71679 4.21683 5.79179 3.04183 7.40846C1.85849 9.03346 1.42516 11.0168 1.80016 13.0001C2.34183 15.8668 4.63349 18.1585 7.50849 18.7001C7.96683 18.7918 8.42516 18.8335 8.88349 18.8335C10.3918 18.8335 11.8502 18.3668 13.0918 17.4585C14.7085 16.2835 15.7835 14.4835 16.0335 12.5251C16.0752 12.2001 15.9752 11.8751 15.7585 11.6335Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    ダッシュボード
                </a>
                <!-- menu active -->

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    請求前確認リスト
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    請求締めチェックツール
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    送り出し指示データ一覧
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    総合事業請求止め <br />
                    チェック機能
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    障害チェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    トランデータチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    おまかせチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    Mファイルチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    Sファイルチェック
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                            <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                            <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                            <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                            <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                        </svg>
                    </i>
                    障害データチェック
                </a>
            </div>
        </section>
        <!-- sidebar -->

        <section class="right-main">
            <header class="main-header">
                <div class="header-account">
                    <span class="name-account">AML.Co  JP</span>
                    <span class="email-account">aml@gmail.com</span>
                </div>
                <a href="#logout" class="header-logout">
                    <i class="icon-logout">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M9 7.2V16.79C9 20 11 22 14.2 22H16.79C19.99 22 21.99 20 21.99 16.8V7.2C22 4 20 2 16.8 2H14.2C11 2 9 4 9 7.2Z" fill="#7C9AA2"/>
                            <path d="M12.43 8.12L15.78 11.47C16.07 11.76 16.07 12.24 15.78 12.53L12.43 15.88C12.14 16.17 11.66 16.17 11.37 15.88C11.08 15.59 11.08 15.11 11.37 14.82L13.44 12.75H2.75C2.34 12.75 2 12.41 2 12C2 11.59 2.34 11.25 2.75 11.25H13.44L11.37 9.18C11.22 9.03 11.15 8.84 11.15 8.65C11.15 8.46 11.22 8.27 11.37 8.12C11.66 7.82 12.13 7.82 12.43 8.12Z" fill="#7C9AA2"/>
                        </svg>
                    </i>
                    ログアウト
                </a>
            </header>

            <form action="" class="main-content">
                <div class="main-heading mb-30">
                    <h1 class="title-heading">ダッシュボード</h1>
                    <div class="filter-heading">
                        <div class="selection-heading">
                            <select class="form-control dropdown-heading">
                                <option value="1">事業所選択</option>
                                <option value="2">Option 1</option>
                                <option value="3">Option 2</option>
                            </select>
                        </div>

                        <div class="selection-heading">
                            <select class="form-control dropdown-heading">
                                <option value="1">支社</option>
                                <option value="2">Option 1</option>
                                <option value="3">Option 2</option>
                            </select>
                        </div>

                        <div id="datepicker" class="datepicker-heading">
                            <input class="input-datepicker" type="text" name="" placeholder="年/月/日" value="2021/10">
                            <span class="icon-datepicker">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                    <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                    <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                    <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                    <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                    <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                    <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                    <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="main-body">
                    <!-- 4 blocks -->
                    <div class="wrapper-article-block">
                        <div class="row">
                            <div class="col-lg-3">
                                <article class="article-block">
                                    <h5 class="title-article-block line-2">事業所名</h5>
                                    <h2 class="date-article-block line-2">AML. Co JP</h2>
                                </article>
                            </div>
                            <div class="col-lg-3">
                                <article class="article-block">
                                    <h5 class="title-article-block line-2">支店名</h5>
                                    <h2 class="date-article-block line-2">Sông Đà</h2>
                                </article>
                            </div>
                            <div class="col-lg-3">
                                <article class="article-block">
                                    <h5 class="title-article-block line-2">年月</h5>
                                    <h2 class="date-article-block line-2">2021/10</h2>
                                </article>
                            </div>
                            <div class="col-lg-3">
                                <!-- block checked  -->
                                <article class="article-block active">
                                    <h5 class="title-article-block line-2">状態</h5>
                                    <h2 class="date-article-block line-2">保留中</h2>
                                </article>
                                <!-- block checked  -->
                            </div>
                        </div>
                    </div>
                    <!-- 4 blocks -->

                    <div class="main-heading mt-30 mb-25">
                        <h4 class="title-result">詳細</h4>
                        <!-- <div class="wrapper-search">
                            <input class="input-search" type="text" placeholder="事業所名" name="search">
                            <button class="button-search" type="submit">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4" d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z" fill="#7C9AA1"/>
                                    <path d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z" fill="#7C9AA1"/>
                                </svg>
                            </button>
                        </div> -->
                    </div>

                    <!-- 11 blocks -->
                    <div class="wrapper-management-block">
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- block has button switches toggle -->
                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">①請求前確認リスト</h4>
                                            <div class="action-management-block">
                                                <small class="notice-management-block">チェックファイル無し</small>
                                                <!-- Button toggle Modal A1000.1 Home -->
                                                <button type="button" id="wrapper-toggle-btn" data-toggle="modal" data-target="#modalQuestion" data-backdrop="static" data-keyboard="false">
                                                    <input type="checkbox" id="toggle-input" class="toggle-input">
                                                    <div id="toggle-btn" class="toggle-btn"></div>
                                                </button>

                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID1" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID1" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>
                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-mint-context">
                                                    <img src="{{ asset('icons/forbidden.svg') }}" alt="close">
                                                    エラー
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <!-- block has button switches toggle -->

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">②請求締めチェックツール</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID2" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID2" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">N/A</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">0</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">0</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <!-- block has tooltip popup -->
                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">③送り出し指示データ一覧</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData" class="btn-right-context btn-blue-context context-popover"
                                                    data-content="..."
                                                    rel="popover"
                                                    data-placement="left"
                                                    data-original-title="Green text"
                                                    data-trigger="manual"
                                                >
                                                    <img src="{{ asset('icons/timer.svg') }}" alt="timer">
                                                    保留中
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <!-- block has tooltip popup -->

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">④総合事業請求止めチェック機能</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID1" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID4" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>
                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-mint-context">
                                                    <img src="{{ asset('icons/forbidden.svg') }}" alt="close">
                                                    エラー
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑤総合事業計上漏れチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID1" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID5" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑥障害チェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID6" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-red-context">
                                                    <img src="{{ asset('icons/slash.svg') }}" alt="slash">
                                                    未チェック
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑦トランデータチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID6" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-green-context">
                                                    <img src="{{ asset('icons/verify.svg') }}" alt="verify">
                                                    完了
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">N/A</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑧おまかせチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID1" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID7" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑨Mファイルチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID8" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-red-context">
                                                    <img src="{{ asset('icons/slash.svg') }}" alt="slash">
                                                    未チェック
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <!-- block has tooltip popup -->
                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑩Sファイルチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID9" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <!-- add class="context-popover" if hover show tooltip -->
                                                <button type="button" id="popoverData" class="btn-right-context btn-blue-context context-popover"
                                                    data-content="需隻でをフ談腰ユ画伎カヒホ新諭ぴ聞賛ょ断防カ東票くだぐ馬野衰ぼこン球難セ紀68包孤湖28働迎僚ゃ。6区心ヨ
                                                        ムヌ午著カ持30遇62止イコサ新評イア者考サメホ備田こにもド表録じげッ個"
                                                    rel="popover"
                                                    data-placement="left"
                                                    data-original-title="保留理由"
                                                    data-trigger="manual"
                                                >
                                                    <img src="{{ asset('icons/timer.svg') }}" alt="timer">
                                                    保留中
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <!-- block has tooltip popup -->

                                <div class="col-management">
                                    <article class="management-block">
                                        <div class="inner-management-block">
                                            <h4 class="title-management-block">⑪TH障害データチェック</h4>
                                            <div class="action-management-block">
                                                <!-- Button upload file: data-target=ID -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalUploadID3" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/export.svg') }}" alt="export">
                                                </button>

                                                <!-- Button Modal B1000.2 -->
                                                <button type="button" class="wrapper-upload-btn" data-toggle="modal" data-target="#modalHistoryDetailID10" data-backdrop="static" data-keyboard="false">
                                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">状態</div>

                                            <div class="right-context">
                                                <button type="button" class="btn-right-context btn-green-context">
                                                    <img src="{{ asset('icons/verify.svg') }}" alt="verify">
                                                    完了
                                                </button>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">最終チェック日時</div>
                                            <div class="right-context">
                                                <p class="text-right-context">2021/19/01 - 20:00</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">チェックの回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                        <div class="inner-management-block context-management-block">
                                            <div class="left-context">エラーの発生回数</div>
                                            <div class="right-context">
                                                <p class="text-right-context">50</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 11 blocks -->
                </div>
            </form>
        </section>
    </div>

    <!-- Modal A1000.1 Home -->
    <div class="modal fade" id="modalQuestion" tabindex="-1" role="dialog" aria-labelledby="modalQuestionTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">状況変更</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text"> この事業所の状況が以下のように変更でしょうか。</p>
                    <p class="modal-text modal-text-bold">完了</p>
                    <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">同意同意</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal A1000.1 Home -->

    <!-- Modal upload file: id=ID &aria-labelledby=ID -->
    <div class="modal fade" id="modalUploadID1" tabindex="-1" role="dialog" aria-labelledby="modalUploadID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <form id="file-upload-form">
                            <input id="file-upload" type="file" name="fileUpload" accept="file_extension" />

                            <label for="file-upload" id="file-drag" class="label-file-drag">
                                <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                <div id="start" class="file-start">
                                    <div id="notimage" class="hidden notimage">Please select file CSV</div>
                                    <span id="file-upload-btn" class="icon-upload-file">
                                        <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                    </span>
                                    <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                </div>
                            </label>
                            <div id="response" class="hidden file-response">
                                <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                <div id="messages"></div>
                            </div>
                            <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>

                            <button type="button" class="home-modal-button default-button" disabled>チェック</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadID2" tabindex="-1" role="dialog" aria-labelledby="modalUploadID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <form id="file-upload-form">
                            <input id="file-upload" type="file" name="fileUpload" accept="file_extension" />

                            <label for="file-upload" id="file-drag" class="label-file-drag">
                                <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                <div id="start" class="file-start">
                                    <div id="notimage" class="hidden notimage">Please select file CSV</div>
                                    <span id="file-upload-btn" class="icon-upload-file">
                                        <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                    </span>
                                    <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                </div>
                            </label>
                            <div id="response" class="hidden file-response">
                                <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                <div class="messages"></div>
                            </div>
                            <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>

                            <button type="button" class="home-modal-button default-button" disabled>チェック</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload file: id=ID &aria-labelledby=ID -->

    <!-- Update Modal here!!! -->
    <!-- Modal B1000.2 view history -->
    <div class="modal fade" id="modalHistoryDetailID1" tabindex="-1" role="dialog" aria-labelledby="modalHistoryDetailID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered view-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェック詳細</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body text-left pt-50 pb-15 max-height">
                    <div class="pl-100 pr-100">
                        <form>
                            <div class="main-heading mb-30">
                                <h2 class="mint-title">TH障害データチェック</h2>
                                <div id="datepicker" class="datepicker-heading">
                                    <input class="input-datepicker" type="text" name="" placeholder="年/月/日" value="2021/10">
                                    <span class="icon-datepicker">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>

                            <div class="main-body">
                                <!-- 4 blocks -->
                                <div class="wrapper-article-block wrapper-article-check">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <article class="article-block first-upload-block">
                                                <h5 class="title-article-block line-2">作成日 </h5>
                                                <h2 class="date-article-block line-2">2021/12/28</h2>
                                            </article>
                                        </div>
                                        <div class="col-lg-3">
                                            <article class="article-block second-upload-block">
                                                <h5 class="title-article-block line-2">支店名</h5>
                                                <h2 class="date-article-block line-2">Sông Đà</h2>
                                            </article>
                                        </div>
                                        <div class="col-lg-3">
                                            <article class="article-block third-upload-block">
                                                <h5 class="title-article-block line-2">年月</h5>
                                                <h2 class="date-article-block line-2">2021/10</h2>
                                            </article>
                                        </div>

                                        <!-- block A2001.1  -->
                                        <div class="col-lg-3">
                                            <article class="article-block checked-upload-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">完了</h2>
                                            </article>
                                        </div>
                                        <!-- block A2001.1  -->

                                        <!-- block A2001.2  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-mint-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">エラー</h2>
                                            </article>
                                        </div> -->
                                        <!-- block A2001.2  -->

                                        <!-- block A2001.3  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-blue-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">保留中</h2>
                                            </article>

                                        </div> -->
                                        <!-- block A2001.3  -->


                                        <!-- block A2001.4  -->
                                        <!-- <div class="col-lg-3">
                                            <article class="article-block checked-red-block">
                                                <h5 class="title-article-block line-2">状態</h5>
                                                <h2 class="date-article-block line-2">未チェック</h2>
                                            </article>
                                        </div> -->
                                        <!-- block A2001.4  -->
                                    </div>
                                </div>
                                <!-- 4 blocks -->
                            </div>

                            <div class="main-heading mt-30 mb-25">
                                <h4 class="blue-title-result">ファイルチェック履歴</h4>
                            </div>

                            <div class="wrapper-detail-check">
                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-green-context">
                                                <img src="{{ asset('icons/verify.svg') }}" alt="verify">
                                                完了
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="green-number-check">0<span></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item detail-check block -->

                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-mint-context">
                                                <img src="{{ asset('icons/forbidden.svg') }}" alt="close">
                                                エラー
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                                        </div>
                                    </div>
                                    <ul class="list-error-file">
                                        <li class="item-error-file">
                                            2行目、A列目：空白にすることはできません。
                                        </li>
                                        <li class="item-error-file">
                                            2行目、B列目：40文字未満で入力してください。
                                        </li>
                                        <li class="item-error-file">
                                            3行目4列目：数字のみを入力してください。
                                        </li>
                                    </ul>
                                </div>
                                <!-- item detail-check block -->

                                <!-- item detail-check block -->
                                <div class="detail-check-block">
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">更新日: <span class="gray-date">2021/12/31 - 20:00 <span></div>
                                            <div class="btn-right-context btn-blue-context">
                                                <img src="{{ asset('icons/timer.svg') }}" alt="timer">
                                                保留中
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">ファイル名: <span class="mint-file-check">Filecheck.csv <span></div>
                                        </div>
                                    </div>
                                    <div class="inner-check-block">
                                        <div class="left-check-block">
                                            <div class="context-check-block">エラー数: <span class="red-number-check">3<span></div>
                                        </div>
                                    </div>
                                    <ul class="list-error-file">
                                        <li class="item-error-file">
                                            2行目、A列目：空白にすることはできません。
                                        </li>
                                        <li class="item-error-file">
                                            2行目、B列目：40文
                                        </li>
                                        <li class="item-error-file">
                                            3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。3行目4列目：数字のみを入力してください。
                                        </li>
                                    </ul>
                                    <div class="reason-check-block mt-20">
                                        <div class="context-check-block">保留理由</div>
                                        <div class="text-reason-check">
                                            一部データ確認待ち
                                        </div>
                                    </div>
                                </div>
                                <!-- item detail-check block -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B1000.2 view history -->

    <!-- scrip -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadFile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
    <script>
        const container = document.getElementById("wrapper-toggle-btn")
        const toggleInput = document.getElementById("toggle-input")
        const toggleBtn = document.getElementById("toggle-btn")
        const modalQuestion = document.getElementById("modalQuestion")

        toggleInput.addEventListener("click", () => {
            if(toggleInput.checked) {
                toggleBtn.style.marginLeft = "16px"
                toggleBtn.style.backgroundColor = "#0F94B5"
                container.style.backgroundColor = "#B5E5F0"
                modalQuestion.addClass("show")
            }
            else {
                toggleBtn.style.marginLeft = "-2px"
                toggleBtn.style.backgroundColor = "#F5F7FB"
                container.style.backgroundColor = "#7c9aa1"
                modalQuestion.removeClass("show")
            }
        })
    </script>
    <!-- scrip -->
</body>
</html>
