<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Detail Branch</title>
  <!-- css -->
  <link rel="stylesheet" href="{{asset('css/main.css?v=6')}}">
  <link rel="stylesheet" href="{{asset('css/template.css?v=6')}}">
  <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
  <!-- css -->
</head>
<body>
    <style>
        body {
            background-image: url("/images/bg-gray-admin.png")!important;
        }
    </style>
    <div class="wrapper d-flex flex-row">
        <!-- sidebar -->
        <section class="left-sidebar admin-sidebar">
            <h2 class="title-sidebar">
                ロゴ
            </h2>
            <div class="nav flex-column nav-pills">
                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.4" d="M14.69 0.5H6.31C2.67 0.5 0.5 2.67 0.5 6.31V14.69C0.5 18.33 2.67 20.5 6.31 20.5H14.69C18.33 20.5 20.5 18.33 20.5 14.69V6.31C20.5 2.67 18.33 0.5 14.69 0.5Z" fill="#BFBFBF"/>
                        <path d="M16.8105 7.37012C16.8105 7.78012 16.4805 8.12012 16.0605 8.12012H10.8105C10.4005 8.12012 10.0605 7.78012 10.0605 7.37012C10.0605 6.96012 10.4005 6.62012 10.8105 6.62012H16.0605C16.4805 6.62012 16.8105 6.96012 16.8105 7.37012Z" fill="#BFBFBF"/>
                        <path d="M8.47055 6.40006L6.22055 8.65006C6.07055 8.80006 5.88055 8.87006 5.69055 8.87006C5.50055 8.87006 5.30055 8.80006 5.16055 8.65006L4.41055 7.90006C4.11055 7.61006 4.11055 7.13006 4.41055 6.84006C4.70055 6.55006 5.17055 6.55006 5.47055 6.84006L5.69055 7.06006L7.41055 5.34006C7.70055 5.05006 8.17055 5.05006 8.47055 5.34006C8.76055 5.63006 8.76055 6.11006 8.47055 6.40006Z" fill="#BFBFBF"/>
                        <path d="M16.8105 14.3701C16.8105 14.7801 16.4805 15.1201 16.0605 15.1201H10.8105C10.4005 15.1201 10.0605 14.7801 10.0605 14.3701C10.0605 13.9601 10.4005 13.6201 10.8105 13.6201H16.0605C16.4805 13.6201 16.8105 13.9601 16.8105 14.3701Z" fill="#BFBFBF"/>
                        <path d="M8.47055 13.4001L6.22055 15.6501C6.07055 15.8001 5.88055 15.8701 5.69055 15.8701C5.50055 15.8701 5.30055 15.8001 5.16055 15.6501L4.41055 14.9001C4.11055 14.6101 4.11055 14.1301 4.41055 13.8401C4.70055 13.5501 5.17055 13.5501 5.47055 13.8401L5.69055 14.0601L7.41055 12.3401C7.70055 12.0501 8.17055 12.0501 8.47055 12.3401C8.76055 12.6301 8.76055 13.1101 8.47055 13.4001Z" fill="#BFBFBF"/>
                    </svg>
                    </i>
                    請求チェック状況管理
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.8" d="M13.3992 2.52009L13.3692 2.59009L10.4692 9.32009H7.61922C6.93922 9.32009 6.29922 9.45009 5.69922 9.71009L7.44922 5.53009L7.48922 5.44009L7.54922 5.28009C7.57922 5.21009 7.59922 5.15009 7.62922 5.10009C8.93922 2.07009 10.4192 1.38009 13.3992 2.52009Z" fill="#BFBFBF"/>
                            <path d="M18.7907 9.52002C18.3407 9.39002 17.8707 9.32002 17.3807 9.32002H10.4707L13.3707 2.59002L13.4007 2.52002C13.5407 2.57002 13.6907 2.64002 13.8407 2.69002L16.0507 3.62002C17.2807 4.13002 18.1407 4.66002 18.6707 5.30002C18.7607 5.42002 18.8407 5.53002 18.9207 5.66002C19.0107 5.80002 19.0807 5.94002 19.1207 6.09002C19.1607 6.18002 19.1907 6.26002 19.2107 6.35002C19.4707 7.20002 19.3107 8.23002 18.7907 9.52002Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M22.2602 14.2001V16.1501C22.2602 16.3501 22.2502 16.5501 22.2402 16.7401C22.0502 20.2401 20.1002 22.0001 16.4002 22.0001H8.60023C8.35023 22.0001 8.12023 21.9801 7.89023 21.9501C4.71023 21.7401 3.01023 20.0401 2.79023 16.8601C2.76023 16.6201 2.74023 16.3901 2.74023 16.1501V14.2001C2.74023 12.1901 3.96023 10.4601 5.70023 9.71007C6.30023 9.45007 6.94023 9.32007 7.62023 9.32007H17.3802C17.8702 9.32007 18.3402 9.39007 18.7902 9.52007C20.7902 10.1301 22.2602 11.9901 22.2602 14.2001Z" fill="#BFBFBF"/>
                            <path opacity="0.6" d="M7.45023 5.53003L5.70023 9.71003C3.96023 10.46 2.74023 12.19 2.74023 14.2V11.27C2.74023 8.43003 4.76023 6.06003 7.45023 5.53003Z" fill="#BFBFBF"/>
                            <path opacity="0.6" d="M22.2591 11.2701V14.2001C22.2591 11.9901 20.7891 10.1301 18.7891 9.52009C19.3091 8.23009 19.4691 7.20009 19.2091 6.35009C19.1891 6.26009 19.1591 6.18009 19.1191 6.09009C20.9891 7.06009 22.2591 9.03009 22.2591 11.2701Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    請求未確定
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.1702 6.9499C21.5302 4.7799 19.7202 2.9699 17.5502 2.3299C15.9002 1.8499 14.7602 1.8899 13.9702 2.4799C13.0202 3.1899 12.9102 4.4699 12.9102 5.3799V7.8699C12.9102 10.3299 14.0302 11.5799 16.2302 11.5799H19.1002C20.0002 11.5799 21.2902 11.4699 22.0002 10.5199C22.6102 9.7399 22.6602 8.5999 22.1702 6.9499Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M19.4094 13.3599C19.1494 13.0599 18.7694 12.8899 18.3794 12.8899H14.7994C13.0394 12.8899 11.6094 11.4599 11.6094 9.69991V6.11991C11.6094 5.72991 11.4394 5.34991 11.1394 5.08991C10.8494 4.82991 10.4494 4.70991 10.0694 4.75991C7.71941 5.05991 5.55941 6.34991 4.14941 8.28991C2.72941 10.2399 2.20941 12.6199 2.65941 14.9999C3.30941 18.4399 6.05941 21.1899 9.50941 21.8399C10.0594 21.9499 10.6094 21.9999 11.1594 21.9999C12.9694 21.9999 14.7194 21.4399 16.2094 20.3499C18.1494 18.9399 19.4394 16.7799 19.7394 14.4299C19.7894 14.0399 19.6694 13.6499 19.4094 13.3599Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    エラー統計
                </a>

                <div class="nav-sidebar-dropdown">
                    <div class="nav-link nav-sidebar nav-sidebar-heading">
                        <i class="icon-sidebar">
                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.5 8C21.1569 8 22.5 6.65685 22.5 5C22.5 3.34315 21.1569 2 19.5 2C17.8431 2 16.5 3.34315 16.5 5C16.5 6.65685 17.8431 8 19.5 8Z" fill="#BFBFBF"/>
                                <path opacity="0.4" d="M19.5 9.5C17.02 9.5 15 7.48 15 5C15 4.28 15.19 3.61 15.49 3H8.02C4.57 3 2.5 5.06 2.5 8.52V16.47C2.5 19.94 4.57 22 8.02 22H15.97C19.43 22 21.49 19.94 21.49 16.48V9.01C20.89 9.31 20.22 9.5 19.5 9.5Z" fill="#BFBFBF"/>
                            </svg>
                        </i>
                        アラート管理
                        <i class="icon-polygon ml-auto">
                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.46967 17.5303C9.17678 17.2374 9.17678 16.7626 9.46967 16.4697L13.9393 12L9.46967 7.53033C9.17678 7.23744 9.17678 6.76256 9.46967 6.46967C9.76256 6.17678 10.2374 6.17678 10.5303 6.46967L15.5303 11.4697C15.671 11.6103 15.75 11.8011 15.75 12C15.75 12.1989 15.671 12.3897 15.5303 12.5303L10.5303 17.5303C10.2374 17.8232 9.76256 17.8232 9.46967 17.5303Z" fill="#BFBFBF"/>
                            </svg>
                        </i>
                    </div>
                    <div class="nav-sidebar-body">
                        <a href="" class="nav-link-dropdown">設定</a>

                        <a href="" class="nav-link-dropdown active">アラート管理</a>

                        <a href="" class="nav-link-dropdown">グループ一覧管理</a>
                    </div>
                </div>

                <a href="#" class="nav-link nav-sidebar active">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.1 4.15002V6H9.62C8.00999 6 7.20001 6.80998 7.20001 8.41998V18H4.65002C3.22002 18 2.5 17.28 2.5 15.85V4.15002C2.5 2.72002 3.22002 2 4.65002 2H8.95001C10.38 2 11.1 2.72002 11.1 4.15002Z" fill="#BFBFBF"/>
                            <path opacity="0.4" d="M17.8702 8.41998V19.58C17.8702 21.19 17.0702 22 15.4602 22H9.62018C8.01018 22 7.2002 21.19 7.2002 19.58V8.41998C7.2002 6.80998 8.01018 6 9.62018 6H15.4602C17.0702 6 17.8702 6.80998 17.8702 8.41998Z" fill="#BFBFBF"/>
                            <path d="M22.5004 4.15002V15.85C22.5004 17.28 21.7803 18 20.3503 18H17.8704V8.41998C17.8704 6.80998 17.0704 6 15.4604 6H13.9004V4.15002C13.9004 2.72002 14.6204 2 16.0504 2H20.3503C21.7803 2 22.5004 2.72002 22.5004 4.15002Z" fill="#BFBFBF"/>
                            <path d="M14.5 11.75H10.5C10.09 11.75 9.75 11.41 9.75 11C9.75 10.59 10.09 10.25 10.5 10.25H14.5C14.91 10.25 15.25 10.59 15.25 11C15.25 11.41 14.91 11.75 14.5 11.75Z" fill="#BFBFBF"/>
                            <path d="M14.5 14.75H10.5C10.09 14.75 9.75 14.41 9.75 14C9.75 13.59 10.09 13.25 10.5 13.25H14.5C14.91 13.25 15.25 13.59 15.25 14C15.25 14.41 14.91 14.75 14.5 14.75Z" fill="#BFBFBF"/>
                            <path d="M13.25 19V22H11.75V19C11.75 18.59 12.09 18.25 12.5 18.25C12.91 18.25 13.25 18.59 13.25 19Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    支店
                </a>

                <a href="#" class="nav-link nav-sidebar">
                    <i class="icon-sidebar">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M11.4608 2.06008L5.96078 4.12008C4.91078 4.52008 4.05078 5.76008 4.05078 6.89008V14.9901C4.05078 15.8001 4.58078 16.8701 5.23078 17.3501L10.7308 21.4601C11.7008 22.1901 13.2908 22.1901 14.2608 21.4601L19.7608 17.3501C20.4108 16.8601 20.9408 15.8001 20.9408 14.9901V6.89008C20.9408 5.77008 20.0808 4.52008 19.0308 4.13008L13.5308 2.07008C12.9708 1.85008 12.0308 1.85008 11.4608 2.06008Z" fill="#BFBFBF"/>
                            <path d="M11.1602 14.2301C10.9702 14.2301 10.7802 14.1601 10.6302 14.0101L9.02023 12.4001C8.73023 12.1101 8.73023 11.6301 9.02023 11.3401C9.31023 11.0501 9.79023 11.0501 10.0802 11.3401L11.1602 12.4201L14.9302 8.65012C15.2202 8.36012 15.7002 8.36012 15.9902 8.65012C16.2802 8.94012 16.2802 9.42012 15.9902 9.71012L11.6902 14.0101C11.5402 14.1601 11.3502 14.2301 11.1602 14.2301Z" fill="#BFBFBF"/>
                        </svg>
                    </i>
                    役割と権限の設定
                </a>
            </div>
        </section>
        <!-- sidebar -->

        <section class="right-main">
            <!-- header -->
            <header class="main-header">
                <div class="header-account">
                    <span class="name-account">AML.Co  JP</span>
                    <span class="email-account">aml@gmail.com</span>
                </div>
                <a href="#logout" class="header-logout">
                    <i class="icon-logout">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.4" d="M9 7.2V16.79C9 20 11 22 14.2 22H16.79C19.99 22 21.99 20 21.99 16.8V7.2C22 4 20 2 16.8 2H14.2C11 2 9 4 9 7.2Z" fill="#7C9AA2"/>
                            <path d="M12.43 8.12L15.78 11.47C16.07 11.76 16.07 12.24 15.78 12.53L12.43 15.88C12.14 16.17 11.66 16.17 11.37 15.88C11.08 15.59 11.08 15.11 11.37 14.82L13.44 12.75H2.75C2.34 12.75 2 12.41 2 12C2 11.59 2.34 11.25 2.75 11.25H13.44L11.37 9.18C11.22 9.03 11.15 8.84 11.15 8.65C11.15 8.46 11.22 8.27 11.37 8.12C11.66 7.82 12.13 7.82 12.43 8.12Z" fill="#7C9AA2"/>
                        </svg>
                    </i>
                    ログアウト
                </a>
            </header>
            <!-- header -->

            <!-- content page -->
            <form action="" class="main-content main-admin pt-20">
                <ol class="breadcrumb main-breadcrumb">
                    <li class="breadcrumb-item main-breadcrumb-item"><a href="#">支店</a></li>
                    <li class="breadcrumb-item main-breadcrumb-item active" aria-current="page">詳細</li>
                </ol>

                <div class="main-heading mb-30">
                    <h1 class="title-heading">aml.co jp</h1>
                </div>

                <div class="detail-info">
                    <div class="row mb-20">
                        <div class="col-md-2 left-detail-info">
                            ID
                        </div>
                        <div class="col-md-10 right-detail-info">
                            000111
                        </div>
                    </div>
                    <div class="row mb-20">
                        <div class="col-md-2 left-detail-info">
                            メール
                        </div>
                        <div class="col-md-10 right-detail-info">
                            aml@gmail.jp
                        </div>
                    </div>
                    <div class="row mb-20">
                        <div class="col-md-2 left-detail-info">
                            利用者数利用者数
                        </div>
                        <div class="col-md-10 right-detail-info">
                            3
                        </div>
                    </div>
                </div>

                <div class="main-body">
                    <div class="wrapper-table">
                        <div class="top-table">
                            <button type="button" class="button-add-branch default-button" data-toggle="modal" data-target="#modalAddOfficeID1" data-backdrop="static" data-keyboard="false">事業所追加</button>
                        </div>

                        <table class="table table-striped table-admin mt-15 mb-15">
                            <thead>
                                <tr>
                                    <th class="col-md-1" scope="col">STT</th>
                                    <th class="col-md-3" scope="col">事業所</th>
                                    <th class="col-md-2" scope="col">更新日</th>
                                    <th class="col-md-3" scope="col">利用者数</th>
                                    <th class="col-md-3" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>000111</td>
                                    <td>aml.co jp</td>
                                    <td>2021/12/28</td>
                                    <td>3</td>
                                    <td>
                                        <a href="#link-page-detail-branch" class="button-view-detail">
                                            <span class="text-view-detail">詳細</span>
                                            <img src="{{ asset('icons/view.svg') }}" alt="view">
                                        </a>
                                        <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalDeleteID1">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>000111</td>
                                    <td>aml.co jp</td>
                                    <td>2021/12/28</td>
                                    <td>3</td>
                                    <td>
                                        <a href="#link-page-detail-branch" class="button-view-detail">
                                            <span class="text-view-detail">詳細</span>
                                            <img src="{{ asset('icons/view.svg') }}" alt="view">
                                        </a>
                                        <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalDeleteID1">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>000111</td>
                                    <td>aml.co jp</td>
                                    <td>2021/12/28</td>
                                    <td>3</td>
                                    <td>
                                        <a href="#link-page-detail-branch" class="button-view-detail">
                                            <span class="text-view-detail">詳細</span>
                                            <img src="{{ asset('icons/view.svg') }}" alt="view">
                                        </a>
                                        <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalDeleteID1">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- add pagination if more 10 rows -->
                        <!-- <nav class="wrapper-pagination">
                            <ul class="pagination">
                                add class="disabled" if cannot previous
                                <li class="page-item disabled">
                                    <a class="page-link" href="#">
                                        <img src="{{ asset('icons/arrow-left.svg') }}" alt="previous">
                                    </a>
                                </li>
                                add class="active" if current page-link
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">...</a></li>
                                <li class="page-item"><a class="page-link" href="#">10</a></li>
                                add class="disabled" if cannot next
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <img src="{{ asset('icons/arrow-right.svg') }}" alt="next">
                                    </a>
                                </li>
                            </ul>
                        </nav> -->
                    </div>
                </div>
            </form>
            <!-- content page -->
        </section>
    </div>

    <!-- Modal B1000.3 事業所追加 -->
    <div class="modal fade" id="modalAddOfficeID1" tabindex="-1" role="dialog" aria-labelledby="modalAddOfficeID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">事業所追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="add-branch">
                        <form>
                            <div class="form-group">
                                <label class="label-form" for="multiSelectForm">事業所名</label>
                                <select id="multiSelectForm" data-placeholder="事業所名" multiple class="chosen-select form-control-add" name="test">
                                    <option value="">AML Co JP</option>
                                    <option value="">AML Co JP</option>
                                    <option value="">AML</option>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label class="label-form" for="inputControlForm">事業所名</label>
                                <input type="email" class="form-control-add" id="inputControlForm" placeholder="事業所名">
                            </div> -->

                            <button type="submit" class="home-modal-button default-button">事業所追加</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B1000.3 事業所追加 -->

    <!-- Modal B1000.4 Delete グループ削除削除 -->
    <div class="modal fade" id="modalDeleteID1" tabindex="-1" role="dialog" aria-labelledby="modalDeleteID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">対象削除削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text"> 削除してもよろしいですか？</p>
                            <div class="d-flex align-items-center justify-content-center mt-40">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                                <button type="button" class="w-205 default-button" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#modalSuccessID1">適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B1000.4 Delete グループ削除削除 -->

    <!-- Modal B1000.5 Success ベースを正常に削除 -->
    <div class="modal fade" id="modalSuccessID1" tabindex="-1" role="dialog" aria-labelledby="modalSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">ベースを正常に削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="success">
                            <p class="modal-text"> ベースを正常に削除</p>

                            <button type="submit" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">適用</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B1000.5 Success ベースを正常に削除 -->

    <!-- scrip -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/toggle-menu.js') }}"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
    <!-- scrip -->
</body>
</html>
