<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="shortcut icon" href="{{asset('icons/favicon.ico')}}">

    <!-- css -->
    <link rel="stylesheet" href="{{asset('css/main.css?v=6')}}">
    <link rel="stylesheet" href="{{asset('css/template.css?v=6')}}">
    <!-- css -->
</head>
<body>
    <div class="login-wrapper login-wrapper-admin">
        <div class="login-outer">
            <a href="/" class="login-logo d-inline-block">
                <img src="{{ asset('icons/logo-admin.svg') }}" alt="logo">
            </a>
            <h1 class="login-title">ログイン</h1>
            <button type="button" class="default-upload-button default-button m-0">
              Login with Zoo
            </button>
            <p class="login-text">
              サイトの有効期限が切れました、 再度ログインして下さい ログインする前に､SSO ポータル <br />
              サイトから再度リンクをクリックしてください。
            </p>
        </div>
    </div>

    <!-- scrip -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
