@extends('errors::minimal')

@section('title', __('サーバーエラー'))
@section('code', '５００')
@section('message', __('サーバーエラー'))
