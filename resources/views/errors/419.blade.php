@extends('errors::minimal')

@section('title', __('Page Expired'))
@section('code', '４１９')
@section('message', __('Page Expired'))
