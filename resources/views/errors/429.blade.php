@extends('errors::minimal')

@section('title', __('リクエスト数が多すぎます'))
@section('code', '４２９')
@section('message', __('リクエスト数が多すぎます'))
