@extends('errors.minimal')

@section('title', __('見つかりません'))
@section('code', '４０４')
@section('message', __('見つかりません'))
