@extends('errors::minimal')

@section('title', __('サービス停止'))
@section('code', '５０３')
@section('message', __('サービス停止'))
