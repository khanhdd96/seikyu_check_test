@extends('errors::minimal')

@section('title', __('禁断'))
@section('code', '４０３')
@section('message', __($exception->getMessage() ?: '禁断'))
