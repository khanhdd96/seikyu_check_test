@extends('errors::minimal')

@section('title', __('無許可'))
@section('code', '４０１')
@section('message', __('無許可'))
