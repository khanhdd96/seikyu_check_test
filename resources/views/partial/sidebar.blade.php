<section class="left-sidebar">
    <a href="/" class="title-sidebar d-inline-block">
        <img src="{{ asset('icons/logo-user.svg') }}" alt="logo-user">
    </a>
    @if(Auth::user()->is_admin)
    <div class="btn-group btn-toggle swtich-role swtich-role-user">
        <a href="{{route('home.index')}}" class="btn btn-default active ">事業所</a>
        <a href="{{route('admin.home.index')}}" class="btn btn-default">管理者</a>
    </div>
    @endif
    <div class="nav flex-column nav-pills">
        <!-- menu active -->
        <a href="/" class="nav-link nav-sidebar @if ((request('type_check')) === null) active @endif">
            <i class="icon-sidebar">
                <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.0585 6.29175C17.5252 4.48342 16.0168 2.97508 14.2085 2.44175C12.8335 2.04175 11.8835 2.07508 11.2252 2.56675C10.4335 3.15842 10.3418 4.22508 10.3418 4.98342V7.05842C10.3418 9.10842 11.2752 10.1501 13.1085 10.1501H15.5002C16.2502 10.1501 17.3252 10.0584 17.9168 9.26675C18.4252 8.61675 18.4668 7.66675 18.0585 6.29175Z" fill="#7C9AA1"/>
                    <path opacity="0.4" d="M15.7585 11.6335C15.5418 11.3835 15.2252 11.2418 14.9002 11.2418H11.9168C10.4502 11.2418 9.25849 10.0501 9.25849 8.58346V5.60013C9.25849 5.27512 9.11683 4.95846 8.86683 4.74179C8.62516 4.52513 8.29183 4.42513 7.97516 4.46679C6.01683 4.71679 4.21683 5.79179 3.04183 7.40846C1.85849 9.03346 1.42516 11.0168 1.80016 13.0001C2.34183 15.8668 4.63349 18.1585 7.50849 18.7001C7.96683 18.7918 8.42516 18.8335 8.88349 18.8335C10.3918 18.8335 11.8502 18.3668 13.0918 17.4585C14.7085 16.2835 15.7835 14.4835 16.0335 12.5251C16.0752 12.2001 15.9752 11.8751 15.7585 11.6335Z" fill="#7C9AA1"/>
                </svg>
            </i>
            ダッシュボード
        </a>
        <!-- menu active -->

        @foreach(\App\Consts::TITLE_MENU as $key => $value)
        <a href="/file_check/{{\App\Consts::TYPE_MENU[$key]}}" class="nav-link nav-sidebar @if (request('type_check') && request('type_check') == \App\Consts::TYPE_MENU[$key])active @endif">
            <i class="icon-sidebar">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M10.6425 0H4.3575C1.6275 0 0 1.6275 0 4.3575V10.6425C0 13.3725 1.6275 15 4.3575 15H10.6425C13.3725 15 15 13.3725 15 10.6425V4.3575C15 1.6275 13.3725 0 10.6425 0Z" fill="#7C9AA1"/>
                    <path d="M12.2325 5.1525C12.2325 5.46 11.985 5.715 11.67 5.715H7.7325C7.425 5.715 7.17 5.46 7.17 5.1525C7.17 4.845 7.425 4.59 7.7325 4.59H11.67C11.985 4.59 12.2325 4.845 12.2325 5.1525Z" fill="#7C9AA1"/>
                    <path d="M5.9775 4.425L4.29 6.1125C4.1775 6.225 4.035 6.2775 3.8925 6.2775C3.75 6.2775 3.6 6.225 3.495 6.1125L2.9325 5.55C2.7075 5.3325 2.7075 4.9725 2.9325 4.755C3.15 4.5375 3.5025 4.5375 3.7275 4.755L3.8925 4.92L5.1825 3.63C5.4 3.4125 5.7525 3.4125 5.9775 3.63C6.195 3.8475 6.195 4.2075 5.9775 4.425Z" fill="#7C9AA1"/>
                    <path d="M12.2325 10.4025C12.2325 10.71 11.985 10.965 11.67 10.965H7.7325C7.425 10.965 7.17 10.71 7.17 10.4025C7.17 10.095 7.425 9.84 7.7325 9.84H11.67C11.985 9.84 12.2325 10.095 12.2325 10.4025Z" fill="#7C9AA1"/>
                    <path d="M5.9775 9.675L4.29 11.3625C4.1775 11.475 4.035 11.5275 3.8925 11.5275C3.75 11.5275 3.6 11.475 3.495 11.3625L2.9325 10.8C2.7075 10.5825 2.7075 10.2225 2.9325 10.005C3.15 9.7875 3.5025 9.7875 3.7275 10.005L3.8925 10.17L5.1825 8.88C5.4 8.6625 5.7525 8.6625 5.9775 8.88C6.195 9.0975 6.195 9.4575 5.9775 9.675Z" fill="#7C9AA1"/>
                </svg>
            </i>
            {!! $value !!}
        </a>
        @endforeach
    </div>
</section>
