<!-- scrip -->
<script>
    $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
    $('textarea').change(function(){
        this.value = $.trim(this.value);
    });
</script>
<script type="text/javascript" src="{{ asset('js/toggle-menu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- scrip -->
</body>
</html>
