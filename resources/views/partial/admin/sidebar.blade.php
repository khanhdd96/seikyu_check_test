<style>
.nav-link-dropdown {
    padding-left: 45px;
}
</style>
<section class="left-sidebar admin-sidebar">
    <a href="{{route('admin.home.index')}}" class="title-sidebar d-inline-block">
        <img src="{{ asset('icons/logo-user.svg') }}" alt="logo-admin">
    </a>
    <div class="btn-group btn-toggle swtich-role swtich-role-admin">
        <a href="{{route('home.index')}}" class="btn btn-default">事業所</a>
        <a href="{{route('admin.home.index')}}" class="btn btn-primary active">管理者</a>
    </div>
    <div class="nav flex-column nav-pills">
        <a href="{{route('admin.home.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('admin.home.index') || Route::currentRouteNamed('facilitity.show')? 'active' : '' }}" class="nav-link nav-sidebar">
            <i class="icon-sidebar">
                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M14.69 0.5H6.31C2.67 0.5 0.5 2.67 0.5 6.31V14.69C0.5 18.33 2.67 20.5 6.31 20.5H14.69C18.33 20.5 20.5 18.33 20.5 14.69V6.31C20.5 2.67 18.33 0.5 14.69 0.5Z" fill="#BFBFBF"/>
                    <path d="M16.8105 7.37012C16.8105 7.78012 16.4805 8.12012 16.0605 8.12012H10.8105C10.4005 8.12012 10.0605 7.78012 10.0605 7.37012C10.0605 6.96012 10.4005 6.62012 10.8105 6.62012H16.0605C16.4805 6.62012 16.8105 6.96012 16.8105 7.37012Z" fill="#BFBFBF"/>
                    <path d="M8.47055 6.40006L6.22055 8.65006C6.07055 8.80006 5.88055 8.87006 5.69055 8.87006C5.50055 8.87006 5.30055 8.80006 5.16055 8.65006L4.41055 7.90006C4.11055 7.61006 4.11055 7.13006 4.41055 6.84006C4.70055 6.55006 5.17055 6.55006 5.47055 6.84006L5.69055 7.06006L7.41055 5.34006C7.70055 5.05006 8.17055 5.05006 8.47055 5.34006C8.76055 5.63006 8.76055 6.11006 8.47055 6.40006Z" fill="#BFBFBF"/>
                    <path d="M16.8105 14.3701C16.8105 14.7801 16.4805 15.1201 16.0605 15.1201H10.8105C10.4005 15.1201 10.0605 14.7801 10.0605 14.3701C10.0605 13.9601 10.4005 13.6201 10.8105 13.6201H16.0605C16.4805 13.6201 16.8105 13.9601 16.8105 14.3701Z" fill="#BFBFBF"/>
                    <path d="M8.47055 13.4001L6.22055 15.6501C6.07055 15.8001 5.88055 15.8701 5.69055 15.8701C5.50055 15.8701 5.30055 15.8001 5.16055 15.6501L4.41055 14.9001C4.11055 14.6101 4.11055 14.1301 4.41055 13.8401C4.70055 13.5501 5.17055 13.5501 5.47055 13.8401L5.69055 14.0601L7.41055 12.3401C7.70055 12.0501 8.17055 12.0501 8.47055 12.3401C8.76055 12.6301 8.76055 13.1101 8.47055 13.4001Z" fill="#BFBFBF"/>
                </svg>
            </i>
            請求チェック状況管理
        </a>

        <a href="{{route('file_check_admin.detail', ['type_check' => \App\Consts::TYPE_CHECK_NUMBER_9])}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('file_check_admin.detail') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.8" d="M13.3992 2.52009L13.3692 2.59009L10.4692 9.32009H7.61922C6.93922 9.32009 6.29922 9.45009 5.69922 9.71009L7.44922 5.53009L7.48922 5.44009L7.54922 5.28009C7.57922 5.21009 7.59922 5.15009 7.62922 5.10009C8.93922 2.07009 10.4192 1.38009 13.3992 2.52009Z" fill="#BFBFBF"/>
                    <path d="M18.7907 9.52002C18.3407 9.39002 17.8707 9.32002 17.3807 9.32002H10.4707L13.3707 2.59002L13.4007 2.52002C13.5407 2.57002 13.6907 2.64002 13.8407 2.69002L16.0507 3.62002C17.2807 4.13002 18.1407 4.66002 18.6707 5.30002C18.7607 5.42002 18.8407 5.53002 18.9207 5.66002C19.0107 5.80002 19.0807 5.94002 19.1207 6.09002C19.1607 6.18002 19.1907 6.26002 19.2107 6.35002C19.4707 7.20002 19.3107 8.23002 18.7907 9.52002Z" fill="#BFBFBF"/>
                    <path opacity="0.4" d="M22.2602 14.2001V16.1501C22.2602 16.3501 22.2502 16.5501 22.2402 16.7401C22.0502 20.2401 20.1002 22.0001 16.4002 22.0001H8.60023C8.35023 22.0001 8.12023 21.9801 7.89023 21.9501C4.71023 21.7401 3.01023 20.0401 2.79023 16.8601C2.76023 16.6201 2.74023 16.3901 2.74023 16.1501V14.2001C2.74023 12.1901 3.96023 10.4601 5.70023 9.71007C6.30023 9.45007 6.94023 9.32007 7.62023 9.32007H17.3802C17.8702 9.32007 18.3402 9.39007 18.7902 9.52007C20.7902 10.1301 22.2602 11.9901 22.2602 14.2001Z" fill="#BFBFBF"/>
                    <path opacity="0.6" d="M7.45023 5.53003L5.70023 9.71003C3.96023 10.46 2.74023 12.19 2.74023 14.2V11.27C2.74023 8.43003 4.76023 6.06003 7.45023 5.53003Z" fill="#BFBFBF"/>
                    <path opacity="0.6" d="M22.2591 11.2701V14.2001C22.2591 11.9901 20.7891 10.1301 18.7891 9.52009C19.3091 8.23009 19.4691 7.20009 19.2091 6.35009C19.1891 6.26009 19.1591 6.18009 19.1191 6.09009C20.9891 7.06009 22.2591 9.03009 22.2591 11.2701Z" fill="#BFBFBF"/>
                </svg>
            </i>
            請求未確定
        </a>

        <a href="{{route('check-statistics.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('check-statistics.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M22.1702 6.9499C21.5302 4.7799 19.7202 2.9699 17.5502 2.3299C15.9002 1.8499 14.7602 1.8899 13.9702 2.4799C13.0202 3.1899 12.9102 4.4699 12.9102 5.3799V7.8699C12.9102 10.3299 14.0302 11.5799 16.2302 11.5799H19.1002C20.0002 11.5799 21.2902 11.4699 22.0002 10.5199C22.6102 9.7399 22.6602 8.5999 22.1702 6.9499Z" fill="#BFBFBF"/>
                    <path opacity="0.4" d="M19.4094 13.3599C19.1494 13.0599 18.7694 12.8899 18.3794 12.8899H14.7994C13.0394 12.8899 11.6094 11.4599 11.6094 9.69991V6.11991C11.6094 5.72991 11.4394 5.34991 11.1394 5.08991C10.8494 4.82991 10.4494 4.70991 10.0694 4.75991C7.71941 5.05991 5.55941 6.34991 4.14941 8.28991C2.72941 10.2399 2.20941 12.6199 2.65941 14.9999C3.30941 18.4399 6.05941 21.1899 9.50941 21.8399C10.0594 21.9499 10.6094 21.9999 11.1594 21.9999C12.9694 21.9999 14.7194 21.4399 16.2094 20.3499C18.1494 18.9399 19.4394 16.7799 19.7394 14.4299C19.7894 14.0399 19.6694 13.6499 19.4094 13.3599Z" fill="#BFBFBF"/>
                </svg>
            </i>
            エラー統計
        </a>

        <div class="nav-sidebar-dropdown">
            <!-- if dropdown active add class="active" to nav-link ==> <div class="nav-link nav-sidebar nav-sidebar-heading active"> -->
            <div id="side-bar-alert" class="nav-link nav-sidebar nav-sidebar-heading">
                <i class="icon-sidebar">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.5 8C21.1569 8 22.5 6.65685 22.5 5C22.5 3.34315 21.1569 2 19.5 2C17.8431 2 16.5 3.34315 16.5 5C16.5 6.65685 17.8431 8 19.5 8Z" fill="#BFBFBF"/>
                        <path opacity="0.4" d="M19.5 9.5C17.02 9.5 15 7.48 15 5C15 4.28 15.19 3.61 15.49 3H8.02C4.57 3 2.5 5.06 2.5 8.52V16.47C2.5 19.94 4.57 22 8.02 22H15.97C19.43 22 21.49 19.94 21.49 16.48V9.01C20.89 9.31 20.22 9.5 19.5 9.5Z" fill="#BFBFBF"/>
                    </svg>
                </i>
                アラート管理
                <script>
                    $(window).on('load', function () {
                        @if(Route::currentRouteNamed('setting-alert-custom.create') || Route::currentRouteNamed('setting-alert-auto.index') || Route::currentRouteNamed('setting-alert-custom.index') || Route::currentRouteNamed('setting-alert-auto.edit') || Route::currentRouteNamed('setting-alert-auto.create')|| Route::currentRouteNamed('setting-alert-custom.edit')|| Route::currentRouteNamed('setting-group-user.index'))
                            $('#side-bar-alert').click();
                        @endif
                    });
                </script>
                <!-- if dropdown active add class="active" to icon-polygon ==> <i class="icon-polygon ml-auto active"> -->
                <i class="icon-polygon ml-auto">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.46967 17.5303C9.17678 17.2374 9.17678 16.7626 9.46967 16.4697L13.9393 12L9.46967 7.53033C9.17678 7.23744 9.17678 6.76256 9.46967 6.46967C9.76256 6.17678 10.2374 6.17678 10.5303 6.46967L15.5303 11.4697C15.671 11.6103 15.75 11.8011 15.75 12C15.75 12.1989 15.671 12.3897 15.5303 12.5303L10.5303 17.5303C10.2374 17.8232 9.76256 17.8232 9.46967 17.5303Z" fill="#BFBFBF"/>
                    </svg>
                </i>
            </div>
            <div class="nav-sidebar-body">
                <a href="{{route('setting-alert-auto.index')}}" class="nav-link-dropdown {{Route::currentRouteNamed('setting-alert-auto.create') || Route::currentRouteNamed('setting-alert-auto.index') || Route::currentRouteNamed('setting-alert-auto.edit') ? 'active' : ''}}">リマインド（自動）アラート</a>

                <!-- if dropdown active add class="active" to nav-link-dropdown ==> <a href="" class="nav-link-dropdown active"> -->
                <a href="{{route('setting-alert-custom.index')}}" class="nav-link-dropdown {{Route::currentRouteNamed('setting-alert-custom.create') || Route::currentRouteNamed('setting-alert-custom.index') || Route::currentRouteNamed('setting-alert-custom.edit') ? 'active' : '' }}">カスタマイズアラート</a>

                <a href="{{route('setting-group-user.index')}}" class="nav-link-dropdown {{Route::currentRouteNamed('setting-group-user.index') ? 'active' : ''}}">グループ一覧管理</a>
            </div>
        </div>

        <a href="{{route('department.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('department.index') || Route::currentRouteNamed('department.edit') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.1 4.15002V6H9.62C8.00999 6 7.20001 6.80998 7.20001 8.41998V18H4.65002C3.22002 18 2.5 17.28 2.5 15.85V4.15002C2.5 2.72002 3.22002 2 4.65002 2H8.95001C10.38 2 11.1 2.72002 11.1 4.15002Z" fill="#BFBFBF"/>
                    <path opacity="0.4" d="M17.8702 8.41998V19.58C17.8702 21.19 17.0702 22 15.4602 22H9.62018C8.01018 22 7.2002 21.19 7.2002 19.58V8.41998C7.2002 6.80998 8.01018 6 9.62018 6H15.4602C17.0702 6 17.8702 6.80998 17.8702 8.41998Z" fill="#BFBFBF"/>
                    <path d="M22.5004 4.15002V15.85C22.5004 17.28 21.7803 18 20.3503 18H17.8704V8.41998C17.8704 6.80998 17.0704 6 15.4604 6H13.9004V4.15002C13.9004 2.72002 14.6204 2 16.0504 2H20.3503C21.7803 2 22.5004 2.72002 22.5004 4.15002Z" fill="#BFBFBF"/>
                    <path d="M14.5 11.75H10.5C10.09 11.75 9.75 11.41 9.75 11C9.75 10.59 10.09 10.25 10.5 10.25H14.5C14.91 10.25 15.25 10.59 15.25 11C15.25 11.41 14.91 11.75 14.5 11.75Z" fill="#BFBFBF"/>
                    <path d="M14.5 14.75H10.5C10.09 14.75 9.75 14.41 9.75 14C9.75 13.59 10.09 13.25 10.5 13.25H14.5C14.91 13.25 15.25 13.59 15.25 14C15.25 14.41 14.91 14.75 14.5 14.75Z" fill="#BFBFBF"/>
                    <path d="M13.25 19V22H11.75V19C11.75 18.59 12.09 18.25 12.5 18.25C12.91 18.25 13.25 18.59 13.25 19Z" fill="#BFBFBF"/>
                </svg>
            </i>
            支店管理
        </a>

        <a href="{{route('decentralization.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('decentralization.index') || Route::currentRouteNamed('detail-decentralization-facility.index') || Route::currentRouteNamed('detail-decentralization-user.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M11.4608 2.06008L5.96078 4.12008C4.91078 4.52008 4.05078 5.76008 4.05078 6.89008V14.9901C4.05078 15.8001 4.58078 16.8701 5.23078 17.3501L10.7308 21.4601C11.7008 22.1901 13.2908 22.1901 14.2608 21.4601L19.7608 17.3501C20.4108 16.8601 20.9408 15.8001 20.9408 14.9901V6.89008C20.9408 5.77008 20.0808 4.52008 19.0308 4.13008L13.5308 2.07008C12.9708 1.85008 12.0308 1.85008 11.4608 2.06008Z" fill="#BFBFBF"/>
                    <path d="M11.1602 14.2301C10.9702 14.2301 10.7802 14.1601 10.6302 14.0101L9.02023 12.4001C8.73023 12.1101 8.73023 11.6301 9.02023 11.3401C9.31023 11.0501 9.79023 11.0501 10.0802 11.3401L11.1602 12.4201L14.9302 8.65012C15.2202 8.36012 15.7002 8.36012 15.9902 8.65012C16.2802 8.94012 16.2802 9.42012 15.9902 9.71012L11.6902 14.0101C11.5402 14.1601 11.3502 14.2301 11.1602 14.2301Z" fill="#BFBFBF"/>
                </svg>
            </i>
            権限管理
        </a>

        <a href="{{route('day-off.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('day-off.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.5 1.25C8.91421 1.25 9.25 1.58579 9.25 2V5C9.25 5.41421 8.91421 5.75 8.5 5.75C8.08579 5.75 7.75 5.41421 7.75 5V2C7.75 1.58579 8.08579 1.25 8.5 1.25Z" fill="#BFBFBF"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M16.5 1.25C16.9142 1.25 17.25 1.58579 17.25 2V5C17.25 5.41421 16.9142 5.75 16.5 5.75C16.0858 5.75 15.75 5.41421 15.75 5V2C15.75 1.58579 16.0858 1.25 16.5 1.25Z" fill="#BFBFBF"/>
                    <path opacity="0.4" d="M22 8.37V17.13C22 17.29 21.99 17.45 21.98 17.6H3.02C3.01 17.45 3 17.29 3 17.13V8.37C3 5.68 5.18 3.5 7.87 3.5H17.13C19.82 3.5 22 5.68 22 8.37Z" fill="#BFBFBF"/>
                    <path d="M21.9834 17.6001C21.7434 20.0701 19.6634 22.0001 17.1334 22.0001H7.87344C5.34344 22.0001 3.26344 20.0701 3.02344 17.6001H21.9834Z" fill="#BFBFBF"/>
                    <path d="M14.03 11.6198C14.48 11.3098 14.76 10.8498 14.76 10.2298C14.76 8.92976 13.72 8.25977 12.5 8.25977C11.28 8.25977 10.23 8.92976 10.23 10.2298C10.23 10.8498 10.52 11.3198 10.96 11.6198C10.35 11.9798 10 12.5598 10 13.2398C10 14.4798 10.95 15.2498 12.5 15.2498C14.04 15.2498 15 14.4798 15 13.2398C15 12.5598 14.65 11.9698 14.03 11.6198ZM12.5 9.49977C13.02 9.49977 13.4 9.78977 13.4 10.2898C13.4 10.7798 13.02 11.0898 12.5 11.0898C11.98 11.0898 11.6 10.7798 11.6 10.2898C11.6 9.78977 11.98 9.49977 12.5 9.49977ZM12.5 13.9998C11.84 13.9998 11.36 13.6698 11.36 13.0698C11.36 12.4698 11.84 12.1498 12.5 12.1498C13.16 12.1498 13.64 12.4798 13.64 13.0698C13.64 13.6698 13.16 13.9998 12.5 13.9998Z" fill="#BFBFBF"/>
                </svg>
            </i>
            日祝・特定判定マスタ管理
        </a>

        <a href="{{route('master-code.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('master-code.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M16.69 2H8.31C4.67 2 2.5 4.17 2.5 7.81V16.18C2.5 19.83 4.67 22 8.31 22H16.68C20.32 22 22.49 19.83 22.49 16.19V7.81C22.5 4.17 20.33 2 16.69 2Z" fill="#BFBFBF"/>
                    <path d="M10.1031 15.7799C9.91313 15.7799 9.72312 15.7099 9.57312 15.5599L7.08312 13.0699C6.49313 12.4799 6.49313 11.5299 7.08312 10.9399L9.57312 8.44992C9.86312 8.15992 10.3431 8.15992 10.6331 8.44992C10.9231 8.73992 10.9231 9.21992 10.6331 9.50992L8.14312 11.9999L10.6331 14.4999C10.9231 14.7899 10.9231 15.2699 10.6331 15.5599C10.4831 15.6999 10.2931 15.7799 10.1031 15.7799Z" fill="#BFBFBF"/>
                    <path d="M14.9037 15.7802C14.7137 15.7802 14.5238 15.7102 14.3738 15.5602C14.0838 15.2702 14.0838 14.7902 14.3738 14.5002L16.8638 12.0002L14.3738 9.50016C14.0838 9.21016 14.0838 8.73016 14.3738 8.44016C14.6637 8.15016 15.1438 8.15016 15.4338 8.44016L17.9238 10.9302C18.5138 11.5202 18.5138 12.4702 17.9238 13.0602L15.4338 15.5502C15.2938 15.7002 15.0937 15.7802 14.9037 15.7802Z" fill="#BFBFBF"/>
                </svg>
            </i>
            おまかせコード管理
        </a>

        <div class="nav-sidebar-dropdown">
            <div id="side-bar-alert-file-in-insurance" class="nav-link nav-sidebar nav-sidebar-heading">
                <i class="icon-sidebar">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.4" d="M18.8281 5.67L7.08813 17.41C6.64813 17.85 5.90813 17.79 5.54813 17.27C4.30813 15.46 3.57812 13.32 3.57812 11.12V6.73C3.57812 5.91 4.19813 4.98 4.95813 4.67L10.5281 2.39C11.7881 1.87 13.1882 1.87 14.4482 2.39L18.4881 4.03999C19.1581 4.30999 19.3281 5.17 18.8281 5.67Z" fill="#BFBFBF"/>
                        <path d="M19.7739 7.03963C20.4239 6.48963 21.4139 6.95962 21.4139 7.80962V11.1196C21.4139 16.0096 17.8639 20.5896 13.0139 21.9296C12.6839 22.0196 12.3239 22.0196 11.9839 21.9296C10.5639 21.5296 9.2439 20.8596 8.1139 19.9796C7.6339 19.6096 7.58392 18.9096 8.00392 18.4796C10.1839 16.2496 16.5639 9.74963 19.7739 7.03963Z" fill="#BFBFBF"/>
                    </svg>
                </i>
                保険業務区分管理
                <i class="icon-polygon ml-auto">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.46967 17.5303C9.17678 17.2374 9.17678 16.7626 9.46967 16.4697L13.9393 12L9.46967 7.53033C9.17678 7.23744 9.17678 6.76256 9.46967 6.46967C9.76256 6.17678 10.2374 6.17678 10.5303 6.46967L15.5303 11.4697C15.671 11.6103 15.75 11.8011 15.75 12C15.75 12.1989 15.671 12.3897 15.5303 12.5303L10.5303 17.5303C10.2374 17.8232 9.76256 17.8232 9.46967 17.5303Z" fill="#BFBFBF"/>
                    </svg>
                </i>
                <script>
                    $(window).on('load', function () {
                            @if(Route::currentRouteNamed('file-in-insurance.index') || Route::currentRouteNamed('file-outside-insurance.index'))
                                $('#side-bar-alert-file-in-insurance').click();
                            @endif
                        });
                </script>
            </div>
            <div class="nav-sidebar-body">
                <a href="{{route('file-in-insurance.index')}}" class="nav-link-dropdown nav-link-flex {{Route::currentRouteNamed('file-in-insurance.store') || Route::currentRouteNamed('file-in-insurance.index') || Route::currentRouteNamed('file-in-insurance.update') ? 'active' : ''}}">保険内中心業務区分</a>
            </div>
            <div class="nav-sidebar-body">
                <a href="{{route('file-outside-insurance.index')}}" class="nav-link-dropdown nav-link-flex {{Route::currentRouteNamed('file-outside-insurance.store') || Route::currentRouteNamed('file-outside-insurance.index') || Route::currentRouteNamed('file-outside-insurance.update') ? 'active' : ''}}">保険外中心業務区分</a>
            </div>
        </div>

        <a href="{{route('template-mail.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('template-mail.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.5 8H4.5C3.4 8 2.5 7.1 2.5 6V4C2.5 2.9 3.4 2 4.5 2H7.5C8.6 2 9.5 2.9 9.5 4V6C9.5 7.1 8.6 8 7.5 8Z" fill="#BFBFBF"/>
                    <path d="M21.3 7H17.7C17.04 7 16.5 6.45999 16.5 5.79999V4.20001C16.5 3.54001 17.04 3 17.7 3H21.3C21.96 3 22.5 3.54001 22.5 4.20001V5.79999C22.5 6.45999 21.96 7 21.3 7Z" fill="#BFBFBF"/>
                    <path d="M21.3 14.5H17.7C17.04 14.5 16.5 13.96 16.5 13.3V11.7C16.5 11.04 17.04 10.5 17.7 10.5H21.3C21.96 10.5 22.5 11.04 22.5 11.7V13.3C22.5 13.96 21.96 14.5 21.3 14.5Z" fill="#BFBFBF"/>
                    <path opacity="0.37" d="M16.5 13.25C16.91 13.25 17.25 12.91 17.25 12.5C17.25 12.09 16.91 11.75 16.5 11.75H13.75V5.75H16.5C16.91 5.75 17.25 5.41 17.25 5C17.25 4.59 16.91 4.25 16.5 4.25H9.5C9.09 4.25 8.75 4.59 8.75 5C8.75 5.41 9.09 5.75 9.5 5.75H12.25V18C12.25 19.52 13.48 20.75 15 20.75H16.5C16.91 20.75 17.25 20.41 17.25 20C17.25 19.59 16.91 19.25 16.5 19.25H15C14.31 19.25 13.75 18.69 13.75 18V13.25H16.5Z" fill="#BFBFBF"/>
                    <path d="M21.3 22H17.7C17.04 22 16.5 21.46 16.5 20.8V19.2C16.5 18.54 17.04 18 17.7 18H21.3C21.96 18 22.5 18.54 22.5 19.2V20.8C22.5 21.46 21.96 22 21.3 22Z" fill="#BFBFBF"/>
                </svg>
            </i>
            メールテンプレート管理
        </a>

        <a href="{{route('download-file.index')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('download-file.index') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M17.3 9H7.7C4.5 9 2.5 11 2.5 14.2V16.79C2.5 20 4.5 22 7.7 22H17.29C20.49 22 22.49 20 22.49 16.8V14.2C22.5 11 20.5 9 17.3 9Z" fill="#BFBFBF"/>
                    <path d="M16.3838 5.57043L13.0338 2.22043C12.7438 1.93043 12.2638 1.93043 11.9738 2.22043L8.62375 5.57043C8.33375 5.86043 8.33375 6.34043 8.62375 6.63043C8.91375 6.92043 9.39375 6.92043 9.68375 6.63043L11.7538 4.56043V15.2504C11.7538 15.6604 12.0938 16.0004 12.5038 16.0004C12.9138 16.0004 13.2538 15.6604 13.2538 15.2504V4.56043L15.3238 6.63043C15.4738 6.78043 15.6638 6.85043 15.8538 6.85043C16.0438 6.85043 16.2338 6.78043 16.3838 6.63043C16.6838 6.34043 16.6838 5.87043 16.3838 5.57043Z" fill="#BFBFBF"/>
                </svg>
            </i>
            各種ファイルダウンロード管理
        </a>
        <a href="{{route('template')}}" class="nav-link nav-sidebar {{ Route::currentRouteNamed('template') ? 'active' : '' }}">
            <i class="icon-sidebar">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M17.3 9H7.7C4.5 9 2.5 11 2.5 14.2V16.79C2.5 20 4.5 22 7.7 22H17.29C20.49 22 22.49 20 22.49 16.8V14.2C22.5 11 20.5 9 17.3 9Z" fill="#BFBFBF"/>
                    <path d="M16.3838 5.57043L13.0338 2.22043C12.7438 1.93043 12.2638 1.93043 11.9738 2.22043L8.62375 5.57043C8.33375 5.86043 8.33375 6.34043 8.62375 6.63043C8.91375 6.92043 9.39375 6.92043 9.68375 6.63043L11.7538 4.56043V15.2504C11.7538 15.6604 12.0938 16.0004 12.5038 16.0004C12.9138 16.0004 13.2538 15.6604 13.2538 15.2504V4.56043L15.3238 6.63043C15.4738 6.78043 15.6638 6.85043 15.8538 6.85043C16.0438 6.85043 16.2338 6.78043 16.3838 6.63043C16.6838 6.34043 16.6838 5.87043 16.3838 5.57043Z" fill="#BFBFBF"/>
                </svg>
            </i>
            請求締めチェックツール
        </a>
    </div>
</section>
