<div class="modal fade" id="modalUpload2Comment" tabindex="-1" role="dialog" aria-labelledby="modalUpload2CommentTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <form id="file-comment-upload-form"  action="{{route('check-1.upload-file-comment')}}"
                          method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                        @csrf
                        <input type="hidden" name="facilityId" value="{{$data['facility']->id}}">
                       <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept=".xls, .xlsx"/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                </div>
                        </div>
                        <div id="listFiles"></div>
                        <div id="listFileError" class="d-none">
                        </div>
                        <div id="text-file-pass" class="d-none">
                            アップロードファイル一覧
                        </div>
                        <div class="list-file-pass d-none">
                            <div id="list-file-pass" class="d-none"></div>
                        </div>
                        <div class="file-uploaded row">
                            <div class="col-12 text-left file-sample" id="textSample">
                                各チェックのアップロード可能ファイリ名称サンプル
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                未収金明細
                            </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/uploadType1Comment.js') }}"></script>

<script>
    uploadType1Comment('#modalUpload2Comment');
    $("#file-comment-upload-form").submit(function (event) {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        thisModal.find("#listFileError").empty();
        thisModal.find("#listFileError").addClass('d-none');
        thisModal.find("#list-file-pass").empty();
        thisModal.find(('#list-file-pass')).addClass('d-none');
        thisModal.find("#error").empty();
        thisModal.find(('#error')).addClass('d-none');
        let fileNameRule = '未収金明細';
        var filePass=[];
        var fileFail =[];
        var file = thisModal.find('#file-upload')[0].files[0];
        if (file.name.includes(fileNameRule)) {
            filePass.push(file.name);
        } else {
            fileFail.push(file.name);
        }
        if (!fileFail.length) {
            validate = true;
        } else {
            thisModal.find('#listFiles').empty();
            thisModal.find("#listFileError").empty();
            thisModal.find(('text-file-pass')).addClass('d-none')
            validate = false;
            var pass = '';
            $.each(filePass, function (key, value) {
                if (value) {
                    pass += "<div class='file-pass'>" +
                        "<div class='file-response file-response-2 file-name-pass'>" +
                        "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                        "<div class='messages-file'>" +
                        "<strong>" + value + "</strong>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                }
            });
            var html = '';
            $.each(fileFail, function (key, value) {
                if (value) {
                    html += "<div class='item-error-file row'>" +
                        "<div class='col-12 file-response item-error'>" +
                        "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                        "<div class='messages-file'><strong>" + value + "</strong></div>" +
                        "</div>" +
                        "<div class='col-12 file-response item-error-message'>" +
                        "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                        "<div class='messages-file'>正しくファイル名称をアップロードしてください。</div>" +
                        "</div>" +
                        "</div>"
                }
            })
            thisModal.find("#listFileError").append(html);
            thisModal.find("#list-file-pass").append(pass);
            thisModal.find("#list-file-pass").removeClass('d-none');
            thisModal.find("#listFileError").removeClass('d-none');
            $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
            $("#preloader").remove();
        }
        if (validate) {
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                var file = '';
                $.each(data.data.files, function (key, value) {
                    file += "<p class='file-name'>" + value['name'] + '</p>'
                })
                $('#modalSuccess #file-name').empty();
                $('#modalSuccess .content').prepend(file)
                $('#modalSuccess').modal('show');
                thisModal.modal('hide');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                thisModal.find("#error").text("システムのエラー。")
                thisModal.find(('#error')).removeClass('d-none');
                $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
                $("#preloader").remove();
            });
        }
    });
</script>
