<div class="modal fade" id="modalUploadID8" tabindex="-1" role="dialog" aria-labelledby="modalUploadID8Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close" onclick="hideError($('#modalUploadID8'))">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <form id="file-upload-form-no8"  action="{{ isset($RouterCheckFile['4']) ? route($RouterCheckFile['4']) : ''}}"
                          method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                        <input type="hidden" name="facilityId" value="{{$data['facility']->id}}">
                        <input type="hidden" name="month" value="{{ !empty($requestDatas) && isset($requestDatas['setting_month'])? $requestDatas['setting_month'] : date('Y/m')}}">
                        <input type="hidden" name="type" value="4">
                        <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept=".xls, .xlsx, .csv"/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                </div>
                        </div>
                        <div id="listFiles"></div>
                        <div id="listFileError" class="d-none">
{{--                            <div class='item-error-file row'>--}}
{{--                                <div class='col-12 file-response item-error'>--}}
{{--                                    <img class='icon-upload' src='{{ asset('icons/document-upload-error.svg') }}' alt='aa'>--}}
{{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
{{--                                </div>--}}
{{--                                <div class='col-12 file-response item-error-message'>--}}
{{--                                    <img class='icon-upload' src='{{ asset('icons/danger.svg') }}' alt='aa'>--}}
{{--                                    <div class='messages-file'><strong>error</strong></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="item-error-file row">--}}
{{--                                <div class="col-12 file-response item-error">--}}
{{--                                    <img class="icon-upload" src="{{ asset('icons/document-upload-error.svg') }}" alt="aa">--}}
{{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
{{--                                </div>--}}
{{--                                <div class="col-12 file-response item-error-message">--}}
{{--                                    <img class="icon-upload" src="{{ asset('icons/danger.svg') }}" alt="aa">--}}
{{--                                    <div class='messages-file'><strong>error</strong></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
{{--                        <div id="text-file-pass" class="d-none">--}}
{{--                            アップロードファイル一覧--}}
{{--                        </div>--}}
{{--                        <div class="list-file-pass d-none">--}}
{{--                            <div id="list-file-pass" class="d-none"></div>--}}
{{--                        </div>--}}
                        <div class="file-uploaded row">
                            <div class="col-12 text-left file-sample" id="textSample">
                                各チェックのアップロード可能ファイリ名称サンプル
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                おまかせチェック ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                おまかせ情報ダウンロード_年月日
                            </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
