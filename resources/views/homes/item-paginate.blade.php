@php
$files = $dataTypeCheckDetail[$value]['files'];
$typeCheck =  $dataTypeCheckDetail[$value]['typeCheck'];
@endphp
@foreach($files as $file)
    <div class="detail-check-block">
        <div class="inner-check-block">
            <div class="left-check-block">
                <div class="context-check-block">更新日: <span class="gray-date">{{date('Y/m/d', strtotime($file->created_at))}} - {{date('H:i', strtotime($file->created_at))}} </span></div>
                <div class="btn-right-context {{ $file->status == \App\Consts::STATUS_CHECK_FILE['success'] ? 'btn-green-context' :
                                                        ($file->status == \App\Consts::STATUS_CHECK_FILE['error'] ? 'btn-mint-context': 'btn-blue-context' )}}">
                    <img src="{{ $file->status == \App\Consts::STATUS_CHECK_FILE['success'] ? asset('icons/verify.svg') :
                                                        ($file->status == \App\Consts::STATUS_CHECK_FILE['error'] ? asset('icons/forbidden.svg') : asset('icons/timer.svg'))}}" alt="verify">
                    {{\App\Consts::STATUS_CHECK[$file->status]}}
                </div>
            </div>
{{--            <a href="{{$file->filepath}}" download class="right-check-block">--}}
{{--                <div class="gray-text-check">ダウンロード</div>--}}
{{--                <span class="btn-download-check">--}}
{{--                                    <img class="" src="{{ asset('icons/download.svg') }}" alt="download">--}}
{{--                                </span>--}}
{{--            </a>--}}
        </div>
        <div class="inner-check-block">
            <div class="left-check-block">
                @php
                    $fileName = $file->file_name;
                    $errorCount = $file->errors_count;
                    $errors = $file->errors;
                    if( $typeCheck->code_check == App\Consts::TYPE_CHECK_UPLOAD['3'] && $file->file){
                        $fileName = $fileName. ', ' .$file->file->file_name;
                        $errorCount = $errorCount + $file->file->errors_count;
                        $errors =$file->errors->merge($file->file->errors);
                    }
                @endphp
                <div class="context-check-block">ファイル名: <span class="mint-file-check">{{$fileName}} </span></div>
            </div>
        </div>
        <div class="inner-check-block">
            <div class="left-check-block">
                <div class="context-check-block">エラー数: <span class="{{$errorCount ? 'red-number-check' : 'green-number-check'}}">{{$errorCount}}</span></div>
            </div>
        </div>
        <ul class="list-error-file">
            @foreach ($errors as $error)
                <li class="item-error-file">
                    {{$error->error_position}}{{$error->error_code != ' ' ? '：' . \App\Consts::ERROR_CHECK_CODE[$error->error_code] : '' }}
                </li>
            @endforeach
        </ul>
        @if($file->status == \App\Consts::STATUS_CHECK_FILE['hold'])
            <div class="reason-check-block mt-20">
                <div class="context-check-block">保留理由</div>
                <div class="text-reason-check">
            {!! nl2br(e($typeCheck->reason))!!}
</div>
</div>
@endif
</div>
@endforeach
@if( $files && $files->nextPageUrl())
<!-- button load more -->
<div class="wrapper-button-loadmore pagination_loadmore" id="pagination">
    <a href="{{$files->nextPageUrl()}}" class="button-loadmore">
    <img class="mr-10" src="{{ asset('icons/add-square.svg') }}" alt="add-square">
    もっと見る
    </a>
</div>
@endif
