<style>
    .title-all {
        font-family: "Noto Sans JP", sans-serif;
        font-weight: 400;
        font-size: 14px;
        color: #00285A;
    }
    .file-name-all {
        font-family: "Noto Sans JP", sans-serif;
        font-weight: 700;
        font-size: 14px;
        color: #222222;
    }
    .function {
        margin-bottom: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed #B8B8B8;
    }
    ul li {
        letter-spacing: -0.02em;
    }
    .sample, .sample-first {
        font-weight: 400;
        font-size: 14px;
        color: #7C9AA1;
    }
    .sample {
        padding-top: 10px;
    }
</style>

<div class="modal fade" id="modalUploadAll" tabindex="-1" role="dialog" aria-labelledby="modalUploadAllTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close" onclick="hideError($('#modalUploadAll'))">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <form id="file-upload-form-all"  action="{{ route('check-all') }}"
                          method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                        <input type="hidden" name="facilityId" value="{{$data['facility']->id}}">
                        <input type="hidden" name="month" value="{{ !empty($requestDatas) && isset($requestDatas['setting_month'])? $requestDatas['setting_month'] : date('Y/m')}}">
                        <div class="multiple-file-upload">
                            <div class="item-file-upload">
                                <input id="file-upload" type="file" name="file[]" accept=".xls, .xlsx, .csv" multiple/>
                                <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                    <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                    <div id="start" class="file-start">
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                        <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div id="listFiles"></div>
                        <div id="listFileError" class="d-none">
                        </div>
                        <div id="text-file-pass" class="d-none">
                            アップロードファイル一覧
                        </div>
                        <div class="list-file-pass d-none" style="padding-top: 10px">
                            <div id="list-file-pass" class="d-none"></div>
                        </div>
                        <div class="col-12 text-left file-sample row" id="textSample">
                            各チェックのアップロード可能ファイリ名称サンプル
                        </div>
                        <ul class="text-left pt-15">
                            <li class="function">
                                <div class="title-all">
                                    ①請求前確認リスト:
                                </div>
                                <div class="file-name-all">
                                    請求前確認リスト
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ②請求締めチェックツール:
                                </div>
                                <div class="sample-first">
                                    未収金明細表 ファイル:
                                </div>
                                <div class="file-name-all">
                                    未収金明細表
                                </div>
                                <div class="sample">
                                    請求計算確認表 ファイル:
                                </div>
                                <div class="file-name-all">
                                    請求計算確認表
                                </div>
                                <div class="sample">
                                    請求計算確認表（介護保険外) ファイル:
                                </div>
                                <div class="file-name-all">
                                    請求計算確認表(介護保険外)
                                </div>
                                <div class="sample">
                                    送り出し指示データ一覧 ファイル:
                                </div>
                                <div class="file-name-all">
                                    送り出し指示データ一覧
                                </div>
                                <div class="sample">
                                    総合出力 ファイル:
                                </div>
                                <div class="file-name-all">
                                    総合出力
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ③送り出し指示データ一覧:
                                </div>
                                <div class="file-name-all">
                                    送り出し指示データ一覧-年月日
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ④総合事業請求止めチェック機能:
                                </div>
                                <div class="file-name-all">
                                    予実明細
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑤総合事業計上漏れチェック:
                                </div>
                                <div class="sample-first">
                                    請求計算確認表 ファイル:
                                </div>
                                <div class="file-name-all">
                                    請求計算確認表
                                </div>
                                <div class="sample">
                                    Sファイル:
                                </div>
                                <div class="file-name-all">
                                    XXXXsXXX
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑥障害チェック
                                </div>
                                <div class="sample">
                                    CSVファイル:
                                </div>
                                <div class="file-name-all">
                                    TH01_年月
                                </div>
                                <div class="sample">
                                    Excel ファイル:
                                </div>
                                <div class="file-name-all">
                                    売上一覧表_保険外
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑦トランデータチェック:
                                </div>
                                <div class="file-name-all">
                                    トランチェックXXXXXXXXXX-年月日-
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑧おまかせチェック:
                                </div>
                                <div class="file-name-all">
                                    おまかせ情報ダウンロード_年月日
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑨Mファイルチェック:
                                </div>
                                <div class="file-name-all">
                                    XXXXmXXX
                                </div>
                                <div class="file-name-all">
                                    請求計算確認表
                                </div>
                            </li>
                            <li class="function">
                                <div class="title-all">
                                    ⑩Sファイルチェック:
                                </div>
                                <div class="file-name-all">
                                    XXXXsXXX
                                </div>
                            </li>
                            <li class="function" style="border-bottom: none">
                                <div class="title-all">
                                    ⑪TH障害データチェック:
                                </div>
                                <div class="file-name-all">
                                    TH01_年月
                                </div>
                            </li>
                        </ul>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAllSuccess" tabindex="-1" role="dialog" aria-labelledby="modalAllSuccess"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 id="all-success-title" class="modal-title home-modal-title">アップロードファイル</h5>
                <button onclick="location.reload()" type="button" class="close home-modal-close"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <div class="delete-office">
                    <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                    <p id="all-success-text" class="modal-text">アップロード済みです。システムはチェック処理を行います。</p>
                    <button type="button" onclick="location.reload()" class="home-modal-button default-button">閉じる
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalAllQuestion" tabindex="-1" role="dialog" aria-labelledby="modalQuestionTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle"> 一括チェックのキャンセル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <form id="delete-job"  action="{{ route('delete-job') }}"
                      method="post" enctype="multipart/form-data" class="" style="max-width: 430px; margin: 0 auto">
                    @csrf
                    <input type="hidden" name="settingMonth"
                           value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                    <input type="hidden" name="facilityId" value="{{($data['facility'] && $data['facility']->id)? $data['facility']->id : ''}}">
                    <input type="hidden" name="month" value="{{ !empty($requestDatas) && isset($requestDatas['setting_month'])? $requestDatas['setting_month'] : date('Y/m')}}">
                    <input type="hidden" name="job_id" value="{{ $data['setting_month']->job_id }}">
                    <div id="system-error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text">一括チェックをキャンセルしますか？</p>
                    <div id="job-error" class="text-danger font-weight-bold" style="padding-top: 5px;"></div>
                    <div class="row pt-30">
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="default-button w-100" style="background: #B5E5F0 !important; color: #0F94B5 !important;">いいえ</button>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="default-button w-100">はい</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function hideError(thisModal) {
        thisModal.find("#listFileError").addClass('d-none')
        thisModal.find("#text-file-pass").addClass('d-none')
        thisModal.find('.list-file-pass').addClass('d-none');
        thisModal.find('#list-file-pass').addClass('d-none');
    }
</script>

