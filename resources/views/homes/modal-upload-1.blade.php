<style>
    #modalUploadID2 .file-sample {
        padding-bottom: 0;
    }
    #modalUploadID2 #text-file-pass {
        padding-top: 0;
    }
</style>
<div class="modal fade" id="modalUploadID2" tabindex="-1" role="dialog" aria-labelledby="modalUploadID2Title"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <form id="file-upload-form-no2"
                          action="{{ isset($RouterCheckFile['1']) ? route($RouterCheckFile['1']) : ''}}"
                          method="post" enctype="multipart/form-data" class="file-upload-form-no outer-modal-single">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                        <input type="hidden" name="facilityId" value="{{$data['facility']->id}}">
                        <input type="hidden" name="month"
                               value="{{ !empty($requestDatas) && isset($requestDatas['setting_month'])? $requestDatas['setting_month'] : date('Y/m')}}">
                        <input type="hidden" name="type" value="2">
                        <div class="multiple-file-upload">
                            <div class="item-file-upload">
                                <input id="file-upload" type="file" name="file[]" accept=".xls, .xlsx, .csv" multiple/>
                                <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag"
                                       class="modal-body file-upload" style="margin-bottom: 20px">
                                    <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                    <div id="start" class="file-start">
                                        <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                        <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                        <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div id="listFileError" class="d-none">
                        </div>
                        <div id="list-file-pass" class="d-none"></div>
                        <div id="listFiles"></div>
                        <div class="file-uploaded row">
                            <div class="col-12 text-left file-sample mb-15" id="textSample">
                                各チェックのアップロード可能ファイリ名称サンプル<br/>
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                未収金明細表 ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                未収金明細表
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                請求計算確認表 ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                請求計算確認表
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                請求計算確認表（介護保険外) ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                請求計算確認表(介護保険外)
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                送り出し指示データ一覧 ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                送り出し指示データ一覧
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                総合出力 ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                総合出力
                            </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button"
                                id="file-upload-form-button-no-2" disabled>チェック
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
