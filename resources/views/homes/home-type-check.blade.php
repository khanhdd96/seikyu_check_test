@php
    $facilityListTypeChecks = explode(',', $data['facility']->list_type_check);
    $type_menu = App\Consts::TYPE_MENU;
    $titles = App\Consts::TITLE;
    $titleNumber = App\Consts::TITLE_NUMBER;
    $statusCheck = App\Consts::STATUS_CHECK;
    $statusCheckIon = App\Consts::STATUS_CHECK_ICON;
    $classStatusCheck = App\Consts::CLASS_STATUS_CHECK;
    $statusCheckFile = App\Consts::STATUS_CHECK_FILE;
    $facility_id =  $data['facility']->id;
@endphp
<style>
    .item-upload-type-2 a.wrapper-upload-btn {
        margin: 0 0 0 11px!important;
    }
    .item-upload-type-2 p {
        color: #B8B8B8;
        font-size: 10px;
        white-space: nowrap;
    }
    .item-upload-type-2 .notice-management-block {
        margin-right: 5px;
        white-space: nowrap;
    }
    .item-upload-type-2 .wrapper-upload-btn {
        margin-left: 5px;
    }
    @media (max-width: 1450px) {
        .item-upload-type-2 .action-management-block {
            display: block;
            text-align: right;
        }
    }
    .right-context a {
        vertical-align: center;
        font-weight: 700;
        size: 14px;
        letter-spacing: -0.02em;
        color: #0F94B5;
        text-decoration: underline;
        cursor: pointer;
    }
    .right-context a:hover {
        color: #0F94B5;
        text-decoration: none;

    }
</style>
<div class="row">
    <div class="col-lg-6">
    @foreach(\App\Consts::TYPE_MENU as $key => $value)
        <!-- block has button switches toggle -->
            @if($key <= 6)
                <div style="position: relative"
                     class="col-management {{ !in_array($type_menu[$key], $facilityListTypeChecks) ? 'hidden-type-check': ''}} ">
                    @if(!in_array(App\Consts::TYPE_MENU[$key], App\Consts::CNC_OPEN))
                        <div style="
                                    width: 100%;
                                    height: 100%;
                                    display: inline-block;
                                    position: absolute;
                                    background-color: #f5ebeb7d;
                                    z-index: 111;
                                "></div>
                    @endif
                    <article class="management-block @if($key == 2) min-height-275 @endif">
                        <div class="inner-management-block">
                            <h4 class="title-management-block"><a
                                    href="{{ isset(App\Consts::ROUTE_CHECK_FILE[$value]) ? asset('file_check/'.$value.'?department_id='.request('department_id').'&facility_id='. request('facility_id') ).'&month='. urlencode(request('setting_month')) : ''}}">{{$titleNumber[$key]}} {{$titles[$key]}} </a>
                            </h4>
                            <div class="action-management-block">
                                @if($key == 1)
                                    <small class="notice-management-block">チェックファイル無し</small>
                                    <!-- Button toggle Modal A1000.1 Home -->
                                    <button type="button" id="wrapper-toggle-btn" class="wrapper-toggle-btn"  data-toggle="modal" data-type="7"
                                            data-target="#modalQuestion" data-backdrop="static" data-keyboard="false">
                                        <input type="checkbox" id="toggle-input" class="toggle-input">
                                        <div id="toggle-btn" class="toggle-btn"></div>
                                    </button>
                                @endif
                                @if($key == 2)
                                    <div class="action-management-block">
                                        <small class="notice-management-block">未収金明細</small>
                                        <!-- Button toggle Modal A1000.1 Home -->
                                        <button type="button" id="wrapper-toggle-btn" class="wrapper-toggle-btn-no" data-toggle="modal" data-type="1"
                                                data-target="" data-backdrop="static"
                                                data-keyboard="false">
                                            <input type="checkbox" id="toggle-input" class="toggle-input">
                                            <div id="toggle-btn" class="toggle-btn"></div>
                                        </button>
                                    </div>
                                @endif
                            <!-- Button upload file: data-target=ID -->
                                {{--@if($data['canUpload']) --}}
                                @if($data['setting_month'] && ($data['setting_month']->status_facilitity !=  App\Consts::STATUS_CHECK_FILE['success']) && $data['setting_month']->can_upload == 1)
                                        <button type="button" class="wrapper-upload-btn @if($data['setting_month']->is_checking) popover-context @endif"
                                                @if($data['setting_month']->is_checking)
                                                data-content="ファイルアップロードの処理が行われています。
                                                           少々お待ちください。"
                                                rel="popover" data-placement="left" data-original-title="通知する"
                                                data-trigger="manual"
                                                @else
                                                data-toggle="modal"
                                                data-target="#modalUploadID{{$key}}" data-backdrop="static"
                                                data-keyboard="false"
                                            @endif>
                                            <img src="{{ asset('icons/export.svg') }}" alt="export" @if($data['setting_month']->is_checking) style="cursor: pointer; filter:grayscale(1)"@endif>
                                        </button>
                            @endif
                            {{-- @endif --}}
                            <!-- Redirect page history -->
                                <button type="button" class="wrapper-upload-btn" onclick="loadDetail('{{$key}}')">
                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                </button>
                            </div>
                        </div>
                        @if($key == 2)
                            <div class="item-upload-type-2 mt-23 mb-15 d-flex justify-content-between">
                                <div class="d-flex align-items-center justify-content-start">
                                    <p>未収金明細</p>
                                    <a download
                                       href="{{isset($data['typeCheck1Admin']) && $data['typeCheck1Admin']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] ? ($data['typeCheck1Admin']->url_download_file_mid_month ?? 'javascript:void(0)') : ((isset($data['typeCheck1Admin']) && $data['typeCheck1Admin']->url_download_file_early_month && $data['typeCheck1Admin']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']) ? $data['typeCheck1Admin']->url_download_file_early_month : 'javascript:void(0)')}}"
                                       class="wrapper-upload-btn">
                                        <img src="{{ asset('icons/down-1.svg') }}" alt="export">
                                    </a>
                                </div>
                                <div class="d-flex align-items-center justify-content-center">
                                    <p>コメントファイルアップロード</p>
                                    <button type="button" {{!isset($data['typeCheck1Admin']) || (isset($data['typeCheck1Admin']) && ($data['typeCheck1Admin']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] || ($data['typeCheck1Admin']->step > \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] && $data['typeCheck1Admin']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']))) ? 'disabled' : ''}} class="wrapper-upload-btn" data-toggle="modal"
                                            data-target="#modalUpload2Comment" data-backdrop="static"
                                            data-keyboard="false">
                                        <img src="{{ asset('icons/export.svg') }}" alt="export">
                                    </button>
                                </div>
                                <div class="d-flex align-items-center justify-content-end">
                                    <p>返事ファイル</p>
                                    <a download
                                       href="{{isset($data['typeCheck1Admin']) && $data['typeCheck1Admin']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'] ? ($data['typeCheck1Admin']->url_download_file_mid_month_after_comment ?? 'javascript:void(0)') : ((isset($data['typeCheck1Admin']) && $data['typeCheck1Admin']->url_download_file_early_month_after_comment && $data['typeCheck1Admin']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH']) ? $data['typeCheck1Admin']->url_download_file_early_month_after_comment : 'javascript:void(0)')}}"
                                       class="wrapper-upload-btn">
                                        <img src="{{ asset('icons/down-1.svg') }}" alt="export">
                                    </a>
                                </div>
                            </div>
                        @endif
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">状態</div>
                            <div class="right-context d-flex" style="align-items: center">
                                @if(isset($data['type_checks'][$value]))
                                    @if($data['type_checks'][$value]->status == 2 && !$data['setting_month']->is_checking)
                                        <div class="pr-10">
                                            <a id="{{ $data['type_checks'][$value]->id }}" class="reserve">保留</a>
                                        </div>
                                    @endif
                                    <button type="button"
                                            class="btn-right-context {{$classStatusCheck[$data['type_checks'][$value]->status]}}"
                                            data-content="{{ nl2br($data['type_checks'][$value]->reason) }}"
                                            rel="popover" data-placement="left" data-original-title="保留理由"
                                            data-trigger="manual">
                                        <img src="{{ asset($statusCheckIon[$data['type_checks'][$value]->status]) }}"
                                             alt="close">
                                        {{$statusCheck[$data['type_checks'][$value]->status]}}
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">最終チェック日時</div>
                            <div class="right-context">
                                <p class="text-right-context">{{isset($data['type_checks'][$value]) && isset($data['type_checks'][$value]->files[0]) ?  date('Y/m/d - H:i', strtotime($data['type_checks'][$value]->files[0]->created_at)) : 'N/A'}}</p>
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">チェックの回数</div>
                            @php
                                $dataCount = '0';
                                if( isset($data['type_checks'][$value]) ) {
                                    $dataCount = $data['type_checks'][$value]->file_not_hold_count;
                                    if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                        $dataCount = $dataCount / 2;
                                    }
                                    if ($value == '2' || $value == '1') {
                                        $dataCount = $data['type_checks'][$value]->check_times_count;
                                    }
                                }
                            @endphp
                            <div class="right-context">
                                <p class="text-right-context">{{$dataCount}}</p>
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">エラーの発生回数</div>
                            <div class="right-context">
                                @php
                                    $dataCount = '0';
                                    if( isset($data['type_checks'][$value]) ) {
                                        $dataCount = $data['type_checks'][$value]->file_error_count;
                                        if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                            $dataCount = $dataCount / 2;
                                        }
                                        if ($value == '2' || $value == '1') {
                                            $dataCount = $data['type_checks'][$value]->check_error_times_count;
                                        }
                                    }
                                @endphp
                                <p class="text-right-context">{{ $dataCount }}</p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- block has button switches toggle -->
            @endif
        @endforeach
    </div>
    <div class="col-lg-6">
    @foreach(\App\Consts::TYPE_MENU as $key => $value)
        <!-- block has button switches toggle -->
            @if($key > 6)
                <div style="position: relative"
                     class="col-management {{ !in_array($type_menu[$key], $facilityListTypeChecks) ? 'hidden-type-check': ''}} ">
                    @if(!in_array(App\Consts::TYPE_MENU[$key], App\Consts::CNC_OPEN))
                        <div style="
                                    width: 100%;
                                    height: 100%;
                                    display: inline-block;
                                    position: absolute;
                                    background-color: #f5ebeb7d;
                                    z-index: 111;
                                "></div>
                    @endif
                    <article class="management-block @if($key == 8) min-height-275 @endif">
                        <div class="inner-management-block">
                            <h4 class="title-management-block"><a
                                    href="{{ isset(App\Consts::ROUTE_CHECK_FILE[$value]) ? asset('file_check/'.$value.'?department_id='.request('department_id').'&facility_id='. request('facility_id') ).'&month='. urlencode(request('setting_month')) : ''}}">{{$titleNumber[$key]}} {{$titles[$key]}} </a>
                            </h4>
                            <div class="action-management-block">
                                <!-- Button upload file: data-target=ID -->

                                @if($data['setting_month'] && ($data['setting_month']->status_facilitity !=  App\Consts::STATUS_CHECK_FILE['success']) && $data['setting_month']->can_upload == 1)
                                    <button type="button" class="wrapper-upload-btn @if($data['setting_month']->is_checking) popover-context @endif"
                                            @if($data['setting_month']->is_checking)
                                            data-content="ファイルアップロードの処理が行われています。
                                                        少々お待ちください。"
                                            rel="popover" data-placement="left" data-original-title="通知する"
                                            data-trigger="manual"
                                            @else
                                            data-toggle="modal"
                                            data-target="#modalUploadID{{$key}}" data-backdrop="static"
                                            data-keyboard="false"
                                        @endif>
                                        <img src="{{ asset('icons/export.svg') }}" alt="export" @if($data['setting_month']->is_checking) style="cursor: pointer; filter:grayscale(1)"@endif>
                                    </button>
                            @endif
                            <!-- Redirect page history -->
                                <button type="button" class="wrapper-upload-btn" onclick="loadDetail('{{$key}}')">
                                    <img src="{{ asset('icons/history.svg') }}" alt="history">
                                </button>
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">状態</div>
                            <div class="right-context d-flex" style="align-items: center">
                                @if(isset($data['type_checks'][$value]))
                                    @if($data['type_checks'][$value]->status == 2 && !$data['setting_month']->is_checking)
                                        <div class="pr-10">
                                            <a id="{{ $data['type_checks'][$value]->id }}" class="reserve">保留</a>
                                        </div>
                                    @endif
                                    <button type="button"
                                            class="btn-right-context {{$classStatusCheck[$data['type_checks'][$value]->status]}}"
                                            data-content="{{ nl2br($data['type_checks'][$value]->reason) }}"
                                            rel="popover" data-placement="left" data-original-title="保留理由"
                                            data-trigger="manual">
                                        <img src="{{ asset($statusCheckIon[$data['type_checks'][$value]->status]) }}"
                                             alt="close">
                                        {{ $statusCheck[$data['type_checks'][$value]->status] }}
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">最終チェック日時</div>
                            <div class="right-context">
                                <p class="text-right-context">{{isset($data['type_checks'][$value]) && isset($data['type_checks'][$value]->files[0]) ?  date('Y/m/d - H:i', strtotime($data['type_checks'][$value]->files[0]->created_at)) : 'N/A'}}</p>
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">チェックの回数</div>
                            @php
                                $dataCount = '0';
                                if(isset($data['type_checks'][$value]) ) {
                                    $dataCount = $data['type_checks'][$value]->file_not_hold_count;
                                    if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                        $dataCount = $dataCount / 2;
                                    }
                                }
                            @endphp
                            <div class="right-context">
                                <p class="text-right-context">{{ $dataCount}}</p>
                            </div>
                        </div>
                        <div class="inner-management-block context-management-block">
                            <div class="left-context">エラーの発生回数</div>
                            <div class="right-context">
                                @php
                                    $dataCount = '0';
                                    if( isset($data['type_checks'][$value]) ) {
                                        $dataCount = $data['type_checks'][$value]->file_error_count;
                                        if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                            $dataCount = $dataCount / 2;
                                        }
                                    }
                                @endphp
                                <p class="text-right-context">{{ $dataCount }}</p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- block has button switches toggle -->
            @endif
        @endforeach
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.toggle-input').click(function () {
            $('#modalQuestion #file-status-question input[name="type"]').val($(this).parent().data('type'));
            const container = $(this).parent();
            const toggleBtn = $(this).parent().find('.toggle-btn');
            if ($(this).is(':checked')) {
                toggleBtn.css('margin-left', "16px");
                toggleBtn.css('background-color', "#0F94B5");
                container.css('background-color', "#B5E5F0");
                $('#modalQuestion').modal('show');
            } else {
                $('.toggle-btn').css('margin-left', "-2px");
                $('.toggle-btn').css('background-color', "#F5F7FB");
                $('.wrapper-toggle-btn-no').css('background-color', "#7c9aa1");
                $('.wrapper-toggle-btn').css('background-color', "#7c9aa1");
                $('#modalQuestion').modal('hide');
            }
        })
        $('#modalQuestion').on('hidden.bs.modal', function () {
            $('#toggle-btn').css('margin-left', "-2px");
            $('#toggle-btn').css('background-color', "rgb(245, 247, 251)");
            $('.toggle-input').prop('checked', false);
        });
        $('a[href$="javascript:void(0)"]').css('cursor', 'default')
    })
</script>
