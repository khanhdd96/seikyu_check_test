@php
    $facilityListTypeChecks = explode(',', $data['facility']->list_type_check);
    $type_menu = App\Consts::TYPE_MENU;
    $titles = App\Consts::TITLE;
    $titleNumber = App\Consts::TITLE_NUMBER;
    $statusCheck = App\Consts::STATUS_CHECK;
    $statusCheckIon = App\Consts::STATUS_CHECK_ICON;
    $classStatusCheck = App\Consts::CLASS_STATUS_CHECK;
    $statusCheckFile = App\Consts::STATUS_CHECK_FILE;
    $facility_id =  $data['facility']->id;
@endphp
<div class="row">
                <div class="col-lg-6">
                    @foreach(\App\Consts::TYPE_MENU as $key => $value)
                    <!-- block has button switches toggle -->
                    @if($key <= 6)
                    <div style="position: relative" class="col-management {{ !in_array($type_menu[$key], $facilityListTypeChecks) ? 'hidden-type-check': ''}} ">
                        <article class="management-block">
                            <div class="inner-management-block">
                                <h4 class="title-management-block">{{$titleNumber[$key]}} {{$titles[$key]}}</h4>
                                <div class="action-management-block">
                                    <!-- Button upload file: data-target=ID -->

                                    <!-- Redirect page history -->
                                    <button type="button" class="wrapper-upload-btn" onclick="loadDetail('{{$key}}')">
                                        <img src="{{ asset('icons/history.svg') }}" alt="history">
                                    </button>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">状態</div>
                                <div class="right-context">
                                    @if(isset($data['type_checks'][$value]))
                                    <button type="button" class="btn-right-context {{$classStatusCheck[$data['type_checks'][$value]->status]}}"
                                    data-content="{!! nl2br(e($data['type_checks'][$value]->reason)) !!}" rel="popover" data-placement="left" data-original-title="保留理由" data-trigger="manual">
                                        <img src="{{ asset($statusCheckIon[$data['type_checks'][$value]->status]) }}" alt="close">
                                        {{$statusCheck[$data['type_checks'][$value]->status]}}
                                    </button>
                                    @endif
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">最終チェック日時</div>
                                <div class="right-context">
                                    <p class="text-right-context">{{isset($data['type_checks'][$value]) && isset($data['type_checks'][$value]->files[0]) ?  date('Y/m/d - H:i', strtotime($data['type_checks'][$value]->files[0]->created_at)) : 'N/A'}}</p>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">チェックの回数</div>
                                @php
                                $dataCount = '0';
                                if( isset($data['type_checks'][$value]) ) {
                                    $dataCount = $data['type_checks'][$value]->file_not_hold_count;
                                    if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                        $dataCount = $dataCount / 2;
                                    }
                                    if (in_array($value, [1,2])) {
                                        $dataCount = $data['type_checks'][$value]->check_times_count;
                                    }
                                }
                                @endphp
                                <div class="right-context">
                                    <p class="text-right-context">{{$dataCount}}</p>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">エラーの発生回数</div>
                                <div class="right-context">
                                    @php
                                        $dataCount = '0';
                                        if( isset($data['type_checks'][$value]) ) {
                                            $dataCount = $data['type_checks'][$value]->file_error_count;
                                            if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                                $dataCount = $dataCount / 2;
                                            }
                                            if (in_array($value, [1,2])) {
                                                $dataCount = $data['type_checks'][$value]->check_error_times_count;
                                            }
                                        }

                                    @endphp
                                    <p class="text-right-context">{{ $dataCount }}</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- block has button switches toggle -->
                    @endif
                    @endforeach
                </div>
                <div class="col-lg-6">
                    @foreach(\App\Consts::TYPE_MENU as $key => $value)
                    <!-- block has button switches toggle -->
                    @if($key > 6)
                    <div style="position: relative" class="col-management {{ !in_array($type_menu[$key], $facilityListTypeChecks) ? 'hidden-type-check': ''}} ">
                        <article class="management-block">
                            <div class="inner-management-block">
                                <h4 class="title-management-block">{{$titleNumber[$key]}} {{$titles[$key]}}</h4>
                                <div class="action-management-block">
                                    <!-- Redirect page history -->
                                    <button type="button" class="wrapper-upload-btn" onclick="loadDetail('{{$key}}')">
                                        <img src="{{ asset('icons/history.svg') }}" alt="history">
                                    </button>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">状態</div>
                                <div class="right-context">
                                    @if(isset($data['type_checks'][$value]))
                                    <button type="button" class="btn-right-context {{$classStatusCheck[$data['type_checks'][$value]->status]}}"
                                        data-content="{!! nl2br(e($data['type_checks'][$value]->reason)) !!}" rel="popover" data-placement="left" data-original-title="保留理由" data-trigger="manual">
                                        <img src="{{ asset($statusCheckIon[$data['type_checks'][$value]->status]) }}" alt="close">
                                        {{ $statusCheck[$data['type_checks'][$value]->status] }}
                                    </button>
                                    @endif
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">最終チェック日時</div>
                                <div class="right-context">
                                <p class="text-right-context">{{isset($data['type_checks'][$value]) && isset($data['type_checks'][$value]->files[0]) ?  date('Y/m/d - H:i', strtotime($data['type_checks'][$value]->files[0]->created_at)) : 'N/A'}}</p>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">チェックの回数</div>
                                @php
                                $dataCount = '0';
                                if( isset($data['type_checks'][$value]) ) {
                                    $dataCount = $data['type_checks'][$value]->file_not_hold_count;
                                    if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                        $dataCount = $dataCount / 2;
                                    }
                                    if (in_array($value, [1,2])) {
                                        $dataCount = $data['type_checks'][$value]->check_times_count;
                                    }
                                }
                                @endphp
                                <div class="right-context">
                                    <p class="text-right-context">{{ $dataCount}}</p>
                                </div>
                            </div>
                            <div class="inner-management-block context-management-block">
                                <div class="left-context">エラーの発生回数</div>
                                <div class="right-context">
                                    @php
                                        $dataCount = '0';
                                        if( isset($data['type_checks'][$value]) ) {
                                            $dataCount = $data['type_checks'][$value]->file_error_count;
                                            if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                                $dataCount = $dataCount / 2;
                                            }
                                            if (in_array($value, [1,2])) {
                                                $dataCount = $data['type_checks'][$value]->check_error_times_count;
                                            }
                                        }
                                    @endphp
                                    <p class="text-right-context">{{ $dataCount }}</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- block has button switches toggle -->
                    @endif
                    @endforeach
                </div>
            </div>
