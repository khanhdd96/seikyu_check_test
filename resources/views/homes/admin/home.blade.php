
@extends('layouts.admin')
@section('title', '請求チェック状況管理')

<style>
    @media (min-width: 1300px) {
        .button-add-branch {
            width: 150px !important;
        }
    }
    .hidden-type-check {
        display: none;
    }
    .messages-file-csv {
        text-align: initial;
    }
    .messages-file-excel {
        text-align: initial;
    }

    .bnt-loadmore {
        background: none;
        color:#0f94b5;
    }
    .a-button-loadmore {
        pointer-events: none
    }
    .select2-selection{
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display:none;
    }
    .select2-results__option--selectable {
        font-size: 16px;
    }
    span.select2.select2-container.select2-container--default {
        height: 50px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }
    #modalError .home-modal-button {
        margin: 50px 0 0 0 !important;
    }
    .icon-datepicker-detail {
        position: absolute;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        width: 24px;
        height: 24px;
        right: 15px;
        top: 50%;
        transform: translateY(-50%);
        z-index: 2;
        border-radius: 5px;
        border: none;
        cursor: pointer;
    }


    #modalError #icon-search-file-modal-error img {
        margin-top: 22px;
    }
    #file-name-2, #file-name {
        margin-bottom: 10px;
    }
    .button-add-branch.mb-10{
        margin-bottom: 10px !important;
    }
    .button-add-branch {
        margin: 0 15px 0 0 !important;
    }
    .button-swap-all {
        min-width: 200px;
    }
    .wrapper-swap_all {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .wrapper-swap_all .form-check {
        margin-right: 15px;
    }
    .wrapper-swap_all .form-check .form-check-input{
        width: 16px;
        height: 16px;
        bottom: 1px;
    }
    .wrapper-swap_all .form-check label{
        font-size: 15px;
    }
    #datepicker-month {
        margin-right: 15px !important;
    }

    #datepicker {
        margin-right: 15px !important;
    }

    /*style radio button modal*/
    .form-radio-check [type="radio"]:checked,
    .form-radio-check [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    .form-radio-check [type="radio"]:checked + label,
    .form-radio-check [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    .form-radio-check [type="radio"]:checked + label:before,
    .form-radio-check [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 20px;
        height: 20px;
        border: 1px solid #2bc0e4;
        border-radius: 100%;
        background: #fff;
    }
    .form-radio-check [type="radio"]:checked + label:after,
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 12px;
        height: 12px;
        background: #2bc0e4;
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    .form-radio-check [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
</style>
<style>
    .hiden-dom {
        display:none;
    }
    .select2-selection{
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display:none;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 30px 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }
    .w-178 {
        width: 178px !important;
    }
    .w-228 {
        width: 200px !important;
    }

    /*@media (max-width: 1326px) {*/
    /*    .a-button-add-branch {*/
    /*        margin-top: 10px !important;*/
    /*        margin-left: 0px !important;*/
    /*    }*/
    /*}*/
    .w-150 {
        width: 150px !important;
    }
    .search-button .button-search {
        left: 15% !important;
        right: unset;
    }
    .search-button {
        /* width: 65px !important; */
        margin-left: 0px !important;
        margin-right: 15px !important;
    }
    .wrapper-search {
        margin-left: 15px !important;
        margin-right: 10px !important;
    }
    /* .selection-heading {
        margin-left: 15px;
    } */
    #popoverData1 {
        background-color: unset;
    }
    .type-old-upload {
        background: #FFE8EC !important;
    }
</style>
@section('content')

@php
$facilitys = $data['facilitys'];
$facilityIs = $facilitys->pluck('id')->toArray();
$facilityIs = implode(",", $facilityIs);
$date = date('Y/m');
if(isset($requestDatas['month'])) {
    preg_match_all('!\d+!', $requestDatas['month'], $matches);
    $date = $matches[0][0].'/'.$matches[0][1];
}
$arrayDate = explode('/', $date);
$yearSearch = $arrayDate[0];
$monthSearch = $arrayDate[1];
$daySearch = '01';
@endphp
<form action="" class="main-content main-admin form-admin-home">
    <div class="main-heading mb-30">
        <h1 class="title-heading">請求チェック状況管理</h1>
    </div>

    <div class="main-body">
        <!-- section-deadline-setting -->
        <div class="section-wrapper section-deadline-setting">
            <h5 class="mint-title-heading mb-20">締め切り設定</h5>
            <div class="filter-heading">
                <div id="datepicker" class="datepicker-heading ml-0">
                    <input class="input-datepicker" id="setting_deadline" {{$date != date('Y/m') ? 'disabled' : ''}} type="text" name="seting_deadline" placeholder="年/月/日" value ="">
                    <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                fill="#7C9AA1" />
                            <path opacity="0.4"
                                d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                fill="#7C9AA1" />
                            <path
                                d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                fill="#7C9AA1" />
                            <path
                                d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                fill="#7C9AA1" />
                            <path
                                d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                fill="#7C9AA1" />
                            <path
                                d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                fill="#7C9AA1" />
                            <path
                                d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                fill="#7C9AA1" />
                            <path
                                d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                fill="#7C9AA1" />
                        </svg>
                    </span>
                </div>
                <!-- Button Modal B2000.2 -->
                <button type="button" class="button-add-branch default-button button-add-branch-add-setting-deadline" data-toggle="modal" {{$date != date('Y/m') ? 'disabled' : ''}}
                    data-target="#modalApplication" disabled data-backdrop="static" data-keyboard="false">適用</button>
            </div>
        </div>
        <!-- section-deadline-setting -->

        <!-- section-check-status -->
        <div class="section-wrapper section-check-status">
            <div class="main-heading mb-30">
                <h5 class="mint-title-heading">チェック状況</h5>
                <div class="d-flex">
                    <div id="datepicker-month" class="datepicker-heading ml-0 mr-0 w-190">
                        <input class="input-datepicker input-date" type="text" name="month" placeholder="年/月/日" value="{{ isset($requestDatas['month']) ? $requestDatas['month'] : date('Y年m月')}}"}}">
                        <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                fill="#7C9AA1" />
                            <path opacity="0.4"
                                  d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                  fill="#7C9AA1" />
                            <path
                                d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                fill="#7C9AA1" />
                            <path
                                d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                fill="#7C9AA1" />
                            <path
                                d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                fill="#7C9AA1" />
                            <path
                                d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                fill="#7C9AA1" />
                            <path
                                d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                fill="#7C9AA1" />
                            <path
                                d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                fill="#7C9AA1" />
                        </svg>
                    </span>
                    </div>
                    <button class="search-button" style="width: 50px" type="button" onclick="submitForm()">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="wrapper-outer-status row align-items-center justify-content-center mb-50">
                <div class="false-For-Bottom-Text wrapper-chart col-xl-3 col-lg-4 col-md-4">
                    <div class="mypiechart">
                        <canvas id="myCanvas1" width="194" height="198"></canvas>
                        <div class="totalValuePie">
                            <span>合計</span>
                            <span>{{$data['dataCountFacility']['total']}}</span>
                        </div>
                    </div>
                </div>
                <div class="list-index-chart col-xl-7 col-lg-8 col-md-8">
                    <div class="row">
                        <div class="col-6 mt-15 mb-15">
                            <div class="item-index-chart">
                                <img src="{{ asset('icons/chart-index-1.svg') }}" alt="icon">
                                <div class="right-index-chart">
                                    <div class="text-index-chart text-index-chart1">
                                        未チェックの事業所数
                                    </div>
                                    <div class="number-index-chart number-index-chart1">{{$data['dataCountFacility']['not_has_check']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mt-15 mb-15">
                            <div class="item-index-chart">
                                <img src="{{ asset('icons/chart-index-2.svg') }}" alt="icon">
                                <div class="right-index-chart">
                                    <div class="text-index-chart text-index-chart2">
                                        保留中の事業所数
                                    </div>
                                    <div class="number-index-chart number-index-chart2">{{$data['dataCountFacility']['hold']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mt-15 mb-15">
                            <div class="item-index-chart">
                                <img src="{{ asset('icons/chart-index-3.svg') }}" alt="icon">
                                <div class="right-index-chart">
                                    <div class="text-index-chart text-index-chart3">
                                        エラーの事業所数
                                    </div>
                                    <div class="number-index-chart number-index-chart3">{{$data['dataCountFacility']['error']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mt-15 mb-15">
                            <div class="item-index-chart">
                                <img src="{{ asset('icons/chart-index-4.svg') }}" alt="icon">
                                <div class="right-index-chart">
                                    <div class="text-index-chart text-index-chart4">
                                        完了の事業所数
                                    </div>
                                    <div class="number-index-chart number-index-chart4">{{$data['dataCountFacility']['success']}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section-check-status -->

        <!-- section-table-field -->
        <div class="section-table-field">
            <form>
                <div class="heading-table-field">
                    <div class="filter-heading mb-30 pl-20 pr-20 flex-wrap">
                        <div class="selection-heading w-150 mb-10" style="margin-left: 0px;">
                            <select name="department_id" class="form-control dropdown-heading select2">
                                <option value="">支店名</option>
                                @foreach($data['departments'] as $department)
                                    <option {{isset($requestDatas['department_id']) && $requestDatas['department_id'] && $requestDatas['department_id'] == $department->id ? 'selected' : ''}} value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="selection-heading w-150 mb-10">
                            <select name="status_facilitity" class="form-control dropdown-heading">
                                <option value="">状況</option>
                                @foreach(App\Consts::STATUS_CHECK as $key=>$value)
                                    <option {{isset($requestDatas['status_facilitity']) && $requestDatas['status_facilitity'] && $requestDatas['status_facilitity'] == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="datepicker-search" class="datepicker-heading w-178 mb-10">
                            <input class="input-datepicker" type="text" name="deadline_search" placeholder="年/月/日" value="{{isset($requestDatas['deadline_search']) ? $requestDatas['deadline_search'] : ''}}">
                            <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                        fill="#7C9AA1" />
                                    <path opacity="0.4"
                                        d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                        fill="#7C9AA1" />
                                </svg>
                            </span>
                        </div>
                        <div class="wrapper-search w-228 mb-10">
                            <input class="input-search" style="z-index: 1;" type="text" placeholder="事業所名" name="search" value="{{isset($requestDatas['search']) ? $requestDatas['search'] : ''}}">
                            <button class="button-search" type="button" style="cursor: unset;">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4"
                                        d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                        fill="#7C9AA1" />
                                    <path
                                        d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                        fill="#7C9AA1" />
                                </svg>
                            </button>
                        </div>
                        <div class="search-button mb-10" style="width:150px" >
                            <button class="button-search" type="submit">
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                                <span style="color:#ffffff; font-weight: bold; font-size: 16px; padding-left: 7px;">検索</span>
                            </button>
                        </div>

                        <a href="{{route('admin.home.index')}}" class="mb-10 button-add-branch default-button a-button-add-branch">
                        条件クリア
                        </a>

                        <a href='{{route("export-csv", ["month" => $date, 'department_id' => request('department_id'), 'search' => request('search'), 'status_facilitity' => request('status_facilitity'), 'deadline_search' => request('deadline_search')])}}' type="button" class="mb-10 button-add-branch default-button a-button-add-branch">CSV出力</a>

                        <button type="button" class="mb-10 button-add-branch default-button a-button-add-branch button-swap-all" data-toggle="modal"
                            data-target="#modalSwapAllStatus" data-backdrop="static" data-keyboard="false">一括アップロード制限</button>

                    </div>


                    <div class="annotate-heading d-flex align-items-center mb-20 pl-20 pr-20">
                        <div class="item-annotate">
                            <div class="color-annotate color-annotate1"></div>
                            <div class="text-annotate">完了</div>
                        </div>
                        <div class="item-annotate">
                            <div class="color-annotate color-annotate2"></div>
                            <div class="text-annotate">エラー</div>
                        </div>
                        <div class="item-annotate">
                            <div class="color-annotate color-annotate3"></div>
                            <div class="text-annotate">未チェック</div>
                        </div>
                        <div class="item-annotate">
                            <div class="color-annotate color-annotate4"></div>
                            <div class="text-annotate">保留中</div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-admin table-home mt-15 mb-15">
                            <thead>
                                <tr>
                                    <th scope="col">順番</th>
                                    <th scope="col">事業所名</th>
                                    <th scope="col">支店名</th>
                                    <th scope="col">締め切り</th>
                                    <th scope="col">状況</th>
                                    <th scope="col" style="text-align: center;">やり直し回数</th>
                                    <th scope="col">各種チェック機能チェック状態</th>
                                    <th scope="col" style="text-align: center;">アップロード制限</th>
                                    <th scope="col">詳細表示</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $i = $facilitys->currentPage() * 3 - 2;

                            @endphp
                            @foreach($facilitys as $facility)
                                @php
                                    $listTypePhecks = explode(',',$facility->list_type_check);
                                    $typeChecks = [];
                                    $statusChecks = [];
                                    if(isset($facility->settingMonth[0])) {
                                        $typeChecks = $facility->settingMonth[0]->typeChecks;
                                        foreach($typeChecks as $typeChecks){
                                            $statusChecks[$typeChecks['code_check']] =  $typeChecks['status'];
                                        }
                                    }
                                    $classColorAnnotate = App\Consts::CLASS_COLOR_ANNOTATE;
                                    $checkUloadOld = false;
                                    if(isset($facility->settingMonth[0])) {
                                        $checkUloadOld = App\Models\SettingMonth::oldCanupload($facility->settingMonth[0]->id, $facility->settingMonth[0]->deadline);
                                    }
                                @endphp
                                <tr class="{{$checkUloadOld ? 'type-old-upload' : ''}}">
                                    <td>{{$i}}</td>
                                    <td>{{$facility->name}}</td>
                                    <td>{{$facility->department->name}}</td>
                                    <td>
                                        <div id="datepicker-table" class="datepicker-heading w-165 ml-0">
                                            <input class="input-datepicker deadline_facility_month" {{$date != date('Y/m') ? 'disabled' : ''}} data-id="{{$facility->id}}" type="text" name="deadline_facility_month" data="{{(isset($facility->settingMonth[0]) && $facility->settingMonth[0]->deadline) ? date('Y/m/d', strtotime($facility->settingMonth[0]->deadline)) : '' }}"  value="{{(isset($facility->settingMonth[0]) && $facility->settingMonth[0]->deadline) ? date('Y/m/d', strtotime($facility->settingMonth[0]->deadline)) : '' }}"  placeholder="年/月/日">
                                            <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                                        fill="#7C9AA1" />
                                                    <path opacity="0.4"
                                                        d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                                        fill="#7C9AA1" />
                                                    <path
                                                        d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                                        fill="#7C9AA1" />
                                                </svg>
                                            </span>

                                        </div>
                                    </td>
                                    <td>
                                        @php
                                            $statusCheck = 4;
                                            if(isset($facility->settingMonth[0]) && $facility->settingMonth[0]->status_facilitity != 4 && $facility->settingMonth[0]->status_facilitity) {
                                                $statusCheck = $facility->settingMonth[0]->status_facilitity;
                                            }
                                        @endphp
                                        <!-- add class="context-popover" if hover show tooltip -->
                                        <button type="button" id="popoverData1">
                                            <div class="selection-heading w-165 ml-0">
                                                <select class="form-control dropdown-heading" data-id='{{$facility->id}}' name="update_status_facility">
                                                    @foreach(App\Consts::STATUS_CHECK as $key => $value)
                                                    <option value="{{$key}}" {{ ($statusCheck == $key ) ? 'selected' : ''}}>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </button>

                                    </td>
                                    <td style="text-align: center;">
                                        @php
                                            $countFile = 0;
                                            if(isset($facility->settingMonth[0]) && $facility->settingMonth[0]->typeChecks) {
                                                foreach($facility->settingMonth[0]->typeChecks as $value){
                                                    ($value['code_check'] == App\Consts::TYPE_CHECK_NUMBER_3 || $value['code_check'] == App\Consts::TYPE_CHECK_NUMBER_5_M ) ?  $countFile += $value['file_not_hold_count']/2 : $countFile += $value['file_not_hold_count'];
                                                }
                                            }
                                        @endphp
                                        {{$countFile}}
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center flex-wrap" style="width: 276px;">
                                            @foreach(App\Consts::TYPE_MENU as $key => $value)
                                                @if(in_array($value,$listTypePhecks))
                                                <div
                                                        data-content="
                                                                        <div class='d-flex align-items-center flex-wrap justify-space-between'>
                                                                            <span style='color: red;'>状態</span>
                                                                            <img class='status-button ml-auto' src='{{isset($statusChecks[$value]) ?  asset(App\Consts::BUTTON_ICON[$statusChecks[$value]]) : asset(App\Consts::BUTTON_ICON[4]) }}' alt=''>
                                                                        </div>
                                                                    " rel="popover" data-placement="right"
                                                        data-original-title="{{App\Consts::TITLE[$key]}}" data-trigger="manual"
                                                    class="color-annotate context-popover  {{ isset($statusChecks[$value]) ? $classColorAnnotate[$statusChecks[$value]] : 'color-annotate3'}}">{{$key}}</div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>
                                        {{--@php
                                            $settingMonth = null;
                                            if(isset($facility->settingMonth[0])) {
                                                $settingMonth = $facility->settingMonth[0];
                                            }
                                            {{--$canUpload = App\Http\Services\Admin\FacilitityService::checkCanUpload($settingMonth,$date, $facility->id);--}}
                                            $canUpload = false;
                                            if ($settingMonth && ($settingMonth->status_facilitity !=  App\Consts::STATUS_CHECK_FILE['success']) && $settingMonth->can_upload == 1) {
                                                $canUpload = true;
                                            }
                                        @endphp --}}
                                            <!-- && ($settingMonth->status_facilitity !=  App\Consts::STATUS_CHECK_FILE['success']) -->
                                        @php
                                            $settingMonth = null;
                                            if(isset($facility->settingMonth[0])) {
                                                $settingMonth = $facility->settingMonth[0];
                                            }
                                            $canUpload = false;
                                            if ($settingMonth && $settingMonth->can_upload == 1) {
                                                $canUpload = true;
                                            }
                                        @endphp
                                        <button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="{{$facility->id}}" class="wrapper-toggle-btn checked toggle-btn2">
                                            <input type="checkbox" id="toggle-input1" class="toggle-input" {{$canUpload == true ? 'checked' : ''}}>
                                            <div id="toggle-btn1" class="toggle-btn"></div>
                                        </button>
                                    </td>
                                    <td style="white-space: nowrap">
                                        @php
                                            $url = 'admin/facilitity/'.$facility->id.'?setting_month='.$date;
                                        @endphp
                                        <!-- Button Modal B2001.2 -->
                                        <a style="display: flex;" href="{{asset($url)}}">
                                            <span class="text-view-detail">詳細</span>
                                            <img src="{{ asset('icons/view.svg') }}" alt="view">
                                        </a>
                                    </td>
                                </tr>
                            @php
                            $i ++;
                            @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- add pagination if more 10 rows -->
                    <nav class="wrapper-pagination">
                        @php
                            $facilitys = $facilitys->appends(request()->except('page'))
                        @endphp
                        @if ($facilitys->hasPages())
                        <ul class="pagination">
                            {{-- Previous Page Link --}}
                            @if ($facilitys->onFirstPage())
                                <li class="page-item disabled pageNumber"><span><img src="{{ asset('icons/arrow-left.svg') }}" alt="previous"></span></li>
                            @else
                                <li class="page-item" ><a class="page-link pageNumber" href="{{ $facilitys->previousPageUrl() }}" rel="prev"><img src="{{ asset('icons/arrow-left.svg') }}" alt="previous"></a></li>
                            @endif

                            @if($facilitys->currentPage() > 3)
                                <li class="page-item"><a class="page-link" href="{{ $facilitys->url(1) }}">1</a></li>
                                @endif
                                @if($facilitys->currentPage() > 4)
                                    <li class="page-item"><a class="page-link">...</a></li>
                                @endif
                                @foreach(range(1, $facilitys->lastPage()) as $i)
                                    @if($i >= $facilitys->currentPage() - 2 && $i <= $facilitys->currentPage() + 2)
                                        @if ($i == $facilitys->currentPage())
                                            <li class="page-item active"><a class="page-link active">{{ $i }}</a></li>
                                        @else
                                            <li class="page-item"><a class="page-link" href="{{ $facilitys->url($i) }}">{{ $i }}</a></li>
                                        @endif
                                    @endif
                                @endforeach
                                @if($facilitys->currentPage() < $facilitys->lastPage() - 3)
                                    <li class="page-item"><a class="page-link">...</a></li>
                                @endif
                                @if($facilitys->currentPage() < $facilitys->lastPage() - 2)
                                <li class="page-item"><a class="page-link" href="{{ $facilitys->url($facilitys->lastPage()) }}">{{ $facilitys->lastPage() }}</a></li>
                            @endif
                            {{-- Next Page Link --}}
                            @if ($facilitys->hasMorePages())
                                <li class="page-item" ><a class="page-link" href="{{ $facilitys->nextPageUrl() }}" rel="next">  <img src="{{ asset('icons/arrow-right.svg') }}" alt="next"></a></li>
                            @else
                                <li class="page-item" class="disabled pageNumber"><span>  <img src="{{ asset('icons/arrow-right.svg') }}" alt="next"></span></li>
                            @endif
                        </ul>@endif

                    </nav>
                </div>
            </form>
        </div>
        <!-- section-table-field -->
    </div>
</form>
</section>
<!-- Modal B2000.2 Home -->
<div class="modal fade" id="modalApplication" tabindex="-1" role="dialog" aria-labelledby="modalApplicationTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">締め切りを設定</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <form id="form-setting-deadline" method="post" action="{{route('setting-deadline', $requestDatas)}}">
                    @csrf
                    <input type="hidden" id="setting_deadline2" type="text" name="seting_deadline" placeholder="年/月/日" value ="{{date('Y/m/d')}}">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text"> 以下の締め切りが設定でしょうか。</p>
                    <p class="modal-text modal-text-bold" id="confirm_deadline">{{date('Y/m/d')}}</p>
                    <button type="submit" class="home-modal-button default-button"
                        >同意</button>
                    </div>
                </form>
        </div>
    </div>
</div>
<!-- Modal B2000.2 Home -->

<!-- Modal B2000.2 Home -->
<div class="modal fade" id="modalSwapAllStatus" tabindex="-1" role="dialog" aria-labelledby="modalSwapAllStatusTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">事業所の一括アップロード制限変更</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <form id="form-setting-swap-all" method="post" action="{{route('setup-status-upload-all', $requestDatas)}}">
                    @csrf
                    <input type="hidden" id="setting_deadline2" type="text" name="seting_deadline" placeholder="年/月/日" value ="{{date('Y/m/d')}}">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <div class="wrapper-swap_all">
                        <div class="form-check form-radio-check">
                            <input class="form-check-input" type="radio" name="toggle_input_all" value="1" id="flexRadioDefault1" checked>
                            <label class="form-check-label" for="flexRadioDefault1">
                                有効する
                            </label>
                        </div>
                        <div class="form-check form-radio-check">
                            <input class="form-check-input" type="radio" name="toggle_input_all" value="0" id="flexRadioDefault2" >
                            <label class="form-check-label" for="flexRadioDefault2">
                                無効する
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="home-modal-button default-button"
                        >適用</button>
                    </div>
                </form>
        </div>
    </div>
</div>
<!-- Modal B2000.2 Home -->

<!-- Modal B2000.2 Home -->
<div class="modal fade" id="modalApplicationDetail" tabindex="-1" role="dialog" aria-labelledby="modalApplicationTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">締め切りを設定</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <form id="form-setting-deadline-detail" action="{{route('edit-deadline')}}" method="post">
                    @csrf
                    <input type="hidden" id="setting_deadline_detail" type="text" name="seting_deadline" placeholder="年/月/日" value ="{{date('Y/m/d')}}">
                    <input type="hidden" id="facilityId" name="facilityId" value="{{$facilityIs}}">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text"> 以下の締め切りが設定でしょうか。</p>
                    <p class="modal-text modal-text-bold" id="confirm_deadline_detail">{{date('Y/m/d')}}</p>
                    <button type="submit" class="home-modal-button default-button"
                        >同意</button>
                        </form>
                </div>
        </div>
    </div>
</div>
<!-- Modal B2000.2 Home -->

<!-- Modal B2000.3 Home -->
<div class="modal fade" id="modalChangeStatusFacility" tabindex="-1" role="dialog" aria-labelledby="modalChangeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">状況を変更</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <form id="form-change-status-facility" action="{{route('edit-status')}}" method="post">
                    @csrf
                    <input type="hidden" id="facilityChangeStatusId" name="facilityId" value="">
                    <input type="hidden" id="facility_year_month" name="facilityYearMonth" value="{{$date}}">
                    <input type="hidden" id="status_facility_update" name="status_facility_update" value="">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text"> この事業所の状況が以下のように変更でしょうか。</p>
                    <p class="modal-text modal-text-bold">完了</p>
                    <button type="submit" class="home-modal-button default-button">同意</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- scrip -->
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.ja.min.js')}}" integrity="sha512-zI0UB5DgB1Bvvrob7MyykjmbEI4e6Qkf5Aq+VJow4nwRZrL2hYKGqRf6zgH3oBQUpxPLcF2IH5PlKrW6O3y3Qw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/style-chart.js') }}"></script> -->
<script>
const container = document.getElementById("wrapper-toggle-btn")
const toggleInput = document.getElementById("toggle-input")
const toggleBtn = document.getElementById("toggle-btn")
const modalQuestion = document.getElementById("modalQuestion")
</script>
<script>
$(document).ready(function () {
    $('.select2').select2({
        language: {
            inputTooShort: function (args) {

                return "任意の文字を入力してください。。。";
            },
            noResults: function () {
                return "見つかりません。。。";
            },
            searching: function () {
                return "検索しています。。。";
            }
        },
    });
    var date=new Date();
    var year=date.getFullYear()
    var month=date.getMonth();
    var day = new Date(year, month + 1, 0).getDate();
    $("#datepicker input").datepicker({
        format: "yyyy/mm/dd",
        autoclose:true,
        language: 'ja',
        startDate: new Date(),
        endDate: new Date(year, month, day)
    });

    $("#datepicker-table input").datepicker({
        format: "yyyy/mm/dd",
        autoclose:true,
        language: 'ja',
        startDate: new Date(),
        endDate: new Date(year, month, day)
    });

    $("#datepicker-month input").datepicker({
        format: "yyyy年mm月",
        startView: "months",
        minViewMode: "months",
        autoclose:true,
        language: 'ja',
        endDate: new Date(year, month, '01')
    });

    var yearSearch = Number('{{$yearSearch}}');
    var monthSearch = Number('{{$monthSearch}}') - 1;
    var daySearch = Number('{{$daySearch}}');
    var endDay = new Date(yearSearch, monthSearch + 1, 0).getDate();
    $("#datepicker-search input").datepicker({
        format: "yyyy/mm/dd",
        autoclose:true,
        language: 'ja',
        startDate: new Date(yearSearch, monthSearch, daySearch),
        endDate: new Date(yearSearch, monthSearch, endDay)
    });


    // $('#datepicker .icon-datepicker').on('click', function () {
    //     $('#datepicker input').trigger('focus');
    // })
    //
    // $('#datepicker-search .icon-datepicker').on('click', function () {
    //     $('#datepicker-search input').trigger('focus');
    // })
    //
    // $('#datepicker-month .icon-datepicker').on('click', function () {
    //     $('#datepicker-month input').trigger('focus');
    // })

    $('input[name="seting_deadline"]').on('change', function (e) {
        let value = $('#setting_deadline').val();
        if(value == '') {
            $('.button-add-branch-add-setting-deadline').prop('disabled', true);
        }
        if(value != '') {
            $('.button-add-branch-add-setting-deadline').prop('disabled', false);
        }
        $('#setting_deadline2').val(value);
        $('#confirm_deadline').html(value);
    })

    $("#form-setting-deadline").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: formData,
            // async: false,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            thisModal.modal('hide');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });

    $('#modalApplication').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#modalApplicationDetail').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#modalChangeStatusFacility').on('hidden.bs.modal', function () {
        location.reload();
    });

    var obj1 = {
        values: [{{$data['dataCountFacility']['hold']}}, {{$data['dataCountFacility']['not_has_check']}}, {{$data['dataCountFacility']['error']}}, {{$data['dataCountFacility']['success']}}],
        colors: ["#54A3F3", "#FD997F", "#51D7FA", "#2FE42B"], //colors: blue保留中事業所の数 - pink未チェックの事業所数 - blueチェック中の事業所数 - green完了の事業所数
        animation: false,
        fillTextData: true,
        fillTextColor: "#fff",
        fillTextAlign: 1.3,
        fillTextPosition: "horizontal",
        doughnutHoleSize: 60,
        doughnutHoleColor: "#fff",
        offset: null,
    };

    //Generate myCanvas1
    generatePieGraph("myCanvas1", obj1);

    $('input[name="deadline_facility_month"]').on('change', function(e){
        var facility_id = $(this).attr('data-id');
        var data = $(this).attr('data');
        var deadline_facility_month = $(this).val();
        if (data != deadline_facility_month) {
            $('#modalApplicationDetail').modal('show');
            $('#setting_deadline_detail').val(deadline_facility_month);
            $('#facilityId').val(facility_id);
            $('#confirm_deadline_detail').html(deadline_facility_month);
        }
    });

    $("#form-setting-deadline-detail").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: formData,
            // async: false,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            thisModal.modal('hide');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });

    });

    $('select[name="update_status_facility"]').on('change', function(e){
        e.preventDefault();
        var facility_id = $(this).attr('data-id');
        var text_selected = $(this).find('option:selected').text();
        var status_facility_update =  $(this).val();
        $('#modalChangeStatusFacility .modal-text-bold').text(text_selected);
        $('#modalChangeStatusFacility').modal('show');
        $('#status_facility_update').val(status_facility_update);
        $('#facilityChangeStatusId').val(facility_id);
    });

    $("#form-change-status-facility").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: formData,
            // async: false,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            thisModal.modal('hide');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });

    });

    $('.toggle-btn2').click(function(e){
        var facility_id = $(this).attr('data-id');
        var action = '{{route('open-status-upload')}}';
        $.ajax({
            url: action,
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "facility_id" : facility_id,
                "month": "{{isset($requestDatas['month']) ? $requestDatas['month'] : ''}}"
            },
            success: function (data) {
                location.reload();
            }
        })

    });


    $("#form-setting-swap-all").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            thisModal.modal('hide');
            location.reload();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });

    });


});

function submitForm()
{
    let requestMonth = '{{request("month")}}';
    if ($("#datepicker-month input") != requestMonth) {
        $('.form-admin-home').submit();
    }
}
</script>
@endsection
