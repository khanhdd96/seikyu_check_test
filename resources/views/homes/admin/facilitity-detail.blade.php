@extends('layouts.admin')
@section('title', '請求チェック状況管理')
<style>
    .hiden-dom {
        display:none;
    }
</style>
@section('content')
    <style>
        .hidden-type-check {
            display: none;
        }
        .messages-file-csv {
            text-align: initial;
        }
        .messages-file-excel {
            text-align: initial;
        }

        .bnt-loadmore {
            background: none;
            color:#0f94b5;
        }
        .a-button-loadmore {
            pointer-events: none
        }
        .select2-selection{
            height: 100% !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display:none;
        }
        .select2-results__option--selectable {
            font-size: 16px;
        }
        span.select2.select2-container.select2-container--default {
            height: 50px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }
        #modalError .home-modal-button {
            margin: 50px 0 0 0 !important;
        }
        .icon-datepicker-detail {
            position: absolute;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            width: 24px;
            height: 24px;
            right: 15px;
            top: 50%;
            transform: translateY(-50%);
            z-index: 2;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }


        #modalError #icon-search-file-modal-error img {
            margin-top: 22px;
        }
        #file-name-2, #file-name {
            margin-bottom: 10px;
        }
        .warning {
            background-color: #fcf8e3;
            color: #8a6d3b;
        }
    </style>
<style>
    .hidden-type-check {
        display: none;
    }
    .messages-file-csv {
        text-align: initial;
    }
    .messages-file-excel {
        text-align: initial;
    }

    .bnt-loadmore {
        background: none;
        color:#0f94b5;
    }
    .a-button-loadmore {
        pointer-events: none
    }
    .breadcrumb-detail {
        font-family: 'Noto Sans JP';
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 17px;
        letter-spacing: -0.02em;
        color: #BFBFBF;
        padding-bottom: 13px;
    }
    .breadcrumb-detail span {
        font-size: 12px;
    }
    .breadcrumb-detail a {
        color: #BFBFBF;
    }
    .main-content {
        padding: 20px 20px !important;
    }
</style>

<form action="" class="main-content main-admin">
    <div class="breadcrumb-detail">
        <p><a href="{{route('admin.home.index')}}"><span>請求チェック状況管理/</span></a><span style="color:#2DA1BE"> {{$data['facility']->name}}</span></p>
    </div>
    <div class="main-heading mb-30">
        <h1 class="title-heading">{{$data['facility']->name}}</h1>
        <div class="filter-heading">
            <div id="datepicker" class="datepicker-heading">
                <input class="input-datepicker" type="text"  name="setting_month" placeholder="年/月" value="{{$requestDatas ? $requestDatas['setting_month'] : date('Y/m')}}">
                <span class="icon-datepicker">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1" />
                        <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1" />
                        <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1" />
                        <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1" />
                        <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1" />
                        <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1" />
                        <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1" />
                        <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1" />
                    </svg>
                </span>
            </div>
            <button class="search-button" style="width: 50px" type="submit">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
        </div>
    </div>

    <div class="main-body">
        <!-- 4 blocks -->
        <div class="wrapper-article-block">
            <div class="row">
                <div class="col-lg-3">
                    <article class="article-block">
                        <h5 class="title-article-block line-2">支店名</h5>
                        <h2 class="date-article-block line-2">{{($data['department'] && $data['department']->name) ? $data['department']->name : 'NA'}}</h2>
                    </article>
                </div>
                <div class="col-lg-3">
                    <article class="article-block">
                        <h5 class="title-article-block line-2">事業所名</h5>
                        <h2 class="date-article-block line-2">{{ ($data['facility'] && $data['facility']->name) ? $data['facility']->name : 'NA'}}</h2>
                    </article>
                </div>
                <div class="col-lg-3">
                    <article class="article-block">
                        <h5 class="title-article-block line-2">年月</h5>
                        <h2 class="date-article-block line-2">{{$requestDatas ? $requestDatas["setting_month"] : date('Y/m')}}</h2>
                    </article>
                </div>
                <div class="col-lg-3">
                    <!-- block checked  -->
                    <article class="article-block {{($data['setting_month'] && $data['setting_month']->status_facilitity ? App\Consts::CLASS_STATUS_CHECK_FILE_DETAIL[$data['setting_month']->status_facilitity] : 'checked-red-block' )}}">
                        <h5 class="title-article-block line-2" >状態</h5>
                        <h2 class="date-article-block line-2">{{(($data['setting_month'] && $data['setting_month']->status_facilitity)  ? App\Consts::STATUS_CHECK[$data['setting_month']->status_facilitity] : '未チェック' )}}</h2>
                    </article>
                    <!-- block checked  -->
                </div>
            </div>
        </div>
        <!-- 4 blocks -->

        <div class="main-heading mt-30 mb-25">
            <h4 class="title-result">詳細</h4>
            <!-- <div class="wrapper-search">
                        <input class="input-search" type="text" placeholder="事業所名" name="search">
                        <button class="button-search" type="submit">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4" d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z" fill="#7C9AA1"/>
                                <path d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z" fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div> -->
        </div>
        <!-- 11 blocks -->
        <div class="wrapper-management-block">
        @if($data['facility'] && $data['facility']->list_type_check)
            @include('homes.admin.home-type-check')
        @else
            <div class="text-center">
                <img src="{{asset('/icons/empty.png')}}" alt="">
                <p class="mt-5" style="font-size: 16px">まだ拠点はありません</p>
            </div>
        @endif
        </div>
        <!-- 11 blocks -->
    </div>
</form>
<!-- Modal A1000.1 Home -->
{{--<div class="modal fade" id="modalQuestion" tabindex="-1" role="dialog" aria-labelledby="modalQuestionTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">状態変更</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
            </div>
            <div class="modal-body home-modal-body">
            <form id="file-status-question"  action="{{route('check-7-success')}}"
                          method="post" enctype="multipart/form-data" class="">
                          @csrf
                    <input type="hidden" name="settingMonth"
                            value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                    <input type="hidden" name="facilityId" value="{{($data['facility'] && $data['facility']->id)? $data['facility']->id : ''}}">
                    <input type="hidden" name="month" value="{{ !empty($requestDatas) ? $requestDatas['setting_month'] : date('Y/m')}}">
                    <input type="hidden" name="type" value="{{App\Consts::TYPE_MENU[1]}}">
                <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                <p class="modal-text"> この事業所の状態が以下のように変更でしょうか。</p>
                <p class="modal-text modal-text-bold">完了</p>
                <button type="submit" class="home-modal-button default-button" >同意</button>
            </form>
            </div>
        </div>
    </div>
</div>
--}}
<!-- Modal A1000.1 Home -->
@php
    $RouterCheckFile = App\Consts::ROUTE_CHECK_FILE;
    $type_menu = App\Consts::TYPE_MENU;
    $titles = App\Consts::TITLE;
    $titleNumber = App\Consts::TITLE_NUMBER;
    $statusCheck = App\Consts::STATUS_CHECK;
    $dataTypeCheckDetail = $data['data_type_check_detail'];
@endphp
{{-- @if($data['facility'])
@foreach(\App\Consts::TYPE_MENU as $key => $value)
<!-- Modal upload file: id=ID &aria-labelledby=ID -->
<div class="modal fade" id="modalUploadID{{$key}}" tabindex="-1" role="dialog" aria-labelledby="modalUploadID{{$key}}Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog {{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'multiple-modal-dialog' : ''}}" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold"></div>
                    <form id="file-upload-form-no{{$key}}"  action="{{ isset($RouterCheckFile[$value]) ? route($RouterCheckFile[$value]) : ''}}"
                          method="post" enctype="multipart/form-data" class ="file-upload-form file-upload-form-no {{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'outer-modal-multiple' : 'outer-modal-single'}}">
                          @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['setting_month'] ? $data['setting_month']->id : 'null'}}">
                        <input type="hidden" name="facilityId" value="{{$data['facility']->id}}">
                        <input type="hidden" name="month" value="{{ !empty($requestDatas) ? $requestDatas['setting_month'] : date('Y/m')}}">
                        <input type="hidden" name="type" value="{{$value}}">
                        <div class="multiple-file-upload {{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'row' : ''}}">
                        @if(\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'])
                            <div class="item-file-upload  {{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] ? 'col-6' : 'col-12'}}">
                                <input id="file-upload-csv{{$key}}" data-type={{\App\Consts::TYPE_CHECK_UPLOAD[$value]}} class="file-upload-test-csv"  type="file" name="{{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'file[]' : 'file'}}" accept=".csv" />
                                <label for="file-upload-csv{{$key}}" data-type={{\App\Consts::TYPE_CHECK_UPLOAD[$value]}} class="file-upload label-file-drag file-drag-csv">
                                    <img id="file-image-{{$key}}" src="#" alt="Preview" class="hidden file-image">
                                    <div class="file-start start-csv">
                                        <div id="notimage" class="hidden notimage">CSVファイルを選択してください。</div>
                                        <span id="file-upload-btn" class="icon-upload-file">
                                            <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                        </span>
                                        <p class="modal-text"> CSVファイル </p>
                                    </div>
                                </label>
                                <div id="response-csv" class="hidden file-response">
                                    <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                    <div id="messages-{{$key}}" class="messages-file-csv"></div>
                                </div>

                                <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>
                            </div>
                            <div class="item-file-upload  {{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] ? 'col-6' : 'col-12'}}">
                                <input id="file-upload-excel{{$key}}" data-type={{\App\Consts::TYPE_CHECK_UPLOAD[$value]}} class="file-upload-test-excel"  type="file" name="{{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'file[]' : 'file'}}" accept=".xls, .xlsx" />
                                <label for="file-upload-excel{{$key}}" data-type={{\App\Consts::TYPE_CHECK_UPLOAD[$value]}} class="file-upload label-file-drag file-drag-excel">
                                    <img id="file-image-{{$key}}" src="#" alt="Preview" class="hidden file-image">
                                    <div class="file-start start-excel">
                                        <div id="notimage" class="hidden notimage">CSVファイルを選択してください。</div>
                                        <span id="file-upload-btn" class="icon-upload-file">
                                            <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                        </span>
                                        <p class="modal-text"> Excel ファイル </p>
                                    </div>
                                </label>

                                <div id="response-excel" class="hidden file-response">
                                    <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                    <div id="messages-{{$key}}" class="messages-file-excel"></div>
                                </div>
                                <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p>
                            </div>
                            @else
                                <div class="item-file-upload">
                                    <input id="file-upload-excel{{$key}}" class="file-upload-test-excel" type="file" name="file" accept="{{\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['EXCEL'] ? '.xls, .xlsx' : (\App\Consts::TYPE_CHECK_UPLOAD[$value] == App\Consts::UPLOAD_TYPE['CSV'] ? '.csv' : '.xls, .xlsx, .csv')}}"/>
                                    <label for="file-upload-excel{{$key}}" id="file-drag" data-type={{\App\Consts::TYPE_CHECK_UPLOAD[$value]}} class="file-upload label-file-drag file-drag-excel">
                                        <img id="file-image-{{$key}}" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn-{{$key}}" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                    <div id="response-excel" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}"
                                             alt="document-upload">
                                        <div id="messages-{{$key}}" class="messages-file-excel"></div>
                                    </div>
                                    @if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) && \App\Consts::SAMPLE_FILE_LINK[$value]['CSV'])
                                        <p class="modal-text"> サンプルファイルをCSV形式でダウンロードする - <a
                                                href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) }}"
                                                download class="btn-download-file">こちらで</a></p>
                                    @endif
                                    @if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) && \App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL'])
                                        <p class="modal-text"> サンプルファイルをExcel形式でダウンロードする -  <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) }}" download class="btn-download-file">こちらで</a></p>
                                    @endif

                                </div>
                            @endif
                        </div>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
--}}
<div class="modal fade" id="modalDetailCheck" tabindex="-1" role="dialog"
     aria-labelledby="modalDetailCheck" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered view-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェック詳細</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body text-left pt-50 pb-15 max-height">
                <div class="pl-100 pr-100">
                    <form class="form_detail_type" method="GET" action="">
                        <input type="hidden" id="page" value="1" />
                        <input type="hidden" id="type_check_modal" value="1" />
                        <div class="main-heading mb-30">
                            <h2 class="mint-title" id="title-check"></h2>
                            <div class="d-flex">
                                <div id="datepicker-history-detail" class="datepicker-heading">
                                    <input type="hidden" class="type_check_detail" name="type" value="1">
                                    <input class="input-datepicker month_detail_datepicker" type="text" name="month" placeholder="年/月" value="">
                                    <span class="icon-datepicker-detail">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                            fill="#7C9AA1" />
                                        <path opacity="0.4"
                                              d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                              fill="#7C9AA1" />
                                        <path
                                            d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                            fill="#7C9AA1" />
                                        <path
                                            d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                            fill="#7C9AA1" />
                                        <path
                                            d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                            fill="#7C9AA1" />
                                        <path
                                            d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                            fill="#7C9AA1" />
                                        <path
                                            d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                            fill="#7C9AA1" />
                                        <path
                                            d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                            fill="#7C9AA1" />
                                    </svg>
                                </span>
                                </div>
                                <button onclick="changeDate($('.month_detail_datepicker').val())"  class="search-button" style="width: 50px" type="button">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                </button>
                            </div>

                        </div>

                        <div class="main-body">
                            <!-- 4 blocks -->
                            <div class="wrapper-article-block wrapper-article-check">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <article class="article-block first-upload-block">
                                            <h5 class="title-article-block line-2">作成日</h5>
                                            <h2 class="date-article-block line-2" id="time-create"></h2>
                                        </article>
                                    </div>
                                    <div class="col-lg-3">
                                        <article class="article-block second-upload-block">
                                            <h5 class="title-article-block line-2">更新日</h5>
                                            <h2 class="date-article-block line-2" id="time-update"></h2>
                                        </article>
                                    </div>
                                    <div class="col-lg-3">
                                        <article class="article-block third-upload-block">
                                            <h5 class="title-article-block line-2">チェックの回数</h5>
                                            <h2 class="date-article-block line-2" id="number-check"></h2>
                                        </article>
                                    </div>
                                    <div class="col-lg-3">
                                        <article class="article-block" id="color-check">
                                            <h5 class="title-article-block line-2">状態</h5>
                                            <h2 class="date-article-block line-2" id="status-check"></h2>
                                        </article>
                                        <!-- block checked  -->
                                    </div>
                                </div>
                            </div>
                            <!-- 4 blocks -->
                        </div>
                        <div class="main-heading mt-30 mb-25">
                            <h4 class="blue-title-result">ファイルチェック履歴</h4>
                        </div>

                        <div class="wrapper-detail-check" id="detail-check">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function loadDetail(typeCheck, month = null) {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        $('#color-check').removeClass();
        $('#type_check_modal').val(typeCheck);
        var facilityId = '<?=$data['facility']->id?>';
        if (month == null) {
            month = $('input[name="setting_month"]').val();
            $('.month_detail_datepicker').val(month);
            var date=new Date();
            var year=date.getFullYear()
            var monthDae=date.getMonth();
            $(".month_detail_datepicker").datepicker({
                format: "yyyy/mm",
                startView: "months",
                minViewMode: "months",
                autoclose:true,
                language: 'ja',
                endDate: new Date(year, monthDae, '01')
            });
        }
        $('#page').val(1)
        $('#detail-check').html('<div class="text-center">' +
            '<img src="{{asset('/icons/empty.png')}}" alt=""><p class="mt-5" style="font-size: 16px">まだファイルがない</p></div>');
        $.ajax({
            url: '{{route('load-detail-check')}}',
            type: 'GET',
            data: {
                type_check : typeCheck,
                facility_id : facilityId,
                month : month,
                page : 1,
            },
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            dataType: "json",
        }).done(function (data) {
            var result = data.data;
            $('#title-check').html(result.title_check);
            $('#time-create').html(result.time_create);
            $('#time-update').html(result.time_update);
            $('#number-check').html(result.number_check);
            $('#status-check').html(result.status_check);
            $('#color-check').addClass('article-block ' + result.color_check);
            if (result.detail_check.length > 0) {
                var html = '';
                let errorClass = '';
                $.each(result.detail_check, function (key, value) {
                    errorClass = value.error_count > 0 ? "red-number-check" : "green-number-check"
                    html+= '<div class="detail-check-block">\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                <div class="context-check-block">更新日: <span class="gray-date">' +value.created_at+ ' </span></div>\n' +
                        '                <div class="btn-right-context ' + value.class + '">\n' +
                        '                    <img src="' + value.image + '" alt="verify">\n' +
                        '                    ' + value.status_text + '\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '        </div>\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                                <div class="context-check-block">ファイル名: <span class="mint-file-check">' + value.file_name + '</span></div>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                <div class="context-check-block">エラー数: <span class="' + errorClass + '">' + value.error_count + '</span></div>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '<ul class="list-error-file">\n';
                    $.each(value.data_error, function (key1, value1) {
                        var classAdded = '';
                        if (value.data_type_error[key1] == '{{\App\Consts::TYPE_WARNING}}') {
                            var classAdded = 'warning';
                        }
                        html+=
                            '                            <li class="item-error-file ' + classAdded + '">\n' +
                            '                    ' + value1 + '\n' +
                            '                </li>\n';
                    })
                    html+='</ul>\n';
                    if (value.status == 3) {
                        html+=
                            '                <div class="reason-check-block mt-20">\n' +
                            '                <div class="context-check-block">保留理由</div>\n' +
                            '                <div class="text-reason-check">\n' +
                            '            ' + value.reason + '\n' +
                            '</div>\n' +
                            '</div>\n';
                    }
                    html+='</div>';
                });
                if (result.has_page) {
                    html+= '<div class="wrapper-button-loadmore pagination_loadmore" id="pagination">\n' +
                        '    <div style="cursor:pointer" onclick="loadMoreDetail()" class="button-loadmore">\n' +
                        '    <img class="mr-10" src="{{ asset('icons/add-square.svg') }}" alt="add-square">\n' +
                        '    もっと見る\n' +
                        '    </div>\n' +
                        '</div>';
                }
                $('#detail-check').html(html);
                $('a.download-button').on('click', function (){
                    download($(this));
                })
            }
        });
        $('#modalDetailCheck').modal('show');

        setTimeout(function () {
                $("#preloader").remove();

            }, 3000
        )
    }
    function download(element) {
        var id = element.data('id');
        // only works with files that don't render in browser
        // ie: not video, not text, not photo
        $.ajax({
            url: '{{route('link-download')}}',
            type: 'GET',
            data: {
                id: id,
            },
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            dataType: "json",
        }).done(function (data) {
            var links = data.data.links;
            for (let i = 0; i < links.length; i++) {
                var frame = document.createElement("iframe");
                frame.src = links[i];
                frame["download"] = 1
                document.body.appendChild(frame);
            }
        })
    }
    function loadMoreDetail()
    {
        var page = parseInt($('#page').val()) + 1;
        $('#page').val(page);
        $('#pagination').remove();
        var facilityId = '<?=$data['facility']->id?>';
        var typeCheck = $('#type_check_modal').val();
        var month = $('.month_detail_datepicker').val();
        $.ajax({
            url: '{{route('load-detail-check')}}',
            type: 'GET',
            data: {
                type_check : typeCheck,
                facility_id : facilityId,
                month : month,
                page : page,
            },
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            dataType: "json",
        }).done(function (data) {
            var result = data.data;
            if (result.detail_check.length > 0) {
                var html = '';
                let errorClass = '';
                $.each(result.detail_check, function (key, value) {
                    errorClass = value.error_count > 0 ? "red-number-check" : "green-number-check"
                    html+= '<div class="detail-check-block">\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                <div class="context-check-block">更新日: <span class="gray-date">' +value.created_at+ ' </span></div>\n' +
                        '                <div class="btn-right-context ' + value.class + '">\n' +
                        '                    <img src="' + value.image + '" alt="verify">\n' +
                        '                    ' + value.status_text + '\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '        </div>\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                                <div class="context-check-block">ファイル名: <span class="mint-file-check">' + value.file_name + '</span></div>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="inner-check-block">\n' +
                        '            <div class="left-check-block">\n' +
                        '                <div class="context-check-block">エラー数: <span class="' + errorClass + '">' + value.error_count + '</span></div>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '<ul class="list-error-file">\n';
                    $.each(value.data_error, function (key1, value1) {
                        var classAdded = '';
                        if (value.data_type_error[key1] == '{{\App\Consts::TYPE_WARNING}}') {
                            var classAdded = 'warning';
                        }
                        html+=
                            '                            <li class="item-error-file ' + classAdded + '">\n' +
                            '                    ' + value1 + '\n' +
                            '                </li>\n';
                    })
                    html+='</ul>\n';
                    if (value.status == 3) {
                        html+=
                            '                <div class="reason-check-block mt-20">\n' +
                            '                <div class="context-check-block">保留理由</div>\n' +
                            '                <div class="text-reason-check">\n' +
                            '            ' + value.reason + '\n' +
                            '</div>\n' +
                            '</div>\n';
                    }
                    html+='</div>';
                });
                if (result.has_page) {
                    html+= '<div class="wrapper-button-loadmore pagination_loadmore" id="pagination">\n' +
                        '    <div style="cursor:pointer" onclick="loadMoreDetail()" class="button-loadmore">\n' +
                        '    <img class="mr-10" src="{{ asset('icons/add-square.svg') }}" alt="add-square">\n' +
                        '    もっと見る\n' +
                        '    </div>\n' +
                        '</div>';
                }
                $('.detail-check-block:last').after(html);
            }
        });

    }

    function changeDate(month)
    {
        var typeCheck = $('#type_check_modal').val();
        loadDetail(typeCheck, month);
    }
</script>
<!-- Modal upload file: id=ID &aria-labelledby=ID -->
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="content">
                    <p id="file-name">Filecheck1.csv</p>
                    <p id="file-name-2" class="hidden" >Filecheck1.xlsx</p>
                    <span aria-hidden="true">
                            <img src="{{ asset('icons/code-error 1.png') }}" alt="close">
                        </span>
                    <p id="number-file-error">ファイルに<strong>3</strong>つのエラーがあります</p>
                </div>
                <div id="error" class="text-danger font-weight-bold"></div>
                <div id="success" class="text-success font-weight-bold"></div>
                <div class="uploader">
                    <form id="send-mail-form" action="{{route('file_check.reserve')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row" id="facilities">
                            <ul class="list-error-file text-left" id="list-error-facility">
                            </ul>
                        </div>
                        <div class="row">
                            <div class="container-checkbox" id="check-send-mail">
                                <input type="checkbox" id="check_reserve" name="check_reserve">
                                <label for="check_reserve">保留</label>
                            </div>
                        </div>
                        <div class="row" id="title-send-mail">
                            <h4>保留理由</h4>
                        </div>
                        <div class="row">
                            <textarea name="reason" id="reason" style="resize:none" placeholder="理由を入力してください"></textarea>
                        </div>
                        <div class="row justify-content-between">
                            <button type="submit" id="file-upload-form-button2"
                                    class="home-modal-button default-button">閉じる
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <p id="file-name">Filecheck1.csv</p>
                <p id="file-name-2" class="hidden" >Filecheck1.xlsx</p>
                <img class="home-modal-icon" src="{{ asset('icons/double-check-success.svg') }}" alt="question-mark">
                <p class="modal-text"> ファイルにエラーはありません。</p>
                <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>

<!-- file scrift -->
<!-- scrip -->
<script type="text/javascript" src="{{ asset('js/uploadFileHome.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
<script>
    const container = document.getElementById("wrapper-toggle-btn")
    const toggleInput = document.getElementById("toggle-input")
    const toggleBtn = document.getElementById("toggle-btn")
    const modalQuestion = document.getElementById("modalQuestion")

    toggleInput.addEventListener("click", () => {
        if (toggleInput.checked) {
            toggleBtn.style.marginLeft = "16px"
            toggleBtn.style.backgroundColor = "#0F94B5"
            container.style.backgroundColor = "#B5E5F0"
            modalQuestion.addClass("show")
        } else {
            toggleBtn.style.marginLeft = "-2px"
            toggleBtn.style.backgroundColor = "#F5F7FB"
            container.style.backgroundColor = "#7c9aa1"
            modalQuestion.removeClass("show")
        }
    })
</script>
<script>
$(document).ready(function () {
    $('.icon-datepicker-detail').on('click', function () {
        $('#datepicker-history-detail .month_detail_datepicker').datepicker('show');
    })

    var date=new Date();
    var year=date.getFullYear()
    var month=date.getMonth();
    $("#datepicker input").datepicker({
        format: "yyyy/mm",
        startView: "months",
        minViewMode: "months",
        autoclose:true,
        language: 'ja',
        endDate: new Date(year, month, '01')
    });
    $(".file-upload-form").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            thisModal.find("#preloader").remove();
            thisModal.modal('hide');
            if (data.data.errors.length > 0){
                var html = '';
                $.each(data.data.errors, function (key, value) {
                    html += ' <li class="item-error-file">\n' +
                        '                 <label for="id' + key + '">' + value['error_position'] + '：' + value['message'] + '</label>\n' +
                        '    </li>'
                });
                $('#list-error-facility').empty();
                $('#list-error-facility').append(html);
                $('#number-file-error strong').text(data.data.errors.length);
                $('#file_check').remove();
                $("#modalError #success").empty();
                $("#modalError #error").empty();
                if(typeof data.data.file_name['csv'] != "undefined") {
                    $("#modalError #file-name").text(data.data.file_name['csv']);
                    $("#modalError #file-name-2").removeClass('hidden');
                    $("#modalError #file-name-2").text(data.data.file_name['excel']);
                } else {
                    $("#modalError #file-name").text(data.data.file_name);
                }
                $('#send-mail-form').append("<input type='hidden' id='type_check_id' name='type_check_id' value='" + data.data.type_check_id + "'>");
                if(typeof data.data.file_id['csv'] != "undefined") {
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['csv'] + "'>");
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['excel'] + "'>");
                } else {
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                }
                var type = thisModal.find('input[name="type"]').val();

                if(type == {{\App\Consts::TYPE_CHECK_NUMBER_10}}) {
                    $('#modalError .justify-content-between').html('<button type="button" id="button-success" class="home-modal-button default-button col-6" name="button" value="ok-check">チェック完了</button><button type="button" id="file-upload-form-button2"class="home-modal-button default-button col-5" value="done-check">閉じる</button>');
                }
                $('#modalError').modal('show');

            } else {
                if(typeof data.data.file_name['csv'] != "undefined") {
                    $("#modalSuccess #file-name").text(data.data.file_name['csv']);
                    $("#modalSuccess #file-name-2").removeClass('hidden');
                    $("#modalSuccess #file-name-2").text(data.data.file_name['excel']);
                } else {
                    $("#modalSuccess #file-name").text(data.data.file_name);
                }
                $('#modalSuccess').modal('show');
            }
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });
    $('.file-upload-form input').change(function () {
        let dataType = $(this).attr('data-type');
        var thisModal = $(this).closest('.modal');
        if (dataType ==  {{App\Consts::UPLOAD_TYPE['BOTH']}}) {
            if (thisModal.find("input").filter(function () {
                return $.trim($(this).val()).length == 0
            }).length != 0) {
                thisModal.find('.file-upload-form-button').attr('disabled', 'disabled')
            }
        }
    });

    $('.file-drag-csv').click(function () {
        var thisModal = $(this).closest('.modal');
        let dataType = $(this).attr('data-type');
        if(dataType == {{App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV']}}) {
            thisModal.find('.file-upload-test-excel').attr('disabled', 'disabled')
            thisModal.find('.file-upload-test-csv').attr('disabled', false);
            thisModal.find('.file-upload-test-excel').val('');
            thisModal.find('.start-excel').removeClass('hidden');
            thisModal.find('.file-drag-excel').addClass('label-file-drag');

        }
    });
    $('.file-drag-excel').click(function () {
        var thisModal = $(this).closest('.modal');
        let dataType = $(this).attr('data-type');
       if(dataType == {{App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV']}}) {
            thisModal.find('.file-upload-test-csv').attr('disabled', 'disabled')
            thisModal.find('.file-upload-test-csv').val('');
            thisModal.find('.file-upload-test-excel').attr('disabled', false);
            thisModal.find('.start-csv').removeClass('hidden');
            thisModal.find('.file-drag-csv').addClass('label-file-drag');
        }
    });

    $('#check_reserve').change(function (event){
        var thisModal = $(this).closest('.modal');
        if(this.checked) {
            thisModal.find('#reason').removeAttr('disabled');
            thisModal.find('#file-upload-form-button2').attr('disabled', 'disabled');
            thisModal.find('#reason').on('input', function (event){
                if($(this).val().length > 0) {
                    thisModal.find('#file-upload-form-button2').removeAttr('disabled');
                } else {
                    thisModal.find('#file-upload-form-button2').attr('disabled', 'disabled');
                }
            })
        } else {
            thisModal.find('#reason').attr('disabled', 'disabled');
            thisModal.find('#file-upload-form-button2').removeAttr('disabled');
            thisModal.find('#reason').val("");
        }
    })
    $(document).on('click', '#send-mail-form button', function (event) {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault();
            var formData = new FormData($('#send-mail-form')[0]);
            formData.append('button', $(this).val());
            $("#modalError #success").empty();
            $("#modalError #error").empty();
            $.ajax({
                url: $('#send-mail-form').attr('action'),
                type: 'POST',
                data: formData,
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
            }).done(function (data) {
                $("#preloader").remove();
                {{--alert('{{\App\Messages::RESERVE_SUCCESS}}')--}}
                location.reload();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                if (jqXHR.responseJSON.validator == true) {
                    var messages = jqXHR.responseJSON.message
                    var text = '';
                    $.each(messages, function (index, value) {
                        text += value + "</br>";
                    });
                    $("#modalError #error").html(text)
                } else {
                    $("#modalError #error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
                }
            });
    });

    $("#file-status-question").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            // async: false,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            location.reload();
            thisModal.modal('hide');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });
    $('.icon-datepicker').on('click', function () {
        $('#datepicker input').trigger('focus');
    })
    $('.modal').on('hidden.bs.modal', function () {
        var thisModal = $(this).closest('.modal');
        thisModal.find('[type=file]').val('');
        thisModal.find('.file-response').addClass('hidden');
    });

    $('#modalQuestion').on('hidden.bs.modal', function () {
        $('#toggle-btn').css('margin-left',"-2px");
        $('#toggle-btn').css('background-color',"rgb(245, 247, 251)");
    });
    $('#modalQuestion').on('show.bs.modal', function () {
        $('#toggle-btn').css('margin-left',"16px");
        $('#toggle-btn').css('background-color',"rgb(15, 148, 181)");
    });
    $('#modalSuccess').on('hidden.bs.modal', function () {
        location.reload();
        var formData = new FormData($(this)[0]);

    });

});
</script>
<script>
    // $('.modal.show').modal({
    //     backdrop: 'static',
    //     keyboard: true,
    //     show: true
    // });
</script>

<script>
$(document).ready(function () {
    $(document).on('click', '.wrapper-button-loadmore a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var thisModal = $(this).closest('.modal');
        var month = thisModal.find('.month_detail_datepicker').val();
        var type_check = thisModal.find('.type_check_detail').val();
        $.ajax({
            url: '/file_check/'+type_check+'?page=' + page,
            type: 'get',
            data: {
                type_check: type_check,
                month: month
            },
            datatype: 'html',
        }).done(function (data) {
            thisModal.find('.wrapper-button-loadmore').remove();
            thisModal.find('.wrapper-detail-check').append(data)
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        });
    });
});

</script>
@endsection
