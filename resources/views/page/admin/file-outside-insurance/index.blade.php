@extends('layouts.admin')
@section('title', '保険外中心業務区分管理')
@section('content')
    <style>
        .number-chartLegend1 {
        white-space: nowrap;
        margin-left: 10px;
    }
    .search-button {
        width: 50px !important;
    }
    .w-428 {
        width: 428px;
    }
    .h-70 {
        height: 70%;
    }
    .select2-selection {
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple {
        line-height: 26px !important;
        font-size: 16px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display: none;
    }

    .select2-results__option--selectable {
        font-size: 16px;
    }
    .table-condensed {
        width: 207px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background: #b5e5f0 !important;
        border-radius: 5px !important;
        padding: 5px 20px 5px 5px !important;
        border: none !important;
        align-items: center;
        max-width: 90% !important;
    }

    .select2-selection--multiple .select2-selection__choice__remove {
        left: unset !important;
        top: 5px !important;
        right: 0;
    }
    .select2-selection--multiple .select2-selection__choice__remove span {
        display: none;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        top: 10px !important;
        right: 4px !important;
        width: 18px !important;
        height: 18px !important;
        background: url(/icons/delete.svg) center center no-repeat !important;
    }
    .selection-heading-comparison .select2-container {
        width: 100% !important;
    }

    .selection-heading-comparison .search-department {
        width: 100% !important;
    }

    .selection-heading-comparison {
        padding: 0px 8px;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
    .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background: rgba(124, 154, 161, 0.4) !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        border-radius: 45% !important;
        border-right: none !important;
    }

    .select2-container--default .select2-search--inline .select2-search__field {
        margin-top: 13px !important;
        margin-left: 15px !important;
        height: 24px !important;
        font-size: 16px;
        margin-bottom: 5px;
    }
    .selection-heading-error {
        width: 216px;
        min-height: 50px;
        position: relative;
        border: none;
        box-sizing: border-box;
        border-radius: 5px;
        margin-left: 15px;
        font-size: 16px;
    }
    .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
        width: 100%;
        margin-top: 13px;
        margin-left: 15px;
    }

    .select2-container .select2-selection--multiple {
        min-height: 50px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding:12px 22px !important;
    }
    button:disabled,
    button[disabled] {
        background-color: #cccccc;
        color: #666666;
    }

    .table thead th {
        vertical-align: middle !important;
    }

    .button-view-detail {
            white-space: nowrap;
    }
    button.close-modal-day-off {
        background: #B5E5F0;
        border-radius: 5px;
    }
    button.submit-modal-day-off {
        background: linear-gradient(96.42deg, #2BC0E4 19.29%, #0F94B5 104.66%);
        border-radius: 5px;
    }
    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da !important;
    }

    /*style radio button modal*/
    .form-radio-check [type="radio"]:checked,
    .form-radio-check [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    .form-radio-check [type="radio"]:checked + label,
    .form-radio-check [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    .form-radio-check [type="radio"]:checked + label:before,
    .form-radio-check [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 20px;
        height: 20px;
        border: 1px solid #2bc0e4;
        border-radius: 100%;
        background: #fff;
    }
    .form-radio-check [type="radio"]:checked + label:after,
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 12px;
        height: 12px;
        background: #2bc0e4;
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    .form-radio-check [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
    .insurance .selection-heading {
        height: 100%;
    }
    input.form-control {
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        height: 100%;
    }
    .insurance .form-check-label {
        color: #222222 !important;
    }
    label.form-check-label {
        font-size: 16px;
    }
    .modal-body.home-modal-body {
        padding-top: 40px;
    }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">保険外中心業務区分管理</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" method="get" autocomplete="off">
                    <div id="date-type" class="wrapper-search">
                        <input type="text" class="input-search" value="{{request('search') ?? ''}}" placeholder="区分名称" id="search" name="search" aria-placeholder="特定" maxlength="100">
                        <button class="button-search" type="button" disabled>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                    d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                    fill="#7C9AA1"/>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div>
                    <div class="search-button" style="width:50px; margin-left:15px">
                        <button class="button-search" type="submit" >
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <button type="button" data-keyboard="false" class="button-add-branch default-button" id="button-create-file-outside-insurance" data-toggle="modal" data-target="#create-file-outside-insurance">新規追加</button>
                </form>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th scope="col" style="width:7%">順番</th>
                        <th scope="col">区分名称</th>
                        <th scope="col">おまかせ対象</th>
                        <th scope="col">最新更新日</th>
                        <!-- <th scope="col">配信回数</th> -->
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($results as $key => $result)
                    <tr>
                        <td >{{($results->currentPage() - 1) * $results->perPage() + $key +1}}</td>
                        <td >
                            {{$result->category_name}}
                        </td>
                        <td >
                            {{$result->fiduciary_goal == 1 ? '対象' : '対象外'}}
                        </td>
                        <td style="white-space: nowrap;">{{date('Y/m/d', strtotime($result->date_update))}}</td>
                        <td>
                            <button type="button" class="button-view-detail button-edit-file-outside-insurance" data-id="{{$result->id}}"
                                data-toggle="modal" data-target="#edit-file-outside-insurance">
                                <span class="text-view-detail">編集</span>
                                <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                            </button>
                            <button type="button" class="button-view-detail button-delete-file-outside-insurance" data-toggle="modal"
                                data-target="#modalDeleteAlert" data-backdrop="static" data-keyboard="false"
                                data-id="{{$result->id}}">
                                <span class="text-view-detail">削除</span>
                                <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                            </button>
                        </td>
                    </tr>
                    @empty
                        <tr class="text-center">
                            <td style="display: revert;" colspan="6">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                {{ $results->appends(request()->except('page'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
    </section>
    </div>
    <!-- Modal D1003.1 -->
    <div class="modal fade home-modal-content" id="create-file-outside-insurance" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-652 insurance" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">保険外中心業務</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700 pb-0">
                    <div class="add-employee">
                        <form id="form-file-outside-insurance" method="post">
                            @csrf
                            <div class="d-flex align-items-center justify-content-center mb-30 modal-view-day-off">
                            <div class="container">
                                <div class="row justify-content-md-center">
                                    <div class="justify-content-center selection-heading w-428 ml-0">
                                        <label class="label-form" for="business-selection">区分名称</label>
                                        <div>
                                            <input type="text" class="form-control create-category-name"
                                                name="createCategoryName" placeholder="区分名称" maxlength="100">
                                            <div class="text-danger mt-2 file-outside-insurance-validate"></div>
                                        </div>
                                    </div>
                                    <div class="justify-content-center selection-heading h-auto w-428 ml-0 mt-15 mb-30">
                                        <label class="label-form" for="business-selection">おまかせ対象</label>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-check form-radio-check pl-0">
                                                    <input class="form-check-input select-checked" type="radio" name="createFiduciaryGoal" value="1" id="flexRadioDefault1">
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        対象
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-check form-radio-check pl-0">
                                                    <input class="form-check-input select-checked" type="radio" name="createFiduciaryGoal" value="2" id="flexRadioDefault2">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        対象外
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-danger mt-2 day-off-type-validate"></div>
                                    </div>
                                </div>
                                <div class="row justify-content-md-center">
                                    <button type="submit" class="btn submit-modal-day-off default-button w-428" id="submit-create-file-outside-insurance">作成</button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1003.1 -->
    <!-- Modal D1003.2 -->
    <div class="modal fade" id="modalCreateAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalCreateAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content pb-50">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">情報を作成</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body pb-0">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 情報の作成が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-outside-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.2 -->
    <!-- Modal D1002.3 -->
    <div class="modal fade home-modal-content" id="edit-file-outside-insurance" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-652 insurance" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">保険外中心業務</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700 pb-0">
                    <div class="add-employee">
                        <form id="form-edit-file-outside-insurance" method="post">
                            @csrf
                            <div class="d-flex align-items-center justify-content-center mb-30 modal-view-day-off">
                            <div class="container">
                                <div class="row justify-content-md-center">
                                    <div class="justify-content-center selection-heading w-428 ml-0">
                                        <label class="label-form" for="business-selection">区分名称</label>
                                        <div>
                                            <input type="text" class="form-control edit-category-name"
                                                name="editCategoryName" placeholder="区分名称" id="editCategoryName" maxlength="100">
                                            <div class="text-danger mt-2 file-outside-insurance-validate"></div>
                                        </div>
                                    </div>
                                    <div class="justify-content-center selection-heading h-auto w-428 ml-0 mt-15 mb-30">
                                        <label class="label-form" for="business-selection">おまかせ対象</label>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-check form-radio-check pl-0">
                                                    <input class="form-check-input select-checked" type="radio" value="1" name="editFiduciaryGoal" id="edit-fiduciaryGoal">
                                                    <label class="form-check-label" for="edit-fiduciaryGoal">
                                                        対象
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-check form-radio-check pl-0">
                                                    <input class="form-check-input select-checked" type="radio" name="editFiduciaryGoal" value="2" id="edit-fiduciaryGoal2">
                                                    <label class="form-check-label" for="edit-fiduciaryGoal2">
                                                        対象外
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-danger mt-2 day-off-type-validate"></div>
                                    </div>
                                </div>
                                <div class="row justify-content-md-center">
                                    <button type="submit" class="btn submit-modal-day-off default-button w-428" id="submit-edit-file-outside-insurance">保存</button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.3 -->
    <!-- Modal D1000.4 -->
    <div class="modal fade" id="modalEditAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalEditAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">情報を編集</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 情報の編集が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-outside-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.4 -->
    <!-- Modal D1002.5 -->
    <div class="modal fade" id="modalDeleteAlert" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlert" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">情報を削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                            <input type="hidden" name="id" id="id-file-outside-insurance">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text"> @if(!request('type') || request('type') == 'role-admin')情報の削除がいかがでしょうか。 @else この使用者を事業所から削除してもよろしいですか？ @endif</p>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                                <button type="submit" class="w-205 default-button" id="submit-form-delete">削除</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.5 -->
    <!-- Modal D1002.6 -->
    <div class="modal fade" id="modalDeleteAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">情報を削除</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 情報の削除が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-outside-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.6 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function () {
            $('#button-create-file-outside-insurance').click(function () {
                $('.create-category-name').val('');
                $('#flexRadioDefault1').prop('checked', true);
                $('div.file-outside-insurance-validate').text('');
                $('#create-file-outside-insurance').modal({backdrop: 'static', keyboard: false});
            });
            $('#submit-create-file-outside-insurance').click(function (e) {
                e.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                var formData = new FormData($('#form-file-outside-insurance')[0]);
                $.ajax({
                    url: '{{route("file-outside-insurance.store")}}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    contentType: false,
                    processData: false,
                    data : formData,
                    success: function (data) {
                        $('#create-file-outside-insurance').modal('hide');
                        $('#modalCreateAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalCreateAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        var err = JSON.parse(e.responseText);
                        $('div.file-outside-insurance-validate').text(err.message.createCategoryName);
                        $("#preloader").remove();
                    }
                });
            });
            $('.reload-file-outside-insurance').click(function () {
                location.reload();
            });
            $('.button-delete-file-outside-insurance').click(function () {
                $('#id-file-outside-insurance').val($(this).data("id"));
                $('#modalDeleteAlert').modal({backdrop: 'static', keyboard: false});
            })
            $('#submit-form-delete').click(function () {
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route("file-outside-insurance.destroy")}}',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-file-outside-insurance').val()},
                    success: function (data) {
                        $('#modalDeleteAlert').modal('hide');
                        $('#modalDeleteAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function (){
                            $('#modalDeleteAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                        $("#preloader").remove();
                    }
                });
            });
            $('.button-edit-file-outside-insurance').click(function () {
                $('#id-file-outside-insurance').val($(this).data("id"));
                $('div.file-outside-insurance-validate').text('');
                $('#edit-file-outside-insurance').modal({backdrop: 'static', keyboard: false});
                $.ajax({
                    url: '{{route("file-outside-insurance.show")}}',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-file-outside-insurance').val()},
                    success: function (data) {
                        $('input[name="editCategoryName"]').val(data.data.category_name);
                        var $radios = $('input:radio[name=editFiduciaryGoal]');
                        $radios.each(function (key, value) {
                            if ($(value).val() == data.data.fiduciary_goal) {
                                $(value).prop('checked', true);
                            }
                        })
                    },
                    error: function (e) {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                    }
                });
            });
            $('#form-edit-file-outside-insurance').submit(function (e) {
                e.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                var formData = new FormData($(this)[0]);
                formData.append('_method', 'PUT');
                formData.append('id', $('#id-file-outside-insurance').val());
                $.ajax({
                    url: '{{route("file-outside-insurance.update")}}',
                    type: 'POST',
                    data : formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#edit-file-outside-insurance').modal('hide');
                        $('#modalEditAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalEditAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        var err = JSON.parse(e.responseText);
                        $('div.file-outside-insurance-validate').text(err.message.editCategoryName);
                        $("#preloader").remove();
                    }
                });
            });
            $('.close-success-modal').click(function () {
                location.reload();
            });
        });
    </script>
@endsection
