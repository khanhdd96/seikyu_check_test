@extends('layouts.admin')
@section('title', 'エラー統計')
<style>
    .number-chartLegend1 {
        white-space: nowrap;
        margin-left: 10px;
    }
    .search-button {
        width: 50px !important;
    }
    .w-216 {
        width: 216px;
    }
    .h-70 {
        /* height: 70%; */
    }
    .select2-selection {
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple {
        line-height: 26px !important;
        font-size: 16px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display: none;
    }

    .select2-results__option--selectable {
        font-size: 16px;
    }
    .table-condensed {
        width: 207px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background: #b5e5f0 !important;
        border-radius: 5px !important;
        padding: 5px 20px 5px 5px !important;
        border: none !important;
        align-items: center;
        max-width: 90% !important;
    }

    .select2-selection--multiple .select2-selection__choice__remove {
        left: unset !important;
        top: 5px !important;
        right: 0;
    }
    .select2-selection--multiple .select2-selection__choice__remove span {
       display: none;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        top: 10px !important;
        right: 4px !important;
        width: 18px !important;
        height: 18px !important;
        background: url(/icons/delete.svg) center center no-repeat !important;
    }
    .selection-heading-comparison .select2-container {
        width: 100% !important;
    }

    .selection-heading-comparison .search-department {
        width: 100% !important;
    }

    .selection-heading-comparison {
        padding: 0px 8px;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
    .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background: rgba(124, 154, 161, 0.4) !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        border-radius: 45% !important;
        border-right: none !important;
    }

    .select2-container--default .select2-search--inline .select2-search__field {
        margin-top: 13px !important;
        margin-left: 15px !important;
        height: 24px !important;
        font-size: 16px;
        margin-bottom: 5px;
    }
    .selection-heading-error {
        width: 216px;
        min-height: 50px;
        position: relative;
        border: none;
        box-sizing: border-box;
        border-radius: 5px;
        margin-left: 15px;
        font-size: 16px;
    }
    .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
        width: 100%;
        margin-top: 13px;
        margin-left: 15px;
    }

    .select2-container .select2-selection--multiple {
        min-height: 50px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding:12px 22px !important;
    }
    .comparison-label {
        margin-left: 17px;
    }
    button.select2-selection__clear {
        padding-top: 13px;
        position: absolute;
        right: 0;
    }
</style>
@section('content')
@php
    $typeCheck = request('type_check') ? request('type_check') : 3;
    $colors = App\Consts::COLOR_CANVAR;
    $suppliers = array_values(App\Consts::LIST_CONST_CHECK_CODE[$typeCheck]);
    if (request('type') && request('type') == 'comparison') {
        $suppliers = $data['comparisonErrors']['names'];
    }
    $suppliers = json_encode($suppliers, JSON_UNESCAPED_UNICODE);
    $canvartValue = [];
    $arrayMonth = [];
@endphp
<div class="main-content main-admin">
    <div class="main-heading mb-30">
        <h1 class="title-heading">エラー統計</h1>
    </div>

    <nav class="wrapper-main-tab d-flex align-items-center justify-content-between">
        <div class="nav nav-tabs main-nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'overview' || request('type') == null) active @endif" id="nav-overview-tab" data-toggle="tab" href="#nav-overview" role="tab"  onclick="setGetParameter('type', 'overview')" aria-controls="nav-overview" aria-selected="@if(request('type') == 'overview' || request('type') == null) true @endif">概要</a>
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'detail') active @endif" id="nav-detail-tab" data-toggle="tab" href="#nav-detail" role="tab" onclick="setGetParameter('type', 'detail')" aria-controls="nav-detail" aria-selected="@if(request('type') == 'detail') true @endif">詳細</a>
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'comparison') active @endif" id="nav-comparison-tab" data-toggle="tab" href="#nav-comparison" role="tab" onclick="setGetParameter('type', 'comparison')" aria-controls="nav-comparison" aria-selected="@if(request('type') == 'comparison') true @endif">比較</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent" data-supplier="{{$suppliers}}">
        <!-- Wrapper tab -->
        @if(request('type') == 'overview' || request('type') == null)
        <div class="tab-pane fade show @if(request('type') == 'overview'  || request('type') == null) show active @endif" id="nav-overview" role="tabpanel" aria-labelledby="nav-office-tab">
            <div class="main-body">
                <div class="container-overview">
                    <form action="" class="form-overview">
                        <input type="hidden" name="type" value="{{request('type')}}">
                        <div class="d-flex mb-20 justify-content-end">
                            <div class="selection-heading">
                                <select class="form-control dropdown-heading select2 search-department select-department-id" name="department_id" onchange="loadDataFacility(this)">
                                    <option value="">支店選択</option>
                                    @foreach($data['departments'] as $value)
                                        <option {{request('department_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="selection-heading">
                                <select class="form-control dropdown-heading select2 search-facility select-facility-id" name="facility_id">
                                    <option value="">事業所選択</option>
                                    @foreach($data['facilitys'] as $value)
                                        <option {{request('facility_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="filter-heading">
                                <div id="datepicker" class="datepicker-heading">
                                    <input class="input-datepicker" type="text" name="month" placeholder="年/月" value=" {{request('month') ? request('month') : date('Y/m')}}">
                                    <span class="icon-datepicker">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <div class="search-button">
                                <button class="button-search" type="submit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-6">

                            @foreach(App\Consts::TITLE as $key => $value)
                            @if($key <= 6)
                            <div class="col-management col-overview-block" style="position: relative">
                                @if(!in_array(App\Consts::TYPE_MENU[$key], App\Consts::CNC_OPEN))
                                    <div style="
                                        width: 100%;
                                        height: 100%;
                                        display: inline-block;
                                        position: absolute;
                                        background-color: #f5ebeb7d;
                                        z-index: 111;"
                                    ></div>
                                @endif
                                <article class="overview-block">
                                    <div class="inner-management-block">
                                        <h4 class="title-management-block"><a href="{{route('check-statistics.index', ['type' => 'detail', 'facility_id' => request('facility_id'), 'type_check' => App\Consts::TYPE_MENU[$key], 'department_id' => request('department_id')])}}">{{App\Consts::TITLE_NUMBER[$key]}}{{$value}}</a></h4>
                                    </div>

                                    <div class="inner-management-block context-management-block">
                                        <div class="left-context">エラーの発生回数</div>
                                        <div class="right-context">
                                            <p class="text-right-context">{{ $data['typeChecks'][App\Consts::TYPE_MENU[$key]]}}</p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            @endif
                            @endforeach
                        </div>

                        <div class="col-lg-6">
                            @foreach(App\Consts::TITLE as $key => $value)
                            @if($key > 6)
                            <div class="col-management col-overview-block" style="position: relative">
                                @if(!in_array(App\Consts::TYPE_MENU[$key], App\Consts::CNC_OPEN))
                                    <div style="
                                        width: 100%;
                                        height: 100%;
                                        display: inline-block;
                                        position: absolute;
                                        background-color: #f5ebeb7d;
                                        z-index: 111;"
                                    ></div>
                                @endif
                                <article class="overview-block">
                                    <div class="inner-management-block">
                                        <h4 class="title-management-block"><a href="{{route('check-statistics.index', ['type' => 'detail', 'facility_id' => request('facility_id'), 'type_check' => App\Consts::TYPE_MENU[$key], 'department_id' => request('department_id')])}}">{{App\Consts::TITLE_NUMBER[$key]}}{{$value}}</a></h4>
                                    </div>

                                    <div class="inner-management-block context-management-block">
                                        <div class="left-context">エラーの発生回数</div>
                                        <div class="right-context">
                                            <p class="text-right-context">{{ $data['typeChecks'][App\Consts::TYPE_MENU[$key]]}}</p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Wrapper tab -->
        @endif
        <!-- Wrapper tab -->
        @if(request('type') == 'detail')
        @php
            $detailErrorYear = $data['detailErrors']['year'];
            $detailErrorMonth = $data['detailErrors']['month'];
            $canvartValue = array_values($detailErrorYear);
            $arrayMonth = array_values($detailErrorMonth);
        @endphp
        <div class="tab-pane fade @if(request('type') == 'detail') show active @endif" id="nav-detail" role="tabpanel" aria-labelledby="nav-accounting-tab">
            <div class="main-body">
                <div class="container-chart">
                    <form action="" class="form-detail">
                        <input type="hidden" name="type" value="{{request('type')}}">
                        <div class="d-flex mb-20 justify-content-end">
                            <div class="selection-heading">
                                <select class="form-control dropdown-heading select2 search-department select-department-id" name="department_id" onchange="loadDataFacility(this)">
                                    <option value="">支店選択</option>
                                    @foreach($data['departments'] as $value)
                                        <option {{request('department_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="selection-heading">
                                <select class="form-control dropdown-heading select2 search-facility select-facility-id" name="facility_id">
                                    <option value="">事業所選択</option>
                                    @foreach($data['facilitys'] as $value)
                                        <option {{request('facility_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="filter-heading">
                                <div id="datepicker-year" class="datepicker-heading">
                                    <input class="input-datepicker" type="text" name="year" placeholder="年" value=" {{request('year') ? request('year') : date('Y')}}">
                                    <span class="icon-datepicker">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <div class="selection-heading">
                                <select class="form-control dropdown-heading select2" name="type_check">
                                    <option value="{{App\Consts::TYPE_MENU[6]}}">障害チェック</option>
                                    @foreach(App\Consts::TITLE as $key => $value)
                                        @if($key != 6)
                                            <option {{request('type_check') == App\Consts::TYPE_MENU[$key] ? 'selected' : ''  }} value="{{App\Consts::TYPE_MENU[$key]}}">{{$value}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="search-button">
                                <button class="button-search" type="submit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>

                        </div>
                    </form>
                    <div class="main-heading mb-20">
                        <h4 class="mint-title-result">
                            @foreach(App\Consts::TITLE as $key => $value)
                                @if(App\Consts::TYPE_MENU[$key] == request('type_check') || (App\Consts::TYPE_MENU[$key] == 3 && !request('type_check')))
                                    {{$value}}
                                @endif
                            @endforeach</h4>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="wrapper-circle-chart bg-chart h-100">
                                <div class="mypiechart mx-auto">
                                    <canvas id="myCanvas2" width="218" height="222"></canvas>
                                    <div class="totalValuePie">
                                        <span>合計</span>
                                        <span>{{array_sum($detailErrorYear)}}</span>
                                    </div>
                                </div>
                                <div id="chartLegend" class="chartLegend mt-65 mb-20">
                                    @php $i = 0; @endphp
                                    @foreach(App\Consts::LIST_CONST_CHECK_CODE[$typeCheck] as $key => $value)
                                    <div class="d-flex align-items-center justify-content-between mt-25" >
                                        <div class="item-chartLegend">
                                            <span class="bg-chartLegend bg-chartLegend1" style="background:{{App\Consts::COLOR_CANVAR[$i]}}"></span>
                                            <span class="txt-chartLegend">{{$value}}</span>
                                        </div>

                                        <div class="number-chartLegend number-chartLegend1" style="color:{{App\Consts::COLOR_CANVAR[$i]}}">{{isset($detailErrorYear[$key]) ? $detailErrorYear[$key] : 0}}</div>
                                    </div>
                                    @php $i++; @endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-8">
                            <div class="wrapper-line-chart bg-chart h-100">
                                <div class="d-flex justify-content-end mb-45">
                                    <div class="selection-heading h-auto" style="width: 100%">
                                        <select class="form-control dropdown-heading select-code-error" style="width: 100%" name="code_error" multiple="multiple">
                                            @php
                                            $i = 0;
                                            @endphp
                                            @foreach(App\Consts::LIST_CONST_CHECK_CODE[$typeCheck] as $key => $value)
                                                <option value="{{$i}}">{{$value}}</option>
                                            @php
                                            $i++;
                                            @endphp
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <canvas id="myChart"></canvas>

                                <!-- <div id="chartLegend" class="chartLegend mt-65 mb-20">
                                    <div class="item-chartLegend mt-25">
                                        <span class="bg-chartLegend bg-chartLegend1"></span>
                                        <span class="txt-chartLegend">エラー１</span>
                                    </div>
                                    <div class="item-chartLegend mt-25">
                                        <span class="bg-chartLegend bg-chartLegend2"></span>
                                        <span class="txt-chartLegend">エラー２</span>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    </div>
            </div>
        </div>
        <!-- Wrapper tab -->
        @endif

        @if(request('type') == 'comparison')
        @php
            $detailErrorMonth = $data['comparisonErrors']['month'];
            $arrayMonth = array_values($detailErrorMonth);
        @endphp
        <!-- Wrapper tab -->
        <div class="tab-pane fade @if(request('type') == 'comparison') show active @endif" id="nav-comparison" role="tabpanel" aria-labelledby="nav-user-tab">
            <form action="">
                <div class="main-body">
                    <div class="container-chart">
                            <input type="hidden" name="type" value="{{request('type')}}">
                            <div class="d-flex mb-20 justify-content-end">
                                <div class="filter-heading">
                                    <div id="datepicker-year" class="datepicker-heading">
                                        <input class="input-datepicker" type="text" name="year" placeholder="年" value="{{request('year') ? request('year') : date('Y')}}">
                                        <span class="icon-datepicker">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                                <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                                <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                                <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                                <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                                <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                                <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                                <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                                <div class="selection-heading">
                                    <select class="form-control dropdown-heading select2" name="type_check">
                                        <option value="">全て</option>
                                        @foreach(App\Consts::TITLE as $key => $value)
                                                <option {{request('type_check') == App\Consts::TYPE_MENU[$key] ? 'selected' : ''  }} value="{{App\Consts::TYPE_MENU[$key]}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="search-button">
                                    <button class="button-search" type="submit">
                                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="main-heading mb-20">
                                @php
                                    $value = "全て";
                                    if(request('type_check')) {
                                        $key = array_search(request('type_check'), App\Consts::TYPE_MENU);
                                        $value = App\Consts::TITLE[$key];
                                    }
                                @endphp
                                <h4 class="mint-title-result">{{$value}}</h4>
                            </div>
                        <div class="row">
                            <label class="label-form comparison-label" for="business-selection">支店名</label>
                            <div class="col-12 pb-10">
                                <div class="selection-heading-comparison selection-heading-comparison-department">
                                    <select class="form-control dropdown-heading search-department select-department-id  select-department-ids" name="department_id[]" multiple="multiple" onchange="loadDataFacility(this)">
                                        @foreach($data['departments'] as $value)
                                            <option {{!empty(request('department_id')) && in_array($value->id, request('department_id')) ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                             <label class="label-form comparison-label" for="business-selection">事業所名</label>
                            <div class="col-12 pb-20">
                                    <div class="selection-heading-comparison">
                                        <select class="form-control dropdown-heading search-facility select-facility-id select-facility-ids" multiple="multiple" name="facility_id[]" placeholder="事業所名">
                                            @foreach($data['facilitys'] as $value)
                                                <option {{!empty(request('facility_id')) && in_array($value->id, request('facility_id')) ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="wrapper-line-chart bg-chart h-70">
                                    <label class="label-form comparison-label" for="business-selection">エラー種類</label>
                                    <div class="d-flex justify-content-end mb-45">
                                        <div class="selection-heading-error" style="width: 100%">
                                            <select class="form-control dropdown-heading select-code-error-2" style="width: 100%" name="code_error[]"  multiple="multiple">
                                                @if (request('type_check'))
                                                    @foreach(App\Consts::LIST_CONST_CHECK_CODE[$typeCheck] as $key => $value)
                                                        <option  {{!empty(request('code_error')) && in_array($key, request('code_error')) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                @else
                                                    @foreach(App\Consts::LIST_CONST_ERROR_CHECK_CODE as $key => $value)
                                                        <option  {{!empty(request('code_error')) && in_array($key, request('code_error')) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- @php
                                        $status = false;
                                        if (request('department_id') && count(request('department_id')) > 1 && !request('facility_id')) {
                                            $status = true;
                                        }
                                        if (request('department_id') && count(request('department_id')) > 0 && request('facility_id') && count(request('facility_id')) > 1) {
                                            $status = true;
                                        }
                                        if (request('department_id') && count(request('department_id')) > 1 && request('facility_id') &&  count(request('facility_id')) > 1) {
                                            $status = true;
                                        }
                                    @endphp --}}
                                    @php
                                        $status = true;
                                        if (request('department_id') && count(request('department_id')) > 1 && request('facility_id') &&  count(request('facility_id')) < 2) {
                                            $status = false;
                                        }
                                    @endphp

                                    <canvas id="myChart" style="margin-bottom: 15px;"></canvas>
                                    {{--@if($status)
                                        <canvas id="myChart"></canvas>
                                    @else
                                        <div class="text-center">
                                            <img src="{{asset('/icons/empty.png')}}" alt="">
                                            <p class="mt-5" style="font-size: 16px">比較データ未選択</p>
                                        </div>
                                    @endif --}}
                                    <!-- <div id="chartLegend" class="chartLegend mt-65 mb-20">
                                        <div class="item-chartLegend mt-25">
                                            <span class="bg-chartLegend bg-chartLegend1"></span>
                                            <span class="txt-chartLegend">エラー１</span>
                                        </div>
                                        <div class="item-chartLegend mt-25">
                                            <span class="bg-chartLegend bg-chartLegend2"></span>
                                            <span class="txt-chartLegend">エラー２</span>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <!-- Wrapper tab -->
        @endif
    </div>
</div>
    <!-- scrip -->
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.ja.min.js')}}" integrity="sha512-zI0UB5DgB1Bvvrob7MyykjmbEI4e6Qkf5Aq+VJow4nwRZrL2hYKGqRf6zgH3oBQUpxPLcF2IH5PlKrW6O3y3Qw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chart.js') }}"></script>
<script type="text/javascript">
    var values = {{JSON_encode($canvartValue)}};
    if(values.length < 2) {
        values.push(0);
    }
    var colors = [];

    @foreach(\App\Consts::COLOR_CANVAR as $color)
        colors.push('{{$color}}');
    @endforeach


    var obj2 = {
        values: values,
        colors: colors,
        animation: false,
        fillTextData: true,
        fillTextColor: "#fff",
        fillTextAlign: 1.3,
        fillTextPosition: "horizontal",
        doughnutHoleSize: 60,
        doughnutHoleColor: "#f9f9f9",
        offset: null,
    };

    //Generate myCanvas1
    generatePieGraph("myCanvas2", obj2);

</script>
<script type="text/javascript" src="{{ asset('js/lib-chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/line-chart1.js') }}"></script>
<script>
     function setGetParameter(paramName, paramValue) {
        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
        url += '?' + paramName + '=' + paramValue;
        window.location.href = url;
    }

    function loadDataFacility(select)
    {
        var type = '{{request("type")}}';
        var departmentId = $('.select-department-id').val();
        $.ajax({
            url: '{{route('get-list-facilyties')}}',
            type: 'POST',
            data: {
                department_id : departmentId
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            dataType: "json",
        }).done(function (data) {

            var html = '<option value="">事業所選択</option>';
            if (type == 'comparison') {
                html = '';
            }
            var facilityList = data.data;
            var selected = null;
            $.each(facilityList, function(key, value) {
                if (key == 0) {
                    selected = value.id;
                }
                html+= '<option value="' + value.id + '">' + value.name + '</option>';
            });
            $('.select-facility-id').html(html);
            $('.select-facility-id').select2({
                placeholder: "事業所名",
                allowClear: true,
                language: {
                    "noResults": function(){
                        return "見つかりません。。。";
                    }
                }
            });
        });
    }
    $(document).ready(function () {
        $('.select2').select2();
        $('.select-code-error').select2({
            placeholder: "全て",
            allowClear: true,
            closeOnSelect: false,
            language: {
                "noResults": function(){
                    return "見つかりません。。。";
                }
            }
        });

        $('.select-code-error-2').select2({
            placeholder: "全て",
            allowClear: true,
            closeOnSelect: false,
            language: {
                "noResults": function(){
                    return "見つかりません。。。";
                }
            }
        });
        $('.select-department-ids').select2({
            allowClear: true,
            language: {
                "noResults": function(){
                    return "見つかりません。。。";
                }
            }
        });

        $('.select-facility-ids').select2({
            allowClear: true,
            language: {
                "noResults": function(){
                    return "見つかりません。。。";
                }
            }
        });
        var date=new Date();
        var year=date.getFullYear()
        var month=date.getMonth();
        $("#datepicker input").datepicker({
            format: "yyyy/mm",
            startView: "months",
            minViewMode: "months",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });

        $("#datepicker-year input").datepicker({
            format: "yyyy",
            startView: "years",
            minViewMode: "years",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });

        $('.select-code-error').change(function(e) {
            var value = $('.select-code-error').val();
            onLegendClicked(e, value);
        })
        $('.icon-datepicker').on('click', function () {
            $('#datepicker input').trigger('focus');
            $('#datepicker-year input').trigger('focus');
        })

    });
</script>
<script>
    var type = '{{request("type")}}';
    var ctx = document.getElementById("myChart").getContext("2d");
    var data = $('#nav-tabContent').attr('data-supplier');
    var datapoints = {{JSON_encode($arrayMonth)}};
    var lineChartColors = [];

    lineChartColors = [];
    @foreach(\App\Consts::COLOR_CANVAR as $color)
        lineChartColors.push('{{$color}}');
    @endforeach

    const legendlabels = [
        "",
        "1月",
        "2月",
        "3月",
        "4月",
        "5月",
        "6月",
        "7月",
        "8月",
        "9月",
        "10月",
        "11月",
        "12月",
    ];
    var datapoints = {{JSON_encode($arrayMonth)}};
    var suppliers = JSON.parse(data);
    function onLegendClicked(e, value) {
        for(let i = 0; i < chart.data.datasets.length; i++) {

            if(check_arr(i, value)) {
                chart.data.datasets[i].hidden = false;
            }
            if(!check_arr(i, value)) {
                chart.data.datasets[i].hidden = true;
            }
            if(value.length == 0) {
                chart.data.datasets[i].hidden = false;
            }
        }
        chart.update();
    };


    function check_arr(element,arr){
        let count = 0;
        for (let i = 0; i < arr.length; i ++){
            if (arr[i] == element)  {
                count ++;
                break
            }
        }
        return (count >0) ? true : false
    }

    var chart = new Chart(ctx, {
        type: "line",
        data: {
            labels: legendlabels,
            datasets: datapoints.map((e, i) => ({
                backgroundColor: lineChartColors[i],
                borderColor: lineChartColors[i],
                fill: false,
                data: e,
                label: suppliers[i],
                lineTension: 0.5,
                pointRadius: 4,
                pointBorderColor: "#FFFFFF",
                pointBorderWidth: 2,
                pointBackgroundColor: lineChartColors[i],
            })),
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: "エラー数",
                    align: "start",
                    color: "#00285A",
                    font: {
                        size: 12,
                        weight: "normal",
                    },
                    padding: { top: 0, left: 0, right: 0, bottom: 30 },
                },
                legend: {
                    display: true,
                    position: "bottom",
                    align: "start",
                    labels: {
                        boxWidth: 15,
                        boxHeight: 15,
                        fontColor: "#222",
                        padding: 30,
                    },
                },
            },
            responsive: true, // Instruct chart js to respond nicely.
            maintainAspectRatio: true, // Add to prevent default behaviour of full-width/height
            interaction: {
                intersect: false,
                axis: "x",
            },
            scales: {
                x: {
                    grid: {
                        color: "#CBCBCB",
                        borderColor: "#000000",
                        tickColor: "#8D8D8D",
                        display: false,
                    },
                    ticks: {
                        color: "#8D8D8D",
                    },
                },
                y: {
                    grid: {
                        color: "#CBCBCB",
                        borderColor: "#333",
                        tickColor: "#8D8D8D",
                        borderDash: [3, 3],
                    },
                    ticks: {
                        color: "#8D8D8D",
                        stepSize: 50
                    },
                    afterDataLimits(scale) {
                        scale.max += 50;
                    },
                },
            },
        },
    });

</script>
@endsection
