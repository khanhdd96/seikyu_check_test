@extends('layouts.admin')
@section('title','請求締めチェックツール')
@section('content')
    <style>
        .select2-selection {
            height: 100% !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display: none;
        }

        .select2-results__option--selectable {
            font-size: 16px;
        }

        span.select2.select2-container.select2-container--default {
            height: 50px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }
        .wrapper-toggle-btn {
            position: relative;
        }
    </style>
    <link rel="stylesheet" href="{{asset('css/type_1_admin.css')}}">
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">請求締めチェックツール</h1>
        </div>
        @if($result['typeCheck'] && $result['typeCheck']->has_error)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                未収金明細ファイルの作成が失敗しまいました。<br/>
                ファイル作り直ししてください。
                <button type="button" class="close" data-dismiss="alert" aria-label="閉じる">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="main-body">
            <div class="option">
                <div class="steps d-flex">
                    <div class="detail-step">
                        <p>テンプレートファイル選択</p>
                        <div class="select-type-file">
                            <div class="form-check form-radio-check">
                                <input class="form-check-input" type="radio" name="type_file" value="early"
                                       id="flexRadioDefault1" {{!isset($result['typeCheck']) || (isset($result['typeCheck']) && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH']) ? 'checked' : 'disabled'}}>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    月初
                                </label>
                            </div>
                            <div class="form-check form-radio-check">
                                <input class="form-check-input" type="radio" name="type_file" value="mid"
                                       {{!isset($result['typeCheck']) || (isset($result['typeCheck']) && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH']) ? 'disabled' : 'checked'}}
                                       id="flexRadioDefault2" data-has-file-mid="">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    月中
                                </label>
                            </div>
                        </div>
                        <button type="button" id="open-modal-upload-file"
                                class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                data-toggle="modal" {{!isset($result['typeCheck']) ||
                                                                 (isset($result['typeCheck']) &&
                                                                  (!$result['typeCheck']->step ||
                                                                   ($result['typeCheck']->step > \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] &&
                                                                    $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'] &&
                                                                     $result['typeCheck']->step != \App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH'])) &&
                                                                    !$result['typeCheck']->is_run_cron && $result['typeCheck']->step != \App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) ? '' : 'disabled' }}>
                            未収金明細<br/>
                            ファイル作成
                        </button>
                    </div>
                    <div class="detail-step">
                        <p>未収金明細ファイル</p>
                        <div class="button">
                            <form action="{{route('check-1-admin.distribution-all-file')}}" method="post">
                                @csrf
                                <button type="button"
                                        {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron || !$result['typeCheck']->step || ($result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']  ? 'disabled' : ''}}
                                        class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                        data-toggle="modal" data-target="#modalQuestionDistributionAll"
                                        data-backdrop="static" data-keyboard="false">
                                    一括配布
                                </button>
                            </form>
                            <button type="button" id="select-facility-distribution-file"
                                    {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron || !$result['typeCheck']->step || ($result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']  ? 'disabled' : ''}}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all">
                                選択配布
                            </button>
                        </div>
                    </div>
                    <div class="detail-step">
                        <p>締め切り設定</p>
                        <div class="date-picker button">
                            <span class="label">
                                一括締切日
                            </span>
                            <div id="datepicker" class="datepicker-heading">
                                <input class="input-datepicker" type="text" autocomplete="off" name="month" {{$result['typeCheck'] && $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'] ? 'disabled' : ''}}
                                       placeholder="年/月/日"
                                       value="{{(isset($result['typeCheck']) && $result['typeCheck']->deadline_mid_month) ? date('Y/m/d', strtotime($result['typeCheck']->deadline_mid_month)) : (isset($result['typeCheck']) && $result['typeCheck']->deadline_early_month ? date('Y/m/d', strtotime($result['typeCheck']->deadline_early_month)) : '')}}">
                                <span class="icon-datepicker" onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                                fill="#7C9AA1"></path>
                                            <path opacity="0.4"
                                                  d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                                  fill="#7C9AA1"></path>
                                            <path
                                                d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                                fill="#7C9AA1"></path>
                                            <path
                                                d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                                fill="#7C9AA1"></path>
                                            <path
                                                d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                                fill="#7C9AA1"></path>
                                            <path
                                                d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                                fill="#7C9AA1"></path>
                                            <path
                                                d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                                fill="#7C9AA1"></path>
                                            <path
                                                d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                                fill="#7C9AA1"></path>
                                        </svg>
                                    </span>
                            </div>
                            <button type="button" id="update-deadline"
                                    {{isset($result['typeCheck']) && ($result['typeCheck']->is_run_cron || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'])  ? 'disabled' : '' }}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                    data-toggle="modal" data-target="" data-backdrop="static" data-keyboard="false">適用
                            </button>
                        </div>
                    </div>
                    <div class="detail-step">
                        <p>コメント確認と処理</p>
                        <div class="button">
                            <button type="button"
                                    {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron || !$result['typeCheck']->step  || (($result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ||  ($result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'])) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']) ? 'disabled' : '' }}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                    data-toggle="modal" data-target="#modalQuestionDistributionReadCommentAll"
                                    data-backdrop="static" data-keyboard="false">コメント一括出力
                            </button>
                            <button type="button"
                                    {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron || !$result['typeCheck']->step  || (($result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ||  ($result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH'])) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']) ? 'disabled' : '' }}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                    data-toggle="modal" id="select-facility-read-comment-file" data-target=""
                                    data-backdrop="static" data-keyboard="false">コメント後処理
                            </button>
                        </div>
                    </div>
                    <div class="detail-step">
                        <p>コメント後ファイル</p>
                        <div class="button">
                            <button type="button"
                                    {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron  || !$result['typeCheck']->step || ($result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] ||  ($result['typeCheck']->step >=\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH']) ? 'disabled' : '' }}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                    data-toggle="modal" data-target="#modalQuestionDistributionAllComment"
                                    data-backdrop="static" data-keyboard="false">一括配布
                            </button>
                            <button type="button"
                                    {{!isset($result['typeCheck']) || $result['typeCheck']->is_run_cron  || !$result['typeCheck']->step || ($result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_EARLY_MONTH'] ||  ($result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_EARLY_MONTH'] && $result['typeCheck']->step < \App\Consts::STEP_TYPE_1_ADMIN['READED_FILE_COMMENT_MID_MONTH']) || $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH']) ? 'disabled' : '' }}
                                    class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                    id="select-facility-distribute-file-after-comment"
                                    data-toggle="modal" data-target="" data-backdrop="static" data-keyboard="false">選択配布
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="template-mail d-flex">
                            <div class="select-template-mail">
                                <p>メール設定</p>
                                <div class="button-select-template-mail">
                                    <button onclick="showPopupSearch(1)"
                                            type="button"
                                            class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                            data-toggle="modal" data-target="#modalAddMail" data-backdrop="static"
                                            data-keyboard="false">配信メール設定<br/>
                                    </button>
                                    <button onclick="showPopupSearch(2)"
                                            type="button"
                                            class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                            data-toggle="modal" data-target="#modalAddMail" data-backdrop="static"
                                            data-keyboard="false">チェックなし<br/>
                                        メール設定
                                    </button>
                                    <button onclick="showPopupSearch(3)"
                                            type="button"
                                            class="mb-10 button-add-branch-1 default-button a-button-add-branch button-swap-all"
                                            data-toggle="modal" data-target="#modalAddMail" data-backdrop="static"
                                            data-keyboard="false">締切日メール設定
                                    </button>
                                </div>
                            </div>
                            <div class="description-template-mail">
                        <span>
                            メール設定機能は記の通りです。<br/>
                            ・各ボタンを押すとメールテンプレートから選択可能<br/>
                            ・「配信メール設定」で「①、③、⑤、⑦」ファイルを作成した後、事業所に配信するメール<br/>
                            ・「チェックなしメール設定」で未収金明細無しの事業所にメールを配信する<br/>
                            ・「締切日メール設定」で「②、⑥」の事業所から締切日以降にコメントファイルをアップロードしない場合は、メールを配信する<br/>
                        </span>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-3 mb-20">--}}
{{--                        <div class="template-mail text-center">--}}
{{--                            <span>組織インポート</span>--}}
{{--                            <button type="button"--}}
{{--                                    class="mb-10 button-add-branch default-button a-button-add-branch button-swap-all"--}}
{{--                                    data-toggle="modal" data-target="#modalUploadID2"--}}
{{--                                    data-backdrop="static" data-keyboard="false">組織インポート--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="container-overview">
                <form action="" class="form-overview">
                    <input type="hidden" name="type" value="{{request('type')}}">
                    <div class="d-flex mb-20 justify-content-end">
                        <div class="selection-heading">
                            <select class="form-control dropdown-heading select2 search-department select-department-id"
                                    name="department_id" onchange="loadDataFacility(this)">
                                <option value="">支店選択</option>
                                @if($result['departments'])
                                    @foreach($result['departments'] as $key => $department)
                                        <option
                                            value="{{ $key }}" {{ request("department_id") == $key ? "selected" : "" }}>{{ $department }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="selection-heading">
                            <select class="form-control dropdown-heading select2 search-facility select-facility-id"
                                    name="facility_id">
                                <option value="">事業所選択</option>
                                @if($result['facilities'])
                                    @foreach($result['facilities'] as $key => $facility)
                                        <option
                                            value="{{ $key }}" {{ request("facility_id") == $key ? "selected" : "" }}>{{ $facility }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="filter-heading">
                            <div id="datepicker2" class="datepicker-heading">
                                <input class="input-datepicker" type="text" name="month" placeholder="年/月"
                                       value=" {{request('month') ? request('month') : date('Y/m')}}">
                                <span class="icon-datepicker" onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                                fill="#7C9AA1"/>
                                            <path opacity="0.4"
                                                  d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                            </div>
                        </div>
                        <div class="search-button ml-3">
                            <button class="button-search" type="submit">
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                    <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                          stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                            </button>
                        </div>
                        <a type="button" data-keyboard="false" class="button-add-branch default-button"
                           href="{{ route("check-1-admin.export-csv", ['month' => request('month'), 'department_id' => request('department_id'), 'facility_id' => request('facility_id')]) }}"
                           id="download-file-multi" style="color: #ffffff;">CSV出力
                        </a>
                    </div>
                </form>

                <div class="top-table">
                </div>
                <form action="{{ route('download-file.all') }}" method="post" autocomplete="off"
                      id="download-multi-file">
                    @csrf
                    <div class="table-responsive">
                        <table class="table table-striped table-admin table-home mt-15 mb-15">
                            <thead>
                            <tr>
                                <th scope="col" style="white-space: nowrap">順番</th>
                                <th scope="col">事業所名</th>
                                <th scope="col">支店名</th>
                                <th scope="col">状況</th>
                                <th scope="col">更新日</th>
                                <th scope="col">ファイル状況</th>
                                <th scope="col">個別締切日</th>
                                <th scope="col" style="text-align: center;">配布対象</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($result['details'] as $key => $detail)
                                <tr class="">
                                    @php
                                        $page = request('page') ?? \App\Consts::BASE_PAGE;
                                    @endphp
                                    <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                    <td>{{$detail->name}}</td>
                                    <td>{{$detail->department->name}}</td>
                                    <td>{{$detail->detailTypeCheck1Admin ? \App\Consts::STATUS_FILE_CHECK_NO_1_ADMIN[$detail->detailTypeCheck1Admin->status] : ''}}</td>
                                    <td style="white-space: nowrap">
                                        {{$detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->updated_at ? date('Y/m/d', strtotime($detail->detailTypeCheck1Admin->updated_at))  : ''}}
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center file-step" style="white-space: nowrap">
                                            @if(!$detail->detailTypeCheck1Admin)
                                                -
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_1)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66602" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M7.232 16.5H12.86V15.436H10.788V6.238H9.808C9.262 6.574 8.604 6.812 7.68 6.966V7.792H9.514V15.436H7.232V16.5Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_2)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66602" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M6.616 16.5H13.056V15.408H10.214C9.696 15.408 9.08 15.45 8.534 15.506C10.956 13.224 12.566 11.138 12.566 9.08C12.566 7.246 11.418 6.056 9.584 6.056C8.282 6.056 7.386 6.658 6.546 7.568L7.288 8.282C7.862 7.61 8.59 7.092 9.43 7.092C10.704 7.092 11.306 7.96 11.306 9.136C11.306 10.886 9.836 12.944 6.616 15.758V16.5Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_3)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66699" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M9.682 16.696C11.516 16.696 12.986 15.59 12.986 13.77C12.986 12.356 12.006 11.446 10.802 11.152V11.096C11.894 10.704 12.622 9.878 12.622 8.632C12.622 6.994 11.362 6.056 9.64 6.056C8.464 6.056 7.554 6.574 6.784 7.274L7.47 8.1C8.044 7.512 8.772 7.092 9.598 7.092C10.662 7.092 11.32 7.736 11.32 8.716C11.32 9.836 10.606 10.676 8.492 10.676V11.67C10.858 11.67 11.684 12.482 11.684 13.714C11.684 14.89 10.816 15.632 9.598 15.632C8.422 15.632 7.652 15.072 7.05 14.442L6.406 15.282C7.078 16.024 8.072 16.696 9.682 16.696Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_4)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66699" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M7.61 12.65L9.948 9.15C10.242 8.646 10.522 8.142 10.774 7.638H10.83C10.802 8.17 10.76 8.996 10.76 9.5V12.65H7.61ZM10.76 16.5H11.964V13.672H13.322V12.65H11.964V6.224H10.55L6.28 12.832V13.672H10.76V16.5Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_5)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66699" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M9.654 16.682C11.39 16.682 13.014 15.408 13.014 13.154C13.014 10.9 11.628 9.892 9.92 9.892C9.304 9.892 8.856 10.046 8.394 10.298L8.646 7.33H12.524V6.224H7.526L7.204 11.04L7.89 11.46C8.478 11.068 8.912 10.858 9.598 10.858C10.886 10.858 11.726 11.726 11.726 13.196C11.726 14.694 10.746 15.618 9.542 15.618C8.352 15.618 7.61 15.072 7.022 14.484L6.392 15.324C7.078 16.01 8.058 16.682 9.654 16.682Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_6)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66699" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M10.102 11.082C11.348 11.082 11.964 11.964 11.964 13.35C11.964 14.75 11.194 15.688 10.214 15.688C8.926 15.688 8.156 14.526 8.002 12.398C8.632 11.46 9.458 11.082 10.102 11.082ZM10.214 16.682C11.81 16.682 13.154 15.338 13.154 13.35C13.154 11.208 12.048 10.13 10.312 10.13C9.5 10.13 8.604 10.606 7.974 11.376C8.03 8.184 9.22 7.106 10.62 7.106C11.25 7.106 11.866 7.4 12.272 7.89L12.972 7.092C12.412 6.49 11.642 6.042 10.578 6.042C8.59 6.042 6.784 7.582 6.784 11.6C6.784 14.988 8.24 16.682 10.214 16.682Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                            @if($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->done_file_7)
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="0.5" y="1.66699" width="19" height="19" rx="9.5"
                                                          stroke="#0F94B5"/>
                                                    <path
                                                        d="M8.772 16.5H10.102C10.256 12.482 10.704 10.088 13.112 7.008V6.224H6.686V7.33H11.656C9.654 10.13 8.94 12.608 8.772 16.5Z"
                                                        fill="#0F94B5"/>
                                                </svg>
                                            @endif
                                        </div>
                                    </td>
                                    <td>

                                        <div class="datepicker-heading w-165 ml-0 deadline">
                                            <input class="input-datepicker deadline_facility_month"
                                                   data-id="{{ $detail->detailTypeCheck1Admin->id ?? '' }}" type="text"
                                                   name="deadline_facility_month" data="" {{$result['typeCheck'] && $result['typeCheck']->step >= \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_AFTER_COMMENT_MID_MONTH'] ? 'disabled' : ''}}
                                                   {{!$detail->detailTypeCheck1Admin ? 'disabled' : ''}} placeholder="年/月/日"
                                                   value="{{$detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->deadline_mid_month ? date('Y/m/d', strtotime($detail->detailTypeCheck1Admin->deadline_mid_month)) : ($detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->deadline_early_month ? date('Y/m/d', strtotime($detail->detailTypeCheck1Admin->deadline_early_month)) : '')}}">
                                            <span class="icon-datepicker" onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                                        fill="#7C9AA1"></path>
                                                    <path opacity="0.4"
                                                          d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                                          fill="#7C9AA1"></path>
                                                    <path
                                                        d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                                        fill="#7C9AA1"></path>
                                                    <path
                                                        d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                                        fill="#7C9AA1"></path>
                                                    <path
                                                        d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                                        fill="#7C9AA1"></path>
                                                    <path
                                                        d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                                        fill="#7C9AA1"></path>
                                                    <path
                                                        d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                                        fill="#7C9AA1"></path>
                                                    <path
                                                        d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                                        fill="#7C9AA1"></path>
                                                </svg>
                                            </span>

                                        </div>
                                    </td>
                                    <td style="white-space: nowrap">
                                        {{$detail->detailTypeCheck1Admin && $detail->detailTypeCheck1Admin->step > \App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH'] ? '配布する' : ' 配布停止'}}
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td style="display: revert; font-size: 16px;" colspan="7" class="text-center">
                                        未収金明細ファイルを作成中
                                    </td>
                                </tr>
                            @endforelse
                        </table>

                    </div>
                </form>
                <nav class="wrapper-pagination">
                    @if($result['details'])
                        {{$result['details']->appends(request()->except('page'))->links('partial.admin.paginate')}}
                    @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadID1" tabindex="-1" role="dialog" aria-labelledby="modalUploadID1Title"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                        <form id="file-upload-form-no1-admin" action="{{route('check-1-admin')}}"
                              method="post" enctype="multipart/form-data"
                              class="file-upload-form-no outer-modal-single">
                            @csrf
                            <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file[]" accept=".xls, .xlsx, .csv"
                                           multiple/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag"
                                           class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div id="listFileError" class="d-none">
                            </div>
                            <div id="list-file-pass" class="d-none"></div>
                            <div id="listFiles"></div>
                            <div class="file-uploaded row">
                                <div class="col-12 text-left file-sample mb-15" id="textSample">
                                    各チェックのアップロード可能ファイリ名称サンプル<br/>
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    公費情報
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    債権一覧照会_国保連
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    債権一覧照会_市区町村
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    債権一覧照会_利用者
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    返戻一覧
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    予実明細
                                </div>
                                {{--                                <div class="col-12 text-left file-name-sample" id="file1Sample">--}}
                                {{--                                    組織図-[出力年月]--}}
                                {{--                                </div>--}}
                            </div>
                            <button type="submit" class="home-modal-button default-button file-upload-form-button"
                                    id="file-upload-form-button-no-2" disabled>チェック
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadID2" tabindex="-1" role="dialog" aria-labelledby="modalUploadID2Title"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                        <form id="file-upload-facility-form" action="{{route('check-1-admin.upload-facilities')}}"
                              method="post" enctype="multipart/form-data"
                              class="file-upload-form-no outer-modal-single">
                            @csrf
                            <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept=".xls, .xlsx"/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag"
                                           class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div id="listFileError" class="d-none">
                            </div>
                            <div id="list-file-pass" class="d-none"></div>
                            <div id="listFiles"></div>
                            <div class="file-uploaded row">
                                <div class="col-12 text-left file-sample mb-15" id="textSample">
                                    各チェックのアップロード可能ファイリ名称サンプル<br/>
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    組織図
                                </div>
                            </div>
                            <button type="submit" class="home-modal-button default-button file-upload-form-button"
                                    id="file-upload-form-button-no-2" disabled>チェック
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalUploadSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modal-tile">一括締切日</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="edit-alert-success">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text" id="text-noti">ファイルのアップロードが完了しました。</p>
                            <button type="button" class="home-modal-button default-button" id="reload-page"
                                    data-dismiss="modal" onclick="location.reload()" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalSettingSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalEditAlertSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modal-tile">一括締切日</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close"
                            onclick="location.reload()">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="edit-alert-success">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text" id="text-noti"> 締め切りの設定が成功しました。</p>
                            <button type="button" class="home-modal-button default-button" id="reload-page"
                                    data-dismiss="modal" onclick="location.reload()" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDistributionFile" tabindex="-1" role="dialog"
         aria-labelledby="modalDistributionFileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">選択配布</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form method="post" action="{{route('check-1-admin.distribution-file')}}">
                            @csrf
                            <div style="display: flex">
                                <div class="wrapper-search mb-10" style="width: 550px">
                                    <input class="input-search key_word" style="z-index: 1;" type="text" maxlength="100"
                                           placeholder="事業所名" name="key_word" value="">
                                    <button class="button-search" type="button" style="cursor: unset;">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"></path>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div class="search-button mb-10">
                                    <button class="search-button" style="width: 50px; margin:0" type="button">
                                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                                stroke="white" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round"></path>
                                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div id="listFacility">
                                <div class="table-responsive hidden-type-check">
                                    <table class="table text-nowrap">
                                        <thead>
                                        <tr>
                                            <th style="text-align: left; padding-left: 17px;padding-right: 40px;">＃</th>
                                            <th width="69%" style="text-align: left">事業所名</th>
                                            <th>
                                                <button type="button" id="select-all-facilities" class="select-all">全て
                                                </button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center mt-40">
                                <button type="button" class="w-205 second-button" data-dismiss="modal"
                                        aria-label="Close">キャンセル
                                </button>
                                <button type="button" class="w-205 default-button" id="distribution_file">適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUploadSuccess" tabindex="-1" role="dialog" aria-labelledby="modalUploadSuccessTile"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">アップロードファイル</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text">アップロード済みです。</p>
                        <button type="button" onclick="location.reload()" class="home-modal-button default-button">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalQuestionDistributionAll" tabindex="-1" role="dialog"
         aria-labelledby="modalQuestionDistributionAllTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">一括配布</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text">全て事業所にファイルを配布しますか。</p>
                    <input type="hidden" name="id" id="facility">
                    <div class="d-flex align-items-center justify-content-center mt-50">
                        <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">
                            キャンセル
                        </button>
                        <button type="button" class="w-205 default-button" id="submit-form">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDistributionFileSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalDistributionFileSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modal-tile">一括配布</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="edit-alert-success">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text" id="text-noti">ファイルの配布が完了しました。</p>
                            <button type="button" class="home-modal-button default-button" id="reload-page"
                                    data-dismiss="modal" onclick="location.reload()" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalReadFileCommentSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalReadFileCommentSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modal-tile">
                        コメント後ファイル
                    </h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="edit-alert-success">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text" id="text-noti">ファイルの読み込みが完了しました。</p>
                            <button type="button" class="home-modal-button default-button" id="reload-page"
                                    data-dismiss="modal" onclick="location.reload()" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalQuestionDistributionReadCommentAll" tabindex="-1" role="dialog"
         aria-labelledby="modalQuestionDistributionReadCommentAllTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">コメント一括出力</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text">事業所のコメントファイルを処理しますか。</p>
                    <input type="hidden" name="id" id="facility">
                    <div class="d-flex align-items-center justify-content-center mt-50">
                        <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">
                            キャンセル
                        </button>
                        <button type="button" class="w-205 default-button" id="submit-form">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalReadCommentSuccess" tabindex="-1" role="dialog"
         aria-labelledby="modalReadCommentSuccessTile"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">コメント確認と処理</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text">ファイルの読み込みが完了しました。</p>
                        <button type="button" onclick="location.reload()" class="home-modal-button default-button">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalReadFileComment" tabindex="-1" role="dialog"
         aria-labelledby="modalReadFileCommentTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">選択配布</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form method="post" action="{{route('check-1-admin.read-file-comment')}}">
                            @csrf
                            <div style="display: flex">
                                <div class="wrapper-search mb-10" style="width: 550px">
                                    <input class="input-search key_word" style="z-index: 1;" type="text" maxlength="100"
                                           placeholder="事業所名" name="key_word" value="">
                                    <button class="button-search" type="button" style="cursor: unset;">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"></path>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div class="search-button mb-10">
                                    <button class="search-button" style="width: 50px; margin:0" type="button">
                                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                                stroke="white" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round"></path>
                                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div id="listFacility">
                                <div class="table-responsive hidden-type-check">
                                    <table class="table text-nowrap">
                                        <thead>
                                        <tr>
                                            <th style="text-align: left; padding-left: 17px;padding-right: 40px;">＃</th>
                                            <th width="69%" style="text-align: left">事業所名</th>
                                            <th>
                                                <button type="button" id="select-all-facilities" class="select-all">全て
                                                </button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center mt-40">
                                <button type="button" class="w-205 second-button" data-dismiss="modal"
                                        aria-label="Close">キャンセル
                                </button>
                                <button type="button" class="w-205 default-button" id="read_file">適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalQuestionDistributionAllComment" tabindex="-1" role="dialog"
         aria-labelledby="modalQuestionDistributionAllCommentTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">
                        コメント後ファイル
                    </h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                    <p class="modal-text">コメント処理後のファイルを配布しますか。</p>
                    <input type="hidden" name="id" id="facility">
                    <div class="d-flex align-items-center justify-content-center mt-50">
                        <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">
                            キャンセル
                        </button>
                        <button type="button" class="w-205 default-button" id="submit-form">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDistributionCommentSuccess" tabindex="-1" role="dialog"
         aria-labelledby="modalDistributionCommentSuccessTile"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">
                        コメント後ファイル
                    </h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text">コメント後ファイルの配布が完了しました。</p>
                        <button type="button" onclick="location.reload()" class="home-modal-button default-button">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDistributeFileAfterComment" tabindex="-1" role="dialog"
         aria-labelledby="modalDistributeFileAfterCommentTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">選択配布</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form method="post" action="{{route('check-1-admin.distribution-file-after-comment')}}">
                            @csrf
                            <div style="display: flex">
                                <div class="wrapper-search mb-10" style="width: 550px">
                                    <input class="input-search key_word" style="z-index: 1;" type="text" maxlength="100"
                                           placeholder="事業所名" name="key_word" value="">
                                    <button class="button-search" type="button" style="cursor: unset;">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"></path>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div class="search-button mb-10">
                                    <button class="search-button" style="width: 50px; margin:0" type="button">
                                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                                stroke="white" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round"></path>
                                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div id="listFacility">
                                <div class="table-responsive hidden-type-check">
                                    <table class="table text-nowrap">
                                        <thead>
                                        <tr>
                                            <th style="text-align: left; padding-left: 17px;padding-right: 40px;">＃</th>
                                            <th width="69%" style="text-align: left">事業所名</th>
                                            <th>
                                                <button type="button" id="select-all-facilities" class="select-all">全て
                                                </button>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center mt-40">
                                <button type="button" class="w-205 second-button" data-dismiss="modal"
                                        aria-label="Close">キャンセル
                                </button>
                                <button type="button" class="w-205 default-button" id="distribution_file">適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('page.admin.template-file.modal-mail-template')

    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            uploadFacilityAdmin('#modalUploadID2')
            $(".input-datepicker").datepicker({
                format: "yyyy/mm/dd",
                startView: "months",
                minViewMode: "days",
                autoclose: true,
                language: 'ja',
                startDate: "now()",
            });
            $('#open-modal-upload-file').click(function () {
                var value = $('input[name=type_file]:checked').val();
                if (value == 'early') {
                    $('#modalUploadID1').modal({backdrop: 'static', keyboard: false})
                    $("#modalUploadID1").modal('show');
                } else {
                    $('#modalUploadID1 form').append('<input type="hidden" name="type" value="' + value + '">')
                    $('#modalUploadID1').modal({backdrop: 'static', keyboard: false})
                    $("#modalUploadID1").modal('show');
                }
            })
        });
        $('#modalUploadID1').on('shown.bs.modal', function (e) {
            $('input[name="file"]').val('');
            uploadType1('#modalUploadID1');
        });
        $('.select-all').click(function () {
            modal = $(this).parents('.modal');
            var numberOfChecked = $('table input:checkbox:checked').length;
            var totalCheckboxes = $('table input:checkbox').length;
            if (numberOfChecked != totalCheckboxes) {
                $('table input').prop('checked', true);
            } else {
                $('table input').prop('checked', false);
                $('table input:checkbox').each(function (key, value) {
                    $(modal).find('form').append('<input type="hidden" id="un_check_' + $(value).val() + '" name="un_check[]" value="' + $(value).val() + '">');
                })
            }
        })
        $('#update-deadline').click(function () {
            var date = $('#datepicker input').val();
            if (date) {
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: "{{route('check-1-admin.update-deadline')}}",
                    type: 'POST',
                    data: {
                        'date': date,
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    dataType: "json",
                }).done(function (data) {
                    $('#modalSettingSuccessID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalSettingSuccessID1').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    $("#preloader").remove();
                    // $('#error').removeClass('d-none');
                });
            }
        })

        $('#modalQuestionDistributionAll #submit-form').click(function () {
            var thisModal = $(this).closest('.modal');
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: "{{route('check-1-admin.distribution-all-file')}}",
                type: 'POST',
                data: [],
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            }).done(function () {
                thisModal.modal('hide');
                $('#modalDistributionFileSuccessID1').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionFileSuccessID1').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        })
        $('#modalDistributionFile #distribution_file').click(function () {
            var thisModal = $(this).closest('.modal');
            var formData = new FormData($('#modalDistributionFile form')[0]);
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: $('#modalDistributionFile form').attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function () {
                thisModal.modal('hide');
                $('#modalDistributionFileSuccessID1').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionFileSuccessID1').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        })
        $('#modalDistributeFileAfterComment #distribution_file').click(function () {
            var thisModal = $(this).closest('.modal');
            var formData = new FormData($('#modalDistributeFileAfterComment form')[0]);
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: $('#modalDistributeFileAfterComment form').attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function () {
                thisModal.modal('hide');
                $('#modalDistributionFileSuccessID1').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionFileSuccessID1').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        })
        $('#modalReadFileComment #read_file').click(function () {
            var thisModal = $(this).closest('.modal');
            var formData = new FormData($('#modalReadFileComment form')[0]);
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: $('#modalReadFileComment form').attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function () {
                thisModal.modal('hide');
                $('#modalReadFileCommentSuccessID1').modal({backdrop: 'static', keyboard: false})
                $('#modalReadFileCommentSuccessID1').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        })
        $('#modalDistributionFile button.search-button').click(function () {
            event.preventDefault();
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalDistributionFile #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility')}}",
                type: 'get',
                data: {
                    'key_word': $('#modalDistributionFile .key_word').val()
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    var checked = '';
                    if (value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                        checked = 'checked';
                    }
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalDistributionFile #listFacility table tbody').append(html);
                $('#modalDistributionFile input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalDistributionFile form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalDistributionFile form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalDistributionFile').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionFile').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $('#select-facility-distribution-file').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalDistributionFile #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility')}}",
                type: 'get',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    var checked = '';
                    if (value.detail_type_check1_admin.step >= '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step >= '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                        checked = 'checked';
                    }
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalDistributionFile #listFacility table tbody').append(html);
                $('#modalDistributionFile input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalDistributionFile form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalDistributionFile form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalDistributionFile').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionFile').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $('#select-facility-read-comment-file').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalReadFileComment #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility-to-read-comment')}}",
                type: 'get',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    var checked = '';
                    if (value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                        checked = 'checked';
                    }
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalReadFileComment #listFacility table tbody').append(html);
                $('#modalReadFileComment input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalReadFileComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalReadFileComment').modal({backdrop: 'static', keyboard: false})
                $('#modalReadFileComment').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $('#select-facility-distribute-file-after-comment').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalDistributeFileAfterComment #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility-distribute-file-after-comment')}}",
                type: 'get',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalDistributeFileAfterComment #listFacility table tbody').append(html);
                $('#modalDistributeFileAfterComment input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalDistributeFileAfterComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalDistributeFileAfterComment').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributeFileAfterComment').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $('#modalDistributeFileAfterComment button.search-button').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalDistributeFileAfterComment #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility-distribute-file-after-comment')}}",
                type: 'get',
                data: {
                    'key_word': $('#modalDistributeFileAfterComment .key_word').val()
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalDistributeFileAfterComment #listFacility table tbody').append(html);
                $('#modalDistributeFileAfterComment input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalDistributeFileAfterComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalDistributeFileAfterComment').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributeFileAfterComment').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $('#modalReadFileComment button.search-button').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $('#modalReadFileComment #listFacility table tbody').empty();
            $.ajax({
                url: "{{route('check-1-admin.get-facility-to-read-comment')}}",
                type: 'get',
                data: {
                    'key_word': $('#modalReadFileComment .key_word').val()
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }).done(function (data) {
                var html = '';
                $.each(data.facilities, function (key, value) {
                    var checked = '';
                    if (value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                        checked = 'checked';
                    }
                    html += '<tr>' +
                        '<td scope="row">' + (key + 1) + '</td>' +
                        '<td>' + value.name + '</td>' +
                        '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                        '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                        '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                        '</button></td>' +
                        '</tr>'
                })
                $('#modalReadFileComment #listFacility table tbody').append(html);
                $('#modalReadFileComment input.toggle-input').click(function () {
                    if ($(this).is(':checked')) {
                        $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                    } else {
                        $('#modalReadFileComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                    }
                })
                $('#modalReadFileComment').modal({backdrop: 'static', keyboard: false})
                $('#modalReadFileComment').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                $('#error').removeClass('d-none');
                $("#preloader").remove();
            });
        })
        $("#file-upload-form-no1-admin").submit(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            thisModal.find("#listFileError").empty();
            thisModal.find("#listFileError").addClass('d-none');
            thisModal.find("#list-file-pass").empty();
            thisModal.find(('#list-file-pass')).addClass('d-none');
            thisModal.find("#error").empty();
            thisModal.find(('#error')).addClass('d-none');
            let fileNames = [
                '予実明細',
                '債権一覧照会_利用者',
                '債権一覧照会_国保連',
                '債権一覧照会_市区町村',
                '公費情報',
                '返戻一覧',
            ]
            var hasAllFile = [];//còn check đủ file chưa
            var checkFile = [];
            var filePass = [];
            var fileFail = [];
            var files = $('#file-upload')[0].files;
            for (var i = 0; i < files.length; i++) {
                var fileName = (files[i].name).split('.')[0];
                checkFile[i] = false;
                fileNames.forEach(function (item, index) {
                    if (fileName.startsWith(item)) {
                        checkFile[i] = true;
                        filePass[i] = files[i].name;
                        if (typeof hasAllFile[fileName] === 'undefined') {
                            hasAllFile[fileName] = true;
                        } else {
                            hasAllFile[fileName] = false;
                        }
                    }
                })
                if (!checkFile[i]) {
                    fileFail[i] = files[i].name;
                }
                // } else {
                //     if (fileName.match(/組織図+[\d]*/i)) {
                //         checkFile[i] = true;
                //         filePass[i] = files[i].name;
                //         if (typeof hasAllFile[fileName] === 'undefined') {
                //             hasAllFile[fileName] = true;
                //         } else {
                //             hasAllFile[fileName] = false;
                //         }
                //     } else {
                //         fileFail[i] = files[i].name;
                //     }
                // }
            }
            if (!hasAllFile.includes('false') && !fileFail.length) {
                validate = true;
            } else {
                thisModal.find('#listFiles').empty();
                thisModal.find("#listFileError").empty();
                thisModal.find(('text-file-pass')).addClass('d-none')
                validate = false;
                var pass = '';
                $.each(filePass, function (key, value) {
                    if (value) {
                        pass += "<div class='file-pass'>" +
                            "<div class='file-response file-response-2 file-name-pass'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                            "<div class='messages-file'>" +
                            "<strong>" + value + "</strong>" +
                            "</div>" +
                            "</div>" +
                            "</div>"
                    }
                });
                var html = '';
                $.each(fileFail, function (key, value) {
                    if (value) {
                        html += "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + value + "</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>正しくファイル名称をアップロードしてください。</div>" +
                            "</div>" +
                            "</div>"
                    }
                })
                thisModal.find("#listFileError").append(html);
                thisModal.find("#list-file-pass").append(pass);
                thisModal.find("#list-file-pass").removeClass('d-none');
                thisModal.find("#listFileError").removeClass('d-none');
                thisModal.find("#error").text("チェックファイルを全てアップロードしてください。");
                thisModal.find(('#error')).removeClass('d-none');
                $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
                $("#preloader").remove();
            }
            if (validate) {
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    thisModal.modal('hide');
                    $('#modalUploadSuccessID1').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    thisModal.find("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    thisModal.find(('#error')).removeClass('d-none');
                    $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
                    $("#preloader").remove();
                });
            }
        });
        $('#modalQuestionDistributionReadCommentAll #submit-form').click(function () {
            var thisModal = $(this).closest('.modal');
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: '{{route('check-1-admin.read-all-file-comment')}}',
                type: 'POST',
                data: [],
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            }).done(function () {
                thisModal.modal('hide');
                $('#modalReadCommentSuccess').modal({backdrop: 'static', keyboard: false})
                $('#modalReadCommentSuccess').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        });
        $("#modalUploadID1").on("hidden.bs.modal", function () {
            var thisModal = $(this).closest('.modal');
            thisModal.find("#listFileError").empty();
            thisModal.find("#listFileError").addClass('d-none');
            thisModal.find("#list-file-pass").empty();
            thisModal.find(('#list-file-pass')).addClass('d-none');
            thisModal.find("#error").empty();
            thisModal.find(('#error')).addClass('d-none');
        });
        $("#modalDistributionFile").on("hidden.bs.modal", function () {
            var thisModal = $(this).closest('.modal');
            thisModal.find("#listFacility tbody").empty();
        });
        $('#modalQuestionDistributionAllComment #submit-form').click(function () {
            var thisModal = $(this).closest('.modal');
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: '{{route('check-1-admin.distribution-all-file-comment')}}',
                type: 'POST',
                data: [],
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            }).done(function () {
                thisModal.modal('hide');
                $('#modalDistributionCommentSuccess').modal({backdrop: 'static', keyboard: false})
                $('#modalDistributionCommentSuccess').modal('show');
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
            });
        });
        $('#formAddMail').submit(function (event) {
            {{--$("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');--}}
            event.preventDefault();
            let formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                let modalSuccess = $('#modalReadFileCommentSuccessID1')
                modalSuccess.find('#text-noti').text('メールテンプレートの設定が完了しました。')
                thisModal.modal('hide')
                modalSuccess.modal('show')
            })
        })
        $('.select2').select2({
            language: {
                inputTooShort: function (args) {

                    return "任意の文字を入力してください。。。";
                },
                noResults: function () {
                    return "見つかりません。。。";
                },
                searching: function () {
                    return "検索しています。。。";
                }
            },
        });

        function loadDataFacility(select) {
            var departmentId = select.value;
            $.ajax({
                url: '{{route('get-list-facilyties')}}',
                type: 'POST',
                data: {
                    department_id: departmentId
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                dataType: "json",
            }).done(function (data) {
                var html = '<option value="">事業所選択</option>';
                ;
                var facilityList = data.data;
                var selected = null;
                $.each(facilityList, function (key, value) {
                    if (key == 0) {
                        selected = value.id;
                    }
                    html += '<option value="' + value.id + '">' + value.name + '</option>';
                });
                $('select[name="facility_id"]').html(html);
                $('select[name="facility_id"]').select2({
                    language: {
                        inputTooShort: function (args) {

                            return "任意の文字を入力してください。。。";
                        },
                        noResults: function () {
                            return "見つかりません。。。";
                        },
                        searching: function () {
                            return "検索しています。。。";
                        }
                    },
                });
            });
        }

        var date = new Date();
        var year = date.getFullYear()
        var month = date.getMonth();
        $("#datepicker2 input").datepicker({
            format: "yyyy/mm",
            startView: "months",
            minViewMode: "months",
            autoclose: true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });
        $(document).ready(function () {
            $('.deadline').on('changeDate', function (event) {

                let input = $(this).find('.input-datepicker')
                var date = input.val();
                let facilityId = input.data('id')
                console.log(date, facilityId)
                if (date) {
                    $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                    $.ajax({
                        url: '{{ route('check-1.update-facility-deadline') }}',
                        method: 'PUT',
                        data: {
                            'date': date,
                            'facility_id': facilityId
                        },
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        dataType: "json",
                    }).done(function () {
                        $('#modalSettingSuccessID1').modal({backdrop: 'static', keyboard: false})
                        $('#modalSettingSuccessID1').modal('show');
                        $("#preloader").remove();
                    }).fail(function () {
                        $("#preloader").remove();
                        alert("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    })
                }

            })

        })
        $("#file-upload-facility-form").submit(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            thisModal.find("#listFileError").empty();
            thisModal.find("#listFileError").addClass('d-none');
            thisModal.find("#list-file-pass").empty();
            thisModal.find(('#list-file-pass')).addClass('d-none');
            thisModal.find("#error").empty();
            thisModal.find(('#error')).addClass('d-none');
            var filePass = [];
            var fileFail = [];
            var file = thisModal.find('#file-upload')[0].files[0];
            if (file.name.includes('組織図')) {
                filePass.push(file.name);
            } else {
                fileFail.push(file.name);
            }
            if (!fileFail.length) {
                validate = true;
            } else {
                thisModal.find('#listFiles').empty();
                thisModal.find("#listFileError").empty();
                thisModal.find(('text-file-pass')).addClass('d-none')
                validate = false;
                var pass = '';
                $.each(filePass, function (key, value) {
                    if (value) {
                        pass += "<div class='file-pass'>" +
                            "<div class='file-response file-response-2 file-name-pass'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                            "<div class='messages-file'>" +
                            "<strong>" + value + "</strong>" +
                            "</div>" +
                            "</div>" +
                            "</div>"
                    }
                });
                var html = '';
                $.each(fileFail, function (key, value) {
                    if (value) {
                        html += "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + value + "</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>正しくファイル名称をアップロードしてください。</div>" +
                            "</div>" +
                            "</div>"
                    }
                })
                thisModal.find("#listFileError").append(html);
                thisModal.find("#list-file-pass").append(pass);
                thisModal.find("#list-file-pass").removeClass('d-none');
                thisModal.find("#listFileError").removeClass('d-none');
                $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
                $("#preloader").remove();
            }
            if (validate) {
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    thisModal.modal('hide');
                    $('#modalUploadSuccessID1').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    thisModal.find("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    thisModal.find(('#error')).removeClass('d-none');
                    $('#modalUploadID1 #file-upload-form-button').prop('disabled', 'disabled');
                    $("#preloader").remove();
                });
            }
        });
    </script>
    <script>
        function showPopupSearch(type) {
            $('.key_word').val('');
            searchTemplate(type);
        }
        $('.home-modal-close').click(function () {
            location.reload();
        })
        $(".modal").on('hide.bs.modal', function(){
            $('input.key_word').val('');
        });
        $('#modalDistributionFile .input-search.key_word').keypress(function () {
            if (event.keyCode == 13) {
                event.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $('#modalDistributionFile #listFacility table tbody').empty();
                $.ajax({
                    url: "{{route('check-1-admin.get-facility')}}",
                    type: 'get',
                    data: {
                        'key_word': $('#modalDistributionFile .key_word').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                }).done(function (data) {
                    var html = '';
                    $.each(data.facilities, function (key, value) {
                        var checked = '';
                        if (value.detail_type_check1_admin.step >= '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step >= '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                            checked = 'checked';
                        }
                        html += '<tr>' +
                            '<td scope="row">' + (key + 1) + '</td>' +
                            '<td>' + value.name + '</td>' +
                            '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                            '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                            '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                            '</button></td>' +
                            '</tr>'
                    })
                    $('#modalDistributionFile #listFacility table tbody').append(html);
                    $('#modalDistributionFile input.toggle-input').click(function () {
                        if ($(this).is(':checked')) {
                            $('#modalDistributionFile form #un_check_' + $(this).val()).remove();
                        } else {
                            $('#modalDistributionFile form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                        }
                    })
                    $('#modalDistributionFile').modal({backdrop: 'static', keyboard: false})
                    $('#modalDistributionFile').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    $('#error').removeClass('d-none');
                    $("#preloader").remove();
                });
            }
        })
        $('#modalDistributeFileAfterComment .input-search.key_word').keypress(function () {
            if (event.keyCode == 13) {
                event.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $('#modalDistributeFileAfterComment #listFacility table tbody').empty();
                $.ajax({
                    url: "{{route('check-1-admin.get-facility-distribute-file-after-comment')}}",
                    type: 'get',
                    data: {
                        'key_word': $('#modalDistributeFileAfterComment .key_word').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                }).done(function (data) {
                    var html = '';
                    $.each(data.facilities, function (key, value) {
                        html += '<tr>' +
                            '<td scope="row">' + (key + 1) + '</td>' +
                            '<td>' + value.name + '</td>' +
                            '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                            '<input type="checkbox" id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                            '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                            '</button></td>' +
                            '</tr>'
                    })
                    $('#modalDistributeFileAfterComment #listFacility table tbody').append(html);
                    $('#modalDistributeFileAfterComment input.toggle-input').click(function () {
                        if ($(this).is(':checked')) {
                            $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                        } else {
                            $('#modalDistributeFileAfterComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                        }
                    })
                    $('#modalDistributeFileAfterComment').modal({backdrop: 'static', keyboard: false})
                    $('#modalDistributeFileAfterComment').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    $('#error').removeClass('d-none');
                    $("#preloader").remove();
                });
            }
        })
        $('#modalReadFileComment .input-search.key_word').click(function () {
            if (event.keyCode == 13) {
                event.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $('#modalReadFileComment #listFacility table tbody').empty();
                $.ajax({
                    url: "{{route('check-1-admin.get-facility-to-read-comment')}}",
                    type: 'get',
                    data: {
                        'key_word': $('#modalReadFileComment .key_word').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                }).done(function (data) {
                    var html = '';
                    $.each(data.facilities, function (key, value) {
                        var checked = '';
                        if (value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_EARLY_MONTH']}}' || value.detail_type_check1_admin.step == '{{\App\Consts::STEP_TYPE_1_ADMIN['DISTRIBUTION_FILE_MID_MONTH']}}') {
                            checked = 'checked';
                        }
                        html += '<tr>' +
                            '<td scope="row">' + (key + 1) + '</td>' +
                            '<td>' + value.name + '</td>' +
                            '<td><button style="margin: 0 auto;" type="button" id="wrapper-toggle-btn1" data-id="2" class="wrapper-toggle-btn checked toggle-btn2">' +
                            '<input type="checkbox" ' + checked + ' id="toggle-input' + key + '" name="facility[]" value="' + value.id + '" class="toggle-input">' +
                            '<div id="toggle-btn' + key + '" class="toggle-btn"></div>' +
                            '</button></td>' +
                            '</tr>'
                    })
                    $('#modalReadFileComment #listFacility table tbody').append(html);
                    $('#modalReadFileComment input.toggle-input').click(function () {
                        if ($(this).is(':checked')) {
                            $('#modalReadFileComment form #un_check_' + $(this).val()).remove();
                        } else {
                            $('#modalReadFileComment form').append('<input type="hidden" id="un_check_' + $(this).val() + '" name="un_check[]" value="' + $(this).val() + '">');
                        }
                    })
                    $('#modalReadFileComment').modal({backdrop: 'static', keyboard: false})
                    $('#modalReadFileComment').modal('show');
                    $("#preloader").remove();
                }).fail(function (jqXHR) {
                    $("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    $('#error').removeClass('d-none');
                    $("#preloader").remove();
                });
            }
        })
    </script>
    <script type="text/javascript" src="{{ asset('js/uploadType1Admin.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadFacilityAdmin.js') }}"></script>
@endsection
