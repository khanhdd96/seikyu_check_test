<div class="modal" id="modalAddMail" tabindex="-1" role="dialog"
     aria-labelledby="modalListMail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">テンプレート一覧</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body text-left max-height-700">
                <div class="add-employee">
                    <form id="formAddMail" action="{{ route('check-1.add-template-mail') }}">
                        @csrf
                        <div style="display: flex">
                            <div class="wrapper-search mb-10" style="width: 550px">
                                <input type="hidden" id="mail-type" name="mail_type" value="">
                                <input class="input-search key_word" style="z-index: 1;" type="text" maxlength="100" placeholder="テンプレート名" name="key_word" value="">
                                <button class="button-search" type="button" style="cursor: unset;">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4" d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z" fill="#7C9AA1"></path>
                                        <path d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z" fill="#7C9AA1"></path>
                                    </svg>
                                </button>
                            </div>
                            <div class="search-button mb-10">
                                <button class="search-button" style="width: 50px; margin:0" type="button" onclick="searchTemplate()">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="body-template row" style="padding: 20px 0; max-height: 400px; overflow-y: auto">
                        </div>
                        <script>
                            function searchTemplate(type)
                            {
                                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                                var key_word = $('#modalAddMail input[name="key_word"]').val();
                                let templateId = ''
                                if (type === 1) {
                                    templateId = '{{ $result['typeCheck']->mail_noti_file_template_id ?? '' }}'
                                } else if (type === 2) {
                                    templateId = '{{ $result['typeCheck']->mail_noti_not_done_id ?? '' }}'
                                } else if (type === 3) {
                                    templateId = '{{ $result['typeCheck']->mail_noti_to_check_id ?? '' }}'
                                }
                                console.log(templateId)
                                $.ajax({
                                    url: '{{route('template-mail.get-list')}}',
                                    type: 'GET',
                                    data: {
                                        key_word: key_word,
                                    },
                                    headers: {
                                        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                                    },
                                    dataType: "json",
                                }).done(function (data) {
                                    var respon = data.data.data;
                                    var html = '';
                                    respon.forEach(function (item, key) {
                                        var textCss = '';
                                        var checked = '';
                                        if (templateId !== '') {
                                            if (item.id == templateId) {
                                                checked = 'checked';
                                            }
                                            if (key != 0) {
                                                textCss = 'style="margin-top: 24px"';
                                            }
                                        } else {
                                            if (key != 0) {
                                                textCss = 'style="margin-top: 24px"';
                                            } else {
                                                checked = 'checked';
                                            }
                                        }
                                        html+= '<div class="col-sm-12" ' + textCss + '>' +
                                            '<div class="form-check form-radio-check pl-0">' +
                                            '<input ' + checked + ' name="mail_id" id="templateMail' + key + '" type="radio" value="' + item.id + '">' +
                                            '<label class="form-check-label" for="templateMail' + key + '">' + htmlEntities(item.title) +'</label>' +
                                            '</div></div>';
                                    });
                                    if (html == '') {
                                        html = '<div class="col-sm-12"><div class="form-check form-radio-check pl-0" style="text-align: center; color: #B8B8B8;">テンプレートが作成されていません。</div></div>'
                                    }
                                    $('#mail-type').val(type)
                                    $('.body-template').html(html);
                                    $("#preloader").remove();
                                })
                            }
                            function htmlEntities(str) {
                                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");;
                            }

                            function IsEmail(email) {
                                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                if(!regex.test(email)) {
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                        </script>
                        <!-- if edit alert success display modal B3003.3 success -->
                        <div class="d-flex align-items-center justify-content-center mt-50">
                            <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="閉じる">
                                キャンセル
                            </button>
                            <button type="submit" class="w-205 default-button">
                                適用
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
