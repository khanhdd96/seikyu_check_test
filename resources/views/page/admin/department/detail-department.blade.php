@extends('layouts.admin')
@section('title', '支店管理')
@section('content')
<!-- content page -->
<style>
    .checkbox-active {
        margin-left: 16px!important;
        background-color : #0F94B5!important;
    }
    .checkbox-inactive {
        margin-left: -2px!important;
        background-color : #F5F7FB!important;
    }
    .wrapper-toggle-btn.active {
        background-color : #B5E5F0
    }
</style>
<div class="main-content main-admin pt-20">
    <ol class="breadcrumb main-breadcrumb">
        <li class="breadcrumb-item main-breadcrumb-item"><a href="{{route('department.index')}}">支店管理</a></li>
        <li class="breadcrumb-item main-breadcrumb-item active" aria-current="page">詳細</li>
    </ol>

    <div class="main-heading mb-30">
        <h1 class="title-heading">{{$results['department']->name}}</h1>
    </div>

    <div class="detail-info">
        <div class="row mb-20">
            <div class="col-md-2 left-detail-info">
                メール
            </div>
            <div class="col-md-10 right-detail-info">
                {{$results['department']->email}}
            </div>
        </div>
        <div class="row mb-20">
            <div class="col-md-2 left-detail-info">
                事業所数
            </div>
            <div class="col-md-10 right-detail-info">
                {{$results['department']->facilities_count}}
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="wrapper-table">
            <div class="top-table">
                <form action="" method="get" class="top-table" autocomplete="off">
                    <div class="wrapper-search">
                        <input class="input-search" type="text" placeholder="事業所名" name="search" value="{{request('search')}}" maxlength="30">
                        <button class="button-search" type="button" disabled>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                      d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                      fill="#7C9AA1"/>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div>
                    <div class="search-button">
                        <button class="button-search" type="submit">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                      stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                </form>
            </div>

            <table class="table table-striped table-admin mt-15 mb-15">
                <thead>
                <tr>
                    <th class="col-md-1" scope="col">順番</th>
                    <th class="col-md-3" scope="col">事業所名</th>
                    <th class="col-md-2" scope="col">作成日付</th>
                    <th class="col-md-3" scope="col">請求システム対象変更</th>
                    <th class="col-md-3" scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($results['facilities'] as $key => $facility)
                <tr>
                    @php
                        $page = request('page') ?? \App\Consts::BASE_PAGE;
                    @endphp
                    <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                    <td>{{$facility->name}}</td>
                    <td>{{date('Y/m/d', strtotime($facility->created_at))}}</td>
                    <td>
                        <button type="button" class="wrapper-toggle-btn @if ($facility->is_active) active @endif" data-toggle="modal" data-target="#modalQuestion" data-backdrop="static" data-keyboard="false" >
                            <input type="checkbox" class="toggle-input" data-facility="{{$facility->id}}">
                            <div  class="toggle-btn @if ($facility->is_active) checkbox-active @else checkbox-inactive @endif"></div>
                        </button>
                    </td>
                    <td>
                    </td>
                </tr>
                @empty
                    <tr class="text-center">
                        <td style="display: revert" colspan="5">{{\App\Messages::EMPTY_RECORD}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <nav class="wrapper-pagination">
                {{ $results['facilities']->appends(request()->except('page'))->links('partial.admin.paginate') }}
            </nav>
        </div>
    </div>
</div>
<!-- content page -->
<div class="modal fade" id="modalQuestion" tabindex="-1" role="dialog" aria-labelledby="modalQuestionTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アクティブ状態を変更する</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                <p class="modal-text"> この事業所のアクティブ状態を変更してもよろしいですか？</p>
                <input type="hidden" name="id" id="facility">
                <div class="d-flex align-items-center justify-content-center mt-50">
                    <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                    <button type="button" class="w-205 default-button" id="submit-form">
                        適用</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   $('.wrapper-toggle-btn input').click(function (){
       $('.wrapper-toggle-btn input').removeAttr('id');
       $('#modalQuestion input').val($(this).data('facility'));
       $('#modalQuestion').addClass("show");
       $(this).attr("id", "input-click-change");
   })
   $('.modal').on('hidden.bs.modal', function () {
       $('.wrapper-toggle-btn input').removeAttr('id');
   });
   $('#submit-form').click(function (e){
       $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
       e.preventDefault();
       $.ajax({
           type: "POST",
           url: "{{route('edit-status-active')}}",
           data: {
               id: $('#facility').val(), // < note use of 'this' here
               "_token": "{{ csrf_token() }}",
           },
           success: function (result) {
               window.location.replace(location.protocol + '//' + location.host + location.pathname);
               // $('#preloader').remove();
               // $('#modalQuestion').modal('hide');
               // $('#input-click-change').parent().toggleClass('active');
               // $('#input-click-change').closest('.wrapper-toggle-btn').children('.toggle-btn').toggleClass('checkbox-inactive');
               // $('#input-click-change').closest('.wrapper-toggle-btn').children('.toggle-btn').toggleClass('checkbox-active');
               // $('.wrapper-toggle-btn input').removeAttr('id');
           },
           error: function (result) {
               $('#preloader').remove();
               alert('{{\App\Messages::SYSTERM_ERROR}}');
           }
       });
   })
</script>
@endsection
