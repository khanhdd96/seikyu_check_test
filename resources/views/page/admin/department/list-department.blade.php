@extends('layouts.admin')
@section('title', '支店管理')
@section('content')
    <style>
        .button-swap-all {
            padding: 5px 15px;
            min-height: 30px;
            background: linear-gradient(96.42deg, #2bc0e4 19.29%, #0f94b5 104.66%);
            border-radius: 5px;
            font-weight: 700;
            font-size: 14px;
            line-height: 23px;
            text-align: center;
            color: #ffffff;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 0 auto;
        }
        .wrapper-swap_all {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .wrapper-swap_all .form-check {
            margin-right: 15px;
        }
        .wrapper-swap_all .form-check .form-check-input{
            width: 16px;
            height: 16px;
            bottom: 1px;
        }
        .wrapper-swap_all .form-check label{
            font-size: 15px;
        }

        /*style radio button modal*/
        .form-radio-check [type="radio"]:checked,
        .form-radio-check [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
        .form-radio-check [type="radio"]:checked + label,
        .form-radio-check [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
        .form-radio-check [type="radio"]:checked + label:before,
        .form-radio-check [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 20px;
            height: 20px;
            border: 1px solid #2bc0e4;
            border-radius: 100%;
            background: #fff;
        }
        .form-radio-check [type="radio"]:checked + label:after,
        .form-radio-check [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 12px;
            height: 12px;
            background: #2bc0e4;
            position: absolute;
            top: 4px;
            left: 4px;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        .form-radio-check [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        .form-radio-check [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }
        .text-view-detail {
            white-space: nowrap;
        }
        .table-facility {
            padding: 20px;
            border: none;
        }
        .table-facility .row {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">支店管理</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <div class="top-table">
                    <form action="" method="get" class="top-table" autocomplete="off">
                        <div class="wrapper-search">
                            <input class="input-search" type="text" placeholder="支店名" name="search" value="{{request('search')}}" maxlength="30">
                            <button class="button-search" type="button" disabled>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4"
                                          d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                          fill="#7C9AA1"/>
                                    <path
                                        d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                        fill="#7C9AA1"/>
                                </svg>
                            </button>
                        </div>
                        <div class="search-button">
                            <button class="button-search" type="submit">
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                    <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                          stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th class="col-md-1" scope="col">順番</th>
                        <th class="col-md-3" scope="col">支店名</th>
                        <th class="col-md-2" scope="col">作成日付</th>
                        <th class="col-md-2" scope="col">事業所数</th>
                        <th class="col-md-2" scope="col" style="text-align: center; white-space: nowrap;">請求システム対象変更</th>
                        <th class="col-md-2" scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($results as $key => $department)
                    <tr>
                        @php
                            $page = request('page') ?? \App\Consts::BASE_PAGE;
                        @endphp
                        <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                        <td>{{$department->name}}</td>
                        <td>{{date('Y/m/d', strtotime($department->created_at))}}</td>
                        <td>{{$department->facilities_count}}</td>
                        <td>
{{--                            <button type="button" class="mb-10 a-button-add-branch button-swap-all toggle-input" data-toggle="modal"--}}
{{--                                    id="toggle-input1" data-backdrop="static" data-keyboard="false"--}}
{{--                                    data-status="{{ $department->facilitie_not_active_count == $department->facilities_count ? '2' : '1'}}"--}}
{{--                                    data-id="{{$department->id}}">一括対象事業所変更--}}
{{--                            </button>--}}
                            <button type="button" class="wrapper-toggle-btn switch {{ $department->facilitie_active_count ? 'checked' : ''}}"  data-toggle="modal" data-type="7" data-backdrop="static" data-keyboard="false"
                                    data-id="{{$department->id}}"
                                    data-target="#modalSwapStatus"
                                    data-status="{{ $department->facilitie_active_count ? 'active' : 'in-active'}}"
                                    style="margin: auto">
                                <input type="checkbox" id="toggle-input" class="toggle-input" {{ $department->facilitie_active_count ? 'checked' : ''}}>
                                <div class="toggle-btn"></div>
                            </button>
                        </td>
                        <td>
                            <a href="#" class="button-view-detail button-facility" data-id="{{ $department->id }}" data-type="active">
                                <span class="text-view-detail">活性</span>
                                <img src="{{ asset('icons/view.svg') }}" alt="view">
                            </a>
                            <a href="#" class="button-view-detail button-facility" data-id="{{ $department->id }}" data-type="inactive">
                                <span class="text-view-detail">非活性</span>
                                <img src="{{ asset('icons/view.svg') }}" alt="view">
                            </a>
                            <a href="{{route('department.edit', ['id'=> $department->id])}}" class="button-view-detail">
                                <span class="text-view-detail">詳細</span>
                                <img src="{{ asset('icons/view.svg') }}" alt="view">
                            </a>
                        </td>
                    </tr>
                    @empty
                        <tr class="text-center">
                            <td style="display: revert;" colspan="6">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <nav class="wrapper-pagination">
                    {{ $results->appends(request()->except('page'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
{{--<div class="modal fade" id="modalQuestion" tabindex="-1" role="dialog" aria-labelledby="modalQuestionTitle" aria-hidden="true">--}}
{{--    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">--}}
{{--        <div class="modal-content home-modal-content">--}}
{{--            <div class="modal-header home-modal-header">--}}
{{--                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アクティブ状態を変更する</h5>--}}
{{--                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">--}}
{{--                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">--}}
{{--                        </span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body home-modal-body">--}}
{{--                <form id="form-setting-sattus-department" method="post" action="{{route('department.status')}}">--}}
{{--                @CSRF--}}
{{--                    <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">--}}
{{--                    <p class="modal-text"> 全ての事務所のアクティブ状態が変更されます。よろしいですか？</p>--}}
{{--                    <input type="hidden" name="id" id="department-id" value="">--}}
{{--                    <div class="d-flex align-items-center justify-content-center mt-50">--}}
{{--                        <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>--}}
{{--                        <button type="submit" class="w-205 default-button" id="submit-form">--}}
{{--                            適用</button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
    <div class="modal fade" id="modalSwapStatus" tabindex="-1" role="dialog" aria-labelledby="modalSwapAllStatusTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">支社の請求チェックの事業所対象一括変更</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <form id="form-setting-sattus-department" method="post" action="{{route('department.status')}}">
                        @CSRF
                        <input type="hidden" name="id" id="department-id" value="">
                        <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                        <div class="wrapper-swap_all">
                            <div class="form-check form-radio-check">
                                <input class="form-check-input" type="radio" name="status" value="true"
                                       id="flexRadioDefault1" checked>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    有効する
                                </label>
                            </div>
                            <div class="form-check form-radio-check">
                                <input class="form-check-input" type="radio" name="status" value="false"
                                       id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    無効する
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button"
                        >適用
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalListFacilities" tabindex="-1" role="dialog" aria-labelledby="modalListFacilitiesTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modalTitle"></h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="wrapper-table" style="max-height: 400px;overflow-y: auto">
                        <table class="table-facility table table-striped table-admin">
                            <thead>
                                <tr class="row">
                                    <th class="col-2" scope="col">順番</th>
                                    <th class="col-6" scope="col">事業所名</th>
                                    <th class="col-4" scope="col">作成日付</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
$(document).ready(function () {
    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");;
    }

    var status = true;
    var classThis = '';
    $('.switch').click(function () {
        classThis = $(this);

        async function changeParam() {
            var departmentId = classThis.attr("data-id");
            $('#modalSwapStatus #department-id').val(departmentId);
            if (classThis.data('status') === 'in-active') {
                $('#modalSwapStatus input[name=status][value="true"]').prop("checked", true);
            } else {
                $('#modalSwapStatus input[name=status][value="false"]').prop("checked", true);
            }
        }

        changeParam().then(
            $('#form-setting-sattus-department button').click()
        );
    })
   // $('.modal').on('hidden.bs.modal', function () {
   //     if (status == false) {
   //          classThis.prop( 'checked', true )
   //     } else {
   //          classThis.prop( 'checked', false )
   //     }
   // });

   $("#form-setting-sattus-department").submit(function (event) {
    $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
       event.preventDefault();
       var formData = new FormData($(this)[0]);
        var thisModal = $(this).closest('.modal');
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            location.reload();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "\n";
                });
                $("#error").text(text)
            } else {
                $("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });
    $('.toggle-input').click(function () {
        $('#modalSwapStatus #file-status-question input[name="type"]').val($(this).parent().data('type'));
        const container = $(this).parent();
        const toggleBtn = $(this).parent().find('.toggle-btn');
        if ($(this).is(':checked')) {
            toggleBtn.css('margin-left', "16px");
            toggleBtn.css('background-color', "#0F94B5");
            container.css('background-color', "#B5E5F0");
            $('#modalSwapStatus').modal('show');
        } else {
            $('.toggle-btn').css('margin-left', "-2px");
            $('.toggle-btn').css('background-color', "#F5F7FB");
            $('.wrapper-toggle-btn-no').css('background-color', "#7c9aa1");
            $('.wrapper-toggle-btn').css('background-color', "#7c9aa1");
            $('#modalSwapStatus').modal('hide');
        }
    })
    $('#modalSwapStatus').on('hidden.bs.modal', function () {
        console.log(1)
        let toggleButton = $('.toggle-btn')
        toggleButton.css('margin-left', "-2px");
        toggleButton.css('background-color', "rgb(245, 247, 251)");
        $('.wrapper-toggle-btn').css('background-color', '#7c9aa1')
        toggleButton.prop('checked', false);
    });
    $('.button-facility').click(function (event) {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault()
        let modal = $('#modalListFacilities')
        let type = $(this).data('type')
        let departmentId = $(this).data('id')
        let title = ''
        if (type === 'active') {
            title = '活性中の事業所一覧'
        } else {
            title = '非活性の事業所一覧'
        }
        $.ajax({
            url: '{{ route('department.facility') }}',
            type: 'GET',
            data: {
                department_id: departmentId,
                type: type
            },
        }).done(function (data) {
            let html = ''
            let facilities = data.data
            facilities.forEach(function (value, key) {
                html += '<tr class=row>' +
                        '<td class="col-2">' + (key + 1) + '</td>' +
                        '<td class="col-6" style="justify-content: center">' + htmlEntities(value.name) + '</td>' +
                        '<td class="col-4" style="justify-content: center">' + value.create + '</td>' +
                        '</tr>'
            })
            modal.find('tbody').empty()
            modal.find('tbody').append(html)
        })
        modal.find('#modalTitle').text(title)
        modal.modal({ backdrop: "static", keyboard: false })
        $('#preloader').remove()
    })
});
</script>
@endsection
