@extends('layouts.admin')
@section('title', 'カスタマイズアラート')
@section('content')
    <style>
        #modalDetailAlertID1 .wrapper-table.pt-10.pb-0.mt-30{
            max-height: 277px;
            overflow-y: scroll;
        }
        .button-view-detail{
            white-space: nowrap;
        }
        .modal-body img {
            max-width: 100% !important;
        }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">カスタマイズアラート</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" method="get" autocomplete="off">
                    <div id="datepicker" class="datepicker-heading">
                        <input class="input-datepicker" type="text" name="month" placeholder="年/月/日" value="{{request('month')}}">
                        <span class="icon-datepicker">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                            fill="#7C9AA1"/>
                                        <path opacity="0.4"
                                              d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                              fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                            fill="#7C9AA1"/>
                                    </svg>
                                </span>
                    </div>
                    <div class="wrapper-search ml-3">
                        <input class="input-search" type="text" placeholder="検索" name="key_word" value="{{request('key_word')}}">
                        <button class="button-search" type="button" style="cursor: unset">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                      d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                      fill="#7C9AA1"/>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div>
                    <div class="search-button" style="width:50px">
                        <button class="button-search" type="submit" >
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <!-- button redirect to template B3002 setting-alert-custom -->
                    <a type="button" href="{{route('setting-alert-custom.create')}}" class="button-add-branch default-button">新アラート作成</a>
                </form>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th scope="col">順番</th>
                        <th scope="col">作成管理者名</th>
                        <th scope="col">配信開始日</th>
                        <th scope="col">配信時</th>
                        <th scope="col">頻度</th>
                        <th scope="col">配信回数</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($results['data'] as $key => $settingMail)
                        <tr>
                        @php
                            $page = request('page') ?? \App\Consts::BASE_PAGE;
                        @endphp
                        <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                        <td>{{isset($results['userNames'][$settingMail->created_by]) ? $results['userNames'][$settingMail->created_by] : ''}}</td>
                        <td style="white-space: nowrap">{{date('Y/m/d', strtotime($settingMail->start_date))}}</td>
                        <td style="white-space: nowrap">{{date('H:i', strtotime($settingMail->time))}}</td>
                        <td>{{$settingMail->frequency_type ? \App\Consts::MAILING_FREQUENCY[$settingMail->frequency_type] : ''}}</td>
                            <td>{{$settingMail->send_times ? $settingMail->history_mails_count .'/'. $settingMail->send_times : ''}}</td>
                        <td>
                            <button type="button" class="button-view-detail view-detail-setting"
                                    data-id="{{$settingMail->id}}">
                                <span class="text-view-detail">詳細</span>
                                <img src="{{ asset('icons/view.svg') }}" alt="view">
                            </button>
                            <!-- Button Redirect to page 3003.1 -->
                            <a href="{{route('setting-alert-custom.edit', ['id' => $settingMail->id])}}" class="button-view-detail">
                                <span class="text-view-detail">編集</span>
                                <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                            </a>
                                <!-- Button Modal xoá alert B3004 -->
                            <button type="button" class="button-view-detail delete-setting-mail" data-toggle="modal"
                                    data-target="#modalDeleteAlertID1" data-backdrop="static" data-keyboard="false"
                                    data-id="{{$settingMail->id}}"
                                {{$settingMail->type == \App\Consts::MAILING_CUSTOM && $settingMail->history_mails_count < $settingMail->send_times ? '' : 'disabled'}}>
                                <span class="text-view-detail">削除</span>
                                <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                            </button>
                        </td>
                    </tr>
                    @empty
                        <tr class="text-center">
                            <td colspan="7" style="display: revert;">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                    {{ $results['data']->appends(request()->except('page'), request()->except('date'), request()->except('key_word'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
    </section>
    </div>

    <!-- Modal B3001.1 詳細なアラート -->
    <div class="modal fade" id="modalDetailAlertID1" tabindex="-1" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">詳細なアラート</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <div class="d-flex align-items-center justify-content-end mb-30">
                                <a href="" class="button-view-detail" id="edit-link">
                                    <span class="text-view-detail">編集</span>
                                    <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                                </a>

                                <button type="button" class="button-view-detail" data-toggle="modal" data-target="#modalDeleteAlertID1" data-backdrop="static" data-keyboard="false" id="delete-link">
                                    <span class="text-view-detail">削除</span>
                                    <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                </button>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">経理者名</div>
                                <div class="col-8 text-setting-alert" id="name"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">作成日</div>
                                <div class="col-8 text-setting-alert" id="date-create"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">作成時</div>
                                <div class="col-8 text-setting-alert" id="time-send"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">頻度</div>
                                <div class="col-8 text-setting-alert" id="frequency-type"></div>
                            </div>

                            <div id="frequency" class="row mb-20 ">
                                <div class="col-4 label-form mb-0">送信間隔 </div>
                                <div class="col-8 text-setting-alert" id="frequency1"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">配信回数</div>
                                <div class="col-8 text-setting-alert" id="number-times-sent"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">メール</div>
                                <div class="col-8 text-setting-alert" id="list-mail"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">メール件名</div>
                                <div class="col-8 text-setting-alert" id="subject"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">アラートの内容</div>
                                <div class="col-8 text-setting-alert" id="content-mail"></div>
                            </div>

                            <div class="wrapper-table mt-30" style="padding:0">
                                <table class="table table-striped table-admin table-no-action mb-15" id="history-mail">
                                    <thead>
                                        <tr>
                                            <th scope="col">順番</th>
                                            <th scope="col">配信日</th>
                                            <th scope="col">配信時</th>
                                            <th scope="col">受信対象</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3001.1 詳細なアラート -->

    <!-- Modal B3004 Xoá Alert -->
    <div class="modal fade" id="modalDeleteAlertID1" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アラート削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form id="delete-setting-form">
                            <input type="hidden" name="id" id="id-setting-month">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark2.svg') }}" alt="question-mark">
                            <p class="modal-text"> このアラートを削除してよろしいでしょうか</p>
                            <button type="submit" class="home-modal-button default-button" id="button-delete-setting_mail">適用</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004 Xoá Alert -->
    <script>
        $(".input-datepicker").datepicker({
            format: "yyyy/mm/dd",
            autoclose: true,
            language: 'ja',
        });
        $('.delete-setting-mail').click(function (){
            var id = $(this).data('id');
            $("#id-setting-month").val(id);
        })
        $('#delete-link').click(function (){
            var id = $(this).data('id');
            $("#id-setting-month").val(id);
            $('#modalDetailAlertID1').modal('hide');
        })
        $('.icon-datepicker').on('click', function () {
            $('.input-datepicker').trigger('focus');
        })
        $(document).ready(function () {
            $("#delete-setting-form").submit(function (event) {
                event.preventDefault();
                var id = $('#id-setting-month').val();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '/admin/setting-alert-custom/' + id,
                    type: 'DELETE',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    success: function () {
                        location.reload();
                    }
                })
            });
            $(".view-detail-setting").click(function (event) {
                var id = $(this).data('id');
                $.ajax({
                    url: '/admin/setting-alert-custom/' + id,
                    type: 'GET',
                    success: function (data) {
                        $('#modalDetailAlertID1 #edit-link').attr('href', '/admin/setting-alert-custom/'+ data.data.id+'/edit')
                        $('#modalDetailAlertID1 #delete-link').data('id', data.data.id)
                        $('#modalDetailAlertID1 #name').text(data.data.name)
                        $('#modalDetailAlertID1 #list-mail').text(data.data.list_mail.join('、'))
                        $('#modalDetailAlertID1 #date-create').text(data.data.created_at)
                        $('#modalDetailAlertID1 #time-send').text(data.data.time_send)
                        $('#modalDetailAlertID1 #frequency-type').text(data.data.frequency_type)
                        if (data.data.frequency_type == '毎日') {
                            $('#modalDetailAlertID1 #frequency').addClass('d-none');
                        } else {
                            $('#modalDetailAlertID1 #frequency1').text(data.data.interval);
                        }
                        $('#modalDetailAlertID1 #number-times-sent').text(data.data.nume_time_send)
                        $('#modalDetailAlertID1 #subject').html(htmlEntities(data.data.subject))
                        $('#modalDetailAlertID1 #content-mail').html(data.data.content_mail)
                        var historyMails = data.data.historyMails;
                        var html = '';
                        historyMails.forEach(function (historyMail, index){
                            html += "<tr>" +
                                "<td>"+ (index + 1)+"</td>" +
                                "<td style='white-space: nowrap;'>"+historyMail.datetime+"</td>" +
                                "<td style='white-space: nowrap;'>"+historyMail.time+"</td>" +
                                "<td id='twoName" + index + "'style='white-space: nowrap'>" +historyMail.userName + "</td>" +
                                "<td class='d-none' style='max-width: 344px' id='allName" + index + "'>" + historyMail.allUserName + "</td>" +
                                "</tr>"
                        })
                        $('#history-mail tbody').empty();
                        $('#history-mail tbody').append(html);
                        $('#history-mail .rest-more').click(function (event) {
                            event.preventDefault();
                            var num = this.id.slice(9);
                            $('#' + 'twoName' + num).addClass('d-none');
                            $('#' + 'allName' + num).removeClass('d-none');
                        });
                        $('#modalDetailAlertID1').modal({backdrop: 'static', keyboard: false})
                        $('#modalDetailAlertID1').modal('show');
                    }
                })
            });
            function htmlEntities(str) {
                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");;
            }
        });
    </script>
@endsection
