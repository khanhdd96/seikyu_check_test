@extends('layouts.admin')
@section('title', 'リマインド（自動）アラート')
@section('content')
    <style>
        .number-chartLegend1 {
        white-space: nowrap;
        margin-left: 10px;
    }
    .search-button {
        width: 50px !important;
    }
    .w-216 {
        width: 216px;
    }
    .h-70 {
        height: 70%;
    }
    .select2-selection {
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple {
        line-height: 26px !important;
        font-size: 16px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display: none;
    }

    .select2-results__option--selectable {
        font-size: 16px;
    }
    .table-condensed {
        width: 207px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background: #b5e5f0 !important;
        border-radius: 5px !important;
        padding: 5px 20px 5px 5px !important;
        border: none !important;
        align-items: center;
        max-width: 90% !important;
    }

    .select2-selection--multiple .select2-selection__choice__remove {
        left: unset !important;
        top: 5px !important;
        right: 0;
    }
    .select2-selection--multiple .select2-selection__choice__remove span {
        display: none;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        top: 10px !important;
        right: 4px !important;
        width: 18px !important;
        height: 18px !important;
        background: url(/icons/delete.svg) center center no-repeat !important;
    }
    .selection-heading-comparison .select2-container {
        width: 100% !important;
    }

    .selection-heading-comparison .search-department {
        width: 100% !important;
    }

    .selection-heading-comparison {
        padding: 0px 8px;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
    .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background: rgba(124, 154, 161, 0.4) !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        border-radius: 45% !important;
        border-right: none !important;
    }

    .select2-container--default .select2-search--inline .select2-search__field {
        margin-top: 13px !important;
        margin-left: 15px !important;
        height: 24px !important;
        font-size: 16px;
        margin-bottom: 5px;
    }
    .selection-heading-error {
        width: 216px;
        min-height: 50px;
        position: relative;
        border: none;
        box-sizing: border-box;
        border-radius: 5px;
        margin-left: 15px;
        font-size: 16px;
    }
    .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
        width: 100%;
        margin-top: 13px;
        margin-left: 15px;
    }

    .select2-container .select2-selection--multiple {
        min-height: 50px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding:12px 22px !important;
    }
    button:disabled,
    button[disabled] {
        background-color: #cccccc;
        color: #666666;
    }

    .table thead th {
        vertical-align: middle !important;
    }

    .button-view-detail {
            white-space: nowrap;
    }
    button.close-modal-day-off {
        background: #B5E5F0;
        border-radius: 5px;
    }
    button.submit-modal-day-off {
        background: linear-gradient(96.42deg, #2BC0E4 19.29%, #0F94B5 104.66%);
        border-radius: 5px;
    }
    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da !important;
    }
    .pr-40 {
        padding-right: 40px;
    }
    .pt-30 {
        padding-top: 30px;
    }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">日祝・特定判定マスタ</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" method="get" autocomplete="off">
                    <div id="date-type" class="selection-heading">
                        <select class="form-control dropdown-heading" id="search" name="search">
                            <option value="">種類</option>
                            <option <?= request('search') == 1 ? "selected" : '' ?> value="1">特定</option>
                            <option <?= request('search') == 2 ? "selected" : '' ?> value="2">日祝</option>
                        </select>
                    </div>
                    <div class="search-button" style="width:50px; margin-left:15px">
                        <button class="button-search" type="submit" >
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <button type="button" data-keyboard="false" class="button-add-branch default-button" id="button-create-day-off" data-toggle="modal" data-target="#create-day-off">新規追加</button>
                </form>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th scope="col">順番</th>
                        <th scope="col">日付</th>
                        <th scope="col">種類</th>
                        <th scope="col">最新更新日</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($results as $key => $result)
                    <tr>
                        <td >{{($results->currentPage() - 1) * $results->perPage() + $key +1}}</td>
                        <td >
                            {{date('Y/m/d', strtotime($result->date_off))}}
                        </td>
                        <td >
                            {{$result->date_off_type == 1 ? '特定' : '日祝'}}
                        </td>
                        <td >{{date('Y/m/d', strtotime($result->date_off_update))}}</td>
                        <td>
                            <button type="button" class="button-view-detail button-edit-day-off" data-id="{{$result->id}}"
                                data-toggle="modal" data-target="#edit-day-off">
                                <span class="text-view-detail">編集</span>
                                <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                            </button>
                            <button type="button" class="button-view-detail button-delete-day-off" data-toggle="modal"
                                data-target="#modalDeleteAlert" data-backdrop="static" data-keyboard="false"
                                data-id="{{$result->id}}">
                                <span class="text-view-detail">削除</span>
                                <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                            </button>
                        </td>
                    </tr>
                    @empty
                        <tr class="text-center">
                            <td style="display: revert;" colspan="5">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                {{ $results->appends(request()->except('page'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
    <!-- Modal D1000.1 -->
    <div class="modal fade home-modal-content" id="create-day-off" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-880" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">日祝・特定新規追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                            <div class="container">
                                <div class="row justify-content-md-center">
                                    <div class="justify-content-center w-654 ml-0">
                                        <label class="label-form" for="business-selection">日付選択</label>
                                        <div id="datepicker" class="datepicker-heading w-100 ml-0">
                                            <input type="text" class="form-control input-datepicker datepicker datepicker-create form-control-add pr-40"
                                                name="create-datepicker" id="create-datepicker" placeholder="年/月/日"  multiple>
                                            <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"></path>
                                                    <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"></path>
                                                    <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"></path>
                                                    <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"></path>
                                                    <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"></path>
                                                    <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"></path>
                                                    <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"></path>
                                                    <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="text-danger mt-1 day-off-validate"></div>
                                    </div>
                                    <div class="justify-content-center h-auto w-654 ml-0 pt-30">
                                        <label class="label-form" for="business-selection">種類</label>
                                        <div class="selection-heading ml-0">
                                            <select class="form-control dropdown-heading w-654"
                                            id="create-day-off-datepicker" name="create-day-off-type">
                                                <option value="1">特定</option>
                                                <option value="2">日祝</option>
                                            </select>
                                        </div>
                                        <div class="text-danger mt-1 day-off-type-validate"></div>
                                    </div>
                                </div>
                                <div class="row justify-content-md-center mt-50">
                                    <button type="button" class="btn close-modal-day-off second-button mr-30" data-dismiss="modal" style="width:312px">キャンセル</button>
                                    <button type="button" class="btn submit-modal-day-off default-button" id="submit-create-day-off" style="width:312px">保存</button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.1 -->
    <!-- Modal D1000.2 -->
    <div class="modal fade" id="modalCreateAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalCreateAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">日祝・特定新規追加完了</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 日祝・特定新規追加が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-list-day-off">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.2 -->
    <!-- Modal D1000.3 -->
    <div class="modal fade home-modal-content" id="edit-day-off" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-880" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">日祝を編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                            <div class="container">
                                <div class="row justify-content-md-center">
                                    <div class="justify-content-center w-654 ml-0">
                                        <label class="label-form" for="business-selection">日付選択</label>
                                        <div id="edit-datepicker" class="datepicker-heading w-100 ml-0">
                                            <input type="text" class="form-control datepicker input-datepicker datepicker-edit" id="edit-datepicker-day-off" name="edit-datepicker-day-off" placeholder="年/月/日">
                                            <input type="hidden" name="id" id="id-day-off-edit">
                                            <span onclick="$(this).parent('div').find('.input-datepicker').trigger('focus')" class="icon-datepicker">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"></path>
                                                    <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"></path>
                                                    <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"></path>
                                                    <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"></path>
                                                    <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"></path>
                                                    <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"></path>
                                                    <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"></path>
                                                    <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="text-danger mt-1 day-off-validate"></div>
                                    </div>
                                    <div class="justify-content-center h-auto w-654 ml-0 pt-30">
                                        <label class="label-form" for="business-selection">種類</label>
                                        <div class="selection-heading ml-0">
                                            <select class="form-control dropdown-heading w-654"
                                                id="edit-day-off-type" name="edit-day-off-type">
                                                <option value="1">特定</option>
                                                <option value="2">日祝</option>
                                            </select>
                                        </div>
                                        <div class="text-danger mt-1 day-off-type-validate"></div>
                                    </div>
                                </div>
                                <div class="row justify-content-md-center mt-50">
                                    <button type="button" class="btn close-modal-day-off second-button" data-dismiss="modal" style="width:312px; margin-right:21px">キャンセル</button>
                                    <button type="button" class="btn submit-modal-day-off default-button" id="submit-edit-day-off" style="width:312px;">保存</button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.3 -->
    <!-- Modal D1000.4 -->
    <div class="modal fade" id="modalEditAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalEditAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">日祝を編集</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 日祝の編集が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-list-day-off">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.4 -->
    <!-- Modal D1000.5 Confirm Xoá Alert -->
    <div class="modal fade" id="modalDeleteAlert" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlert" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">日祝・特定 削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                            <input type="hidden" name="id" id="id-day-off">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text">該当日祝・特定を削除しますか。</p>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                                <button type="submit" class="w-205 default-button" id="submit-form-delete">
                                    削除</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.5 -->
    <!-- Modal D1000.6 -->
    <div class="modal fade" id="modalDeleteAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">日祝・特定 削除</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 日祝・特定 削除成功しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-list-day-off">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.6 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.ja.min.js')}}" integrity="sha512-zI0UB5DgB1Bvvrob7MyykjmbEI4e6Qkf5Aq+VJow4nwRZrL2hYKGqRf6zgH3oBQUpxPLcF2IH5PlKrW6O3y3Qw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function () {
            $("input.datepicker-create").datepicker({
                format: "yyyy/mm/dd",
                autoclose: false,
                language: 'ja',
                multidate: true,
            });
            $("input.datepicker-edit").datepicker({
                format: "yyyy/mm/dd",
                autoclose: true,
                language: 'ja',
            });
            $('.icon-datepicker').on('click', function () {
                $('#datepicker input').trigger('focus');
            })
            $('.button-edit-day-off').click(function (){
                $('#id-day-off-edit').val($(this).data("id"));
                $('div.day-off-validate').text('');
                $('div.day-off-type-validate').text('');
                $('#edit-day-off').modal({backdrop: 'static', keyboard: false});
                $.ajax({
                    url: '{{route("day-off.show")}}',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-day-off-edit').val()},
                    success: function (data) {
                        $('#edit-datepicker-day-off').val(data.data.date_off);
                        $('#edit-datepicker-day-off').datepicker('setDate', data.data.date_off);
                        $('#edit-day-off-type').val(data.data.date_off_type);
                    },
                    error: function (e) {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                        $("#preloader").remove();
                    }
                });
            });
            $('#submit-edit-day-off').click(function () {
                var dayOff = $("input[name='edit-datepicker-day-off']").val();
                var dayOffType = $("select[name='edit-day-off-type']").val();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route("day-off.update")}}',
                    type: 'PUT',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {
                        id : $('#id-day-off-edit').val(),
                        dayOff : dayOff,
                        dayOffType : dayOffType
                    },
                    success: function (data) {
                        $('#edit-day-off').modal('hide');
                        $('#modalEditAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalEditAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        if (e.status == 400) {
                            var err = JSON.parse(e.responseText);
                            $('div.day-off-validate').text(err.message.dayOff);
                            $('div.day-off-type-validate').text(err.message.dayOffType);
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                        $("#preloader").remove();
                    }
                });

            })
            $('#button-create-day-off').click(function () {
                $('#create-datepicker').val('');
                $('#create-day-off-datepicker').val('1');
                $('#create-datepicker').datepicker('setDate', null);
                $('div.day-off-validate').text('');
                $('#create-day-off').modal({backdrop: 'static', keyboard: false});
            })
            $('.button-delete-day-off').click(function () {
                $('#id-day-off').val($(this).data("id"));
                $('#modalDeleteAlert').modal({backdrop: 'static', keyboard: false});
            })
            $('#submit-form-delete').click(function () {
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route("day-off.destroy")}}',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-day-off').val()},
                    success: function (data) {
                        $('#modalDeleteAlert').modal('hide');
                        $('#modalDeleteAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalDeleteAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                        $("#preloader").remove();
                    }
                });
            });
            $('.reload-list-day-off').click(function () {
                location.reload();
            });
            $('#submit-create-day-off').click(function () {
                var dayOff = $("input[name='create-datepicker']").val();
                var dayOffType = $("select[name='create-day-off-type']").val();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route("day-off.store")}}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {dayOff : dayOff, dayOffType : dayOffType},
                    success: function (data) {
                        $('#create-day-off').modal('hide');
                        $('#modalCreateAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalCreateAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        if (e.status == 400) {
                            var err = JSON.parse(e.responseText);
                            $('div.day-off-validate').text(err.message.dayOff);
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                        $("#preloader").remove();
                    }
                });
            });
            $('.close-success-modal').click(function () {
                location.reload();
            });
        });
    </script>
@endsection
