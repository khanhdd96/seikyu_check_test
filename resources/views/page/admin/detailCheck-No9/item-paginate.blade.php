@foreach($files as $file)
    <div class="detail-check-block">
        <div class="inner-check-block">
            <div class="left-check-block">
                <div class="context-check-block">更新日: <span class="gray-date">{{date('Y/m/d', strtotime($file->created_at))}} - {{date('H:i', strtotime($file->created_at))}} </span></div>
                <div class="btn-right-context {{ $file->status == \App\Consts::STATUS_CHECK_FILE['success'] ? 'btn-green-context' :
                                                        ($file->status == \App\Consts::STATUS_CHECK_FILE['error'] ? 'btn-mint-context': 'btn-blue-context' )}}">
                    <img src="{{ $file->status == \App\Consts::STATUS_CHECK_FILE['success'] ? asset('icons/verify.svg') :
                                                        ($file->status == \App\Consts::STATUS_CHECK_FILE['error'] ? asset('icons/forbidden.svg') : asset('icons/timer.svg'))}}" alt="verify">
                    {{\App\Consts::STATUS_CHECK[$file->status]}}
                </div>
            </div>
{{--            <a href="{{$file->filepath}}" download class="right-check-block">--}}
{{--                <div class="gray-text-check">ダウンロード</div>--}}
{{--                <span class="btn-download-check">--}}
{{--                     <img class="" src="{{ asset('icons/download.svg') }}" alt="download">--}}
{{--                </span>--}}
{{--            </a>--}}
        </div>
        <div class="inner-check-block">
            <div class="left-check-block">
                <div class="context-check-block">ファイル名: <span class="mint-file-check">{{$file->file_name}} </span></div>
            </div>
        </div>
        <div class="inner-check-block">
            <div class="left-check-block">
                <div class="context-check-block">請求未確定事業所: <span class="{{$file->errors_count ? 'red-number-check' : 'green-number-check'}}">{{$file->errors_count}}</span></div>
            </div>
        </div>
        <ul class="list-error-file">
            @foreach ($file->errors as $error)
                <li class="item-error-file">
                    {{$error->error_position}}
                </li>
            @endforeach
        </ul>
        @if($file->mail_recipients)
            <div class="inner-check-block mt-4">
                <div class="left-check-block">
                    <div class="context-check-block">メールを受信する事業所一覧: <span class="red-number-check">{{count($file->mail_recipients)}}</span></div>
                </div>
            </div>
            <ul class="list-error-file">
                @foreach ($file->mail_recipients as $name)
                    <li class="item-error-file">
                        {{$name}}
                    </li>
                @endforeach
            </ul>
        @endif
        @if($file->check_send_mail)
            <div class="reason-check-block mt-20">
                <div class="context-check-block">メール送信日時: <span class="gray-date">
                    {{$file->time_send_mail ? date('Y/m/d', strtotime($file->time_send_mail)) .' - ' . date('H:i', strtotime($file->time_send_mail)): ''}}
                    </span>
                </div>
            </div>
            <div class="reason-check-block mt-20">
                <div class="context-check-block">メール内容</div>
                <div class="text-reason-check">
                    {!! nl2br(e($file->suspended_message))!!}
                </div>
            </div>
        @endif
    </div>
    @endforeach
@if( $files && $files->nextPageUrl())
    <!-- button load more -->
    <div class="wrapper-button-loadmore" id="pagination">
        <a href="{{$files->nextPageUrl()}}" class="button-loadmore">
            <img class="mr-10" src="{{ asset('icons/add-square.svg') }}" alt="add-square">
            もっと見る
        </a>
    </div>
@endif
