@extends('layouts.admin')
@section('title', '請求未確定')
@section('content')
    <style>
        #modalError .home-modal-button {
            margin-left: 0!important;
            margin-right: 0!important;
        }
        .container-radio {
            display: flex;
            justify-content: space-between;
            width: 100%;
            margin: 10px 0 20px;
        }
        [type="radio"]:checked,
        [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
        [type="radio"]:checked + label,
        [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
        [type="radio"]:checked + label:before,
        [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 3px;
            width: 18px;
            height: 18px;
            border: 1px solid #ddd;
            border-radius: 100%;
            background: #fff;
        }
        [type="radio"]:checked + label:after,
        [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 10px;
            height: 10px;
            background: #0F94B5;
            position: absolute;
            top: 7px;
            left: 4px;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }
        .T-modal{
            width: 430px;
            margin: 0 auto;
        }
        .w-205{
            margin-left: 0;
            margin-right: 0;
        }
        #modalConfirmMail{
            overflow-y: auto;
        }
        #modalConfirmMail::-webkit-scrollbar {
            width: 0;
            display: none;
        }
    </style>
    <div class="main-content main-detail-check">
        <div class="main-heading mb-30">
            <h1 class="title-heading">請求未確定</h1>
            <button type="button" class="default-upload-button default-button m-0" data-toggle="modal"
                    data-target="#modalUploadID3">ファイルアップロード
            </button>
        </div>
        <div class="right-block-datepicker">
            <form action="{{ route('file_check_admin.detail', ['type_check' => request('type_check')]) }}" method="get">
                <div class="filter-heading" style="justify-content: end;">
                    <div id="datepicker" class="datepicker-heading ml-0" style="position: relative">
                        <input class="input-datepicker" type="text" name="month" placeholder="年/月" value="{{$data['month']}}">
                        <span class="icon-datepicker">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                            fill="#7C9AA1"/>
                                        <path opacity="0.4"
                                              d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                              fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                            fill="#7C9AA1"/>
                                    </svg>
                    </span>
                    </div>
                    <button class="search-button" type="submit">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
        <div class="main-body">
            <!-- 4 blocks -->
            <div class="wrapper-article-block wrapper-article-check">
                <div class="row">
                    <div class="col-lg-3">
                        <article class="article-block first-upload-block">
                            <h5 class="title-article-block line-2">作成日</h5>
                            <h2 class="date-article-block line-2">{{$data['createdAt'] ? date('Y/m/d', strtotime($data['createdAt'])) : 'N/A'}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                        <article class="article-block second-upload-block">
                            <h5 class="title-article-block line-2">更新日</h5>
                            <h2 class="date-article-block line-2">{{$data['updatedAt'] ? date('Y/m/d', strtotime($data['updatedAt'])) : 'N/A'}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                        <article class="article-block third-upload-block">
                            <h5 class="title-article-block line-2">チェックの回数</h5>
                            <h2 class="date-article-block line-2">{{ $data['fileCount'] != 0 ? $data['fileCount'] : '0'}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                    @php $status = isset($typeCheck) && $typeCheck && $typeCheck->status ? $typeCheck->status : \App\Consts::STATUS_CHECK_FILE['not_has_check']; @endphp
                    <!-- block checked  -->
                        <article class="article-block {{\App\Consts::CLASS_STATUS_CHECK_FILE_DETAIL[$status]}}">
                            <h5 class="title-article-block line-2">状態</h5>
                            <h2 class="date-article-block line-2">{{\App\Consts::STATUS_CHECK[$status]}}</h2>
                        </article>
                        <!-- block checked  -->
                    </div>
                </div>
            </div>
            <!-- 4 blocks -->
        </div>

        <div class="main-heading mt-30 mb-25">
            <h4 class="mint-title-result">ファイルチェック履歴</h4>
        </div>
        <div class="wrapper-detail-check" id="detail-check">
            @if(count($files))
                @include('page.admin.detailCheck-No9.item-paginate')
            @else
                <div class="text-center">
                    <img src="{{asset('/icons/empty.png')}}" alt="">
                    <p class="mt-5" style="font-size: 16px">まだファイルがない</p>
                </div>
            @endif
        </div>
    </div>

<!-- Modal upload file: id=ID &aria-labelledby=ID -->
<div class="modal fade" id="modalUploadID3" tabindex="-1" role="dialog" aria-labelledby="modalUploadID1Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <form id="file-upload-form" action="{{route('check-9')}}"
                          method="post" enctype="multipart/form-data" class="file-upload-form file-upload-form-no">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['settingMonth'] ? $data['settingMonth']->id : ''}}">
                        <input type="hidden" name="month" value="{{$data['month']}}">
                        <input type="hidden" name="type" value="{{request('type_check')}}">
                        <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                        <div class="multiple-file-upload row" style="margin-left: 0; margin-right: 0">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept=".xls, .xlsx, .csv"/>
                                    <label for="file-upload" id="file-drag" class="modal-body file-upload">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                    <div id="response" class="hidden file-response" style="margin-bottom: 0">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}"
                                             alt="document-upload">
                                        <div id="messages" class="messages-file"></div>
                                    </div>
                                    {{-- <p class="modal-text"> サンプルファイルをCSV形式でダウンロードする - <a
                                                href="{{asset('assets/縮小版_予実明細.csv')}}"
                                                download class="btn-download-file">こちらで</a></p>
                                    <p class="modal-text"> サンプルファイルをExcel形式でダウンロードする -  <a href="{{ asset('assets/縮小版_予実明細.xlsx') }}" download class="btn-download-file">こちらで</a></p>
                                    --}}
                                </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button"  id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal upload file: id=ID &aria-labelledby=ID -->
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="content">
                <p id="file-name">Filecheck1.csv</p>
                    <span aria-hidden="true">
                            <img src="{{ asset('icons/code-error 1.png') }}" alt="close">
                        </span>
                    <p id="number-file-error">ファイルに<strong>3</strong>つ請求未確定の事業所があります</p>
                </div>
                <div id="error" class="text-danger font-weight-bold"></div>
                <div id="success" class="text-success font-weight-bold"></div>
                <div class="uploader">
                    <form id="send-mail-form" action="{{route('sendMail.no-9')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row" id="facilities">
                            <ul class="list-error-file text-left" id="list-error-facility">
                            </ul>
                        </div>
                        <div id="facilities-error" class="text-danger mt-2 text-left"></div>
                        <div class="container-radio d-flex">
                            <p class="mr-20">
                                <input type="radio" id="send-all" name="radio-group" checked>
                                <label for="send-all"><span style="color: #222222">一括メール送信</span></label>
                            </p>
                            <p class="mr-20">
                                <input type="radio" id="send-specific" name="radio-group">
                                <label for="send-specific"><span style="color: #222222">事業所ごとにメール送信</span></label>
                            </p>
                        </div>
                        <div class="row" id="title-send-mail">
                            <h4>メール内容</h4>
                        </div>
                        <div class="row">
                            <textarea name="mail_content" id="mail-content" style="resize:none" maxlength="500" minlength="50">
@if($data['templateMail']){{$data['templateMail'] }}
@else
各位

お疲れ様です。
昨日の未実績数は下記の通りです。
ご確認及びご対応の程何卒よろしくお願い申し上げます。
@endif
                            </textarea>
                            <div id="content-error" class="text-danger mt-2 text-left"></div>
                        </div>
                        <div class="row justify-content-between">
                            <button type="submit" id="file-upload-form-button2"
                                    class="home-modal-button default-button"
                                    data-dismiss="modal">確認する
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalConfirmMail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalErrorTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body T-modal" style="text-align: center; padding: 30px 0px;">
                <div class="content">
                    <p id="number-file-error" style="margin: 0 0 30px 0;">各事業所へ下記のメール内容を送信致します。</p>
                </div>
                <div id="error" class="text-danger font-weight-bold"></div>
                <div id="success" class="text-success font-weight-bold"></div>
                <form id="confirm-mail-form" action="{{route('sendMail.no-9')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div id="facility-mail"></div>
                    <div id="content-error" class="text-danger mt-2 text-left"></div>
                    <div class="d-flex align-items-center justify-content-between mt-50">
                        <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close" id="mail-back">戻る</button>
                        <button type="submit" class="w-205 default-button" id="submit-mail">
                            送信する</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <p id="file-name">Filecheck1.csv</p>
                <img class="home-modal-icon" src="{{ asset('icons/double-check-success.svg') }}" alt="question-mark">
                <p class="modal-text">請求未確認の事業所がありません。</p>
                <button type="button" class="home-modal-button default-button button-close" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSendMailSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSendMailSuccess" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="閉じる">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <div class="edit-alert-success">
                    <form>
                        <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                        <p class="modal-text">メールの送信が完了しました</p>
                        <button id="send-mail-success-close" type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">閉じる</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/uploadFile.js') }}"></script>
<!-- multiple -->
<script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
<script>
    const container = document.getElementById("wrapper-toggle-btn")
    const toggleInput = document.getElementById("toggle-input")
    const toggleBtn = document.getElementById("toggle-btn")
</script>
<script>
    var facilitiesGlobal;
    var dataGlobal;
    $(document).ready(function () {
        $(document).on('click', '#pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getData(page);
        });
        $('#modalError .close.home-modal-close').click(function (){
            location.reload();
        })
        $('#modalSuccess .home-modal-close').click(function (){
            location.reload();
        })
        $('#modalSuccess .button-close').click(function (){
            location.reload();
        })
        $('#modalSendMailSuccess #send-mail-success-close').click(function (){
            location.reload();
        })
    });

    function getData(page) {
        // body...
        $.ajax({
            url: '?page=' + page,
            type: 'get',
            data: {
                type_check: "{{request('type_check')}}",
                month: "{{$data['month']}}"
            },
            datatype: 'html',
        }).done(function (data) {
            $('#pagination').remove();
            $('#detail-check').append(data)
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        });
    }
    $("#file-upload-form-button").click(function (event) {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault();
        var formData = new FormData($('#file-upload-form')[0]);
        $('#modalUploadID3 #error').empty();
        $.ajax({
            url: $('#file-upload-form').attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (data) {
            $("#preloader").remove();
            $('#modalUploadID3').modal('hide');
            $('.modal-backdrop').remove();
            $('#modalUploadID3 #error').empty();
            var facilities = data.data.facilities;
            facilitiesGlobal = data.data.count_errors;
            dataGlobal = data.data;
            if (facilities.length > 0) {
                var html = '';
                $.each(facilities, function (key, value) {
                    html += ' <li class="item-error-file">\n' +
                        '           <div class="checkbox-select-facility container-checkbox">\n' +
                        '                <input type="checkbox" class="d-none" checked disabled name="facility[' + key + ']" value="' + value['error_position'] + '" id="id' + key + '">\n' +
                        '                 <label for="id' + key + '"><div class="label-facility-no-9">' + value['error_position'] + '</div></label>\n' +
                        '                  </div>\n' +
                        '    </li>'
                });
                $('#list-error-facility').empty();
                $('#list-error-facility').append(html);
                $('#number-file-error strong').text(facilities.length);
                $('#file_check').remove();
                $("#modalError #success").empty();
                $("#modalError #error").empty();
                $("#modalError #file-name").text(data.data.file_name);
                $('#send-mail-form').append("<input type='hidden' id='file_check' name='file_check' value='" + data.data.file + "'>");
                $('#modalError').modal({backdrop: 'static', keyboard: false})
                $('#modalError').modal('show');
            } else {
                $("#modalSuccess #file-name").text(data.data.file_name);
                $('#modalSuccess').modal({backdrop: 'static', keyboard: false})
                $('#modalSuccess').modal('show');
            }
        }).fail(function (jqXHR) {
            $("#preloader").remove();
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "</br>";
                });
                $("#modalUploadID3 #error").html(text)
            } else {
                $("#modalUploadID3 #error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });

    $('#send-specific').change(function (event){
        if(this.checked) {
            $(".checkbox-select-facility input").prop('checked', false);
            $(".checkbox-select-facility input").prop('disabled', false);
        }
    })
    $('#send-all').change(function (event){
        if(this.checked) {
            $(".checkbox-select-facility input").prop('checked', true);
            $(".checkbox-select-facility input").prop('disabled', true);
        }
    })
    $('#file-upload-form-button2').click(function (event){
        if($('#send-all').prop("checked")) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            if (facilitiesGlobal.length > 0) {
                var html = '';
                var mailContent = htmlEntities($('#send-mail-form #mail-content').val()) + '<br>';
                $.each(facilitiesGlobal, function (key, value) {
                    html +=  ' <li class="item-error-file">\n' +
                        '           <div class="checkbox-select-facility">\n' +
                        '                <input type="checkbox" class="d-none" checked name="facility[' + key + ']" value="' + value['error_position'] + '" id="id' + key + '">\n' +
                        '                 <label for="id' + key + '"><div class="label-facility-no-9">' + value['error_position'] + '</div></label>\n' +
                        '                  </div>\n' +
                        '    </li>'
                    mailContent += value['error_position'] + ':' + ' ' + value['error_count'] + '<br>';
                });
                var content = '<div id="facilities" >\n' +
                    '    <ul class="list-error-file text-left" id="list-error-facility" style="margin-bottom: 20px !important;">\n' +
                    html +
                    '</ul>\n' +
                    '   </div>' +
                    '<div id="title-send-mail" style="text-align: left">' +
                    '<h4>メール内容</h4>' +
                    '</div>' +
                    '<div>' +
                    '<div id="mail-content" style="resize:none; text-align: left; !important;">' +
                    mailContent +
                    '</div>' +
                    '</div>'
                $('#modalConfirmMail #facility-mail').empty();
                $('#modalConfirmMail #facility-mail').append(content);
                $('#modalConfirmMail #number-file-error strong').text(facilitiesGlobal.length);
                $('#send-mail-form').append("<input type='hidden' id='type_mail' name='type_mail' value='all'>");
                var facilities = JSON.stringify(facilitiesGlobal);
                $('#send-mail-form').append("<input type='hidden' id='data' name='data' value='" + facilities + "'>");
                $("#preloader").remove();
                $('#modalConfirmMail').modal({backdrop: 'static', keyboard: false})
                $('#modalConfirmMail').modal('show');
            }
        } else {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            var checked = new Array();
            $('#send-mail-form .checkbox-select-facility input:checked').each(function() {
                checked.push($(this).val());
            })
            if (checked.length > 0) {
                $('#modalConfirmMail #facility-mail').empty();
                var mailContent = htmlEntities($('#send-mail-form #mail-content').val()) + '<br>';
                $.each(checked, function (key1, value1) {
                    var html = '';
                    $.each(facilitiesGlobal, function (key, value) {
                       if (value['error_position'] == value1) {
                           html += '<div id="facilities" >\n' +
                               '    <ul class="list-error-file text-left" id="list-error-facility" style="margin-bottom: 20px !important;">\n' +
                               '        <li class="item-error-file">\n' +
                               '           <div class="checkbox-select-facility">\n' +
                               '                <input type="checkbox" class="d-none" checked name="facility[' + key + ']" value="' + value['error_position'] + '" id="id' + key + '">\n' +
                               '                 <label for="id' + key + '"><div class="label-facility-no-9">' + value['error_position'] + '</div></label>\n' +
                               '                  </div>\n' +
                               '    </li>' +
                               '</ul>\n' +
                               '   </div>' +
                               '<div id="title-send-mail" style="text-align: left">' +
                               '<h4>メール内容</h4>' +
                               '</div>' +
                               '<div>' +
                               '<div id="mail-content" style="resize:none; text-align: left; !important;">' +
                               mailContent + value['error_position'] + ':' + ' ' + value['error_count'] + '</br>' +
                               '</div>' +
                               '</div>'
                           $('#modalConfirmMail #facility-mail').append(html);
                       }
                   })
                });
                $('#modalConfirmMail #number-file-error strong').text(checked.length);
                var facilities = JSON.stringify(facilitiesGlobal);
                $('#send-mail-form').append("<input type='hidden' id='data' name='data' value='" + facilities + "'>");
                $("#preloader").remove();
                $('#modalConfirmMail').modal({backdrop: 'static', keyboard: false})
                $('#modalConfirmMail').modal('show');
            } else {
                location.reload();
            }
        }
    })
    $('#modalConfirmMail #submit-mail').click(function (event){
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        $('#send-mail-form .checkbox-select-facility input').prop('disabled', false);
        event.preventDefault();
        var formData = new FormData($('#send-mail-form')[0]);
        $("#modalError #content-error").empty();
        $("#modalError #error").empty();
        $.ajax({
            url: $('#send-mail-form').attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (data) {
            $("#preloader").remove();
            $('#modalConfirmMail').modal('hide');
            $('#modalSendMailSuccess').modal({backdrop: 'static', keyboard: false})
            $('#modalSendMailSuccess').modal('show');
        }).fail(function (jqXHR) {
            $("#preloader").remove();
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                if (typeof messages.facility !== "undefined" || messages.facility !== null) {
                    var textFacilityErrors = '';
                    $.each(messages.facility, function (index, value) {
                        textFacilityErrors += value + "</br>";
                    });
                    $("#modalConfirmMail #facilities-error").html(textFacilityErrors)
                }
                if (typeof messages.mail_content !== "undefined" || messages.mail_content !== null) {
                    var textContentErrors = '';
                    $.each(messages.mail_content, function (index, value) {
                        textContentErrors += value + "</br>";
                    });
                    $("#modalError #content-error").html(textContentErrors)
                    $('#modalConfirmMail').modal('hide');
                    $('#modalError').modal('show');
                }
            } else {
                $("#modalConfirmMail #error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    })
    $('#mail-back').click(function (event){
        if (facilitiesGlobal.length > 0) {
            $('#modalError').modal({backdrop: 'static', keyboard: false})
            $("#modalError #content-error").empty();
            $("#modalError #error").empty();
            $('#modalError').modal('show');
        }
    })
    $('#modalUploadID3').on('shown.bs.modal', function (e) {
        $('input[name="file"]').val('');
        ekUpload('#modalUploadID3');
    });
    $('#modalError').on('shown.bs.modal', function (e) {
        $('#list-error-facility li input').change(function (){
            var checked = $("#list-error-facility li input:checked" ).length;
            var inputCheck = $("#list-error-facility li input" ).length;
            if (inputCheck != checked) {
                $('#select-all').prop('checked', false);
            } else {
                $('#select-all').prop('checked', true);
            }
            if (checked == 0 ) {
                // $('#file-upload-form-button2').prop('disabled', true);
            } else {
                if ($("#mail-content").val().length > 0) {
                    $('#file-upload-form-button2').prop('disabled', false);
                }
            }
        })
    });
    $('#modalUploadID3').on('hidden.bs.modal', function () {
        let thisModal = $(this).closest('.modal')
        $('#file-upload-form-button').prop('disabled', true);
        thisModal.find('#error').empty()
        thisModal.find('#notimage').empty()
        $('#messages').empty();
        $('#response').addClass('hidden ');
    })
    $(document).ready(function() {
        var date=new Date();
        var year=date.getFullYear()
        var month=date.getMonth();
        $("#datepicker input").datepicker({
            format: "yyyy/mm",
            startView: "months",
            minViewMode: "months",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });
        $('.icon-datepicker').on('click', function () {
            $('#datepicker input').trigger('focus');
        })
    })
    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");;
    }
</script>
@endsection
