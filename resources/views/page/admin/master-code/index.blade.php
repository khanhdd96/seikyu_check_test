@extends('layouts.admin')
@section('title', 'おまかせコードマスタ')
@section('content')
    <style>
        .number-chartLegend1 {
        white-space: nowrap;
        margin-left: 10px;
    }
    .search-button {
        width: 50px !important;
    }
    .w-428 {
        width: 428px;
    }
    .h-70 {
        height: 70%;
    }
    .select2-selection {
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple {
        line-height: 26px !important;
        font-size: 16px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display: none;
    }

    .select2-results__option--selectable {
        font-size: 16px;
    }
    .table-condensed {
        width: 207px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background: #b5e5f0 !important;
        border-radius: 5px !important;
        padding: 5px 20px 5px 5px !important;
        border: none !important;
        align-items: center;
        max-width: 90% !important;
    }

    .select2-selection--multiple .select2-selection__choice__remove {
        left: unset !important;
        top: 5px !important;
        right: 0;
    }
    .select2-selection--multiple .select2-selection__choice__remove span {
        display: none;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        top: 10px !important;
        right: 4px !important;
        width: 18px !important;
        height: 18px !important;
        background: url(/icons/delete.svg) center center no-repeat !important;
    }
    .selection-heading-comparison .select2-container {
        width: 100% !important;
    }

    .selection-heading-comparison .search-department {
        width: 100% !important;
    }

    .selection-heading-comparison {
        padding: 0px 8px;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
    .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background: rgba(124, 154, 161, 0.4) !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        border-radius: 45% !important;
        border-right: none !important;
    }

    .select2-container--default .select2-search--inline .select2-search__field {
        margin-top: 13px !important;
        margin-left: 15px !important;
        height: 24px !important;
        font-size: 16px;
        margin-bottom: 5px;
    }
    .selection-heading-error {
        width: 216px;
        min-height: 50px;
        position: relative;
        border: none;
        box-sizing: border-box;
        border-radius: 5px;
        margin-left: 15px;
        font-size: 16px;
    }
    .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
        width: 100%;
        margin-top: 13px;
        margin-left: 15px;
    }

    .select2-container .select2-selection--multiple {
        min-height: 50px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding:12px 22px !important;
    }
    button:disabled,
    button[disabled] {
        background-color: #cccccc;
        color: #666666;
    }

    .table thead th {
        vertical-align: middle !important;
    }

    .button-view-detail {
            white-space: nowrap;
    }
    button.close-modal-day-off {
        background: #B5E5F0;
        border-radius: 5px;
    }
    button.submit-modal-day-off {
        background: linear-gradient(96.42deg, #2BC0E4 19.29%, #0F94B5 104.66%);
        border-radius: 5px;
    }
    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da !important;
    }

    /*style radio button modal*/
    .form-radio-check [type="radio"]:checked,
    .form-radio-check [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    .form-radio-check [type="radio"]:checked + label,
    .form-radio-check [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    .form-radio-check [type="radio"]:checked + label:before,
    .form-radio-check [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 20px;
        height: 20px;
        border: 1px solid #2bc0e4;
        border-radius: 100%;
        background: #fff;
    }
    .form-radio-check [type="radio"]:checked + label:after,
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 12px;
        height: 12px;
        background: #2bc0e4;
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    .form-radio-check [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    .form-radio-check [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
    .insurance .selection-heading {
        height: 100%;
    }
    input.form-control {
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        height: 46px;
    }
    .insurance .form-check-label {
        color: #222222 !important;
    }
    button.btn {
        border: unset;
    }
    .modal-body.master-code {
        padding-bottom: 10px !important;
        padding-top: 20px !important;
    }
    .add-sub-code {
        background: #F5F7FB !important;
        border: 1px dashed #0F94B5 !important;
        color: #222222 !important;
    }
    input.w-560 {
        max-width: 560px !important;
    }
    .add-sub-code-body .button-view-detail {
        margin-left: auto;
    }
    .group-item{
        display: flex;
    }
    .group-item .item-left {
        width: calc(100% - 94px);
    }
    .group-item .item-right {
        margin-top: 60px;
        height: 100%;
        padding-left: 30px;
    }
    .table-admin.table th {
        white-space: nowrap;
    }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">おまかせコードマスタ</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" method="get" autocomplete="off">
                    <div id="date-type" class="wrapper-search">
                        <input type="text" class="input-search" value="{{request('search') ?? ''}}" placeholder="マスタコード名" id="search" name="search" aria-placeholder="特定" maxlength="100">
                        <button class="button-search" type="button" disabled>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                    d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                    fill="#7C9AA1"/>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div>
                    <div class="search-button" style="width:150px!important; margin-left:15px">
                        <select name="type" class="form-control dropdown-heading">
                            <option value="">タイプ</option>
                            <option value="-1" @if(request('type') == -1) selected @endif>なし</option>
                            @foreach(\App\Consts::TYPE_CODE as $key => $value)
                                <option value="{{$key}}" @if(request('type') == $key) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="search-button" style="width:50px; margin-left:15px">
                        <button class="button-search" type="submit" >
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <button type="button" data-keyboard="false" class="button-add-branch default-button" id="button-create-master-code" data-toggle="modal" data-target="#create-master-code">新規追加</button>
                </form>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th scope="col" style="width:7%">順番</th>
                        <th scope="col">本体コード</th>
                        <th scope="col">タイプ</th>
                        <th scope="col">割増コード数</th>
                        <th scope="col">最新更新日</th>
                        <!-- <th scope="col">配信回数</th> -->
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($results as $key => $result)
                    <tr>
                        <td >{{($results->currentPage() - 1) * $results->perPage() + $key + 1}}</td>
                        <td >
                            {{$result->code_name}}
                        </td>
                        <td style="white-space: nowrap">
                            {{ is_null($result->type) ? 'なし' : \App\Consts::TYPE_CODE[$result->type] }}
                        </td>
                        <td >
                            {{$result->master_sub_code_count}}
                        </td>
                        <td style="white-space: nowrap">{{date('Y/m/d', strtotime($result->date_update))}}</td>
                        <td>
                            <button type="button" class="button-view-detail button-view-master-code"
                                data-toggle="modal" data-target="#view-master-code"
                                data-id="{{$result->id}}">
                                <span class="text-view-detail">詳細</span>
                                <img src="{{ asset('icons/view.svg') }}" alt="view">
                            </button>
                            <button type="button" class="button-view-detail button-edit-master-code-{{$result->id}} button-edit-master-code" data-id="{{$result->id}}"
                                data-toggle="modal" data-target="#edit-master-code">
                                <span class="text-view-detail">編集</span>
                                <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                            </button>
                            <button type="button" class="button-view-detail button-delete-master-code-{{$result->id}} button-delete-master-code" data-toggle="modal"
                                data-target="#modalDeleteAlert" data-backdrop="static" data-keyboard="false"
                                data-id="{{$result->id}}">
                                <span class="text-view-detail">削除</span>
                                <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                            </button>
                        </td>
                    </tr>
                    @empty
                        <tr class="text-center">
                            <td style="display: revert;" colspan="5">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                {{ $results->appends(request()->except('page'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
    </section>
    </div>
    <!-- Modal D1001.1 view master code -->
    <div class="modal fade" id="view-master-code" tabindex="-1" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">おまかせコード詳細</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body master-code text-left max-height-700 mb-0">
                    <div class="add-employee">
                        <form>
                            <div class="d-flex align-items-center justify-content-end mb-30">
                                <button onclick="clickToEdit()" type="button" class="button-view-detail edit-delete-in-detail" id="delete-link">
                                    <span class="text-view-detail">編集</span>
                                    <img src="{{ asset('icons/edit.svg') }}" alt="trash">
                                </button>

                                <button onclick="clickToDelete()" type="button" class="button-view-detail edit-delete-in-detail" data-toggle="modal" data-target="#modalDeleteAlert" data-backdrop="static" data-keyboard="false" id="delete-link">
                                    <span class="text-view-detail">削除</span>
                                    <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                </button>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">本体コード</div>
                                <div class="col-8 text-setting-alert" id="view-code-name"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">タイプ</div>
                                <div class="col-8 text-setting-alert" id="view-code-type"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">割増コード数</div>
                                <div class="col-8 text-setting-alert" id="view-sub-code-count"></div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">最新更新日</div>
                                <div class="col-8 text-setting-alert" id="view-date-update"></div>
                            </div>
                            <div class="wrapper-table mt-30" style="padding:0">
                                <table class="table table-striped table-admin table-no-action mb-15" id="history-mail">
                                    <thead>
                                        <tr>
                                            <th scope="col">順番</th>
                                            <th scope="col">割増コード</th>
                                            <th scope="col">タイプ</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-view-sub-master-code">
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.1 view master code -->
    <!-- Modal D1001.2 create master code -->
    <div class="modal fade home-modal-content" id="create-master-code" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-880" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">おまかせコード新規追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body master-code text-left max-height-700">
                <div class="add-employee">
                        <form id="form-create-master-code" method="post">
                            @csrf
                            <div class="d-flex align-items-center justify-content-center mb-40 modal-view-day-off">
                            <div class="container">
                                <div class="w-654 mx-auto">
                                    <div class="justify-content-md-center h-auto  ml-0 pt-30">
                                        <div class="justify-content-center">
                                            <label class="label-form" for="business-selection">本体コード</label>
                                            <div>
                                                <input type="text" class="form-control create-code-name"
                                                    name="createCodeName" placeholder="本体コード" maxlength="100">
                                                <div class="text-danger mt-2 mb-2 code-name-validate input-validate-name"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="justify-content-md-center h-auto  ml-0 pt-20">
                                        <div class="justify-content-center">
                                            <label class="label-form" for="business-selection">タイプ</label>
                                            <div class="selection-heading" style="width: 100%; margin:0">
                                                <select name="type" class="form-control dropdown-heading">
                                                    <option value="">タイプ</option>
                                                    @foreach(\App\Consts::TYPE_CODE as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger mt-2 mb-2 type-validate input-validate-type"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-sub-code-body">

                                    </div>
                                    <div class="justify-content-md-center">
                                        <button type="button" class="btn add-sub-code default-button mb-50 mt-25 w-100" id="add-create-sub-code">
                                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-10">
                                            <path opacity="0.4" d="M12.5 22C18.0228 22 22.5 17.5228 22.5 12C22.5 6.47715 18.0228 2 12.5 2C6.97715 2 2.5 6.47715 2.5 12C2.5 17.5228 6.97715 22 12.5 22Z" fill="#0F94B5"/>
                                            <path d="M16.5 11.25H13.25V8C13.25 7.59 12.91 7.25 12.5 7.25C12.09 7.25 11.75 7.59 11.75 8V11.25H8.5C8.09 11.25 7.75 11.59 7.75 12C7.75 12.41 8.09 12.75 8.5 12.75H11.75V16C11.75 16.41 12.09 16.75 12.5 16.75C12.91 16.75 13.25 16.41 13.25 16V12.75H16.5C16.91 12.75 17.25 12.41 17.25 12C17.25 11.59 16.91 11.25 16.5 11.25Z" fill="#0F94B5"/>
                                        </svg>
                                        割増コード追加
                                        </button>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <button type="button" class="btn close-modal-master-code second-button w-100" data-dismiss="modal">キャンセル</button>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <button type="button" class="btn submit-modal-master-code default-button w-100" id="submit-create-master-code">保存</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1001.2 -->
    <!-- Modal D1002.3 alert create success -->
    <div class="modal fade" id="modalCreateAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalCreateAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content pb-50">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">おまかせコード新規追加</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body pb-0">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> おまかせコード新規追加が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-in-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.3 -->
    <!-- Modal D1001.4 submit edit -->
    <div class="modal fade home-modal-content" id="edit-master-code" role="dialog" aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-880" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">おまかせコードを編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body master-code text-left max-height-700">
                <div class="add-employee">
                        <form id="form-edit-master-code" method="post">
                            @csrf
                            <div class="d-flex align-items-center justify-content-center mb-40 modal-view-day-off">
                            <div class="container">
                                <div class="w-654 mx-auto">
                                    <div class="justify-content-md-center h-auto  ml-0 pt-30">
                                        <div class="justify-content-center">
                                            <label class="label-form" for="business-selection">本体コード</label>
                                            <div>
                                                <input type="text" class="form-control edit-code-name"
                                                    name="editCodeName" placeholder="本体コード" maxlength="100">
                                                <div class="text-danger mt-2 mb-2 edit-code-name-validate input-validate-name"></div>
                                                <div id="array-sub-code-id">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="justify-content-md-center h-auto  ml-0 pt-20">
                                        <div class="justify-content-center">
                                            <label class="label-form" for="business-selection">タイプ</label>
                                            <div class="selection-heading" style="width: 100%; margin:0">
                                                <select name="editCodeType" class="form-control dropdown-heading edit-code-type">
                                                    <option value="">タイプ</option>
                                                    @foreach(\App\Consts::TYPE_CODE as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger mt-2 mb-2 type-validate input-validate-type"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-edit-sub-code-body">

                                    </div>
                                    <div class="justify-content-md-center">
                                        <button type="button" class="btn add-sub-code default-button mb-50 mt-25 w-100" id="add-edit-sub-code">
                                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-10">
                                            <path opacity="0.4" d="M12.5 22C18.0228 22 22.5 17.5228 22.5 12C22.5 6.47715 18.0228 2 12.5 2C6.97715 2 2.5 6.47715 2.5 12C2.5 17.5228 6.97715 22 12.5 22Z" fill="#0F94B5"/>
                                            <path d="M16.5 11.25H13.25V8C13.25 7.59 12.91 7.25 12.5 7.25C12.09 7.25 11.75 7.59 11.75 8V11.25H8.5C8.09 11.25 7.75 11.59 7.75 12C7.75 12.41 8.09 12.75 8.5 12.75H11.75V16C11.75 16.41 12.09 16.75 12.5 16.75C12.91 16.75 13.25 16.41 13.25 16V12.75H16.5C16.91 12.75 17.25 12.41 17.25 12C17.25 11.59 16.91 11.25 16.5 11.25Z" fill="#0F94B5"/>
                                        </svg>
                                        子マスタコード追加
                                        </button>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <button type="button" class="btn close-modal-master-code second-button w-100" data-dismiss="modal">キャンセル</button>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <button type="button" class="btn submit-modal-master-code default-button w-100" id="submit-edit-master-code">保存</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.3 -->
    <!-- Modal D1001.4 alert edit success-->
    <div class="modal fade" id="modalEditAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalEditAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">情報を編集</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 情報の編集が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-in-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.4-->
    <!-- Modal D1001.5 confirm delete-->
    <div class="modal fade" id="modalDeleteAlert" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlert" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">おまかせコード削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                            <input type="hidden" name="id" id="id-master-code">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text"> @if(!request('type') || request('type') == 'role-admin')おまかせコードを削除でしょうか。 @else この使用者を事業所から削除してもよろしいですか？ @endif</p>
                            <div class="text-danger mt-2 mb-2 delete-master-name-validate"></div>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                                <button type="submit" class="w-205 default-button" id="submit-delete-master-code">削除</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1001.5 -->
    <!-- Modal D1001.6 alert delete success-->
    <div class="modal fade" id="modalDeleteAlertSuccess" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">おまかせコード削除</h5>
                    <button type="button" class="close home-modal-close close-success-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text">おまかせコード削除が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-file-in-insurance">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1002.6 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        function clickToEdit() {
            var id = $('#id-master-code').val();
            setTimeout(function () {
                $('.button-edit-master-code-' + id).click();
            }, 350);
        }
        function clickToDelete() {
            var id = $('#id-master-code').val();
            $('.button-delete-master-code-' + id).click();
        }
        var type = JSON.parse('<?=json_encode(\App\Consts::TYPE_CODE)?>');
        var stringType = '<label class="label-form" for="business-selection">タイプ</label><div class="selection-heading" style="width: 100%; margin:0"><select name="createSubCodeType[]" class="form-control dropdown-heading"><option value="">タイプ</option>';
        var stringEditTypeNew = '<label class="label-form" for="business-selection">タイプ</label><div class="selection-heading" style="width: 100%; margin:0"><select name="editSubCodeType[]" class="form-control dropdown-heading"><option value="">タイプ</option>';
        $.each(type, function(key, value) {
            stringType += '<option value="' + key + '">' + value + '</option>';
            stringEditTypeNew += '<option value="' + key + '">' + value + '</option>';
            });
        stringType+= '</select></div>';
        stringEditTypeNew+= '</select></div>';

        $(document).ready(function () {
            let indexOfValidate = -1;
            function reloadClassEditSubCode() {
                $('.check-validate-edit-sub-code-name').each(function (index) {
                    $(this).removeAttr('class');
                    var newStringValidateEditSubCode = "text-danger mt-2 mb-2 check-validate-edit-sub-code-name sub-code-name-validate-" + index;
                    $(this).addClass(newStringValidateEditSubCode);
                });
            }
            function reloadClassCreateSubCode() {
                $('.check-validate-sub-code-name').each(function (index) {
                    $(this).removeAttr('class');
                    var newStringValidateCreateSubCode = "text-danger mt-2 mb-2 check-validate-sub-code-name sub-code-name-validate-" + index;
                    $(this).addClass(newStringValidateCreateSubCode);
                });
            }
            $('#button-create-master-code').click(function () {
                $('.create-code-name').val('');
                $('div.code-name-validate').text('');
                $('div.add-sub-code-body').empty();
                $('#create-master-code').modal({backdrop: 'static', keyboard: false});
            });
            $('.button-view-master-code').click(function () {
                $('#table-view-sub-master-code').empty();
                $('#id-master-code').val($(this).data("id"));
                $('#view-master-code').modal({backdrop: 'static', keyboard: false});
                $.ajax({
                    url: '{{route("master-code.show")}}',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-master-code').val()},
                    success: function (data) {
                        $('.edit-delete-in-detail').attr("data-id", data.data.id);
                        $('#view-code-name').text(data.data.code_name);
                        var typeName = '';
                        $.each(type, function (key, value) {
                            if (data.data.type == key) {
                                typeName = value;
                            }
                        })
                        $('#view-code-type').text(typeName ? typeName : 'なし');
                        $('#view-sub-code-count').text(data.data.master_sub_code.length);
                        $('#view-date-update').text(data.data.date_update.replaceAll("-", "/"));
                        let subCodeName = '';
                        data.data.master_sub_code.forEach(function(item, index) {
                            indexOfValidate = index + 1;
                            var typeName = '';
                            $.each(type, function (key, value) {
                                if (item.type == key) {
                                    typeName = value;
                                }
                            })
                            subCodeName = item.sub_code_name.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;').replace(/ /g, '&nbsp;')
                            $('#table-view-sub-master-code').append("<tr><td>" + indexOfValidate + "</td><td>" + subCodeName + "</td><td>" + (typeName? typeName : 'なし') + "</td></tr>");
                        });
                    },
                    error: function (e) {
                        if (e.status == 404) {
                            window.location.href = "{{route('404-not-found')}}";
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                    }
                });
            });
            $('.edit-delete-in-detail').click(function () {
                $('#view-master-code').modal('hide');
            })
            $('#add-create-sub-code').click(function (e) {
                e.preventDefault();
                indexOfValidate = indexOfValidate + 1;
                var stringValidateCreateSubCode = "text-danger mt-2 mb-2 check-validate-sub-code-name sub-code-name-validate-" + indexOfValidate;
                $('div.add-sub-code-body').append("<div class='justify-content-center h-auto pt-20'>" +
                        "<label class='label-form' for='business-selection'>割増コード</label>" +
                        "<div class='group-item'>" +
                            "<div class='item-left'>" +
                                "<input type='text' class='form-control create-company-name' name='createSubCodeName[]' placeholder='割増コード' maxlength='100'>" +
                                "<div class='"+ stringValidateCreateSubCode +"'></div>" +
                                stringType +
                            "</div>" +
                            "<div class='item-right'>" +
                                "<button type='button' class='button-view-detail delete-sub-code' data-toggle='modal' data-target='#modalDeleteAlertID1' data-backdrop='static' data-keyboard='false'>" +
                                    "<img src='{{ asset('icons/trash.svg') }}' alt='trash'>" +
                                "</button>" +
                            "</div>" +
                    "</div>");
                $('.delete-sub-code').click(function () {
                    $(this).parent().parent().parent().remove();
                });
            });
            $('#submit-create-master-code').click(function (e) {
                e.preventDefault();
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                var formData = new FormData($('#form-create-master-code')[0]);
                $('.input-validate-name').text('');
                $('.check-validate-sub-code-name').text('');
                reloadClassCreateSubCode();
                $.ajax({
                    url: '{{route("master-code.store")}}',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data : formData,
                    success: function (data) {
                        $('#create-master-code').modal('hide');
                        $('#modalCreateAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalCreateAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        if (e.status == 400) {
                            var err = JSON.parse(e.responseText);
                            var arr = Object.keys(err.message)
                            arr.forEach((val, index) => {
                                $('div.sub-code-name-validate-'+ val.split('.')[1]).text(err.message[val][0]);
                            })
                            $('div.code-name-validate').text(err.message.createCodeName);
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                        $("#preloader").remove();
                    }
                });
            });
            $('.reload-file-in-insurance').click(function () {
                location.reload();
            });
            $('.button-delete-master-code').click(function () {
                $('#id-master-code').val($(this).data("id"));
                $('div.delete-master-name-validate').text('');
                $('#modalDeleteAlert').modal({backdrop: 'static', keyboard: false});
            })
            $('#submit-delete-master-code').click(function () {
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route("master-code.destroy")}}',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-master-code').val()},
                    success: function (data) {
                        $('#modalDeleteAlert').modal('hide');
                        $('#modalDeleteAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function (){
                            $('#modalDeleteAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        if (e.status == 400) {
                            var err = JSON.parse(e.responseText);
                            $('div.delete-master-name-validate').text(err.message);
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                        $("#preloader").remove();
                    }
                });
            });
            $('.button-edit-master-code').click(function () {
                $('#id-master-code').val($(this).data("id"));
                $('div.add-edit-sub-code-body').empty();
                $('div.check-validate-edit-sub-code-name').text('');
                $('#edit-master-code').modal({backdrop: 'static', keyboard: false});
                $('#array-sub-code-id').empty();
                $.ajax({
                    url: '{{route("master-code.show")}}',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {id : $('#id-master-code').val()},
                    success: function (data) {
                        $('input.edit-code-name').val(data.data.code_name);
                        $('.edit-code-type').val(data.data.type);
                        indexOfValidate = data.data.master_sub_code.length - 1;
                        data.data.master_sub_code.forEach(function (item, index) {
                            var stringTypeEdit = '<label class="label-form" for="business-selection">タイプ</label><div class="selection-heading" style="width: 100%; margin:0"><select name="editSubCodeType[]" class="form-control dropdown-heading"><option value="">タイプ</option>';
                            $.each(type, function(key, value) {
                                var checked = '';
                                if (key == item.type) {
                                    checked = 'selected';
                                }
                                stringTypeEdit += '<option ' + checked + ' value="' + key + '">' + value + '</option>';
                            });
                            stringTypeEdit+= '</select></div>';
                            var stringValidateSubCode = "text-danger mt-2 mb-2 check-validate-edit-sub-code-name sub-code-name-validate-" + index;
                            $('div.add-edit-sub-code-body').append("<div class='justify-content-center h-auto pt-20'>" +
                                "<label class='label-form' for='business-selection'>割増コード</label>" +
                                "<div class='group-item'>" +
                                    "<div class='item-left'>" +
                                        "<input type='text' class='form-control edit-sub-code-name' maxlength='100' name='editSubCodeName[]'  placeholder='割増コード'" +
                                        " value='" + item.sub_code_name + "'>" +
                                        "<input type='hidden' name='subCodeEditId[]' value='" + item.id + "'>" +
                                        "<div " +
                                        " class='" + stringValidateSubCode +"'></div>" +
                                        stringTypeEdit +
                                    "</div>" +
                                    "<div class='item-right'>" +
                                        "<button type='button' class='button-view-detail delete-edit-sub-code-"+ index +"' data-toggle='modal' data-target='#modalDeleteAlertID1' data-backdrop='static' data-keyboard='false' id='delete-sub-code'" +
                                        " data-id='" + item.id + "'>" +
                                            "<img src='{{ asset('icons/trash.svg') }}' alt='trash'>" +
                                        "</button>" +
                                    "</div>" +
                            "</div>");
                            $(".delete-edit-sub-code-" + index).click(function(){
                                $(this).parent().parent().parent().remove();
                                subCodeId = $(this).data("id");
                                $('#array-sub-code-id').append("<input type='hidden' name='subCodeId[]' " +
                                    " value='" + subCodeId + "'>");
                            });
                        });
                    },
                    error: function (e) {
                        if (e.status == 404) {
                            window.location.href = "{{route('404-not-found')}}";
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                    }
                });
            });
            $('#add-edit-sub-code').click(function (e) {
                e.preventDefault();
                indexOfValidate = indexOfValidate + 1;
                var stringValidateEditSubCode = "text-danger mt-2 mb-2 check-validate-edit-sub-code-name sub-code-name-validate-" + indexOfValidate;
                $('div.add-edit-sub-code-body').append("<div class='justify-content-center h-auto pt-20'>" +
                    "<label class='label-form' for='business-selection'>割増コード</label>" +
                    "<div class='group-item'>" +
                        "<div class='item-left'>" +
                            "<input type='text' class='form-control create-company-name' name='editSubCodeName[]' placeholder='割増コード' maxlength='100'>" +
                            "<div " +
                                " class='" + stringValidateEditSubCode +"'></div>" +
                    stringEditTypeNew +
                        "</div>" +
                        "<div class='item-right'>" +
                            "<button type='button' class='button-view-detail delete-edit-sub-code' data-toggle='modal' data-target='#modalDeleteAlertID1' data-backdrop='static' data-keyboard='false'>" +
                                "<img src='{{ asset('icons/trash.svg') }}' alt='trash'>" +
                            "</button>" +
                        "</div>" +
                "</div>");
                $('.delete-edit-sub-code').click(function () {
                    $(this).parent().parent().parent().remove();
                });
            });
            $('#submit-edit-master-code').click(function (e) {
                e.preventDefault();
                var formData = new FormData($('#form-edit-master-code')[0]);
                $('.input-validate-name').text('');
                $('div.check-validate-edit-sub-code-name').text('');
                reloadClassEditSubCode();
                formData.append('_method', 'PUT');
                formData.append('id', $('#id-master-code').val());
                $.ajax({
                    url: '{{route("master-code.update")}}',
                    type: 'POST',
                    data : formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#edit-master-code').modal('hide');
                        $('#modalEditAlertSuccess').modal({backdrop: 'static', keyboard: false});
                        setTimeout(function () {
                            $('#modalEditAlertSuccess').modal('show');
                            $("#preloader").remove();
                        }, 200);
                    },
                    error: function (e) {
                        if (e.status == 400) {
                            var err = JSON.parse(e.responseText);
                            var arr = Object.keys(err.message)
                            arr.forEach((val, index) => {
                                $('div.sub-code-name-validate-'+ val.split('.')[1]).text(err.message[val][0]);
                            })
                            $('div.edit-code-name-validate').text(err.message.editCodeName);
                        } else {
                            alert('{{\App\Messages::SYSTERM_ERROR}}');
                        }
                        $("#preloader").remove();
                    }
                });
            });
            $('.close-success-modal').click(function () {
                location.reload();
            });
        });
    </script>
@endsection
