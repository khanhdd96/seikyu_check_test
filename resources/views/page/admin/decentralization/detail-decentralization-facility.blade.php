@extends('layouts.admin')
@section('title', '役割と権限の設定/詳細')
@section('content')
    <style>
        .select2-selection{
            height: 100% !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display:none;
        }
        .select2-results__option--selectable {
            font-size: 16px;
        }
        span.select2-selection.select2-selection--multiple {
            min-height: 50px !important;
            padding: 10px 10px 15px 10px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }
    </style>
    @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="閉じる">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="main-content main-admin pt-20" >
        <ol class="breadcrumb main-breadcrumb">
            <li class="breadcrumb-item main-breadcrumb-item"><a href="{{route('decentralization.index', ['type'=> 'user'])}}">権限管理</a></li>
            <li class="breadcrumb-item main-breadcrumb-item active">詳細</li>
        </ol>

        <div class="main-heading mb-30">
            <h1 class="title-heading">{{$results['facility']['name']}}</h1>
        </div>

        <div class="detail-info">
            <div class="row mb-20">
                <div class="col-md-2 left-detail-info">
                    メール
                </div>
                <div class="col-md-10 right-detail-info">
                    {{$results['facility']['email']}}
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-md-2 left-detail-info">
                    利用者数
                </div>
                <div class="col-md-10 right-detail-info">
                    {{$results['countRole']}}
                </div>
            </div>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <div class="top-table">
                    <div class="wrapper-search">
                        <form action="" method="get">
                            <input class="input-search" type="text" placeholder="氏名" name="search"
                                   value="{{request('search')}}" maxlength="30">
                            <button class="button-search" type="submit">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4"
                                          d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                          fill="#7C9AA1"/>
                                    <path
                                        d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                        fill="#7C9AA1"/>
                                </svg>
                            </button>
                        </form>
                    </div>
                    <button type="button" data-toggle="modal"
                            data-target="#modalEditDecentralizate" data-backdrop="static"
                            data-keyboard="false" class="button-add-branch default-button">利用者を追加</button>
                    <button type="button"
                            class="button-add-branch default-button" id="save-decentralization" disabled>保存
                    </button>
                </div>
                <form action="{{route('detail-decentralization-facility.update')}}" id="decentralization-update"
                      class="" method="post">
                    @csrf
                    {{ method_field('put') }}
                    <table class="table table-striped table-admin mt-15 mb-15">
                        <thead>
                        <tr>
                            <th width="10%" scope="col">順番</th>
                            <th width="18%" scope="col">氏名</th>
                            <th width="18%" scope="col">メール</th>
                            <th width="15%" scope="col">現職</th>
                            <th width="12%" scope="col">閲覧権限</th>
                            <th width="12%" scope="col">請求権限</th>
                            <th width="15%" scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($results['userRols'] as $key => $role)
                            <tr>
                                @php
                                    $page = request('page') ?? \App\Consts::BASE_PAGE;
                                @endphp
                                <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                <td>{{$role->user->code . ' - ' .  $role->user->name}}</td>
                                <td>{{$role->user->email}}</td>
                                <td>{{$role->position ? $role->position->name : ''}}</td>
                                <td>
                                    <div class="container-checkbox">
                                        <input type="checkbox" class="view" id="id1-{{$key}}"
                                               name="view[{{$role->id}}]"
                                               @if($role->view)checked @endif data-edit-id="id2-{{$key}}" data-id="{{$role->id}}">
                                        <label for="id1-{{$key}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <!-- note: tag input id=for of tag label -->
                                    <div class="container-checkbox">
                                        <input type="checkbox" class="edit" id="id2-{{$key}}"
                                               data-view-id="id1-{{$key}}" name="edit[{{$role->id}}]"
                                               @if($role->edit)checked @endif data-id="{{$role->id}}">
                                        <label for="id2-{{$key}}"></label>
                                    </div>
                                </td>
                                <td>
                                    @if(!$role->is_clone)
                                        <button type="button" class="button-view-detail delete-setting-mail delete-role"
                                                data-toggle="modal"
                                                data-target="#modalDeleteAlertID1" data-backdrop="static"
                                                data-keyboard="false"
                                                data-id="{{$role->id}}">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr class="text-center">
                                <td style="display: revert" colspan="7" class="">
                                    {{\App\Messages::EMPTY_RECORD}}
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </form>
                <nav class="wrapper-pagination">
                    {{ $results['userRols']->appends(request()->except('page'))->links('partial.admin.paginate') }}

                </nav>
            </div>
        </div>
    </div>
    <!-- content page -->
    </section>

    <div class="modal fade" id="modalEditDecentralizate" tabindex="-1" role="dialog"
         aria-labelledby="modalEditAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">利用者を追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="閉じる">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left">
                    <form action="{{route('detail-decentralization-facility.store', ['id' => request('id')])}}" method="post">
                        <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                            @csrf
                            @method('post')
                            <div class="form-group mb-0">
                                <label class="label-form" for="business-selection">氏名から選択</label>
                                <div class="selection-heading h-auto w-430 ml-0  mt-10">
                                    <select id="role-admin-selection" class="form-control dropdown-heading"
                                            name="users[]" style="width: 100%" multiple data-placeholder="氏名を入力してください">
                                        @foreach($results['users'] as $user)
                                            <option value="{{$user->id}}">{{$user->code .' - ' . $user->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="text-danger mt-2 d-none" id="error_required"></div>
                                    @error('users')
                                    <div class="text-danger mt-2">{{ $message }}</div>
                                    @enderror
                                    <div class="form-group mb-0 mt-30">
                                        <label class="label-form" for="">権限を選択</label>
                                        <div class="container-checkbox mt-23">
                                            <input type="checkbox" class="view" id="view" name="view" value="true" checked readonly onclick="return false;">
                                            <label for="view">閲覧権限</label>
                                        </div>
                                        <div class="container-checkbox mt-23">
                                            <input type="checkbox" class="view" id="edit" name="edit" value="true" checked>
                                            <label for="edit">請求権限</label>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-around mt-50">
                                        <button type="button" class="w-205 m-0 second-button" data-dismiss="modal" aria-label="閉じる">キャンセル</button>
                                        <button type="submit" class="w-205 default-button" id="submit-form">
                                            適用</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDeleteAlertID1" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">利用者を削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="閉じる">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form id="delete-form" action="{{route('detail-decentralization.delete')}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="hidden" name="id" id="id-role">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text"> この利用者を事業所から削除してもよろしいですか？</p>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="閉じる">キャンセル</button>
                                <button type="submit" class="w-205 default-button" id="">
                                    適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Modal B1001.3a Delete グループ削除削除 -->
<script>
    $(document).ready(function() {
        $('#decentralization-update .edit').change(function (event) {
            $('#save-decentralization').attr('disabled', false);
            $('#id_roles-id'+$(this).data("id")).remove();
            $('#decentralization-update').append('<input type="hidden" id="id_roles-id'+$(this).data("id")+'" name="id_roles[]" value="'+$(this).data("id")+'">')
            if (this.checked) {
                var viewId = $(this).data('view-id');
                if (!$('#'+viewId).checked) {
                    $('#'+viewId).prop('checked', true);
                }
            }
        })
        $('#decentralization-update .view').change(function (event) {
            $('#id_roles-id'+$(this).data("id")).remove();
            var id = $(this).data("id");
            $('#decentralization-update').append('<input type="hidden" id="id_roles-id'+$(this).data("id")+'" name="id_roles[]" value="'+id+'">')
            $('#save-decentralization').attr('disabled', false);
            if (!this.checked) {
                var editId = $(this).data('edit-id');
                $('#' + editId).prop('checked', false);
            }
        })
    })
    $("#save-decentralization").click(function (event){
        $('#decentralization-update').submit();
    })
    $("#modalEditDecentralizate").on("hidden.bs.modal", function(){
        $(this).find('form')[0].reset();
        $('select').trigger('chosen:updated');
    });
    $('.delete-role').click(function () {
        var id = $(this).data('id');
        $("#id-role").val(id);
    });
    $('#modalEditDecentralizate #submit-form').click(function (){
        var datas = $('#role-admin-selection').val();
        if (!datas.length) {
            event.preventDefault();
            $('#error_required').text('氏名を選択を入力してください。');
            $('#error_required').removeClass('d-none');
        }
    })
    $('#role-admin-selection').on('change', function (){
        $('#error_required').addClass('d-none');
        $('#error_required').text('');
    })
    // $(function () {
    //     $('#role-admin-selection').select2({
    //         minimumInputLength: 1,
    //         closeOnSelect: false,
    //         language: {
    //             inputTooShort: function (args) {
    //
    //                 return "任意の文字を入力してください。。。";
    //             },
    //             noResults: function () {
    //                 return "見つかりません。。。";
    //             },
    //             searching: function () {
    //                 return "検索しています。。。";
    //             }
    //         },
    //     });
    // })
</script>
@endsection
