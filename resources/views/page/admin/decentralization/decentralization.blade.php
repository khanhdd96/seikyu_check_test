@extends('layouts.admin')
@section('title', '権限管理')
@section('content')
<style>
    .select2-selection{
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display:none;
    }
    .select2-results__option--selectable {
        font-size: 16px;
    }
    span.select2-selection.select2-selection--multiple {
        min-height: 50px !important;
        padding: 10px 10px 15px 10px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .type {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
        appearance: none;
    }
</style>
<div class="main-content main-admin">
    <div class="main-heading mb-30">
        <h1 class="title-heading">権限管理</h1>
    </div>

    <nav class="wrapper-main-tab">
        <div class="nav nav-tabs main-nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'role-admin' || request('type') == null) active @endif" id="nav-office-tab" data-toggle="tab"
               onclick="setGetParameter('type', 'role-admin')" href="#nav-office" role="tab" aria-controls="nav-office" aria-selected="@if(request('type') == 'role-admin' || request('type') == null) true @endif">管理者権限</a>
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'role-facility') active @endif" id="nav-accounting-tab" data-toggle="tab"
               href="#nav-accounting" onclick="setGetParameter('type', 'role-facility')" role="tab" aria-controls="nav-accounting" aria-selected="@if(request('type') == 'role-facility') true @endif">利用者権限</a>
            <a class="nav-item nav-link main-navtab-link @if(request('type') == 'user') active @endif" onclick="setGetParameter('type', 'user')" id="nav-user-tab" data-toggle="tab" href="#nav-user"
               role="tab" aria-controls="nav-user" aria-selected="@if(request('type') == 'user') true @endif">事業所管理</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <!-- Wrapper tab -->
        @if(request('type') == 'role-admin' || request('type') == null)
            <div class="tab-pane fade @if(request('type') == 'role-admin' || request('type') == null) show active @endif"
                 id="nav-office" role="tabpanel" aria-labelledby="nav-office-tab">
                <div class="main-body">
                    <div class="wrapper-table wrapper-tab-table">
                        <form class="top-table" action="" method="get" autocomplete="off">
                            <select name="option_type" class="type form-control wrapper-search mr-20">
                                <option value=""></option>
                                <option {{request('option_type') == 1 ? 'selected' : ''}} value="1">管理者</option>
                                <option {{request('option_type') == 2 ? 'selected' : ''}} value="2">部門</option>
                            </select>
                            <div class="wrapper-search">
                                <div >
                                    <input class="input-search" type="text" placeholder="氏名" name="search"
                                           value="{{request('search')}}" maxlength="30">
                                    <button style="cursor: unset" class="button-search" type="button">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="search-button" style="width: 50px;">
                                <button class="button-search" type="submit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                            <button type="button" data-keyboard="false" class="button-add-branch default-button" id="updated_role_admin">権限追加</button>
                        </form>

                        <table class="table table-striped table-admin mt-15 mb-20">
                            <thead>
                            <tr>
                                <th width="80" scope="col">順番</th>
                                <th scope="col">管理者名/部門名</th>
                                <th scope="col">最終更新日</th>
                                <th scope="col">種類</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($results['list-data'] as $key => $user)
                                <tr>
                                    @php
                                        $page = request('page') ?? \App\Consts::BASE_PAGE;
                                    @endphp
                                    <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                    <td>{{$user->type == \App\Consts::EMPLOYEE_TYPE ? $results['userCodes'][$user->id_component] .' - '. $results['users'][$user->id_component] : $results['departmentCodes'][$user->id_component] .' - '. $results['list_department'][$user->id_component]}}</td>
                                    <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
                                    <td>{{$user->type == \App\Consts::EMPLOYEE_TYPE ? '管理者' : '部門'}}</td>
                                    <td>
                                        <button type="button" class="button-view-detail delete-setting-mail delete-role"
                                                data-toggle="modal"
                                                data-target="#modalDeleteAlertID1" data-backdrop="static"
                                                data-keyboard="false"
                                                data-id="{{$user->id}}">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td style="display: revert" colspan="6">{{\App\Messages::EMPTY_RECORD}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <nav class="wrapper-pagination">
                            {{ $results['list-data']->appends(request()->except('page'), request()->except('option_type'))->links('partial.admin.paginate') }}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Wrapper tab -->
        @endif
        <!-- Wrapper tab -->
        @if(request('type') == 'role-facility')
            <div class="tab-pane fade @if(request('type') == 'role-facility') show active @endif" id="nav-accounting"
                 role="tabpanel" aria-labelledby="nav-accounting-tab">
                <div class="main-body">
                    <div class="wrapper-table wrapper-tab-table">
                        <form class="top-table" action="" method="get" autocomplete="off">
                            <select name="option_type" class="type form-control wrapper-search mr-20">
                                <option value=""></option>
                                <option {{request('option_type') == 1 ? 'selected' : ''}} value="1">事業所</option>
                                <option {{request('option_type') == 2 ? 'selected' : ''}} value="2">職種（グレード）</option>
                            </select>
                            <input type="hidden" name="type" value="role-facility">
                            <div class="wrapper-search">
                                <div >
                                    <input class="input-search" type="text" placeholder="氏名" name="search"
                                           value="{{request('search')}}" maxlength="30">
                                    <button style="cursor: unset" class="button-search" type="button">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="search-button" style="width: 50px;">
                                <button class="button-search" type="submit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                            <button type="button" data-keyboard="false" data-backdrop="static" class="button-add-branch default-button" id="updated_role_facility">権限追加</button>
                        </form>

                        <table class="table table-striped table-admin mt-15 mb-20">
                            <thead>
                            <tr>
                                <th width="80" scope="col">順番</th>
                                <th scope="col">利用者名/職種（グレード）名</th>
                                <th scope="col">最終更新日</th>
                                <th scope="col">種類</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($results['list-data'] as $key => $user)
                                <tr>
                                    @php
                                        $page = request('page') ?? \App\Consts::BASE_PAGE;
                                    @endphp
                                    <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                    <td>{{$user->type == \App\Consts::EMPLOYEE_TYPE ? $results['userCodes'][$user->id_component] .' - '. $results['users'][$user->id_component] : $results['positionCodes'][$user->id_component] .' - '. $results['list_position'][$user->id_component]}}</td>
                                    <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
                                    <td>{{$user->type == \App\Consts::EMPLOYEE_TYPE ? '事業所' : '職種（グレード）'}}</td>
                                    <td>
                                        <button type="button" class="button-view-detail delete-setting-mail delete-role"
                                                data-toggle="modal"
                                                data-target="#modalDeleteAlertID1" data-backdrop="static"
                                                data-keyboard="false"
                                                data-id="{{$user->id}}">
                                            <span class="text-view-detail">削除</span>
                                            <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td style="display: revert" colspan="6">{{\App\Messages::EMPTY_RECORD}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <nav class="wrapper-pagination">
                            {{ $results['list-data']->appends(request()->except('page'), request()->except('option_type'))->links('partial.admin.paginate') }}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Wrapper tab -->
        @endif
        <!-- Wrapper tab -->
        @if(request('type') == 'user')
            <div class="tab-pane fade @if(request('type') == 'user') show active @endif" id="nav-user" role="tabpanel"
                 aria-labelledby="nav-user-tab">
                <div class="main-body">
                    <div class="wrapper-table wrapper-tab-table">
                        <form class="top-table" action="" method="get" autocomplete="off">
                            <input type="hidden" name="type" value="user">
                            <div class="wrapper-search">
                                <div >
                                    <input class="input-search" type="text" placeholder="事業所名" name="search"
                                           value="{{request('search')}}" maxlength="30">
                                    <button style="cursor: unset" class="button-search" type="button">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="search-button" style="width: 50px;">
                                <button class="button-search" type="submit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                        </form>

                        <table class="table table-striped table-admin mt-15 mb-20">
                            <thead>
                            <tr>
                                <th width="80" scope="col">順番</th>
                                <th scope="col">事業所名</th>
                                <th scope="col">支店名</th>
                                <th scope="col">最終更新日</th>
                                <th scope="col">利用者数</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($results['list-data'] as $key => $facility)
                            <tr>
                                @php
                                    $page = request('page') ?? \App\Consts::BASE_PAGE;
                                @endphp
                                <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                <td>{{$facility->code .' - '. $facility->name}}</td>
                                <td>{{$facility->department ? $facility->department->name : ''}}</td>
                                <td>{{date('Y/m/d', strtotime($facility->created_at))}}</td>
                                <td>{{$facility->roles_count}}</td>
                                <td>
                                    <a href="{{route('detail-decentralization-facility.index', ['id'=> $facility->id])}}" class="button-view-detail">
                                        <span class="text-view-detail">詳細</span>
                                        <img src="{{ asset('icons/view.svg') }}" alt="view">
                                    </a>
                                </td>
                            </tr>
                            @empty
                                <tr class="text-center">
                                    <td style="display: revert" colspan="5">{{\App\Messages::EMPTY_RECORD}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <nav class="wrapper-pagination">
                            {{ $results['list-data'] ->appends(request()->except('page'))->links('partial.admin.paginate') }}
                        </nav>
                    </div>
                </div>
            </div>
        @endif
        <!-- Wrapper tab -->
    </div>

    <div class="modal fade" id="modalEditDecentralizate" tabindex="-1" role="dialog"
         aria-labelledby="modalEditAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">権限編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <form action="{{route('detail-decentralization.update-role')}}" method="post">
                        <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                        @csrf
                        @method('put')
                        <input type="hidden" name="type" value="{{ request('type') ?? 'role-admin'}}">
                        @if($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{$errors->first()}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="form-group mr-20 mb-0">
                            <label class="label-form" for="business-selection">ユーザ名から選択</label>
                            <div class="selection-heading h-auto w-430 ml-0  mt-10 mb-50">
                                <select id="role-admin-selection" class="form-control dropdown-heading" style="width: 100%"
                                        name="users[]" multiple="multiple" data-placeholder=" ">
                                </select>
                                @if(request('type') == 'role-facility')
                                <label class="label-form mt-30 mb-10" for="">職種（グレード）から選択</label>
                                <div class="selection-heading h-auto w-430 ml-0  mt-10 mb-50">
                                    <select id="selection-position" class="form-control" name="positions[]" style="width: 100%" multiple data-placeholder="職種（グレード）から選択">
                                    </select>
                                </div>
                                @endif
                                @if(request('type') == 'role-admin' || !request('type'))
                                    <label class="label-form mt-30 mb-10" for="">部門から選択</label>
                                    <div class="selection-heading h-auto w-430 ml-0  mt-10 mb-50">
                                        <select id="selection-position" class="form-control" name="departments[]" style="width: 100%" multiple data-placeholder="部門から選択">
                                        </select>
                                    </div>
                                @endif
                            </div>
                            @error('users')
                            <div class="text-danger mt-2">{{ $message }}</div>
                            @enderror
                            <button type="submit"
                                    class="ml-0 button-add-branch default-button w-430">更新
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDeleteAlertID1" tabindex="-1" role="dialog" aria-labelledby="modalDeleteAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">使用者を削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form id="delete-form" action="{{route('detail-decentralization.delete-role')}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="hidden" name="type" value="{{ request('type') ?? 'role-admin'}}">
                            <input type="hidden" name="id" id="id">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                            <p class="modal-text"> @if(!request('type') || request('type') == 'role-admin')この使用者を管理者割当から削除してもよろしいですか？ @else この使用者を事業所から削除してもよろしいですか？ @endif</p>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">キャンセル</button>
                                <button type="submit" class="w-205 default-button" id="submit-form">
                                    適用</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function setGetParameter(paramName, paramValue) {
        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
        url += '?' + paramName + '=' + paramValue;
        window.location.href = url;
    }
    $(function () {
        // $('#role-admin-selection').select2({
        //     minimumInputLength: 1,
        //     closeOnSelect: false,
        //     language: {
        //         inputTooShort: function (args) {
        //
        //             return "任意の文字を入力してください。。。";
        //         },
        //         noResults: function () {
        //             return "見つかりません。。。";
        //         },
        //         searching: function () {
        //             return "検索しています。。。";
        //         }
        //     },
        // });
        // $('#selection-position').select2({
        //     closeOnSelect: false,
        //     language: {
        //         inputTooShort: function (args) {
        //
        //             return "任意の文字を入力してください。。。";
        //         },
        //         noResults: function () {
        //             return "見つかりません。。。";
        //         },
        //         searching: function () {
        //             return "検索しています。。。";
        //         }
        //     },
        // });
    })
    $('.delete-role').click(function () {
        var id = $(this).data('id');
        $("#id").val(id);
    });
    $('#updated_role_admin').click(function () {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault();
        $.ajax({
            type: "get",
            url: "{{route('decentralization.get_data_role_admin')}}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (result) {
                $('#role-admin-selection').empty();
                $('#selection-position').empty();
                   $.each(result.users, function (id, name) {
                       var selected = '';
                       if(jQuery.inArray(parseInt(id), result.usersSelect) !== -1) {
                           selected = 'selected';
                       }
                       $('#role-admin-selection').append('<option value="' + id + '" ' + selected + '>'+ id + ' - ' + name + '</option>');
                   })
                $.each(result.list_department, function (key, value) {
                    var selected = '';
                    if(jQuery.inArray(parseInt(value.id), result.departmentsSelect) !== -1) {
                        selected = 'selected';
                    }
                    var text = value.code;
                    if (value.name) {
                        text += ' - ' + value.name;
                    }
                    $('#selection-position').append('<option value="' + value.id + '" ' + selected + '>' + text + '</option>');
                })
                $('#role-admin-selection').trigger("chosen:updated");
                $('#selection-position').trigger("chosen:updated");
                $('#modalEditDecentralizate').modal({backdrop: 'static', keyboard: false});
                $('#modalEditDecentralizate').modal('show');
                $('#preloader').remove();
            },
            error: function (result) {
                $('#preloader').remove();
                alert('{{\App\Messages::SYSTERM_ERROR}}');
            }
        })
    })
    $('#updated_role_facility').click(function () {
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        event.preventDefault();
        $.ajax({
            type: "get",
            url: "{{route('decentralization.get_data_role_facility')}}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (result) {
                $('#role-admin-selection').empty();
                $('#selection-position').empty();
                $.each(result.users, function (id, name) {
                    var selected = '';
                    if (jQuery.inArray(parseInt(id), result.usersSelect) !== -1) {
                        selected = 'selected';
                    }
                    $('#role-admin-selection').append('<option value="' + id + '" ' + selected + '>'+id +' - ' + name + '</option>');
                })
                $.each(result.list_position, function (id, name) {
                    var selected = '';
                    if (jQuery.inArray(parseInt(id), result.positionsSelect) !== -1) {
                        selected = 'selected';
                    }
                    $('#selection-position').append('<option value="' + id + '" ' + selected + '>' + id + ' - ' + name + '</option>');
                })
                $('#role-admin-selection').trigger("chosen:updated");
                $('#selection-position').trigger("chosen:updated");
                $('#modalEditDecentralizate').modal({backdrop: 'static', keyboard: false})
                $('#modalEditDecentralizate').modal('show');
                $('#preloader').remove();
            },
            error: function (result) {
                $('#preloader').remove();
                alert('{{\App\Messages::SYSTERM_ERROR}}');
            }
        })
    })
</script>

@endsection
