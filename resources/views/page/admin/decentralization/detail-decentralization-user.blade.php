@extends('layouts.admin')
@section('title', '役割と権限の設定/詳細')
@section('content')
    @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="main-content main-admin pt-20" >
        <ol class="breadcrumb main-breadcrumb">
            <li class="breadcrumb-item main-breadcrumb-item"><a href="#">役割と権限の設定/ 詳細</a></li>
        </ol>

        <div class="main-heading mb-30">
            <h1 class="title-heading"> {{$results['user']->name}}</h1>
        </div>

        <div class="detail-info">
            <div class="row mb-20">
                <div class="col-md-2 left-detail-info">
                    メール
                </div>
                <div class="col-md-10 right-detail-info">
                    {{$results['user']->email}}
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-md-2 left-detail-info">
                    利用者数
                </div>
                <div class="col-md-10 right-detail-info">
                    {{$results['facilityUserCount']}}
                </div>
            </div>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <div class="top-table">
                    <button type="button"
                            class="button-add-branch default-button" id="save-decentralization" disabled
                            data-toggle="modal" data-target="#modalDeleteID1" data-backdrop="static"
                            data-keyboard="false">保存
                    </button>
                </div>
                <form action="{{route('detail-decentralization-facility.update')}}" id="decentralization-update"
                      class="" method="post">
                    @csrf
                    {{ method_field('put') }}
                    <table class="table table-striped table-admin mt-15 mb-15">
                        <thead>
                        <tr>
                            <th width="10%" scope="col">順番</th>
                            <th width="18%" scope="col">氏名</th>
                            <th width="18%" scope="col">メール</th>
                            <th width="15%" scope="col">最終更新日</th>
                            <th width="12%" scope="col">閲覧権限</th>
                            <th width="12%" scope="col">請求権限</th>
                            <th width="15%" scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($results['userRols'] as $key => $role)
                            <tr>
                                @php
                                    $page = request('page') ?? \App\Consts::BASE_PAGE;
                                @endphp
                                <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                                <td>{{$results['facilities'][$role->facilities_id]['name']}}</td>
                                <td>{{$results['facilities'][$role->facilities_id]['email']}}</td>
                                <td>{{date('Y/m/d', strtotime($role->created_at))}}</td>
                                <td>
                                    <div class="container-checkbox">
                                        <input type="checkbox" class="view" id="id1-{{$key}}"
                                               name="view[{{$role->id}}]"
                                               @if($role->view)checked @endif data-edit-id="id2-{{$key}}" data-id="{{$role->id}}">
                                        <label for="id1-{{$key}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <!-- note: tag input id=for of tag label -->
                                    <div class="container-checkbox">
                                        <input type="checkbox" class="edit" id="id2-{{$key}}"
                                               data-view-id="id1-{{$key}}" name="edit[{{$role->id}}]"
                                               @if($role->edit)checked @endif data-id="{{$role->id}}">
                                        <label for="id2-{{$key}}"></label>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        @empty
                            <tr class="text-center">
                                <td colspan="6" class="">
                                    {{\App\Messages::EMPTY_RECORD}}
                                </td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </form>
                <nav class="wrapper-pagination">
                    {{ $results['userRols']->appends(request()->except('page'))->links('partial.admin.paginate') }}

                </nav>
            </div>
        </div>
    </div>
    <!-- content page -->
    </section>

<div class="modal fade" id="modalDeleteID1" tabindex="-1" role="dialog" aria-labelledby="modalDeleteID1Title"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">ユーザー権限変更</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                        <p class="modal-text"> ユーザーの権限を変更してもよろしいですか。？</p>
                        <div class="d-flex align-items-center justify-content-center mt-40">
                            <button type="button" class="w-430 default-button" data-dismiss="modal" aria-label="Close"
                                    data-toggle="modal" id="submit-form" data-target="#modalSuccessID1" data-backdrop="static"
                                    data-keyboard="false">同意
                            </button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal B1001.3a Delete グループ削除削除 -->
<script>
    $(document).ready(function() {
        $('.edit').change(function (event) {
            $('#save-decentralization').attr('disabled', false);
            $('#id_roles-id'+$(this).data("id")).remove();
            $('#decentralization-update').append('<input type="hidden" id="id_roles-id'+$(this).data("id")+'" name="id_roles[]" value="'+$(this).data("id")+'">')
            if (this.checked) {
                var viewId = $(this).data('view-id');
                if (!$('#'+viewId).checked) {
                    $('#'+viewId).prop('checked', true);
                }
            }
        })
        $('.view').change(function (event) {
            $('#id_roles-id'+$(this).data("id")).remove();
            var id = $(this).data("id");
            $('#decentralization-update').append('<input type="hidden" id="id_roles-id'+$(this).data("id")+'" name="id_roles[]" value="'+id+'">')
            $('#save-decentralization').attr('disabled', false);
            if (!this.checked) {
                var editId = $(this).data('edit-id');
                $('#' + editId).prop('checked', false);
            }
        })
    })
    $("#submit-form").click(function (event){
        $('#decentralization-update').submit();
    })
</script>
@endsection
