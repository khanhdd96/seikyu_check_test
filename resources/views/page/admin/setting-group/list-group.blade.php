@extends('layouts.admin')
@section('title', 'グループ一覧管理')
@section('content')
    <style>
        .list-groups thead tr th:nth-child(1),
        .list-groups tbody tr td:nth-child(1){
            width: 10%;
        }
        .list-groups thead tr th:nth-child(2),
        .list-groups tbody tr td:nth-child(2){
            width: 30%;
        }
        .list-groups thead tr th:nth-child(3),
        .list-groups tbody tr td:nth-child(3){
            width: 15%;
        }
        .list-groups thead tr th:nth-child(4),
        .list-groups tbody tr td:nth-child(4){
            width: 15%;
        }
        .list-groups thead tr th:nth-child(5),
        .list-groups tbody tr td:nth-child(5){
            width: 10%;
        }
        #table-facility thead tr th:nth-child(1),
        #table-facility tbody tr td:nth-child(1),
        #table-user thead tr th:nth-child(1),
        #table-user tbody tr td:nth-child(1),
        #detail-item thead tr th:nth-child(1),
        #detail-item tbody tr td:nth-child(1),
        #detail-item-user-admin thead tr th:nth-child(1),
        #detail-item-user-admin tbody tr td:nth-child(1){
            width: 15%;
        }
        #table-facility thead tr th:nth-child(2),
        #table-facility tbody tr td:nth-child(2),
        #table-user thead tr th:nth-child(2),
        #table-user tbody tr td:nth-child(2),
        #detail-item thead tr th:nth-child(2),
        #detail-item tbody tr td:nth-child(2),
        #detail-item-user-admin thead tr th:nth-child(2),
        #detail-item-user-admin tbody tr td:nth-child(2){
            width: 35%;
        }
        #table-facility thead tr th:nth-child(3),
        #table-facility tbody tr td:nth-child(3),
        #table-user thead tr th:nth-child(3),
        #table-user tbody tr td:nth-child(3){
            width: 50%;
        }
        .text-nowrap{
            white-space: nowrap;
        }
        .max-width-652 {
            max-width: 652px;
        }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">グループ一覧管理</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" autocomplete="off">
                    <div id="datepicker" class="datepicker-heading mr-15">
                        <input class="input-datepicker" type="text" name="date" placeholder="年/月/日"
                               value="{{request('date')}}">
                        <span class="icon-datepicker">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                            fill="#7C9AA1"/>
                                        <path opacity="0.4"
                                              d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                              fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                            fill="#7C9AA1"/>
                                        <path
                                            d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                            fill="#7C9AA1"/>
                                    </svg>
                                </span>
                    </div>
                    <div class="wrapper-search">
                        <input class="input-search" type="text" placeholder="検索" name="search"
                               value="{{request('search')}}">
                        <button class="button-search" type="button" disabled>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                      d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                      fill="#7C9AA1"/>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"/>
                            </svg>
                        </button>
                    </div>

                    <div class="search-button" style="width:50px">
                        <button class="button-search" type="submit">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                      stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <!-- button show modal 3004.4 -->
                    <button
                        type="button"
                        class="button-add-branch default-button"
                        data-toggle="modal"
                        data-target="#modalEditGroupID1"
                        data-backdrop="static"
                        data-keyboard="false"
                    >
                        新グループ作成
                    </button>
                </form>

                <table class="table table-striped table-admin mt-15 mb-20 list-groups">
                    <thead>
                        <tr>
                            <th scope="col">順番</th>
                            <th scope="col">グループ名</th>
                            <th scope="col">作成日</th>
                            <th scope="col">作成時</th>
                            <th scope="col">合計</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($data['list-data'] as $key => $group)
                        <tr>
                        @php
                            $page = request('page') ?? \App\Consts::BASE_PAGE;
                        @endphp
                            <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                            <td>{{$group->group_name}}</td>
                            <td style="white-space: nowrap">{{date('Y/m/d', strtotime($group->created_at))}}</td>
                            <td>{{date('H:i', strtotime($group->created_at))}}</td>
                            <td>{{$data['dataCounts'][$group->id]['user'] + $data['dataCounts'][$group->id]['facilitity_ids']}}</td>
                            <td style="white-space: nowrap">
                                <!-- Button Modal B3004.1 -->
                                <button type="button" class="button-view-detail button-detail" data-id="{{$group->id}}">
                                    <span class="text-view-detail">詳細</span>
                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                </button>

                                <!-- Button Modal B3004.2 -->
                                <button type="button" class="button-view-edit button-view-detail"
                                        data-id="{{$group->id}}" data-keyboard="false">
                                    <span class="text-view-detail">編集</span>
                                    <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                                </button>

                                <!-- Button Modal xoá alert B3004 -->
                                <button type="button" class="button-delete button-view-detail text-nowrap" data-id="{{$group->id}}">
                                    <span class="text-view-detail">削除</span>
                                    <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr class="text-center">
                            <td style="display: revert" colspan="6" class="text-center">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                    {{ $data['list-data']->appends(request()->except('page'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>
    <!-- Modal B3004.4 新グループ作成 -->
    <div class="modal fade" id="modalAddAcountID1" tabindex="-1" role="dialog" aria-labelledby="modalAddAcountID1Title"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered max-width-652" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">新グループ作成</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body max-height">
                    <div class="add-employee">
                        <div class="wrapper-search w-100 mb-30" id="search-facility-block">
                            <input class="input-search" type="text" maxlength="30" style="width: calc(100% - 60px);"
                                   placeholder="検索事業所" name="search" autocomplete="off">
                            <div class="button-search" style="right: 65px">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4"
                                          d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                          fill="#7C9AA1"/>
                                    <path
                                        d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                        fill="#7C9AA1"/>
                                </svg>
                            </div>
                            <div class="search-button float-right" style="width:50px">
                                <button class="button-search" type="submit" id="group-search">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                            stroke="white" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>

                        <div class="wrapper-list-option mb-30">
                            <div class="heading-list-option">
                                <h5 class="heading-title-option">事業所一覧</h5>
                                <img class="heading-icon-option" src="{{ asset('icons/arrow-circle-heading.svg') }}"
                                     alt="arrow-circle">
                            </div>
                            <div class="body-list-option scroll-bar-hidden" id="list-facility">
                                <div class="body-group-option mb-20">
                                    <div class="container-checkbox container-checkbox-group">
                                        <input type="checkbox" id="select-all">
                                        <label for="select-all">全て事業所を追加</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-danger mt-2" id="facilities-error"></div>
                        <input type="hidden" name="facility_ids[]" value="" id="facilities-id-searched">
                        <input type="hidden" name="user_ids[]" value="" id="users-id-searched">
                        <div class="wrapper-search w-100 mb-30" id="search-user-block">
                            <input class="input-search" type="text" maxlength="30" style="width: calc(100% - 60px);"
                                   placeholder="検索経理者" name="search" autocomplete="off">
                            <div class="button-search" style="right: 65px" disabled>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.4"
                                          d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                          fill="#7C9AA1"/>
                                    <path
                                        d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                        fill="#7C9AA1"/>
                                </svg>
                            </div>
                            <div class="search-button float-right" style="width:50px">
                                <button class="button-search" type="submit" id="group-search">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                            stroke="white" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="wrapper-list-option mb-30">
                            <div class="heading-list-option">
                                <h5 class="heading-title-option">営業経理一覧</h5>
                                <img class="heading-icon-option" src="{{ asset('icons/arrow-circle-heading.svg') }}"
                                     alt="arrow-circle">
                            </div>
                            <div class="body-list-option scroll-bar-hidden" id="list-user-admin">
                                <div class="body-group-option mb-20">
                                    <div class="container-checkbox container-checkbox-group">
                                        <input type="checkbox" id="select-all-user-admin" name="" checked>
                                        <label for="select-all-user-admin">全て経理のアカウントを追加</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- if edit alert success display modal B3004.5 success -->
                        <div class="d-flex align-items-center justify-content-center mt-50">
                            <button type="button" class="w-315 second-button mr-10" id="close-form-search-group">キャンセル
                            </button>
                            <button type="submit" class="w-315 default-button" id="submit-form-search-group">
                                作成
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004.4 新グループ作成 -->

    <!-- Modal B3004.1 詳細なグループ -->
    <div class="modal fade" id="modalDetailGroupID1" tabindex="-1" role="dialog"
         aria-labelledby="modalDetailGroupID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-652" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">詳細なグループ</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height">
                    <div class="add-employee">
                        <form>
                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">グループ名</div>
                                <div class="col-8 text-setting-alert" id="name-group">aml jp co group</div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">作成日</div>
                                <div class="col-8 text-setting-alert" id="date-create">2021/19/01</div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">作成時</div>
                                <div class="col-8 text-setting-alert" id="time-create">20:00</div>
                            </div>

                            <div class="row mb-20">
                                <div class="col-4 label-form mb-0">合計</div>
                                <div class="col-8 text-setting-alert" id="count-item">5</div>
                            </div>
                            <div class="mb-16">
                                <h5 class="heading-title-option" id="heading-list-facility">事業所一覧</h5>
                            </div>
                            <div class="wrapper-table pt-10 pb-0 mt-30 body-list-option scroll-bar-hidden">
                                <table class="table table-striped table-admin table-no-action mt-15 mb-15"
                                       id="detail-item">
                                    <thead>
                                    <tr>
                                        <th scope="col">順番</th>
                                        <th scope="col">氏名</th>
                                        <th scope="col">メール</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mb-16 mt-30">
                                <h5 class="heading-title-option" id="heading-list-user">経理担当者一覧</h5>
                            </div>
                            <div class="wrapper-table pt-10 pb-0 mt-30 body-list-option scroll-bar-hidden">
                                <table class="table table-striped table-admin table-no-action mt-15 mb-15"
                                       id="detail-item-user-admin">
                                    <thead>
                                    <tr>
                                        <th scope="col">順番</th>
                                        <th scope="col">氏名</th>
                                        <th scope="col">メール</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004.1 詳細なグループ -->

    <!-- Modal B3004.2 グループ編集 -->
    <div class="modal fade" id="modalEditGroupID1" tabindex="-1" role="dialog" aria-labelledby="modalEditGroupID1Title"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog max-width-652" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">グループ編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height">
                    <div class="add-employee">
                        <form class="text-left" method="post" id="store-group"
                              action="{{route('setting-group-user.store')}}">
                            <div class="form-group w-430 mb-10">
                                <label class="label-form" for="group-name-3">グループ名 </label>
                                <input type="text" class="form-control-add" id="group-name-3" name="group_name"
                                      maxlength="30" placeholder="グループ名" value="" autocomplete="off">
                            </div>
                            <div class="text-danger mt-0 mb-30" id="group-name-error"></div>
                            <!-- button show modal 3004.3 -->
                            <button type="button" id="select-facilities-and-user"
                                    class="button-view-detail button-add-modal ml-auto mb-15" data-toggle="modal"
                                    data-target="#modalAddAcountID1" data-backdrop="static" data-keyboard="false">
                                <img src="{{ asset('icons/add.svg') }}" alt="add">
                                <span class="text-add-modal">アカウント追加</span>
                            </button>
                            <div class="mb-16 d-flex">
                                <h5 class="heading-title-option w-50" id="heading-list-facility">事業所一覧</h5>
                            </div>
                            <div class="wrapper-table pt-10 pb-0 mt-30 body-list-option scroll-bar-hidden">
                                <table class="table table-striped table-admin mt-15 mb-15" id="table-facility">
                                    <thead>
                                    <tr>
                                        <th scope="col">順番</th>
                                        <th scope="col">氏名</th>
                                        <th scope="col">メール</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mb-16 mt-30 d-flex">
                                <h5 class="heading-title-option w-50" id="heading-list-user">経理担当者一覧</h5>
                            </div>
                            <div class="wrapper-table pt-10 pb-0 mt-30 body-list-option scroll-bar-hidden">
                                <table class="table table-striped table-admin mt-15 mb-15" id="table-user">
                                    <thead>
                                    <tr>
                                        <th scope="col">順番</th>
                                        <th scope="col">氏名</th>
                                        <th scope="col">メール</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <button style="width: 100%; margin: 50px auto 0 !important" disabled
                                    type="submit"
                                    class="default-button" id="submit-form-store-group">
                                作成
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004.2 グループ編集 -->

    <!-- Modal B3004.6 Xoá Nhóm -->
    <div class="modal fade" id="modalDeleteAlertID1" tabindex="-1" role="dialog"
         aria-labelledby="modalDeleteAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">グループ削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form id="delete-form" action="{{route('setting-group-user.delete')}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="hidden" name="id" id="id-group">
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}"
                                 alt="question-mark">
                            <p class="modal-text"> このグループを削除してもよろしいですか？</p>
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal"
                                        aria-label="Close">キャンセル
                                </button>
                                <button type="submit" class="w-205 default-button" id="submit-form">
                                    閉じる
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004.6 Xoá Nhóm -->

    <!-- Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->
    <div class="modal fade" id="modalDeleteObjectID1" tabindex="-1" role="dialog"
         aria-labelledby="modalDeleteObjectID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">対象削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}"
                                 alt="question-mark">
                            <p class="modal-text"> この対象をグループから削除してもよろしいですか？</p>
                            <button type="button" class="home-modal-button default-button" id="delete-object-group"
                                    data-dismiss="modal" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->

    <!-- modal B3004.5 success -->
    <div class="modal fade" id="modalEditAlertSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalEditAlertSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="modal-tile">新グループ作成</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="edit-alert-success">
                        <form>
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text" id="text-noti"> 新グループ作成を成功しました。</p>
                            <button type="button" class="home-modal-button default-button" id="reload-page"
                                    data-dismiss="modal" aria-label="Close">閉じる
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal B3004.5 success -->

    <!-- Modal B3004.3 アカウント追加 -->
    <script>
        $(document).ready(function () {
            $("#datepicker input").datepicker({
                format: "yyyy/mm/dd",
                autoclose: true,
                language: 'ja',
            });
            $('.icon-datepicker').on('click', function () {
                $('#datepicker input').trigger('focus');
            })
        })
        $('.button-delete').click(function () {
            $('#modalDeleteAlertID1 #id-group').val($(this).data('id'));
            $('#modalDeleteAlertID1').modal({backdrop: 'static', keyboard: false})
            $('#modalDeleteAlertID1').modal('show');
        })
        $('.button-detail').click(function (event) {
            $('#detail-item tbody').empty();
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            $.ajax({
                url: '{{route('setting-group-user.show')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id')
                },
                success: function ($results) {
                    var group = $results.group
                    var users = $results.user
                    var facilities = $results.facilities
                    $('#name-group').text(group.group_name);
                    $('#date-create').text(moment(new Date(group.created_at)).utcOffset('GMT +09:00').format('yyyy/MM/DD'));
                    $('#time-create').text(moment(new Date(group.created_at)).utcOffset('GMT +09:00').format('HH:mm'));
                    $('#count-item').text(users.length + facilities.length);
                    var dataTr = '';
                    var countUser = 0
                    $.each(users, function (index, value) {
                        var name = value.name ?? '';
                        var email = value.email ?? '';
                        dataTr += '<tr><td>' + index + '</td>\n' +
                            '<td>' + name + '</td>\n' +
                            '<td>' + email + '</td>\n' +
                            '<td>\n' +
                            'ユーザー\n' +
                            '</td></tr>';
                        index++;
                    });
                    var countFacility = 0;
                    $.each(facilities, function (index, value) {
                        var name = value.name ?? '';
                        var email = value.email ?? '';
                        dataTr += '<tr><td>' + index + 1 + '</td>\n' +
                            '<td>' + name + '</td>\n' +
                            '<td>' + email + '</td>\n' +
                            '<td>\n' +
                            '事業所\n' +
                            '</td></tr>';
                        index++;
                    });
                    $('#detail-item tbody').append(dataTr);
                    $('#preloader').remove();
                    $('#modalDetailGroupID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalDetailGroupID1').modal('show');
                },
                error: function () {
                    $('#preloader').remove();
                    if (jqXHR.status == 404) {
                        alert('{{\App\Messages::EMPTY_RECORD}}');
                    } else {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                    }
                }
            })
        })
        $('#modalAddAcountID1 #search-facility-block #group-search').click(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var ids = $('#modalAddAcountID1 input.facilities-id-searched').map((_,el) => el.value).get()
            $.ajax({
                url: '{{route('setting-group-user.search-facility')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'keyword': $('#modalAddAcountID1 input[name="search"]').val(),
                    'ids': ids,
                },
                success: function (results) {
                    var facilities = results.facilities;
                    $('#modalAddAcountID1 #list-facility .remove-item input:not(:checked)').parents('.remove-item ').remove();
                    var text = '';
                    $.each(facilities, function (index, value) {
                        text += '<div class="sub-options mb-10 ml-50 remove-item">\n' +
                            '                                <div class="d-flex align-items-center parent-option mb-10">' +
                            '                                    <div class="container-checkbox facility-checkbox">\n' +
                            '                                        <input type="checkbox" id="facility-' + value.id + '" '+results.checked+' name="facilities[]" value="' + value.id + '" data-name="' + value.name + '" data-email="' + value.email + '">' +
                            '                                            <label for="facility-' + value.id + '">' + value.name + '</label>' +
                            '                                    </div>' +
                            '                                </div>' +
                            '                            </div>';
                        if (results.checked != '') {
                            $('#modalAddAcountID1 #list-facility').append('<input type="hidden" class="facilities-id-searched" id="facility-id-' + value.id + '" name="facility_ids[]" value="' + value.id + '">');
                        }
                    });
                    $('#modalAddAcountID1 #list-facility').append(text);
                    var input = $('#list-facility').find('.remove-item input').length;
                    var inputChecked = $('#list-facility').find('.remove-item input:checked').length;
                    if (input > 0) {
                        $('#modalAddAcountID1 #list-facility div').removeClass('d-none');
                    } else {
                        $('#modalAddAcountID1 #list-facility div.body-group-option.mb-20').addClass('d-none');
                    }
                    if (inputChecked == input) {
                        $('#select-all').prop('checked', true);
                    } else {
                        $('#select-all').prop('checked', false);
                    }
                    $('.facility-checkbox input').click(function () {
                        var input = $(this).parents('#list-facility').find('.remove-item input').length;
                        var inputChecked = $(this).parents('#list-facility').find('.remove-item input:checked').length;
                        if (inputChecked == input) {
                            $('#select-all').prop('checked', true);
                        } else {
                            $('#select-all').prop('checked', false);
                        }
                        if (this.checked) {
                            $('#modalAddAcountID1 #list-facility').append('<input type="hidden" class="facilities-id-searched" id="facility-id-'+$(this).val()+'" name="facility_ids[]" value="'+$(this).val()+'">');
                        } else {
                            $('#modalAddAcountID1 #list-facility input#facility-id-'+$(this).val()).remove();
                        }
                    })
                    $('#preloader').remove();
                },
                error: function () {
                    $('#preloader').remove();
                    alert('{{\App\Messages::SYSTERM_ERROR}}');
                }
            })
        })
        $('#modalAddAcountID1').on('shown.bs.modal', function () {
            $('#select-all').click(function () {
                if (this.checked) {
                    $(this).parents('#list-facility').find('input').prop('checked', true);
                    var ids = $('#modalAddAcountID1 #list-facility .remove-item input').map((_,el) => el.value).get();
                    $('#modalAddAcountID1 .facilities-id-searched').remove();
                    $.each(ids, function (index, value) {
                        $('#modalAddAcountID1 #list-facility').append('<input type="hidden" class="facilities-id-searched" id="facility-id-'+ value +'" name="facility_ids[]" value="'+ value+'">');
                    })
                } else {
                    $(this).parents('#list-facility').find('input').prop('checked', false);
                    $('#modalAddAcountID1 .facilities-id-searched').remove();
                }
            })
            $('.facility-checkbox input').click(function () {
                var input = $(this).parents('#list-facility').find('.remove-item input').length;
                var inputChecked = $(this).parents('#list-facility').find('.remove-item input:checked').length;
                if (inputChecked == input) {
                    $('#select-all').prop('checked', true);
                } else {
                    $('#select-all').prop('checked', false);
                }
                if (this.checked) {
                    $('#modalAddAcountID1 #list-facility').append('<input type="hidden" class="facilities-id-searched" id="facility-id-'+$(this).val()+'" name="facility_ids[]" value="'+$(this).val()+'">');
                } else {
                    $('#modalAddAcountID1 #list-facility input#facility-id-'+$(this).val()).remove();
                }
            })
            $('.user-checkbox input').click(function () {
                var input = $(this).parents('#list-user-admin').find('.remove-item input').length;
                var inputChecked = $(this).parents('#list-user-admin').find('.remove-item input:checked').length;
                if (inputChecked == input) {
                    $('#select-all-user-admin').prop('checked', true);
                } else {
                    $('#select-all-user-admin').prop('checked', false);
                    $('#modalAddAcountID1 .users-id-searched').remove();
                }
                if (this.checked) {
                    $('#modalAddAcountID1 #list-user-admin').append('<input type="hidden" class="users-id-searched" id="user-id-' + $(this).val() + '" value="' + $(this).val() + '">');
                } else {
                    $('#modalAddAcountID1 #list-list-user-admin input#user-id-' + $(this).val()).remove();
                }
            });
            $('#select-all-user-admin').click(function () {
                if (this.checked) {
                    $(this).parents('#list-user-admin').find('input').prop('checked', true);
                    var ids = $('#modalAddAcountID1 #list-user-admin .remove-item input').map((_,el) => el.value).get();
                    $('#modalAddAcountID1 .users-id-searched').remove();
                    $.each(ids, function (index, value) {
                        $('#modalAddAcountID1 #list-user-admin').append('<input type="hidden" class="users-id-searched" id="user-id-' + value + '" value="' + value + '">');
                    })
                } else {
                    $(this).parents('#list-user-admin').find('input').prop('checked', false);
                    $('#modalAddAcountID1 .users-id-searched').remove();
                }
            })
        });
        $('.button-detail').click(function (event) {
            $('#detail-item tbody').empty();
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            $.ajax({
                url: '{{route('setting-group-user.show')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id')
                },
                success: function ($results) {
                    var group = $results.group
                    var users = $results.user
                    var facilities = $results.facilities
                    $('#name-group').text(group.group_name);
                    $('#date-create').text(moment(new Date(group.created_at)).utcOffset('GMT +09:00').format('yyyy/MM/DD'));
                    $('#time-create').text(moment(new Date(group.created_at)).utcOffset('GMT +09:00').format('HH:mm'));
                    $('#count-item').text(users.length + facilities.length);
                    var dataTr = '';
                    var dataTr1 = '';
                    $.each(users, function (index, value) {
                        var name = value.name ?? '';
                        var email = value.email ?? '';
                        dataTr1 += '<tr><td>' + (index + 1) + '</td>\n' +
                            '<td>' + name + '</td>\n' +
                            '<td>' + email + '</td>\n' +
                            '=</tr>';
                    });
                    $('#modalDetailGroupID1 #heading-list-facility').text('事業所一覧 (' + facilities.length + ')');
                    $('#modalDetailGroupID1 #heading-list-user').text('経理担当者一覧 (' + users.length + ')');
                    $.each(facilities, function (index, value) {
                        var name = value.name ?? '';
                        var email = value.email ?? '';
                        dataTr += '<tr><td>' + (index + 1) + '</td>\n' +
                            '<td>' + name + '</td>\n' +
                            '<td>' + email + '</td>\n' +
                            '</tr>';
                    });
                    $('#detail-item tbody').empty();
                    $('#detail-item tbody').append(dataTr);
                    $('#detail-item-user-admin tbody').empty();
                    $('#detail-item-user-admin tbody').append(dataTr1);
                    $('#preloader').remove();
                    $('#modalDetailGroupID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalDetailGroupID1').modal('show');
                },
                error: function () {
                    $('#preloader').remove();
                    alert('{{\App\Messages::SYSTERM_ERROR}}');
                }
            })
        })
        $('#modalAddAcountID1 #search-user-block #group-search').click(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var ids = $('#modalAddAcountID1 input.users-id-searched').map((_,el) => el.value).get()
            $.ajax({
                url: '{{route('setting-group-user.search-user')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'keyword': $('#modalAddAcountID1 #search-user-block input[name="search"]').val(),
                    'ids': ids
                },
                success: function (results) {
                    var users = results.users;
                    var text = '';
                    $('#modalAddAcountID1 #list-user-admin .remove-item input:not(:checked)').parents('.remove-item ').remove();
                    $.each(users, function (index, value) {
                        text += '<div class="sub-options mb-10 ml-50 remove-item">\n' +
                            '                                <div class="d-flex align-items-center parent-option mb-10">' +
                            '                                    <div class="container-checkbox user-checkbox">\n' +
                            '                                        <input type="checkbox" id="user-' + value.id + '" ' + results.checked + ' name="users[]" value="' + value.id + '" data-name="' + value.name + '" data-email="' + value.email + '">' +
                            '                                            <label for="user-' + value.id + '">' + value.name + '</label>' +
                            '                                    </div>' +
                            '                                </div>' +
                            '                            </div>';
                        if (results.checked != '') {
                            $('#modalAddAcountID1 #list-user-admin').append('<input type="hidden" class="users-id-searched" id="user-id-' + value.id + '" value="' + value.id + '">');
                        }
                    });
                    $('#modalAddAcountID1 #list-user-admin').append(text);
                    var input = $('#list-user-admin').find('.remove-item input').length;
                    var inputChecked = $('#list-user-admin').find('.remove-item input:checked').length;
                    if (input > 0) {
                        $('#modalAddAcountID1 #list-user-admin div').removeClass('d-none');
                    } else {
                        $('#modalAddAcountID1 #list-user-admin div.body-group-option.mb-20').addClass('d-none');
                    }
                    if (inputChecked == input) {
                        $('#select-all-user-admin').prop('checked', true);
                    } else {
                        $('#select-all-user-admin').prop('checked', false);
                    }
                    $('.user-checkbox input').click(function () {
                        var input = $(this).parents('#list-user-admin').find('.remove-item input').length;
                        var inputChecked = $(this).parents('#list-user-admin').find('.remove-item input:checked').length;
                        if (inputChecked == input) {
                            $('#select-all-user-admin').prop('checked', true);
                        } else {
                            $('#select-all-user-admin').prop('checked', false);
                        }
                        if (this.checked) {
                            $('#modalAddAcountID1 #list-user-admin').append('<input type="hidden" class="users-id-searched" id="user-id-'+$(this).val()+'" value="'+$(this).val()+'">');
                        } else {
                            $('#modalAddAcountID1 #list-user-admin input#user-id-'+$(this).val()).remove();
                        }
                    })
                    $('#preloader').remove();
                },
                error: function () {
                    $('#preloader').remove();
                    alert('{{\App\Messages::SYSTERM_ERROR}}');
                }
            })
        })
        $('#select-facilities-and-user').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            $.ajax({
                url: '{{route('setting-group-user.search-all')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function (results) {
                    var facilities = results.listFacility;
                    if (facilities.length > 0) {
                        $('#modalAddAcountID1 #list-facility div').removeClass('d-none');
                    }
                    var text = '';
                    $.each(facilities, function (index, value) {
                        text += '<div class="sub-options mb-10 ml-50 remove-after-search remove-item">\n' +
                            '                                <div class="d-flex align-items-center parent-option mb-10">' +
                            '                                    <div class="container-checkbox facility-checkbox">\n' +
                            '                                        <input type="checkbox" checked id="facility-' + value.id + '" name="facilities[]"  checked value="' + value.id + '" data-name="' + value.name + '" data-email="' + value.email + '">' +
                            '                                            <label for="facility-' + value.id + '">' + value.name + '</label>' +
                            '                                    </div>' +
                            '                                </div>' +
                            '                            </div>';
                        $('#modalAddAcountID1 #list-facility').append('<input type="hidden" class="facilities-id-searched" id="facility-id-'+value.id+'" name="facility_ids[]" value="'+value.id+'">');
                    });
                    $('#modalAddAcountID1 #list-facility').append(text);
                    $('#modalAddAcountID1 input#facilities-id-searched').val(results.idsSearched);
                    var input = $('#list-facility').find('.remove-after-search input').length;
                    var inputChecked = $('#list-facility').find('.remove-after-search input:checked').length;
                    if (inputChecked == input) {
                        $('#select-all').prop('checked', true);
                    } else {
                        $('#select-all').prop('checked', false);
                    }
                    $('.facility-checkbox input').click(function () {
                        var input = $(this).parents('#list-facility').find('.remove-after-search input').length;
                        var inputChecked = $(this).parents('#list-facility').find('.remove-after-search input:checked').length;
                        if (inputChecked == input) {
                            $('#select-all').prop('checked', true);
                        } else {
                            $('#select-all').prop('checked', false);
                        }
                    })
                    var users = results.listAccounting;
                    var text = '';
                    if (users.length > 0) {
                        $('#modalAddAcountID1 #list-user-admin div').removeClass('d-none');
                    }
                    $.each(users, function (index, value) {
                        text += '<div class="sub-options mb-10 ml-50 remove-after-search remove-item">\n' +
                            '                                <div class="d-flex align-items-center parent-option mb-10">' +
                            '                                    <div class="container-checkbox user-checkbox">\n' +
                            '                                        <input type="checkbox" checked id="user-' + value.id + '" name="users[]" value="' + value.id + '" data-name="' + value.name + '" data-email="' + value.email + '">' +
                            '                                            <label for="user-' + value.id + '">' + value.name + '</label>' +
                            '                                    </div>' +
                            '                                </div>' +
                            '                            </div>';
                        $('#modalAddAcountID1 #list-user-admin').append('<input type="hidden" class="users-id-searched" id="user-id-'+value.id+'" value="'+value.id+'">');
                    });
                    $('#modalAddAcountID1 #list-user-admin').append(text);
                    $('#modalAddAcountID1 input#users-id-searched').val(results.idsSearched);
                    var input = $('#list-user-admin').find('.remove-after-search input').length;
                    var inputChecked = $('#list-user-admin').find('.remove-after-search input:checked').length;
                    if (inputChecked == input) {
                        $('#select-all-user-admin').prop('checked', true);
                    } else {
                        $('#select-all-user-admin').prop('checked', false);
                    }
                    $('.user-checkbox input').click(function () {
                        var input = $(this).parents('#list-user-admin').find('.remove-after-search input').length;
                        var inputChecked = $(this).parents('#list-user-admin').find('.remove-after-search input:checked').length;
                        if (inputChecked == input) {
                            $('#select-all-user-admin').prop('checked', true);
                        } else {
                            $('#select-all-user-admin').prop('checked', false);
                        }
                    })
                    $('#preloader').remove();
                },
                error: function () {
                    $('#preloader').remove();
                    alert('{{\App\Messages::SYSTERM_ERROR}}');
                }
            })
            $('#modalEditGroupID1').modal('hide');
            $('#modalAddAcountID1').modal({backdrop: 'static', keyboard: false})
            $('#modalAddAcountID1').modal('show');
        })
        $('#modalAddAcountID1 #close-form-search-group').click(function () {
            $('#modalEditGroupID1').modal({backdrop: 'static', keyboard: false})
            $('#modalEditGroupID1').modal('show');
            $('#modalAddAcountID1').modal('hide');
        })
        $('#modalAddAcountID1').on('hidden.bs.modal', function (e) {
            $('#modalAddAcountID1 .remove-item').remove();
            $('#modalAddAcountID1 .facilities-id-searched').remove();
            $('#modalAddAcountID1 .users-id-searched').remove();
            $('#modalAddAcountID1 input').val('');
            $('#modalEditGroupID1').modal({backdrop: 'static', keyboard: false})
            $('#modalEditGroupID1').modal('show');
        });
        $('#modalAddAcountID1 #submit-form-search-group').click(function () {
            var index = $("#table-facility td.order").last().text() - 1 >= 0 ? $("#table-facility td.order").last().text() : 0;
            $("#list-facility .remove-item input:checked").each(function () {
                if ($('.facility-added[value="' + $(this).val() + '"]').length == 0) {
                    index++;
                    $('#modalEditGroupID1 #store-group').append('<input type=hidden class="facility remove-item" name="facilities[]" value="' + $(this).val() + '">')
                    $('#modalEditGroupID1 #store-group').append('<input type=hidden class="facility-added remove-item" name="facility-added[]" value="' + $(this).val() + '">')
                    var email = $(this).data("email") ?? '';
                    var name = $(this).data("name") ?? '';
                    $('#modalEditGroupID1 #table-facility').append('<tr id="facility-' + $(this).val() + '" class="remove-item">\n' +
                        '                        <td class="order">' + index + '</td>\n' +
                        '                        <td>' + name + '</td>\n' +
                        '                        <td>' + email + '</td>\n' +
                        '                        <td>\n' +
                        '                            <!-- Button Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->\n' +
                        '                            <button type="button" class="button-view-detail delete-object-group text-nowrap" onclick="deleteObjectGroup(\'facility\', ' + $(this).val() + ')">\n' +
                        '                                <span class="text-view-detail">削除</span>\n' +
                        '                                <img src="{{ asset("icons/trash.svg") }}" alt="trash">\n' +
                        '                            </button>\n' +
                        '                        </td>\n' +
                        '                    </tr>');
                }
            });
            var indexUser = $("#table-user td.order").last().text() - 1 >= 0 ? $("#table-user td.order").last().text() : 0;
            $("#list-user-admin .remove-item input:checked").each(function () {
                if ($('.user-added[value="' + $(this).val() + '"]').length == 0) {
                    indexUser++;
                    $('#modalEditGroupID1 #store-group').append('<input type=hidden class="user remove-item" name="users[]" value="' + $(this).val() + '">')
                    $('#modalEditGroupID1 #store-group').append('<input type=hidden class="user-added remove-item" name="user-added[]" value="' + $(this).val() + '">')
                    var email = $(this).data("email") ?? '';
                    var name = $(this).data("name") ?? '';
                    $('#modalEditGroupID1 #table-user').append('<tr id="user-' + $(this).val() + '" class="remove-item">\n' +
                        '                        <td class="order">' + indexUser + '</td>\n' +
                        '                        <td>' + name + '</td>\n' +
                        '                        <td>' + email + '</td>\n' +
                        '                        <td>\n' +
                        '                            <!-- Button Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->\n' +
                        '                            <button type="button" class="button-view-detail delete-object-group text-nowrap" onclick="deleteObjectGroup(\'user\', ' + $(this).val() + ')">\n' +
                        '                                <span class="text-view-detail">削除</span>\n' +
                        '                                <img src="{{ asset("icons/trash.svg") }}" alt="trash">\n' +
                        '                            </button>\n' +
                        '                        </td>\n' +
                        '                    </tr>');
                    if ($('#table-user tr').length > 1) {
                        $(this).closest('tr').remove();
                        $('#table-user td.order').text(function (i) {
                            return i + 1;
                        });

                    }
                }
            });
            $('#modalEditGroupID1 #heading-list-user').text('経理担当者一覧 ( ' + indexUser + ' )');
            $('#modalEditGroupID1 #heading-list-facility').text('事業所一覧 (' + index + ')');
            if (index || indexUser) {
                $('#modalEditGroupID1 #submit-form-store-group').prop('disabled', false);
            }
            $('#modalEditGroupID1').modal({backdrop: 'static', keyboard: false})
            $('#modalEditGroupID1').modal('show');
            $('#modalAddAcountID1').modal('hide');
        })
        $('#modalEditGroupID1 .close.home-modal-close').on('click', function (e) {
            $('#modalEditGroupID1 #heading-list-user').text('経理担当者一覧 ');
            $('#modalEditGroupID1 #heading-list-facility').text('事業所一覧 ');
            $('#modalEditGroupID1 .remove-item').remove();
            $('#modalEditGroupID1 #group-name-error').empty();
            $('#modalEditGroupID1 input').val('');
            $('#modalEditGroupID1 #store-group input[name="id_group"]').remove();
            $('#submit-form-store-group').prop('disabled', true);
        });

        function deleteObjectGroup(type, id) {
            $('#modalDeleteObjectID1').append('<input type="hidden" name="type" value="' + type + '">');
            $('#modalDeleteObjectID1').append('<input type="hidden" name="id" value="' + id + '">');
            $('#modalEditGroupID1').modal('hide');
            $('#modalDeleteObjectID1').modal({backdrop: 'static', keyboard: false})
            $('#modalDeleteObjectID1').modal('show');
        }

        $('#modalDeleteObjectID1 #delete-object-group').click(function () {
            var type = $('#modalDeleteObjectID1 input[name="type"]').val();
            $('#modalDeleteObjectID1 input[name="type"]').remove();
            var id = $('#modalDeleteObjectID1 input[name="id"]').val();
            $('#modalDeleteObjectID1 input[name="id"]').remove();
            $('#modalEditGroupID1 input[value="' + id + '"].' + type).remove();
            $('#modalEditGroupID1 input[value="' + id + '"].' + type + '-added').remove();
            $('#' + type + '-' + id).remove();
            if ($('#modalEditGroupID1 #table-' + type + ' tr').length > 1) {
                $(this).closest('tr').remove();
                $('#modalEditGroupID1 #table-' + type + ' td.order').text(function (i) {
                    return i + 1;
                });
            }
            var index = $('#modalEditGroupID1 #table-' + type + ' tr').length - 1;
            if (type == 'user') {
                $('#modalEditGroupID1 #heading-list-user').text('経理担当者一覧 ( ' + index + ' )');
            } else {
                $('#modalEditGroupID1 #heading-list-facility').text('事業所一覧 (' + index + ')');
            }
            $('#modalEditGroupID1').modal({backdrop: 'static', keyboard: false})
            $('#modalEditGroupID1').modal('show');
            $('#modalDeleteObjectID1').modal('hide');
        })
        $('#modalEditGroupID1 #submit-form-store-group').click(function () {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var formData = new FormData($('#modalEditGroupID1 #store-group')[0]);
            var id = $('#modalEditGroupID1 #store-group input[name="id_group"]').val();
            $('#group-name-error').empty();
            $.ajax({
                url: $('#modalEditGroupID1 #store-group').attr('action'),
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (results) {
                    $('#modalEditGroupID1 .remove-item').remove();
                    $('#modalEditGroupID1 input').val('');
                    $('#submit-form-store-group').prop('disabled', true);
                    $('#modalEditGroupID1').modal('hide');
                    $('#modalEditAlertSuccessID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalEditAlertSuccessID1').modal('show');
                    if (id) {
                        $('#modalEditAlertSuccessID1 #modal-tile').text('グループを編集');
                        $('#modalEditAlertSuccessID1 #text-noti').text('グループ編集を成功しました。');
                    } else {
                        $('#modalEditAlertSuccessID1 #modal-tile').text('新グループ作成');
                        $('#modalEditAlertSuccessID1 #text-noti').text('新グループ作成を成功しました。');
                    }
                    $('#preloader').remove();
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 404) {
                        alert('{{\App\Messages::EMPTY_RECORD}}');
                        location.reload();
                    } else {
                        var error = jqXHR.responseJSON.errors;
                        if (error.group_name) {
                            var groupNameError = '';
                            $.each(error.group_name, function (key, value) {
                                groupNameError += value + '<br>'
                            })
                            $('#group-name-error').html(groupNameError);
                        }
                        $('#preloader').remove();
                    }
                }
            })
        })
        $('#modalEditAlertSuccessID1 #reload-page').click(function () {
            location.reload();
        })
        $('.button-view-edit').click(function (){
            $.ajax({
                url: '{{route('setting-group-user.show')}}',
                type: 'get',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id')
                },
                success: function ($results) {
                    var group = $results.group
                    var users = $results.user
                    var facilities = $results.facilities
                    $('#modalEditGroupID1 #store-group input[name="group_name"]').val(group.group_name);
                    $('#modalEditGroupID1 #store-group').append('<input type="hidden" name="id_group" value="' + group.id + '">');
                    var index = 0;
                    $.each(facilities, function (i, value) {
                        index++;
                        $('#modalEditGroupID1 #store-group').append('<input type=hidden class="facility remove-item" name="facilities[]" value="' + value.id + '">')
                        $('#modalEditGroupID1 #store-group').append('<input type=hidden class="facility-added remove-item" name="facility-added[]" value="' + value.id + '">')
                        var email = value.email ?? '';
                        var name = value.name ?? '';
                        $('#modalEditGroupID1 #table-facility').append('<tr id="facility-' + value.id + '" class="remove-item">\n' +
                            '                        <td class="order">' + index + '</td>\n' +
                            '                        <td>' + name + '</td>\n' +
                            '                        <td>' + email + '</td>\n' +
                            '                        <td>\n' +
                            '                            <!-- Button Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->\n' +
                            '                            <button type="button" class="button-view-detail delete-object-group text-nowrap" onclick="deleteObjectGroup(\'facility\', ' + value.id + ')">\n' +
                            '                                <span class="text-view-detail">削除</span>\n' +
                            '                                <img src="{{ asset("icons/trash.svg") }}" alt="trash">\n' +
                            '                            </button>\n' +
                            '                        </td>\n' +
                            '                    </tr>');
                    });
                    var indexUser =  0;
                    $.each(users, function (i, value) {
                        indexUser++;
                        $('#modalEditGroupID1 #store-group').append('<input type=hidden class="user remove-item" name="users[]" value="' + value.id + '">')
                        $('#modalEditGroupID1 #store-group').append('<input type=hidden class="user-added remove-item" name="user-added[]" value="' + value.id + '">')
                        var email = value.email ?? '';
                        var name = value.name ?? '';
                        $('#modalEditGroupID1 #table-user').append('<tr id="user-' + value.id + '" class="remove-item">\n' +
                            '                        <td class="order">' + indexUser + '</td>\n' +
                            '                        <td>' + name + '</td>\n' +
                            '                        <td>' + email + '</td>\n' +
                            '                        <td>\n' +
                            '                            <!-- Button Modal B3004.7 Xoá Đối Tượng Trong Nhóm -->\n' +
                            '                            <button type="button" class="button-view-detail delete-object-group text-nowrap" onclick="deleteObjectGroup(\'user\', ' + value.id + ')">\n' +
                            '                                <span class="text-view-detail">削除</span>\n' +
                            '                                <img src="{{ asset("icons/trash.svg") }}" alt="trash">\n' +
                            '                            </button>\n' +
                            '                        </td>\n' +
                            '                    </tr>');
                        if ($('#table-user tr').length > 1) {
                            $(this).closest('tr').remove();
                            $('#table-user td.order').text(function (i) {
                                return i + 1;
                            });

                        }
                    });
                    $('#modalEditGroupID1 #heading-list-user').text('経理担当者一覧 ( ' + indexUser + ' )');
                    $('#modalEditGroupID1 #heading-list-facility').text('事業所一覧 (' + index + ')');
                    if (index || indexUser) {
                        $('#modalEditGroupID1 #submit-form-store-group').prop('disabled', false);
                    }

                    $('#preloader').remove();
                    $('#modalEditGroupID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalEditGroupID1').modal('show');
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 404) {
                        alert('{{\App\Messages::EMPTY_RECORD}}');
                        location.reload();
                    } else {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                    }
                    $('#preloader').remove();
                }
            });
        })
    </script>
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
@endsection
