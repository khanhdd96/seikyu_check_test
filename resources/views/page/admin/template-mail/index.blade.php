@extends('layouts.admin')
@section('title', 'メールテンプレート管理')
@section('content')
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
    <style>
        .button-optional:hover {
            background-color: rgba(15, 148, 181, 0.2);
        }

        .number-chartLegend1 {
            white-space: nowrap;
            margin-left: 10px;
        }

        strong, h1, h2, h3, h4, h5, span, div {
            color: #222222;
        }

        .search-button {
            width: 50px !important;
        }

        .w-216 {
            width: 216px;
        }

        .h-70 {
            height: 70%;
        }

        .select2-selection {
            height: 100% !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }

        .select2-container--default .select2-selection--multiple {
            line-height: 26px !important;
            font-size: 16px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
            border-radius: 5px !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__clear {
            display: none;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display: none;
        }

        .select2-results__option--selectable {
            font-size: 16px;
        }

        .table-condensed {
            width: 207px !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background: #b5e5f0 !important;
            border-radius: 5px !important;
            padding: 5px 20px 5px 5px !important;
            border: none !important;
            align-items: center;
            max-width: 90% !important;
        }

        .select2-selection--multiple .select2-selection__choice__remove {
            left: unset !important;
            top: 5px !important;
            right: 0;
        }

        .select2-selection--multiple .select2-selection__choice__remove span {
            display: none;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            top: 10px !important;
            right: 4px !important;
            width: 18px !important;
            height: 18px !important;
            background: url(/icons/delete.svg) center center no-repeat !important;
        }

        .selection-heading-comparison .select2-container {
            width: 100% !important;
        }

        .selection-heading-comparison .search-department {
            width: 100% !important;
        }

        .selection-heading-comparison {
            padding: 0px 8px;
        }

        .pb-10 {
            padding-bottom: 10px;
        }

        .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background: rgba(124, 154, 161, 0.4) !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            border-radius: 45% !important;
            border-right: none !important;
        }

        .select2-container--default .select2-search--inline .select2-search__field {
            margin-top: 13px !important;
            margin-left: 15px !important;
            height: 24px !important;
            font-size: 16px;
            margin-bottom: 5px;
        }

        .selection-heading-error {
            width: 216px;
            min-height: 50px;
            position: relative;
            border: none;
            box-sizing: border-box;
            border-radius: 5px;
            margin-left: 15px;
            font-size: 16px;
        }

        .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
            width: 100%;
            margin-top: 13px;
            margin-left: 15px;
        }

        .select2-container .select2-selection--multiple {
            min-height: 50px !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            padding: 12px 22px !important;
        }

        button:disabled,
        button[disabled] {
            background-color: #cccccc;
            color: #666666;
        }

        .table thead th {
            vertical-align: middle !important;
        }

        .button-view-detail {
            white-space: nowrap;
        }

        button.close-modal-day-off {
            background: #B5E5F0;
            border-radius: 5px;
        }

        button.submit-modal-day-off {
            background: linear-gradient(96.42deg, #2BC0E4 19.29%, #0F94B5 104.66%);
            border-radius: 5px;
        }

        .select2-container--default .select2-selection--single {
            border: 1px solid #ced4da !important;
        }

        .pr-40 {
            padding-right: 40px;
        }

        .pt-30 {
            padding-top: 30px;
        }


        html,
        body {
            position: relative;
            height: 100%;
        }

        .swiper {
            width: 85%;
            height: auto;
            margin: 0 auto;
            position: initial;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .button-optional {
            font-weight: 400;
            font-size: 12px;
            line-height: 17px;
            color: #0F94B5;
            padding: 5px 10px;
            border: 1px solid #0F94B5;
            border-radius: 30px;
            background: #fff;
            width: 100%;
        }

        .button-optional:focus, .button-optional:focus-visible, .button-optional:active {
            box-shadow: none;
            border: 1px solid #0F94B5;
            outline: none;
            background: #fff;
        }

        .slider-button svg {
            width: 120px;
        }

        .swiper-button-prev.slider-button {
            left: 0;
        }

        .swiper-button-next.slider-button {
            right: 0;
        }

        .wrapper-slider {
            width: 100%;
            display: flex;
            justify-content: center;
            text-align: center;
            overflow: hidden;
            margin-top: 15px;
            position: relative;
        }

        .list-tag {
            display: flex;
            max-width: 654px;
            overflow-x: auto;
            padding-bottom: 10px;
        }

        .list-tag .swiper-slide:not(:last-child) {
            margin-right: 10px;
        }

        .list-tag::-webkit-scrollbar {
            height: 7px;
        }

        /* Track */
        .list-tag::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        .list-tag::-webkit-scrollbar-thumb {
            background: #B8B8B8;
        }

        /* Handle on hover */
        .list-tag::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
    </style>
    <div class="main-content main-admin">
        <div class="main-heading mb-30">
            <h1 class="title-heading">メールテンプレート管理</h1>
        </div>
        <div class="main-body">
            <div class="wrapper-table">
                <form class="top-table" method="get" autocomplete="off">
                    <div class="wrapper-search ml-3">
                        <input class="input-search" maxlength="100" type="text" placeholder="テンプレート名" name="key_word"
                               value="{{request('key_word')}}">
                        <button class="button-search" type="button" style="cursor: unset">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                      d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
                                      fill="#7C9AA1"></path>
                                <path
                                    d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z"
                                    fill="#7C9AA1"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="search-button" style="width:50px; margin-left:15px">
                        <button class="button-search" type="submit">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5"
                                      stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <button onclick="clearModal()" type="button" data-toggle="modal" data-target="#create-day-off"
                            data-backdrop="static" data-keyboard="false"
                            class="button-add-branch default-button">テンプレートを作成
                    </button>
                </form>
                <script>
                    function clearModal() {
                        $('input[name="title"]').val('');
                        CKEDITOR.instances.content.setData('');
                        $('.title-validate').html('');
                        $('.content-validate').html('');
                    }

                    function loadDataTemplate(id) {
                        $('.title-edit-validate').html('');
                        $('.content-edit-validate').html('');
                        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                        $('#template_id').val(id);
                        $.ajax({
                            url: '{{route('template-mail.detail')}}',
                            type: 'GET',
                            data: {
                                id: id,
                            },
                            headers: {
                                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                            },
                            dataType: "json",
                        }).done(function (data) {
                            var respon = data.data;
                            $('#detail_template_id').val(id);
                            $("#preloader").remove();
                            $('input[name="title_edit"]').val(respon.title);
                            CKEDITOR.instances.content_edit.setData(respon.content);
                        })
                    }
                </script>

                <table class="table table-striped table-admin mt-15 mb-20">
                    <thead>
                    <tr>
                        <th width="80" scope="col">順番</th>
                        <th scope="col">テンプレート名</th>
                        <th scope="col">作成日</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($results['data'] as $key => $data)
                        <tr>
                            @php
                                $page = request('page') ?? \App\Consts::BASE_PAGE;
                            @endphp
                            <td>{{$key + 1 + ($page - 1 )* \App\Consts::BASE_ADMIN_PAGE_SIZE}}</td>
                            <td>{{ $data->title }}</td>
                            <td style="white-space: nowrap">{{date('Y/m/d', strtotime($data->created_at))}}</td>
                            <td>
                                <button data-toggle="modal" data-backdrop="static" data-keyboard="false"
                                        data-target="#detail-template" onclick="showDetail('{{$data->id}}')"
                                        type="button" class="button-view-detail view-detail-setting">
                                    <span class="text-view-detail">詳細</span>
                                    <img src="{{ asset('icons/view.svg') }}" alt="view">
                                </button>
                                <!-- Button Redirect to page 3003.1 -->
                                <button onclick="loadDataTemplate('{{$data->id}}')" data-toggle="modal"
                                        data-target="#edit-template" data-backdrop="static" data-keyboard="false"
                                        type="button" class="button-edit-{{$data->id}} button-view-detail">
                                    <span class="text-view-detail">編集</span>
                                    <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                                </button>
                                <!-- Button Modal xoá alert B3004 -->
                                <button type="button" onclick="$('#delete-template-id').val('{{$data->id}}')"
                                        class="button-delete-{{$data->id}}  button-view-detail delete-setting-mail"
                                        data-toggle="modal"
                                        data-target="#modalDelete" data-backdrop="static" data-keyboard="false">
                                    <span class="text-view-detail">削除</span>
                                    <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr class="text-center">
                            <td colspan="4" style="display: revert;">{{\App\Messages::EMPTY_RECORD}}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <!-- add pagination if more 10 rows -->
                <nav class="wrapper-pagination">
                    {{ $results['data']->appends(request()->except('page'), request()->except('date'), request()->except('key_word'))->links('partial.admin.paginate') }}
                </nav>
            </div>
        </div>
    </div>

    <!-- Modal D1000.2 -->
    <div class="modal fade" id="modalCreateSuccess" tabindex="-1" role="dialog" aria-labelledby="modalCreateSuccess"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">新規テンプレートメールを追加</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 新規テンプレートメール追加が完了しました。</p>
                        <button type="button" onclick="location.reload()" class="home-modal-button default-button">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal D1000.1 -->
    <div class="modal fade home-modal-content" id="create-day-off" role="dialog"
         aria-labelledby="modalDetailAlertID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" style="max-width: 991px" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">新規テンプレートを追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form id="form-create" action="{{route('template-mail.store')}}" method="POST">
                            <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                                <div class="container">
                                    <div class="row justify-content-md-center">
                                        <div class="justify-content-center w-654 ml-0">
                                            <label class="label-form" for="business-selection">件名</label>
                                            <div id="datepicker" class="datepicker-heading w-100 ml-0">
                                                <input type="text" maxlength="100"
                                                       class="form-control input-datepicker datepicker datepicker-create form-control-add pr-40"
                                                       name="title"
                                                       placeholder="【#CURRENT_MONTH#月実施請求締め】#CURRENT_DATE#">
                                                </span>
                                            </div>
                                            <div class="text-danger mt-1 title-validate"></div>
                                            <div class="list-tag mt-20">
                                                @foreach(\App\Consts::LIST_PARAM_SUBJECT as $key => $value)
                                                    <div title="{{$key}}" style="cursor: pointer; width: auto"
                                                         onclick="loadTitle('{{$key}}')"
                                                         class="swiper-slide">
                                                        <div class="button-optional w-auto">{{$value}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="justify-content-center h-auto w-654 ml-0 pt-30">
                                            <label class="label-form" for="business-selection">内容を入力してください</label>
                                            <textarea data-maxlen="255" class="form-control" placeholder="内容を入力してください"
                                                      name="content"></textarea>
                                            <div class="text-danger mt-1 content-validate"></div>
                                            <div class="list-tag mt-20">
                                                @foreach(\App\Consts::LIST_PARAM_TEMPLATE as $key => $value)
                                                    <div title="{{$key}}" style="cursor: pointer; width: auto"
                                                         onclick="loadContent('{{$key}}')"
                                                         class="swiper-slide">
                                                        <div class="button-optional w-auto">{{$value}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center mt-50">
                                        <button type="button" class="btn close-modal-day-off second-button mr-30"
                                                data-dismiss="modal" style="width:312px">キャンセル
                                        </button>
                                        <button type="submit" class="btn default-button" style="width:312px">保存</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal D1000.2 -->
    <div class="modal fade" id="modalConfirmCreate" tabindex="-1" role="dialog" aria-labelledby="modalConfirmCreate"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">新規テンプレートメールを追加</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> 新規テンプレートメール追加が完了しました。</p>
                        <button type="submit" class="home-modal-button default-button reload-list-day-off">閉じる</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.2 -->

    <div class="modal fade edit-modal-content" id="edit-template" role="dialog"
         aria-labelledby="modalEditTemplateMail" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" style="max-width: 991px" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">テンプレートメールを編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form id="form-edit" action="{{route('template-mail.edit')}}" method="POST">
                            <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                                <div class="container">
                                    <input type="hidden" id="template_id">
                                    <div class="row justify-content-md-center">
                                        <div class="justify-content-center w-654 ml-0">
                                            <label class="label-form" for="business-selection">件名</label>
                                            <div class="datepicker-heading w-100 ml-0">
                                                <input type="text" maxlength="100"
                                                       class="form-control input-datepicker datepicker datepicker-create form-control-add pr-40"
                                                       name="title_edit"
                                                       placeholder="【#CURRENT_MONTH#月実施請求締め】#CURRENT_DATE#">
                                                </span>
                                            </div>
                                            <div class="text-danger mt-1 title-edit-validate"></div>
                                            <div class="list-tag mt-20">
                                                @foreach(\App\Consts::LIST_PARAM_SUBJECT as $key => $value)
                                                    <div title="{{$key}}" style="cursor: pointer; width: auto"
                                                         onclick="loadTitleEdit('{{$key}}')"
                                                         class="swiper-slide">
                                                        <div class="button-optional w-auto">{{$value}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="justify-content-center h-auto w-654 ml-0 pt-30">
                                            <label class="label-form" for="business-selection">内容を入力してください</label>
                                            <textarea class="form-control" placeholder="内容を入力してください"
                                                      name="content_edit"></textarea>
                                            <div class="text-danger mt-1 content-edit-validate"></div>
                                            <div class="list-tag mt-20">
                                                @foreach(\App\Consts::LIST_PARAM_TEMPLATE as $key => $value)
                                                    <div title="{{$key}}" style="cursor: pointer; width: auto"
                                                         onclick="loadContentEdit('{{$key}}')"
                                                         class="swiper-slide">
                                                        <div class="button-optional w-auto">{{$value}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center mt-50">
                                        <button type="button" class="btn close-modal-day-off second-button mr-30"
                                                data-dismiss="modal" style="width:312px">キャンセル
                                        </button>
                                        <button type="submit" class="btn default-button" style="width:312px">保存</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUpdateSuccess" tabindex="-1" role="dialog" aria-labelledby="modalUpdateSuccess"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title">テンプレートメールを編集</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text">テンプレートメール変更が完了しました。</p>
                        <button type="submit" onclick="location.reload()" class="home-modal-button default-button">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade detail-modal-content" id="detail-template" role="dialog"
         aria-labelledby="modalEditTemplateMail" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered alert-modal-dialog" style="max-width: 880px" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">テンプレートメールを編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div style="padding-top: 63px;" class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <input type="hidden" id="detail-template-id">
                            <div class="d-flex align-items-center justify-content-center modal-view-day-off">
                                <div class="container">
                                    <div class="row justify-content-md-center">
                                        <div class="justify-content-center w-654 ml-0 mb-45">

                                            <div style="text-align: right; padding-left: 135px">
                                                <!-- Button Redirect to page 3003.1 -->
                                                <button onclick="clickButtonEdit()" style="display: inline-block" type="button" class="button-view-detail">
                                                    <span class="text-view-detail">編集</span>
                                                    <img src="{{ asset('icons/edit.svg') }}" alt="edit">
                                                </button>
                                                <script>
                                                    function clickButtonEdit() {
                                                        $('#detail-template').modal('hide');
                                                        setTimeout(function () {
                                                            $('#edit-template').modal('show');
                                                            $('#edit-template').modal({backdrop: 'static', keyboard: false});
                                                        }, 350);
                                                        loadDataTemplate($('#detail-template-id').val());
                                                    }

                                                    function clickButtonDelete() {
                                                        $('#detail-template').modal('hide');
                                                        setTimeout(function () {
                                                            $('#modalDelete').modal('show');
                                                            $('#modalDelete').modal({backdrop: 'static', keyboard: false});
                                                        }, 350);
                                                        $('#delete-template-id').val($('#detail-template-id').val());
                                                    }
                                                </script>
                                                <!-- Button Modal xoá alert B3004 -->
                                                <button onclick="clickButtonDelete()" style="display: inline-block"
                                                        type="button" class="button-view-detail">
                                                    <span class="text-view-detail">削除</span>
                                                    <img src="{{ asset('icons/trash.svg') }}" alt="trash">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="justify-content-center w-654 ml-0">
                                            <strong
                                                style="font-size: 16px; padding-bottom: 20px;display: inline-block;">件名</strong>
                                            <p id="title-detail" class="w-100 ml-0"></p>
                                        </div>
                                        <div class="justify-content-center h-auto w-654 ml-0 pt-30">
                                            <label style="margin-bottom: 20px;" class="label-form"
                                                   for="business-selection">内容を入力してください</label>
                                            <p id="content-detail"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">メールテンプレートを削除</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <input type="hidden" id="delete-template-id">
                        <img class="home-modal-icon" src="{{ asset('icons/question-mark.svg') }}" alt="question-mark">
                        <p class="modal-text">該当テンプレートを削除しますか。</p>
                        <div class="d-flex align-items-center justify-content-center mt-50">
                            <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="Close">
                                キャンセル
                            </button>
                            <button type="submit" class="w-205 default-button" onclick="deleteTemplate()">
                                削除
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal D1000.5 -->
    <!-- Modal D1000.6 -->
    <div class="modal fade" id="modalDeleteSuccess" tabindex="-1" role="dialog"
         aria-labelledby="modalDeleteAlertSuccessTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">テンプレートメールを削除</h5>
                    <button onclick="location.reload()" type="button" class="close home-modal-close"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <div class="delete-office">
                        <img class="home-modal-icon" src="{{ asset('icons/success.svg') }}" alt="question-mark">
                        <p class="modal-text"> テンプレートメール削除が完了しました。</p>
                        <button onclick="location.reload()" type="submit"
                                class="home-modal-button default-button reload-list-day-off">閉じる
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script src="{{asset('js/plugin.js')}}"></script>
    <script>
        CKEDITOR.replace('content', {
            language: 'ja',
            extraPlugins: 'editorplaceholder',
            editorplaceholder: '内容を入力してください',
        });
        CKEDITOR.replace('content_edit', {
            language: 'ja',
            extraPlugins: 'editorplaceholder',
            editorplaceholder: '内容を入力してください',
        });

        function showDetail(id) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: '{{route('template-mail.detail')}}',
                type: 'GET',
                data: {
                    id: id,
                },
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                dataType: "json",
            }).done(function (data) {
                var respon = data.data;
                $('#title-detail').html(htmlEntities(respon.title));
                $('#detail-template-id').val(id);
                $('#content-detail').html(respon.content);
                $("#preloader").remove();
            })
        }

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");
            ;
        }

        function loadTitle(text) {
            var valueInput = $('input[name="title"]').val() + text;
            $('input[name="title"]').val(valueInput);
        }

        function loadTitleEdit(text) {
            var valueInput = $('input[name="title_edit"]').val() + text;
            $('input[name="title_edit"]').val(valueInput);
        }

        function loadContent(text) {
            CKEDITOR.instances.content.insertText(text);
            // var content = CKEDITOR.instances.content.getData() + text;
            // CKEDITOR.instances.content.setData(content);
        }

        function loadContentEdit(text) {
            // var content = CKEDITOR.instances.content_edit.getData() + text;
            // CKEDITOR.instances.content_edit.setData(content);
            CKEDITOR.instances.content_edit.insertText(text);
        }

        $('#form-create').submit(function (e) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            e.preventDefault();
            var formData = new FormData();
            formData.append('content', CKEDITOR.instances.content.getData());
            formData.append('title', $('input[name="title"]').val());
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#create-day-off').modal('hide');
                    $('#modalCreateSuccess').modal({backdrop: 'static', keyboard: false});
                    $("#preloader").remove();
                },
                error: function (e) {
                    if (e.status == 400) {
                        var err = JSON.parse(e.responseText);
                        $.each(err.message, function (key, arrMessage) {
                            var textError = '';
                            arrMessage.forEach(function (value, key) {
                                textError += value + '<br/>'
                            });
                            $('.' + key + '-validate').html(textError);
                        });
                    } else {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                    }
                    $("#preloader").remove();
                }
            });
        })
        $('#form-edit').submit(function (e) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            e.preventDefault();
            var formData = new FormData();
            formData.append('content', CKEDITOR.instances.content_edit.getData());
            formData.append('title', $('input[name="title_edit"]').val());
            formData.append('id', $('#template_id').val());
            formData.append('_method', 'PUT');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                contentType: false,
                processData: false,
                success: function () {
                    $('#edit-template').modal('hide');
                    $('#modalUpdateSuccess').modal({backdrop: 'static', keyboard: false});
                    $("#preloader").remove();
                },
                error: function (e) {
                    if (e.status == 400) {
                        var err = JSON.parse(e.responseText);
                        $.each(err.message, function (key, arrMessage) {
                            var textError = '';
                            arrMessage.forEach(function (value, key) {
                                textError += value + '<br/>'
                            });
                            $('.' + key + '-edit-validate').html(textError);
                        });
                    } else {
                        alert('{{\App\Messages::SYSTERM_ERROR}}');
                    }
                    $("#preloader").remove();
                }
            });
        })

        function deleteTemplate() {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            $.ajax({
                url: '{{route("template-mail.delete")}}',
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: {id: $('#delete-template-id').val()},
                success: function () {
                    $('#modalDelete').modal('hide');
                    $('#modalDeleteSuccess').modal({backdrop: 'static', keyboard: false});
                    $("#preloader").remove();
                },
                error: function (e) {
                    alert('{{\App\Messages::SYSTERM_ERROR}}');
                    $("#preloader").remove();
                }
            });
        }
    </script>
@endsection
