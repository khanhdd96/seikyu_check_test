@extends('layouts.admin')
@section('title', 'リマインド（自動）アラート')
@section('content')
    <style>
        .button-mail {
            background: #F5F7FB;
            width: 163px;
            height: 55px;
            border: 1px dashed #0F94B5;
            border-radius: 5px;
        }
        .button-mail:focus {
            background: #F5F7FB;
            width: 163px;
            height: 55px;
            border: 1px dashed #0F94B5;
            border-radius: 5px;
        }

        /*style radio button modal*/
        .form-radio-check [type="radio"]:checked,
        .form-radio-check [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
        .form-radio-check [type="radio"]:checked + label,
        .form-radio-check [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
        .form-radio-check [type="radio"]:checked + label:before,
        .form-radio-check [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 20px;
            height: 20px;
            border: 1px solid #2bc0e4;
            border-radius: 100%;
            background: #fff;
        }
        .form-radio-check [type="radio"]:checked + label:after,
        .form-radio-check [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 20px;
            height: 20px;
            background: #2bc0e4;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        .form-radio-check [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        .form-radio-check [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }
        input.form-control {
            padding-top: 10px !important;
            padding-bottom: 10px !important;
            height: 100%;
        }
        .form-check-label {
            color: #222222 !important;
        }
        label.form-check-label {
            font-size: 16px;
        }
        /*custom scroll*/
        .body-template::-webkit-scrollbar {
            width: 4px;
        }
        .body-template::-webkit-scrollbar-track {
            background: #f1f1f1;
        }
        .body-template::-webkit-scrollbar-thumb {
            background: #B8B8B8;
        }
        .body-template::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
        /*end custom scroll*/
        .button-mail{
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <form action="{{route('setting-alert-auto.update', ['id'=>$data['mailSetting']->id])}}" id="setting-alert-form"
          class="main-content main-admin" method="post" autocomplete="off">
        @csrf
        @method('put')
        <ol class="breadcrumb main-breadcrumb">
            <li class="breadcrumb-item main-breadcrumb-item"><a href="{{route('setting-alert-auto.index')}}">リマインド（自動）アラート</a>
            </li>
            <li class="breadcrumb-item main-breadcrumb-item active" aria-current="page">アラート編集</li>
        </ol>
        <div class="main-heading mb-30">
            <h1 class="title-heading">リマインド（自動）アラート</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-setting-alert">
                <p class="text-setting-alert mb-50">
                    デフォルト設定：初期設定として毎月配信日時：月初の営業日朝12時、15時、18時「配信期間：<br/>
                    （送信日の締切日設定されている事業所）毎日配信」となります
                </p>
                <div class="d-flex  flex-wrap mb-30">
                    <div class="mint-setting-alert">日時設定</div>
                </div>
                <div class="row mb-30">
                    <div class="form-group col-4 mb-0">
                        <label class="label-form" for="form-1">日選択</label>
                        <div id="datepicker" class="datepicker-heading ml-0 w-100">
                            <input id="form-datepicker-1" class="input-datepicker" type="text"
                                   value="{{old('start_date') ?? date('Y/m/d', strtotime($data['mailSetting']->start_date))}}"
                                   name="start_date" placeholder="年/月/日">
                            <span class="icon-datepicker" id="form-icon-datepicker-1">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                        fill="#7C9AA1"/>
                                    <path opacity="0.4"
                                          d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                          fill="#7C9AA1"/>
                                    <path
                                        d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                        fill="#7C9AA1"/>
                                    <path
                                        d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                        fill="#7C9AA1"/>
                                    <path
                                        d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                        fill="#7C9AA1"/>
                                    <path
                                        d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                        fill="#7C9AA1"/>
                                    <path
                                        d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                        fill="#7C9AA1"/>
                                    <path
                                        d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                        fill="#7C9AA1"/>
                                </svg>
                            </span>
                        </div>
                        @error('start_date')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                    <?php $time = date('H:i', strtotime($data['mailSetting']->time));
                    if (old('time')) {
                        $time = old('time');
                    }
                    ?>
{{--                    <div class="form-group col-8 mb-0">--}}
{{--                        <label class="label-form" for="form-2">時選択</label>--}}
{{--                        <div class="datepicker-heading ml-0 w-100">--}}
{{--                            <div class="d-inline-block align-items-center flex-wrap">--}}
{{--                                <div class="container-checkbox mt-10">--}}
{{--                                    <input disabled {{$time == '12:00' ? 'checked' : ''}} id="12h" value="12:00"--}}
{{--                                           type="checkbox" class="time" name="time">--}}
{{--                                    <label for="12h">12:00</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="d-inline-block align-items-center flex-wrap ml-31">--}}
{{--                                <div class="container-checkbox mt-10">--}}
{{--                                    <input disabled {{$time == '15:00' ? 'checked' : ''}} id="15h" value="15:00"--}}
{{--                                           type="checkbox" class="time" name="time">--}}
{{--                                    <label for="15h">15:00</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="d-inline-block align-items-center flex-wrap ml-31">--}}
{{--                                <div class="container-checkbox mt-10">--}}
{{--                                    <input disabled {{$time == '18:00' ? 'checked' : ''}} id="18h" value="18:00"--}}
{{--                                           type="checkbox" class="time" name="time">--}}
{{--                                    <label for="18h">18:00</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        @error('time')--}}
{{--                        <div class="text-danger mt-2">{{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
                </div>
                <div class="row mb-30">
                    <div class="form-group  col-4 mb-0">
                        <label class="label-form" for="frequency">頻度</label>
                        <div class="selection-heading w-100 ml-0">
                            <select id="frequency" class="form-control dropdown-heading w-100" name="frequency_type">
                                <option value="" disabled selected>頻度を選択してください</option>
                                @foreach(\App\Consts::MAILING_FREQUENCY as $key => $frequency)
                                    <option value="{{$key}}"
                                            @if($key == old('frequency_type') || $key == $data['mailSetting']->frequency_type)selected @endif>{{$frequency}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('frequency_type')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                    <div
                        class="form-group  mb-0 col-4 @if(old('frequency_type') == \App\Consts::MAILING_FREQUENCY_DAILI || $data['mailSetting']->frequency_type == \App\Consts::MAILING_FREQUENCY_DAILI) @error('interval') '' @else d-none @enderror @endif"
                        id="numer-date-diff">
                        <label class="label-form" for="number-diff-times-1">送信間隔 </label>
                        <?php $value = '';?>
                        @if(old('interval')) <?php $value = old('interval')?> @else @error('interval') <?php $value = old('interval')?> @else <?php $value = $data['mailSetting']->interval?>@enderror @endif
                        <input type="number" class="form-control-add number-class" value="{{$value}}"
                               name="interval" @if(old('frequency_type') == \App\Consts::MAILING_FREQUENCY_DAILI || $data['mailSetting']->frequency_type == \App\Consts::MAILING_FREQUENCY_DAILI) @error('interval')
                        '' @else disabled @enderror @endif id="number-diff-times-1" placeholder="送信間隔">
                        @error('interval')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                    <div
                        class="form-group col-4 {{old('frequency_type') == \App\Consts::MAILING_FREQUENCY_DAILI || $data['mailSetting']->frequency_type == \App\Consts::MAILING_FREQUENCY_DAILI ? 'w-430' : 'w-300'}} mb-0"
                        id="number-of-times">
                        <label class="label-form" for="number-of-times-1">回数</label>
                        <input disabled type="number" class="form-control-add number-class" name="send_times"
                               value="{{old('send_times') ?? $data['mailSetting']->send_times}}" id="number-of-times-1"
                               placeholder="回数">
                        @error('send_times')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex  flex-wrap mb-30">
                    <div class="mint-setting-alert">受信対象</div>
                </div>
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="container-checkbox">
                        <input type="checkbox" id="except-for-some"
                               name="except"{{old('users') || old('groups') || $data['mailSetting']->user_ids || $data['mailSetting']->grourp_ids ? 'checked' : ''}}>
                        <label for="except-for-some">一部を除く対象</label>
                    </div>
                </div>

                <!-- if checked checkbox remove class="form-group-disabled" and property "disabled" -->
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <!-- add class="form-group-disabled" if disable form -->
                    <div
                        class="form-group {{$data['mailSetting']->user_ids || old('users') ? '' : 'form-group-disabled' }} form-group-select w-100 mb-0">
                        <label class="label-form" for="user-list">事業所選択</label>
                        <select onchange="disableButton()" id="user-list-1" data-placeholder="ユーザーを選択してください。" multiple
                                class="chosen-select form-control-add"
                                {{$data['mailSetting']->user_ids || old('users') ? '' : 'disabled' }} name="users[]">
                            @foreach($data['users'] as $user)
                                <option value="{{$user->id}}"
                                @if(old('users'))
                                    @foreach(old('users') as $i => $field)
                                        {{ ($field == $user->id ? "selected":"") }}
                                        @endforeach
                                    @else
                                    @if($data['mailSetting']->user_ids)
                                        @foreach($data['mailSetting']->user_ids as $i => $field)
                                            {{ ($field == $user->id ? "selected":"") }}
                                            @endforeach
                                        @endif
                                    @endif
                                >{{$user->name}}</option>
                            @endforeach
                        </select>
                        @error('users')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex align-items-center flex-wrap mb-30">
                    <!-- add class="form-group-disabled" if disable form -->
                    <div
                        class="form-group w-100 {{$data['mailSetting']->grourp_ids || old('groups') ? '' : 'form-group-disabled' }} mb-0 form-group-select">
                        <label class="label-form" for="group-list">グループ一覧</label>
                        <select onchange="disableButton()" id="group-list-1" data-placeholder="グループを選択してください。" multiple
                                class="chosen-select form-control-add"
                                name="groups[]" {{$data['mailSetting']->grourp_ids || old('groups') ? '' : 'disabled' }}>
                            @foreach($data['groups'] as $group)
                                <option value="{{$group->id}}"
                                @if(old('groups'))
                                    @foreach(old('groups') as $i => $field)
                                        {{ ($field == $group->id ? "selected":"") }}
                                        @endforeach
                                    @else
                                    @if($data['mailSetting']->grourp_ids)
                                        @foreach($data['mailSetting']->grourp_ids as $i => $field)
                                            {{ ($field == $group->id ? "selected":"") }}
                                            @endforeach
                                        @endif
                                    @endif>{{$group->group_name}}</option>
                            @endforeach
                        </select>
                        @error('groups')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex  flex-wrap mb-30">
                    <div class="mint-setting-alert">外部メールを送信する</div>
                </div>

                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="form-group w-100 mb-0">
                        <label class="label-form" for="number-of-times">メール</label>
                        <div id="list_mail">
                            @forelse(old('list_mail') ?? $data['mailSetting']->list_mail ?? [] as $key => $value)
                                <div class="detail-mail @if($key == 0)mt-15 @else mt-30 @endif">
                                    <input style="width:573px; height: 55px" placeholder="外部メールを入力してください" class="form-control-add" name="list_mail[]" value="{{$value}}"/>
                                    <svg style="margin-left: 20px; cursor: pointer" onclick="deleteMail(this)" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M21.0736 5.23C19.4636 5.07 17.8536 4.95 16.2336 4.86V4.85L16.0136 3.55C15.8636 2.63 15.6436 1.25 13.3036 1.25H10.6836C8.35358 1.25 8.13357 2.57 7.97358 3.54L7.76358 4.82C6.83358 4.88 5.90358 4.94 4.97358 5.03L2.93358 5.23C2.51358 5.27 2.21358 5.64 2.25358 6.05C2.29358 6.46 2.65358 6.76 3.07358 6.72L5.11358 6.52C10.3536 6 15.6336 6.2 20.9336 6.73C20.9636 6.73 20.9836 6.73 21.0136 6.73C21.3936 6.73 21.7236 6.44 21.7636 6.05C21.7936 5.64 21.4936 5.27 21.0736 5.23Z" fill="#E42B2B"/>
                                        <path opacity="0.3991" d="M19.2317 8.14C18.9917 7.89 18.6617 7.75 18.3217 7.75H5.6817C5.3417 7.75 5.0017 7.89 4.7717 8.14C4.5417 8.39 4.4117 8.73 4.4317 9.08L5.0517 19.34C5.1617 20.86 5.3017 22.76 8.7917 22.76H15.2117C18.7017 22.76 18.8417 20.87 18.9517 19.34L19.5717 9.09C19.5917 8.73 19.4617 8.39 19.2317 8.14Z" fill="#E42B2B"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.57812 17C9.57812 16.5858 9.91391 16.25 10.3281 16.25H13.6581C14.0723 16.25 14.4081 16.5858 14.4081 17C14.4081 17.4142 14.0723 17.75 13.6581 17.75H10.3281C9.91391 17.75 9.57812 17.4142 9.57812 17Z" fill="#E42B2B"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.75 13C8.75 12.5858 9.08579 12.25 9.5 12.25H14.5C14.9142 12.25 15.25 12.5858 15.25 13C15.25 13.4142 14.9142 13.75 14.5 13.75H9.5C9.08579 13.75 8.75 13.4142 8.75 13Z" fill="#E42B2B"/>
                                    </svg>
                                </div>
                            @empty
                                <div class="detail-mail mt-15">
                                    <input style="width:573px; height: 55px" placeholder="外部メールを入力してください" class="form-control-add" name="list_mail[]" value=""/>
                                    <svg class="d-none" style="margin-left: 20px; cursor: pointer" onclick="deleteMail(this)" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M21.0736 5.23C19.4636 5.07 17.8536 4.95 16.2336 4.86V4.85L16.0136 3.55C15.8636 2.63 15.6436 1.25 13.3036 1.25H10.6836C8.35358 1.25 8.13357 2.57 7.97358 3.54L7.76358 4.82C6.83358 4.88 5.90358 4.94 4.97358 5.03L2.93358 5.23C2.51358 5.27 2.21358 5.64 2.25358 6.05C2.29358 6.46 2.65358 6.76 3.07358 6.72L5.11358 6.52C10.3536 6 15.6336 6.2 20.9336 6.73C20.9636 6.73 20.9836 6.73 21.0136 6.73C21.3936 6.73 21.7236 6.44 21.7636 6.05C21.7936 5.64 21.4936 5.27 21.0736 5.23Z" fill="#E42B2B"/>
                                        <path opacity="0.3991" d="M19.2317 8.14C18.9917 7.89 18.6617 7.75 18.3217 7.75H5.6817C5.3417 7.75 5.0017 7.89 4.7717 8.14C4.5417 8.39 4.4117 8.73 4.4317 9.08L5.0517 19.34C5.1617 20.86 5.3017 22.76 8.7917 22.76H15.2117C18.7017 22.76 18.8417 20.87 18.9517 19.34L19.5717 9.09C19.5917 8.73 19.4617 8.39 19.2317 8.14Z" fill="#E42B2B"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.57812 17C9.57812 16.5858 9.91391 16.25 10.3281 16.25H13.6581C14.0723 16.25 14.4081 16.5858 14.4081 17C14.4081 17.4142 14.0723 17.75 13.6581 17.75H10.3281C9.91391 17.75 9.57812 17.4142 9.57812 17Z" fill="#E42B2B"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.75 13C8.75 12.5858 9.08579 12.25 9.5 12.25H14.5C14.9142 12.25 15.25 12.5858 15.25 13C15.25 13.4142 14.9142 13.75 14.5 13.75H9.5C9.08579 13.75 8.75 13.4142 8.75 13Z" fill="#E42B2B"/>
                                    </svg>
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-center mt-30">
                        <button onclick="addListMail()" class="button-mail" type="button">
                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.4"
                                      d="M12.6172 22.9219C18.14 22.9219 22.6172 18.4447 22.6172 12.9219C22.6172 7.39903 18.14 2.92188 12.6172 2.92188C7.09434 2.92188 2.61719 7.39903 2.61719 12.9219C2.61719 18.4447 7.09434 22.9219 12.6172 22.9219Z"
                                      fill="#0F94B5"/>
                                <path
                                    d="M16.6172 12.1719H13.3672V8.92188C13.3672 8.51188 13.0272 8.17188 12.6172 8.17188C12.2072 8.17188 11.8672 8.51188 11.8672 8.92188V12.1719H8.61719C8.20719 12.1719 7.86719 12.5119 7.86719 12.9219C7.86719 13.3319 8.20719 13.6719 8.61719 13.6719H11.8672V16.9219C11.8672 17.3319 12.2072 17.6719 12.6172 17.6719C13.0272 17.6719 13.3672 17.3319 13.3672 16.9219V13.6719H16.6172C17.0272 13.6719 17.3672 13.3319 17.3672 12.9219C17.3672 12.5119 17.0272 12.1719 16.6172 12.1719Z"
                                    fill="#0F94B5"/>
                            </svg>
                            <span style="font-weight: 400; font-style: normal;
font-size: 16px;
line-height: 23px;
text-align: center;
letter-spacing: -0.02em;
padding-left: 10px">メールを追加</span>
                        </button>
                    </div>
                </div>
                <script>
                    function addListMail() {
                        var count = $('.detail-mail').length;
                        var margin = 'mt-15';
                        var display = 'd-none';
                        if (count > 0) {
                            margin = 'mt-30';
                            display = '';
                        }
                        $('.detail-mail').find('svg').removeClass('d-none');
                        $('#list_mail').append('<div class="detail-mail ' + margin + '"><input placeholder="外部メールを入力してください" style="width:573px; height: 55px" class="form-control-add" name="list_mail[]" value=""/>' +
                            '<svg class="' + display + '" style="margin-left: 20px; cursor: pointer" onclick="deleteMail(this)" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                            '<path d="M21.0736 5.23C19.4636 5.07 17.8536 4.95 16.2336 4.86V4.85L16.0136 3.55C15.8636 2.63 15.6436 1.25 13.3036 1.25H10.6836C8.35358 1.25 8.13357 2.57 7.97358 3.54L7.76358 4.82C6.83358 4.88 5.90358 4.94 4.97358 5.03L2.93358 5.23C2.51358 5.27 2.21358 5.64 2.25358 6.05C2.29358 6.46 2.65358 6.76 3.07358 6.72L5.11358 6.52C10.3536 6 15.6336 6.2 20.9336 6.73C20.9636 6.73 20.9836 6.73 21.0136 6.73C21.3936 6.73 21.7236 6.44 21.7636 6.05C21.7936 5.64 21.4936 5.27 21.0736 5.23Z" fill="#E42B2B"/>' +
                            '<path opacity="0.3991" d="M19.2317 8.14C18.9917 7.89 18.6617 7.75 18.3217 7.75H5.6817C5.3417 7.75 5.0017 7.89 4.7717 8.14C4.5417 8.39 4.4117 8.73 4.4317 9.08L5.0517 19.34C5.1617 20.86 5.3017 22.76 8.7917 22.76H15.2117C18.7017 22.76 18.8417 20.87 18.9517 19.34L19.5717 9.09C19.5917 8.73 19.4617 8.39 19.2317 8.14Z" fill="#E42B2B"/>' +
                            '<path fill-rule="evenodd" clip-rule="evenodd" d="M9.57812 17C9.57812 16.5858 9.91391 16.25 10.3281 16.25H13.6581C14.0723 16.25 14.4081 16.5858 14.4081 17C14.4081 17.4142 14.0723 17.75 13.6581 17.75H10.3281C9.91391 17.75 9.57812 17.4142 9.57812 17Z" fill="#E42B2B"/>' +
                            '<path fill-rule="evenodd" clip-rule="evenodd" d="M8.75 13C8.75 12.5858 9.08579 12.25 9.5 12.25H14.5C14.9142 12.25 15.25 12.5858 15.25 13C15.25 13.4142 14.9142 13.75 14.5 13.75H9.5C9.08579 13.75 8.75 13.4142 8.75 13Z" fill="#E42B2B"/>' +
                            '</svg></div>');
                    }

                    function deleteMail(item) {
                        var count = $('.detail-mail').length;
                        $(item).parent().remove();
                        if (count == 2) {
                            $('.detail-mail').find('svg').addClass('d-none');
                        }
                    }
                </script>
                <div class="d-flex align-items-center flex-wrap" style="background: #F9F9F9;
    padding: 30px 20px 0 20px;">
                    <div class="form-group w-100 mb-0" style="text-align: right">
                        <span onclick="showPopupSearch()" data-toggle="modal" data-target="#modalListMail" data-backdrop="static" data-keyboard="false" style="display: inline-block;cursor: pointer;font-weight: 400; font-size: 16px;line-height: 23px;color: #0F94B5;" class="label-form">テンプレートメール選択する</span>
                    </div>
                </div>
                <script>
                    function showPopupSearch() {
                        $('.key_word').val('');
                        searchTemplate();
                    }
                </script>
                <div class="d-flex align-items-center flex-wrap" style="background: #F9F9F9;
    padding: 30px 20px 0 20px;">
                    <div class="form-group w-100 mb-0">
                        <label class="label-form" for="number-of-times">メール件名</label>
                        <input class="form-control-add" id="subject-1"
                               placeholder="メール件名" name="subject" value="{{old('subject') ?? $data['mailSetting']->subject}}" />
                        @error('subject')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap" style="background: #F9F9F9;
    padding: 30px 20px;">
                    <div class="form-group w-100 mb-0">
                        <label class="label-form" for="number-of-times">メール本文</label>
                        <textarea class="form-control-add form-control-textarea" id="contents" rows="3"
                                  placeholder="内容を入力してください" maxlength="1500"
                                  name="content">{{old('content') ?? $data['mailSetting']->content}}</textarea>
                        @error('content')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="button" id="button-save" class="setting-alert-button default-button"
                        @if (request('show') == true) disabled @endif>確認する
                </button>
            </div>
        </div>
    </form>
    <div class="modal" id="modalEditAlertSuccessID1" tabindex="-1" role="dialog"
         aria-labelledby="modalEditAlertSuccessID1Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アラート編集</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <div class="mint-setting-alert mb-30 mt-15">日時設定</div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="label-form mb-20">日選択</div>
                                    <div class="text-setting-alert mb-35" id="start_time-popup"></div>
                                </div>
                                <div class="col-6">
                                    <div class="label-form mb-20">時選択</div>
                                    <div class="text-setting-alert mb-35" id="time-popup"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="label-form mb-20">頻度</div>
                                    <div class="text-setting-alert mb-35" id="frequency-popup"></div>
                                </div>
                                <div
                                    class="col-6 @if(old('frequency_type') == \App\Consts::MAILING_FREQUENCY_DAILI || $data['mailSetting']->frequency_type == \App\Consts::MAILING_FREQUENCY_DAILI)  @error('interval') '' @else d-none @enderror @endif"
                                    id="numer-date-diff1">
                                    <div class="label-form mb-20">送信間隔</div>
                                    <div class="text-setting-alert mb-35" id="interval-popup"></div>
                                </div>
                                <div class="col-6">
                                    <div class="label-form mb-20">回数</div>
                                    <div class="text-setting-alert mb-35" id="send_times-popup"></div>
                                </div>
                            </div>
                            <div class="mint-setting-alert mb-30">受信対象</div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="label-form mb-20">事業所選択</div>
                                    <div class="text-setting-alert mb-35" id="user-list-popup"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="label-form mb-20">グループ一覧</div>
                                    <div class="text-setting-alert mb-35" id="group-list-popup"></div>
                                </div>
                            </div>

                            <div class="mint-setting-alert mb-30">外部メールを送信する</div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="label-form mb-20">メール</div>
                                    <div class="text-setting-alert mb-35" id="list-mail-popup"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="label-form mb-20">メール件名</div>
                                    <div class="text-setting-alert mb-35" id="subject-popup"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="label-form mb-20">メール本文</div>
                                    <div class="text-setting-alert" id="content-popup"></div>
                                </div>
                            </div>

                            <!-- if edit alert success display modal B3003.3 success -->
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="閉じる">
                                    キャンセル
                                </button>
                                <button type="button" class="w-205 default-button" id="submit-form-setting">
                                    保存
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modalListMail" tabindex="-1" role="dialog"
         aria-labelledby="modalListMail" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">テンプレート一覧</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body text-left max-height-700">
                    <div class="add-employee">
                        <form>
                            <div style="display: flex">
                                <div class="wrapper-search mb-10" style="width: 550px">
                                    <input class="input-search key_word" style="z-index: 1;" type="text" maxlength="100" placeholder="テンプレート名" name="key_word" value="">
                                    <button class="button-search" type="button" style="cursor: unset;">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4" d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z" fill="#7C9AA1"></path>
                                            <path d="M21.3 22C21.12 22 20.94 21.93 20.81 21.8L18.95 19.94C18.68 19.67 18.68 19.23 18.95 18.95C19.22 18.68 19.66 18.68 19.94 18.95L21.8 20.81C22.07 21.08 22.07 21.52 21.8 21.8C21.66 21.93 21.48 22 21.3 22Z" fill="#7C9AA1"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div class="search-button mb-10">
                                    <button class="search-button" style="width: 50px; margin:0" type="button" onclick="searchTemplate()">
                                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                            <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="body-template row" style="padding: 20px 0; max-height: 400px; overflow-y: auto">
                            </div>
                            <script>
                                function searchTemplate()
                                {
                                    $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                                    var key_word = $('input[name="key_word"]').val();
                                    $.ajax({
                                        url: '{{route('template-mail.get-list')}}',
                                        type: 'GET',
                                        data: {
                                            key_word: key_word,
                                        },
                                        headers: {
                                            'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                                        },
                                        dataType: "json",
                                    }).done(function (data) {
                                        var respon = data.data.data;
                                        var html = '';
                                        respon.forEach(function (item, key) {
                                            var textCss = '';
                                            var checked = '';
                                            if (key != 0) {
                                                textCss = 'style="margin-top: 24px"';
                                            } else {
                                                checked = 'checked';
                                            }
                                            html+= '<div class="col-sm-12" ' + textCss + '>' +
                                                '<div class="form-check form-radio-check pl-0">' +
                                                '<input ' + checked + ' class="form-check-input select-checked" name="mail_id" id="flexRadioDefault' + key + '" type="radio" value="' + item.id + '">' +
                                                '<label class="form-check-label" for="flexRadioDefault' + key + '">' + htmlEntities(item.title) +'</label>' +
                                                '</div></div>';
                                        });
                                        if (html == '') {
                                            html = '<div class="col-sm-12"><div class="form-check form-radio-check pl-0" style="text-align: center; color: #B8B8B8;">テンプレートが作成されていません。</div></div>'
                                        }
                                        $('.body-template').html(html);
                                        $("#preloader").remove();
                                    })
                                }
                                function htmlEntities(str) {
                                    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, "<br/>").replace(/ /g, "\u00a0");;
                                }

                                function IsEmail(email) {
                                    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                    if(!regex.test(email)) {
                                        return false;
                                    }else{
                                        return true;
                                    }
                                }
                            </script>
                            <!-- if edit alert success display modal B3003.3 success -->
                            <div class="d-flex align-items-center justify-content-center mt-50">
                                <button type="button" class="w-205 second-button" data-dismiss="modal" aria-label="閉じる">
                                    キャンセル
                                </button>
                                <button type="button" class="w-205 default-button" onclick="loadTemplateMail()">
                                    適用
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('js/ckeditor.js')}}"></script>
    <script src="{{asset('js/plugin.js')}}"></script>
    <script>
        function loadTemplateMail() {
            var id = $("input[name='mail_id']:checked").val();
            $('#modalListMail').modal('hide');
            if (id) {
                $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
                $.ajax({
                    url: '{{route('template-mail.detail')}}',
                    type: 'GET',
                    data: {
                        id: id,
                    },
                    headers: {
                        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                    },
                    dataType: "json",
                }).done(function (data) {
                    var respon = data.data;
                    $('input[name="subject"]').val(respon.title);
                    CKEDITOR.instances.contents.setData(respon.content);
                    $("#preloader").remove();
                })
            }
        }
    </script>
    @if(Session::has('success'))
        <div class="modal fade  show" id="modalSuccess" tabindex="-1" role="dialog"
             aria-labelledby="modalEditAlertSuccessID1Title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
                <div class="modal-content home-modal-content">
                    <div class="modal-header home-modal-header">
                        <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">新アラート作成</h5>
                        <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                        </button>
                    </div>
                    <div class="modal-body home-modal-body">
                        <div class="edit-alert-success">
                            <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                            <p class="modal-text"> アラート編集成功。</p>
                            <button onclick="location.href='{{route('setting-alert-auto.index')}}'" type="button"
                                    class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">適用
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <script>
        CKEDITOR.replace( 'contents', {
            language: 'ja',
            extraPlugins: 'editorplaceholder',
            editorplaceholder: '内容を入力してください',
        });
        $(document).ready(function () {
            $('#frequency').on('change', function () {
                $('#number-diff-times-1').val('');
                if ($(this).val() && $(this).val() != {{\App\Consts::MAILING_FREQUENCY_DAILI}}) {
                    $('#numer-date-diff').removeClass('d-none');
                    $('#numer-date-diff input').attr('disabled', false);
                    $('#numer-date-diff1').removeClass('d-none');
                    $('#number-of-times').removeClass('w-430');
                    $('#number-of-times').addClass('w-300');
                } else {
                    $('#numer-date-diff').addClass('d-none');
                    $('#numer-date-diff1').addClass('d-none');
                    $('#numer-date-diff input').attr('disabled', 'disabled');
                    $('#number-of-times').removeClass('w-300');
                    $('#number-of-times').addClass('w-430');
                }
            })

            $('input').on('input', function () {
                if ($('#except-for-some').checked) {
                    disableButton();
                }
            })
            $('textarea').on('input', function () {
                $('#button-save').prop('disabled', false);
            })
            $('#except-for-some').change(function () {
                if (this.checked) {
                    $('.form-group-select').removeClass('form-group-disabled');
                    $('.form-group-select select').attr('disabled', false).trigger('chosen:updated');
                    $('#button-save').prop('disabled', true);
                } else {
                    $('.form-group-select').addClass('form-group-disabled');
                    $('.form-group-select select').prop('disabled', true).trigger('chosen:updated');
                    $('option').prop('selected', false);
                    $('select').trigger('chosen:updated');
                    $('#button-save').prop('disabled', false);
                }
            })
            var date = new Date();
            var year = date.getFullYear()
            var month = date.getMonth();
            $("#form-datepicker-1").datepicker({
                format: "yyyy/mm/dd",
                autoclose: true,
                language: 'ja',
                startDate: new Date()
            });
            $('#form-icon-datepicker-1').on('click', function () {
                $('#form-datepicker-1').trigger('focus');
            })

            $(".number-class").on("keypress", function (evt) {
                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                    evt.preventDefault();
                }
            });

        })
        $(function () {
            $('.time').on('click', function () {
                var isChecked = $('.time').is(':checked');
                if (isChecked == false) {
                    $(this).prop("checked", true);
                } else {
                    $('.time').not(this).prop('checked', false);
                }
            })
            $('#button-save').on('click', function () {
                var listMails = $('input[name="list_mail[]"]');
                var error = false;
                listMails.each(function (key, item) {
                    $(item).parent().find('p').remove();
                    $(item).css('color', 'black');
                    var email = $(item).val();
                    if (email && email != null && !IsEmail(email)) {
                        error = true;
                        $(item).css('color', 'red');
                        $(item).parent().append('<p style="color:red">入力したメールアドレスが不正です。</p>');
                    }
                })
                if (error == false) {
                    $('#modalEditAlertSuccessID1').modal({backdrop: 'static', keyboard: false})
                    $('#modalEditAlertSuccessID1').modal('show');
                    var userSelect = [];
                    $('#user-list-1').children(':selected').each(function () {
                        userSelect.push($(this).text());
                    });
                    var userText = (userSelect.splice(0, 5)).join('、');
                    if (userSelect.length > 0) {
                        userText += ', ...<span class="rest-more">+' + userSelect.length + '</span>'
                    }
                    $('#user-list-popup').html(userText);
                    var groupSelect = [];
                    $('#group-list-1').children(':selected').each(function () {
                        groupSelect.push($(this).text());
                    });
                    var groupText = (groupSelect.splice(0, 5)).join('、');
                    if (groupSelect.length > 0) {
                        groupText += ', ...<span class="rest-more">+' + groupSelect.length + '</span>'
                    }
                    var listMailText = [];
                    var listMail = $('input[name="list_mail[]"]');
                    listMail.each(function (key, input) {
                        if ($(input).val()) {
                            listMailText.push($(input).val());
                        }
                    })
                    $('#group-list-popup').html(groupText);
                    $('#list-mail-popup').html(listMailText.join('、'));
                    $('#start_time-popup').html($("#form-datepicker-1").val());
                    $('#time-popup').html($(".time:checked").val());
                    $('#frequency-popup').html($("#frequency option:selected").text());
                    $('#interval-popup').html($("#number-diff-times-1").val());
                    $('#send_times-popup').html($("#number-of-times-1").val());
                    $('#subject-popup').html(htmlEntities($("#subject-1").val()));
                    $('#content-popup').html(CKEDITOR.instances.contents.getData());
                }


            })

            $('#submit-form-setting').on('click', function () {
                $('#setting-alert-form').submit();
            })
        })

        function disableButton() {
            $('#button-save').prop('disabled', true);
            var groupValue = $('#group-list-1').val();
            var userValue = $('#user-list-1').val();
            if (userValue.length > 0 || groupValue.length > 0) {
                $('#button-save').prop('disabled', false);
            }
        }
    </script>
@endsection
