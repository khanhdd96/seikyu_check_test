@extends('layouts.admin')
@section('title', '設定')
@section('content')
    <style>
        .select2-selection{
            height: 100% !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display:none;
        }
        .select2-results__option--selectable {
            font-size: 16px;
        }
        span.select2-selection.select2-selection--multiple {
            min-height: 50px !important;
            padding: 10px 10px 15px 10px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }
    </style>
    <form action="{{route('setting-alert-auto.store')}}" class="main-content main-admin" method="post" autocomplete="off">
        @csrf
        <div class="main-heading mb-30">
            <h1 class="title-heading">設定</h1>
        </div>

        <div class="main-body">
            <div class="wrapper-setting-alert">
                <p class="text-setting-alert mb-50">
                    デフォルト設定：初期設定として毎月「配信日時：月初の営業日朝9時」「配信期間：<br/>
                    （チェック完了するまで）毎日配信」「配信回数：回数10回（※10日分）」となります
                </p>
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="form-group mr-20 mb-0">
                        <label class="label-form" for="form-1">日選択</label>
                        <div id="datepicker" class="datepicker-heading ml-0 w-430">
                            <input id="form-datepicker-1" class="input-datepicker" type="text" value="{{old('start_date') ?? ''}}" name="start_date" placeholder="年/月/日">
                            <span class="icon-datepicker" id="form-icon-datepicker-1">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z"
                                                fill="#7C9AA1"/>
                                            <path opacity="0.4"
                                                  d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z"
                                                fill="#7C9AA1"/>
                                            <path
                                                d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                        </div>
                        @error('start_date')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mr-20 mb-0">
                        <label class="label-form" for="form-2">時選択</label>
                        <div class="datepicker-heading ml-0 w-430">
                            <input id="form-timer-1" class="input-datepicker" type="text" name="time"  value="{{old('time') ?? ''}}" placeholder="00:00">
                            <span class="icon-datepicker icon-clock">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.4"
                                                  d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
                                                  fill="#7C9AA1"/>
                                            <path
                                                d="M15.71 15.93C15.58 15.93 15.45 15.9 15.33 15.82L12.23 13.97C11.46 13.51 10.89 12.5 10.89 11.61V7.50999C10.89 7.09999 11.23 6.75999 11.64 6.75999C12.05 6.75999 12.39 7.09999 12.39 7.50999V11.61C12.39 11.97 12.69 12.5 13 12.68L16.1 14.53C16.46 14.74 16.57 15.2 16.36 15.56C16.21 15.8 15.96 15.93 15.71 15.93Z"
                                                fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                        </div>
                        @error('time')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="text-form-group w-430 mr-20">頻度: 毎日</div>
                    <div class="text-form-group w-430 mr-20">回数: 10</div>
                </div>


                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="container-checkbox">
                        <input type="checkbox" id="except-for-some" name="except"{{old('users') || old('groups') ? 'checked' : ''}}>
                        <label for="except-for-some">一部を除く対象</label>
                    </div>
                </div>

                <!-- if checked checkbox remove class="form-group-disabled" and property "disabled" -->
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <!-- add class="form-group-disabled" if disable form -->
                    <div class="form-group {{old('users') ? '' : 'form-group-disabled' }} form-group-select w-100 mb-0">
                        <label class="label-form" for="user-list">事業所選択</label>
                        <select id="user-list-1" data-placeholder="ユーザーを選択してください。" multiple
                                class="chosen-select form-control-add" {{old('users') ? '' : 'disabled' }} name="users[]">
                            @foreach($data['users'] as $user)
                                <option value="{{$user->id}}"
                                @if(old('users'))
                                    @foreach(old('users') as $i => $field)
                                        {{ ($field == $user->id ? "selected":"") }}
                                        @endforeach
                                    @else
                                    @endif
                                >{{$user->name}}</option>
                            @endforeach
                        </select>
                        @error('users')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex align-items-center flex-wrap mb-30">
                    <!-- add class="form-group-disabled" if disable form -->
                    <div class="form-group w-100 {{old('groups') ? '' : 'form-group-disabled' }} mb-0 form-group-select">
                        <label class="label-form" for="group-list">グループ一覧</label>
                        <select id="group-list-1" data-placeholder="グループを選択してください。" multiple
                                class="chosen-select form-control-add" name="groups[]" {{old('groups') ? '' : 'disabled' }}>
                            @foreach($data['groups'] as $group)
                                <option value="{{$group->id}}"
                                @if(old('groups'))
                                    @foreach(old('groups') as $i => $field)
                                        {{ ($field == $group->id ? "selected":"") }}
                                        @endforeach
                                    @else
                                    @endif>{{$group->group_name}}</option>
                            @endforeach
                        </select>
                        @error('groups')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap mb-30">
                    <div class="form-group w-100 mb-0">
                        <label class="label-form" for="number-of-times">回数</label>
                        <textarea class="form-control-add form-control-textarea" id="number-of-times" rows="3"
                                  placeholder="回数" maxlength="1500" name="content">{{old('content') ?? ''}}</textarea>
                        @error('content')
                        <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <button type="submit" id="button-save" class="setting-alert-button default-button">保存</button>
            </div>
        </div>
    </form>
    @if(Session::has('success'))
        <div class="modal fade  show" id="modalEditAlertSuccessID1" tabindex="-1" role="dialog" aria-labelledby="modalEditAlertSuccessID1Title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
                <div class="modal-content home-modal-content">
                    <div class="modal-header home-modal-header">
                        <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">新アラート作成</h5>
                        <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                        </button>
                    </div>
                    <div class="modal-body home-modal-body">
                        <div class="edit-alert-success">
                            <form>
                                <img class="home-modal-icon" src="{{ asset('icons/tick.svg') }}" alt="success">
                                <p class="modal-text"> 通知設定を成功しました。
                                </p>
                                <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">適用</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <script>
        $(document).ready(function () {
            // $('#business-selection').select2({
            //     // minimumInputLength: 1,
            //     language: {
            //         inputTooShort: function (args) {
            //
            //             return "任意の文字を入力してください。。。";
            //         },
            //         noResults: function () {
            //             return "見つかりません。。。";
            //         }
            //     },
            // });
            // $('#group-list-1').select2({
            //     closeOnSelect: false,
            //     language: {
            //         inputTooShort: function (args) {
            //             return "任意の文字を入力してください。。。";
            //         },
            //         noResults: function () {
            //             return "見つかりません。。。";
            //         },
            //         searching: function () {
            //             return "検索しています。。。";
            //         }
            //     },
            // });
            // $('#user-list-1').select2({
            //     minimumInputLength: 1,
            //     closeOnSelect: false,
            //     language: {
            //         inputTooShort: function (args) {
            //
            //             return "任意の文字を入力してください。。。";
            //         },
            //         noResults: function () {
            //             return "見つかりません。。。";
            //         },
            //         searching: function () {
            //             return "検索しています。。。";
            //         }
            //     },
            // });
        });
        $(document).ready(function () {
            var date = new Date();
            var year = date.getFullYear()
            var month = date.getMonth();
            $("#form-datepicker-1").datepicker({
                format: "yyyy/mm/dd",
                autoclose: true,
                language: 'ja',
                startDate: new Date()
            });
            $('#form-icon-datepicker-1').on('click', function () {
                $('#form-datepicker-1').trigger('focus');
            })
            $('#form-timer-1').timepicker();
            $('.icon-datepicker.icon-clock').on('click', function () {
                $('#form-timer-1').trigger('click');
            })
            $(".number-class").on("keypress", function (evt) {
                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                    evt.preventDefault();
                }
            });
        })
        $('select').change(function () {
            var user = $("#user-list-1 :selected" ).length;
            var group = $("#group-list-1 :selected" ).length;
            if (user > 0 || group > 0) {
                $('#button-save').prop('disabled', false);
            } else {
                $('#button-save').prop('disabled', true);
            }
        })
        $('#except-for-some').change(function (){
            if (this.checked) {
                $('.form-group-select').removeClass('form-group-disabled');
                $('.form-group-select select').attr('disabled', false).trigger('chosen:updated');
                var user = $("#user-list-1 :selected" ).length;
                var group = $("#group-list-1 :selected" ).length;
                if (user > 0 || group > 0) {
                    $('#button-save').prop('disabled', false);
                } else {
                    $('#button-save').prop('disabled', true);
                }
            } else {
                $('.form-group-select').addClass('form-group-disabled');
                $('.form-group-select select').prop('disabled', true).trigger('chosen:updated');
                $('option').prop('selected', false);
                $('select').trigger('chosen:updated');
                $('#button-save').prop('disabled', false);
            }
        })
    </script>
@endsection
