@extends('layouts.admin')
@section('title', '各種ファイルダウンロード管理')
<style>
    .number-chartLegend1 {
        white-space: nowrap;
        margin-left: 10px;
    }
    .search-button {
        width: 50px !important;
    }
    .w-216 {
        width: 216px;
    }
    .h-70 {
        height: 70%;
    }
    .select2-selection {
        height: 100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 22px !important;
        font-size: 16px !important;
        padding: 12px 15px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple {
        line-height: 26px !important;
        font-size: 16px !important;
        background: url(/icons/arrow-circle-down.svg) no-repeat right 13px top 12px #fbfbfb !important;
        border-radius: 5px !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px !important;
        display: none;
    }

    .select2-results__option--selectable {
        font-size: 16px;
    }
    .table-condensed {
        width: 207px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background: #b5e5f0 !important;
        border-radius: 5px !important;
        padding: 5px 20px 5px 5px !important;
        border: none !important;
        align-items: center;
        max-width: 90% !important;
    }

    .select2-selection--multiple .select2-selection__choice__remove {
        left: unset !important;
        top: 5px !important;
        right: 0;
    }
    .select2-selection--multiple .select2-selection__choice__remove span {
        display: none;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        top: 10px !important;
        right: 4px !important;
        width: 18px !important;
        height: 18px !important;
        background: url(/icons/delete.svg) center center no-repeat !important;
    }
    .selection-heading-comparison .select2-container {
        width: 100% !important;
    }

    .selection-heading-comparison .search-department {
        width: 100% !important;
    }

    .selection-heading-comparison {
        padding: 0px 8px;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
    .selection-heading-comparison-department .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background: rgba(124, 154, 161, 0.4) !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        border-radius: 45% !important;
        border-right: none !important;
    }

    .select2-container--default .select2-search--inline .select2-search__field {
        margin-top: 13px !important;
        margin-left: 15px !important;
        height: 24px !important;
        font-size: 16px;
        margin-bottom: 5px;
    }
    .selection-heading-error {
        width: 216px;
        min-height: 50px;
        position: relative;
        border: none;
        box-sizing: border-box;
        border-radius: 5px;
        margin-left: 15px;
        font-size: 16px;
    }
    .selection-heading-error .select2-container--default .select2-search--inline .select2-search__field {
        width: 100%;
        margin-top: 13px;
        margin-left: 15px;
    }

    .select2-container .select2-selection--multiple {
        min-height: 50px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding:12px 22px !important;
    }
    button:disabled,
    button[disabled] {
        background-color: #cccccc;
        color: #666666;
    }

    .table thead th {
        vertical-align: middle !important;
    }
    .table tbody td:nth-child(5),
    .table thead th:nth-child(5) {
        width: 30%;
    }

</style>
@section('content')
<div class="main-content main-admin">
    <div class="main-heading mb-30">
        <h1 class="title-heading">各種ファイルダウンロード管理</h1>
    </div>
    <div class="main-body">
        <div class="container-overview">
            <form action="" class="form-overview">
                <input type="hidden" name="type" value="{{request('type')}}">
                <div class="d-flex mb-20 justify-content-end">
                    <div class="selection-heading">
                        <select class="form-control dropdown-heading select2 search-department select-department-id" name="department_id" onchange="loadDataFacility(this)">
                            <option value="">支店選択</option>
                            @foreach($data['departments'] as $value)
                                <option {{request('department_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="selection-heading">
                        <select class="form-control dropdown-heading select2 search-facility select-facility-id" name="facility_id">
                            <option value="">事業所選択</option>
                            @foreach($data['facilitys'] as $value)
                                <option {{request('facility_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="selection-heading">
                        <select class="form-control dropdown-heading select2" name="type_check">
                        <option value="">全て
                        </option>
                        @foreach(App\Consts::TITLE as $key => $value)
                                <option {{request('type_check') == App\Consts::TYPE_MENU[$key] ? 'selected' : ''  }} value="{{App\Consts::TYPE_MENU[$key]}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="filter-heading">
                        <div id="datepicker" class="datepicker-heading">
                            <input class="input-datepicker" type="text" name="month" placeholder="年/月" value=" {{request('month') ? request('month') : date('Y/m')}}">
                            <span class="icon-datepicker">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1"/>
                                            <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1"/>
                                            <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1"/>
                                            <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1"/>
                                            <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1"/>
                                            <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1"/>
                                            <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1"/>
                                        </svg>
                                    </span>
                        </div>
                    </div>
                    <div class="search-button ml-3">
                        <button class="button-search" type="submit">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                        </button>
                    </div>
                    <button type="button" data-keyboard="false" disabled class="button-add-branch default-button" id="download-file-multi">ダウンロード
                    </button>
                </div>
            </form>

            <div class="top-table">
            </div>
            <form action="{{ route('download-file.all') }}" method="post" autocomplete="off" id="download-multi-file">
                @csrf
                <div class="table-responsive">
                    <table class="table table-striped table-admin mt-15 mb-20">
                        <thead>
                        <tr>
                            <th width="80"><div class="container-checkbox">
                                    <input type="checkbox" id="check-all">
                                    <label for="check-all"><div class="label-facility-no-9"></div></label>
                                </div></th>
                            <th scope="col">事業所名</th>
                            <th scope="col">チェック機能</th>
                            <th scope="col">ファイルアップロード日時</th>
{{--                            <th scope="col">チェックステータス</th>--}}
                            <th scope="col" style="text-align:center; white-space: nowrap;">アップロードファイル名</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data['files'] as $key => $file)
                            <tr>
                                @php
                                    $page = request('page') ?? \App\Consts::BASE_PAGE;
                                    if (array_filter($file->fileSuccessLatest->files->toArray())) {
                                        $names = $file->fileSuccessLatest->files->pluck('file_name')->toArray();
                                        $file->fileSuccessLatest->file_name .= ',<br /> ' . implode(',<br /> ', $names);
                                    }
                                @endphp
                                <td><div class="checkbox-select-facility container-checkbox">
                                        <input type="checkbox" id="file-{{ $file->fileSuccessLatest->id }}" name="file[]" value="{{ $file->fileSuccessLatest->id }}">
                                        <label for="file-{{ $file->fileSuccessLatest->id }}"><div class="label-facility-no-9"></div></label>
                                    </div></td>
                                <td>{{ $file->facility->name }}</td>
                                <td>{{ \App\Consts::TITLE[array_search($file->code_check, \App\Consts::TYPE_MENU)] }}</td>
                                <td>{{ date('Y/m/d H:i:s', strtotime($file->fileSuccessLatest->created_at)) }}</td>
{{--                                <td>{{ \App\Consts::STATUS_CHECK[$file->status] }}</td>--}}
                                <td style="justify-content: flex-start">{!! $file->fileSuccessLatest->file_name !!}</td>
                                <td><a href="javascript:void(0)" data-id="{{$file->fileSuccessLatest->id}}" download class="right-check-block download-button">
                                    <span class="btn-download-check">
                        <img class="" src="{{ asset('icons/download.svg') }}" alt="download">
                        </span>
                                    </a></td>
                            </tr>
                        @empty
                            <tr class="text-center">
                                <td style="display: revert;" colspan="5">{{\App\Messages::EMPTY_RECORD}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </form>
            <nav class="wrapper-pagination">
                {{ $data['files']->appends(request()->except('page'))->links('partial.admin.paginate') }}
            </nav>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.ja.min.js')}}" integrity="sha512-zI0UB5DgB1Bvvrob7MyykjmbEI4e6Qkf5Aq+VJow4nwRZrL2hYKGqRf6zgH3oBQUpxPLcF2IH5PlKrW6O3y3Qw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="{{ asset('js/tooltip.js') }}"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/multi-select.js') }}"></script>
<script>
    function loadDataFacility(select)
    {
        var departmentId = $('.select-department-id').val();
        $.ajax({
            url: '{{route('get-list-facilyties')}}',
            type: 'POST',
            data: {
                department_id : departmentId
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            dataType: "json",
        }).done(function (data) {
            var html = '<option value="">事業所選択</option>';
            var facilityList = data.data;
            var selected = null;
            $.each(facilityList, function(key, value) {
                if (key == 0) {
                    selected = value.id;
                }
                html+= '<option value="' + value.id + '">' + value.name + '</option>';
            });
            $('.select-facility-id').html(html);
            $('.select-facility-id').select2();
        });
    }
    $(document).ready(function () {
        $('.select2').select2({
            language: {
                inputTooShort: function (args) {

                    return "任意の文字を入力してください。。。";
                },
                noResults: function () {
                    return "見つかりません。。。";
                },
                searching: function () {
                    return "検索しています。。。";
                }
            },
        });
        $('a.download-button').on('click', function (){
            download($(this));
        })
        var date=new Date();
        var year=date.getFullYear()
        var month=date.getMonth();
        $("#datepicker input").datepicker({
            format: "yyyy/mm",
            startView: "months",
            minViewMode: "months",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });

        $("#datepicker-year input").datepicker({
            format: "yyyy",
            startView: "years",
            minViewMode: "years",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });
        $('.icon-datepicker').on('click', function () {
            $('#datepicker input').trigger('focus');
            $('#datepicker-year input').trigger('focus');
        });
        $("#check-all").change(function () {
            if(this.checked) {
                $(".checkbox-select-facility input").prop('checked', true);
                $("#download-file-multi").prop('disabled', false);
            } else {
                $(".checkbox-select-facility input").prop('checked', false);
                $("#download-file-multi").prop('disabled', true);
            }
        })
        $(".checkbox-select-facility input").change(function () {
            if ($(".checkbox-select-facility input:checkbox:checked").length != $(".checkbox-select-facility input:checkbox").length) {
                $("#check-all").prop('checked', false);
            } else {
                $("#check-all").prop('checked', true);
            }
            if ($(".checkbox-select-facility input:checkbox:checked").length == 0) {
                $("#download-file-multi").prop('disabled', true);
            } else {
                $("#download-file-multi").prop('disabled', false);
            }
        })

        $("#download-file-multi").click(function(){
            //$("#download-multi-file").submit();
            var formData = new FormData($("#download-multi-file")[0]);
            $.ajax({
                url: '{{ route('download-file.all-ajax') }}',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
            }).done(function (data) {
                var links = data.data;
                for (let i = 0; i < links.length; i++) {
                    var frame = document.createElement("iframe");
                    frame.style.display = "none";
                    frame.src = links[i];
                    frame["download"] = 1
                    document.body.appendChild(frame);

                }
            })
        })
    });
    function download(element) {
        var id = element.data('id');
        // only works with files that don't render in browser
        // ie: not video, not text, not photo
        $.ajax({
            url: '{{route('link-download')}}',
            type: 'GET',
            data: {
                id: id,
            },
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            dataType: "json",
        }).done(function (data) {
            var links = data.data.links;
            for (let i = 0; i < links.length; i++) {
                var frame = document.createElement("iframe");
                frame.style.display = "none";
                frame.src = links[i];
                frame["download"] = 1
                document.body.appendChild(frame);
            }
        })
    }
</script>
@endsection
