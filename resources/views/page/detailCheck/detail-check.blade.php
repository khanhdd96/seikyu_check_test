@extends('layouts.app')
@section('title', \App\Consts::TITLE[array_search (request('type_check'), \App\Consts::TYPE_MENU)])
@section('content')
    <style>
        #modalError .home-modal-button {
            margin: 50px 0 0 0 !important;
        }
        .select2-selection{
            height: 100% !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 46px !important;
            display:none;
        }
        span.select2.select2-container.select2-container--default {
            height: 50px;
        }
        .select2-results__option--selectable {
            font-size: 16px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 22px !important;
            font-size: 16px !important;
            padding: 12px 15px !important;
            background: url(/icons/arrow-circle-down.svg) no-repeat right 13px bottom 12px #fbfbfb;
            border-radius: 5px !important;
        }
        #file-name-2, #file-name, .file-name {
            margin-bottom: 10px;
        }
        #notification {
            font-weight: normal;
            font-size: 14px;
            line-height: 23px;
        }
        .table-responsive {
             max-height: 300px;
             overflow-y: auto;
        }
        .file-sample {
            padding-bottom: 5px;
            font-weight: 400;
            font-size: 12px;
            color: #00285A;
        }
        .file-title {
            padding-top: 10px;
            font-weight: 400;
            font-size: 14px;
            color: #7C9AA1;
        }
        .file-name-sample {
            font-weight: 700;
            padding-right: 0 !important;
        }
        #file-upload-form-no5 {
            width: 426px !important;
        }
        .file-response-2 {
            margin-bottom: 15px !important;
            align-items: unset !important;
        }
        .icon-upload {
            width: 16px;
            height: 16px;
            margin-right: 8px;
            margin-top: 3px;
        }
        .item-error {
            margin-bottom: 0 !important;
            line-height: 23px;
            font-size: 16px;
            padding-top: 3px;
        }
        .item-error-message {
            font-size: 14px;
            font-weight: 400;
            margin-bottom: 0 !important;
            padding-bottom: 3px;
        }
        #listFileError {
            max-width: 652px;
            padding-bottom: 20px;
        }
        #text-file-pass {
            padding-top: 10px;
            font-size: 12px;
            color: #00285A;
            font-weight: 400;
            text-align: left;
        }
        .list-file-pass {
            padding-top: 30px;
            padding-bottom: 20px;
        }
        .file-type {
            font-weight: 400;
            font-size: 14px;
            color: #7C9AA1;
            text-align: left;
        }
        .file-name-pass {
            padding-top: 10px;
        }
        .item-upload div p {
            font-style: normal;
            font-weight: 400;
            font-size: 10px;
            line-height: 14px;
            color: #B8B8B8;
        }
        .btn-download-file-type-1 {
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 17px;
            color: #00285A;
        }
        .modal-text {
            letter-spacing: -0.02em;
        }
        .warning {
            background-color: #fcf8e3;
            color: #8a6d3b;
        }
    </style>
    <div class="main-content main-detail-check">
        <div class="main-heading mb-30">
            <h1 class="title-heading">{{ \App\Consts::TITLE[array_search (request('type_check'), \App\Consts::TYPE_MENU)] }}</h1>
                <button type="button" class="default-upload-button default-button m-0" data-toggle="modal"
                        @if($data['settingMonth']->is_checking) disabled @endif
                        data-target="@if (request('type_check') == 2) #modalUploadID5 @elseif(request('type_check') == 1) #modalUploadID2 @elseif(request('type_check') == 4) #modalUploadID8 @else #modalUploadID1 @endif" data-backdrop="static" data-keyboard="false" {{($data['settingMonth']->status_facilitity !=  App\Consts::STATUS_CHECK_FILE['success'] && $data['settingMonth']->can_upload == 1) ? '' : 'disabled'}}>ファイルアップロード
                </button>
        </div>
        <div class="right-block-datepicker">
            <form action="" method="get">
                <div class="filter-heading" style="justify-content: end;">
                <div class="selection-heading">
                    <select name="department_id" class="select2 form-control dropdown-heading" onchange="loadDataFacility(this)">
                        @if($data['departments'])
                            @foreach($data['departments'] as $department)
                                <option value="{{$department->id}}" {{ request("department_id") == $department->id ? "selected" : "" }}>{{$department->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="selection-heading">
                    <select name="facility_id" class="select2 form-control dropdown-heading">
                        @foreach($data['facilities'] as $facility)
                            <option value="{{$facility->id}}" {{ request("facility_id") == $facility->id ? "selected" : "" }}>{{$facility->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div id="datepicker" class="datepicker-heading" style="position: relative">
                    <input class="input-datepicker" type="text"  name="month" placeholder="年/月" value="{{$data['month']}}">
                    <span class="icon-datepicker">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.7502 3.56V2C16.7502 1.59 16.4102 1.25 16.0002 1.25C15.5902 1.25 15.2502 1.59 15.2502 2V3.5H8.75023V2C8.75023 1.59 8.41023 1.25 8.00023 1.25C7.59023 1.25 7.25023 1.59 7.25023 2V3.56C4.55023 3.81 3.24023 5.42 3.04023 7.81C3.02023 8.1 3.26023 8.34 3.54023 8.34H20.4602C20.7502 8.34 20.9902 8.09 20.9602 7.81C20.7602 5.42 19.4502 3.81 16.7502 3.56Z" fill="#7C9AA1" />
                        <path opacity="0.4" d="M20 9.84C20.55 9.84 21 10.29 21 10.84V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V10.84C3 10.29 3.45 9.84 4 9.84H20Z" fill="#7C9AA1" />
                        <path d="M8.5 14.9999C8.24 14.9999 7.98 14.8899 7.79 14.7099C7.61 14.5199 7.5 14.2599 7.5 13.9999C7.5 13.7399 7.61 13.4799 7.79 13.2899C8.07 13.0099 8.51 12.9199 8.88 13.0799C9.01 13.1299 9.12 13.1999 9.21 13.2899C9.39 13.4799 9.5 13.7399 9.5 13.9999C9.5 14.2599 9.39 14.5199 9.21 14.7099C9.02 14.8899 8.76 14.9999 8.5 14.9999Z" fill="#7C9AA1" />
                        <path d="M12 14.9999C11.74 14.9999 11.48 14.8899 11.29 14.7099C11.11 14.5199 11 14.2599 11 13.9999C11 13.7399 11.11 13.4799 11.29 13.2899C11.38 13.1999 11.49 13.1299 11.62 13.0799C11.99 12.9199 12.43 13.0099 12.71 13.2899C12.89 13.4799 13 13.7399 13 13.9999C13 14.2599 12.89 14.5199 12.71 14.7099C12.66 14.7499 12.61 14.7899 12.56 14.8299C12.5 14.8699 12.44 14.8999 12.38 14.9199C12.32 14.9499 12.26 14.9699 12.2 14.9799C12.13 14.9899 12.07 14.9999 12 14.9999Z" fill="#7C9AA1" />
                        <path d="M15.5 15C15.24 15 14.98 14.89 14.79 14.71C14.61 14.52 14.5 14.26 14.5 14C14.5 13.74 14.61 13.48 14.79 13.29C14.89 13.2 14.99 13.13 15.12 13.08C15.3 13 15.5 12.98 15.7 13.02C15.76 13.03 15.82 13.05 15.88 13.08C15.94 13.1 16 13.13 16.06 13.17C16.11 13.21 16.16 13.25 16.21 13.29C16.39 13.48 16.5 13.74 16.5 14C16.5 14.26 16.39 14.52 16.21 14.71C16.16 14.75 16.11 14.79 16.06 14.83C16 14.87 15.94 14.9 15.88 14.92C15.82 14.95 15.76 14.97 15.7 14.98C15.63 14.99 15.56 15 15.5 15Z" fill="#7C9AA1" />
                        <path d="M8.5 18.5C8.37 18.5 8.24 18.47 8.12 18.42C7.99 18.37 7.89 18.3 7.79 18.21C7.61 18.02 7.5 17.76 7.5 17.5C7.5 17.24 7.61 16.98 7.79 16.79C7.89 16.7 7.99 16.63 8.12 16.58C8.3 16.5 8.5 16.48 8.7 16.52C8.76 16.53 8.82 16.55 8.88 16.58C8.94 16.6 9 16.63 9.06 16.67C9.11 16.71 9.16 16.75 9.21 16.79C9.39 16.98 9.5 17.24 9.5 17.5C9.5 17.76 9.39 18.02 9.21 18.21C9.16 18.25 9.11 18.3 9.06 18.33C9 18.37 8.94 18.4 8.88 18.42C8.82 18.45 8.76 18.47 8.7 18.48C8.63 18.49 8.57 18.5 8.5 18.5Z" fill="#7C9AA1" />
                        <path d="M12 18.5C11.74 18.5 11.48 18.39 11.29 18.21C11.11 18.02 11 17.76 11 17.5C11 17.24 11.11 16.98 11.29 16.79C11.66 16.42 12.34 16.42 12.71 16.79C12.89 16.98 13 17.24 13 17.5C13 17.76 12.89 18.02 12.71 18.21C12.52 18.39 12.26 18.5 12 18.5Z" fill="#7C9AA1" />
                        <path d="M15.5 18.5C15.24 18.5 14.98 18.39 14.79 18.21C14.61 18.02 14.5 17.76 14.5 17.5C14.5 17.24 14.61 16.98 14.79 16.79C15.16 16.42 15.84 16.42 16.21 16.79C16.39 16.98 16.5 17.24 16.5 17.5C16.5 17.76 16.39 18.02 16.21 18.21C16.02 18.39 15.76 18.5 15.5 18.5Z" fill="#7C9AA1" />
                    </svg>
                </span>
                </div>
                <button class="search-button" type="submit">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.375 22.5C18.8623 22.5 22.5 18.8623 22.5 14.375C22.5 9.88769 18.8623 6.25 14.375 6.25C9.88769 6.25 6.25 9.88769 6.25 14.375C6.25 18.8623 9.88769 22.5 14.375 22.5Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M23.75 23.75L22.5 22.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>
            </div>
            </form>
        </div>

        <div class="main-body">
            <!-- 4 blocks -->
            <div class="wrapper-article-block wrapper-article-check">
                <div class="row">
                    <div class="col-lg-3">
                        <article class="article-block first-upload-block">
                            <h5 class="title-article-block line-2">作成日</h5>
                            <h2 class="date-article-block line-2">{{$data['createdAt'] ? date('Y/m/d', strtotime($data['createdAt'])) : 'N/A'}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                        <article class="article-block second-upload-block">
                            <h5 class="title-article-block line-2">更新日</h5>
                            <h2 class="date-article-block line-2">{{$data['updatedAt'] ? date('Y/m/d', strtotime($data['updatedAt'])) : 'N/A'}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                        <article class="article-block third-upload-block">
                            <h5 class="title-article-block line-2">チェックの回数</h5>
                            @php
                            $dataCount = '0';
                            if($data['fileCount'] != 0) {
                                $dataCount = $data['fileCount'];
                                if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH']) {
                                    $dataCount = $data['fileCount'] / 2;
                                }
                            }
                            @endphp
                            <h2 class="date-article-block line-2">{{ $dataCount}}</h2>
                        </article>
                    </div>
                    <div class="col-lg-3">
                    @php $status = isset($typeCheck) && $typeCheck && $typeCheck->status ? $typeCheck->status : \App\Consts::STATUS_CHECK_FILE['not_has_check']; @endphp
                    <!-- block checked  -->
                        <article class="article-block {{\App\Consts::CLASS_STATUS_CHECK_FILE_DETAIL[$status]}}">
                            <h5 class="title-article-block line-2">状態</h5>
                            <h2 class="date-article-block line-2">{{\App\Consts::STATUS_CHECK[$status]}}</h2>
                        </article>
                        <!-- block checked  -->
                    </div>
                </div>
            </div>
            <!-- 4 blocks -->
        </div>
        <div class="main-heading mt-30 mb-25">
            <h4 class="mint-title-result">ファイルチェック履歴</h4>
{{--            @if(request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_1)--}}
{{--            <div class="d-flex">--}}
{{--                <a download href="{{isset($data['typeCheck1Admin']) && !in_array($data['typeCheck1Admin']->step, [\App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_EARLY_MONTH'], \App\Consts::STEP_TYPE_1_ADMIN['UPLOAD_FILE_MID_MONTH']]) ? ($data['typeCheck1Admin']->url_file_mid_month ? Storage::disk('s3')->url($data['typeCheck1Admin']->url_file_mid_month) : Storage::disk('s3')->url($data['typeCheck1Admin']->url_file_early_month)): 'javascript:void(0)'}}"--}}
{{--                   class="mb-10 button-add-branch default-button a-button-add-branch">--}}
{{--                    未収金明細--}}
{{--                </a>--}}
{{--                <a href=""--}}
{{--                   class="mb-10 button-add-branch default-button a-button-add-branch">--}}
{{--                    コメントファイル<br/>--}}
{{--                    アップロード--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            @endif--}}
        </div>
        <div class="wrapper-detail-check" id="detail-check">
            @if(count($files))
                @include('page.detailCheck.item-paginate')
            @else
                <div class="text-center">
                    <img src="{{asset('/icons/empty.png')}}" alt="">
                    <p class="mt-5" style="font-size: 16px">まだファイルがない</p>
                </div>
            @endif
        </div>
    </div>

<!-- Modal upload file: id=ID &aria-labelledby=ID -->
{{--<div class="modal fade" id="modalUploadID3" tabindex="-1" role="dialog" aria-labelledby="modalUploadID3Title" aria-hidden="true">--}}
{{--    <div class="modal-dialog modal-dialog-centered home-modal-dialog multiple-modal-upload" role="document">--}}
{{--        <div class="modal-content home-modal-content">--}}
{{--            <div class="modal-header home-modal-header">--}}
{{--                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>--}}
{{--                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">--}}
{{--                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">--}}
{{--                        </span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body home-modal-body">--}}
{{--                <div id="error" class="text-danger font-weight-bold"></div>--}}
{{--                <!-- Upload  -->--}}
{{--                <div class="uploader uploader-multi">--}}
{{--                    <form id="file-upload-form" action="">--}}
{{--                        <div class="multiple-file-upload row">--}}
{{--                            @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['CSV'])--}}
{{--                            <div class="item-file-upload  {{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] ? 'col-6' : 'col-12'}}">--}}
{{--                                <input id="file-upload-csv" type="file" name="{{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'file[]' : 'file'}}" accept=".csv" />--}}

{{--                                <label for="file-upload-csv" id="file-drag-csv" class="label-file-drag">--}}
{{--                                    <img id="file-image-csv" src="#" alt="Preview" class="hidden file-image">--}}
{{--                                    <div id="start-csv" class="file-start">--}}
{{--                                        <div id="notimage-csv" class="hidden notimage">CSVファイルを選択してください。</div>--}}
{{--                                        <span id="file-upload-btn" class="icon-upload-file">--}}
{{--                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">--}}
{{--                                            </span>--}}
{{--                                        <p class="modal-text"> CSVファイル </p>--}}
{{--                                    </div>--}}
{{--                                </label>--}}
{{--                                <div id="response-csv" class="hidden file-response">--}}
{{--                                    <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">--}}
{{--                                    <div id="messages-csv"></div>--}}
{{--                                </div>--}}
{{--                                <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="{{ isset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV']) ? asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV']) : "" }}" download class="btn-download-file">こちらで</a></p>--}}

{{--                            </div>--}}
{{--                            @endif--}}
{{--                            @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL'])--}}
{{--                            <div class="item-file-upload {{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] || \App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL_OR_CSV'] ? 'col-6' : 'col-12'}}">--}}
{{--                                <input id="file-upload-excel" type="file" name="{{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'file[]' : 'file'}}" accept=".xls, .xlsx" />--}}
{{--                                <label for="file-upload-excel" id="file-drag-excel" class="label-file-drag">--}}
{{--                                    <img id="file-image-excel" src="#" alt="Preview" class="hidden file-image">--}}
{{--                                    <div id="start-excel" class="file-start">--}}
{{--                                        <div id="notimage-excel" class="hidden notimage">Excel ファイルを選択してください。</div>--}}
{{--                                        <span id="file-upload-btn" class="icon-upload-file">--}}
{{--                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">--}}
{{--                                            </span>--}}
{{--                                        <p class="modal-text"> Excel ファイル </p>--}}
{{--                                    </div>--}}
{{--                                </label>--}}
{{--                                <div id="response-excel" class="hidden file-response">--}}
{{--                                    <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">--}}
{{--                                    <div id="messages-excel"></div>--}}
{{--                                </div>--}}
{{--                                <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="{{ isset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL']) ? asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL']) : "" }}" download class="btn-download-file">こちらで</a></p>--}}

{{--                            </div>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                        <!-- <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="#downloadFile" download class="btn-download-file">こちらで</a></p> -->--}}

{{--                        <button type="submit" class="home-modal-button default-button" id="file-upload-form-button"  disabled>チェック</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="modal fade" id="modalUploadID1" tabindex="-1" role="dialog" aria-labelledby="modalUploadID1Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH']) multiple-modal-dialog @endif" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                <div class="uploader">
                    <form id="file-upload-form" action="{{ isset(\App\Consts::ROUTE_CHECK_FILE[request('type_check')]) ? route(\App\Consts::ROUTE_CHECK_FILE[request('type_check')]) : ''}}"
                    method="post" enctype="multipart/form-data" class="file-upload-form @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH']) file-upload-form file-upload-form-no outer-modal-multiple @else outer-modal-single @endif">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['settingMonth'] ? $data['settingMonth']->id : ''}}">
                        <input type="hidden" name="facilityId" value="{{$data['facilityId']}}">
                        <input type="hidden" name="month" value="{{$data['month']}}">
                        <input type="hidden" name="type" value="{{request('type_check')}}">
                        <div class="multiple-file-upload @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH']) row @endif">
                            @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'])
                                <div class="item-file-upload col-6">
                                    <input type="hidden" class="check_type_upload" value="upload-2file"/>
                                    <input id="file-upload-csv" type="file" name="file[]" accept=".csv" />

                                    <label for="file-upload-csv" id="file-drag-csv" class="label-file-drag">
                                        <img id="file-image-csv" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start-csv" class="file-start">
                                            <div id="notimage-csv" class="hidden notimage">CSVファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> CSVファイル </p>
                                        </div>
                                    </label>
                                    <div id="response-csv" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                        <div id="messages-csv"></div>
                                    </div>
                                    {{-- <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV']) }}" download class="btn-download-file">こちらで</a></p> --}}

                                </div>

                                <div class="item-file-upload col-6">
                                    <input id="file-upload-excel" type="file" name="file[]" accept=".xls, .xlsx" />

                                    <label for="file-upload-excel" id="file-drag-excel" class="label-file-drag">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start-excel" class="file-start">
                                            <div id="notimage-excel" class="hidden notimage">Excel ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> Excel ファイル </p>
                                        </div>
                                    </label>
                                    <div id="response-excel" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}" alt="document-upload">
                                        <div id="messages-excel"></div>
                                    </div>
                                    {{-- <p class="modal-text"> サンプルテンプレートファイルのダウンロード - <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL']) }}" download class="btn-download-file">こちらで</a></p> --}}

                                </div>
                            @else
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept="{{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['EXCEL'] ? '.xls, .xlsx' : (\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['CSV'] ? '.csv' : '.xls, .xlsx, .csv')}}"/>
                                    <label for="file-upload" id="file-drag" class="modal-body file-upload">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                    <div id="response" class="hidden file-response">
                                        <img class="mr-22" src="{{ asset('icons/document-upload.svg') }}"
                                             alt="document-upload">
                                        <div id="messages" class="messages-file"></div>
                                    </div>
                                    {{-- @if(isset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV']) && \App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV'])
                                        <p class="modal-text"> サンプルファイルをCSV形式でダウンロードする - <a
                                                href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['CSV']) }}"
                                                download class="btn-download-file">こちらで</a></p>
                                    @endif
                                    @if(isset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL']) && \App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL'])
                                        <p class="modal-text" style="margin-top: 9px !important;"> サンプルファイルをExcel形式でダウンロードする -  <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[request('type_check')]['EXCEL']) }}" download class="btn-download-file">こちらで</a></p>
                                    @endif --}}
                                </div>
                            @endif
                                <div class="file-uploaded row " style="{{\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'] ? 'margin-left: 0px;' : 'margin-left: -5px;'}}">
                                    <div class="col-12 text-left file-sample mb-0" id="textSample">
                                        各チェックのアップロード可能ファイリ名称サンプル<br/>
                                    </div>
                                    @php
                                        $value = request('type_check');
                                    @endphp
                                    @if($value == 7)
                                        <div class="col-12 text-left file-title" id="file1">
                                            請求前確認リスト:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            請求前確認リスト
                                        </div>
                                    @elseif($value == 8)
                                        <div class="col-12 text-left file-title" id="file1">
                                            送り出し指示データ一覧:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            送り出し指示データ一覧-年月日
                                        </div>
                                    @elseif($value == 10)
                                        <div class="col-12 text-left file-title" id="file1">
                                            総合事業請求止めチェック機能:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            予実明細
                                        </div>
                                    @elseif($value == 3)
                                        <div class="col-6 text-left file-title" id="file1">
                                            CSVファイル:
                                        </div>
                                        <div class="col-6 text-left file-title" id="file1">
                                            Excel ファイル:
                                        </div>
                                        <div class="col-6 text-left file-name-sample" id="file1Sample">
                                            TH01_年月
                                        </div>

                                        <div class="col-6 text-left file-name-sample" id="file1Sample">
                                            売上一覧表_保険外
                                        </div>
                                    @elseif($value == 6)
                                        <div class="col-12 text-left file-title" id="file1">
                                            トランデータチェック:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            トランチェックXXXXXXXXXX-年月日-
                                        </div>
                                    @elseif($value == '5-M')
                                        <div class="col-6 text-left file-title" id="file1">
                                            CSVファイル:
                                        </div>
                                        <div class="col-6 text-left file-title" id="file1">
                                            Excel ファイル:
                                        </div>
                                        <div class="col-6 text-left file-name-sample" id="file1Sample">
                                            XXXXmXXX
                                        </div>

                                        <div class="col-6 text-left file-name-sample" id="file1Sample">
                                            請求計算確認表
                                        </div>
                                    @elseif($value == '5-S')
                                        <div class="col-12 text-left file-title" id="file1">
                                            Sファイルチェック:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            XXXXsXXX
                                        </div>
                                    @elseif($value == '5-TH')
                                        <div class="col-12 text-left file-title" id="file1">
                                            TH障害データチェック:
                                        </div>
                                        <div class="col-12 text-left file-name-sample" id="file1Sample">
                                            TH01_年月
                                        </div>
                                    @endif
                                </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button"  id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUploadID5" tabindex="-1" role="dialog" aria-labelledby="modalUploadID5Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="uploader">
                    <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                    <form id="file-upload-form-no5"  action="{{ route('check-2') }}"
                          method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                        @csrf
                        <input type="hidden" name="settingMonth"
                               value="{{$data['settingMonth'] ? $data['settingMonth']->id : ''}}">
                        <input type="hidden" name="facilityId" value="{{$data['facilityId']}}">
                        <input type="hidden" name="month" value="{{ $data['month']}}">
                        <input type="hidden" name="type" value="2">
                        <div class="multiple-file-upload">
                            <div class="item-file-upload">
                                <input id="file-upload" type="file" name="file[]" accept=".xls, .xlsx, .csv" multiple/>
                                <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                    <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                    <div id="start" class="file-start">
                                        <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                        <span id="file-upload-btn" class="icon-upload-file">
                                            <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                        </span>
                                        <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                    </div>
                                </label>
                                {{--@if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) && \App\Consts::SAMPLE_FILE_LINK[$value]['CSV'])
                                    <p class="modal-text"> サンプルファイルをCSV形式でダウンロードする - <a
                                            href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) }}"
                                            download class="btn-download-file">こちらで</a></p>
                                @endif
                                @if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) && \App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL'])
                                    <p class="modal-text" style="margin-top: 9px !important;"> サンプルファイルをExcel形式でダウンロードする -  <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) }}" download class="btn-download-file">こちらで</a></p>
                                @endif --}}

                            </div>
                        </div>
                        <div id="listFiles"></div>
                        <div id="listFileError" class="d-none">
                            {{--                            <div class='item-error-file row'>--}}
                            {{--                                <div class='col-12 file-response item-error'>--}}
                            {{--                                    <img class='icon-upload' src='{{ asset('icons/document-upload-error.svg') }}' alt='aa'>--}}
                            {{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class='col-12 file-response item-error-message'>--}}
                            {{--                                    <img class='icon-upload' src='{{ asset('icons/danger.svg') }}' alt='aa'>--}}
                            {{--                                    <div class='messages-file'><strong>error</strong></div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="item-error-file row">--}}
                            {{--                                <div class="col-12 file-response item-error">--}}
                            {{--                                    <img class="icon-upload" src="{{ asset('icons/document-upload-error.svg') }}" alt="aa">--}}
                            {{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 file-response item-error-message">--}}
                            {{--                                    <img class="icon-upload" src="{{ asset('icons/danger.svg') }}" alt="aa">--}}
                            {{--                                    <div class='messages-file'><strong>error</strong></div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                        <div id="text-file-pass" class="d-none">
                            アップロードファイル一覧
                        </div>
                        <div class="list-file-pass d-none">
                            <div id="list-file-pass" class="d-none"></div>
                        </div>
                        <div class="file-uploaded row">
                            <div class="col-12 text-left file-sample" id="textSample">
                                各チェックのアップロード可能ファイリ名称サンプル
                            </div>
                            <div class="col-12 text-left file-title" id="file1">
                                請求計算確認表 ファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file1Sample">
                                請求計算確認表
                            </div>
                            <div class="col-12 text-left file-title" id="file2">
                                Sファイル:
                            </div>
                            <div class="col-12 text-left file-name-sample" id="file2Sample">
                                XXXXsXXX
                            </div>
                        </div>
                        <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button" disabled>チェック</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUploadID2" tabindex="-1" role="dialog" aria-labelledby="modalUploadID2Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                        <form id="file-upload-form-no2"  action="{{ route('check-1') }}"
                              method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                            @csrf
                            <input type="hidden" name="settingMonth"
                                   value="{{$data['settingMonth'] ? $data['settingMonth']->id : ''}}">
                            <input type="hidden" name="facilityId" value="{{$data['facilityId']}}">
                            <input type="hidden" name="month" value="{{ $data['month']}}">
                            <input type="hidden" name="type" value="2">
                            <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file[]" accept=".xls, .xlsx, .csv" multiple/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <div id="notimage" class="hidden notimage">ファイルを選択してください。</div>
                                            <span id="file-upload-btn" class="icon-upload-file">
                                            <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                        </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                    {{--@if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) && \App\Consts::SAMPLE_FILE_LINK[$value]['CSV'])
                                        <p class="modal-text"> サンプルファイルをCSV形式でダウンロードする - <a
                                                href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['CSV']) }}"
                                                download class="btn-download-file">こちらで</a></p>
                                    @endif
                                    @if(isset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) && \App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL'])
                                        <p class="modal-text" style="margin-top: 9px !important;"> サンプルファイルをExcel形式でダウンロードする -  <a href="{{ asset(\App\Consts::SAMPLE_FILE_LINK[$value]['EXCEL']) }}" download class="btn-download-file">こちらで</a></p>
                                    @endif --}}

                                </div>
                            </div>
                            <div id="listFileError" class="d-none">
                            </div>
                            <div id="list-file-pass" class="d-none"></div>
                            <div id="listFiles"></div>
                            <div class="file-uploaded row">
                                <div class="col-12 text-left file-sample mb-15" id="textSample">
                                    各チェックのアップロード可能ファイリ名称サンプル<br/>
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    未収金明細表 ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    未収金明細表
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    請求計算確認表 ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    請求計算確認表
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    請求計算確認表（介護保険外) ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    請求計算確認表(介護保険外)
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    送り出し指示データ一覧 ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    送り出し指示データ一覧
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    総合出力 ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    総合出力
                                </div>
                            </div>
                            <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button-no-2" disabled>チェック</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="modalUploadID8" tabindex="-1" role="dialog" aria-labelledby="modalUploadID8Title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
            <div class="modal-content home-modal-content">
                <div class="modal-header home-modal-header">
                    <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">アップロードファイル</h5>
                    <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                    </button>
                </div>
                <div class="modal-body home-modal-body">
                    <!-- Upload  -->
                    <div class="uploader">
                        <div id="error" class="text-danger font-weight-bold" style="padding-bottom: 10px;"></div>
                        <form id="file-upload-form-no8"  action="{{ route('check-4') }}"
                              method="post" enctype="multipart/form-data" class ="file-upload-form-no outer-modal-single">
                            @csrf
                            <input type="hidden" name="settingMonth"
                                   value="{{$data['settingMonth'] ? $data['settingMonth']->id : ''}}">
                            <input type="hidden" name="facilityId" value="{{$data['facilityId']}}">
                            <input type="hidden" name="month" value="{{ $data['month']}}">
                            <input type="hidden" name="type" value="4">
                            <div class="multiple-file-upload">
                                <div class="item-file-upload">
                                    <input id="file-upload" type="file" name="file" accept=".xls, .xlsx, .csv"/>
                                    <label onclick="$(this).parent('div').find('#file-upload').click()" id="file-drag" class="modal-body file-upload" style="margin-bottom: 20px">
                                        <img id="file-image" src="#" alt="Preview" class="hidden file-image">
                                        <div id="start" class="file-start">
                                            <span id="file-upload-btn" class="icon-upload-file">
                                                <img class="mb-30" src="{{ asset('icons/upload.svg') }}" alt="upload">
                                            </span>
                                            <p class="modal-text"> こちらにファイルをアップロードしてください。</p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div id="listFiles"></div>
                            <div id="listFileError" class="d-none">
                                {{--                            <div class='item-error-file row'>--}}
                                {{--                                <div class='col-12 file-response item-error'>--}}
                                {{--                                    <img class='icon-upload' src='{{ asset('icons/document-upload-error.svg') }}' alt='aa'>--}}
                                {{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class='col-12 file-response item-error-message'>--}}
                                {{--                                    <img class='icon-upload' src='{{ asset('icons/danger.svg') }}' alt='aa'>--}}
                                {{--                                    <div class='messages-file'><strong>error</strong></div>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                                {{--                            <div class="item-error-file row">--}}
                                {{--                                <div class="col-12 file-response item-error">--}}
                                {{--                                    <img class="icon-upload" src="{{ asset('icons/document-upload-error.svg') }}" alt="aa">--}}
                                {{--                                    <div class='messages-file'><strong>aaaa</strong></div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col-12 file-response item-error-message">--}}
                                {{--                                    <img class="icon-upload" src="{{ asset('icons/danger.svg') }}" alt="aa">--}}
                                {{--                                    <div class='messages-file'><strong>error</strong></div>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                            </div>
                            {{--                        <div id="text-file-pass" class="d-none">--}}
                            {{--                            アップロードファイル一覧--}}
                            {{--                        </div>--}}
                            {{--                        <div class="list-file-pass d-none">--}}
                            {{--                            <div id="list-file-pass" class="d-none"></div>--}}
                            {{--                        </div>--}}
                            <div class="file-uploaded row">
                                <div class="col-12 text-left file-sample" id="textSample">
                                    各チェックのアップロード可能ファイリ名称サンプル
                                </div>
                                <div class="col-12 text-left file-title" id="file1">
                                    おまかせチェック ファイル:
                                </div>
                                <div class="col-12 text-left file-name-sample" id="file1Sample">
                                    おまかせ情報ダウンロード_年月日
                                </div>
                            </div>
                            <button type="submit" class="home-modal-button default-button file-upload-form-button" id="file-upload-form-button" disabled>チェック</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload file: id=ID &aria-labelledby=ID -->
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                        </span>
                </button>
            </div>
            <div class="modal-body home-modal-body">
                <!-- Upload  -->
                <div class="content">
                    <p id="file-name" @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH']) style="margin-bottom: 10px" @endif>Filecheck1.csv</p>
                    <p id="file-name-2" class="hidden" >Filecheck1.xlsx</p>
                    <span aria-hidden="true">
                            <img src="{{ asset('icons/code-error 1.png') }}" alt="close">
                        </span>
                    <p id="number-file-error">ファイルに<strong>3</strong>つのエラーがあります</p>
                    @if(request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_10 || request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_4)
                        <p id="notification">エラー内容を確認した上で、エラーが問題ない場合は下記の「OK」ボタンを押下してチェック機能を完了にしてください。エラー内容が問題ならば、下記の「NG」ボタンを押下して再度修正したファイルをアップロードしてチェック機能を実行してください。</p>
                    @endif
                </div>
                <div id="error" class="text-danger font-weight-bold"></div>
                <div id="success" class="text-success font-weight-bold"></div>
                    <div class="uploader">
                        <form id="send-mail-form" action="{{route('file_check.reserve')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row" id="facilities">
                                <ul class="list-error-file text-left" id="list-error-facility">
                                </ul>
                                @if(request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_10 || request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_4)
                                <div class="table-responsive mt-5">
                                    <table class=" table text-nowrap max table-bordered" id="list-error-10">
                                        <thead>
                                        <tr>
                                            <th scope="col" style="white-space: nowrap;">請求年月</th>
                                            <th scope="col" style="white-space: nowrap;">法人番号</th>
                                            <th scope="col" style="white-space: nowrap;">法人名</th>
                                            <th scope="col" style="white-space: nowrap;">事業所コード</th>
                                            <th scope="col" style="white-space: nowrap;">事業所名</th>
                                            <th scope="col" style="white-space: nowrap;">ｻｰﾋﾞｽ提供年月</th>
                                            <th scope="col" style="white-space: nowrap;"> 利用者名</th>
                                            <th scope="col" style="white-space: nowrap;">利用者名ｶﾅ</th>
                                            <th scope="col" style="white-space: nowrap;">利用者ID</th>
                                            <th scope="col" style="white-space: nowrap;">被保険者番号</th>
                                            <th scope="col" style="white-space: nowrap;">計画単位数</th>
                                            <th scope="col" style="white-space: nowrap;">限度額管理対象単位数</th>
                                            <th scope="col" style="white-space: nowrap;">限度額管理対象外単位数</th>
                                            <th scope="col" style="white-space: nowrap;">給付単位数</th>
                                            <th scope="col" style="white-space: nowrap;">公費分単位数</th>
                                            <th scope="col" style="white-space: nowrap;">単位数単価</th>
                                            <th scope="col" style="white-space: nowrap;">給付率</th>
                                            <th scope="col" style="white-space: nowrap;">公費給付率</th>
                                            <th scope="col" style="white-space: nowrap;">①自治体請求額(②+③+⑦)</th>
                                            <th scope="col" style="white-space: nowrap;">②公費請求額</th>
                                            <th scope="col" style="white-space: nowrap;">③事業請求額</th>
                                            <th scope="col" style="white-space: nowrap;">④利用者請求額(⑤+⑥-⑦)</th>
                                            <th scope="col" style="white-space: nowrap;">⑤利用者負担額</th>
                                            <th scope="col" style="white-space: nowrap;">⑥利用者超過額</th>
                                            <th scope="col" style="white-space: nowrap;">⑦市区町村減免額</th>
                                            <th scope="col" style="white-space: nowrap;">⑧自治体請求額(処遇改善加算を除く)</th>
                                            <th scope="col" style="white-space: nowrap;">⑨利用者請求額(処遇改善加算を除く)</th>
                                            <th scope="col" style="white-space: nowrap;">⑩処遇改善加算額</th>
                                            <th scope="col" style="white-space: nowrap;">⑪特定処遇改善加算額</th>
                                            <th scope="col" style="white-space: nowrap;">A 請求額合計(①+④)</th>
                                            <th scope="col" style="white-space: nowrap;">B 請求額合計(⑧+⑨+⑩+⑪)</th>
                                            <th scope="col" style="white-space: nowrap;">端数(A-B)</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                                @elseif(request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_8)
                                    <div class="table-responsive mt-5">
                                        <table id="list-error-10" class=" table text-nowrap max table-bordered" border="0" cellpadding="0" cellspacing="0" style="width:1083px;" width="1081">
                                            <colgroup>
                                                <col />
                                                <col span="2" />
                                                <col />
                                                <col />
                                                <col span="2" />
                                                <col />
                                                <col />
                                                <col />
                                                <col span="3" />
                                            </colgroup>
                                            <thead>
                                            <tr height="15">
                                                <th height="69" rowspan="2" style="height:69px;width:77px;text-align: center;vertical-align: middle;">サービス<br />
                                                    提供年月</th>
                                                <th colspan="2" style="width:144px; text-align: center;vertical-align: middle;">送り出し指示</th>
                                                <th rowspan="2" style="width:65px;text-align: center;vertical-align: middle;">利用者ＩＤ</th>
                                                <th rowspan="2" style="width:239px;text-align: center;vertical-align: middle;">利用者名前</th>
                                                <th rowspan="2" style="width:56px;text-align: center;vertical-align: middle;">交換<br />
                                                    識別番号</th>
                                                <th rowspan="2" style="width:56px;text-align: center;vertical-align: middle;">保険者<br />
                                                    番号</th>
                                                <th rowspan="2" style="width:68px;text-align: center;vertical-align: middle;">被保険者<br />
                                                    番号</th>
                                                <th rowspan="2" style="width:104px;text-align: center;vertical-align: middle;">生年月日</th>
                                                <th rowspan="2" style="width:41px;text-align: center;vertical-align: middle;">性別</th>
                                                <th rowspan="2" style="width:77px;text-align: center;vertical-align: middle;">請求種別</th>
                                                <th colspan="2" style="width:155px;text-align: center;vertical-align: middle;">自動発送区分</th>
                                            </tr>
                                            <tr height="54">
                                                <th height="54" style="height:54px;text-align: center;vertical-align: middle;">国保連</th>
                                                <th style="text-align: center;vertical-align: middle;">利用者</th>
                                                <th style="width:77px;text-align: center;vertical-align: middle;">請求書</th>
                                                <th style="text-align: center;vertical-align: middle;">領収書</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="container-checkbox" id="check-send-mail">
                                    <input type="checkbox" id="check_reserve" name="check_reserve">
                                    <label for="check_reserve">保留</label>
                                </div>
                            </div>
                            <div class="row" id="title-send-mail">
                                <h4>保留理由</h4>
                            </div>
                            <div class="row">
                                <textarea name="reason" id="reason" style="resize:none" disabled maxlength="150"
                                          placeholder="理由を入力してください"></textarea>
                                <div id="reason-error" class="text-danger mt-2 text-left"></div>
                            </div>
                            @if(request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_10 || request('type_check') == \App\Consts::TYPE_CHECK_NUMBER_4)
                                <div class="row justify-content-between">
                                    <button type="button" id="button-success"
                                            class="home-modal-button default-button w-205" name="button"
                                            value="done-check">
                                        NG
                                    </button>
                                    <button type="submit" name="button" id="file-upload-form-button2"
                                            class="home-modal-button default-button w-205" value="ok-check">OK
                                    </button>
                                </div>
                            @else
                                <div class="row justify-content-between">
                                    <button type="button" id="file-upload-form-button2"
                                            class="home-modal-button default-button">閉じる
                                    </button>
                                </div>
                            @endif
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered home-modal-dialog" role="document">
        <div class="modal-content home-modal-content">
            <div class="modal-header home-modal-header">
                <h5 class="modal-title home-modal-title" id="exampleModalLongTitle">チェックファイル</h5>
                <button type="button" class="close home-modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="{{ asset('icons/close-circle.svg') }}" alt="close">
                    </span>
                </button>
            </div>
            <div class="modal-body home-modal-body content">
                <p id="file-name">Filecheck1.csv</p>
                <p id="file-name-2" class="hidden" >Filecheck1.xlsx</p>
                <img class="home-modal-icon" src="{{ asset('icons/double-check-success.svg') }}" alt="question-mark">
                <p class="modal-text"> ファイルにエラーはありません。</p>
                <button type="button" class="home-modal-button default-button" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>
<!-- scrip -->
<!-- multiple -->
@if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'])
    <script type="text/javascript" src="{{ asset('js/uploadFileCSV.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadFileExcel.js') }}"></script>
@else
    <script type="text/javascript" src="{{ asset('js/uploadFile.js') }}"></script>

@endif
    <script type="text/javascript" src="{{ asset('js/uploadFile2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadType1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploadFile4.js') }}"></script>
    <!-- multiple -->
<script>
    const container = document.getElementById("wrapper-toggle-btn")
    const toggleInput = document.getElementById("toggle-input")
    const toggleBtn = document.getElementById("toggle-btn")
    const modalQuestion = document.getElementById("modalQuestion")

    toggleInput.addEventListener("click", () => {
        if(toggleInput.checked) {
            toggleBtn.style.marginLeft = "16px"
            toggleBtn.style.backgroundColor = "#0F94B5"
            container.style.backgroundColor = "#B5E5F0"
            modalQuestion.addClass("show")
        }
        else {
            toggleBtn.style.marginLeft = "-2px"
            toggleBtn.style.backgroundColor = "#F5F7FB"
            container.style.backgroundColor = "#7c9aa1"
            modalQuestion.removeClass("show")
        }
    })
</script>
<script>
    function loadDataFacility(select)
    {
        var departmentId = select.value;
        $.ajax({
            url: '{{route('get-list-facilyties')}}',
            type: 'POST',
            data: {
                department_id : departmentId
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            dataType: "json",
        }).done(function (data) {
            var html = '';
            var facilityList = data.data;
            $.each(facilityList, function(key, value) {
                html+= '<option value="' + value.id + '">' + value.name + '</option>';
            });
            $('select[name="facility_id"]').html(html);
            $('select[name="facility_id"]').select2({
                language: {
                    inputTooShort: function (args) {

                        return "任意の文字を入力してください。。。";
                    },
                    noResults: function () {
                        return "見つかりません。。。";
                    },
                    searching: function () {
                        return "検索しています。。。";
                    }
                },
            });
        });
    }
    $(document).ready(function () {
        $('.select2').select2({
            language: {
                inputTooShort: function (args) {
                    return "任意の文字を入力してください。。。";
                },
                noResults: function () {
                    return "見つかりません。。。";
                },
                searching: function () {
                    return "検索しています。。。";
                }
            },
        });
        $(document).on('click', '#pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getData(page);
        });
    });

    function getData(page) {
        // body...
        $.ajax({
            url: '?page=' + page,
            type: 'get',
            data: {
                type_check: "{{request('type_check')}}",
                month: "{{$data['month']}}"
            },
            datatype: 'html',
        }).done(function (data) {
            $('#pagination').remove();
            $('#detail-check').append(data)
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        });
    }
    $('#modalUploadID1').on('hidden.bs.modal', function () {
        $('#messages-excel').empty();
        $('#response-excel').addClass('hidden ');
        $('#file-upload-form-button').prop('disabled', true);
        $('#messages-csv').empty();
        $('#response-csv').addClass('hidden ');
        $('#messages').empty();
        $('#response').addClass('hidden ');
        $(this).find('#error').text('');
    })
    $('#modalUploadID1').on('shown.bs.modal', function (e) {
        $('input[name="file"]').val('');
        @if(\App\Consts::TYPE_CHECK_UPLOAD[request('type_check')] == App\Consts::UPLOAD_TYPE['BOTH'])
        ekUploadCsv('#modalUploadID1');
        ekUploadExcel('#modalUploadID1');
        @else
        ekUpload('#modalUploadID1');
        @endif
    });
    $('#modalUploadID5').on('shown.bs.modal', function (e) {
        $('input[name="file"]').val('');
        ekUpload2('#modalUploadID5');
    });
    $('#modalUploadID8').on('shown.bs.modal', function (e) {
        $('input[name="file"]').val('');
        ekUpload4('#modalUploadID8');
    });
    $('#check_reserve').change(function (event){
        $("#modalError #reason-error").empty();
        if(this.checked) {
            $('#reason').removeAttr('disabled');
            $('#file-upload-form-button2').attr('disabled', 'disabled');
            $('#button-success').attr('disabled', 'disabled');
            $('#reason').on('input', function (event){
                if($(this).val().length > 0) {
                    $('#file-upload-form-button2').removeAttr('disabled');
                } else {
                    $('#file-upload-form-button2').attr('disabled', 'disabled');
                }
            })
        } else {
            $('#reason').attr('disabled', 'disabled');
            $('#file-upload-form-button2').removeAttr('disabled');
            $('#button-success').removeAttr('disabled');
            $('#reason').val("");
        }
    })
    $("#file-upload-form-button").click(function (e) {
        var thisModal = $(this).closest('.modal');
        $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
        e.preventDefault();
        var formData = new FormData($('#file-upload-form')[0]);
        $.ajax({
            url: $('#file-upload-form').attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            $("#preloader").remove();
            $('#modalUploadID1').modal('hide');
            $('.modal-backdrop').remove();
            $('#modalUploadID1 #error').empty();
            if (data.data.errors.length > 0){
                var html = '';
                $.each(data.data.errors, function (key, value) {
                    html += ' <li class="item-error-file">\n' +
                        '                 <label for="id' + key + '">' + value['error_position'] + '：' + value['message'] + '</label>\n' +
                        '    </li>'
                });
                var tbody = '';
                $.each(data.data.dataErrors, function (key, data) {
                    var tr = '';
                    $.each(data, function (index, value) {
                        if (value) {
                            tr += '<td scope="row">' + value + '</td>';
                        } else {
                            tr += '<td scope="row"></td>';
                        }
                    })
                    tbody +='<tr>' + tr + '</tr>'
                });
                $('#list-error-10 tbody').empty();
                $('#list-error-10 tbody').append(tbody);
                $('#list-error-facility').empty();
                $('#list-error-facility').append(html);
                $('#number-file-error strong').text(data.data.errors.length);
                $('#file_check').remove();
                $("#modalError #success").empty();
                $("#modalError #error").empty();
                if(typeof data.data.file_name['csv'] != "undefined" && typeof data.data.file_name['excel'] != "undefined") {
                    $("#modalError #file-name").text(data.data.file_name['csv']);
                    $("#modalError #file-name").css('margin-bottom', '0px!important');
                    $("#modalError #file-name-2").removeClass('hidden');
                    $("#modalError #file-name-2").text(data.data.file_name['excel']);
                } else {
                    $("#modalError #file-name").text(data.data.file_name);
                }
                $('#send-mail-form').append("<input type='hidden' id='type_check_id' name='type_check_id' value='" + data.data.type_check_id + "'>");
                if(typeof data.data.file_id['csv'] != "undefined") {
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['csv'] + "'>");
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['excel'] + "'>");
                } else {
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                }
                $('#modalError').modal({backdrop: 'static', keyboard: false})
                $('#modalError').modal('show');
            } else {
                $('#modalSuccess').modal({backdrop: 'static', keyboard: false})
                if(typeof data.data.file_name['csv'] != "undefined") {
                    $("#modalSuccess #file-name").text(data.data.file_name['csv']);
                    $("#modalSuccess #file-name-2").removeClass('hidden');
                    $("#modalSuccess #file-name-2").text(data.data.file_name['excel']);
                } else {
                    $("#modalSuccess #file-name").text(data.data.file_name);
                }
                $('#modalSuccess').modal('show');
            }
        }).fail(function (jqXHR) {
            $("#preloader").remove();
            if (jqXHR.responseJSON.validator == true) {
                var messages = jqXHR.responseJSON.message
                var text = '';
                $.each(messages, function (index, value) {
                    text += value + "</br>";
                });
                thisModal.find("#error").html(text)
            } else {
                thisModal.find("#error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
            }
        });
    });
    $("#send-mail-form button").click(function (event) {
        var checked = $('#check_reserve').prop("checked");
        if (checked || $(this).val() == 'ok-check') {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var formData = new FormData($('#send-mail-form')[0]);
            formData.append('button', $(this).val());
            $("#modalError #success").empty();
            $("#modalError #error").empty();
            $("#modalError #reason-error").empty();
            $.ajax({
                url: $('#send-mail-form').attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR) {
                $("#preloader").remove();
                if (jqXHR.responseJSON.validator == true) {
                    var messages = jqXHR.responseJSON.message
                    var text = '';
                    if (typeof messages.reason !== "undefined" || messages.facility !== null) {
                        var textErrors = '';
                        $.each(messages.reason, function (index, value) {
                            textErrors += value + "</br>";
                        });
                        $("#modalError #reason-error").html(textErrors)
                    }
                } else {
                    $("#modalError #error").text("{{App\Messages::FILE_TEMPLATE_ERROR}}")
                }
            });
        } else {
            location.reload();
        }
    });
    $(document).ready(function() {
        var date=new Date();
        var year=date.getFullYear()
        var month=date.getMonth();
        $("#datepicker input").datepicker({
            format: "yyyy/mm",
            startView: "months",
            minViewMode: "months",
            autoclose:true,
            language: 'ja',
            endDate: new Date(year, month, '01')
        });
        $('#modalError .close.home-modal-close').click(function (){
            location.reload();
        })
        $('.icon-datepicker').on('click', function () {
            $('#datepicker input').trigger('focus');
        })

        $('.modal').on('hidden.bs.modal', function () {
            var thisModal = $(this).closest('.modal');
            thisModal.find('[type=file]').val('');
            thisModal.find('#messages-excel').empty();
            thisModal.find('#messages-csv').empty();
            thisModal.find('.file-response').addClass('hidden');
            thisModal.find("#notimage").addClass("hidden");
            thisModal.find("#notimage-csv").addClass("hidden");
            thisModal.find("#notimage-excel").addClass("hidden");
            thisModal.find("#error").text(' ');
            $(".modal #success").empty();
            $(".modal #error").empty();
        });
        $('#modalSuccess button').click(function (){
            location.reload()
        })
        $('.modal').on('show.bs.modal', function () {
            $(".modal #success").empty();
            $(".modal #error").empty();
        })
        $("#file-upload-form-no5").submit(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            $('#modalError').find('.file-name').remove();
            var formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                thisModal.modal('hide');
                var file = '';
                if (data.data.errors.length > 0){
                    var html = '';
                    let fileId = '';
                    $.each(data.data.errors, function (key, value) {
                        html += ' <li class="item-error-file">\n' +
                            '                 <label for="id' + key + '">' + value['error_position'] + '：' + value['message'] + '</label>\n' +
                            '    </li>'
                    });
                    var tbody = '';
                    $('#list-error-facility').empty();
                    $('#list-error-facility').append(html);
                    $('#number-file-error strong').text(data.data.errors.length);
                    $('#file_check').remove();
                    $("#modalError #success").empty();
                    $("#modalError #error").empty();
                    // if(typeof data.data.file_name['csv'] != "undefined") {
                    //     $("#modalError #file-name").text(data.data.file_name['csv']);
                    //     $("#modalError #file-name-2").removeClass('hidden');
                    //     $("#modalError #file-name-2").text(data.data.file_name['excel']);
                    // } else {
                    //     $("#modalError #file-name").text(data.data.file_name);
                    // }
                    $.each(data.data.files, function (key, value) {
                        file += "<p class='file-name'>" + value['file_name'] + '</p>';
                        fileId += "<input type='hidden' name='file_id[]' value='" + value['id'] + "'>";
                    })
                    $('#modalError #file-name').empty();
                    $('#modalError .content').prepend(file)
                    $('#send-mail-form').append("<input type='hidden' id='type_check_id' name='type_check_id' value='" + data.data.type_check_id + "'>");
                    $('#send-mail-form').append(fileId);
                    // if(typeof data.data.file_id['csv'] != "undefined") {
                    //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['csv'] + "'>");
                    //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['excel'] + "'>");
                    // } else {
                    //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                    // }
                    var type = thisModal.find('input[name="type"]').val();

                    if($('#modalError .hidden-type-check ').length < 2 ) {
                        $('#modalError .table-responsive ').addClass('hidden-type-check');
                        $('#modalError #notification ').addClass('hidden-type-check');
                    }
                    $('#modalError').modal('show');
                } else {
                    $.each(data.data.files, function (key, value) {
                        file += "<p class='file-name'>" + value['file_name'] + '</p>'
                    })
                    $('#modalSuccess #file-name').empty();
                    $('#modalSuccess .content').prepend(file)
                    $('#modalSuccess').modal('show');
                }
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                thisModal.find('#listFiles').empty();
                var response = jqXHR.responseJSON;
                if (response.validator === true) {
                    var messages = response.message
                    var files = response.file
                    var html = '';
                    var filePass = '';
                    var errors = [];
                    var fileName = '';
                    var errorText = '';
                    var hasFilePass = false;
                    $.each(messages, function (index, value) {
                        var error = index.split('.');
                        if (error.length > 1) {
                            fileName = files[error[1]];
                            errorText = value
                            errors.push(error[1])
                        } else {
                            error = (value + '').split('/');
                            fileName = files[error[0]];
                            errorText = error[1];
                            errors.push(error[0])
                        }
                        html += "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + fileName +"</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>" + errorText + "</div>" +
                            "</div>" +
                            "</div>"
                    });
                    for (var i = 0, f; (f = files[i]); i++) {
                        var filePassType = '';
                        if (!(errors.includes(i.toString()))) {
                            if (f.startsWith('総合事業計上漏れチェック-請求計算確認表')) {
                                filePassType = '請求計算確認表 ファイル:'
                            } else if (f.startsWith('総合事業計上漏れチェック-S')) {
                                filePassType = 'Sファイル:'
                            } else if (f.startsWith('総合事業計上漏れチェック-請求前確認リスト')) {
                                filePassType = '会計締め前確認リストファイル:'
                            }
                            hasFilePass = true;
                            filePass += "<div class='file-pass'>" +
                                "<div class='file-type'>" + filePassType + "</div>" +
                                "<div class='file-response file-response-2 file-name-pass'>" +
                                "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                                "<div class='messages-file'>" +
                                "<strong>" + f + "</strong>" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                        }
                    }
                    thisModal.find("#listFileError").removeClass('d-none')
                    thisModal.find("#listFileError").append(html)
                    if (hasFilePass) {
                        thisModal.find("#text-file-pass").removeClass('d-none')
                        thisModal.find(".list-file-pass").removeClass('d-none')
                        thisModal.find("#list-file-pass").removeClass('d-none')
                        thisModal.find("#list-file-pass").empty()
                        thisModal.find("#list-file-pass").append(filePass)
                    }
                    thisModal.find("#file-upload-form-button").attr("disabled", true);

                } else {
                    thisModal.find("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                }
                $("#preloader").remove();
            });
        });
        $("#file-upload-form-no8").submit(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            $('#modalError').find('.file-name').remove();
            var formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                thisModal.modal('hide');
                var file = '';
                if (data.data.errors.length > 0){
                    var html = '';
                    let fileId = '';
                    $.each(data.data.errors, function (key, value) {
                        html += ' <li class="item-error-file">\n' +
                            '                 <label for="id' + key + '">' + value['error_position'] + '：' + value['message'] + '</label>\n' +
                            '    </li>'
                    });
                    var tbody = '';
                    $('#list-error-facility').empty();
                    $('#list-error-facility').append(html);
                    $('#number-file-error strong').text(data.data.errors.length);
                    $('#file_check').remove();
                    $("#modalError #success").empty();
                    $("#modalError #error").empty();
                    $('#modalError #file-name').empty();
                    $('#modalError .content').prepend(file)
                    $("#modalError #file-name").text(data.data.file_name);
                    $('#send-mail-form').append("<input type='hidden' id='type_check_id' name='type_check_id' value='" + data.data.type_check_id + "'>");
                    $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                    var type = thisModal.find('input[name="type"]').val();
                    if($('#modalError .hidden-type-check ').length < 2 ) {
                        $('#modalError .table-responsive ').addClass('hidden-type-check');
                        $('#modalError #notification ').addClass('hidden-type-check');
                    }
                    $('#list-error-10 thead tr').empty();
                    let head = ''
                    let body = ''
                    $.each(data.data.column, function (key, value) {
                        head += '<th scope="col" style="white-space: nowrap;">' + value + '</th>'
                    })
                    $.each(data.data.errorData, function (key, data) {
                        let tr = ''
                        $.each(data, function (index, value) {
                            if (value) {
                                tr += '<td scope="row" style="white-space: nowrap">' + value + '</td>';
                            } else {
                                tr += '<td scope="row"></td>';
                            }
                        })
                        body += '<tr>' + tr + '</tr>'
                    });
                    $('#list-error-10 thead tr').append(head);
                    $('#list-error-10 tbody').append(body);
                    if(type == {{\App\Consts::TYPE_CHECK_NUMBER_4}}) {
                        let noti = $('#modalError #notification ')
                        $('#modalError .table-responsive ').removeClass('hidden-type-check')
                        noti.removeClass('hidden-type-check');
                        noti.css('text-align', 'left')
                    }
                    $('#modalError').modal('show');
                } else {
                    $("#modalSuccess #file-name").text(data.data.file_name);
                    $('#modalSuccess').modal('show');
                }
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                thisModal.find('#listFiles').empty();
                var response = jqXHR.responseJSON;
                if (response.validator === true) {
                    var messages = response.message
                    let html = '';
                    $.each(messages, function (index, value) {
                        html = "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + response.file +"</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>" + value + "</div>" +
                            "</div>" +
                            "</div>"
                    });
                    thisModal.find("#listFileError").removeClass('d-none')
                    thisModal.find("#listFileError").append(html)
                    thisModal.find("#file-upload-form-button").attr("disabled", true);
                } else {
                    thisModal.find("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                }
                $("#preloader").remove();
            });
        });
        $("#file-upload-form-no2").submit(function (event) {
            $("body").prepend('<div id=\'preloader\' style=\'position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;z-index: 99999999; background: url("{{ asset('icons/loading.gif') }}") center no-repeat #ffffff;\'></div>');
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            var thisModal = $(this).closest('.modal');
            thisModal.find("#listFileError").empty();
            thisModal.find("#listFileError").addClass('d-none');
            thisModal.find("#list-file-pass").empty();
            thisModal.find(('#list-file-pass')).addClass('d-none');
            thisModal.find("#error").empty();
            thisModal.find(('#error')).addClass('d-none');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                thisModal.modal('hide');
                if (data.data.errors.length > 0) {
                    var html = '';
                    var file = '';
                    let count = 0;
                    $.each(data.data.errors, function (key, value) {
                        value['message'] = value['message'] != ' ' ? ': '+ value['message'] : '';
                        if (value['type'] != undefined && value['type'] == '{{\App\Consts::TYPE_WARNING}}') {
                            html += ' <li class="item-error-file warning">\n' +
                                '                 <label for="id' + key + '">' + value['error_position'] + value['message'] + '</label>\n' +
                                '    </li>';
                            count++;
                        } else {
                            html += ' <li class="item-error-file">\n' +
                                '                 <label for="id' + key + '">' + value['error_position'] + value['message'] + '</label>\n' +
                                '    </li>';
                            count++;
                        }
                    });
                    var tbody = '';
                    $('#list-error-facility').empty();
                    $('#list-error-facility').append(html);
                    $('#number-file-error strong').text(data.data.errors.length);
                    $('#file_check').remove();
                    $("#modalError #success").empty();
                    $("#modalError #error").empty();
                    // if(typeof data.data.file_name['csv'] != "undefined") {
                    //     $("#modalError #file-name").text(data.data.file_name['csv']);
                    //     $("#modalError #file-name-2").removeClass('hidden');
                    //     $("#modalError #file-name-2").text(data.data.file_name['excel']);
                    // } else {
                    //     $("#modalError #file-name").text(data.data.file_name);
                    // }
                    if (count) {
                        $.each(data.data.files, function (key, value) {
                            file += "<p class='file-name'>" + value['name'] + '</p>'
                            if (key == 1) {
                                $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + value['id'] + "'>");
                            }
                        })
                        $('#modalError #file-name').empty();
                        $('#modalError .content').prepend(file)
                        $('#send-mail-form').append("<input type='hidden' id='type_check_id' name='type_check_id' value='" + data.data.type_check_id + "'>");
                        $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                        // if(typeof data.data.file_id['csv'] != "undefined") {
                        //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['csv'] + "'>");
                        //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id[]' value='" + data.data.file_id['excel'] + "'>");
                        // } else {
                        //     $('#send-mail-form').append("<input type='hidden' id='file_id' name='file_id' value='" + data.data.file_id + "'>");
                        // }
                        $('#modalError').modal('show');
                    } else {
                        $.each(data.data.files, function (key, value) {
                            file += "<p class='file-name'>" + value['name'] + '</p>'
                        })
                        $('#modalSuccess #file-name').empty();
                        $('#modalSuccess .content').prepend(file)
                        $('#modalSuccess').modal('show');
                    }
                } else {
                    ('#modalSuccess').modal('show');
                }
                $("#preloader").remove();
            }).fail(function (jqXHR) {
                thisModal.find('#listFiles').empty();
                thisModal.find("#listFileError").empty();
                thisModal.find(('text-file-pass')).addClass('d-none')
                var response = jqXHR.responseJSON;
                var files = response.file;
                var html = '';
                var filePass = '';
                var errors = [];
                var fileName = '';
                var errorText = '';
                var hasFilePass = false;
                if (response.validator === true) {
                    if (response.message.file !== undefined) {
                        let errorMissFile = jqXHR.responseJSON.message.file;
                        $.each(errorMissFile, function (i, fileMiss) {
                            html += fileMiss + '<br/>';
                        });
                        $('#modalUploadID2 #error').html(html);
                        thisModal.find(('#error')).removeClass('d-none');
                    }
                    delete response.message.file;
                    html = '';
                    $.each(response.message, function (index, value) {
                        var error = index.split('.');
                        if (error.length > 1) {
                            fileName = files[error[1]];
                            errorText = value
                            errors.push(error[1])
                        } else {
                            error = (value + '').split('/');
                            fileName = files[error[0]];
                            errorText = error[1];
                            errors.push(error[0])
                        }
                        html += "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + fileName +"</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>" + errorText + "</div>" +
                            "</div>" +
                            "</div>"
                    });
                    for (var i = 0, f; (f = files[i]); i++) {
                        var filePassType = '';
                        if (!(errors.includes(i.toString()))) {
                            hasFilePass = true;
                            filePass += "<div class='file-pass'>" +
                                "<div class='file-response file-response-2 file-name-pass'>" +
                                "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                                "<div class='messages-file'>" +
                                "<strong>" + f + "</strong>" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                        }
                    }
                    thisModal.find("#listFileError").append(html);
                    thisModal.find("#list-file-pass").append(filePass);
                    thisModal.find("#list-file-pass").removeClass('d-none');
                    thisModal.find("#listFileError").removeClass('d-none');
                } else {
                    thisModal.find("#error").text("エラーが発生した。テンプレートファイルを確認し、再度お試しください。")
                    thisModal.find(('#error')).removeClass('d-none');
                }
                $('#modalUploadID2 #file-upload-form-button').prop('disabled', 'disabled');
                $("#preloader").remove();
            });
        });

    })
    $('#modalUploadID2').on('shown.bs.modal', function (e) {
        $('input[name="file"]').val('');
        uploadType1('#modalUploadID2');
    });
</script>
@endsection
