$(function () {
  collapse();
});

function collapse() {
  $(document).ready(function() {
    $('#icon_bars').on('click', function() {
      let displayStype = $('.left-sidebar').css('display') == 'none' ? 'block' : 'none';
      let rightMainWidth = $('.left-sidebar').css('display') == 'none' ? 'calc(100% - 240px)' : '100%';
      $('.left-sidebar').css('display', displayStype);
      $('.right-main').css('width', rightMainWidth);
    })
  })
};
