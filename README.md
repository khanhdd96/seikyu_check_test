## Seikyu Check
### Basic
#### Clone project: 
```sh
    $ git clone ...
```

#### Install project
```sh
    $ composer install
    $ php artisan key:generate
    $ php artisan passport:install
    $ php artisan serve // server start with port 8000
``
