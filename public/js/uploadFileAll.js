function ekUploadAll(modal, month) {
    function Init() {
        var thisModal = $(modal);
        var fileSelect = thisModal.find("#file-upload")[0],
            fileDrag = thisModal.find("#file-drag")[0],
            submitButton = thisModal.find("#submit-button")[0];
        fileSelect.addEventListener("change", function (event) {
            fileSelectHandler(event, month)
        });

        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", function (event) {
                fileSelectHandler(event, month)
            });
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById("file-drag");

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className =
            e.type === "dragover" ? "modal-body file-upload" : "modal-body file-upload";
    }

    function fileSelectHandler(e, month) {
        // Fetch FileList object
        month = month.replaceAll('/', '')
        var files = e.target.files || e.dataTransfer.files;
        var thisModal = $('.modal.fade.show');
        thisModal.find("#error").text('');
        thisModal.find('#listFiles').empty();
        thisModal.find('#listFileError').empty();
        var html = '';
        if (files.length >0) {
            // Cancel event and hover styling
            fileDragHover(e);
            let fileName = {
                '1': '(.*)(請求前確認リスト)(.*)',
                '2_file1' : '(.*)(未収金明細表)(.*)',
                '2_file2' : '(.*)(請求計算確認表)(?!（介護保険外）)(.*)',
                '2_file3' : '(.*)請求計算確認表（介護保険外）(.*)',
                '2_file4' : '(.*)送り出し指示データ一覧-|送り出し指示データ一覧-[\\d]{8}(.*)$',
                '2_file5' : '(.*)(総合出力)(.*)',
                '3' : '(.*)送り出し指示データ一覧-|送り出し指示データ一覧-[\\d]{8}(.*)$',
                '4' : '(.*)(予実明細)(.*)',
                '5_file1' : '(.*)(請求計算確認表)(?!（介護保険外）)(.*)',
                '5_file2' : '(.*)([\\d]{4})s([\\d]{3})(.*)',
                '6_csv' : '(.*)(TH01_)[\\d]{6}(.*)',
                '6_excel' : '(.*)(売上一覧表_保険外)(.*)',
                '7' : '(.*)(トランチェック)[\\d]{10}-[\\d]{8}-(.*)',
                '8' : '(.*)おまかせ情報ダウンロード_|おまかせ情報ダウンロード_[\\d]{8}(.*)',
                '9_file1' : '(.*)([\\d]{4})m([\\d]{3})(.*)',
                '9_file2' : '(.*)(請求計算確認表)(?!（介護保険外）)(.*)',
                '10' : '(.*)([\\d]{4})s([\\d]{3})(.*)',
                '11' : '(.*)(TH01_)[\\d]{6}(.*)',
            };
            let fileExtension = {
                '1' : ['xls', 'xlsx'],
                '2_file1' : ['xls', 'xlsx'],
                '2_file2' : ['xls', 'xlsx'],
                '2_file3' : ['xls', 'xlsx'],
                '2_file4' : ['xls', 'xlsx'],
                '2_file5' : ['xls', 'xlsx', 'csv'],
                '3' : ['xls', 'xlsx'],
                '4' : ['xls', 'xlsx', 'csv'],
                '5_file1' : ['xls', 'xlsx'],
                '5_file2' : ['xls', 'xlsx', 'csv'],
                '6_excel' : ['xls', 'xlsx'],
                '6_csv' : ['csv'],
                '7' : ['xls', 'xlsx', 'csv'],
                '8' : ['xls', 'xlsx', 'csv'],
                '9_file1' : ['csv'],
                '9_file2' : ['xls', 'xlsx'],
                '10' : ['xls', 'xlsx', 'csv'],
                '11' : ['csv'],
            }
            let fileList = [];
            let filePass = [];
            let fileData = {};
            let fileErrorName = [];
            let fileErrorExtension = [];
            let fileDuplicate = [];
            let fileNotEnough = [];
            let hasError = false
            for (var i = 0; i < files.length; i++) {
                fileList.push(files[i].name)
                uploadFile(e, files[i], thisModal);
            }
            for (var j = 0; j < fileList.length; j++) {
                var checked = false
                for (let key in fileName) {
                    var regex = new RegExp(fileName[key])
                    if (regex.test(fileList[j].split('.')[0])) {
                        checked = true
                        if (filePass.indexOf(key) !== -1) {
                            fileDuplicate.push(fileList[j])
                            continue
                        }
                        filePass.push(key)
                        fileData[key] = fileList[j]
                    }
                }
                if (!checked) {
                    fileErrorName.push(fileList[j])
                }
            }
            let count2 = 0;
            let count5 = 0;
            let count6 = 0;
            let count9 = 0;
            const listFunction = {
                '1' : '①請求前確認リスト:',
                '2' : '②請求締めチェックツール:',
                '3' : '③送り出し指示データ一覧:',
                '4' : '④総合事業請求止めチェック機能:',
                '5' : '⑤総合事業計上漏れチェック:',
                '6' : '⑥障害チェック',
                '7' : '⑦トランデータチェック:',
                '8' : '⑧おまかせチェック:',
                '9' : '⑨Mファイルチェック:',
                '10' : '⑩Sファイルチェック:',
                '11' : '⑪TH障害データチェック:'
            }
            let listFile = {
                '1' : [],
                '2' : [],
                '3' : [],
                '4' : [],
                '5' : [],
                '6' : [],
                '7' : [],
                '8' : [],
                '9' : [],
                '10' : [],
                '11' : [],
            }
            for (let keyName in fileData) {
                let valueName = fileData[keyName]
                if (keyName === '3') {
                    let text3 = valueName.indexOf('送り出し指示データ一覧-') !== -1 ? '送り出し指示データ一覧-' : '送り出し指示データ一覧-'
                    let date3 = valueName.substr((valueName.indexOf(text3) + text3.length), 6)
                    if (date3 !== month) {
                        fileErrorName.push(valueName)
                        delete fileData[keyName]
                    }
                }
                if (keyName === '6_csv') {
                    let text6 = 'TH01_'
                    let date6 = valueName.substr((valueName.indexOf(text6) + text6.length), 6)
                    if (date6 !== month) {
                        fileErrorName.push(valueName)
                        delete fileData[keyName]
                    }
                }
                if (keyName === '7') {
                    let text7 = 'トランチェック'
                    let date7 = valueName.substr((valueName.indexOf(text7) + text7.length + 11), 6)
                    if (date7 !== month) {
                        fileErrorName.push(valueName)
                        delete fileData[keyName]
                    }
                }
                if (keyName === '8') {
                    let text8 = valueName.indexOf('おまかせ情報ダウンロード_') !== -1 ? 'おまかせ情報ダウンロード_' : 'おまかせ情報ダウンロード_'
                    let date8 = valueName.substr((valueName.indexOf(text8) + text8.length), 6)
                    if (date8 !== month) {
                        fileErrorName.push(valueName)
                        delete fileData[keyName]
                    }
                }
                if (keyName === '11') {
                    let text11 = 'TH01_'
                    let date11 = valueName.substr((valueName.indexOf(text11) + text11.length), 6)
                    if (date11 !== month) {
                        fileErrorName.push(valueName)
                        delete fileData[keyName]
                    }
                }
            }
            for (let key in fileData) {
                for (let key2 in fileExtension) {
                    if (key === key2 && fileExtension[key2].indexOf(fileData[key].split('.')[1].toLowerCase()) === -1) {
                        fileErrorExtension.push(fileData[key])
                        delete fileData[key]
                        break
                    }
                }
            }
            for (let key3 in fileData) {
                let value = fileData[key3]
                for (let keyFunction in listFunction) {
                    if (key3.split('_')[0] === keyFunction) {
                        listFile[keyFunction].push(value)
                        if (keyFunction === '2') {
                            count2++
                        }
                        if (keyFunction === '5') {
                            count5++
                        }
                        if (keyFunction === '6') {
                            count6++
                        }
                        if (keyFunction === '9') {
                            count9++
                        }
                    }
                }
            }
            if (count2 !== 0 && count2 !== 5) {
                fileNotEnough.push('②請求締めチェックツール')
            }
            if (count5 !== 0 && count5 !== 2) {
                fileNotEnough.push('⑤総合事業計上漏れチェック')
            }
            if (count6 !== 0 && count6 !== 2) {
                fileNotEnough.push('⑥障害チェック')
            }
            if (count9 !== 0 && count9 !== 2) {
                fileNotEnough.push('⑨Mファイルチェック')
            }
            let error = {
                'name' : fileErrorName,
                'extension' : fileErrorExtension,
                'duplicate' : fileDuplicate,
                'notEnough' : fileNotEnough
            }
            if (fileErrorName.length > 0 || fileDuplicate.length > 0 || fileErrorExtension.length > 0 || fileNotEnough.length > 0) {
                hasError = true
                let errorName = ''
                let errorMessage = ''
                for (let keyError in error) {
                    let val = error[keyError]
                    if (keyError === 'name') {
                        errorMessage = 'ファイル名エラー'
                    }
                    if (keyError === 'extension') {
                        errorMessage = 'アップロードされたファイル形式が正しくない'
                    }
                    if (keyError === 'duplicate') {
                        errorMessage = 'ファイルが重複されています。'
                    }
                    if (keyError === 'notEnough') {
                        errorMessage = 'ファイルが不足しています。'
                    }
                    for (var a = 0; a < val.length; a++) {
                        errorName += "<div class='item-error-file row'>" +
                            "<div class='col-12 file-response item-error'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/document-upload-error.svg'" + "alt='aa'>" +
                            "<div class='messages-file'><strong>" + val[a] +"</strong></div>" +
                            "</div>" +
                            "<div class='col-12 file-response item-error-message'>" +
                            "<img class='icon-upload' src='" + location.origin + "/icons/danger.svg'" + "alt='aa'>" +
                            "<div class='messages-file'>" + errorMessage + "</div>" +
                            "</div>" +
                            "</div>"
                    }
                }
                thisModal.find("#listFileError").removeClass('d-none')
                thisModal.find("#listFileError").append(errorName)
            }
            if (Object.keys(fileData).length > 0) {
                let filePass = ''
                for (let keyFn in listFile) {
                    if (listFile[keyFn].length > 0) {
                        let listFilePass = ''
                        listFile[keyFn].forEach(function (value) {
                            listFilePass += "<div class='file-response' style='margin-bottom: 5px; letter-spacing: -0.02em; float: none; clear: none'>" +
                                "<img class='icon-upload' src='" + location.origin + "/icons/document-upload.svg'" + "alt='document-upload'>" +
                                "<div class='messages-file'>" +
                                "<strong>" + value + "</strong>" +
                                "</div>" +
                                "</div>"
                        })
                        filePass += "<div class='file-pass'>" +
                            "<div class='title-all' style='text-align: left; margin-top: 10px'>" + listFunction[keyFn] + "</div>" +
                            listFilePass +
                            "</div>"
                    }
                }
                thisModal.find("#text-file-pass").removeClass('d-none')
                thisModal.find(".list-file-pass").removeClass('d-none')
                thisModal.find("#list-file-pass").removeClass('d-none')
                thisModal.find("#list-file-pass").empty()
                thisModal.find("#list-file-pass").append(filePass)
            }
            thisModal.find('#file-upload-form-all').append("<input type='hidden' id='listFile' name='list_file' value='" + JSON.stringify(listFile) + "'>")
            // Process all File objects
            // for (var i = 0, f; (f = files[i]); i++) {
            //     html += "<div class=\"file-response file-response-2\" id='file" + i + "'>" +
            //         "<img class='icon-upload' src=\"" + location.origin + "/icons/document-upload.svg\"" +
            //         "alt=\"document-upload\">" +
            //         "<div class='messages-file'><strong>" + f.name + "</strong></div>" +
            //         " </div>"
            //     uploadFile(e, f, thisModal);
            // }
            // thisModal.find('#listFiles').append(html);
            // for (var i = 0, f; (f = files[i]); i++) {
            //     parseFile(f, thisModal);
            //     uploadFile(e, f, thisModal);
            // }
            if (!hasError) {
                thisModal.find("#file-upload-form-button").attr("disabled", false);
                // thisModal.find('.list-file-pass').addClass('d-none')
                // thisModal.find('#list-file-pass').addClass('d-none')
            } else {
                thisModal.find("#file-upload-form-button").attr("disabled", true);
            }
        } else {
            thisModal.find("#file-image").addClass("hidden");
            thisModal.find("#start").removeClass("hidden");
            thisModal.find("#response").addClass("hidden");
            thisModal.find("#listFileError").addClass('d-none')
            thisModal.find("#text-file-pass").addClass('d-none')
            thisModal.find('.list-file-pass').addClass('d-none');
            thisModal.find('#list-file-pass').addClass('d-none');
            thisModal.find(".file-upload-form-no")[0].reset();
            thisModal.find("#file-upload-form-button").attr("disabled", true);
        }
    }
    function uploadFile(e, file, thisModal) {
        if (e.dataTransfer) {
            thisModal.find('#file-upload').prop("files", e.dataTransfer.files);
        } else {
            thisModal.find('#file-upload').prop("files", e.target.files);
        }
        var xhr = new XMLHttpRequest(),
            fileInput = thisModal.find("#class-roster-file"),
            pBar = document.getElementById("file-progress"),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                // pBar.style.display = "inline";
                xhr.upload.addEventListener(
                    "loadstart",
                    setProgressMaxValue,
                    false
                );
                xhr.upload.addEventListener(
                    "progress",
                    updateFileProgress,
                    false
                );
            } else {
                output(
                    "サイズが最大110MBのアップロードするファイル"
                );
            }
        }
    }
    // Check for the various File API support.
    function setProgressMaxValue(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById("file-drag").style.display = "none";
    }
}
