function ekUpload() {
    function Init() {
        // var fileSelect = document.getElementById("file-upload"),
        var fileSelectCsv = $(".file-upload-test-csv"),
            fileSelectExcel = $(".file-upload-test-excel"),
            fileDrag = document.getElementById("file-drag"),
            submitButton = document.getElementById("submit-button");
        fileSelectCsv.on("change", fileSelectHandlerCsv);
        fileSelectExcel.on("change", fileSelectHandlerExcel);

        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", fileSelectHandler, false);
        }
    }

    function fileDragHover(e, thisModal, typeFile) {
        var fileDrag = thisModal.find(".file-upload-" + typeFile);

        e.stopPropagation();
        e.preventDefault();
        let type = "dragover" ? "hover" : "modal-body file-upload-" + typeFile;
        e.type === "dragover" ? "hover" : "modal-body file-upload-" + typeFile;
        fileDrag.removeClass("label-file-drag");
        fileDrag.addClass(type);
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var thisModal = $(this).closest(".modal");
        var files = $(this)[0].files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e, thisModal);

        // Process all File objects
        for (var i = 0, f; (f = files[i]); i++) {
            parseFile(f, thisModal);
            uploadFile(f, thisModal);
        }
    }

    function fileSelectHandlerCsv(e) {
        // Fetch FileList object
        var thisModal = $(this).closest(".modal");
        var files = $(this)[0].files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e, thisModal, "csv");

        // Process all File objects
        for (var i = 0, f; (f = files[i]); i++) {
            parseFile(f, thisModal, "csv");
            // uploadFile(f, thisModal, 'csv');
        }
    }

    function fileSelectHandlerExcel(e) {
        // Fetch FileList object
        var thisModal = $(this).closest(".modal");
        var files = $(this)[0].files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e, thisModal, "excel");

        // Process all File objects
        for (var i = 0, f; (f = files[i]); i++) {
            parseFile(f, thisModal, "excel");
        }
    }

    // Output
    function output(msg, modal, typeFile) {
        // Response
        modal.find(".messages-file-" + typeFile).html(msg);
    }

    function parseFile(file, modal, typeFile) {
        let dataType = modal
            .find(".file-upload-test-" + typeFile)
            .attr("data-type");
        output("<strong>" + file.name + "</strong>", modal, typeFile);
        var imageName = file.name;
        var isGood = /\.(?=xlsx|csv|xls)/gi.test(imageName);
        if (isGood) {
            if (dataType != 3) {
                if (typeFile == "csv") {
                    modal.find("#response-excel").addClass("hidden");
                } else {
                    modal.find("#response-csv").addClass("hidden");
                }
            }

            modal.find(".file-upload-form-button").removeAttr("disabled");
            modal.find(".start-" + typeFile).addClass("hidden");
            modal.find("#response-" + typeFile).removeClass("hidden");
            modal.find(".notimage").addClass("hidden");
        } else {
            modal.find("file-image").addClass("hidden");
            modal.find("notimage").removeClass("hidden");
            modal.find("start" + typeFile).removeClass("hidden");
            modal.find("response").addClass("hidden");
            modal.find("file-upload-form").reset();
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(file, modal) {
        var xhr = new XMLHttpRequest(),
            fileInput = document.getElementById("class-roster-file"),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                xhr.upload.addEventListener(
                    "loadstart",
                    setProgressMaxValue,
                    false
                );
                xhr.upload.addEventListener(
                    "progress",
                    updateFileProgress,
                    false
                );

                // File received / failed
                xhr.onreadystatechange = function (e) {
                    if (xhr.readyState == 4) {
                        // Everything is good!
                        // progress.className = (xhr.status == 200 ? "success" : "failure");
                        // document.location.reload(true);
                    }
                };

                // Start upload
                xhr.open(
                    "POST",
                    modal.getElementById("file-upload-form").action,
                    true
                );
                xhr.setRequestHeader("X-File-Name", file.name);
                xhr.setRequestHeader("X-File-Size", file.size);
                xhr.setRequestHeader("Content-Type", "multipart/form-data");
                xhr.send(file);
            } else {
                output(
                    "Please upload a smaller file (< " + fileSizeLimit + " MB)."
                );
            }
        }
    }

    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById("file-drag").style.display = "none";
    }
}
ekUpload();
