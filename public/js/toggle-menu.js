$(document).ready(function () {
    $(".nav-sidebar-body").slideUp();
    $(".nav-sidebar-heading").click(function (event) {
        $(this).next().slideToggle();
        $(this).toggleClass("active");
        $(this).children(".icon-polygon").toggleClass("active");
    });

    $(".heading-list-option").click(function (event) {
        $(this).next().slideToggle();
        $(this).toggleClass("active");
        $(this).children(".heading-icon-option").toggleClass("active");
    });

    $(".ic-left-option").click(function (event) {
        $(this).parent().next().slideToggle();
        $(this).toggleClass("active");
    });
});
