function ekUpload(modal) {
    function Init() {
        var thisModal = $(modal);
        var fileSelect = thisModal.find("#file-upload")[0],
            fileDrag = thisModal.find("#file-drag")[0],
            submitButton = thisModal.find("#submit-button")[0];
        fileSelect.addEventListener("change", fileSelectHandler);

        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", fileSelectHandler, false);
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById("file-drag");

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className =
            e.type === "dragover" ? "modal-body file-upload" : "modal-body file-upload";
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        var thisModal = $('.modal.fade.show');
        if (files.length > 0) {
            // Cancel event and hover styling
            fileDragHover(e);

            // Process all File objects
            for (var i = 0, f; (f = files[i]); i++) {
                parseFile(f, thisModal);
                uploadFile(e, f, thisModal);
            }
        } else {
            thisModal.find("#file-image").addClass("hidden");
            thisModal.find("#notimage").removeClass("hidden");
            thisModal.find("#start").removeClass("hidden");
            thisModal.find("#response").addClass("hidden");
            thisModal.find(".file-upload-form")[0].reset();
            thisModal.find("#file-upload-form-button").attr("disabled", true);
            thisModal.find("#error").text(' ');
        }
    }

    // Output
    function output(msg, thisModal) {
        // Response

        var m = thisModal.find("#messages")[0];
        m.innerHTML = msg;
    }

    function parseFile(file, thisModal) {
        output("<strong>" + file.name + "</strong>", thisModal);
        var imageName = file.name;
        var isGood = /\.(?=xlsx|csv|xls)/gi.test(imageName);
        if (isGood) {
            thisModal.find("#file-upload-form-button").removeAttr("disabled");
            thisModal.find("#start").addClass("hidden");
            thisModal.find("#response").removeClass("hidden");
            thisModal.find("#notimage").addClass("hidden");
        } else {
            thisModal.find("#file-image").addClass("hidden");
            thisModal.find("#notimage").removeClass("hidden");
            thisModal.find("#start").removeClass("hidden");
            thisModal.find("#response").addClass("hidden");
            thisModal.find("#file-upload-form").reset();
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(e, file, thisModal) {
        if (e.dataTransfer) {
            thisModal.find('#file-upload').prop("files", e.dataTransfer.files);
        } else {
            thisModal.find('#file-upload').prop("files", e.target.files);
        }
        var xhr = new XMLHttpRequest(),
            fileInput = thisModal.find("#class-roster-file"),
            pBar = document.getElementById("file-progress"),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                // pBar.style.display = "inline";
                xhr.upload.addEventListener(
                    "loadstart",
                    setProgressMaxValue,
                    false
                );
                xhr.upload.addEventListener(
                    "progress",
                    updateFileProgress,
                    false
                );
            } else {
                output(
                    "サイズが最大110MBのアップロードするファイル"
                );
            }
        }
    }

    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById("file-drag").style.display = "none";
    }
}
