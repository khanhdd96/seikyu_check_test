$(".popover-context")
    .popover({
        trigger: "manual",
        html: true,
        animation: false,
    })
    .on("mouseenter", function () {
        var _this = this;
        let content = $(this).data('content');
        content = replaceEntity(content)
        $(this).attr('data-content', content);
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover("hide");
        });
    })
    .on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
function replaceEntity(text) {
    text = text.replace(/<br \/>/g, '/n').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;').replace(/ /g, '&nbsp;').replace(/\/n/g, '<br />')
    return text
}
$(".context-popover")
    .popover({
        trigger: "manual",
        html: true,
        animation: false,
    })
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover("hide");
        });
    })
    .on("mouseleave", function () {
        var _this = this;
        if (!$(".popover:hover").length) {
            $(_this).popover("hide");
        }
    });

