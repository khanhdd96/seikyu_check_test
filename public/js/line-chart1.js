var ctx = document.getElementById("myChart1").getContext("2d");

const lineChartColors1 = ["#B5E5F0", "#00285A"];
const legendlabels2 = [
    "",
    "1月",
    "2月",
    "3月",
    "4月",
    "5月",
    "6月",
    "7月",
    "8月",
    "9月",
    "10月",
    "11月",
    "12月",
];
var datapoints = [
    [0, 50, 400, 25, , , , , , 420, 500, 10, 20],
    [0, 30, 150, 50, 25, 40, 10, 20, 120, 380, 400, 50, 40]
];
var suppliers = ["事業所 1", "事業所２"];

var myChart = new Chart(ctx, {
    type: "line",
    data: {
        labels: legendlabels2,
        datasets: datapoints.map((e, i) => ({
            backgroundColor: lineChartColors1[i],
            borderColor: lineChartColors1[i],
            fill: false,
            data: e,
            label: suppliers[i],
            lineTension: 0.5,
            pointRadius: 4,
            pointBorderColor: "#FFFFFF",
            pointBorderWidth: 2,
            pointBackgroundColor: lineChartColors1[i],
        })),
    },
    options: {
        plugins: {
            title: {
                display: true,
                text: "エラー数",
                align: "start",
                color: "#00285A",
                font: {
                    size: 12,
                    weight: "normal",
                },
                padding: { top: 0, left: 0, right: 0, bottom: 30 },
            },
            legend: {
                display: true,
                position: "bottom",
                align: "start",
                labels: {
                    boxWidth: 15,
                    boxHeight: 15,
                    fontColor: "#222",
                    padding: 30,
                },
            },
        },
        responsive: true, // Instruct chart js to respond nicely.
        maintainAspectRatio: true, // Add to prevent default behaviour of full-width/height
        interaction: {
            intersect: false,
            axis: "x",
        },
        scales: {
            x: {
                grid: {
                    color: "#CBCBCB",
                    borderColor: "#000000",
                    tickColor: "#8D8D8D",
                    display: false,
                },
                ticks: {
                    color: "#8D8D8D",
                },
            },
            y: {
                grid: {
                    color: "#CBCBCB",
                    borderColor: "#333",
                    tickColor: "#8D8D8D",
                    borderDash: [3, 3],
                },
                ticks: {
                    color: "#8D8D8D",
                    stepSize: 50
                },
                afterDataLimits(scale) {
                    scale.max += 50;
                },
            },
        },
    },
});
