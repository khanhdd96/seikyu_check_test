function ekUploadExcel(modal) {
    function Init() {

        var thisModal = $(modal);
        var fileSelect = thisModal.find("#file-upload-excel")[0],
            fileDrag = thisModal.find("#file-drag-excel")[0],
            submitButton = thisModal.find("#submit-button")[0];

        fileSelect.addEventListener("change", fileSelectHandler, false);
        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // File Drop
            fileDrag.addEventListener("dragover", fileDragHover, false);
            fileDrag.addEventListener("dragleave", fileDragHover, false);
            fileDrag.addEventListener("drop", fileSelectHandler, false);
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById("file-drag-excel");

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className =
            e.type === "dragover" ? "modal-body file-upload-excel" : "modal-body file-upload-excel";
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        var thisModal = $('.modal.fade.show');
        if (files.length > 0) {
            // Cancel event and hover styling
            fileDragHover(e);
            // Process all File objects
            for (var i = 0, f; (f = files[i]); i++) {
                parseFile(f, thisModal);
                uploadFile(e, f, thisModal);
            }
        } else {
            thisModal.find("#file-image-excel").addClass("hidden");
            thisModal.find("#notimage-excel").removeClass("hidden");
            thisModal.find("#start-excel").removeClass("hidden");
            thisModal.find("#response-excel").addClass("hidden");
            thisModal.find("#file-upload-excel").val('');
            thisModal.find("#file-upload-form-button").attr("disabled", true);
            thisModal.find("#error").text(' ');
        }
    }

    // Output
    function output(msg, thisModal) {
        // Response
        var m = thisModal.find("#messages-excel")[0];
        m.innerHTML = msg;
    }

    function parseFile(file, thisModal) {
        output("<strong>" + file.name + "</strong>", thisModal);
        var imageName = file.name;
        var isGood = /\.(?=xlsx|xls)/gi.test(imageName);
        var checkType = thisModal.find('.check_type_upload');
        if (isGood) {
            if (checkType.length > 0) {
                thisModal.find('button[type="submit"]').attr('disabled', true);
                var file = thisModal.find('#messages-csv').text();
                if (file) {
                    thisModal.find('button[type="submit"]').attr('disabled', false);
                }
            } else {
                thisModal.find('button[type="submit"]').attr('disabled', true);
            }
            thisModal.find("#response-excel").removeClass("hidden");
            thisModal.find("#notimage-excel").addClass("hidden");
        } else {
            thisModal.find("#file-image-excel").addClass("hidden");
            thisModal.find("#notimage-excel").removeClass("hidden");
            thisModal.find("#start-excel").removeClass("hidden");
            thisModal.find("#response-excel").addClass("hidden");
            thisModal.find('#messages-excel').empty();
            thisModal.find('button[type="submit"]').attr('disabled', true);
            thisModal.find("#file-upload-excel").val('');
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById("file-progress");

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(e, file, thisModal) {
        if (e.dataTransfer) {
            thisModal.find('#file-upload-excel').prop("files", e.dataTransfer.files);
        } else {
            thisModal.find('#file-upload-excel').prop("files", e.target.files);
        }
        var xhr = new XMLHttpRequest(),
            fileInput = document.getElementById("class-roster-file"),
            pBar = document.getElementById("file-progress"),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                // pBar.style.display = "inline";
                xhr.upload.addEventListener(
                    "loadstart",
                    setProgressMaxValue,
                    false
                );
                xhr.upload.addEventListener(
                    "progress",
                    updateFileProgress,
                    false
                );
            } else {
                output(
                    "サイズが最大110MBのアップロードするファイル"
                );
            }
        }
    }

    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById("file-drag-excel").style.display = "none";
    }
}
